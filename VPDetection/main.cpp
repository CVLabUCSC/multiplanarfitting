#include "main.h"

#include <vector>

#include <VPDetector.h>

using namespace std;
using namespace cv;

// Detect vanishing points and calibrate them individually
pair<Mat, LineClusters> detectVP(const cv::Mat &image, const Mat &K0, const CLUSTERING_TYPE type) {
	LineClusters lineClusters;
	pair<Mat, LineClusters> result;
	vector<int> indices;
	float inlierThr = 2.f;
	float lineLengthThr = 40.f;
	vector<int> selectedIndices;

	if (type == CLUSTERING_TYPE::RANSAC) {

	}
	else if (type == CLUSTERING_TYPE::T_LINKAGE) {
		lineClusters = LineClustering(image, 2, inlierThr, lineLengthThr, false);
	}
	else {

	}
	
	selectedIndices = VPDetector::findOrthogonalClusters(lineClusters, K0, inlierThr, result.first);

	result.second = lineClusters.subCluster(selectedIndices, inlierThr, true);

	return result;
}

inline bool filenameValidation(const String &filename) {
	// Check if the filename contains specific string
	if (filename.find("_VD") != String::npos) {
		return false;
	}

	return true;
}

void bundleCalibration(String path) {
	vector<String> filenames;
	vector<pair<Mat, LineClusters>> data;

	glob(path, filenames);

	// Detect vanishing directions and corresponding line clusters for each input image
	for (String filename : filenames) {
		if (!filenameValidation(filename)) {
			continue;
		}

		Mat image = imread(filename);

		if (image.empty()) {
			continue;
		}

		Mat K0 = (Mat_<float>(3, 3) << 800.f, 0, image.cols / 2, 0, 800.f, image.rows / 2, 0, 0, 1);

		data.push_back(detectVP(image, K0, CLUSTERING_TYPE::T_LINKAGE));
	}

	// Refine the vanishing directions and the calibration matrix

}

void singleCalibration(String filename) {

}

int main() {
	bundleCalibration("");
}
