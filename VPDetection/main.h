#pragma once

#include <opencv2\opencv.hpp>

#include <LineClustering.h>

enum CLUSTERING_TYPE {
	RANSAC = 0,
	T_LINKAGE = 1
};

// Detect vanishing directions of given image.
std::pair<cv::Mat, LineClusters> detectVP(const cv::Mat &image, const cv::Mat &K0, CLUSTERING_TYPE type);

inline bool filenameValidation(const cv::String &filename);

void bundleCalibration(cv::String path);

void singleCalibration(cv::String filename);

// Main entry
int main();
