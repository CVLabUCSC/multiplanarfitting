#pragma once

#include "RandomSampler.h"

#include <vector>

// Initialize random sampler using given parameters
void initRandomSampler(RandomSampler &randomSampler, const std::vector<CDataType*> &pointPairs,
	const int numOfModels, RS_NFSAMPLINGTYPE samplingType = NFST_UNIFORM);
