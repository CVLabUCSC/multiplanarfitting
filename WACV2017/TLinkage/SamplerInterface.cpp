#include "SamplerInterface.h"

using namespace std;

void initRandomSampler(RandomSampler &randomSampler, const vector<CDataType*> &pointPairs,
	const int numOfModels, RS_NFSAMPLINGTYPE samplingType)
{
	float SigmaExp = 1.0;
	int KdTreeRange = 10;
	float KdTreeCloseProb = 0.8f;
	float KdTreeFarProb = 1.0f - KdTreeCloseProb;

	// Add points to the random sampler
	randomSampler.SetPoints(pointPairs);

	// Initialize non-first sampling type
	switch (samplingType)
	{
	case NFST_EXP:
		randomSampler.SetNFSamplingTypeExp(SigmaExp);
		break;

	case NFST_NN:
		randomSampler.SetNFSamplingTypeNN(KdTreeRange, KdTreeCloseProb, KdTreeFarProb, false);
		break;

	case NFST_NN_ME:
		randomSampler.SetNFSamplingTypeNN(KdTreeRange, KdTreeCloseProb, KdTreeFarProb, true);
		break;
	}
}
