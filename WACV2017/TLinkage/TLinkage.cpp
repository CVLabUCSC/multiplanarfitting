#include "TLinkage.h"

using std::list;
using std::vector;

// given two Preference function Vectors compute the tanimoto distance
float PFTanimotoDist(const vector<float> &nB1, const vector<float> &nB2)
{
	assert(nB1.size() == nB2.size());

	float dot_prod = 0.f;
	float B1square = 0.f;
	float B2square = 0.f;

	for (unsigned int i = 0; i < nB1.size(); i++) {
		dot_prod += nB1[i] * nB2[i];
		B1square += nB1[i] * nB1[i];
		B2square += nB2[i] * nB2[i];
	}

	return 1.0f - dot_prod / (B1square + B2square - dot_prod);
}
