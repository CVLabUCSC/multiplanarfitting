#pragma once

#include <vector>
#include <list>

#include <opencv2\opencv.hpp>
#include "kdtree++\kdtree.hpp"
#include "..\types.h"
#include "..\PointPairCluster.h"

#ifndef JL_NO_THREADS
#define MAXTHREADS 8
#include <boost/thread/thread.hpp>
#include <boost/bind.hpp>
#include <boost/thread/mutex.hpp>
#endif

#ifndef JL_NO_THREADS
// Mutex for shared variables over threads
static boost::mutex mMutexMCurrentInvalidatingDistance;
#endif

struct sDist;
struct sClLnk;

// Store data points information
struct sPtLnk {
	const CDataType &mData;
	std::vector<float> mPreferenceFunction;
	sClLnk *mBelongingCluster;
	bool mToBeUpdateKDTree;
	unsigned int mAddedIdx;
	// Used for find k nearest ..
	bool mAlreadyFound;

	sPtLnk(const CDataType &dataType) : mData(dataType) {
	}
};

// Store cluster information
struct sClLnk{
	std::vector<float> mPreferenceFunction;
	std::list<sDist *>  mPairwiseTanimotoDistance;
	std::list<sPtLnk *> mBelongingPts;
	void *mOtherDataCl; // Pointer to other user-defined data. May be useful for user-defined cluster-cluster test
};

// Store the tanimoto distance beetwen two clusters
struct sDist{
	sClLnk *mCluster1;
	sClLnk *mCluster2;
	float mPairwiseTanimotoDistance;
	bool mToBeUpdated;

	bool operator()(const sDist *left, const sDist *right) const {
		return left->mPairwiseTanimotoDistance < right->mPairwiseTanimotoDistance;
	}
};

struct sPtLnkPointer{
	sPtLnk *mPtLnk;
	// Used for the kd-tree
	typedef float value_type;

	inline value_type operator[](size_t const N) const {
		return mPtLnk->mData[N];
	}
};

// Used for the kd-tree
inline bool operator==(sPtLnkPointer const& A, sPtLnkPointer const& B) {
	if (A.mPtLnk == B.mPtLnk) {
		return true;
	}
	return false;
}

// Predicate to check if a node was already extracted in k-nn
struct sPredicateAlreadyFoundJL {
	bool operator()(const sPtLnkPointer& A) const{
		return !A.mPtLnk->mAlreadyFound;
	}
};

// given two Preference function Vectors compute the tanimoto distance
float PFTanimotoDist(const std::vector<float> &nB1, const std::vector<float> &nB2);

class ClusterSort {
public:
	bool operator() (const sClLnk *left, const sClLnk *right) const {
		return left->mBelongingPts.size() > right->mBelongingPts.size();
	}

	bool operator() (const std::vector<std::pair<cv::Point2f, cv::Point2f>> &left,
		const std::vector<std::pair<cv::Point2f, cv::Point2f>> &right) const {
		return left.size() > right.size();
	}

	bool operator() (const PointPairCluster &left,
		const PointPairCluster &right) const {
		return left.size() > right.size();
	}
};

template<class MODEL>
class TLinkage
{
public:
	// Costructor and destructor
	TLinkage(float nInliersThreshold, // Scale of the algorithm
		double(*distanceFunction)(const MODEL &, const CDataType &),
		unsigned int nModelBufferSize = 0, // Model buffer size, set to 0 to allow growable size
		bool nCopyCoord = true, // Copy the coords value of the new points or use directly the pointer? (The second choice is faster but you need to deallocate memory outside the class)		
		unsigned int nPtDimension = 3, //Used by Kd-Tree
		int nKNeighboards = -1,  //K-neighboards for each points to be used for the clusterization (neg value means use all the dataset)
		bool(*nClusterClusterDiscardTest)(const sClLnk *, const sClLnk *) = NULL, // Function handler to perform a cluster-cluster test. Should return true if the cluster can be merged by some additional condition, false otherwise.
		void(*nInitializeAdditionalData)(sClLnk *) = NULL, // Function handler -  initialization routine when additional data are created
		void(*nClusterMergeAdditionalOperation)(const sClLnk *) = NULL, // Function handler - called when two clusters are merged
		void(*nDestroyAdditionalData)(sClLnk *) = NULL // Function handler -  called when additional data are destroyed
		) :
		mDataPoints(), mDataClusters(), mModels(), mDistancesToBeUpdated(), mKDTree(nPtDimension) {
		// Max models sizes, set to 0 to allow list
		mModelBufferSize = nModelBufferSize;
		mCurrentModelPointer = 0;
		mCopyPtsCoords = nCopyCoord;
		mInliersThreshold = nInliersThreshold;
		if (mModelBufferSize != 0)
			mModels.resize(mModelBufferSize, NULL);

		DistanceFunction = distanceFunction;

		mDistancesToBeUpdatedSize = 0;
		mKNeighboards = nKNeighboards;
		mClusterClusterDiscardTest = nClusterClusterDiscardTest;
		mClusterMergeAdditionalOperation = nClusterMergeAdditionalOperation;
		mInitializeAdditionalData = nInitializeAdditionalData;
		mDestroyAdditionalData = nDestroyAdditionalData;
		mDataPointsSize = 0;
	}

	~TLinkage() {

	}

	// Data Points accessor
	sPtLnk *AddPoint(const CDataType &dataPoint);

	unsigned int AddModel(MODEL &model);

	// Do a clusterization, return the final clusters
	const std::list<sClLnk *> DoTLClusterization(void(*OnProgress)(float) = NULL);

private:

	// Max models sizes, set to 0 to allow list (for incremental memory usage as-needed)
	unsigned int mModelBufferSize;
	unsigned int mCurrentModelPointer;

	bool mCopyPtsCoords;
	
	// Scale of the algorithm
	float mInliersThreshold;

	// Initial Data Points
	std::list<sPtLnk *> mDataPoints;
	unsigned int mDataPointsSize;

	// Current clusters
	std::list<sClLnk *> mDataClusters;

	// Current models
	std::vector<MODEL*> mModels;

	// Store the distances that have to be updated
	std::list<sDist *> mDistancesToBeUpdated;
	int mDistancesToBeUpdatedSize;

	// Mode k-nearest neighboard to find
	int mKNeighboards;

	// if the Number of neighboards is set(>0), we need a kd-tree to store the neighorhood information
	KDTree::KDTree<sPtLnkPointer> mKDTree;

	// Helper threadable functions
	static void UpdateDistance(sDist * tIterDist, float *nCurrentInvalidatingDistance,
		bool(*nClusterClusterDiscardTest)(const sClLnk *, const sClLnk *));

	// Compute preference function
	float PreferenceFunction(const MODEL &model, const CDataType &dataPoint);

	// Remove a model from the buffer
	// Helper function called only by addModel
	void RemoveModel(unsigned int nModelN);

	// Find k-nearest neighboard in kd-tree
	std::list<sPtLnkPointer> FindKNearest(sPtLnkPointer __V, const float __Max_R, int k, int nMaxSize);

	// Distance function must be defined by a user
	double(*DistanceFunction)(const MODEL &model, const CDataType &dataPoint);

	// Function handler to perform a cluster-cluster test. Should return true if the clusters can be merged togheter, false otherwise.
	// The test is additive to the one already performed inside J-Linkage. If function pointer is null, simply it's ignored
	bool(*mClusterClusterDiscardTest)(const sClLnk *, const sClLnk *);

	// Function handler - initialization routine when additional data are created
	void(*mInitializeAdditionalData)(sClLnk *);

	// Function handler - called when two clusters are merged
	void(*mClusterMergeAdditionalOperation)(const sClLnk *);

	// Function handler - called when additional data are destroyed
	void(*mDestroyAdditionalData)(sClLnk *);
};

// Add a new point to the class
template <class MODEL>
sPtLnk *TLinkage<MODEL>::AddPoint(const CDataType &dataPoint) {
	// Create a new point and a new cluster as well
	sPtLnk *nPt = new sPtLnk(dataPoint);
	sClLnk *nPtCl = new sClLnk();
	nPt->mBelongingCluster = nPtCl;
	nPtCl->mBelongingPts.push_back(nPt);

	// Resize the vector of preference function
	if (mModelBufferSize > 0) {
		nPt->mPreferenceFunction.resize(mModelBufferSize);
		nPtCl->mPreferenceFunction.resize(mModelBufferSize);
	}
	else {
		nPt->mPreferenceFunction.resize((unsigned int)mModels.size());
		nPtCl->mPreferenceFunction.resize((unsigned int)mModels.size());
	}

	// Create the Preference function -> check the distance of every points from the loaded models.
	nPt->mPreferenceFunction.resize((unsigned int)mModels.size());
	nPtCl->mPreferenceFunction.resize((unsigned int)mModels.size());

	// Calculate the PF of the new point
	unsigned int cModelCount = 0;
	for (vector<MODEL *>::iterator tIter = mModels.begin(); tIter != mModels.end(); ++tIter){
		if ((*tIter) == NULL)
			continue;

		float preference = PreferenceFunction((**tIter), nPt->mData);
		nPt->mPreferenceFunction[cModelCount] = preference;
		nPtCl->mPreferenceFunction[cModelCount] = preference;
		cModelCount++;
	}

	// Add the point to the kd-tree
	sPtLnkPointer nPtPointer;
	nPtPointer.mPtLnk = nPt;
	nPtPointer.mPtLnk->mAlreadyFound = false;
	mKDTree.insert(nPtPointer);
	//mKDTree.optimise();
	nPt->mToBeUpdateKDTree = true;

	// Add the point and the new cluster to the list
	mDataPoints.push_back(nPt);
	mDataClusters.push_back(nPtCl);
	nPt->mAddedIdx = mDataPointsSize;
	mDataPointsSize++;

	if (mInitializeAdditionalData != NULL)
		mInitializeAdditionalData(nPtCl);

	return nPt;
}

// Add a new model to the buffer - update the ps of all points and clusters
template <class MODEL>
unsigned int TLinkage<MODEL>::AddModel(MODEL &model)
{
	// If a model already exist in the position, remove it
	if (mModelBufferSize != 0 && mCurrentModelPointer < mModels.size()){
		RemoveModel(mCurrentModelPointer);
	}

	// Add the model to the buffer list
	if (mModelBufferSize == 0) {
		if (mCopyPtsCoords) {
			mModels.push_back(new MODEL(model));
		}
		else {
			mModels.push_back(&model);
		}
	}
	else {
		if (mCopyPtsCoords) {
			mModels[mCurrentModelPointer] = new MODEL(model);
		}
		else {
			mModels[mCurrentModelPointer] = &model;
		}
	}

	// Update the preference set of all the points
	for (std::list<sPtLnk *>::iterator tIter = mDataPoints.begin(); tIter != mDataPoints.end(); tIter++){
		if (mModelBufferSize == 0) {
			(*tIter)->mPreferenceFunction.resize(mCurrentModelPointer + 1);
		}

		(*tIter)->mPreferenceFunction[mCurrentModelPointer] = PreferenceFunction(model, (*tIter)->mData);
	}

	// Update the preference function of all the clusters
	for (std::list<sClLnk *>::iterator tIterCl = mDataClusters.begin(); tIterCl != mDataClusters.end(); tIterCl++)
	{
		if (mModelBufferSize == 0)
			(*tIterCl)->mPreferenceFunction.resize(mCurrentModelPointer + 1);

		bool allTrue = true;

		for (std::list<sPtLnk *>::iterator tIterPt = (*tIterCl)->mBelongingPts.begin(); tIterPt != (*tIterCl)->mBelongingPts.end(); tIterPt++) {
			if ((*tIterPt)->mPreferenceFunction[mCurrentModelPointer] == 0.f) {
				allTrue = false;
				break;
			}
		}

		// If the model set all to true we need a distance update
		if (allTrue) {
			float preference = 1.f;
			for (std::list<sPtLnk *>::iterator tIterPt = (*tIterCl)->mBelongingPts.begin(); tIterPt != (*tIterCl)->mBelongingPts.end(); tIterPt++) {
				float val = PreferenceFunction(model, (*tIterPt)->mData);
				if (val < preference) {
					preference = val;
				}
			}
			(*tIterCl)->mPreferenceFunction[mCurrentModelPointer] = preference;
			for (std::list<sDist *>::iterator tIterDist = (*tIterCl)->mPairwiseTanimotoDistance.begin(); tIterDist != (*tIterCl)->mPairwiseTanimotoDistance.end(); tIterDist++){
				if (!(*tIterDist)->mToBeUpdated) {
					mDistancesToBeUpdated.push_back((*tIterDist));
					mDistancesToBeUpdatedSize++;
					(*tIterDist)->mToBeUpdated = true;
				}
				// else already added to the update list
			}
		}
		else {
			(*tIterCl)->mPreferenceFunction[mCurrentModelPointer] = 0.f;
		}
	}

	unsigned int toReturn = mCurrentModelPointer;

	// Update the adder pointer
	if (mModelBufferSize == 0)
		++mCurrentModelPointer;
	else
		mCurrentModelPointer = (++mCurrentModelPointer) % mModelBufferSize;


	return toReturn;
}

template <class MODEL>
void TLinkage<MODEL>::RemoveModel(unsigned int nModelN){

	assert(nModelN < mModels.size());

	// Check if already removed
	if (mModels[nModelN] == NULL){
		return;
	}

	delete mModels[nModelN];

	mModels[nModelN] = NULL;

	// Update all the pts preference functions
	for (std::list<sPtLnk *>::iterator tIter = mDataPoints.begin(); tIter != mDataPoints.end(); tIter++){
		(*tIter)->mPreferenceFunction[nModelN] = 0.f;
	}

	// And all the cluster preferences functions
	// Update the preference set of all the clusters
	for (std::list<sClLnk *>::iterator tIterCl = mDataClusters.begin(); tIterCl != mDataClusters.end(); tIterCl++){
		// If the preferencet of the cluster for the model was true, we need a distance update
		if ((*tIterCl)->mPreferenceFunction[nModelN] > 0.f) {
			(*tIterCl)->mPreferenceFunction[mCurrentModelPointer] = 0.f;
			for (std::list<sDist *>::iterator tIterDist = (*tIterCl)->mPairwiseTanimotoDistance.begin(); tIterDist != (*tIterCl)->mPairwiseTanimotoDistance.end(); tIterDist++) {
				if (!(*tIterDist)->mToBeUpdated){
					mDistancesToBeUpdated.push_back((*tIterDist));
					mDistancesToBeUpdatedSize++;
					(*tIterDist)->mToBeUpdated = true;
				}
				// else already added to the update list
			}
		}
	}
}

// Compute preference function
template <class MODEL>
float TLinkage<MODEL>::PreferenceFunction(const MODEL &model, const CDataType &dataPoint)
{
	float distance = DistanceFunction(model, dataPoint);

	if (distance < mInliersThreshold) {
		return std::exp(-distance / (mInliersThreshold / 5.0f));
	}
	else {
		return 0.f;
	}
}

template <class MODEL>
const std::list<sClLnk *> TLinkage<MODEL>::DoTLClusterization(void(*OnProgress)(float))
{
	float tEps = (float)1.0f / (1.0f + (float)mModels.size()); // Just a distance to see if the tanimoto distance is equal to one
	float tOneEpsDistance = 1.0f - tEps; // Just a distance to see if the tanimoto distance is equal to one

	// Update neighboardhood information
	mKDTree.optimise();

	for (list<sPtLnk *>::iterator tIterPt = mDataPoints.begin(); tIterPt != mDataPoints.end(); ++tIterPt)
	{
		if (!(*tIterPt)->mToBeUpdateKDTree)
			continue;
		// Threadable build Distance list for kdtree
		sPtLnkPointer nPtPointer; nPtPointer.mPtLnk = *tIterPt; nPtPointer.mPtLnk->mAlreadyFound = false;
		list<sPtLnkPointer> kneigh = FindKNearest(nPtPointer, std::numeric_limits<float>::max(), mKNeighboards, (int)mDataPointsSize);

		// Find K-Nearest
		for (list<sPtLnkPointer>::iterator tIter = kneigh.begin(); tIter != kneigh.end(); ++tIter)
		{
			//for(std::vector<sPtLnkPointer>::iterator tIter = nearPtsIdx.begin(); tIter != nearPtsIdx.end(); ++tIter){
			if ((*tIter).mPtLnk->mBelongingCluster == (*tIterPt)->mBelongingCluster)
				continue;

			bool alreadyPresent = false;
			for (list<sDist *>::iterator distIter = (*tIter).mPtLnk->mBelongingCluster->mPairwiseTanimotoDistance.begin();
				distIter != (*tIter).mPtLnk->mBelongingCluster->mPairwiseTanimotoDistance.end();
				distIter++){

				if ((*distIter)->mCluster1 == (*tIterPt)->mBelongingCluster || (*distIter)->mCluster2 == (*tIterPt)->mBelongingCluster)
					alreadyPresent = true;
			}

			if (alreadyPresent)
				continue;

			sDist *tDist = new sDist;
			tDist->mCluster1 = (*tIterPt)->mBelongingCluster;
			tDist->mCluster2 = (*tIter).mPtLnk->mBelongingCluster;

			(*tIterPt)->mBelongingCluster->mPairwiseTanimotoDistance.push_back(tDist);
			(*tIter).mPtLnk->mBelongingCluster->mPairwiseTanimotoDistance.push_back(tDist);

			tDist->mToBeUpdated = true;
			mDistancesToBeUpdated.push_back(tDist);
			mDistancesToBeUpdatedSize++;
		}

		(*tIterPt)->mToBeUpdateKDTree = false;
	}

	if (OnProgress != NULL)
		OnProgress(0.01f);

	// First step: update all the pw distances that needs an update
	// Please Note: If a distance don't need to be updated it means that it would be certainly equal to 1 from the previous JLClusterization
	// --> Store also a list with ALL the pairwise unique jaccard distance
	vector<sDist *> mHeapDistanceList(mDistancesToBeUpdatedSize);

	unsigned int counter = 0;
	while (mDistancesToBeUpdatedSize > 0)
	{
		sDist *tDist = mDistancesToBeUpdated.back();
		mDistancesToBeUpdated.pop_back();
		mDistancesToBeUpdatedSize--;
		tDist->mPairwiseTanimotoDistance =
			PFTanimotoDist(tDist->mCluster1->mPreferenceFunction,
			tDist->mCluster2->mPreferenceFunction);
		tDist->mToBeUpdated = false;
		if (tDist->mPairwiseTanimotoDistance < tOneEpsDistance){
			mHeapDistanceList[counter] = tDist;
			++counter;
		}
	}

	if (OnProgress != NULL)
		OnProgress(0.02f);

	mHeapDistanceList.resize(counter);

	// A distance that will invalidate the heap, needing a heap resort
	float mCurrentInvalidatingDistance = 1.0f;

	// Make the heap
	std::sort(mHeapDistanceList.begin(), mHeapDistanceList.end(), sDist());

	unsigned int currentSortIdx = 0;
	unsigned int initialDistance = (unsigned int)mHeapDistanceList.size();

	while (mHeapDistanceList.size() > 0)
	{
		sDist *sCurrentMinDist = NULL;
		if (currentSortIdx < mHeapDistanceList.size())
			sCurrentMinDist = mHeapDistanceList[currentSortIdx];
		// TODO speed up previous line!

		if (sCurrentMinDist == NULL || sCurrentMinDist->mPairwiseTanimotoDistance > tOneEpsDistance && mCurrentInvalidatingDistance > tOneEpsDistance)
		{
			// All the distance will be equals to one - clusterization is finished
			// We've finished since all distances have been processed or are equal to 1.0f
			mHeapDistanceList.clear();
		}
		else if (sCurrentMinDist->mCluster1 == NULL || sCurrentMinDist->mCluster2 == NULL)
		{
			// Eliminate the non-valid distance(belong to an eliminated clusters
			for (std::vector<sDist *>::iterator tIterDist = mHeapDistanceList.begin() + currentSortIdx + 1; tIterDist != mHeapDistanceList.end(); tIterDist++){
				assert((*tIterDist) != sCurrentMinDist);
			}
			delete sCurrentMinDist;
			++currentSortIdx;
		}
		else if (sCurrentMinDist->mPairwiseTanimotoDistance > (mCurrentInvalidatingDistance - tEps))
		{
			// We need an heap resort
			mHeapDistanceList.erase(mHeapDistanceList.begin(), mHeapDistanceList.begin() + (currentSortIdx));

			if (mHeapDistanceList.size() > 1){

				// First eliminate all the distance equals to one from the heap
				// Push all these distances to the end of the vector...
				unsigned int idxElementProcessing = 0;
				unsigned int idxLastUsefullElement = (unsigned int)mHeapDistanceList.size() - 1;

				while (idxElementProcessing <= idxLastUsefullElement && idxLastUsefullElement > 0)
				{
					if (mHeapDistanceList[idxElementProcessing]->mPairwiseTanimotoDistance > tOneEpsDistance || mHeapDistanceList[idxElementProcessing]->mCluster1 == NULL || mHeapDistanceList[idxElementProcessing]->mCluster2 == NULL){
						// In this case we need to move the distance to the end

						// Swap elements
						sDist *temp = mHeapDistanceList[idxElementProcessing];
						mHeapDistanceList[idxElementProcessing] = mHeapDistanceList[idxLastUsefullElement];
						mHeapDistanceList[idxLastUsefullElement] = temp;

						// If the distance belongs to one eliminated cluster, delete it
						if (mHeapDistanceList[idxLastUsefullElement]->mCluster1 == NULL || mHeapDistanceList[idxLastUsefullElement]->mCluster2 == NULL)
							delete mHeapDistanceList[idxLastUsefullElement];

						idxLastUsefullElement--;
					}
					else{
						idxElementProcessing++;
					}
				}

				if (idxLastUsefullElement > 0){
					// ... and then erase them
					if (idxLastUsefullElement < mHeapDistanceList.size() - 1)
						mHeapDistanceList.erase(mHeapDistanceList.begin() + idxLastUsefullElement + 1, mHeapDistanceList.end());
					// Re-Set the heap
					std::sort(mHeapDistanceList.begin(), mHeapDistanceList.end(), sDist());
				}
				else{
					// Ok we finished
					mHeapDistanceList.clear();
				}

			}

			mCurrentInvalidatingDistance = 2.0f;
			currentSortIdx = 0;
		}
		else {
			// The distance is less than the invalidating distance, merge the two cluster and update the other distances accordingly

			// if kd-tree is used, merge the distances of the clusters for adding the new ones of the new created cluster
			list<sClLnk *> distancesToBeAdded;

			sCurrentMinDist->mCluster2->mPairwiseTanimotoDistance.remove(sCurrentMinDist);
			sCurrentMinDist->mCluster1->mPairwiseTanimotoDistance.remove(sCurrentMinDist);

			for (list<sDist *>::iterator distIter1 = sCurrentMinDist->mCluster2->mPairwiseTanimotoDistance.begin(); distIter1 != sCurrentMinDist->mCluster2->mPairwiseTanimotoDistance.end(); ++distIter1){
				// Threadable Check distance to merge KD-Tree
				bool add = true;
				// Check if the distance already exists
				for (std::list<sDist *>::iterator distIter2 = sCurrentMinDist->mCluster1->mPairwiseTanimotoDistance.begin(); distIter2 != sCurrentMinDist->mCluster1->mPairwiseTanimotoDistance.end(); ++distIter2){
					if ((*distIter1)->mCluster1 == (*distIter2)->mCluster1 || (*distIter1)->mCluster2 == (*distIter2)->mCluster2
						|| (*distIter1)->mCluster2 == (*distIter2)->mCluster1 || (*distIter1)->mCluster1 == (*distIter2)->mCluster2){
						add = false; // Point already present
						break;
					}
				}
				if (add){
					if ((*distIter1)->mCluster1 != sCurrentMinDist->mCluster2)
						distancesToBeAdded.push_back((*distIter1)->mCluster1);
					else if ((*distIter1)->mCluster2 != sCurrentMinDist->mCluster2)
						distancesToBeAdded.push_back((*distIter1)->mCluster2);
				}

			}


			// Update the cluster pointer of all the points of the deleted cluster
			for (std::list<sPtLnk *>::iterator ptIter = sCurrentMinDist->mCluster2->mBelongingPts.begin(); ptIter != sCurrentMinDist->mCluster2->mBelongingPts.end(); ptIter++)
				(*ptIter)->mBelongingCluster = sCurrentMinDist->mCluster1;


			// Merge the two clusters into cluster 1
			for (unsigned int i = 0; i < sCurrentMinDist->mCluster1->mPreferenceFunction.size(); i++) {
				sCurrentMinDist->mCluster1->mPreferenceFunction[i] =
					sCurrentMinDist->mCluster1->mPreferenceFunction[i] < sCurrentMinDist->mCluster2->mPreferenceFunction[i] ?
					sCurrentMinDist->mCluster1->mPreferenceFunction[i] : sCurrentMinDist->mCluster2->mPreferenceFunction[i];
			}
			sCurrentMinDist->mCluster1->mBelongingPts.merge(sCurrentMinDist->mCluster2->mBelongingPts);

			// Delete cluster 2
			mDataClusters.remove(sCurrentMinDist->mCluster2);

			for (std::list<sClLnk *>::iterator clIter = distancesToBeAdded.begin(); clIter != distancesToBeAdded.end(); ++clIter)
			{
				// Threadable Add kd-tree distances
				sDist *tDist = new sDist;
				tDist->mCluster1 = sCurrentMinDist->mCluster1;
				tDist->mCluster2 = *clIter;

				sCurrentMinDist->mCluster1->mPairwiseTanimotoDistance.push_back(tDist);
				(*clIter)->mPairwiseTanimotoDistance.push_back(tDist);
				mHeapDistanceList.push_back(tDist);
			}

			// Update all the distances of the old cluster -- delete
			for (std::list<sDist *>::iterator tIterDist = sCurrentMinDist->mCluster2->mPairwiseTanimotoDistance.begin(); tIterDist != sCurrentMinDist->mCluster2->mPairwiseTanimotoDistance.end(); tIterDist++)
			{
				// Threadable: delete distance for the old cluster
				if (sCurrentMinDist != (*tIterDist)){
					if ((*tIterDist)->mCluster1 != sCurrentMinDist->mCluster2)
						(*tIterDist)->mCluster1->mPairwiseTanimotoDistance.remove((*tIterDist));
					else if ((*tIterDist)->mCluster2 != sCurrentMinDist->mCluster2)
						(*tIterDist)->mCluster2->mPairwiseTanimotoDistance.remove((*tIterDist));
					(*tIterDist)->mCluster1 = NULL;
					(*tIterDist)->mCluster2 = NULL;
				}
			}

			// Do additional user-defined update steps if specified
			if (mClusterMergeAdditionalOperation != NULL)
				mClusterMergeAdditionalOperation(sCurrentMinDist->mCluster1);

#ifndef JL_NO_THREADS
			boost::thread_group *tg = new boost::thread_group();
			// Update all the distances of the new cluster
			for (std::list<sDist *>::iterator tIterDist = sCurrentMinDist->mCluster1->mPairwiseTanimotoDistance.begin(); tIterDist != sCurrentMinDist->mCluster1->mPairwiseTanimotoDistance.end(); tIterDist++){
				// Update distances
				tg->create_thread(boost::bind(&(UpdateDistance), *tIterDist, &mCurrentInvalidatingDistance, mClusterClusterDiscardTest));
				if (tg->size() >= MAXTHREADS){
					tg->join_all();
					delete tg;
					tg = new boost::thread_group();
				}
			}
			tg->join_all();
			delete tg;
#else
			for (std::list<sDist *>::iterator tIterDist = sCurrentMinDist->mCluster1->mPairwiseJaccardDistance.begin(); tIterDist != sCurrentMinDist->mCluster1->mPairwiseJaccardDistance.end(); tIterDist++){
				// Update distances
				JLinkage::UpdateDistance(*tIterDist, &mCurrentInvalidatingDistance, mClusterClusterDiscardTest);
			}

#endif

			if (OnProgress != NULL)
				OnProgress(1.0f - ((float)(mHeapDistanceList.size() - currentSortIdx) / (float)initialDistance));

			// Delete old cluster
			if (sCurrentMinDist->mCluster2 && mDestroyAdditionalData)
				mDestroyAdditionalData(sCurrentMinDist->mCluster2);

			delete sCurrentMinDist->mCluster2;
			delete sCurrentMinDist;

			++currentSortIdx;

		}
	}

	if (OnProgress != NULL)
		OnProgress(1.0f);

	// return the list of clusters
	return mDataClusters;
}

template <class MODEL>
void TLinkage<MODEL>::UpdateDistance(sDist *tIterDist, float *nCurrentInvalidatingDistance,
	bool(*nClusterClusterDiscardTest)(const sClLnk *, const sClLnk *))
{
	tIterDist->mPairwiseTanimotoDistance =
		PFTanimotoDist(tIterDist->mCluster1->mPreferenceFunction,
		tIterDist->mCluster2->mPreferenceFunction);

	// If pw distance is < 1, Do also additional test if a function is specified
	if (tIterDist->mPairwiseTanimotoDistance < 1.0f
		&& (nClusterClusterDiscardTest != NULL && !nClusterClusterDiscardTest(tIterDist->mCluster1, tIterDist->mCluster2)))
		tIterDist->mPairwiseTanimotoDistance = 1.0f;

#ifndef JL_NO_THREADS
	boost::mutex::scoped_lock
		lock(mMutexMCurrentInvalidatingDistance);
#endif
	// Store the minimum updated distance
	if (tIterDist->mPairwiseTanimotoDistance < *nCurrentInvalidatingDistance)
		*nCurrentInvalidatingDistance = tIterDist->mPairwiseTanimotoDistance;
}

// Find K-nearest neighboard from the kd-tree
template <class MODEL>
std::list<sPtLnkPointer> TLinkage<MODEL>::FindKNearest(sPtLnkPointer __V, float const __Max_R, int k, int nMaxSize){
	int msSizePRAF = 0;
	std::list<sPtLnkPointer> msAlreadyFoundPRAF;
	__V.mPtLnk->mAlreadyFound = true;
	while (msSizePRAF < k && msSizePRAF < nMaxSize){
		std::pair<KDTree::KDTree<sPtLnkPointer>::iterator, float> nif = mKDTree.find_nearest_if(__V, __Max_R, sPredicateAlreadyFoundJL());
		msAlreadyFoundPRAF.push_back(*nif.first);
		msAlreadyFoundPRAF.back().mPtLnk->mAlreadyFound = true;
		msSizePRAF++;
	}
	// Reset values
	for (std::list<sPtLnkPointer>::iterator it = msAlreadyFoundPRAF.begin(); it != msAlreadyFoundPRAF.end(); ++it)
		(*it).mPtLnk->mAlreadyFound = false;
	__V.mPtLnk->mAlreadyFound = false;

	return msAlreadyFoundPRAF;
}
