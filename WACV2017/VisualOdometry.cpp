#include "VisualOdometry.h"

#include <ceres\ceres.h>

namespace VisualOdometry {
	using namespace std;
	using namespace cv;

	Point3d mulPointMat(const Point2f &P, const Mat &M) {
		Point3d MP(
			M.at<float>(0, 0) * P.x + M.at<float>(0, 1) * P.y + M.at<float>(0, 2),
			M.at<float>(1, 0) * P.x + M.at<float>(1, 1) * P.y + M.at<float>(1, 2),
			M.at<float>(2, 0) * P.x + M.at<float>(2, 1) * P.y + M.at<float>(2, 2));

		return MP;
	}

	Mat refineTranslation(const vector<pair<Point2f, Point2f>> &pointPairs, const Mat &K, const Mat &t) {
		const Mat Kinv = K.inv();
		ceres::Problem problem;
		double x[3];	// intial translation

		if (t.empty()) {
			x[0] = 1.0f;
			x[1] = 1.0f;
			x[2] = 1.0f;
		}
		else {
			if (t.type() != CV_64F) {
				cout << "Initial translation vector has an invalid type." << endl;
				return Mat();
			}
			else if (t.cols == 3 && t.rows == 1) {
				x[0] = t.at<float>(0);
				x[1] = t.at<float>(1);
				x[2] = t.at<float>(2);
			}
			else {
				cout << "Initial translation vector has an invalid dimension: " << t.size() << endl;
				cout << "It must be a 3x1 column vector" << endl;
				return Mat();
			}
		}

		// x should be the variable to be optimized
		for (const auto &pointPair : pointPairs) {
			problem.AddResidualBlock(
				EssentialResidual::Create({
				mulPointMat(pointPair.first, Kinv),
				mulPointMat(pointPair.second, Kinv) }),
				new ceres::CauchyLoss(0.5f), x);
		}

		ceres::Solver::Options options;
		ceres::Solver::Summary summary;

		options.linear_solver_type = ceres::DENSE_SCHUR; // Either DENSE_SCHUR or DENSE_QR
		options.minimizer_progress_to_stdout = true;
		ceres::Solve(options, &problem, &summary);

		cout << summary.FullReport() << endl;
		system("pause");

		return (Mat_<float>(3, 1) << x[0], x[1], x[2]);
	}
}