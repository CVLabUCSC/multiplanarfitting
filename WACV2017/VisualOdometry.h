#pragma once

#include "EssentialResidual.h"

#include <vector>

namespace VisualOdometry {
	/**
	 Multiple given point P with given matrix M, e.g., M * P.
	 In this function, P is regarded as a homogeneous point.
	 @param P An input point.
	 @param M A 3x3 input matrix.
	 @return Return M multiplied with a homogeneous representation of P.
	 */
	cv::Point3d mulPointMat(const cv::Point2f &P, const cv::Mat &M);

	cv::Mat refineTranslation(const std::vector<std::pair<cv::Point2f, cv::Point2f>> &pointPairs,
		const cv::Mat &K, const cv::Mat &t = cv::Mat());
}