#pragma once

#include <vector>

#include <opencv2\opencv.hpp>

#include "LineClustering.h"
#include "PointPairClustering.h"

namespace PlaneFitting {
	struct EqKeypoints {
		EqKeypoints(const float &threshold)
			: m_threshold(threshold) {}

		bool operator()(const cv::Point2f& a, const cv::Point2f& b) const {
			return norm(a - b) < m_threshold;
		}

		const float m_threshold;
	};

	// Compute Jaccard Distance between two clusters
	double computeJaccardDistance(const PointPairCluster &cluster1, const PointPairCluster &cluster2);

	bool isCentroidOnSameSide(const PointPairCluster &cluster1, const PointPairCluster &cluster2, const cv::Mat &VL);

	double MPDMergeDistance(const PointPairCluster &cluster1, const PointPairCluster &cluster2);

	// Sample points based on their regions
	// image		: input image
	// lineClusters	: detected line clusters
	// pointPairs	: point correspondences
	// K			: The camera calibration matrix
	std::vector<std::vector<unsigned int>> regionBasedSampling(const cv::Mat &image,
		LineClusters &lineClusters, const std::vector<CDataType*> pointPairs,
		const cv::Mat &K);

	// Generate homographies based on points' regions
	std::vector<cv::Mat> generateMWHomographies(const cv::Mat &image,
		LineClusters &lineClusters, const std::vector<CDataType*> pointPairs,
		const cv::Mat &K, const cv::Mat &R, const cv::Mat &VD, bool useGlobalNormals,
		bool findLineByExtending, std::vector<cv::Mat> &normals = std::vector<cv::Mat>());

	// Generate homographies based on points' regions
	float generateMWHomographiesDebug(const cv::Mat &image,
		LineClusters &lineClusters, const std::vector<CDataType*> pointPairs,
		const cv::Mat &K, const cv::Mat &R, const cv::Mat &VD);

	std::vector<cv::Mat> generateMWHomographies(const cv::Mat &image,
		std::vector<LineClusters> &lineClusters, const std::vector<CDataType*> pointPairs,
		const cv::Mat &K, std::vector<cv::Mat> &normals = std::vector<cv::Mat>());

	// Generate homorgaphies based on given orientation map
	std::vector<cv::Mat> generateMWHomographies(const cv::Mat &image,
		const std::vector<CDataType*> pointPairs, const cv::Mat &OM,
		const cv::Mat &K, const cv::Mat &R, const cv::Mat &VD, int modelsPerGroup,
		std::vector<cv::Mat> &normals = std::vector<cv::Mat>());

	// Compute planar homographies in a Manhattan world with given point pairs.
	std::vector<cv::Mat> generateMWHomographies(const std::vector<CDataType*> &pointPairs,
		const std::vector<std::vector<unsigned int>> &indices, const int minimumSampleSet,
		const cv::Mat &K1, const cv::Mat &K2, const cv::Mat &R, const cv::Mat &VD1,
		std::vector<cv::Mat> &normals = std::vector<cv::Mat>());

	std::vector<cv::Mat> generateWMWHomographies(const std::vector<CDataType*> &pointPairs,
		const std::vector<std::vector<unsigned int>> &indices, const int minimumSampleSet,
		const cv::Mat &K1, const cv::Mat &K2, const cv::Mat &R);

	// Generate planar homograhpies with given point pairs
	std::vector<cv::Mat> generateHomographies(const std::vector<CDataType*> &pointPairs, 
		const std::vector<std::vector<unsigned int>> &indices, const int minimumSampleSet);

	// Compute planar homographies from each point pair cluster
	std::vector<cv::Mat> computeHomographies(const PointPairClusters &pointPairClusters, const int method);

	// Compute Manhattan-world homographies from each point pair cluster
	std::vector<cv::Mat> computeMWHomographies(const PointPairClusters &pointPairClusters,
		const cv::Mat &K1, const cv::Mat &K2, const cv::Mat &R, const cv::Mat &VD,
		std::vector<cv::Mat> &normals = std::vector<cv::Mat>());

	// Compute an array of normals of each cluster. Normal of each cluster must be initialized.
	std::vector<cv::Mat> computeNormals(const PointPairClusters &pointPairClusters, const cv::Mat &VD);

	// Merge clusters using Manhattan-world homographies
	PointPairClusters mergeClusters(const PointPairClusters &pointPairClusters,
		const cv::Mat &K1, const cv::Mat &K2, const cv::Mat &R, const cv::Mat &VD, const float threshold,
		const int method = CV_RANSAC, cv::Mat debugImage1 = cv::Mat());

	// Merge clusters using unconstrained homographies
	PointPairClusters mergeClusters(const PointPairClusters &pointPairClusters,
		const cv::Mat &K1, const cv::Mat &VD, const float threshold, const int method = CV_RANSAC,
		const cv::Mat debugImage1 = cv::Mat());

	PointPairClusters mergeClusters(const PointPairClusters &pointPairClusters,
		const float threshold, const int method = CV_RANSAC);

	std::vector<PointPairCluster> multiplePlaneDetection(const PointPairClusters &pointPairClusters,
		const cv::Size &imageSize, const float threshold);
}