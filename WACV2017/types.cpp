#include "types.h"


PointPair::PointPair(const cv::Point2f &point1, const cv::Point2f &point2) :
Point1(point1), Point2(point2) {
}

unsigned int PointPair::getPointDimension() const {
	return 4;
}

const float PointPair::getElement(std::size_t idx) const {
	switch (idx % 4) {
	case 0:
		return Point1.x;
	case 1:
		return Point1.y;
	case 2:
		return Point2.x;
	case 3:
		return Point2.y;
	default:
		return Point1.x;
	}
}

float PointPair::operator[](std::size_t idx) {
	return getElement(idx);
}

const float PointPair::operator[](std::size_t idx) const {
	return getElement(idx);
}