#pragma once

#include "TLinkage\TLinkage.h"

#include "PointPairClusters.h"

//-------------------------------------
// Utility functions
//-------------------------------------

// Point reprojection error computed by given homography.
double ReprojectionError(const cv::Mat &homography, const CDataType &dataType);

// Euclidean distance between first point of two given point pairs.
double EuclideanDistancePointPair(const CDataType &dataPoint1, const CDataType &dataPoint2);

// Convert clusters list into two vectors of points.
void convertClusters(const std::list<sClLnk *> &clustersList,
	std::vector<std::vector<cv::Point2f>> &points1, std::vector<std::vector<cv::Point2f>> &points2);

// Convert clusters list into two vectors of points.
std::vector<std::vector<PointMatch>> convertClusters(const std::list<sClLnk *> &clustersList);

std::vector<PointPairCluster> convertClusters(const std::list<sClLnk *> &clustersList,
	const std::vector<cv::Mat> &normals);

cv::Mat drawPointPairs(const cv::Mat &image1, const cv::Mat &image2, std::vector<CDataType*> pointPairs, int radius);

cv::Mat drawPointPairs(const cv::Mat &image1, const cv::Mat &image2, std::vector<CDataType> pointPairs, int radius);

cv::Mat drawPoints(const cv::Mat &image, std::vector<std::vector<cv::Point2f>> &points, int radius);

cv::Mat drawPoints(const cv::Mat &image, const std::vector<PointMatch> &pointPairClusters, int radius);

cv::Mat drawPoints(const cv::Mat &image, std::vector<std::vector<std::pair<cv::Point2f, cv::Point2f>>> &pointPairClusters, int radius);

cv::Mat drawPoints(const cv::Mat &image, const std::vector<CDataType*> &points, int radius);

cv::Mat drawPoints(const cv::Mat &image, const std::vector<PointPairCluster> &pointPairClusters,
	int radius, const cv::Mat &VD = cv::Mat());

cv::Mat drawPointsL(const cv::Mat &image, const std::vector<PointPairCluster> &pointPairClusters, int radius);

cv::Mat drawPoints(const cv::Mat &image, const PointPairClusters &pointPairClusters,
	int radius, const cv::Mat &VD = cv::Mat(), const cv::Mat &K = cv::Mat(), const cv::Mat &R = cv::Mat());

//-------------------------------------
// Point Pairs Clustering Functions
//-------------------------------------

void computeSIFTFeaturePoints(const cv::Mat &image, std::vector<cv::KeyPoint> &keypoints, cv::Mat &descriptors);

void computeMatches(const std::vector<cv::KeyPoint> &keypoints1,
	const std::vector<cv::KeyPoint> &keypoints2,
	const cv::Mat &descriptors1, const cv::Mat &descriptors2, std::vector<CDataType*> &pointPairs);

void computeMatches(const std::vector<cv::KeyPoint> &keypoints1,
	const std::vector<cv::KeyPoint> &keypoints2,
	const cv::Mat &descriptors1, const cv::Mat &descriptors2,
	std::vector<std::pair<cv::Point2f, cv::Point2f>> &pointPairs);

// Compute point matches using SIFT descriptors and FLANN matcher
void computeMatches(cv::Mat image1, cv::Mat image2, std::vector<CDataType*> &pointPairs);

// Generate random homographies with given point pairs.
std::vector<cv::Mat> generateHomographies(std::vector<CDataType*> &pointPairs, const int minimumSampleSet);

//-------------------------------------
// Homography Functions
//-------------------------------------

std::vector<std::pair<cv::Point2f, cv::Point2f>>
	computeInliers(const std::vector<CDataType*> &pointPairs, const cv::Mat &H, const double threshold = 2.5);

//-------------------------------------
// Main Entry
//-------------------------------------

// Test point pair clustering using T-linkage with given image pair
cv::Mat PointPairClustering(cv::Mat image1, cv::Mat image2);

std::vector<PointPairCluster> PointPairClustering(const std::vector<CDataType*> &pointPairs,
	std::vector<cv::Mat> &models, const std::vector<cv::Mat> &normals = std::vector<cv::Mat>());

PointPairClusters generatePointPairClusters(const std::vector<CDataType*> &pointPairs,
	const cv::Mat &label, const cv::Mat &VD, float threshold = 3.f);
