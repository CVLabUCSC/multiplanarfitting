#pragma once

#include "LineClusters.h"

namespace VPDetector {
	// Choose three clusters with mutually orthogonal vanishing directions.
	// The initial calibration matrix K does not need to be accurate, and will be refined.
	// Returns a 3x3 matrix where each column represents vanishing directions.
	// The first column aways represents the vertical vanishing direction.
	cv::Mat detectVP(const LineClusters &lineClusters, std::vector<int> &selectedIndices, cv::Mat &K, float threshold);

	std::vector<cv::Mat> detectVP(const std::vector<LineClusters> &lineClusters,
		std::vector<std::vector<int>> &selectedIndices, cv::Mat &K, float inlierThreshold);

	// Find clusters with mutually orthogonal vanishing directions. Vanishing directions will be computed using
	// given calibration matrix. You can expect good selection of clusters even if the calibration matrix is not accurate.
	// threshold: inlier threshold. Typically this threshold is set to around 2.0.
	std::vector<int> findOrthogonalClusters(const LineClusters &lineClusters, const cv::Mat &K,
		const float threshold, cv::Mat &VD = cv::Mat());

	// Warper of refineCalibration function. This function computes vanishing directions of each cluster
	// with given calibration matrix.
	// It will return the matrix of refined vanishing directions.
	cv::Mat refineCalibration(const LineClusters &lineClusters, cv::Mat &K, const float inlierThreshold);

	// Refine given calibration matrix and vanishing directions of input clusters. The vanishing directions of input
	// cluster must be perpendicular each other.
	// It will return the matrix of refined vanishing directions.
	cv::Mat refineCalibration(const LineClusters &lineClusters, const cv::Mat &VD, cv::Mat &K, const float inlierThreshold);

	// Batch-refinement of given calibration matrix K and vanishing directions of input clusters, VDs.
	// It will return the vector of matrices, where each column represents the vanishing direction.
	std::vector<cv::Mat> refineCalibration(const std::vector<LineClusters> &lineClusters,
		const std::vector<cv::Mat> &VDs, cv::Mat &K, const float inlierThreshold);

	// Batch-refinement of given calibration matrix K and vanishing directions of input clusters, VDs.
	// It will return the vector of matrices, where each column represents the vanishing direction.
	std::vector<cv::Mat> refineCalibration2(const std::vector<LineClusters> &lineClusters,
		const std::vector<cv::Mat> &VDs, const cv::Mat &K, const float inlierThreshold);

	//-----------------------------
	// Utility Functions
	//-----------------------------

	// Draw the vanishing directions on given image.
	cv::Mat drawAxis(const cv::Mat &image, cv::Mat& VD, const cv::Mat& K);

	// Orthogonalize the given matrix of vanishing directions
	void Orthogonalize(const LineClusters &lineClusters, cv::Mat &VD);

	//-----------------------------
	// Internal Functions
	//-----------------------------


};

