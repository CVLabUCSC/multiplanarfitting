#include "LineClustering.h"
#include "TLinkage\SamplerInterface.h"

#include <vector>
#include <map>

using namespace std;
using namespace cv;

Mat g_debugImage;

//-------------------------------------
// Inline utility functions
//-------------------------------------

inline vector<float> computeIntersection(const CDataType &line1, const CDataType &line2) {

	vector<float> intersectPoint(3, 0.0f);

	float xs0 = line1[0];
	float ys0 = line1[1];
	float xe0 = line1[2];
	float ye0 = line1[3];
	float xs1 = line2[0];
	float ys1 = line2[1];
	float xe1 = line2[2];
	float ye1 = line2[3];

	float l0[3], l1[3], v[3];
	vec_cross(xs0, ys0, 1,
		xe0, ye0, 1,
		l0[0], l0[1], l0[2]);
	vec_cross(xs1, ys1, 1,
		xe1, ye1, 1,
		l1[0], l1[1], l1[2]);
	vec_cross(l0[0], l0[1], l0[2],
		l1[0], l1[1], l1[2],
		v[0], v[1], v[2]);
	vec_norm(v[0], v[1], v[2]);

	intersectPoint[0] = v[0];
	intersectPoint[1] = v[1];
	intersectPoint[2] = v[2];

	return intersectPoint;
}

inline double LineDistance(const Mat &model, const CDataType &line) {
	float l[3];
	float midPoint[3] = { (line[0] + line[2]) / 2.0, (line[1] + line[3]) / 2.0, 1 };

	vec_cross(midPoint[0], midPoint[1], midPoint[2],
		model.at<float>(0), model.at<float>(1), model.at<float>(2),
		l[0], l[1], l[2]);

	return fabs(l[0] * line[0] + l[1] * line[1] + l[2]) / sqrt(l[0] * l[0] + l[1] * l[1]);
}

inline double LineDistance(const CDataType &model, const CDataType &line) {
	float l[3];
	float midPoint[3] = { (line[0] + line[2]) / 2.0, (line[1] + line[3]) / 2.0, 1 };

	vec_cross(midPoint[0], midPoint[1], midPoint[2],
		model[0], model[1], model[2],
		l[0], l[1], l[2]);

	return fabs(l[0] * line[0] + l[1] * line[1] + l[2]) / sqrt(l[0] * l[0] + l[1] * l[1]);
}

inline double LineDistance(const Point2f &model, const CDataType &line) {
	float l[3];
	float midPoint[3] = { (line[0] + line[2]) / 2.0, (line[1] + line[3]) / 2.0, 1 };

	vec_cross(midPoint[0], midPoint[1], midPoint[2],
		model.x, model.y, 1.f,
		l[0], l[1], l[2]);

	return fabs(l[0] * line[0] + l[1] * line[1] + l[2]) / sqrt(l[0] * l[0] + l[1] * l[1]);
}

inline double LineDistance(const Point2f &model, const line_descriptor::KeyLine &line) {
	float l[3];
	float midPoint[3] = { (line.startPointX + line.endPointX) / 2.0, (line.startPointY + line.endPointY) / 2.0, 1 };

	vec_cross(midPoint[0], midPoint[1], midPoint[2],
		model.x, model.y, 1.f,
		l[0], l[1], l[2]);

	return fabs(l[0] * line.startPointX + l[1] * line.startPointY + l[2]) / sqrt(l[0] * l[0] + l[1] * l[1]);
}

inline double LineDistance(const Point2f &point, const float *line) {
	float absquare = sqrt(line[0] * line[0] + line[1] * line[1]);
	return fabs(line[0] * point.x + line[1] * point.y + line[2]) / absquare;
}

inline bool compLinesX(const Line& line1, const Line& line2) {
	return line1.getMidPoint().x < line2.getMidPoint().x;
}

inline bool compLinesY(const Line& line1, const Line& line2) {
	return line1.getMidPoint().y < line2.getMidPoint().y;
}

inline bool compXCoordinates(const Line &line, const float &val) {
	return line.getMidPoint().x < val;
}

inline bool compYCoordinates(const Line &line, const float &val) {
	return line.getMidPoint().y < val;
}

//-------------------------------------
// Supportive utility functions
//-------------------------------------

void convertClustersType(const list<sClLnk *> &clustersList,
	LineClusters &lineClusters, int minCardinarity) {
	int clusterIndex = 0;

	lineClusters.clear();

	for (list<sClLnk *>::const_iterator citer = clustersList.cbegin(); citer != clustersList.cend(); citer++)
	{
		if ((*citer)->mBelongingPts.size() < minCardinarity) {
			continue;
		}

		vector<Line> lines;

		for (list<sPtLnk *>::const_iterator piter = (*citer)->mBelongingPts.cbegin();
			piter != (*citer)->mBelongingPts.cend(); piter++) {
			Line l(Point2f((*piter)->mData[0], (*piter)->mData[1]), Point2f((*piter)->mData[2], (*piter)->mData[3]));
			lines.push_back(l);
		}

		lineClusters.addLineCluster(LineCluster(lines));

		clusterIndex++;
	}
}

void computeInlierLines(const std::list<sClLnk *> &clustersList, LineClusters &lineClusters,
	int minCardinarity, float threshold, bool allowDuplicates) {
	vector<Line> allLines;
	vector<Point2f> models;
	int numOfClusters = 0;

	lineClusters.clear();

	for (list<sClLnk *>::const_iterator citer = clustersList.cbegin(); citer != clustersList.cend(); citer++)
	{
		if ((*citer)->mBelongingPts.size() < minCardinarity) {
			continue;
		}

		vector<Line> lines;

		for (list<sPtLnk *>::const_iterator piter = (*citer)->mBelongingPts.cbegin();
			piter != (*citer)->mBelongingPts.cend(); piter++) {
			Line l(Point2f((*piter)->mData[0], (*piter)->mData[1]), Point2f((*piter)->mData[2], (*piter)->mData[3]));
			lines.push_back(l);
		}
		
		// compute a vanishing point of current cluster
		models.push_back(LineCluster::computeVanishingPoint(lines));

		// add current lines to allLines
		allLines.insert(allLines.end(), lines.begin(), lines.end());

		numOfClusters++;
	}
	
	// Recompute inliers for each model
	if (allowDuplicates) {
		for (int ci = 0; ci < numOfClusters; ci++) {
			vector<Line> lines;

			for (int li = 0; li < (int)allLines.size(); li++) {
				float dist = Line::LineDistance(models[ci], allLines[li]);

				if (dist <= threshold) {
					lines.push_back(allLines[li]);
				}
			}

			if (lines.size() >= minCardinarity) {
				lineClusters.addLineCluster(LineCluster(lines));
			}
		}
	}
	else {
		vector<float> minimumDistance(allLines.size(), FLT_MAX);
		vector<int> indices(allLines.size(), -1);
		vector<vector<Line>> tempClusters(numOfClusters);

		for (int ci = 0; ci < numOfClusters; ci++) {
			vector<Line> lines;

			for (int li = 0; li < (int)allLines.size(); li++) {
				float dist = Line::LineDistance(models[ci], allLines[li]);

				if (dist <= minimumDistance[li]) {
					minimumDistance[li] = dist;
					indices[li] = ci;
				}
			}
		}

		for (int li = 0; li < (int)allLines.size(); li++) {
			tempClusters[indices[li]].push_back(allLines[li]);
		}

		for (int ci = 0; ci < numOfClusters; ci++) {
			if (tempClusters.size() >= minCardinarity) {
				lineClusters.addLineCluster(LineCluster(tempClusters[ci]));
			}
		}
	}
}

void computeInlierLines(const std::list<sClLnk *> &clustersList, vector<CDataType*> alllines,
	LineClusters &lineClusters, int minCardinarity, float threshold) {
	vector<Point2f> models;
	vector<int> numOfLines;
	vector<int> indices;
	vector<bool> isTaken(alllines.size(), false);

	for (list<sClLnk *>::const_iterator citer = clustersList.cbegin(); citer != clustersList.cend(); citer++)
	{
		if ((*citer)->mBelongingPts.size() < minCardinarity) {
			continue;
		}

		vector<Line> lines;

		for (list<sPtLnk *>::const_iterator piter = (*citer)->mBelongingPts.cbegin();
			piter != (*citer)->mBelongingPts.cend(); piter++) {
			Line l(Point2f((*piter)->mData[0], (*piter)->mData[1]), Point2f((*piter)->mData[2], (*piter)->mData[3]));
			lines.push_back(l);
		}

		// compute a vanishing point of current cluster
		models.push_back(LineCluster::computeVanishingPoint(lines));
		numOfLines.push_back((int)lines.size());
	}

	sortIdx(numOfLines, indices, SORT_DESCENDING);

	for (int i = 0; i < (int)indices.size(); i++) {
		vector<Line> lines;
		vector<int> takenIndices;

		for (int li = 0; li < (int)alllines.size(); li++) {
			float dist = LineDistance(models[indices[i]], *alllines[li]);

			if (dist <= threshold && !isTaken[li]) {
				lines.push_back(Line(Point2f((*alllines[li])[0], (*alllines[li])[1]),
					Point2f((*alllines[li])[2], (*alllines[li])[3])));
				isTaken[li] = true;
				takenIndices.push_back(li);
			}
		}

		if (lines.size() >= minCardinarity) {
			lineClusters.addLineCluster(LineCluster(lines));
		}
		else {
			for (int ind = 0; ind < (int)takenIndices.size(); ind++) {
				isTaken[takenIndices[ind]] = false;
			}
		}
	}

	// Add remaining lines
	for (int li = 0; li < (int)alllines.size(); li++) {
		if (!isTaken[li]) {
			lineClusters.addLine(Line(Point2f((*alllines[li])[0], (*alllines[li])[1]),
				Point2f((*alllines[li])[2], (*alllines[li])[3])));
		}
	}
}

LineClusters generateClusters(const vector<CDataType*> &lines, const vector<Point2f> &VPs, const float threshold) {
	const int numOfModels = (int)VPs.size();
	const int numOfLines = (int)lines.size();
	vector<vector<Line>> lineClusters(numOfModels);
	vector<vector<double>> errors(numOfModels, vector<double>(numOfLines));
	vector<int> modelIndex(numOfLines, -1);

	for (int li = 0; li < numOfLines; li++) {
		double minError = threshold;

		for (int mi = 0; mi < numOfModels; mi++) {
			double error = LineDistance(VPs[mi], *lines[li]);

			if (error < minError) {
				minError = error;
				modelIndex[li] = mi;
			}
		}
	}

	for (int li = 0; li < numOfLines; li++) {
		if (modelIndex[li] >= 0) {
			const PointPair *pointPair = dynamic_cast<PointPair*>(lines[li]);

			lineClusters[modelIndex[li]].push_back(Line(pointPair->Point1, pointPair->Point2));
		}
	}

	vector<LineCluster> vecLineCluster;
	for (int ci = 0; ci < numOfModels; ci++) {
		if (lineClusters[ci].size() < 2) {
			continue;
		}

		vecLineCluster.push_back(LineCluster(lineClusters[ci]));
	}

	return LineClusters(vecLineCluster);
}

LineClusters generateClusters(const std::vector<CDataType*> &lines,
	const Mat &VD, const Mat &K, const float threshold) {
	assert(VD.cols > 0 && VD.rows == 3 && K.cols == 3 && K.rows == 3);

	vector<Point2f> VPs;

	for (int i = 0; i < VD.cols; i++) {
		Mat point = K * VD.col(i);

		VPs.push_back(Point2f(point.at<float>(0) / point.at<float>(2), point.at<float>(1) / point.at<float>(2)));
	}

	return generateClusters(lines, VPs, threshold);
}

vector<int> selectClusters(const LineClusters &lineClusters, const Mat &VDs, const Mat &K) {
	vector<int> selected(VDs.cols, -1);
	const Mat Kinv = K.inv();

	for (int vi = 0; vi < VDs.cols; vi++) {
		double maxCosine = 0.0;

		for (int ci = 0; ci < (int)lineClusters.size(); ci++) {
			Point2f vp = lineClusters[ci].getVanishingPoint();
			Mat vd = Kinv * (Mat_<float>(3, 1) << vp.x, vp.y, 1.f);

			vd = vd / norm(vd);
			
			double cosine = abs(VDs.col(vi).dot(vd));

			if (cosine > maxCosine) {
				maxCosine = cosine;
				selected[vi] = ci;
			}
		}
	}

	return selected;
}

//-------------------------------------
// Drawing functions
//-------------------------------------

Mat drawLines(const Mat &image, vector<CDataType*> &lines, int lineWidth)
{
	Mat output;

	if (image.channels() == 3) {
		cvtColor(image, output, CV_BGR2GRAY);
		cvtColor(output, output, CV_GRAY2BGR);
	}
	else {
		cvtColor(image, output, CV_GRAY2BGR);
	}
	
	for (CDataType *l : lines) {
		line(output, Point2f((*l)[0], (*l)[1]), Point2f((*l)[2], (*l)[3]), CV_RGB(255, 0, 0), lineWidth);
	}

	return output;
}

Mat drawLineCluster(const Mat &image, const LineCluster &lineCluster, const int lineWidth, const Scalar &color) {
	Mat output = image.clone();

	for (const Line l : lineCluster.Lines) {
		circle(output, l.StartPoint, 2, CV_RGB(255, 255, 255));
		circle(output, l.EndPoint, 2, CV_RGB(255, 255, 255));
		line(output, l.StartPoint, l.EndPoint, color, lineWidth);
	}

	return output;
}

Mat drawLineClusters(const Mat &image, const vector<LineCluster> &lineClusters, int lineWidth)
{
	Mat output;
	RNG rng(0xFFFFFFFF);

	if (image.channels() == 3) {
		cvtColor(image, output, CV_BGR2GRAY);
		cvtColor(output, output, CV_GRAY2BGR);
	}
	else {
		cvtColor(image, output, CV_GRAY2BGR);
	}

	for (LineCluster cluster : lineClusters) {
		int icolor = (unsigned)rng;
		Scalar color(icolor & 255, (icolor >> 8) & 255, (icolor >> 16) & 255);

		for (Line l : cluster.Lines) {
			circle(output, l.StartPoint, 2, CV_RGB(255, 255, 255));
			circle(output, l.EndPoint, 2, CV_RGB(255, 255, 255));
			line(output, l.StartPoint, l.EndPoint, color, lineWidth);
		}
	}

	return output;
}

Mat drawLineClusters(const Mat &image, const LineClusters &lineClusters, const int lineWidth, const bool convertGray, const vector<int> &indices)
{
	Mat output;
	RNG rng(0xFFFFFFFF);

	if (convertGray) {
		if (image.channels() == 3) {
			cvtColor(image, output, CV_BGR2GRAY);
			cvtColor(output, output, CV_GRAY2BGR);
		}
		else if (image.channels() == 1) {
			cvtColor(image, output, CV_GRAY2BGR);
		}
	}
	else {
		output = image.clone();
	}

	if (indices.size() == 0) {
		for (LineCluster cluster : lineClusters.Clusters) {
			int icolor = (unsigned)rng;
			Scalar color(icolor & 255, (icolor >> 8) & 255, (icolor >> 16) & 255);

			for (Line l : cluster.Lines) {
				circle(output, l.StartPoint, 2, CV_RGB(255, 255, 255));
				circle(output, l.EndPoint, 2, CV_RGB(255, 255, 255));
				line(output, l.StartPoint, l.EndPoint, color, lineWidth);
			}
		}
	}
	else {
		for (int i = 0; i < (int)indices.size(); i++) {
			int icolor = (unsigned)rng;
			Scalar color(icolor & 255, (icolor >> 8) & 255, (icolor >> 16) & 255);

			if (indices[i] >= 0 && indices[i] < lineClusters.size()) {
				for (Line l : lineClusters[indices[i]].Lines) {
					circle(output, l.StartPoint, 2, CV_RGB(255, 255, 255));
					circle(output, l.EndPoint, 2, CV_RGB(255, 255, 255));
					line(output, l.StartPoint, l.EndPoint, color, lineWidth);
				}
			}
		}
	}

	return output;
}

Mat drawLineClustersL(const Mat &image, const LineClusters &lineClusters, int lineWidth, const std::vector<int> &indices)
{
	Mat output;
	Mat legend = Mat::zeros(image.rows, 140, CV_8UC3);
	RNG rng(0xFFFFFFFF);
	int idx = 0;

	if (image.channels() == 3) {
		cvtColor(image, output, CV_BGR2GRAY);
		cvtColor(output, output, CV_GRAY2BGR);
	}
	else {
		cvtColor(image, output, CV_GRAY2BGR);
	}

	if (indices.size() == 0) {
		for (LineCluster cluster : lineClusters.Clusters) {
			int icolor = (unsigned)rng;
			Scalar color(icolor & 255, (icolor >> 8) & 255, (icolor >> 16) & 255);

			for (Line l : cluster.Lines) {
				circle(output, l.StartPoint, 2, CV_RGB(255, 255, 255));
				circle(output, l.EndPoint, 2, CV_RGB(255, 255, 255));
				line(output, l.StartPoint, l.EndPoint, color, lineWidth);
			}

			circle(legend, Point(10, 5 + idx * 10), 2, color, 2);
			std::ostringstream text;
			text << "cluster " << std::setfill('0') << std::setw(2) << idx;
			putText(legend, text.str(), Point(20, 10 + idx * 10), cv::FONT_HERSHEY_PLAIN, 1, CV_RGB(255, 255, 255));
			idx++;
		}
	}
	else {
		for (int i = 0; i < (int)indices.size(); i++) {
			int icolor = (unsigned)rng;
			Scalar color(icolor & 255, (icolor >> 8) & 255, (icolor >> 16) & 255);

			if (indices[i] >= 0 && indices[i] < lineClusters.size()) {
				for (Line l : lineClusters[indices[i]].Lines) {
					circle(output, l.StartPoint, 2, CV_RGB(255, 255, 255));
					circle(output, l.EndPoint, 2, CV_RGB(255, 255, 255));
					line(output, l.StartPoint, l.EndPoint, color, lineWidth);
				}
			}

			circle(legend, Point(10, 5 + idx++ * 10), 2, color, 2);
			std::ostringstream text;
			text << "cluster " << std::setfill('0') << std::setw(2) << idx;
			putText(legend, text.str(), Point(20, 10 + idx * 10), cv::FONT_HERSHEY_PLAIN, 1, CV_RGB(255, 255, 255));
			idx++;
		}
	}

	Mat combined(output.rows, output.cols + legend.cols, output.type());
	combined.adjustROI(0, 0, 0, -legend.cols);
	output.copyTo(combined);
	combined.adjustROI(0, 0, -output.cols, legend.cols);
	legend.copyTo(combined);
	combined.adjustROI(0, 0, output.cols, 0);
	output = combined;

	return output;
}

// Draw lines in cluster with colors corresponding to given values
Mat drawLineCluster(const Mat &image, const LineCluster &lineCluster, const vector<float> &values) {
	vector<int> sortedIndices;
	int blue, red;
	Mat outputImage = image.clone();
	int centerIndex = 0;

	sortIdx(values, sortedIndices, SORT_ASCENDING);

	// Assumed that zero value does not exist
	for (unsigned int i = 1; i < sortedIndices.size(); i++) {
		if (values[sortedIndices[i - 1]] * values[sortedIndices[i]] < 0) {
			centerIndex = i;
		}
	}

	// draw lines of negative values
	if (centerIndex != 0) {
		for (int i = 0; i < centerIndex; i++) {
			blue = (int)(255.f * (1.f - ((float)i / (float)centerIndex)));
			line(outputImage, lineCluster[sortedIndices[i]].StartPoint, lineCluster[sortedIndices[i]].EndPoint, CV_RGB(0, 0, blue), 2);
		}

		for (int i = centerIndex; i < sortedIndices.size(); i++) {
			red = (int)(255.f * ((float)(i - centerIndex) / (float)(sortedIndices.size() - centerIndex)));
			line(outputImage, lineCluster[sortedIndices[i]].StartPoint, lineCluster[sortedIndices[i]].EndPoint, CV_RGB(red, 0, 0), 2);
		}
	}
	else {

	}

	return outputImage;
}

//-------------------------------------
// Line functions
//-------------------------------------

vector<CDataType*> detectLines(const Mat &image, float lineLengthThreshold)
{
	vector<CDataType*> outputLines;
	vector<cv::line_descriptor::KeyLine> lines;
	Ptr<line_descriptor::LSDDetector> lsddetector = line_descriptor::LSDDetector::createLSDDetector();
	Mat grayImage;
	Mat output = image.clone();

	cvtColor(image, grayImage, COLOR_BGR2GRAY);
	
	lsddetector->detect(grayImage, lines, 2, 1, generateEmptySpaceMask(grayImage));

	// Remove a line if its length is smaller than given threshold
	for (vector<cv::line_descriptor::KeyLine>::iterator iter = lines.begin(); iter != lines.end();) {
		if (iter->lineLength < lineLengthThreshold) {
			if (false) {
				if (iter->lineLength > lineLengthThreshold / 2) {
					Point2f centerPoint((iter->startPointX + iter->endPointX) / 2.f,
						(iter->startPointY + iter->endPointY) / 2.f);

					iter->startPointX = iter->startPointX + (iter->startPointX - iter->endPointX) / iter->lineLength * (lineLengthThreshold / 2.f);
					iter->endPointX = iter->endPointX + (iter->endPointX - iter->startPointX) / iter->lineLength * (lineLengthThreshold / 2.f);

					iter->startPointY = iter->startPointY + (iter->startPointY - iter->endPointY) / iter->lineLength * (lineLengthThreshold / 2.f);
					iter->endPointY = iter->endPointY + (iter->endPointY - iter->startPointY) / iter->lineLength * (lineLengthThreshold / 2.f);

					iter++;
				}
				else {
					iter = lines.erase(iter);
				}
			}
			else {
				iter = lines.erase(iter);
			}
		}
		else {
			iter++;
		}
	}

	mergeLines(lines, 30.f);

	for (cv::line_descriptor::KeyLine l : lines) {
		outputLines.push_back(new PointPair(l.getStartPoint(), l.getEndPoint()));
	}

	return outputLines;
}

Mat generateEmptySpaceMask(const Mat &grayImage, int kernelSize) {
	Mat mask = grayImage * 255;
	Mat element = getStructuringElement(MORPH_ELLIPSE,
		Size(2 * kernelSize + 1, 2 * kernelSize + 1),
		Point(kernelSize, kernelSize));

	/// Apply the erosion operation
	erode(mask, mask, element);

	return mask;
}

void mergeLines(vector<cv::line_descriptor::KeyLine> &lines, float lineGapThreshold, float lineDistThreshold) {
	bool updated = true;
	vector<bool> merged(lines.size(), false);   // which line will be deleted.

	while (updated) {
		updated = false;

		for (unsigned int ind1 = 0; ind1 < lines.size() - 1; ind1++) {
			if (merged[ind1]) {
				continue;
			}

			for (unsigned int ind2 = ind1 + 1; ind2 < lines.size(); ind2++) {
				if (merged[ind2]) {
					continue;
				}

				cv::line_descriptor::KeyLine newLine;
				float lineGap = mergeLines(lines[ind1], lines[ind2], newLine);

				// If gap between two lines is too large, do not merge the lines.
				if (lineGap > lineGapThreshold) {
					continue;
				}

				// Compute line coefficient of merged line
				float newLineCoefficient[3];
				vec_cross(newLine.startPointX, newLine.startPointY, 1.f,
					newLine.endPointX, newLine.endPointY, 1.f,
					newLineCoefficient[0], newLineCoefficient[1], newLineCoefficient[2]);

				float dist = (LineDistance(lines[ind1].getStartPoint(), newLineCoefficient) +
					LineDistance(lines[ind1].getEndPoint(), newLineCoefficient) +
					LineDistance(lines[ind2].getStartPoint(), newLineCoefficient) +
					LineDistance(lines[ind2].getEndPoint(), newLineCoefficient)) * 0.25f;

				if (dist < lineDistThreshold) {
					lines[ind1] = newLine;
					merged[ind2] = true;
				}
			}
		}
	}

	int lineIndex = 0;
	for (vector<cv::line_descriptor::KeyLine>::iterator iter = lines.begin(); iter != lines.end(); lineIndex++) {
		if (merged[lineIndex]) {
			iter = lines.erase(iter);
		}
		else {
			iter++;
		}
	}
}

float mergeLines(const cv::line_descriptor::KeyLine &line1,
	const cv::line_descriptor::KeyLine &line2, cv::line_descriptor::KeyLine &newLine) {
	// Compute length of lines from the origin
	float len1 = sqrt(line1.pt.x * line1.pt.x + line1.pt.y * line1.pt.y);
	float len2 = sqrt(line2.pt.x * line2.pt.x + line2.pt.y * line2.pt.y);

	float w1 = len1 / (len1 + len2);
	float w2 = len2 / (len1 + len2);

	Point2f midPoint;
	midPoint.x = (w1*(line1.startPointX + line1.endPointX) + w2*(line2.startPointX + line2.endPointX)) * 0.5;
	midPoint.y = (w1*(line1.startPointY + line1.endPointY) + w2*(line2.startPointY + line2.endPointY)) * 0.5;

	// Define the orientation of the merged line as the weighted sum of the orientations of the given segments
	// adjust line directions
	float radian;
	if (line1.pt.x * line2.pt.x + line1.pt.y * line2.pt.y < 0) {
		radian = w1 * atan2(line1.pt.y, line1.pt.x) + w2 * atan2(-line2.pt.y, -line2.pt.x);
	}
	else {
		radian = w1 * atan2(line1.pt.y, line1.pt.x) + w2 * atan2(line2.pt.y, line2.pt.x);
	}
	Point2f lineDir(cos(radian), sin(radian));

	vector<float> p;
	p.push_back((line1.startPointX - midPoint.x) * lineDir.x + (line1.startPointY - midPoint.y) * lineDir.y);
	p.push_back((line1.endPointX - midPoint.x) * lineDir.x + (line1.endPointY - midPoint.y) * lineDir.y);
	p.push_back((line2.startPointX - midPoint.x) * lineDir.x + (line2.startPointY - midPoint.y) * lineDir.y);
	p.push_back((line2.endPointX - midPoint.x) * lineDir.x + (line2.endPointY - midPoint.y) * lineDir.y);

	vector<int> idx;
	sortIdx(p, idx, SORT_ASCENDING);
	vector<Point2f> points = { line1.getStartPoint(), line1.getEndPoint(), line2.getStartPoint(), line2.getEndPoint() };

	newLine.startPointX = points[idx[0]].x;
	newLine.startPointY = points[idx[0]].y;
	newLine.endPointX = points[idx[3]].x;
	newLine.endPointY = points[idx[3]].y;

	return (p[idx[3]] - p[idx[0]]) - (fabs(p[0] - p[1]) + fabs(p[2] - p[3]));
}

//-------------------------------------
// Line Clustering Functions
//-------------------------------------

vector<Mat> generateVPModels(vector<CDataType*> &lines, unsigned int numOfModels) {
	vector<Mat> VPmodels;

	RandomSampler randomSampler(4, 2, &LineDistance);
	vector<vector<unsigned int>> indices;

	initRandomSampler(randomSampler, lines, lines.size());

	randomSampler.SetNFSamplingTypeExp(1.0);

	randomSampler.GetNSample(indices, numOfModels);

	// Generate models
	for (int i = 0; i < (int)indices.size(); i++) {
		vector<float> modelParams = computeIntersection(*lines[indices[i][0]], *lines[indices[i][1]]);
		VPmodels.push_back((Mat_<float>(3, 1) << modelParams[0], modelParams[1], modelParams[2]));
	}
	
	return VPmodels;
}

//-------------------------------------
// Main Entry
//-------------------------------------

// Test line clustering using T-linkage with given image
LineClusters LineClustering(Mat image, int minCardinarity, float threshold, float lineLengthThreshold,
	bool allowDuplicates) {
	int mKDTreeRange = 3;
	unsigned int nPtDimension = 4;
	vector<CDataType*> lines;
	vector<Mat> VPmodels;
	LineClusters lineClusters;
	Mat output;
	double t = (double)getTickCount();
	
	cout << "Detect lines..." << endl;
	lines = detectLines(image, lineLengthThreshold);
	imwrite("results\\detected_lines.jpg", drawLines(image, lines, 2));

	output = drawLines(image, lines, 2);
	imwrite("results\\lines_merged.jpg", drawLines(image, lines, 2));

	cout << "Generate random models of vanishing points..." << endl;
	VPmodels = generateVPModels(lines, 500);

	TLinkage<Mat> tlinkage(threshold, &LineDistance, VPmodels.size(), true, nPtDimension, mKDTreeRange);

	cout << "Initialize T-linkage..." << endl;
	for (unsigned int i = 0; i < lines.size(); i++) {
		tlinkage.AddPoint(*lines[i]);
	}

	for (unsigned int i = 0; i < VPmodels.size(); i++) {
		tlinkage.AddModel(VPmodels[i]);
	}

	cout << "Data clustering with T-linkage..." << endl;
	list<sClLnk *> clustersList = tlinkage.DoTLClusterization();

	// TODO: move the following function from 'PointPairClustering' to somewhere else
	//convertClustersType(clustersList, lineClusters, minCardinarity);
	//computeInlierLines(clustersList, lineClusters, minCardinarity, threshold, allowDuplicates);
	computeInlierLines(clustersList, lines, lineClusters, minCardinarity, threshold);

	// Sort clusters by their cardinality
	lineClusters.sort();

	imwrite("results\\lineCluster.png", drawLineClusters(image.clone(), lineClusters, 2, true));

	// memory clean-up
	cout << "Clean-up memory used by T-linkage..." << endl;
	for (unsigned int i = 0; i < lines.size(); i++) {
		delete lines[i];
	}

	for (list<sClLnk *>::iterator iter = clustersList.begin(); iter != clustersList.end(); iter++) {
		delete *iter;
	}

	t = ((double)getTickCount() - t) / getTickFrequency();
	cout << "*** Line clustering took " << t << " seconds: " << endl;

	return lineClusters;
}

LineClusters LineClustering(const Mat &image, const Mat &VD, const Mat &K,
	const float threshold, const float lineLengthThreshold, const bool allowDuplicates) {
	vector<CDataType*> lines = detectLines(image, lineLengthThreshold);

	return generateClusters(lines, VD, K, threshold);
}
