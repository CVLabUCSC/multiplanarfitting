#pragma once

#include <opencv2\opencv.hpp>
#include <ceres\cost_function.h>

struct EssentialResidual {
	/**
	 The basic constructor of this structure. Point pair must be calibrated.
	 */
	EssentialResidual(const std::pair<cv::Point3d, cv::Point3d> &pointPair);

	/**
	 @params x Input array representing 3-dimensional translation vector.
	 @params residuals Output residual containing a scalar value.
	 */
	template <typename T>
	bool operator()(const T* const x, T* residuals) const;

	static ceres::CostFunction* Create(const std::pair<cv::Point3d, cv::Point3d> &pointPair);

	/**
	 Calibrated point of the first image point.
	 */
	cv::Mat Point1;

	/**
	 Calibrated point of the second image point.
	 */
	cv::Mat Point2_t;

	/**
	 The rotation matrix.
	 */
	static cv::Mat R;
};