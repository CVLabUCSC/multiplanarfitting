#include <boost\format.hpp>
#include <boost\filesystem.hpp>
#include <boost\tokenizer.hpp>
#include <iomanip>
#include <fstream>
#include <ctime>

#include "main.h"
#include "PointPairClustering.h"
#include "VPDetector.h"
#include "Homography.h"
#include "TLinkage\SamplerInterface.h"
#include "Utilities.h"
#include "RegionSegmentation.h"

#include "MultiPlanarFitting.h"
#include "PointPairClusters.h"

#include "VisualOdometry.h"

using namespace std;
using namespace cv;

// Fixed folders
static const string OutputFolder = "results\\";

using cv::line_descriptor::KeyLine;

DataFiles::DataFiles(const DataInfo& dataInfo, int numZeros) {
	// Build filenames
	DataFolder = dataInfo.dataFoldername + dataInfo.datasetName + "\\";

	Image1filename = buildFilename(DataFolder, dataInfo.filePrefix, '0', numZeros,
		dataInfo.fileIndex, "", dataInfo.imageExtension);

	Image1GTfilename = buildFilename(DataFolder, dataInfo.filePrefix, '0', numZeros,
		dataInfo.fileIndex, GTSufix, dataInfo.imageExtension);

	Image1VDfilename = buildFilename(DataFolder, dataInfo.filePrefix, '0', numZeros,
		dataInfo.fileIndex, VDSufix, dataInfo.dataExtension);

	Image1OMfilename = buildFilename(DataFolder, dataInfo.filePrefix, '0', numZeros,
		dataInfo.fileIndex, OMSufix, dataInfo.imageExtension);

	CalibrationFilename = DataFolder + "K.txt";
}

//----------------------------
// Utilities for experiments
//----------------------------

inline string buildFilename(string foldername, string filePrefix, char emptyCharacter,
	int emptylength, int fileIndex, string fileSufix, string fileExtension) {
	stringstream stringBuilder;
	stringBuilder << foldername << filePrefix << setfill(emptyCharacter) << setw(emptylength)
		<< fileIndex << fileSufix << fileExtension;
	return stringBuilder.str();
}

inline string buildOutputFolder(string dataname, int refFileIndex, int tarFileIndex) {
	stringstream stringBuilder;
	stringBuilder << OutputFolder << dataname << "_" << setfill('0') << setw(3) << refFileIndex << "-"
		<< setfill('0') << setw(3) << tarFileIndex << "\\";

	string builtPath = stringBuilder.str();
	boost::filesystem::path outputPath(builtPath);

	if (!boost::filesystem::exists(outputPath)) {
		assert(boost::filesystem::create_directory(outputPath));
	}

	return builtPath;
}

inline string buildTestName(std::string dataname, int refFileIndex, int tarFileIndex) {
	stringstream stringBuilder;
	stringBuilder << dataname << "_" << setfill('0') << setw(3) << refFileIndex << "-"
		<< setfill('0') << setw(3) << tarFileIndex;

	return stringBuilder.str();
}

inline string buildLogFilename() {
	stringstream stringBuilder;
	time_t t = time(0);
	tm now = *localtime(&t);

	stringBuilder << OutputFolder << now.tm_year + 1900 << setw(2) << setfill('0') << now.tm_mon + 1
		<< setw(2) << setfill('0') << now.tm_mday
		<< setw(2) << setfill('0') << now.tm_hour
		<< setw(2) << setfill('0') << now.tm_min
		<< setw(2) << setfill('0') << now.tm_sec << ".txt";

	return stringBuilder.str();
}

vector<DataInfo> loadSetupFile(std::string setupFilename) {
	vector<DataInfo> dataInfo;
	ifstream fin(setupFilename);

	if (!fin.is_open()) {
		cout << "Failed to open the setup file: " << setupFilename << endl;
		system("pause");
		return dataInfo;
	}

	for (string input; std::getline(fin, input);) {
		size_t firstIndex = input.find_first_not_of(' ');
		char firstChar = input[firstIndex];
		if (firstChar == '#')
			continue;

		if (firstIndex != 0)
			input = input.substr(firstIndex, input.size());

		stringstream strstr(input);
		DataInfo info;

		strstr >> info.dataFoldername
			>> info.datasetName
			>> info.filePrefix
			>> info.imageExtension
			>> info.dataExtension
			>> info.fileIndex
			>> info.secImgIdxBeg
			>> info.numOfImg
			>> info.imgIdxStep;

		dataInfo.push_back(info);
	}

	fin.close();

	return dataInfo;
}

//----------------------------
// Experiment Functions
//----------------------------

void DrawRectangleMouseCallback(int event, int x, int y, int, void *data) {
	CallbackData *callbackData = (CallbackData *)data;

	// Action when left button is clicked
	if (event == CV_EVENT_LBUTTONDOWN) {
		if (!callbackData->start_draw) {
			callbackData->roi_x0 = x;
			callbackData->roi_y0 = y;
			callbackData->start_draw = true;
		}
	}

	// Action when mouse is moving
	if ((event == CV_EVENT_MOUSEMOVE) && callbackData->start_draw) {
		// Redraw bounding box for annotation
		Mat current_view;
		callbackData->image.copyTo(current_view);
		rectangle(current_view, Point(callbackData->roi_x0, callbackData->roi_y0), Point(x, y), Scalar(0, 255, 0));
		imshow(callbackData->window_name, current_view);
	}

	if ((event == CV_EVENT_LBUTTONUP) && callbackData->start_draw) {
		callbackData->roi_x1 = x;
		callbackData->roi_y1 = y;
		callbackData->start_draw = false;

		callbackData->need_update = true;
	}
}

bool vanishingPointDetection(const Mat &image, Mat K) {
	String windowName = "Vanishing Point Detection";
	Mat windowImage = image.clone();
	float threshold = 2.f;
	float lineLengthThreshold = 60.f;
	LineClusters lineClusters = LineClustering(image, 2, threshold, lineLengthThreshold, false);
	vector<int> indices;
	int inputKey;

	//imwrite("output.jpg", drawLineClusters(windowImage, lineClusters, 1));

	Mat VD = VPDetector::detectVP(lineClusters, indices, K, threshold);

	namedWindow(windowName, WINDOW_AUTOSIZE);
	windowImage = drawLineClusters(windowImage, lineClusters.subCluster(indices, 0.f), 1);
	//windowImage = drawLineCluster(windowImage, lineClusters[indices[0]], 2, CV_RGB(255, 0, 0));
	//windowImage = VPDetector::drawAxis(windowImage, VD, K);
	imwrite("results\\clustered_lines.jpg", windowImage);
	imshow(windowName, windowImage);
	inputKey = waitKey();

	return inputKey != 27;
}

bool VPDetectionPair(const Mat &image1, const Mat &image2, Mat K) {
	String windowName = "Vanishing Point Detection";
	vector<LineClusters> lineClusters(2);
	float threshold = 2.f;
	float lineLengthThreshold = 40.f;
	vector<vector<int>> indices;
	Mat windowImage;
	int inputKey;

	lineClusters[0] = LineClustering(image1, 2, threshold, lineLengthThreshold, false);
	lineClusters[1] = LineClustering(image2, 2, threshold, lineLengthThreshold, false);

	vector<Mat> VD = VPDetector::detectVP(lineClusters, indices, K, threshold);

	namedWindow(windowName, WINDOW_AUTOSIZE);

	cout << "Calibration :\n" << K << endl;

	cout << "VD1 :\n" << VD[0] << endl;
	windowImage = drawLineClusters(image1, lineClusters[0], 1, true, indices[0]);
	windowImage = VPDetector::drawAxis(windowImage, VD[0], K);
	imwrite("results\\pair1.png", windowImage);
	imshow(windowName, windowImage);
	inputKey = waitKey();

	cout << "VD2 :\n" << VD[1] << endl;
	windowImage = drawLineClusters(image2, lineClusters[1], 1, true, indices[1]);
	windowImage = VPDetector::drawAxis(windowImage, VD[1], K);
	imwrite("results\\pair2.png", windowImage);
	imshow(windowName, windowImage);
	inputKey = waitKey();

	return inputKey != 27;
}

bool testRegionGenerator(const Mat &image, const float threshold, const float lineLengthThreshold) {
	const String windowName = "Region Generator";
	int inputKey = 0;
	LineClusters lineClusters = LineClustering(image, 2, threshold, lineLengthThreshold, true);
	Mat windowImage = image.clone();
	CallbackData callbackData;
	vector<int> indices;
	Mat K = (Mat_<float>(3, 3) << 800.f, 0, image.cols / 2, 0, 800.f, image.rows / 2, 0, 0, 1);

	// Convert RGB image to 3-channel grayscale image
	cvtColor(image, lineClusters.debugImage, CV_BGR2GRAY);
	cvtColor(lineClusters.debugImage, lineClusters.debugImage, CV_GRAY2BGR);

	// Initialize the window callback data
	callbackData.window_name = windowName;
	callbackData.image = image.clone();

	// Initialize the visualize window
	namedWindow(windowName, WINDOW_AUTOSIZE);
	windowImage = drawLineClusters(windowImage, lineClusters, 1);
	imshow(windowName, windowImage);
	setMouseCallback(windowName, DrawRectangleMouseCallback, &callbackData);

	// Estimate the calibration matrix
	Mat VD = VPDetector::detectVP(lineClusters, indices, K, threshold);

	// Test the image
	while (inputKey != 27) {
		inputKey = waitKey(30);

		switch (inputKey) {
		case KEY_ARROW_RIGHT:	// show the region w.r.t. the right line
			break;

		case KEY_ARROW_LEFT:	// show the region w.r.t. the left line
			break;
		}

		if (inputKey == 'n') {
			break;
		}

		if (callbackData.need_update) {
			int vertLine1Index = -1, vertLine2Index = -1;
			int horzLine1Index = -1;
			int vertClusterIndex = -1, horizClusterIndex = -1;
			float closestDistanceVertical1 = FLT_MAX, closestDistanceVertical2 = FLT_MAX;
			float closestDistanceHorizontal1 = FLT_MAX, closestDistanceHorizontal2 = FLT_MAX;
			ClusterLineIndices clusterLineIndices1, clusterLineIndices2;

			Point2f centerPoint((callbackData.roi_x0 + callbackData.roi_x1) / 2.f,
				(callbackData.roi_y0 + callbackData.roi_y1) / 2.f);
			Rect region(callbackData.roi_x0, callbackData.roi_y0, abs(callbackData.roi_x0 - callbackData.roi_x1),
				abs(callbackData.roi_y0 - callbackData.roi_y1));

			windowImage = image.clone();
			clusterLineIndices1.ClusterIndex = -1;
			clusterLineIndices1.Line1Index = -1;
			clusterLineIndices1.Line2Index = -1;
			clusterLineIndices2.ClusterIndex = -1;
			clusterLineIndices2.Line1Index = -1;
			clusterLineIndices2.Line2Index = -1;

			/*vector<Point2f> regions;
			lineClusters.findRegion(centerPoint, windowImage.size(), regions,
				min(abs(callbackData.roi_y0 - callbackData.roi_y1), abs(callbackData.roi_x0 - callbackData.roi_x1)),
				clusterLineIndices1, clusterLineIndices2);*/

			//vector<Point> polygon = lineClusters.findRegion(centerPoint, clusterLineIndices1, clusterLineIndices2);
			//vector<Point> polygon = lineClusters.findRegion(centerPoint, region, clusterLineIndices1, clusterLineIndices2);

			vector<pair<int, int>> selectedLines;
			vector<Point> polygon = REGION_DETECTION::findRegion(centerPoint, lineClusters, K, image.size(), true, region, selectedLines);

			windowImage = drawLineClusters(windowImage, lineClusters, 1);
			//windowImage = VPDetector::drawAxis(windowImage, VD, K);

			// Draw selected lines
			/*if (clusterLineIndices1.ClusterIndex >= 0) {
				if (clusterLineIndices1.Line1Index >= 0) {
					line(windowImage, lineClusters[clusterLineIndices1.ClusterIndex][clusterLineIndices1.Line1Index].StartPoint,
						lineClusters[clusterLineIndices1.ClusterIndex][clusterLineIndices1.Line1Index].EndPoint, CV_RGB(255, 0, 0), 2);
				}

				if (clusterLineIndices1.Line2Index >= 0) {
					line(windowImage, lineClusters[clusterLineIndices1.ClusterIndex][clusterLineIndices1.Line2Index].StartPoint,
						lineClusters[clusterLineIndices1.ClusterIndex][clusterLineIndices1.Line2Index].EndPoint, CV_RGB(255, 0, 0), 2);
				}

				if (clusterLineIndices2.ClusterIndex >= 0) {
					if (clusterLineIndices2.Line1Index >= 0) {
						Mat lineNormal = Line::computeLineNormal(centerPoint, lineClusters[clusterLineIndices2.ClusterIndex].getVanishingPoint());
						Point2f crossPoint = lineClusters[clusterLineIndices1.ClusterIndex][clusterLineIndices1.Line1Index].getIntersection(lineNormal);
						line(windowImage, centerPoint, crossPoint, CV_RGB(200, 200, 0));

						line(windowImage, lineClusters[clusterLineIndices2.ClusterIndex][clusterLineIndices2.Line1Index].StartPoint,
							lineClusters[clusterLineIndices2.ClusterIndex][clusterLineIndices2.Line1Index].EndPoint, CV_RGB(255, 0, 0), 2);
					}

					if (clusterLineIndices2.Line2Index >= 0) {
						line(windowImage, lineClusters[clusterLineIndices2.ClusterIndex][clusterLineIndices2.Line2Index].StartPoint,
							lineClusters[clusterLineIndices2.ClusterIndex][clusterLineIndices2.Line2Index].EndPoint, CV_RGB(255, 0, 0), 2);
					}
				}
			}

			// draw polygon
			if (clusterLineIndices1.Line1Index >= 0 && clusterLineIndices2.Line1Index >= 0) {
				line(windowImage, polygon[0], polygon[1], CV_RGB(255, 255, 0), 2);
				line(windowImage, polygon[1], polygon[2], CV_RGB(255, 255, 0), 2);
				line(windowImage, polygon[2], polygon[3], CV_RGB(255, 255, 0), 2);
				line(windowImage, polygon[3], polygon[0], CV_RGB(255, 255, 0), 2);
			}*/
			
			// Draw the region-of-interest
			rectangle(windowImage, Point(callbackData.roi_x0, callbackData.roi_y0),
				Point(callbackData.roi_x1, callbackData.roi_y1), CV_RGB(0, 255, 0));

			// Draw the selected lines
			for (const pair<int, int>& indexPair : selectedLines) {
				if (indexPair.first >= 0 && indexPair.first < lineClusters.size() &&
					indexPair.second >= 0 && indexPair.second < lineClusters[indexPair.first].size()) {
					line(windowImage, lineClusters[indexPair.first][indexPair.second].StartPoint,
						lineClusters[indexPair.first][indexPair.second].EndPoint, CV_RGB(255, 0, 0), 2);
				}
			}

			// Draw additional information
			if (selectedLines[0].first >= 0 && selectedLines[0].first < lineClusters.size() &&
				selectedLines[0].second >= 0 && selectedLines[0].second < lineClusters[selectedLines[0].first].size() &&
				selectedLines[1].first >= 0 && selectedLines[1].first < lineClusters.size() &&
				selectedLines[1].second >= 0 && selectedLines[1].second < lineClusters[selectedLines[1].first].size()) {

				// Draw the vanishing directions
				Mat lineNormal = Line::computeLineNormal(centerPoint, lineClusters[selectedLines[1].first].getVanishingPoint());
				Point2f crossPoint = lineClusters[selectedLines[0].first][selectedLines[0].second].getIntersection(lineNormal);
				line(windowImage, centerPoint, crossPoint, CV_RGB(200, 200, 0));

				lineNormal = Line::computeLineNormal(centerPoint, lineClusters[selectedLines[0].first].getVanishingPoint());
				crossPoint = lineClusters[selectedLines[1].first][selectedLines[1].second].getIntersection(lineNormal);
				line(windowImage, centerPoint, crossPoint, CV_RGB(200, 200, 0));

				// Draw a normal of the plane
				Point2f vp1 = lineClusters[selectedLines[0].first].getVanishingPoint();
				Point2f vp2 = lineClusters[selectedLines[1].first].getVanishingPoint();

				Mat vd1 = K.inv() * (Mat_<float>(3, 1) << vp1.x, vp1.y, 1.f);
				Mat vd2 = K.inv() * (Mat_<float>(3, 1) << vp2.x, vp2.y, 1.f);

				vd1 /= norm(vd1);
				vd2 /= norm(vd2);

				Mat normal = vd1.cross(vd2);

				Mat endPointVec = K * (normal);

				Point2f endPoint(endPointVec.at<float>(0) / endPointVec.at<float>(2),
					endPointVec.at<float>(1) / endPointVec.at<float>(2));

				Point2f vecDir = -(endPoint - centerPoint);
				vecDir /= (norm(vecDir) / 30.f);

				endPoint = vecDir + centerPoint;

				line(windowImage, centerPoint, endPoint, CV_RGB(0, 255, 255), 3);
			}

			callbackData.need_update = false;
			imshow(windowName, windowImage);

			imwrite("results\\selected_lines.png", windowImage);
		}
	}

	destroyWindow(windowName);

	return inputKey != 27;
}

vector<Point> testRegionGenerator(const Mat &image, const LineClusters& lineClusters,
	const Mat &K, vector<pair<int, int>> &selectedLines) {
	const String windowName = "Region Generator";
	int inputKey = 0;
	Mat windowImage = image.clone();
	CallbackData callbackData;
	vector<Point> polygon;

	// Initialize the window callback data
	callbackData.window_name = windowName;
	callbackData.image = image.clone();

	// Initialize the visualize window
	namedWindow(windowName, WINDOW_AUTOSIZE);
	windowImage = drawLineClusters(windowImage, lineClusters, 1);
	imshow(windowName, windowImage);
	setMouseCallback(windowName, DrawRectangleMouseCallback, &callbackData);

	// Test the image
	while (inputKey != 27) {
		inputKey = waitKey(30);

		if (callbackData.need_update) {
			Point2f centerPoint((callbackData.roi_x0 + callbackData.roi_x1) / 2.f,
				(callbackData.roi_y0 + callbackData.roi_y1) / 2.f);
			Rect region(callbackData.roi_x0, callbackData.roi_y0, abs(callbackData.roi_x0 - callbackData.roi_x1),
				abs(callbackData.roi_y0 - callbackData.roi_y1));

			selectedLines.clear();
			polygon = REGION_DETECTION::findRegion(centerPoint, lineClusters, K, image.size(), true, region, selectedLines);
			
			windowImage = drawLineClusters(image.clone(), lineClusters, 1);

			// Draw the region-of-interest
			rectangle(windowImage, Point(callbackData.roi_x0, callbackData.roi_y0),
				Point(callbackData.roi_x1, callbackData.roi_y1), CV_RGB(0, 255, 0));

			// Draw the selected lines
			for (const pair<int, int>& indexPair : selectedLines) {
				if (indexPair.first >= 0 && indexPair.first < lineClusters.size() &&
					indexPair.second >= 0 && indexPair.second < lineClusters[indexPair.first].size()) {
					line(windowImage, lineClusters[indexPair.first][indexPair.second].StartPoint,
						lineClusters[indexPair.first][indexPair.second].EndPoint, CV_RGB(255, 0, 0), 2);
				}
			}

			// Draw polygon
			line(windowImage, polygon[0], polygon[1], CV_RGB(255, 0, 255), 1);
			line(windowImage, polygon[0], polygon[3], CV_RGB(255, 0, 255), 1);
			line(windowImage, polygon[0], polygon[2], CV_RGB(255, 0, 255), 1);
			line(windowImage, polygon[1], polygon[3], CV_RGB(255, 0, 255), 1);

			callbackData.need_update = false;
			imshow(windowName, windowImage);
		}
	}

	destroyWindow(windowName);

	return polygon;
}

void main_testAll() {
	//Mat image1 = imread("..\\Images\\MichiganIndoor\\Library\\frame00001.png");
	Mat image1 = imread("..\\Images\\SFO pictures\\IMG_0025.jpg");
	Mat image2 = imread("..\\Images\\MichiganIndoor\\Library\\frame00050.png");
	bool doResize = true;

	// Resize the input images if input images are too large
	if (doResize) {
		if (max(image1.cols, image1.rows) > 1920) {
			cv::resize(image1, image1, cv::Size(image1.cols / 4, image1.rows / 4));
		}
		if (max(image1.cols, image1.rows) > 1920) {
			cv::resize(image2, image2, cv::Size(image2.cols / 4, image2.rows / 4));
		}
	}

	/*
	Mat gray, edges;
	cvtColor(image1, gray, CV_BGR2GRAY);
	cv::Canny(gray, edges, 50, 100);
	imshow("Edges", edges);
	waitKey();
	*/

	// 1) Point pair clustering
	//cv::imwrite("clusters.png", PointPairClustering(image1, image2));

	// 2) Line clustering
	//LineClustering(image1, 4.f);

	testRegionGenerator(image1, 2.f, 50.f);

	system("pause");
}

void main_imagePairCalibration() {
	Mat image1 = imread("..\\Images\\MichiganIndoor\\Library\\frame00001.png");
	Mat image2 = imread("..\\Images\\MichiganIndoor\\Library\\frame00002.png");

	Mat K = (Mat_<float>(3, 3) << 800.f, 0, image1.cols / 2, 0, 800.f, image1.rows / 2, 0, 0, 1);

	VPDetectionPair(image1, image2, K);

	system("pause");
}

bool testRegionalFitting(const Mat &image1, const vector<CDataType*> &pointPairs,
	const LineClusters &lineClusters, const Mat &K, const Mat &R, const Mat &VD) {
	Mat image1Gray;
	int inputKey = 0;

	cvtColor(image1, image1Gray, CV_BGR2GRAY);
	cvtColor(image1Gray, image1Gray, CV_GRAY2BGR);

	namedWindow("Regions");
	for (int pi = 29; pi < pointPairs.size(); pi++) {
		Point2f centerPoint((*pointPairs[pi])[0], (*pointPairs[pi])[1]);
		vector<vector<Point>> polygon(1);
		Mat windowImage = drawPoints(image1Gray, pointPairs, 2);
		windowImage = drawLineClusters(windowImage, lineClusters, 1);
		Mat mask = Mat::zeros(image1.size(), CV_8U);
		vector<PointPair> selectedPointPairs;
		vector<pair<int, int>> selectedLines;

		Mat H, normal;
		vector<pair<Point2f, Point2f>> inlierPointPairs;

		polygon[0] = REGION_DETECTION::findRegion(centerPoint,
			lineClusters, K, image1.size(), true, Rect(), selectedLines);

		if (polygon[0].size() != 4) {
			cout << "Failed to find a region from current point: " << centerPoint << endl;
			continue;
		}

		// draw polygon
		if (polygon[0].size() == 4) {
			line(windowImage, polygon[0][0], polygon[0][1], CV_RGB(255, 255, 0), 2);
			line(windowImage, polygon[0][1], polygon[0][2], CV_RGB(255, 255, 0), 2);
			line(windowImage, polygon[0][2], polygon[0][3], CV_RGB(255, 255, 0), 2);
			line(windowImage, polygon[0][3], polygon[0][0], CV_RGB(255, 255, 0), 2);
		}

		//imwrite("results\\region_sample1.png", windowImage);

		// Draw the selected lines
		for (int i = 0; i < selectedLines.size(); i++) {
			if (selectedLines[i].first >= 0 && selectedLines[i].first < lineClusters.size() &&
				selectedLines[i].second >= 0 && selectedLines[i].second < lineClusters[selectedLines[i].first].size()) {
				line(windowImage, lineClusters[selectedLines[i].first][selectedLines[i].second].StartPoint,
					lineClusters[selectedLines[i].first][selectedLines[i].second].EndPoint, CV_RGB(255, 0, 0), 2);
			}
		}

		//imwrite("results\\region_sample2.png", windowImage);

		// Generate a mask with found region polygon
		fillPoly(mask, polygon, CV_RGB(255, 255, 255));

		// Find points that are in the region
		for (CDataType* dataPoint : pointPairs) {
			const PointPair *pointPair = dynamic_cast<PointPair*>(dataPoint);

			if (mask.at<uchar>(pointPair->Point1) != 0) {
				selectedPointPairs.push_back(*pointPair);
				circle(windowImage, pointPair->Point1, 2, CV_RGB(0, 255, 255), 2);
			}
		}

		circle(windowImage, centerPoint, 2, CV_RGB(255, 0, 0), 2);

		imshow("Regions", windowImage);
		inputKey = waitKey();
		if (inputKey == 's') {
			imwrite("results\\region_sample.png", windowImage);
		}
		else if (inputKey == 27) {
			break;
		}

		normal = REGION_DETECTION::computeNormal(polygon[0], K);

		if (selectedPointPairs.size() < 2) {
			cout << "No points were selected from this region." << endl;
			continue;
		}

		cout << "-Computed normal: " << normal.t() << endl;

		// Compute a MW homography with all the points in the region using computed normal
		H = PlaneFitting::computeMWHomography(selectedPointPairs, K, K, R, normal);

		inlierPointPairs = computeInliers(pointPairs, H);

		windowImage = drawPoints(image1, inlierPointPairs, 2);
		putText(windowImage, "Computed inliers with regional normal", Point(15, 25), cv::FONT_HERSHEY_PLAIN, 2, CV_RGB(0, 255, 0));

		imshow("Regions", windowImage);
		inputKey = waitKey();
		if (inputKey == 's') {
			imwrite("results\\region_inliers_sample.png", windowImage);
		}
		else if (inputKey == 27 || inputKey == 'c') {
			break;
		}
		
		// Compute a MW homography with all the points in the region using one of global normals
		normal = VD.col(PlaneFitting::getClosestDirectionalColumn(normal, VD));
		H = PlaneFitting::computeMWHomography(selectedPointPairs, K, K, R, normal);
		inlierPointPairs = computeInliers(pointPairs, H);

		cout << "-Selected normal: " << normal.t() << endl;

		windowImage = drawPoints(image1, inlierPointPairs, 2);
		putText(windowImage, "Computed inliers with global normal", Point(15, 25), cv::FONT_HERSHEY_PLAIN, 2, CV_RGB(0, 255, 0));

		imshow("Regions", windowImage);
		inputKey = waitKey();
		if (inputKey == 's') {
			imwrite("results\\region_inliers_sample_global.png", windowImage);
		}
		else if (inputKey == 27) {
			break;
		}
	}
	destroyWindow("Regions");

	return inputKey != 'c';
}

void main_samplingRegionGeneration() {
	vector<KeyPoint> keypoints1, keypoints2;
	Mat descriptors1, descriptors2;
	const float threshold = 2.f;
	const float lineLengthThreshold = 40.f;

	const Mat image1 = imread("");
	const Mat image2 = imread("");
	const Mat K = REGION_DETECTION::load3x3Matrix("");

	computeSIFTFeaturePoints(image1, keypoints1, descriptors1);
	computeSIFTFeaturePoints(image2, keypoints2, descriptors2);
	
	vector<CDataType*> pointPairs;
	computeMatches(keypoints1, keypoints2, descriptors1, descriptors2, pointPairs);

	vector<pair<int, int>> selectedLines;
	LineClusters lineClusters = LineClustering(image1, 2, threshold, lineLengthThreshold, false);
	vector<Point> polygon = testRegionGenerator(image1, lineClusters, K, selectedLines);
}

void main_visualOdometry(string dataFilename) {
	vector<DataInfo> dataList = loadSetupFile(dataFilename);
	const int numZeros = 5;
	const float threshold = 2.5f;
	const float lineThreshold = 2.5f;

	for (const DataInfo &dataInfo : dataList) {
		DataFiles dataFiles(dataInfo, numZeros);

		// load related data files
		Mat image1 = imread(dataFiles.Image1filename);
		Mat image1Gray;
		Mat K = REGION_DETECTION::load3x3Matrix(dataFiles.CalibrationFilename);
		vector<Mat> VD(2);
		vector<KeyPoint> keypoints1;
		Mat descriptors1;

		const float lineLengthThrLong = sqrt(image1.cols * image1.cols + image1.rows * image1.rows) / 30.f;
		const float lineLengthThrShort = sqrt(image1.cols * image1.cols + image1.rows * image1.rows) / 50.f;

		VD[0] = REGION_DETECTION::load3x3Matrix(dataFiles.Image1VDfilename);

		cvtColor(image1, image1Gray, CV_BGR2GRAY);
		cvtColor(image1Gray, image1Gray, CV_GRAY2BGR);

		computeSIFTFeaturePoints(image1, keypoints1, descriptors1);

		// Build filenames
		int secImgIdxEnd = dataInfo.secImgIdxBeg + (dataInfo.numOfImg * dataInfo.imgIdxStep);
		for (int secondImgIdx = dataInfo.secImgIdxBeg; secondImgIdx < secImgIdxEnd; secondImgIdx += dataInfo.imgIdxStep) {
			// Build filenames
			string image2filename = buildFilename(dataFiles.DataFolder, dataInfo.filePrefix, '0', numZeros, secondImgIdx, "", dataInfo.imageExtension);
			string image2VDfilename = buildFilename(dataFiles.DataFolder, dataInfo.filePrefix, '0', numZeros, secondImgIdx, VDSufix, dataInfo.dataExtension);
			string outputFolder = buildOutputFolder(dataInfo.datasetName, dataInfo.fileIndex, secondImgIdx);
			string testName = buildTestName(dataInfo.datasetName, dataInfo.fileIndex, secondImgIdx);

			Mat image2 = imread(image2filename);
			vector<Mat> models;
			Mat R, t;
			vector<KeyPoint> keypoints2;
			Mat descriptors2;
			double ARI;

			if (image2.empty()) {
				cout << "Not able to read image file from:\n" << image2filename << endl;
				system("pause");
				return;
			}

			VD[1] = REGION_DETECTION::load3x3Matrix(image2VDfilename);

			// Set the rotation matrix
			R = VD[1] * VD[0].inv();
			cout << "R =\n" << R << "\n" << endl;

			computeSIFTFeaturePoints(image2, keypoints2, descriptors2);

			vector<pair<Point2f, Point2f>> pointPairs;
			computeMatches(keypoints1, keypoints2, descriptors1, descriptors2, pointPairs);

			imwrite(outputFolder + "DetectedPoints.png", drawPoints(image1.clone(), pointPairs, 2));

			cout << K << endl;

			// Do optimization here
			EssentialResidual::R = R;
			t = VisualOdometry::refineTranslation(pointPairs, K);

			cout << t.t() << endl;
		}
	}
}

void main_multiPlanarFitting(string dataFilename, int numZeros) {
	vector<DataInfo> dataList = loadSetupFile(dataFilename);
	const bool readVDFromFile = true;

	double t = 0.0;
	double timeConsumed1 = 0.0, timeConsumed2 = 0.0;
	int num_test = 0;

	ofstream fout(buildLogFilename());
	fout << left
		<< setw(24) << "Dataset" << " "
		<< setw(8) << "RegMW" << " "
		//<< setw(8) << "RegMW+MF" << " "
		<< setw(8) << "RegMW+M" << " "
		<< setw(8) << "OM+MW" << " "
		<< setw(8) << "OM+MW+M" << " "
		//<< setw(8) << "OMCCMW" << " "
		//<< setw(8) << "OMCCMW+M" << " "
		<< setw(8) << "RndMW" << " "
		<< setw(8) << "RndMW+MF" << " "
		<< setw(8) << "RndMW+M" << " "
		<< setw(8) << "Rnd+MWM" << " "
		<< setw(8) << "RndWMWMF" << " "
		<< setw(8) << "RndUC" << " "
		<< setw(8) << "RndUCMF" << " "
		<< endl;

	if (!fout.is_open()) {
		cout << "Failed to open output log file: " << buildLogFilename() << endl;
		system("pause");
		return;
	}

	for (const DataInfo &dataInfo : dataList) {
		DataFiles dataFiles(dataInfo, numZeros);
		
		const float threshold = 2.5f;
		const float lineThreshold = 2.5f;
		const int numOfModels = 5000;
		const Mat image1 = imread(dataFiles.Image1filename);
		const Mat label = imread(dataFiles.Image1GTfilename, CV_LOAD_IMAGE_GRAYSCALE);
		const Mat OM = imread(dataFiles.Image1OMfilename, CV_LOAD_IMAGE_GRAYSCALE);
		Mat K = REGION_DETECTION::load3x3Matrix(dataFiles.CalibrationFilename);
		vector<KeyPoint> keypoints1;
		Mat descriptors1;
		Mat image1Gray;
		vector<Mat> VD(2);

		//const float lineLengthThreshold = 40.f;
		const float lineLengthThrLong = sqrt(image1.cols * image1.cols + image1.rows * image1.rows) / 30.f;
		const float lineLengthThrShort = sqrt(image1.cols * image1.cols + image1.rows * image1.rows) / 50.f;

		if (image1.empty()) {
			cout << "Not able to read image file from:\n" << dataFiles.Image1filename << endl;
			system("pause");
			continue;
		}

		if (label.empty()) {
			cout << "Not able to read image file from:\n" << dataFiles.Image1GTfilename << endl;
			system("pause");
			continue;
		}

		if (OM.empty()) {
			cout << "Not able to read image file from:\n" << dataFiles.Image1OMfilename << endl;
		}

		if (K.empty()) {
			cout << "Failed to load the calibration matrix from: " << dataFiles.CalibrationFilename << endl;
			system("pause");
			continue;
		}

		cvtColor(image1, image1Gray, CV_BGR2GRAY);
		cvtColor(image1Gray, image1Gray, CV_GRAY2BGR);

		computeSIFTFeaturePoints(image1, keypoints1, descriptors1);

		int secImgIdxEnd = dataInfo.secImgIdxBeg + (dataInfo.numOfImg * dataInfo.imgIdxStep);
		vector<LineClusters> lineClusters(2);

		if (readVDFromFile) {
			VD[0] = REGION_DETECTION::load3x3Matrix(dataFiles.Image1VDfilename);
			lineClusters[0] = LineClustering(image1, VD[0], K, lineThreshold, lineLengthThrShort);
		}
		else {
			lineClusters[0] = LineClustering(image1, 2, lineThreshold, lineLengthThrLong, false);
			//lineClusters[0] = lineClusters[0].subCluster(selectClusters(lineClusters[0], VD[0], K), threshold, true);
		}

		for (int secondImgIdx = dataInfo.secImgIdxBeg; secondImgIdx < secImgIdxEnd; secondImgIdx += dataInfo.imgIdxStep) {

			// Build filenames
			string image2filename = buildFilename(dataFiles.DataFolder, dataInfo.filePrefix, '0', numZeros, secondImgIdx, "", dataInfo.imageExtension);
			string image2VDfilename = buildFilename(dataFiles.DataFolder, dataInfo.filePrefix, '0', numZeros, secondImgIdx, VDSufix, dataInfo.dataExtension);
			string outputFolder = buildOutputFolder(dataInfo.datasetName, dataInfo.fileIndex, secondImgIdx);
			string testName = buildTestName(dataInfo.datasetName, dataInfo.fileIndex, secondImgIdx);

			Mat image2 = imread(image2filename);
			//Mat K = (Mat_<float>(3, 3) << 800.f, 0, image1.cols / 2, 0, 800.f, image1.rows / 2, 0, 0, 1);
			vector<Mat> models;
			Mat R;
			vector<KeyPoint> keypoints2;
			Mat descriptors2;
			double ARI;

			if (image2.empty()) {
				cout << "Not able to read image file from:\n" << image2filename << endl;
				system("pause");
				return;
			}

			cout << "\n*** " << testName << " ***" << endl;
			fout << setw(24) << left << testName << " ";
			fout << fixed;

			if (readVDFromFile) {
				VD[1] = REGION_DETECTION::load3x3Matrix(image2VDfilename);
				lineClusters[1] = LineClustering(image2, VD[1], K, lineThreshold, lineLengthThrShort);

				imwrite(outputFolder + "lineClustersFromVD.png", drawLineClusters(image1.clone(), lineClusters[0], 2, true));
			}
			else {
				vector<vector<int>> selectedLineClusters;

				lineClusters[1] = LineClustering(image2, 2, lineThreshold, lineLengthThrLong, false);

				imwrite(outputFolder + "lineClusters1_org.png", drawLineClusters(image1.clone(), lineClusters[0], 2, true));
				imwrite(outputFolder + "lineClusters2_org.png", drawLineClusters(image2.clone(), lineClusters[1], 2, true));

				for (int it = 0; it < 1; it++) {
					VD = VPDetector::detectVP(lineClusters, selectedLineClusters, K, lineThreshold);

					lineClusters[0] = LineClustering(image1, VD[0], K, lineThreshold, lineLengthThrShort);
					lineClusters[1] = LineClustering(image2, VD[1], K, lineThreshold, lineLengthThrShort);
				}

				imwrite(outputFolder + "lineClusters1.png", drawLineClusters(image1.clone(), lineClusters[0], 2, true));
				imwrite(outputFolder + "lineClusters2.png", drawLineClusters(image2.clone(), lineClusters[1], 2, true));

				cout << K << endl;
			}

			// Select three orthogonal clusters which are aligned with one of vanishing directions
			//selectedLineClusters.push_back(selectClusters(lineClusters[0], VD[0], K));

			cout << "VD0=" << VD[0] << endl;
			cout << "-Before alignment-" << endl;
			cout << "VD1_0=" << VD[1] << endl;
			VD[1] = PlaneFitting::alignRotation(VD[0], VD[1]);
			cout << "-After alignment-" << endl;
			cout << "VD1_1=" << VD[1] << endl;

			// Set the rotation matrix
			R = VD[1] * VD[0].inv();
			cout << "R=" << R << endl;

			computeSIFTFeaturePoints(image2, keypoints2, descriptors2);

			vector<CDataType*> pointPairs;
			computeMatches(keypoints1, keypoints2, descriptors1, descriptors2, pointPairs);
			
			imwrite(outputFolder + "DetectedPoints.png", drawPoints(image1.clone(), pointPairs, 2));

			// Generate ground-truth clusters
			PointPairClusters GTPointPairClusters = generatePointPairClusters(pointPairs, label, VD[0]);

			imwrite(outputFolder + "Ground-Truth.png", drawPoints(image1.clone(), GTPointPairClusters, 2));
			imwrite(outputFolder + "Ground-Truth_dir.png", drawPoints(image1, GTPointPairClusters, 2, VD[0], K, R));

			//if (!testRegionalFitting(image1, pointPairs, lineClusters[0], K, R, VD[0])) {
			//	continue;
			//}

			vector<Mat> normals;
			vector<vector<unsigned int>> indices;
			vector<PointPairCluster> pointPairClusters;
			PointPairClusters outputClusters;

			RandomSampler randomSampler(4, 4, &EuclideanDistancePointPair);
			initRandomSampler(randomSampler, pointPairs, pointPairs.size());
			indices.clear();
			randomSampler.GetNSample(indices, numOfModels);

			//imwrite("results\\lineClusterRecomputed.png", drawLineClusters(image1.clone(), lineClusters[0].subCluster(selectedLineClusters[0], threshold, true), 2, true));

			//----------------------------
			// Region-based sampling with global normals
			//----------------------------
			
			t = getTickCount();
			models = PlaneFitting::generateMWHomographies(pointPairs, indices, 2, K, K, R, VD[0], normals);

			// T-linkage clustering with models
			pointPairClusters = PointPairClustering(pointPairs, models, normals);
			timeConsumed2 += getTickCount() - t;
			num_test++;
			normals.clear();

			cout << "Try region-based sampling with global normals..." << endl;

			//fout << PlaneFitting::generateMWHomographiesDebug(image1, lineClusters[0],
			//	pointPairs, K, R, VD[0]) << " ";

			t = getTickCount();
			models = PlaneFitting::generateMWHomographies(image1, lineClusters[0],
				pointPairs, K, R, VD[0], true, true, normals);

			// T-linkage clustering with models
			pointPairClusters = PointPairClustering(pointPairs, models, normals);
			timeConsumed1 += getTickCount() - t;
			
			cout << "(DEBUG) Normal of each cluster: " << endl;
			for (int i = 0; i < pointPairClusters.size(); i++) {
				cout << pointPairClusters[i].PlaneNormal.t() << endl;
			}

			outputClusters = PointPairClusters(pointPairClusters, pointPairs);
			imwrite(outputFolder + "output_MW_regSamp.png", drawPoints(image1, outputClusters, 2));
			imwrite(outputFolder + "output_MW_regSamp_dir.png", drawPoints(image1, outputClusters, 2, VD[0], K, R));
			ARI = PointPairClusters::computeAdjustedRandIndex(GTPointPairClusters, outputClusters);
			cout << "ARI before merge: " << ARI << endl;
			fout << setprecision(6) << ARI << " ";

			// Apply post-processing of Multiple Plane Detection paper
			//pointPairClusters = PlaneFitting::multiplePlaneDetection(outputClusters, image1.size(), threshold);
			//if (pointPairClusters.size() > 0) {
			//	PointPairClusters tempClusters = PointPairClusters(pointPairClusters, pointPairs);
			//	imwrite(outputFolder + "output_MW_random_MPD.png", drawPoints(image1, tempClusters, 2));
			//	ARI = PointPairClusters::computeAdjustedRandIndex(GTPointPairClusters, tempClusters);
			//	cout << "ARI after MPD merge: " << ARI << endl;
			//	fout << setprecision(6) << ARI << " ";
			//}
			//else {
			//	cout << "ARI after MPD merge: FAILED" << endl;
			//	fout << setprecision(6) << ARI << " ";
			//}

			// Apply post-processing
			outputClusters = PlaneFitting::mergeClusters(outputClusters, K, K, R, VD[0], threshold, 0);
			//outputClusters = PlaneFitting::mergeClusters(outputClusters, K, VD[0], threshold, 0, image1.clone());
			imwrite(outputFolder + "output_MW_regSamp_merged.png", drawPoints(image1, outputClusters, 2));
			imwrite(outputFolder + "output_MW_regSamp_merged_dir.png", drawPoints(image1, outputClusters, 2, VD[0], K, R));
			ARI = PointPairClusters::computeAdjustedRandIndex(GTPointPairClusters, outputClusters);
			cout << "ARI after merge: " << ARI << endl;
			fout << setprecision(6) << ARI << " ";
			cout << "===" << endl;
			
			//----------------------------
			// Region-based sampling with regional normals
			//----------------------------

			//cout << "Try region-based sampling with regional normals..." << endl;

			//normals.clear();
			//models = PlaneFitting::generateMWHomographies(image1, lineClusters[0],
			//	pointPairs, K, R, VD[0], false, true, normals);

			//// T-linkage clustering with models
			//pointPairClusters = PointPairClustering(pointPairs, models, normals);

			//cout << "(DEBUG) Normal of each cluster: " << endl;
			//for (int i = 0; i < pointPairClusters.size(); i++) {
			//	cout << pointPairClusters[i].PlaneNormal.t() << endl;
			//}

			//outputClusters = PointPairClusters(pointPairClusters, pointPairs);
			//imwrite(outputFolder + "outputClusters_MW_regional_normal.png", drawPoints(image1, outputClusters, 2));
			//imwrite(outputFolder + "outputClusters_MW_regional_normal_dir.png", drawPoints(image1, outputClusters, 2, VD[0], K));
			//ARI = PointPairClusters::computeAdjustedRandIndex(GTPointPairClusters, outputClusters);
			//cout << "ARI before merge: " << ARI << endl;
			//fout << setprecision(6) << ARI << " ";

			//// Apply post-processing
			//outputClusters = PlaneFitting::mergeClusters(outputClusters, K, K, R, VD[0], threshold, 0);
			////outputClusters = PlaneFitting::mergeClusters(outputClusters, K, VD[0], threshold, 0, image1.clone());
			//imwrite(outputFolder + "outputClusters_MW_regional_normal_merged.png", drawPoints(image1, outputClusters, 2));
			//imwrite(outputFolder + "outputClusters_MW_regional_normal_merged_dir.png", drawPoints(image1, outputClusters, 2, VD[0], K));
			//ARI = PointPairClusters::computeAdjustedRandIndex(GTPointPairClusters, outputClusters);
			//cout << "ARI after merge: " << ARI << endl;
			//fout << setprecision(6) << ARI << " ";
			//cout << "===" << endl;

			//----------------------------
			// Orientation-map-based sampling with global normals
			//----------------------------
			/*
			if (!OM.empty()) {
				//----------------------------
				// Orientation-map-based sampling with global normals
				//----------------------------
				normals.clear();
				models = PlaneFitting::generateMWHomographies(image1, pointPairs, OM, K, R, VD[0], 1000, normals);

				pointPairClusters = PointPairClustering(pointPairs, models, normals);

				outputClusters = PointPairClusters(pointPairClusters, pointPairs);
				imwrite(outputFolder + "output_MW_OM.png", drawPoints(image1, outputClusters, 2));
				imwrite(outputFolder + "output_MW_OM_dir.png", drawPoints(image1, outputClusters, 2, VD[0], K, R));
				ARI = PointPairClusters::computeAdjustedRandIndex(GTPointPairClusters, outputClusters);
				cout << "ARI before merge: " << ARI << endl;
				fout << setprecision(6) << ARI << " ";

				// Apply post-processing
				outputClusters = PlaneFitting::mergeClusters(outputClusters, K, K, R, VD[0], threshold, 0);
				//outputClusters = PlaneFitting::mergeClusters(outputClusters, K, VD[0], threshold, 0, image1.clone());
				imwrite(outputFolder + "output_MW_OM_merged.png", drawPoints(image1, outputClusters, 2));
				imwrite(outputFolder + "output_MW_OM_merged_dir.png", drawPoints(image1, outputClusters, 2, VD[0], K, R));
				ARI = PointPairClusters::computeAdjustedRandIndex(GTPointPairClusters, outputClusters);
				cout << "ARI after merge: " << ARI << endl;
				fout << setprecision(6) << ARI << " ";
				cout << "===" << endl;

				//----------------------------
				// Orientation-map-based clustering
				//----------------------------

				normals.clear();
				//outputClusters = generatePointPairClusters(pointPairs, PlaneFitting::separateOMLabels(OM), VD[0]);

				//imwrite(outputFolder + "output_OM.png", drawPoints(image1, outputClusters, 2));
				//imwrite(outputFolder + "output_OM_dir.png", drawPoints(image1, outputClusters, 2, VD[0], K, R));
				//ARI = PointPairClusters::computeAdjustedRandIndex(GTPointPairClusters, outputClusters);
				//cout << "ARI before merge: " << ARI << endl;
				//fout << setprecision(6) << ARI << " ";

				// Apply post-processing
				//outputClusters = PlaneFitting::mergeClusters(outputClusters, K, K, R, VD[0], threshold, 0);
				//imwrite(outputFolder + "output_OM_merged.png", drawPoints(image1, outputClusters, 2));
				//imwrite(outputFolder + "output_OM_merged_dir.png", drawPoints(image1, outputClusters, 2, VD[0], K, R));
				//ARI = PointPairClusters::computeAdjustedRandIndex(GTPointPairClusters, outputClusters);
				//cout << "ARI after merge: " << ARI << endl;
				//fout << setprecision(6) << ARI << " ";
				//cout << "===" << endl;
			}
			else {
				fout << fixed << setprecision(6) << 0.0 << " ";
				fout << fixed << setprecision(6) << 0.0 << " ";
			}
			*/
			//----------------------------
			// Random sampling -- MW
			//----------------------------
			/*
			normals.clear();

			t = getTickCount();
			models = PlaneFitting::generateMWHomographies(pointPairs, indices, 2, K, K, R, VD[0], normals);
			
			// T-linkage clustering with models
			pointPairClusters = PointPairClustering(pointPairs, models, normals);
			timeConsumed2 += getTickCount() - t;
			num_test++;

			outputClusters = PointPairClusters(pointPairClusters, pointPairs);
			imwrite(outputFolder + "output_MW_random.png", drawPoints(image1, outputClusters, 2));
			imwrite(outputFolder + "output_MW_random_dir.png", drawPoints(image1, outputClusters, 2, VD[0], K, R));
			ARI = PointPairClusters::computeAdjustedRandIndex(GTPointPairClusters, outputClusters);
			cout << "ARI before merge: " << ARI << endl;
			fout << setprecision(6) << ARI << " ";

			// Apply post-processing of Multiple Plane Detection paper
			pointPairClusters = PlaneFitting::multiplePlaneDetection(outputClusters, image1.size(), threshold);
			if (pointPairClusters.size() > 0) {
				PointPairClusters tempClusters = PointPairClusters(pointPairClusters, pointPairs);
				imwrite(outputFolder + "output_MW_random_MPD.png", drawPoints(image1, tempClusters, 2));
				ARI = PointPairClusters::computeAdjustedRandIndex(GTPointPairClusters, tempClusters);
				cout << "ARI after MPD merge: " << ARI << endl;
				fout << setprecision(6) << ARI << " ";
			}
			else {
				cout << "ARI after MPD merge: FAILED" << endl;
				fout << setprecision(6) << ARI << " ";
			}

			// Apply post-processing
			outputClusters = PlaneFitting::mergeClusters(outputClusters, K, K, R, VD[0], threshold, 0);
			//outputClusters = PlaneFitting::mergeClusters(outputClusters, K, VD[0], threshold, 0, image1.clone());
			imwrite(outputFolder + "output_MW_random_merged.png", drawPoints(image1, outputClusters, 2));
			imwrite(outputFolder + "output_MW_random_merged_dir.png", drawPoints(image1, outputClusters, 2, VD[0], K, R));
			ARI = PointPairClusters::computeAdjustedRandIndex(GTPointPairClusters, outputClusters);
			cout << "ARI after merge: " << ARI << endl;
			fout << setprecision(6) << ARI << " ";
			cout << "===" << endl;
			*/
			//----------------------------
			// Random sampling -- weak-Manhattan World
			//----------------------------
			/*
			models = PlaneFitting::generateWMWHomographies(pointPairs, indices, 3, K, K, R);

			// T-linkage clustering with models
			pointPairClusters = PointPairClustering(pointPairs, models);

			outputClusters = PointPairClusters(pointPairClusters, pointPairs);
			imwrite(outputFolder + "output_WMW_random.png", drawPoints(image1, outputClusters, 2));
			ARI = PointPairClusters::computeAdjustedRandIndex(GTPointPairClusters, outputClusters);
			cout << "ARI before merge: " << ARI << endl;
			fout << setprecision(6) << ARI << " ";

			// Apply post-processing of Multiple Plane Detection paper
			pointPairClusters = PlaneFitting::multiplePlaneDetection(outputClusters, image1.size(), threshold);
			if (pointPairClusters.size() > 0) {
				PointPairClusters tempClusters = PointPairClusters(pointPairClusters, pointPairs);
				imwrite(outputFolder + "output_WMW_random_MPD.png", drawPoints(image1, tempClusters, 2));
				ARI = PointPairClusters::computeAdjustedRandIndex(GTPointPairClusters, tempClusters);
				cout << "ARI after MPD merge: " << ARI << endl;
				fout << setprecision(6) << ARI << " ";
			}
			else {
				cout << "ARI after MPD merge: FAILED" << endl;
				fout << setprecision(6) << ARI << " ";
			}

			//----------------------------
			// Random sampling -- unconstrained
			//----------------------------

			models = PlaneFitting::generateHomographies(pointPairs, indices, 4);

			// T-linkage clustering with models
			pointPairClusters = PointPairClustering(pointPairs, models);

			outputClusters = PointPairClusters(pointPairClusters, pointPairs);
			imwrite(outputFolder + "output_UC_random.png", drawPoints(image1, outputClusters, 2));
			ARI = PointPairClusters::computeAdjustedRandIndex(GTPointPairClusters, outputClusters);
			cout << "ARI before merge: " << ARI << endl;
			fout << setprecision(6) << ARI << " ";

			// Apply post-processing of Multiple Plane Detection paper
			pointPairClusters = PlaneFitting::multiplePlaneDetection(outputClusters, image1.size(), threshold);
			if (pointPairClusters.size() > 0) {
				PointPairClusters tempClusters = PointPairClusters(pointPairClusters, pointPairs);
				imwrite(outputFolder + "output_UC_random_MPD.png", drawPoints(image1, tempClusters, 2));
				ARI = PointPairClusters::computeAdjustedRandIndex(GTPointPairClusters, tempClusters);
				cout << "ARI after MPD merge: " << ARI << endl;
				fout << setprecision(6) << ARI << " ";
			}
			else {
				cout << "ARI after MPD merge: FAILED" << endl;
				fout << setprecision(6) << ARI << " ";
			}
			*/
			//----------------------------
			// Clean-up memories
			//----------------------------

			for (unsigned int i = 0; i < pointPairs.size(); i++) {
				delete pointPairs[i];
			}

			fout << endl;
		}
	}

	if (fout.is_open()) {
		fout.close();
	}

	cout << "The average consumed time for region-based: " << (timeConsumed1 / num_test) / getTickFrequency() << endl;
	cout << "The average consumed time for random-based: " << (timeConsumed2 / num_test) / getTickFrequency() << endl;
}

bool main_metricRectification() {
	float threshold = 2.5f;
	float lineLengthThreshold = 40.f;
	const int numOfModels = 5000;
	Mat image = imread("Images\\IMG_0064.JPG");
	Mat imageGray;
	LineClusters lineClusters;

	resize(image, image, Size(1024, 768));

	Mat K = (Mat_<float>(3, 3) << 800.f, 0, image.cols / 2, 0, 800.f, image.rows / 2, 0, 0, 1);

	cvtColor(image, imageGray, CV_BGR2GRAY);
	cvtColor(imageGray, imageGray, CV_GRAY2BGR);

	lineClusters = LineClustering(image, 2, threshold, lineLengthThreshold, false);

	vector<int> selectedClusters;

	// Estimate the calibration matrix
	Mat VD = VPDetector::detectVP(lineClusters, selectedClusters, K, threshold);

	namedWindow("Warping Test");

	int inKey = 0;
	while (inKey != 27) {
		vector<pair<int, int>> selectedLines;
		vector<Point> polygon = testRegionGenerator(image, lineClusters, K, selectedLines);
		vector<pair<Line, Line>> linePairs;

		linePairs.push_back(pair<Line, Line>(Line(polygon[0], polygon[1]), Line(polygon[0], polygon[3])));
		linePairs.push_back(pair<Line, Line>(Line(polygon[0], polygon[2]), Line(polygon[1], polygon[3])));

		Mat H = metricRectification(lineClusters[selectedLines[0].first].getVanishingPoint(),
			lineClusters[selectedLines[1].first].getVanishingPoint(), linePairs);

		vector<Mat> testPoints;
		Point2d bestPoint(0, 0);

		testPoints.push_back((Mat_<double>(3, 1) << 0, 0, 1));
		testPoints.push_back((Mat_<double>(3, 1) << 0, image.rows, 1));
		testPoints.push_back((Mat_<double>(3, 1) << image.cols, 0, 1));
		testPoints.push_back((Mat_<double>(3, 1) << image.cols, image.rows, 1));

		for (int i = 0; i < testPoints.size(); i++) {
			Mat projectedPointMat = H * testPoints[i];
			Point2d projectedPoint(projectedPointMat.at<double>(0) / projectedPointMat.at<double>(2),
				projectedPointMat.at<double>(1) / projectedPointMat.at<double>(2));

			if (projectedPoint.x < bestPoint.x) {
				bestPoint.x = projectedPoint.x;
			}

			if (projectedPoint.y < bestPoint.y) {
				bestPoint.y = projectedPoint.y;
			}
		}

		Mat T = (Mat_<double>(3, 3) <<
			1, 0, -(bestPoint.x),
			0, 1, -(bestPoint.y),
			0, 0, 1);

		cout << "T=" << T << endl;

		//Mat S = (Mat_<double>(3, 3) << 0.2, 0, 0, 0, 0.2, 0, 0, 0, 1);

		Mat testImage;
		warpPerspective(image, testImage, T * H, image.size() * 4);
		imwrite("results\\warpingTest.png", testImage);
		imshow("Warping Test", testImage);
		inKey = waitKey();
	}

	destroyWindow("Warping Test");

	return true;
}

void main_warpingTest(string dataFilename) {
	vector<DataInfo> dataList = loadSetupFile(dataFilename);
	const int numZeros = 3;

	for (const DataInfo &dataInfo : dataList) {
		DataFiles dataFiles(dataInfo, numZeros);

		const Mat image1 = imread(dataFiles.Image1filename);
		const Mat image2 = imread(buildFilename(dataFiles.DataFolder, dataInfo.filePrefix,
			'0', numZeros, dataInfo.secImgIdxBeg, "", dataInfo.imageExtension));
		Mat K = REGION_DETECTION::load3x3Matrix(dataFiles.CalibrationFilename);
		vector<Mat> VD(2);
		
		if (image1.empty()) {
			cout << "Not able to read image file from:\n" << dataFiles.Image1filename << endl;
			system("pause");
			continue;
		}

		if (K.empty()) {
			cout << "Failed to load the calibration matrix from: " << dataFiles.CalibrationFilename << endl;
			system("pause");
			continue;
		}

		VD[0] = REGION_DETECTION::load3x3Matrix(dataFiles.Image1VDfilename);
		VD[1] = REGION_DETECTION::load3x3Matrix(buildFilename(dataFiles.DataFolder,
			dataInfo.filePrefix, '0', numZeros, dataInfo.secImgIdxBeg, VDSufix, dataInfo.dataExtension));

		// Set the rotation matrix
		Mat R = VD[1] * VD[0].inv();
		cout << "R=" << R << endl;

		// Transformate the first image
		Mat H = K * R * K.inv();

		Mat warped;
		warpPerspective(image1, warped, H, image1.size());
		imwrite("warped.png", warped);
		waitKey();
	}
}

Mat metricRectification(const vector<pair<Line, Line>> &pairLines) {
	int numOfPairs = (int)pairLines.size();
	Mat A(numOfPairs, 5, CV_64F);
	Mat b(numOfPairs, 1, CV_64F);
	Mat x;

	for (unsigned int i = 0; i < numOfPairs; i++) {
		Mat l = pairLines[i].first.LineVector;
		Mat m = pairLines[i].second.LineVector;

		cout << "l=" << l.t() << endl;
		
		A.at<double>(i, 0) = l.at<float>(0) * m.at<float>(0);
		A.at<double>(i, 1) = (l.at<float>(0) * m.at<float>(1) + l.at<float>(1) * m.at<float>(0)) / 2.0;
		A.at<double>(i, 2) = l.at<float>(1) * m.at<float>(1);
		A.at<double>(i, 3) = (l.at<float>(0) * m.at<float>(2) + l.at<float>(2) * m.at<float>(0)) / 2.0;
		A.at<double>(i, 4) = (l.at<float>(1) * m.at<float>(2) + l.at<float>(2) * m.at<float>(1)) / 2.0;

		b.at<double>(i, 0) = -l.at<float>(2) * m.at<float>(2);
	}

	solve(A, b, x);

	Mat C = (Mat_<double>(3, 3) << x.at<double>(0), x.at<double>(1) / 2.0, x.at<double>(3) / 2.0,
		x.at<double>(1) / 2.0, x.at<double>(2), x.at<double>(4) / 2.0,
		x.at<double>(3) / 2.0, x.at<double>(4) / 2.0, 1.0);

	// C is symmetirc
	Mat U;		//eigenvector matrix
	Mat E;		//eigenvalues

	eigen(C, E, U);

	assert(E.at<double>(0) * E.at<double>(1) >= 0);

	if (E.at<double>(0) < 0 && E.at<double>(1) < 0) {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				C.at<double>(i, j) *= -1.0;
			}
		}

		eigen(C, E, U);
	}

	Mat ss = Mat::zeros(3, 3, CV_64F);
	ss.at<double>(0, 0) = sqrt(abs(E.at<double>(0)));
	ss.at<double>(1, 1) = sqrt(abs(E.at<double>(1)));
	ss.at<double>(2, 2) = 10;

	double scale = 0.02;
	double angle = 0.0;

	Mat S = (Mat_<double>(3, 3) << scale*cos(angle), -scale*sin(angle), 0,
		scale*sin(angle), scale*cos(angle), 0,
		0, 0, 1);

	return ((U.t() * ss).t() * S).inv();
}

Mat metricRectification(const Point2f &vp1, const Point2f &vp2, vector<pair<Line, Line>> &linePairs) {
	Mat H = Mat::eye(3, 3, CV_64F);

	// Affine rectification
	//const Point2f vp1 = linePairs[0].first.getIntersection(linePairs[1].first);
	//const Point2f vp2 = linePairs[0].second.getIntersection(linePairs[1].second);

	const Vec3d vp1h(vp1.x, vp1.y, 1.);
	const Vec3d vp2h(vp2.x, vp2.y, 1.);

	Vec3d vl1x2 = vp1h.cross(vp2h);

	vl1x2 = normalize(vl1x2);

	H.at<double>(2, 0) = vl1x2[0];
	H.at<double>(2, 1) = vl1x2[1];
	H.at<double>(2, 2) = vl1x2[2];

	// Metric rectification
	Mat l1 = linePairs[0].first.LineVector;
	Mat m1 = linePairs[0].second.LineVector;
	Mat l2 = linePairs[1].first.LineVector;
	Mat m2 = linePairs[1].second.LineVector;

	l1.at<float>(0) /= l1.at<float>(2);
	l1.at<float>(1) /= l1.at<float>(2);
	l2.at<float>(0) /= l2.at<float>(2);
	l2.at<float>(1) /= l2.at<float>(2);
	m1.at<float>(0) /= m1.at<float>(2);
	m1.at<float>(1) /= m1.at<float>(2);
	m2.at<float>(0) /= m2.at<float>(2);
	m2.at<float>(1) /= m2.at<float>(2);

	Mat M = (Mat_<double>(2, 2) << l1.at<float>(0) * m1.at<float>(0),
		l1.at<float>(0) * m1.at<float>(1) + l1.at<float>(1) * m1.at<float>(0),
		l2.at<float>(0) * m2.at<float>(0),
		l2.at<float>(0) * m2.at<float>(1) + l2.at<float>(1) * m2.at<float>(0));
	Mat b = (Mat_<double>(2, 1) << -l1.at<float>(1) * m1.at<float>(1), -l2.at<float>(1) * m2.at<float>(1));

	Mat x;
	solve(M, b, x);

	// Set matrix S
	Mat S = (Mat_<double>(2, 2) << x.at<double>(0), x.at<double>(1), x.at<double>(1), 1.0);

	// SVD S=UDV_T
	Mat s, U, Vt;
	SVDecomp(S, s, U, Vt);

	Mat D = Mat::zeros(2, 2, CV_64F);
	D.at<double>(0, 0) = sqrt(s.at<double>(0));
	D.at<double>(1, 1) = sqrt(s.at<double>(1));

	Mat A = U * D * Vt;
	
	Mat H2 = (Mat_<double>(3, 3) << A.at<double>(0, 0), A.at<double>(0, 1), 0.0,
		A.at<double>(1, 0), A.at<double>(1, 1), 0.0,
		0, 0, 1.0);

	return /*H2.inv() */ H;
}

Mat findRotationalHomography(const pair<Line, Line> &crossingLines, const pair<Point2f, Point2f> &vanishingPoints) {
	//Mat H = K * R * K

	return Mat();
}

void main() {
	int option = 9;
	String testpath = "Images\\";
	vector<String> filenames;
	bool doResize = true;

	if (option == 0) {
		main_visualOdometry("Input\\UCSC.txt");
	}
	// Run the multiple planar fitting algorithm
	else if (option == 1) {
		main_multiPlanarFitting("Input\\MichiganIndoor.txt", 5);
	}
	// Run the image pair calibration sample code
	else if (option == 2) {
		main_imagePairCalibration();
	}
	// Run the individual vanishing point detection and calibration algorithm
	else if (option == 3) {
		glob(testpath, filenames);

		for (String &filename : filenames) {
			Mat image = imread(filename);

			if (image.empty()) {
				continue;
			}

			if (doResize) {
				if (max(image.cols, image.rows) > 1920) {
					//cv::resize(image, image, cv::Size(image.cols / 2, image.rows / 2));
					cv::resize(image, image, cv::Size(1024, 768));
					//cv::resize(image, image, cv::Size(1280, 960));
				}
			}

			Mat K = (Mat_<float>(3, 3) << 800.f, 0, image.cols / 2, 0, 800.f, image.rows / 2, 0, 0, 1);

			if (!vanishingPointDetection(image, K)) {
				break;
			}
		}
	}
	// Run the metric rectification to make the image to be fronto-parallel to the camera image plane.
	else if (option == 4) {
		main_metricRectification();
	}
	else if (option == 5) {
		main_warpingTest("Input\\UCSC.txt");
	}
	// Run the closest lines selection algorithm
	else {
		glob(testpath, filenames);

		for (String &filename : filenames) {
			Mat image = imread(filename);

			if (image.empty()) {
				continue;
			}

			if (doResize) {
				if (max(image.cols, image.rows) > 1920) {
					//cv::resize(image, image, cv::Size(image.cols / 2, image.rows / 2));
					cv::resize(image, image, cv::Size(1024, 768));
					//cv::resize(image, image, cv::Size(1280, 960));
				}
			}

			if (!testRegionGenerator(image, 2.f, 30.f)) {
				break;
			}
		}
	}

	system("pause");
}
