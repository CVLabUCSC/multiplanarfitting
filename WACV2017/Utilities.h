#pragma once

#include <opencv2\opencv.hpp>
#include <vector>
#include <map>

#include "types.h"

namespace PlaneFitting {

	// Get a pointer of given value-type variable
	template<typename T> T* getPointer(T& t) { return &t; }

	// Compute vanishing lines from given calibration matrix K and vanishing
	// directions VD. Each column of VD represents the vanishing direction.
	// Each column of returning matrix will represents a vanishing line on
	// the image.
	cv::Mat computeVanishingLines(const cv::Mat &K, const cv::Mat &VD);

	std::vector<cv::Mat> computeVanishingLines(const std::vector<cv::Mat> &normals,
		const cv::Mat &VL, const cv::Mat &VD);

	// Seperate point matches into two groups using given vanishing line VL, then
	// pick the larger group and return it.
	std::vector<PointPair> pickLargerGroupVL(const std::vector<CDataType*> &pointPairs, const cv::Mat &VL);

	// Seperate point matches into two groups using given vanishing line VL, then
	// pick the larger group and save them to points1_out and points2_out.
	std::vector<PointPair> pickLargerGroupVL(const std::vector<PointPair> &pointPairs, const cv::Mat &VL);

	// Seperate point matches into two groups using given vanishing line VL, then
	// pick the larger group and return it.
	std::vector<PointMatch> pickLargerGroupVL(const std::vector<PointMatch> &pointPairs, const cv::Mat &VL);

	// Separte point matches with given vanishing line VL. Points on one side of
	// vanishing line will be stored to pointsNegGroup1_out, and points on the other
	// side will be stored to pointsPosGroup2_out.
	void separateMatchesVL(const std::vector<CDataType*> &pointPairs, const cv::Mat &VL,
		std::vector<PointPair> &pointsNegGroup1_out, std::vector<PointPair> &pointsPosGroup2_out);

	// Separte point matches with given vanishing line VL. Points on one side of
	// vanishing line will be stored to pointsNegGroup1_out, and points on the other
	// side will be stored to pointsPosGroup2_out.
	void separateMatchesVL(const std::vector<PointPair> &pointPairs, const cv::Mat &VL,
		std::vector<PointPair> &pointsNegGroup1_out, std::vector<PointPair> &pointsPosGroup2_out);

	// Separte point matches with given vanishing line VL. Points on one side of
	// vanishing line will be stored to pointsNegGroup1_out, and points on the other
	// side will be stored to pointsPosGroup2_out.
	void separateMatchesVL(const std::vector<PointMatch> &pointPairs, const cv::Mat &VL,
		std::vector<PointMatch> &pointsNegGroup1_out, std::vector<PointMatch> &pointsPosGroup2_out);

	// Normalize the P so that the last row of P has 1s. Each column of P
	// represents a point in a homogenous coordinate system.
	cv::Mat normalizeHomogeneous(const cv::Mat &P);

	cv::Mat alignRotation(const cv::Mat &refMat, const cv::Mat &tarMat);

	// Get one of the columns of matrix, which has the most similar direction with given vector.
	int getClosestDirectionalColumn(const cv::Mat &vector, const cv::Mat &matrix);

	std::map<uchar, int> findUniqueLabels(const std::vector<CDataType*> pointPairs, const cv::Mat &mask);

	std::map<uchar, int> findUniqueLabels(const cv::Mat &mask);

	cv::Mat separateOMLabels(const cv::Mat &OM);

	//-------------------------------
	// Basic Mathemetics
	//-------------------------------

	// Decompose given matrix m into Q, and R, using QR Decomposition
	template<typename TYPE>
	void QRDecomp(cv::Mat& m, cv::Mat& Q, cv::Mat& R = cv::Mat())
	{
		std::vector<cv::Mat> q(m.rows);
		cv::Mat z = m.clone();

		for (int k = 0; k < m.cols && k < m.rows - 1; k++){
			std::vector<TYPE> e(m.rows, 0);
			std::vector<TYPE> x(m.rows, 0);
			TYPE a = 0;

			cv::Mat z1 = cv::Mat::zeros(z.rows, z.cols, m.type());

			for (int i = 0; i < k; i++){
				z1.at<TYPE>(i, i) = 1;
			}
			for (int y = k; y < z1.rows; y++){
				for (int x = k; x < z1.cols; x++){
					z1.at<TYPE>(y, x) = z.at<TYPE>(y, x);
				}
			}
			z = z1.clone();

			for (int i = 0; i < z.rows; i++){
				a += pow(z.at<TYPE>(i, k), 2);
				x[i] = z.at<TYPE>(i, k);
			}

			a = sqrt(a);
			if (m.at<TYPE>(k, k) > 0){
				a = -a;
			}
			for (int i = 0; i < m.rows; i++){
				if (i == k){
					e[i] = 1;
				}
				else{
					e[i] = 0;
				}
			}

			for (int i = 0; i < m.rows; i++){
				e[i] = x[i] + a * e[i];
			}

			TYPE norm = 0;
			for (int i = 0; i < e.size(); i++){
				norm += pow(e[i], 2);
			}
			norm = sqrt(norm);
			for (int i = 0; i < e.size(); i++){
				if (norm != 0){
					e[i] /= norm;
				}
			}
			cv::Mat E(e.size(), 1, m.type());
			for (int i = 0; i < e.size(); i++){
				E.at<TYPE>(i, 0) = e[i];
			}

			q[k] = cv::Mat::eye(m.rows, m.rows, m.type()) - 2 * E * E.t();
			z1 = q[k] * z;
			z = z1.clone();
		}

		Q = q[0].clone();
		R = q[0] * m;
		for (int i = 1; i < m.cols && i < m.rows - 1; i++){
			Q = q[i] * Q;
		}
		R = Q * m;
		Q = Q.t();

		if (m.rows > m.cols){
			R = R(cv::Rect(0, 0, m.cols, m.cols)).clone();
			Q = Q(cv::Rect(0, 0, m.cols, m.rows)).clone();
		}
	}
}