#pragma once

#include <unordered_map>
#include "PointPairCluster.h"
#include "types.h"

class PointPairClusters {
public:
	const std::vector<PointPairCluster> &Clusters;
	const std::vector<PointMatch> &Outliers;

	PointPairClusters();
	PointPairClusters(const PointPairClusters& pointPairClusters);
	PointPairClusters(const std::vector<PointPairCluster> &pointPairClusters,
		const std::vector<CDataType*> &pointPairs);
	PointPairClusters(const std::vector<PointPairCluster> &pointPairClusters,
		const std::vector<PointMatch> &pointMatches, float outlierThreshold = 0.f);

	size_t size() const;

	int findClusterIndex(const PointMatch &pointMatch) const;

	std::vector<PointMatch> getAllPointMatches() const;

	// Compute inliers using given homgraphy H. If VL is given, it will take the points from
	// one of the two groups of points, that is seperated by the VL.
	PointPairCluster computeInliers(const cv::Mat &H, const cv::Mat &VL = cv::Mat(),
		const cv::Mat &normal = cv::Mat(), const float threshold = 3.f) const;

	std::vector<PointPairCluster> computeInliers(const std::vector<cv::Mat> &H,
		const std::vector<cv::Mat> &VLs = std::vector<cv::Mat>(),
		const std::vector<cv::Mat> &normals = std::vector<cv::Mat>(), const float threshold = 3.f) const;
	
	PointPairClusters& operator=(const PointPairClusters &pointPairCluster);

	// Direct access to each line
	PointPairCluster &operator[](const int index);

	// Direct access to each line for read-only
	const PointPairCluster &operator[](const int index) const;

	const static int OUTLIER_CLUSTER_INDEX = -1;
	const static int POINT_DOES_NOT_EXIST = -2;
private:
	std::vector<PointPairCluster> m_pointPairClusters;
	std::vector<PointMatch> m_outliers;
	std::unordered_map<PointMatch, int, CustomHash> m_indexer;

	inline void updatePointMatches(const std::vector<PointPairCluster> &pointPairClusters);

	inline void updateIndexer();

	void takeOutliers(float threshold);

public:
	// Compute Adjusted Rand Index to measure how much the point matches in given clusters is similar.
	// The outlier point matches in the cluster1 will be ignored.
	static double computeAdjustedRandIndex(const PointPairClusters &clusters1, const PointPairClusters &clusters2);

	static double computeAdjustedRandIndex2(const PointPairClusters &clusters1, const PointPairClusters &clusters2);
};
