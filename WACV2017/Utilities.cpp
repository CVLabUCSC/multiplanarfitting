#include "Utilities.h"


namespace PlaneFitting {
	using namespace std;
	using namespace cv;

	Mat computeVanishingLines(const Mat &K, const Mat &VD)
	{
		Mat VP = K * VD;
		Mat VL(3, 3, CV_64F);

		VP = normalizeHomogeneous(VP);

		VP.col(1).cross(VP.col(2)).copyTo(VL.col(0));
		VP.col(0).cross(VP.col(2)).copyTo(VL.col(1));
		VP.col(0).cross(VP.col(1)).copyTo(VL.col(2));

		return VL;
	}

	vector<Mat> computeVanishingLines(const vector<Mat> &normals,
		const Mat &VL, const Mat &VD) {
		vector<Mat> VLs;

		for (int i = 0; i < normals.size(); i++) {
			VLs.push_back(VL.col(getClosestDirectionalColumn(normals[i], VD)));
		}

		return VLs;
	}

	vector<PointPair> pickLargerGroupVL(const vector<CDataType*> &pointPairs, const Mat &VL) {
		vector<PointPair> pointPairs1, pointPairs2;

		separateMatchesVL(pointPairs, VL, pointPairs1, pointPairs2);

		if (pointPairs1.size() > pointPairs2.size()) {
			return pointPairs1;
		}
		else {
			return pointPairs2;
		}
	}

	vector<PointPair> pickLargerGroupVL(const vector<PointPair> &pointPairs, const Mat &VL) {
		vector<PointPair> pointPairs1, pointPairs2;

		separateMatchesVL(pointPairs, VL, pointPairs1, pointPairs2);

		if (pointPairs1.size() > pointPairs2.size()) {
			return pointPairs1;
		}
		else {
			return pointPairs2;
		}
	}

	vector<PointMatch> pickLargerGroupVL(const vector<PointMatch> &pointPairs, const Mat &VL) {
		vector<PointMatch> pointPairs1, pointPairs2;

		separateMatchesVL(pointPairs, VL, pointPairs1, pointPairs2);

		if (pointPairs1.size() > pointPairs2.size()) {
			return pointPairs1;
		}
		else {
			return pointPairs2;
		}
	}

	void separateMatchesVL(const vector<CDataType*> &pointPairs, const Mat &VL,
		vector<PointPair> &pointsNegGroup1_out, vector<PointPair> &pointsPosGroup2_out) {
		double a = VL.at<double>(0);
		double b = VL.at<double>(1);
		double c = VL.at<double>(2);

		pointsNegGroup1_out.clear();
		pointsPosGroup2_out.clear();

		for (unsigned int i = 0; i < pointPairs.size(); i++) {
			PointPair *pointPair = dynamic_cast<PointPair*>(pointPairs[i]);
			double val = pointPair->Point1.x * a + pointPair->Point1.y * b + c;
			if (val < 0.0) {
				pointsNegGroup1_out.push_back(*pointPair);
			}
			else if (val > 0.0) {
				pointsPosGroup2_out.push_back(*pointPair);
			}
			else {
				pointsNegGroup1_out.push_back(*pointPair);
				pointsPosGroup2_out.push_back(*pointPair);
			}
		}
	}

	void separateMatchesVL(const vector<PointPair> &pointPairs, const Mat &VL,
		vector<PointPair> &pointsNegGroup1_out, vector<PointPair> &pointsPosGroup2_out) {
		double a = VL.at<double>(0);
		double b = VL.at<double>(1);
		double c = VL.at<double>(2);

		pointsNegGroup1_out.clear();
		pointsPosGroup2_out.clear();

		for (unsigned int i = 0; i < pointPairs.size(); i++) {
			double val = pointPairs[i].Point1.x * a + pointPairs[i].Point1.y * b + c;
			if (val < 0.0) {
				pointsNegGroup1_out.push_back(pointPairs[i]);
			}
			else if (val > 0.0) {
				pointsPosGroup2_out.push_back(pointPairs[i]);
			}
			else {
				pointsNegGroup1_out.push_back(pointPairs[i]);
				pointsPosGroup2_out.push_back(pointPairs[i]);
			}
		}
	}

	void separateMatchesVL(const vector<PointMatch> &pointPairs, const Mat &VL,
		vector<PointMatch> &pointsNegGroup1_out, vector<PointMatch> &pointsPosGroup2_out) {
		double a = VL.at<double>(0);
		double b = VL.at<double>(1);
		double c = VL.at<double>(2);

		pointsNegGroup1_out.clear();
		pointsPosGroup2_out.clear();

		for (unsigned int i = 0; i < pointPairs.size(); i++) {
			double val = pointPairs[i].first.x * a + pointPairs[i].first.y * b + c;
			if (val < 0.0) {
				pointsNegGroup1_out.push_back(pointPairs[i]);
			}
			else if (val > 0.0) {
				pointsPosGroup2_out.push_back(pointPairs[i]);
			}
			else {
				pointsNegGroup1_out.push_back(pointPairs[i]);
				pointsPosGroup2_out.push_back(pointPairs[i]);
			}
		}
	}

	Mat normalizeHomogeneous(const Mat &P)
	{
		Mat P_normalized = P.clone();

		for (int i = 0; i < P.cols; i++) {
			P_normalized.at<float>(0, i) /= P_normalized.at<float>(2, i);
			P_normalized.at<float>(1, i) /= P_normalized.at<float>(2, i);
			P_normalized.at<float>(2, i) /= P_normalized.at<float>(2, i);
		}

		return P_normalized;
	}

	Mat alignRotation(const Mat &refMat, const Mat &tarMat) {
		assert(refMat.size() == tarMat.size());

		Mat result(tarMat.size(), tarMat.type());

		for (int ci = 0; ci < refMat.cols; ci++) {
			float maxCos = 0.f;
			int idx = -1;

			for (int cj = 0; cj < tarMat.cols; cj++) {
				float cosVal = abs(refMat.col(ci).dot(tarMat.col(cj)));

				if (cosVal > maxCos) {
					maxCos = cosVal;
					idx = cj;
				}
			}

			tarMat.col(idx).copyTo(result.col(ci));
		}

		for (int ci = 0; ci < refMat.cols; ci++) {
			int maxIdx = 0;
			float maxVal = abs(refMat.at<float>(0, ci));
			for (int ri = 1; ri < refMat.rows; ri++) {
				if (maxVal < abs(refMat.at<float>(ri, ci))) {
					maxVal = abs(refMat.at<float>(ri, ci));
					maxIdx = ri;
				}
			}

			if (refMat.at<float>(maxIdx, ci) * result.at<float>(maxIdx, ci) < 0.) {
				for (int ri = 0; ri < refMat.rows; ri++) {
					result.at<float>(ri, ci) *= -1;
				}
			}
		}

		return result;
	}

	int getClosestDirectionalColumn(const Mat &vector, const Mat &matrix) {
		assert(vector.rows == matrix.rows && vector.cols == 1 && matrix.cols >= 1);

		const Mat normalizedVector = vector / norm(vector);
		double maxCosine = 0;
		int bestColumn = 0;

		for (int i = 0; i < matrix.cols; i++) {
			const Mat normalizedColumn = matrix.col(i) / norm(vector);
			double cosine = abs(normalizedVector.dot(normalizedColumn));

			if (cosine > maxCosine) {
				maxCosine = cosine;
				bestColumn = i;
			}
		}

		return bestColumn;
	}

	map<uchar, int> findUniqueLabels(const vector<CDataType*> pointPairs, const Mat &mask) {
		map<uchar, int> labels;
		int label = 0;

		// Find unique labels
		for (CDataType* data : pointPairs) {
			const PointPair pointPair = *dynamic_cast<PointPair*>(data);
			uchar label_current = (int)mask.at<uchar>(pointPair.Point1);
			if (labels.find(label_current) == labels.end()) {
				if (label_current == 255) {
					labels[label_current] = -1;
				}
				else {
					labels[label_current] = label;
					label++;
				}
			}
		}

		return labels;
	}

	map<uchar, int> findUniqueLabels(const Mat &mask) {
		map<uchar, int> labels;
		int label = 0;

		// Find unique labels
		for (int y = 0; y < mask.cols; y++) {
			for (int x = 0; x < mask.rows; x++) {
				uchar label_current = (int)mask.at<uchar>(x, y);
				if (labels.find(label_current) == labels.end()) {
					if (label_current == 255) {
						labels[label_current] = -1;
					}
					else {
						labels[label_current] = label;
						label++;
					}
				}
			}
		}

		return labels;
	}

	Mat separateOMLabels(const Mat &OM) {
		vector<Mat> labels_dir(3);
		Mat Label = Mat::zeros(OM.size(), CV_8U);
		vector<int> baseLabel = { 0, 100, 200 };
		vector<float> range = { 100.f, 100.f, 40.f };

		if (OM.empty() || OM.type() != CV_8U) {
			cout << "Failed to read correct label file" << endl;
			system("pause");
			return Mat();
		}

		// Take label of each normal
		labels_dir[0] = OM < 100;
		labels_dir[1] = (OM >= 100) & (OM < 200);
		labels_dir[2] = (OM >= 200) & (OM < 250);

		for (int i = 0; i < 3; i++) {
			Mat compLabels(OM.size(), CV_32S);
			int nLabels = connectedComponents(labels_dir[i], compLabels, 8);

			if (nLabels < 1) {
				continue;
			}

			// Compute intensity for each label
			float step = range[i] / (float)nLabels;
			assert(step >= 1);
			for (int r = 0; r < Label.rows; ++r) {
				for (int c = 0; c < Label.cols; ++c) {
					int label = compLabels.at<int>(r, c);
					if (label == 0) {
						continue;
					}
					Label.at<uchar>(r, c) = uchar(baseLabel[i] + (int)(step * label));
				}
			}
		}

		return Label;
	}
}