#pragma once

#include "types.h"
#include "LineClusters.h"

#include <vector>

namespace PlaneFitting {
	// Compute a planar homography with given point pairs.
	// At least four points are required to compute the homography.
	cv::Mat computeHomography(const std::vector<PointPair> &pointPairs);
	cv::Mat findHomography(const std::vector<PointMatch> &pointMatches);

	// Compute a planar homography in a weak-Manhattan world with given point pairs.
	// In the original paper, they used only 2.5 points to compute the homography using
	// the difficient-rank system solver. In here, instead of using 2.5 points, it uses
	// 3 points using least-square for convenience.
	// Therefore, at least three points are required to compute the homography.
	// @params pointPairs The corresponding points between two images
	// @params K1 The calibration matrix of the first points
	// @params K2 The calibration matrix of the second points
	// @params R The rotation between point pairs
	cv::Mat computeWeakMWHomography3pts(const std::vector<PointPair> &pointPairs,
		const cv::Mat &K1, const cv::Mat &K2, const cv::Mat &R);

	// Compute a planar homography in a Manhattan world with given point pairs.
	// At least two points are required to compute the homography.
	cv::Mat computeMWHomography(const std::vector<PointPair> &pointPairs,
		const cv::Mat &K1, const cv::Mat &K2, const cv::Mat &R, const cv::Mat &normal);

	cv::Mat computeMWHomography(const std::vector<PointMatch> &pointPairs,
		const cv::Mat &K1, const cv::Mat &K2, const cv::Mat &R, const cv::Mat &normal, cv::Mat &t_hat = cv::Mat());

	cv::Mat computeMWHomography(const std::vector<cv::Point2f> points1, const std::vector<cv::Point2f> points2,
		const cv::Mat &K1, const cv::Mat &K2, const cv::Mat &R, const cv::Mat &normal);

	cv::Mat computeMWHomography(const cv::Mat &p1, const cv::Mat &p2, const cv::Mat &K1,
		const cv::Mat &K2, const cv::Mat &R, const cv::Mat &normal, cv::Mat &t_hat = cv::Mat());

	//------------------------
	// Distance functions
	//------------------------

	// Point reprojection error computed by given homography.
	double ReprojectionError(const cv::Mat &homography, const PointMatch &pointMatch);

	//------------------------
	// Utilities
	//------------------------

	// Construct matrices of homogeneous points
	void pointPairsToHomogeneousMatrices(const std::vector<PointPair> &pointPairs, cv::Mat &p1, cv::Mat &p2);

	// Construct matrices of homogeneous points
	void pointPairsToHomogeneousMatrices(const std::vector<PointMatch> &pointPairs, cv::Mat &p1, cv::Mat &p2);

	// Normalize each column of given matrix
	inline void normalizeColumnWise(cv::Mat &mat);
}