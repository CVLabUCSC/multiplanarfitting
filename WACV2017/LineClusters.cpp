#include "LineClusters.h"

#include <functional>

using namespace std;
using namespace cv;

//-------------------------------------
// LineClusters class function definitions
//-------------------------------------

LineClusters::LineClusters() : Clusters(m_clusters) {
}

LineClusters::LineClusters(const LineClusters &lineClusters) :
Clusters(m_clusters) {
	*this = lineClusters;
}

LineClusters::LineClusters(const vector<LineCluster> &lineClusters) :
m_clusters(lineClusters), Clusters(m_clusters) {
	constructLineIndexer();
}

void LineClusters::addDummyLines(const Size &imageSize) {
	Point2f centerPoint(imageSize.width / 2, imageSize.height / 2);

	for (LineCluster &lineCluster : m_clusters) {
		Point2f vanishingPoint = lineCluster.getVanishingPoint();
		float minPosCosine = 2.f, minNegCosine = -2.f;
		float cosine;
		Point2f posCornerPoint, negCornerPoint;

		if (vanishingPoint.x >= 0 && vanishingPoint.x < imageSize.width &&
			vanishingPoint.y >= 0 && vanishingPoint.y < imageSize.height) {
			lineCluster.add(Line(Point2f(0, 0), Point2f(imageSize.width, 0), true));
			lineCluster.add(Line(Point2f(imageSize.width, 0), Point2f(imageSize.width, imageSize.height), true));
			lineCluster.add(Line(Point2f(imageSize.width, imageSize.height), Point2f(0, imageSize.height), true));
			lineCluster.add(Line(Point2f(0, imageSize.height), Point2f(0, 0), true));
			continue;
		}

		cosine = computeSignedCosine(vanishingPoint, centerPoint, Point2f(0, 0));
		if (cosine >= 0 && cosine < minPosCosine) {
			minPosCosine = cosine;
			posCornerPoint.x = 0;
			posCornerPoint.y = 0;
		}
		else if (cosine < 0 && cosine > minNegCosine) {
			minNegCosine = cosine;
			negCornerPoint.x = 0;
			negCornerPoint.y = 0;
		}

		cosine = computeSignedCosine(vanishingPoint, centerPoint, Point2f(0, imageSize.height));
		if (cosine >= 0 && cosine < minPosCosine) {
			minPosCosine = cosine;
			posCornerPoint.x = 0;
			posCornerPoint.y = imageSize.height;
		}
		else if (cosine < 0 && cosine > minNegCosine) {
			minNegCosine = cosine;
			negCornerPoint.x = 0;
			negCornerPoint.y = imageSize.height;
		}

		cosine = computeSignedCosine(vanishingPoint, centerPoint, Point2f(imageSize.width, 0));
		if (cosine >= 0 && cosine < minPosCosine) {
			minPosCosine = cosine;
			posCornerPoint.x = imageSize.width;
			posCornerPoint.y = 0;
		}
		else if (cosine < 0 && cosine > minNegCosine) {
			minNegCosine = cosine;
			negCornerPoint.x = imageSize.width;
			negCornerPoint.y = 0;
		}

		cosine = computeSignedCosine(vanishingPoint, centerPoint, Point2f(imageSize.width, imageSize.height));
		if (cosine >= 0 && cosine < minPosCosine) {
			minPosCosine = cosine;
			posCornerPoint.x = imageSize.width;
			posCornerPoint.y = imageSize.height;
		}
		else if (cosine < 0 && cosine > minNegCosine) {
			minNegCosine = cosine;
			negCornerPoint.x = imageSize.width;
			negCornerPoint.y = imageSize.height;
		}

		lineCluster.add(Line(vanishingPoint, posCornerPoint, true));
		lineCluster.add(Line(vanishingPoint, negCornerPoint, true));
	}
}

void LineClusters::removeDummyLines() {
	for (LineCluster &lineCluster : m_clusters) {
		lineCluster.removeDummyLines();
	}
}

void LineClusters::addLineCluster(const LineCluster &lineCluster) {
	int clusterIndex = (int)m_clusters.size();

	m_clusters.push_back(lineCluster);

	for (int li = 0; li < (int)lineCluster.size(); li++) {
		m_lineIndexer[lineCluster[li]] = clusterIndex;
	}
}

void LineClusters::addLine(const Line& l) {
	if (m_lineIndexer.find(l) == m_lineIndexer.end()) {
		m_outlierLines.push_back(l);
		m_lineIndexer[l] = -1;
	}
}

void LineClusters::addLines(const vector<Line> &lines) {
	for (const Line &l : lines) {
		if (m_lineIndexer.find(l) == m_lineIndexer.end()) {
			m_outlierLines.push_back(l);
			m_lineIndexer[l] = -1;
		}
	}
}

void LineClusters::clear() {
	m_clusters.clear();
	m_lineIndexer.clear();
}

void LineClusters::sort() {
	std::sort(m_clusters.begin(), m_clusters.end(), sortClusterByLines);
	constructLineIndexer();
}

size_t LineClusters::size() const {
	return m_clusters.size();
}

int LineClusters::computeCardinality(cv::Point2f vanishingPoint, float threshold, bool fromOutliers) const {
	int numOfInliers = 0;
	
	for (unordered_map<Line, int, LineHash>::const_iterator citer = m_lineIndexer.cbegin();
		citer != m_lineIndexer.cend(); citer++) {
		if (citer->second < 0) {
			float dist = Line::LineDistance(vanishingPoint, citer->first);

			if (dist <= threshold) {
				numOfInliers++;
			}
		}
	}

	return numOfInliers;
}

LineClusters LineClusters::subCluster(const vector<int> &selectedIndices,
	const float threshold, const bool recomputeInliers) const {
	LineClusters lineClusters;

	// Validations
	if (selectedIndices.size() == 0) {
		return lineClusters;
	}

	if (recomputeInliers) {
		vector<Point2f> VPs;
		vector<vector<Line>> vecLineCluster;
		vector<Line> outliers;

		for (int ci = 0; ci < (int)selectedIndices.size(); ci++) {
			VPs.push_back(LineCluster::computeVanishingPoint(m_clusters[selectedIndices[ci]].Lines));
		}

		vecLineCluster.resize(VPs.size());

		// For each line, choose one model with minimum error
		for (unordered_map<Line, int, LineHash>::const_iterator citer = m_lineIndexer.cbegin();
			citer != m_lineIndexer.cend(); citer++) {
			int bestIndex = -1;
			float minDist = threshold;

			for (int vpi = 0; vpi < (int)VPs.size(); vpi++) {
				float dist = Line::LineDistance(VPs[vpi], citer->first);
				if (dist < minDist) {
					minDist = dist;
					bestIndex = vpi;
				}
			}

			if (bestIndex >= 0) {
				vecLineCluster[bestIndex].push_back(citer->first);
			}
			else {
				outliers.push_back(citer->first);
			}
		}

		for (const vector<Line> &lineCluster : vecLineCluster) {
			lineClusters.addLineCluster(lineCluster);
		}

		lineClusters.addLines(outliers);
	}
	else {
		for (const int idx : selectedIndices) {
			lineClusters.addLineCluster(m_clusters[idx]);
		}
	}

	return lineClusters;
}

void LineClusters::findClosestVerticalLine(const cv::Point2f &point, int &lineIndex, int &VPIndex) {
	vector<int> clusterIndices, sortedIndices, VPIndices;
	vector<float> distances;
	Mat lineNormal;
	Point2f crossPoint;
	float dist;

	for (unsigned int ci = 0; ci < m_clusters.size(); ci++) {
		// Search the closest line over all groups of vertical lines
		if (m_clusters[ci].isVertical()) {
			// Initialize
			distances.resize(m_clusters[ci].size(), FLT_MAX);
			clusterIndices.resize(m_clusters[ci].size(), -1);

			for (unsigned int li = 0; li < m_clusters[ci].size(); li++) {
				for (unsigned int cj = 0; cj < m_clusters.size(); cj++) {
					// compute distance using vanishing point of each cluster
					if (ci != cj) {
						lineNormal = Line::computeLineNormal(point, m_clusters[cj].getVanishingPoint());
						crossPoint = m_clusters[ci][li].getIntersection(lineNormal);

						if (crossPoint.x >= min(m_clusters[ci][li].StartPoint.x, m_clusters[ci][li].EndPoint.x) &&
							crossPoint.x <= max(m_clusters[ci][li].StartPoint.x, m_clusters[ci][li].EndPoint.x) &&
							crossPoint.y >= min(m_clusters[ci][li].StartPoint.y, m_clusters[ci][li].EndPoint.y) &&
							crossPoint.y <= max(m_clusters[ci][li].StartPoint.y, m_clusters[ci][li].EndPoint.y)) {

							dist = (float)cv::norm(crossPoint - point);
							if (dist < distances[li]) {
								distances[li] = dist;
								clusterIndices[li] = ci;
								VPIndex = cj;
							}
						}
					}
				}
			}
			break;
		}
	}

	sortIdx(distances, sortedIndices, SORT_ASCENDING);

	lineIndex = sortedIndices[0];
}

void LineClusters::findClosestLine(const cv::Point2f &point, int &clusterIndex, int &lineIndex, int &VPIndex) {
	float closestDistance = FLT_MAX;

	for (unsigned int ci = 0; ci < m_clusters.size(); ci++) {
		for (unsigned int li = 0; li < m_clusters[ci].size(); li++) {
			for (unsigned int cj = 0; cj < m_clusters.size(); cj++) {
				// compute distance using vanishing point of each cluster
				if (ci != cj) {
					Mat lineNormal = Line::computeLineNormal(point, m_clusters[cj].getVanishingPoint());
					Point2f crossPoint = m_clusters[ci][li].getIntersection(lineNormal);

					if (crossPoint.x >= min(m_clusters[ci][li].StartPoint.x, m_clusters[ci][li].EndPoint.x) &&
						crossPoint.x <= max(m_clusters[ci][li].StartPoint.x, m_clusters[ci][li].EndPoint.x) &&
						crossPoint.y >= min(m_clusters[ci][li].StartPoint.y, m_clusters[ci][li].EndPoint.y) &&
						crossPoint.y <= max(m_clusters[ci][li].StartPoint.y, m_clusters[ci][li].EndPoint.y)) {

						float dist = (float)cv::norm(crossPoint - point);
						if (dist < closestDistance) {
							closestDistance = dist;
							clusterIndex = ci;
							lineIndex = li;
							VPIndex = cj;
						}
					}
				}
			}
		}
	}
}

vector<Point> LineClusters::findRegion(const Point2f &centerPoint,
	ClusterLineIndices &clusterLineIndices, ClusterLineIndices &pairLineIndices) {
	return findRegion(centerPoint, Rect(), clusterLineIndices, pairLineIndices);
}

vector<Point> LineClusters::findRegion(const Point2f &centerPoint, const Rect &region,
	ClusterLineIndices &clusterLineIndices, ClusterLineIndices &pairLineIndices) {

	// Find the closest line and its group
	findClosestLine(centerPoint, clusterLineIndices.ClusterIndex, clusterLineIndices.Line1Index, region);

	// If line was not found, exit the function
	if (clusterLineIndices.ClusterIndex < 0) {
		cout << "Failed to find the line" << endl;
		return vector<Point>(4);
	}

	const Line &group1Line1 = m_clusters[clusterLineIndices.ClusterIndex][clusterLineIndices.Line1Index];
	const Point2f vanishingPoint1 = m_clusters[clusterLineIndices.ClusterIndex].getVanishingPoint();

	// Find the closest line in one of the other clusters
	findClosestLineBetween(centerPoint, clusterLineIndices.ClusterIndex,
		Line(vanishingPoint1, centerPoint), group1Line1,
		pairLineIndices.ClusterIndex, pairLineIndices.Line1Index, region);

	// If line was not found, exit the function
	if (pairLineIndices.Line1Index < 0) {
		cout << "Failed to find the line" << endl;
		return vector<Point>(4);
	}

	// Find the pair line of the first line group
	clusterLineIndices.Line2Index = findClosestLineOpposite(centerPoint, clusterLineIndices.ClusterIndex,
		clusterLineIndices.Line1Index, pairLineIndices.ClusterIndex, pairLineIndices.Line1Index, region);

	if (clusterLineIndices.Line2Index < 0) {
		cout << "Failed to find the line" << endl;
	}

	// If line was not found, find a line in the second cluster instead.
	if (clusterLineIndices.Line2Index < 0) {
		pairLineIndices.Line2Index = findClosestLineOpposite(centerPoint, pairLineIndices.ClusterIndex,
			pairLineIndices.Line1Index, clusterLineIndices.ClusterIndex, clusterLineIndices.Line1Index, region);
	}
	else {
		const Line &group1Line2 = m_clusters[clusterLineIndices.ClusterIndex][clusterLineIndices.Line2Index];
		pairLineIndices.Line2Index = m_clusters[pairLineIndices.ClusterIndex].findClosestLineOpposite(centerPoint,
			vanishingPoint1, pairLineIndices.Line1Index, group1Line1, group1Line2, region);
	}

	if (pairLineIndices.Line2Index < 0) {
		cout << "Failed to find the line" << endl;
	}

	return computePolynomial(centerPoint, clusterLineIndices, pairLineIndices);
}

bool LineClusters::findRegion(const Point2f &centerPoint, const Size &imageSize, vector<Point2f> &points,
	const float minLineLength,
	ClusterLineIndices &clusterLineIndices, ClusterLineIndices &pairLineIndices) const {
	int primeClusterIndex = -1;
	int secondaryClusterIndex = -1;
	const int numOfClusters = (int)m_clusters.size();
	vector<Mat> centerLines;
	float minDistance = FLT_MAX;

	// Initialization
	clusterLineIndices.ClusterIndex = -1;
	clusterLineIndices.Line1Index = -1;
	clusterLineIndices.Line2Index = -1;
	pairLineIndices.ClusterIndex = -1;
	pairLineIndices.Line1Index = -1;
	pairLineIndices.Line2Index = -1;

	// lines connecting centerPoint and vanishing points of each cluster
	for (int ci = 0; ci < numOfClusters; ci++) {
		centerLines.push_back(Line::computeLineNormal(m_clusters[ci].getVanishingPoint(), centerPoint));
	}

	// Iterate for each cluster to find the best line pair and their cluster index
	for (int ci = 0; ci < numOfClusters; ci++) {
		int centerIndex = -1;
		Point2f vanishingPoint = m_clusters[ci].getVanishingPoint();
		vector<float> cosines;
		vector<int> cosineIndices;
		const LineCluster &currentCluster = m_clusters[ci];

		// compute signed cosines of each line with respect to the center separating line
		for (unsigned int li = 0; li < currentCluster.size(); li++) {
			cosines.push_back(computeSignedCosine(vanishingPoint, centerPoint, currentCluster[li].EndPoint));

			if (cosines.back() < 0) {
				cosines.back() = -1.f - cosines.back();
			}
			else {
				cosines.back() = 1.f - cosines.back();
			}
		}

		// Sort cosine values in ascending order
		sortIdx(cosines, cosineIndices, SORT_ASCENDING);

		// Assumed that zero value does not exist
		for (unsigned int i = 1; i < cosineIndices.size(); i++) {
			if (cosines[cosineIndices[i - 1]] * cosines[cosineIndices[i]] < 0) {
				centerIndex = i;
				break;
			}
		}

		// If no dummy lines added, it will throw an exception
		//assert(centerIndex >= 0);

		// iterate for each pair of lines until it process meets criteria,
		// where one line is from the positive set, and the other line is from the negative set.
		for (int i = centerIndex - 1; i >= 0; i--) {
			const Line &testLine1 = currentCluster[cosineIndices[i]];
			// Dummy line cannot be a base line
			if (testLine1.length() < minLineLength ||
				!testLine1.isProjectable(centerPoint) ||
				testLine1.isDummyLine()) {
				continue;
			}

			for (int j = centerIndex; j < currentCluster.size(); j++) {
				const Line &testLine2 = currentCluster[cosineIndices[j]];
				for (int cj = 0; cj < numOfClusters; cj++) {
					Point2f VP4Dist = m_clusters[cj].getVanishingPoint();
					if (ci != cj) {
						if (testLine2.length() < minLineLength ||
							!testLine2.isProjectable(centerPoint)
							/*!testLine2.isBetween(Line(VP4Dist, testLine1.StartPoint), Line(VP4Dist, testLine1.EndPoint))*/) {
							continue;
						}

						Point2f cross1 = testLine1.getIntersection(centerLines[cj]);
						Point2f cross2 = testLine2.getIntersection(centerLines[cj]);

						if (!testLine1.isPointOnLine(cross1)) {
							continue;
						}

						// compute distance
						float dist = norm(cross1 - centerPoint) + norm(cross2 - centerPoint);

						if (dist < minDistance) {
							vector<Point> contour;
							contour.push_back(testLine1.StartPoint);
							contour.push_back(testLine1.EndPoint);
							contour.push_back(testLine2.EndPoint);
							contour.push_back(testLine2.StartPoint);

							// Center point must be inside the contour
							if (pointPolygonTest(contour, centerPoint, false) >= 0) {
								minDistance = dist;
								clusterLineIndices.ClusterIndex = ci;
								clusterLineIndices.Line1Index = cosineIndices[i];
								clusterLineIndices.Line2Index = cosineIndices[j];
								pairLineIndices.ClusterIndex = cj;
							}
						}
					}
				}
			}
		}
	}

	// Failed to find pair of lines
	if (clusterLineIndices.ClusterIndex < 0) {
		return false;
	}

	// Find the closest line of different cluster
	findClosestLineBetween(centerPoint, clusterLineIndices, pairLineIndices);
}

//-------------------------------------
// Overloaded operators
//-------------------------------------

LineCluster &LineClusters::operator[](const int index) {
	return m_clusters[index];
}

const LineCluster &LineClusters::operator[](const int index) const {
	return m_clusters[index];
}

// Overload operator
LineClusters& LineClusters::operator = (const LineClusters &lineClusters) {
	m_clusters = lineClusters.Clusters;
	m_lineIndexer = lineClusters.m_lineIndexer;
	m_outlierLines = lineClusters.m_outlierLines;

	return *this;
}

//-------------------------------------
// Priavate functions
//-------------------------------------

// Construct line indexer.
void LineClusters::constructLineIndexer() {
	m_lineIndexer.clear();

	for (int ci = 0; ci < (int)m_clusters.size(); ci++) {
		for (int li = 0; li < m_clusters[ci].size(); li++) {
			m_lineIndexer[m_clusters[ci][li]] = ci;
		}
	}

	for (const Line& l : m_outlierLines) {
		m_lineIndexer[l] = -1;
	}
}

void LineClusters::findClosestLine(const Point2f &centerPoint, int &clusterIdx, int &lineIdx, Rect exclude) {
	const int numOfClusters = m_clusters.size();	// the number of clusters
	float minDistance = FLT_MAX;					// store the closest distance

	for (int ci = 0; ci < numOfClusters; ci++) {
		for (int cj = 0; cj < numOfClusters; cj++) {
			if (ci == cj) {
				continue;
			}

			int lineIndex;

			if (m_clusters[ci].findClosestLineFrom(centerPoint,
				m_clusters[cj].getVanishingPoint(), minDistance, lineIndex, exclude)) {
				clusterIdx = ci;
				lineIdx = lineIndex;
			}
		}
	}
}

int LineClusters::findClosestLineOpposite(const cv::Point2f &centerPoint, const int clusterIndex, const int lineIndex,
	const int supClusterIndex, const int supLineIndex, Rect exclude) {
	const LineCluster &sourceCluster = m_clusters[clusterIndex];
	const Line &baseLine = sourceCluster[lineIndex];
	const Point2f supVP = m_clusters[supClusterIndex].getVanishingPoint();

	const Line &consLine1 = m_clusters[supClusterIndex][supLineIndex];

	float supCosine = computeSignedCosine(supVP, centerPoint, consLine1.EndPoint);
	float supSubCosine1 = computeSignedCosine(supVP, centerPoint, baseLine.StartPoint);
	float supSubCosine2 = computeSignedCosine(supVP, centerPoint, baseLine.EndPoint);

	//assert(supCosine * supSubCosine1 < 0 || supCosine * supSubCosine2 < 0);

	const Line consLine2(supVP, supCosine * supSubCosine1 < 0 ? baseLine.StartPoint : baseLine.EndPoint);

	return sourceCluster.findClosestLineOpposite(centerPoint, supVP, lineIndex, consLine1, consLine2, exclude);
}

void LineClusters::findClosestLineBetween(const Point2f &centerPoint, const int VDClusterIdx, const Line& line1,
	const Line& line2, int &clusterIdx, int &lineIdx, Rect exclude) {
	Point2f vanishingPoint = m_clusters[VDClusterIdx].getVanishingPoint();
	float minDist = FLT_MAX;

	for (int ci = 0; ci < (int)m_clusters.size(); ci++) {
		if (ci != VDClusterIdx) {
			int lineIndex;

			if (m_clusters[ci].findClosestLineBetween(centerPoint, vanishingPoint, line1, line2,
				minDist, lineIndex, exclude)) {
				clusterIdx = ci;
				lineIdx = lineIndex;
			}
		}
	}
}

void LineClusters::findClosestLine(const Point2f &point, ClusterLineIndices &clusterLineIndices,
	Mat &crossLineNormal, Point2f &intersectPoint) const {
	const int numOfClusters = m_clusters.size();
	float closestDistance = FLT_MAX;

	// Find the closest line
	for (unsigned int ci = 0; ci < numOfClusters; ci++) {
		const int numOfLines = m_clusters[ci].size();
		for (int li = 0; li < numOfLines; li++) {
			// Does not care dummy lines
			if (m_clusters[ci][li].isDummyLine()) {
				continue;
			}

			for (int cj = 0; cj < numOfClusters; cj++) {
				// compute distance using vanishing point of each cluster
				if (ci != cj) {
					Mat lineNormal = Line::computeLineNormal(point, m_clusters[cj].getVanishingPoint());
					Point2f crossPoint = m_clusters[ci][li].getIntersection(lineNormal);

					if (crossPoint.x >= min(m_clusters[ci][li].StartPoint.x, m_clusters[ci][li].EndPoint.x) &&
						crossPoint.x <= max(m_clusters[ci][li].StartPoint.x, m_clusters[ci][li].EndPoint.x) &&
						crossPoint.y >= min(m_clusters[ci][li].StartPoint.y, m_clusters[ci][li].EndPoint.y) &&
						crossPoint.y <= max(m_clusters[ci][li].StartPoint.y, m_clusters[ci][li].EndPoint.y)) {

						float dist = (float)cv::norm(crossPoint - point);
						if (dist < closestDistance) {
							closestDistance = dist;
							clusterLineIndices.ClusterIndex = ci;
							clusterLineIndices.Line1Index = li;
							crossLineNormal = lineNormal;
							intersectPoint = crossPoint;
						}
					}
				}
			}
		}
	}
}

void LineClusters::findClosestOppositeLine(const cv::Point2f &point, ClusterLineIndices &clusterLineIndices,
	const Mat &crossLineNormal, const Point2f &intersectPoint) const {
	Point2f VP = m_clusters[clusterLineIndices.ClusterIndex].getVanishingPoint();
	float closestDistance = FLT_MAX;
	float signOfClosestLine = (VP.x - point.x)*(intersectPoint.y - point.y) -
		(VP.y - point.y)*(intersectPoint.x - point.x);
	const LineCluster &lineCluster = m_clusters[clusterLineIndices.ClusterIndex];

	// Find the second closest line on the same group, which is on the other side of the point.
	for (unsigned int li = 0; li < lineCluster.size(); li++) {
		if (li != clusterLineIndices.Line1Index) {
			Point2f crossPoint = lineCluster[li].getIntersection(crossLineNormal);
			float sign = (VP.x - point.x)*(crossPoint.y - point.y) - (VP.y - point.y)*(crossPoint.x - point.x);
			const Line &currentLine = lineCluster[li];

			if (signOfClosestLine * sign < 0) {
				if (closestDistance == FLT_MAX && currentLine.isDummyLine()) {
					clusterLineIndices.Line2Index = li;
					continue;
				}

				if (crossPoint.x >= min(currentLine.StartPoint.x, currentLine.EndPoint.x) &&
					crossPoint.x <= max(currentLine.StartPoint.x, currentLine.EndPoint.x) &&
					crossPoint.y >= min(currentLine.StartPoint.y, currentLine.EndPoint.y) &&
					crossPoint.y <= max(currentLine.StartPoint.y, currentLine.EndPoint.y)) {
					float dist = (float)cv::norm(crossPoint - point);
					if (dist < closestDistance) {
						closestDistance = dist;
						clusterLineIndices.Line2Index = li;
					}
				}
			}
		}
	}
}

void LineClusters::findClosestLineBetween(const cv::Point2f &point,
	const ClusterLineIndices &clusterLineIndices, ClusterLineIndices &pairLineIndices) const {
	float minLineDistance = FLT_MAX;
	const int numOfClusters = m_clusters.size();
	const Line &line1 = m_clusters[clusterLineIndices.ClusterIndex][clusterLineIndices.Line1Index];
	const Line &line2 = m_clusters[clusterLineIndices.ClusterIndex][clusterLineIndices.Line2Index];
	const Point2f inputVP = m_clusters[clusterLineIndices.ClusterIndex].getVanishingPoint();

	for (int ci = 0; ci < numOfClusters; ci++) {
		if (ci == clusterLineIndices.ClusterIndex) {
			continue;
		}

		const LineCluster &lineCluster = m_clusters[ci];
		const int numOfLines = lineCluster.size();
		for (int li = 0; li < numOfLines; li++) {
			float dist = FLT_MAX;
			const Line &currentLine = lineCluster[li];
			if (currentLine.isBetween(line1, line2)) {
				if (lineCluster[li].isDummyLine()) {
					dist = (FLT_MAX / 2.f);
				}
				else {
					Mat lineNormal = Line::computeLineNormal(point, inputVP);
					Point2f crossPoint = currentLine.getIntersection(lineNormal);

					// Check if the point is projectable
					//dist = (float)cv::norm(crossPoint - point);
					if (crossPoint.x >= min(currentLine.StartPoint.x, currentLine.EndPoint.x) &&
						crossPoint.x <= max(currentLine.StartPoint.x, currentLine.EndPoint.x) &&
						crossPoint.y >= min(currentLine.StartPoint.y, currentLine.EndPoint.y) &&
						crossPoint.y <= max(currentLine.StartPoint.y, currentLine.EndPoint.y)) {
						dist = (float)cv::norm(crossPoint - point);
					}
				}

				if (dist < minLineDistance) {
					minLineDistance = dist;
					pairLineIndices.ClusterIndex = ci;
					pairLineIndices.Line1Index = li;
				}
			}
		}
	}
}

vector<Point> LineClusters::findRegion(const Point2f &point, const Size imageSize,
	const ClusterLineIndices &clusterLineIndices, ClusterLineIndices &pairLineIndices) const {
	// Definitions part 1
	const LineCluster &lineCluster = m_clusters[pairLineIndices.ClusterIndex];
	const Line &baseLine = lineCluster[pairLineIndices.Line1Index];
	const int numberOfLines = lineCluster.size();
	const Point2f vanishinigPoint = m_clusters[pairLineIndices.ClusterIndex].getVanishingPoint();
	multimap<float, int, std::greater<float>> cosines;
	float cosineOfInput = computeSignedCosine(vanishinigPoint, point, baseLine.StartPoint);
	Mat occupiedPixels = Mat::zeros(imageSize, CV_8U);
	Mat element = getStructuringElement(MORPH_RECT, Size(5, 5), Point(2, 2));
	const Line &lineLimit1 = m_clusters[clusterLineIndices.ClusterIndex][clusterLineIndices.Line1Index];
	const Line &lineLimit2 = m_clusters[clusterLineIndices.ClusterIndex][clusterLineIndices.Line2Index];
	const float gapAllowance = 5.f;

	// Compute cosines for each line
	for (int li = 0; li < numberOfLines; li++) {
		if (li != pairLineIndices.Line1Index && lineCluster[li].isBetween(lineLimit1, lineLimit2)
			&& !lineCluster[li].isDummyLine()) {
			// compute cosine
			float cosine = computeSignedCosine(vanishinigPoint, point, lineCluster[li].StartPoint);

			if (cosineOfInput * cosine < 0) {	// on the opposite side
				cosines.insert(pair<float, int>(abs(cosine), li));
			}
		}
	}

	// Draw pixels occupied by line in other clusters
	for (int ci = 0; ci < m_clusters.size(); ci++) {
		if (ci != pairLineIndices.ClusterIndex) {
			for (int li = 0; li < m_clusters[ci].size(); li++) {
				line(occupiedPixels, m_clusters[ci][li].StartPoint, m_clusters[ci][li].EndPoint, CV_RGB(255, 255, 255));
			}
		}
	}

	// Definitions 2
	vector<vector<Point>> region;
	Point2f point1 = baseLine.getIntersection(lineLimit1);
	Point2f point2 = baseLine.getIntersection(lineLimit2);
	float distLimit1 = max(norm(point1 - lineLimit1.StartPoint), norm(point1 - lineLimit1.EndPoint)) + gapAllowance;
	float distLimit2 = max(norm(point2 - lineLimit2.StartPoint), norm(point2 - lineLimit2.EndPoint)) + gapAllowance;
	Mat chosenArea = Mat::zeros(occupiedPixels.size(), CV_8U);

	// Initialization
	region.push_back(computeMaximumRegion(baseLine, vanishinigPoint, point, lineLimit1, lineLimit2));

	/*for (multimap<float, int, std::greater<float>>::const_iterator citer = cosines.cbegin();
	citer != cosines.cend(); citer++) {

	Point2f tempPoint2 = lineLimit2.getIntersection(lineCluster[citer->second]);
	Point2f tempPoint3 = lineLimit1.getIntersection(lineCluster[citer->second]);

	if (norm(region[0][0] - region[0][3]) > distLimit1 && norm(region[0][1] - region[0][2]) > distLimit2) {
	break;
	}

	cv::fillPoly(chosenArea, region, CV_RGB(255, 255, 255));
	cv::erode(chosenArea, chosenArea, element);

	bitwise_and(occupiedPixels, chosenArea, chosenArea);

	if (countNonZero(chosenArea) > 0) {
	break;
	}

	region[0][2] = tempPoint2;
	region[0][3] = tempPoint3;
	}*/

	return region[0];
}

vector<Point> LineClusters::computePolynomial(const Point2f centerPoint,
	const ClusterLineIndices &clusterLineIndices, const ClusterLineIndices &pairLineIndices) const {
	vector<Point> polygon(4);
	const LineCluster &cluster1 = m_clusters[clusterLineIndices.ClusterIndex];
	const LineCluster &cluster2 = m_clusters[pairLineIndices.ClusterIndex];

	const Line &group1Line1 = cluster1[clusterLineIndices.Line1Index];
	const Line &group2Line1 = cluster2[pairLineIndices.Line1Index];

	if (clusterLineIndices.Line2Index < 0 && pairLineIndices.Line2Index < 0) {
		Point2f crossPoint = group2Line1.getIntersection(group1Line1);
		Point2f group1Line2_StartPoint, group2Line2_StartPoint;

		if (norm(crossPoint - group2Line1.StartPoint) > norm(crossPoint - group2Line1.EndPoint)) {
			group1Line2_StartPoint = group2Line1.StartPoint;
		}
		else {
			group1Line2_StartPoint = group2Line1.EndPoint;
		}

		if (norm(crossPoint - group1Line1.StartPoint) > norm(crossPoint - group1Line1.EndPoint)) {
			group2Line2_StartPoint = group1Line1.StartPoint;
		}
		else {
			group2Line2_StartPoint = group1Line1.EndPoint;
		}

		const Line group1Line2(group1Line2_StartPoint, cluster1.getVanishingPoint());
		const Line group2Line2(group2Line2_StartPoint, cluster2.getVanishingPoint());

		polygon[0] = group1Line1.getIntersection(group2Line1);
		polygon[1] = group2Line1.getIntersection(group1Line2);
		polygon[2] = group1Line2.getIntersection(group2Line2);
		polygon[3] = group2Line2.getIntersection(group1Line1);
	}
	else if (pairLineIndices.Line2Index < 0) {
		// Found three lines (two lines of the first group and one line of the pair group)
		const Line &group1Line2 = cluster1[clusterLineIndices.Line2Index];
		const Line &group2Line2 = computeIncompleteLine(centerPoint, clusterLineIndices, pairLineIndices);

		polygon[0] = group1Line1.getIntersection(group2Line1);
		polygon[1] = group2Line1.getIntersection(group1Line2);
		polygon[2] = group1Line2.getIntersection(group2Line2);
		polygon[3] = group2Line2.getIntersection(group1Line1);
	}
	else if (clusterLineIndices.Line2Index < 0) {
		// Found three lines (one line of the first group and two lines of the pair group)
		const Line &group2Line2 = cluster2[pairLineIndices.Line2Index];
		const Line &group1Line2 = computeIncompleteLine(centerPoint, pairLineIndices, clusterLineIndices);

		polygon[0] = group1Line1.getIntersection(group2Line1);
		polygon[1] = group2Line1.getIntersection(group1Line2);
		polygon[2] = group1Line2.getIntersection(group2Line2);
		polygon[3] = group2Line2.getIntersection(group1Line1);
	}
	else {
		// Found all of four lines
		const Line &group1Line2 = cluster1[clusterLineIndices.Line2Index];
		const Line &group2Line2 = cluster2[pairLineIndices.Line2Index];

		polygon[0] = group1Line1.getIntersection(group2Line1);
		polygon[1] = group2Line1.getIntersection(group1Line2);
		polygon[2] = group1Line2.getIntersection(group2Line2);
		polygon[3] = group2Line2.getIntersection(group1Line1);
	}

	return polygon;
}

Line LineClusters::computeIncompleteLine(const Point2f centerPoint,
	const ClusterLineIndices &compLineIndices, const ClusterLineIndices &imcompLineIndices) const {
	const LineCluster &cluster1 = m_clusters[compLineIndices.ClusterIndex];
	const LineCluster &cluster2 = m_clusters[imcompLineIndices.ClusterIndex];

	const Line &group1Line2 = cluster1[compLineIndices.Line2Index];
	vector<Line> lines;
	vector<float> cosines(4);

	Point2f vanishingPoint = cluster2.getVanishingPoint();

	lines.push_back(Line(vanishingPoint, cluster1[compLineIndices.Line1Index].StartPoint));
	lines.push_back(Line(vanishingPoint, cluster1[compLineIndices.Line1Index].EndPoint));
	lines.push_back(Line(vanishingPoint, cluster1[compLineIndices.Line2Index].StartPoint));
	lines.push_back(Line(vanishingPoint, cluster1[compLineIndices.Line2Index].EndPoint));

	int bestLineIndex = -1;
	float minCosine = 1.f;
	float cosineFixedLine = computeSignedCosine(vanishingPoint, centerPoint,
		cluster2[imcompLineIndices.Line1Index].EndPoint);

	for (int i = 0; i < 4; i++) {
		float cosine = computeSignedCosine(vanishingPoint, centerPoint, lines[i].EndPoint);
		if (cosine * cosineFixedLine < 0 && abs(cosine) < minCosine) {
			minCosine = abs(cosine);
			bestLineIndex = i;
		}
	}

	return lines[bestLineIndex];
}

vector<Point> computeMaximumRegion(const Line &baseLine, const Point2f &vanishingPoint,
	const Point2f &centerPoint, const Line &line1, const Line &line2) {
	Point2f point1 = baseLine.getIntersection(line1);
	Point2f point2 = baseLine.getIntersection(line2);
	vector<Point> tetraton(4);
	Line separator(vanishingPoint, centerPoint);
	Point2f pt1, pt2;

	// Initialization
	tetraton[0] = point1;
	tetraton[1] = point2;

	float sign = separator.computeSign(baseLine.StartPoint);

	// Choose a point in desired side
	if (separator.computeSign(line1.StartPoint) * sign < 0) {
		pt1 = line1.StartPoint;
	}
	else if (separator.computeSign(line1.EndPoint) * sign < 0) {
		pt1 = line1.EndPoint;
	}
	else {
		pt1 = point1;
	}

	if (separator.computeSign(line2.StartPoint) * sign < 0) {
		pt2 = line2.StartPoint;
	}
	else if (separator.computeSign(line2.EndPoint) * sign < 0) {
		pt2 = line2.EndPoint;
	}
	else {
		pt2 = point2;
	}

	if (norm(pt1 - point1) > norm(pt2 - point2)) {
		tetraton[2] = line2.getIntersection(Line(vanishingPoint, pt1));
		tetraton[3] = line1.getIntersection(Line(vanishingPoint, pt1));
	}
	else {
		tetraton[2] = line2.getIntersection(Line(vanishingPoint, pt2));
		tetraton[3] = line1.getIntersection(Line(vanishingPoint, pt2));
	}

	return tetraton;
}

//-------------------------------------
// Cluster Sorting
//-------------------------------------

bool sortClusterByPoints(const vector<pair<Point2f, Point2f>> &left,
	const vector<pair<Point2f, Point2f>> &right) {
	return left.size() > right.size();
}

bool sortClusterByLines(const LineCluster &left, const LineCluster &right) {
	return left.Lines.size() > right.Lines.size();
}
