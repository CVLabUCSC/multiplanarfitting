#include "LineCluster.h"

using namespace std;
using namespace cv;


//-------------------------------------
// LineCluster class function definitions
//-------------------------------------

LineCluster::LineCluster(const LineCluster& lineCluster) :
Lines(m_lines) {
	*this = lineCluster;
}

LineCluster::LineCluster(const vector<Line> &lines) :
m_lines(lines), Lines(m_lines) {
	computeVanishingPoint();
	sortLinePointOrder();
}

void LineCluster::add(const Line& line) {
	m_lines.push_back(line);

	float distStart = norm(m_vanishingPoint - line.StartPoint);
	float distEnd = norm(m_vanishingPoint - line.EndPoint);

	if (distStart > distEnd) {
		m_lines.back().swapPoints();
	}
}

void LineCluster::removeDummyLines() {
	vector<Line>::iterator iter = m_lines.begin();

	while (iter != m_lines.end()) {
		if (iter->isDummyLine()) {
			iter = m_lines.erase(iter);
		}
		else {
			iter++;
		}
	}
}

bool LineCluster::isVertical() const {
	return m_isVertical;
}

Point2f LineCluster::getVanishingPoint() const {
	return m_vanishingPoint;
}

bool LineCluster::findClosestLineFrom(const Point2f &centerPoint, const Point2f &dirPoint,
	float &minDistance, int &lineIndex, const Rect exclude) const {
	const int numOfLines = m_lines.size();		// the number of lines in this cluster
	Line centerDirLine(dirPoint, centerPoint);	// A line connecting centerPoint and dirPoint.
	bool isLineFound = false;					// will be set true if at least one line is found

	for (int li = 0; li < numOfLines; li++) {
		const Line& currentLine = m_lines[li];

		if (exclude.contains(currentLine.StartPoint) && exclude.contains(currentLine.EndPoint)) {
			continue;
		}

		// Does not care dummy lines
		if (currentLine.isDummyLine()) {
			continue;
		}

		Point2f crossPoint = currentLine.getIntersection(centerDirLine);

		// A line must be visible by centerPoint, and an intersect point must be actually on the line.
		if (currentLine.isProjectable(centerPoint) && currentLine.isPointOnLine(crossPoint)) {
			float dist = (float)cv::norm(crossPoint - centerPoint);

			if (dist < minDistance) {
				minDistance = dist;
				lineIndex = li;
				isLineFound = true;
			}
		}
	}

	return isLineFound;
}

bool LineCluster::findClosestLineBetween(const cv::Point2f &centerPoint, const cv::Point2f &dirPoint,
	const Line &line1, const Line &line2, float &minDistance, int &lineIndex, Rect exclude) const {
	const int numOfLines = m_lines.size();		// the number of lines in this cluster
	Line centerDirLine(dirPoint, centerPoint);	// A line connecting centerPoint and dirPoint.
	bool isLineFound = false;					// will be set true if at least one line is found

	for (int li = 0; li < numOfLines; li++) {
		const Line& currentLine = m_lines[li];

		if (exclude.contains(currentLine.StartPoint) && exclude.contains(currentLine.EndPoint)) {
			continue;
		}

		// Does not care dummy lines
		if (currentLine.isDummyLine()) {
			continue;
		}

		if (currentLine.isBetween(line1, line2)) {
			Point2f crossPoint = currentLine.getIntersection(centerDirLine);

			if (currentLine.isPointOnLine(crossPoint)) {
				float dist = (float)cv::norm(crossPoint - centerPoint);

				if (dist < minDistance) {
					minDistance = dist;
					lineIndex = li;
					isLineFound = true;
				}
			}
		}
	}

	return isLineFound;
}

int LineCluster::findClosestLineOpposite(const Point2f &centerPoint, const Point2f supVP,
	const int baseLineIndex, const Line &consLine1, const Line &consLine2, Rect exclude) const {
	int foundLineIdx = -1, prefLineIdx = -1;
	float maxCosine = 0.f, maxPrefCosine = 0.f;
	const int numberOfLines = m_lines.size();
	const Line &baseLine = m_lines[baseLineIndex];
	float baseCosine = computeSignedCosine(m_vanishingPoint, centerPoint, baseLine.EndPoint);
	const Line centerLine(supVP, centerPoint);

	for (int li = 0; li < numberOfLines; li++) {
		// compute signed cosines of each line with respect to the center separating line,
		// then take only cosines of the opposite side.
		if (li == baseLineIndex) {
			continue;
		}

		const Line &testLine = m_lines[li];

		if (exclude.contains(testLine.StartPoint) && exclude.contains(testLine.EndPoint)) {
			continue;
		}

		float cosine = computeSignedCosine(m_vanishingPoint, centerPoint, testLine.EndPoint);

		// Proceed further only if current line is on the other side of given base line.
		if (baseCosine * cosine < 0) {
			cosine = abs(cosine);

			if (cosine > maxCosine && testLine.isBetween(consLine1, consLine2)) {
				if (testLine.isPointOnLine(testLine.getIntersection(centerLine))) {
					maxCosine = cosine;
					foundLineIdx = li;
				}
				else if (cosine > maxPrefCosine) {
					maxPrefCosine = cosine;
					prefLineIdx = li;
				}
			}
		}
	}

	// non-projectable line will be used only if no projectable line is available.
	if (foundLineIdx < 0 && maxPrefCosine != -1.f) {
		foundLineIdx = prefLineIdx;
	}

	return foundLineIdx;
}

bool LineCluster::findClosestLineBetween(const Line &baseLine, const Line &line1, const Line &line2,
	int &lineIndex, float &distance, const float minimumLength) {
	bool isUpdated = false;
	
	for (int li = 0; li < m_lines.size(); li++) {
		if (m_lines[li].length() >= minimumLength) {
			Point2f crossPoint = baseLine.getIntersection(m_lines[li]);
			float currentDistance = norm(crossPoint - baseLine.EndPoint);
			if (abs(currentDistance) < distance && m_lines[li].isBetween(line1, line2)) {
				lineIndex = li;
				distance = currentDistance;
				isUpdated = true;
			}
		}
	}

	return isUpdated;
}

bool LineCluster::findClosestLineBetween(const Line &line1, const Line &line2, int &lineIndex, float &distance) {
	vector<float> distances;
	vector<int> indices;

	// Compute distances
	for (Line l : m_lines) {
		float dist = 0.f, dist1, dist2;
		Point2f pt1, pt2;
		if (l.isBetween(line1, line2)) {
			if (l.isDummyLine()) {
				dist = (FLT_MAX / 2.f);
			}
			else {
				if (line1.isDummyLine()) {
					dist1 = (FLT_MAX / 2.f);
				}
				else {
					pt1 = line1.getIntersection(l);
					dist1 = min(cv::norm(pt1 - line1.StartPoint), cv::norm(pt1 - line1.EndPoint));
				}

				if (line2.isDummyLine()) {
					dist2 = (FLT_MAX / 2.f);
				}
				else {
					pt2 = line2.getIntersection(l);
					dist2 = min(cv::norm(pt2 - line2.StartPoint), cv::norm(pt2 - line2.EndPoint));
				}

				dist = min(dist1, dist2);
			}
		}
		else {
			dist = FLT_MAX;
		}
		distances.push_back(abs(dist));
	}

	sortIdx(distances, indices, SORT_ASCENDING);

	// Choose a line as close as possible, which is between two given lines.
	for (unsigned int i = 0; i < distances.size(); i++) {
		// If distance is larger than the best one, we do not need to find another line.
		int index = indices[i];
		if (distances[index] >= distance) {
			return false;
		}
		else if (distances[index] < distance) {
			lineIndex = index;
			distance = distances[index];
			return true;
		}
	}

	return false;
}

// Find the closest line of given line.
bool LineCluster::findClosestLine(const Line &line, int &lineIndex, float &distance) const {
	vector<float> distances;
	vector<int> indices;

	// Compute distances
	for (Line l : m_lines) {
		float dist = min(cv::norm(line.StartPoint - l.StartPoint), cv::norm(line.StartPoint - l.EndPoint));
		dist = min(dist, (float)cv::norm(line.EndPoint - l.StartPoint));
		dist = min(dist, (float)cv::norm(line.EndPoint - l.EndPoint));
		distances.push_back(dist);
	}

	sortIdx(distances, indices, SORT_ASCENDING);

	// Choose a projectable line as close as possible.
	for (unsigned int i = 0; i < distances.size(); i++) {
		// If distance is larger than the best one, we do not need to find another line.
		int index = indices[i];
		if (abs(distances[index]) >= distance) {
			return false;
		}
		else if (distances[index] < distance && line.isProjectable(line)) {
			lineIndex = index;
			distance = distances[index];
			return true;
		}
	}

	return false;
}

// Find the closest line of given point. If it success to find the line where the distance from
// point to the line is closer than given distance, returns true.
bool LineCluster::findClosestLine(const cv::Point2f &point, int &lineIndex, float &distance,
	const float minimumLength) const {
	bool isUpdated = false;

	// Compute distances
	for (unsigned int li = 0; li < m_lines.size(); li++) {
		if (m_lines[li].length() > minimumLength) {
			float lineDistance = m_lines[li].computeDistance(point, true);

			if (abs(lineDistance) < distance && m_lines[li].isProjectable(point)) {
				lineIndex = li;
				distance = lineDistance;
				isUpdated = true;
			}
		}
	}

	return isUpdated;
}

bool LineCluster::findClosestLines(const Point2f &point, int &line1Index, int &line2Index,
	float &distance1, float &distance2, const float minimumLength) const {
	vector<float> distances;
	vector<int> indices;
	int centerIndex = 0;
	float distance1temp = distance1, distance2temp = distance2;
	int line1IndexTemp = -1, line2IndexTemp = -1;

	// Compute distances
	for (Line l : m_lines) {
		if (l.length() < minimumLength) {
			distances.push_back(FLT_MAX);
		}
		else {
			distances.push_back(l.computeDistance(point, true));
		}
	}

	sortIdx(distances, indices, SORT_ASCENDING);

	// Find the index of center element (change from negative to positive distance)
	for (unsigned int i = 1; i < indices.size(); i++) {
		if (distances[indices[i - 1]] * distances[indices[i]] < 0) {
			centerIndex = i - 1;
		}
	}

	// Choose the line from the left side of the point
	for (int i = centerIndex; i >= 0; i--) {
		// If distance is larger than the best one, we do not need to find another line.
		int lineIndex = indices[i];
		if (abs(distances[lineIndex]) >= distance1temp) {
			break;
		}
		else if (m_lines[lineIndex].isProjectable(point) && abs(distances[lineIndex]) < distance1temp) {
			line1IndexTemp = lineIndex;
			distance1temp = abs(distances[lineIndex]);
			break;
		}
	}

	// Choose the line from the right side of the point
	for (int i = centerIndex + 1; i < (int)distances.size(); i++) {
		// If distance is larger than the best one, we do not need to find another line.
		int lineIndex = indices[i];
		if (abs(distances[lineIndex]) >= distance2temp) {
			break;
		}
		else if (m_lines[lineIndex].isProjectable(point) && abs(distances[lineIndex]) < distance2temp) {
			line2IndexTemp = lineIndex;
			distance2temp = abs(distances[lineIndex]);
			break;
		}
	}

	// update lines only if one of new lines is closer than previous lines
	if (min(distance1temp, distance2temp) < min(distance1, distance2)) {
		line1Index = line1IndexTemp;
		line2Index = line2IndexTemp;
		distance1 = distance1temp;
		distance2 = distance2temp;
		return true;
	}

	return false;
}

void LineCluster::computeCosineOfLines(const cv::Point2f &fixedPoint, const cv::Point2f &basePoint,
	vector<float> &cosines) const {
	// Compute degree for each line
	for (int li = 0; li < m_lines.size(); li++) {
		cosines.push_back(computeSignedCosine(fixedPoint, basePoint, m_lines[li].StartPoint));
	}
}

size_t LineCluster::size() const {
	return m_lines.size();
}

LineCluster& LineCluster::operator=(const LineCluster &lineCluster) {
	m_isVertical = lineCluster.isVertical();
	m_lines = lineCluster.Lines;
	m_vanishingPoint = lineCluster.getVanishingPoint();

	return *this;
}

Line &LineCluster::operator[](const int index) {
	return m_lines[index];
}

const Line &LineCluster::operator[](const int index) const {
	return m_lines[index];
}

Point2f LineCluster::computeVanishingPoint(const vector<Line> &lines) {
	Mat lineMat(lines.size(), 3, CV_32F);
	Mat solution;

	assert(lines.size() >= 2);

	for (int row = 0; row < lines.size(); row++) {
		lineMat.row(row) = lines[row].LineVector.t();
		/*vec_cross(m_lines[row].StartPoint.x, m_lines[row].StartPoint.y, 1.f,
		m_lines[row].EndPoint.x, m_lines[row].EndPoint.y, 1.f,
		lineMat.at<float>(row, 0), lineMat.at<float>(row, 1), lineMat.at<float>(row, 2));*/
	}

	cv::SVD::solveZ(lineMat, solution);

	return Point2f(solution.at<float>(0) / solution.at<float>(2), solution.at<float>(1) / solution.at<float>(2));
}

void LineCluster::computeVanishingPoint() {
	Mat lineMat(m_lines.size(), 3, CV_32F);
	Mat solution;

	for (int row = 0; row < m_lines.size(); row++) {
		lineMat.row(row) = m_lines[row].LineVector.t();
		/*vec_cross(m_lines[row].StartPoint.x, m_lines[row].StartPoint.y, 1.f,
		m_lines[row].EndPoint.x, m_lines[row].EndPoint.y, 1.f,
		lineMat.at<float>(row, 0), lineMat.at<float>(row, 1), lineMat.at<float>(row, 2));*/
	}

	cv::SVD::solveZ(lineMat, solution);

	m_vanishingPoint.x = solution.at<float>(0) / solution.at<float>(2);
	m_vanishingPoint.y = solution.at<float>(1) / solution.at<float>(2);

	m_isVertical = (abs(m_vanishingPoint.y) / abs(m_vanishingPoint.x) > 5.f);
}

void LineCluster::sortLinePointOrder() {
	for (Line& l : m_lines) {
		float distStart = norm(m_vanishingPoint - l.StartPoint);
		float distEnd = norm(m_vanishingPoint - l.EndPoint);

		if (distStart > distEnd) {
			m_lines.back().swapPoints();
		}
	}
}

inline float computeSignedCosine(const Point2f &fixedPoint, const Point2f &point1, const Point2f &point2) {
	Point2f vector1 = point1 - fixedPoint;
	Point2f vector2 = point2 - fixedPoint;

	double cosine = (vector1.x * vector2.x + vector1.y * vector2.y) / (norm(vector1) * norm(vector2));
	double sign = (vector1.x * vector2.y) - (vector1.y * vector2.x);

	if (sign < 0) {
		return -cosine;
	}
	else {
		return cosine;
	}
}
