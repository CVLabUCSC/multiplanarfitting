#include "PointPairClustering.h"
#include "TLinkage\SamplerInterface.h"
#include "Homography.h"

#include <iostream>
#include <vector>
#include <list>

#include <opencv2\xfeatures2d\nonfree.hpp>

#include "..\MultiPlanarFitting\HomographyEstimator.h"
#include "Utilities.h"

using namespace std;
using namespace cv;

using cv::xfeatures2d::SIFT;

// SIFT matcher parameter
const double NN_SQ_DIST_RATIO_THR = 0.49;

//-------------------------------------
// Utility functions
//-------------------------------------
double ReprojectionError(const Mat &homography, const CDataType &dataType) {
	if (homography.type() == CV_64F) {
		double x = dataType[0] * homography.at<double>(0, 0) + dataType[1] * homography.at<double>(0, 1) + homography.at<double>(0, 2);
		double y = dataType[0] * homography.at<double>(1, 0) + dataType[1] * homography.at<double>(1, 1) + homography.at<double>(1, 2);
		double z = dataType[0] * homography.at<double>(2, 0) + dataType[1] * homography.at<double>(2, 1) + homography.at<double>(2, 2);

		x /= z;
		y /= z;

		return sqrt((dataType[2] - x)*(dataType[2] - x) + (dataType[3] - y)*(dataType[3] - y));
	}
	else if (homography.type() == CV_32F) {
		float x = dataType[0] * homography.at<float>(0, 0) + dataType[1] * homography.at<float>(0, 1) + homography.at<float>(0, 2);
		float y = dataType[0] * homography.at<float>(1, 0) + dataType[1] * homography.at<float>(1, 1) + homography.at<float>(1, 2);
		float z = dataType[0] * homography.at<float>(2, 0) + dataType[1] * homography.at<float>(2, 1) + homography.at<float>(2, 2);

		x /= z;
		y /= z;

		return sqrt((dataType[2] - x)*(dataType[2] - x) + (dataType[3] - y)*(dataType[3] - y));
	}
	else {
		cout << "Invalid homography type: " << homography.type() << endl;
		assert(false);
	}
}

double EuclideanDistancePointPair(const CDataType &dataPoint1, const CDataType &dataPoint2)
{
	return std::sqrt(std::pow(dataPoint1[0] - dataPoint2[0], 2) + std::pow(dataPoint1[1] - dataPoint2[1], 2) + std::pow(dataPoint1[2] - dataPoint2[2], 2) + std::pow(dataPoint1[3] - dataPoint2[3], 2));
}

void convertClusters(const list<sClLnk *> &clustersList,
	vector<vector<Point2f>> &points1, vector<vector<Point2f>> &points2)
{
	int clusterIndex = 0;
	for (list<sClLnk *>::const_iterator citer = clustersList.cbegin(); citer != clustersList.cend(); citer++)
	{
		if ((*citer)->mBelongingPts.size() <= 4)
			continue;

		points1.push_back(vector<Point2f>());
		points2.push_back(vector<Point2f>());

		for (list<sPtLnk *>::const_iterator piter = (*citer)->mBelongingPts.cbegin();
			piter != (*citer)->mBelongingPts.cend(); piter++) {
			points1[clusterIndex].push_back(Point2f((*piter)->mData[0], (*piter)->mData[1]));
			points2[clusterIndex].push_back(Point2f((*piter)->mData[2], (*piter)->mData[3]));
		}

		clusterIndex++;
	}
}

vector<vector<pair<Point2f, Point2f>>> convertClusters(const list<sClLnk *> &clustersList) {
	int clusterIndex = 0;
	vector<vector<pair<Point2f, Point2f>>> pointPairClusters;

	for (list<sClLnk *>::const_iterator citer = clustersList.cbegin(); citer != clustersList.cend(); citer++)
	{
		pointPairClusters.push_back(std::vector<std::pair<cv::Point2f, cv::Point2f>>());

		for (list<sPtLnk *>::const_iterator piter = (*citer)->mBelongingPts.cbegin();
			piter != (*citer)->mBelongingPts.cend(); piter++) {
			pointPairClusters[clusterIndex].push_back(
				pair<Point2f, Point2f>(Point2f((*piter)->mData[0], (*piter)->mData[1]),
				Point2f((*piter)->mData[2], (*piter)->mData[3])));
		}

		clusterIndex++;
	}

	return pointPairClusters;
}

vector<PointPairCluster> convertClusters(const list<sClLnk *> &clustersList, const vector<Mat> &normals) {
	vector<PointPairCluster> pointPairClusters;

	for (list<sClLnk *>::const_iterator citer = clustersList.cbegin(); citer != clustersList.cend(); citer++)
	{
		int maxCount = 0;
		Mat normal;
		vector<int> accumulator(normals.size(), 0);
		for (int i = 0; i < normals.size(); i++) {
			if ((*citer)->mPreferenceFunction[i] > 0) {
				accumulator[i]++;
			}
		}

		for (int i = 0; i < normals.size(); i++) {
			if (accumulator[i] > maxCount) {
				maxCount = accumulator[i];
				normal = normals[i];
			}
		}

		PointPairCluster pointPairCluster(normal);

		for (list<sPtLnk *>::const_iterator piter = (*citer)->mBelongingPts.cbegin();
			piter != (*citer)->mBelongingPts.cend(); piter++) {
			pointPairCluster.addPointPair(Point2f((*piter)->mData[0], (*piter)->mData[1]),
				Point2f((*piter)->mData[2], (*piter)->mData[3]));
		}

		pointPairClusters.push_back(pointPairCluster);
	}

	return pointPairClusters;
}

Mat drawPointPairs(const Mat &image1, const Mat &image2, vector<CDataType*> pointPairs, int radius)
{
	Mat output = Mat::zeros(image1.rows > image2.rows ? image1.rows : image2.rows,
		image1.cols + image2.cols, image1.type());

	output.adjustROI(0, 0, 0, -image2.cols);
	image1.copyTo(output);
	output.adjustROI(0, 0, -image1.cols, image2.cols);
	image2.copyTo(output);
	output.adjustROI(0, 0, image1.cols, 0);

	for (unsigned int i = 0; i < pointPairs.size(); i++) {
		cv::Point point1((*pointPairs[i])[0], (*pointPairs[i])[1]);
		cv::Point point2(image1.cols + (*pointPairs[i])[2], (*pointPairs[i])[3]);

		cv::circle(output, point1, radius, CV_RGB(0, 255, 0), 2);
		cv::circle(output, point2, radius, CV_RGB(0, 255, 0), 2);
		cv::line(output, point1, point2, CV_RGB(0, 0, 255));
	}

	return output;
}

Mat drawPointPairs(const Mat &image1, const Mat &image2, vector<CDataType> pointPairs, int radius)
{
	Mat output = Mat::zeros(image1.rows > image2.rows ? image1.rows : image2.rows,
		image1.cols + image2.cols, image1.type());

	output.adjustROI(0, 0, 0, -image2.cols);
	image1.copyTo(output);
	output.adjustROI(0, 0, -image1.cols, image2.cols);
	image2.copyTo(output);
	output.adjustROI(0, 0, image1.cols, 0);

	for (unsigned int i = 0; i < pointPairs.size(); i++) {
		cv::Point point1(pointPairs[i][0], pointPairs[i][1]);
		cv::Point point2(image1.cols + pointPairs[i][2], pointPairs[i][3]);

		cv::circle(output, point1, radius, CV_RGB(0, 255, 0), 2);
		cv::circle(output, point2, radius, CV_RGB(0, 255, 0), 2);
		cv::line(output, point1, point2, CV_RGB(0, 0, 255));
	}

	return output;
}

Mat drawPoints(const Mat &image, vector<vector<Point2f>> &points, int radius)
{
	Mat result;
	RNG rng(0xFFFFFFFF);

	if (image.channels() == 3) {
		cvtColor(image, result, CV_BGR2GRAY);
		cvtColor(result, result, CV_GRAY2BGR);
	}
	else {
		cvtColor(image, result, CV_GRAY2BGR);
	}

	for (unsigned int ci = 0; ci < points.size(); ci++) {
		int icolor = (unsigned)rng;
		Scalar color = Scalar(icolor & 255, (icolor >> 8) & 255, (icolor >> 16) & 255);

		for (size_t pi = 0; pi < points[ci].size(); pi++)
			circle(result, points[ci][pi], radius, color, 2);
	}

	return result;
}

Mat drawPoints(const Mat &image, const vector<PointMatch> &pointPairs, int radius) {
	Mat result;

	if (image.channels() == 3) {
		cvtColor(image, result, CV_BGR2GRAY);
		cvtColor(result, result, CV_GRAY2BGR);
	}
	else {
		cvtColor(image, result, CV_GRAY2BGR);
	}

	for (unsigned int pi = 0; pi < pointPairs.size(); pi++) {
		circle(result, pointPairs[pi].first, radius, CV_RGB(0, 255, 0), 2);
	}

	return result;
}

Mat drawPoints(const Mat &image, vector<vector<pair<Point2f, Point2f>>> &pointPairClusters, int radius) {
	Mat result;
	RNG rng(0xFFFFFFFF);

	if (image.channels() == 3) {
		cvtColor(image, result, CV_BGR2GRAY);
		cvtColor(result, result, CV_GRAY2BGR);
	}
	else {
		cvtColor(image, result, CV_GRAY2BGR);
	}

	for (unsigned int ci = 0; ci < pointPairClusters.size(); ci++) {
		int icolor = (unsigned)rng;
		Scalar color = Scalar(icolor & 255, (icolor >> 8) & 255, (icolor >> 16) & 255);

		for (size_t pi = 0; pi < pointPairClusters[ci].size(); pi++)
			circle(result, pointPairClusters[ci][pi].first, radius, color, 2);
	}

	return result;
}

Mat drawPoints(const Mat &image, const vector<CDataType*> &points, int radius) {
	Mat result;

	if (image.channels() == 3) {
		cvtColor(image, result, CV_BGR2GRAY);
		cvtColor(result, result, CV_GRAY2BGR);
	}
	else {
		cvtColor(image, result, CV_GRAY2BGR);
	}

	for (unsigned int pi = 0; pi < points.size(); pi++) {
		circle(result, Point((*points[pi])[0], (*points[pi])[1]), radius, CV_RGB(0, 255, 0), 2);
	}

	return result;
}

Mat drawPoints(const Mat &image, const vector<PointPairCluster> &pointPairClusters,
	int radius, const Mat &VD) {
	Mat result;
	RNG rng(0xFFFFFFFF);

	if (image.channels() == 3) {
		cvtColor(image, result, CV_BGR2GRAY);
		cvtColor(result, result, CV_GRAY2BGR);
	}
	else {
		cvtColor(image, result, CV_GRAY2BGR);
	}

	if (VD.empty()) {
		for (unsigned int ci = 0; ci < pointPairClusters.size(); ci++) {
			int icolor = (unsigned)rng;
			Scalar color = Scalar(icolor & 255, (icolor >> 8) & 255, (icolor >> 16) & 255);

			for (size_t pi = 0; pi < pointPairClusters[ci].size(); pi++)
				circle(result, pointPairClusters[ci][pi].first, radius, color, 2);
		}
	}
	else {
		assert(VD.rows == 3 && VD.cols == 3);

		Scalar colors[3] = {CV_RGB(255, 0, 0), CV_RGB(0, 255, 0), CV_RGB(0, 0, 255)};

		for (unsigned int ci = 0; ci < pointPairClusters.size(); ci++) {
			Scalar color = colors[PlaneFitting::getClosestDirectionalColumn(pointPairClusters[ci].PlaneNormal, VD)];
			
			for (size_t pi = 0; pi < pointPairClusters[ci].size(); pi++)
				circle(result, pointPairClusters[ci][pi].first, radius, color, 2);
		}
	}

	return result;
}

Mat drawPointsL(const Mat &image, const vector<PointPairCluster> &pointPairClusters, int radius) {
	Mat result;
	RNG rng(0xFFFFFFFF);
	Mat legend = Mat::zeros(image.rows, 140, CV_8UC3);

	if (image.channels() == 3) {
		cvtColor(image, result, CV_BGR2GRAY);
		cvtColor(result, result, CV_GRAY2BGR);
	}
	else {
		cvtColor(image, result, CV_GRAY2BGR);
	}

	for (unsigned int ci = 0; ci < pointPairClusters.size(); ci++) {
		int icolor = (unsigned)rng;
		Scalar color = Scalar(icolor & 255, (icolor >> 8) & 255, (icolor >> 16) & 255);

		for (size_t pi = 0; pi < pointPairClusters[ci].size(); pi++)
			circle(result, pointPairClusters[ci][pi].first, radius, color, 2);

		circle(legend, Point(10, 5 + ci * 10), 3, color, 2);
		std::ostringstream text;
		text << "cluster " << std::setfill('0') << std::setw(2) << ci;
		putText(legend, text.str(), Point(20, 10 + ci * 10), cv::FONT_HERSHEY_PLAIN, 1, CV_RGB(255, 255, 255));
	}

	Mat combined(result.rows, result.cols + legend.cols, result.type());
	combined.adjustROI(0, 0, 0, -legend.cols);
	result.copyTo(combined);
	combined.adjustROI(0, 0, -result.cols, legend.cols);
	legend.copyTo(combined);
	combined.adjustROI(0, 0, result.cols, 0);
	result = combined;

	return result;
}

Mat drawPoints(const Mat &image, const PointPairClusters &pointPairClusters,
	int radius, const Mat &VD, const Mat &K, const Mat &R) {
	Mat result;
	RNG rng(0xFFFFFFFF);

	if (image.channels() == 3) {
		cvtColor(image, result, CV_BGR2GRAY);
		cvtColor(result, result, CV_GRAY2BGR);
	}
	else {
		cvtColor(image, result, CV_GRAY2BGR);
	}

	if (VD.empty()) {
		for (unsigned int ci = 0; ci < pointPairClusters.size(); ci++) {
			int icolor = (unsigned)rng;
			Scalar color = Scalar(icolor & 255, (icolor >> 8) & 255, (icolor >> 16) & 255);

			for (size_t pi = 0; pi < pointPairClusters[ci].size(); pi++)
				circle(result, pointPairClusters[ci][pi].first, radius, color, 2);
		}
	}
	else {
		assert(VD.rows == 3 && VD.cols == 3);

		if (K.empty()) {
			Scalar colors[3] = { CV_RGB(255, 0, 0), CV_RGB(0, 255, 0), CV_RGB(0, 0, 255) };

			for (unsigned int ci = 0; ci < pointPairClusters.size(); ci++) {
				Scalar color = colors[PlaneFitting::getClosestDirectionalColumn(pointPairClusters[ci].PlaneNormal, VD)];

				for (size_t pi = 0; pi < pointPairClusters[ci].size(); pi++)
					circle(result, pointPairClusters[ci][pi].first, radius, color, 2);
			}
		}
		else {
			vector<Point2f> VPs;
			vector<Mat> translations(3);
			vector<Scalar> colors(3);

			const int x_index = PlaneFitting::getClosestDirectionalColumn((Mat_<float>(3, 1) << 1, 0, 0), VD);
			const int y_index = PlaneFitting::getClosestDirectionalColumn((Mat_<float>(3, 1) << 0, 1, 0), VD);
			const int z_index = PlaneFitting::getClosestDirectionalColumn((Mat_<float>(3, 1) << 0, 0, 1), VD);

			colors[x_index] = CV_RGB(255, 0, 0);
			colors[y_index] = CV_RGB(0, 0, 255);
			colors[z_index] = CV_RGB(0, 255, 0);

			for (int i = 0; i < VD.cols; i++) {
				Mat point = K * VD.col(i);

				VPs.push_back(Point2f(point.at<float>(0) / point.at<float>(2), point.at<float>(1) / point.at<float>(2)));
			}

			for (unsigned int ci = 0; ci < pointPairClusters.size(); ci++) {
				int normalIndex = PlaneFitting::getClosestDirectionalColumn(pointPairClusters[ci].PlaneNormal, VD);
				int icolor = (unsigned)rng;
				Scalar color = Scalar(icolor & 255, (icolor >> 8) & 255, (icolor >> 16) & 255);
				float dirlength = 8;
				int lineWidth = 2;

				Point2f VP = VPs[normalIndex];

				if (R.empty() || normalIndex == z_index) {
					for (const PointMatch &pointPair : pointPairClusters[ci].PointPairs) {
						const Point2f &point = pointPair.first;
						Mat v = (Mat_<float>(2, 1) << VPs[normalIndex].x - point.x, VPs[normalIndex].y - point.y);
						float n = -norm(v);

						line(result, point, Point2f(point.x + v.at<float>(0) * radius * dirlength / n,
							point.y + v.at<float>(1) * radius * dirlength / n),
							colors[normalIndex], lineWidth);
						circle(result, point, radius, color, 2);
					}
				}
				else {
					Mat H, t_hat;
					double xLengthRatio = -0.4;
					double yLengthRatio = -0.3;
					double zLengthRatio = -1.3;

					//PlaneFitting::computeMWHomography(pointPairClusters[ci].PointPairs,
					//	K, K, R, pointPairClusters[ci].PlaneNormal, t_hat);
					
					//if (normalIndex == x_index) {
					//	H = K * (Mat::eye(3, 3, CV_32F) + (Mat_<float>(3, 1) << -norm(t_hat) * xLengthRatio, 0, 0) * pointPairClusters[ci].PlaneNormal.t()) * K.inv();
					//}
					//else if (normalIndex == y_index) {
					//	H = K * (Mat::eye(3, 3, CV_32F) + (Mat_<float>(3, 1) << 0, -norm(t_hat) * yLengthRatio, 0) * pointPairClusters[ci].PlaneNormal.t()) * K.inv();
					//}
					//else {
					//	H = K * (Mat::eye(3, 3, CV_32F) + (Mat_<float>(3, 1) << 0, 0, -norm(t_hat) * zLengthRatio) * pointPairClusters[ci].PlaneNormal.t()) * K.inv();
					//}
					if (normalIndex == x_index) {
						H = K * (Mat::eye(3, 3, CV_32F) + (Mat_<float>(3, 1) << -0.05, 0, 0) * pointPairClusters[ci].PlaneNormal.t()) * K.inv();
					}
					else if (normalIndex == y_index) {
						H = K * (Mat::eye(3, 3, CV_32F) + (Mat_<float>(3, 1) << 0, 0.1, 0) * pointPairClusters[ci].PlaneNormal.t()) * K.inv();
					}
					else {
						H = K * (Mat::eye(3, 3, CV_32F) + (Mat_<float>(3, 1) << 0, 0, 0.1) * pointPairClusters[ci].PlaneNormal.t()) * K.inv();
					}

					for (const PointMatch &pointPair : pointPairClusters[ci].PointPairs) {
						Mat point = (Mat_<float>(3, 1) << pointPair.first.x, pointPair.first.y, 1.f);
						point = H * point;

						point.at<float>(0) /= point.at<float>(2);
						point.at<float>(1) /= point.at<float>(2);

						line(result, pointPair.first, Point2f(point.at<float>(0), point.at<float>(1)),
							colors[normalIndex], lineWidth);
						circle(result, pointPair.first, radius, color, 2);
					}
				}
				
				/*else if (z_index != normalIndex) {
					for (const PointMatch &pointPair : pointPairClusters[ci].PointPairs) {
						const Point2f &point = pointPair.first;
						Mat v = (Mat_<float>(2, 1) << VPs[normalIndex].x - point.x, VPs[normalIndex].y - point.y);
						float n = norm(v);

						line(result, point, Point2f(point.x + v.at<float>(0) * radius * dirlength / n,
							point.y + v.at<float>(1) * radius * dirlength / n),
							colors[normalIndex], lineWidth);
						circle(result, point, radius, color, 2);
					}
				}
				else {
					Mat t_hat;

					PlaneFitting::computeMWHomography(pointPairClusters[ci].PointPairs,
						K, K, R, pointPairClusters[ci].PlaneNormal, t_hat);

					//(t_hat * 1.5)

					Mat H = K * (Mat::eye(3, 3, CV_32F) + (Mat_<float>(3, 1) << 0, 0, -norm(t_hat) * 1.5) * pointPairClusters[ci].PlaneNormal.t()) * K.inv();

					for (const PointMatch &pointPair : pointPairClusters[ci].PointPairs) {
						Mat point = (Mat_<float>(3, 1) << pointPair.first.x, pointPair.first.y, 1.f);
						point = H * point;

						point.at<float>(0) /= point.at<float>(2);
						point.at<float>(1) /= point.at<float>(2);

						line(result, pointPair.first, Point2f(point.at<float>(0), point.at<float>(1)),
							colors[normalIndex], lineWidth);
						circle(result, pointPair.first, radius, color, 2);
					}
				}*/
			}
		}
	}

	for (const PointMatch &pointMatch : pointPairClusters.Outliers) {
		line(result, Point(pointMatch.first.x - 4, pointMatch.first.y - 4),
			Point(pointMatch.first.x + 4, pointMatch.first.y + 4), CV_RGB(255, 255, 0), 2);
		line(result, Point(pointMatch.first.x + 4, pointMatch.first.y - 4),
			Point(pointMatch.first.x - 4, pointMatch.first.y + 4), CV_RGB(255, 255, 0), 2);
	}

	return result;
}

//-------------------------------------
// Point Pairs Clustering Functions
//-------------------------------------

void computeSIFTFeaturePoints(const Mat &image, vector<KeyPoint> &keypoints, Mat &descriptors) {
	assert(image.type() == CV_8UC3);

	Mat grayImg;

	cvtColor(image, grayImg, CV_BGR2GRAY);

	Ptr<SIFT> detector = SIFT::create();

	detector->detect(grayImg, keypoints);
	detector->compute(grayImg, keypoints, descriptors);

	detector.release();
}

void computeMatches(const vector<KeyPoint> &keypoints1, const vector<KeyPoint> &keypoints2,
	const Mat &descriptors1, const Mat &descriptors2, vector<CDataType*> &pointPairs) {
	FlannBasedMatcher matcher;
	vector<vector<DMatch>> matches;
	matcher.knnMatch(descriptors1, descriptors2, matches, 2);

	for (int i = 0; i < (int)matches.size(); i++)
	{
		if (matches[i].size() == 1) {
			pointPairs.push_back(new PointPair(keypoints1[matches[i][0].queryIdx].pt, keypoints2[matches[i][0].trainIdx].pt));
		}
		else if (matches[i].size() == 2) {
			if (matches[i][0].distance <= matches[i][1].distance * NN_SQ_DIST_RATIO_THR) {
				pointPairs.push_back(new PointPair(keypoints1[matches[i][0].queryIdx].pt, keypoints2[matches[i][0].trainIdx].pt));
			}
		}
	}
}

void computeMatches(const vector<KeyPoint> &keypoints1, const vector<KeyPoint> &keypoints2,
	const Mat &descriptors1, const Mat &descriptors2, vector<pair<Point2f, Point2f>> &pointPairs) {
	FlannBasedMatcher matcher;
	vector<vector<DMatch>> matches;
	matcher.knnMatch(descriptors1, descriptors2, matches, 2);

	for (int i = 0; i < (int)matches.size(); i++)
	{
		if (matches[i].size() == 1) {
			pointPairs.push_back({ keypoints1[matches[i][0].queryIdx].pt, keypoints2[matches[i][0].trainIdx].pt });
		}
		else if (matches[i].size() == 2) {
			if (matches[i][0].distance <= matches[i][1].distance * NN_SQ_DIST_RATIO_THR) {
				pointPairs.push_back({ keypoints1[matches[i][0].queryIdx].pt, keypoints2[matches[i][0].trainIdx].pt });
			}
		}
	}
}

void computeMatches(Mat image1, Mat image2, vector<CDataType*> &pointPairs)
{
	vector<KeyPoint> keypoints1, keypoints2;
	Mat descriptors1, descriptors2;

	computeSIFTFeaturePoints(image1, keypoints1, descriptors1);
	computeSIFTFeaturePoints(image1, keypoints2, descriptors2);

	computeMatches(keypoints1, keypoints2, descriptors1, descriptors2, pointPairs);

	imwrite("matches.jpg", drawPointPairs(image1, image2, pointPairs, 2));
}

vector<Mat> generateHomographies(vector<CDataType*> &pointPairs, const int minimumSampleSet)
{
	vector<Mat> models;

	RandomSampler randomSampler(4, minimumSampleSet, &EuclideanDistancePointPair);
	vector<vector<unsigned int>> indices;
	initRandomSampler(randomSampler, pointPairs, pointPairs.size());

	randomSampler.GetNSample(indices, pointPairs.size() * 20);

	// Generate models
	for (int i = 0; i < (int)indices.size(); i++) {
		vector<Point2f> srcPoints1, srcPoints2;
		for (int j = 0; j < minimumSampleSet; j++) {
			srcPoints1.push_back(cv::Point2f((*pointPairs[indices[i][j]])[0], (*pointPairs[indices[i][j]])[1]));
			srcPoints2.push_back(cv::Point2f((*pointPairs[indices[i][j]])[2], (*pointPairs[indices[i][j]])[3]));
		}

		models.push_back(cv::findHomography(srcPoints1, srcPoints2));
	}

	return models;
}

//-------------------------------------
// Homography Functions
//-------------------------------------

vector<pair<Point2f, Point2f>> computeInliers(const vector<CDataType*> &pointPairs,
	const Mat &H, const double threshold) {
	vector<Point2f> points1, points2;
	vector<pair<Point2f, Point2f>> resultPointPairs;

	for (CDataType* dataPoint : pointPairs) {
		if (ReprojectionError(H, *dataPoint) < threshold) {
			resultPointPairs.push_back(pair<Point2f, Point2f>(Point2f((*dataPoint)[0], (*dataPoint)[1]),
				Point2f((*dataPoint)[2], (*dataPoint)[3])));
		}
	}

	return resultPointPairs;
}

//-------------------------------------
// Main Entry
//-------------------------------------

Mat PointPairClustering(Mat image1, Mat image2) {
	unsigned int nPtDimension = 2;
	int mKDTreeRange = 10;
	const int minimumSampleSet = 4;
	vector<CDataType*> pointPairs;
	vector<Mat> models;
	vector<vector<Point2f>> points1, points2;

	//cv::imwrite("matches.png", DrawPointPairs(image1, image2, pointPairs, 2));

	cout << "Compute point matches..." << endl;
	computeMatches(image1, image2, pointPairs);

	cout << "Generate random models of homographies..." << endl;
	models = generateHomographies(pointPairs, minimumSampleSet);

	TLinkage<Mat> tlinkage(2.5f, &ReprojectionError, models.size(), true, nPtDimension, mKDTreeRange);

	cout << "Initialize T-linkage..." << endl;
	for (unsigned int i = 0; i < pointPairs.size(); i++) {
		tlinkage.AddPoint(*pointPairs[i]);
	}

	for (unsigned int i = 0; i < models.size(); i++) {
		tlinkage.AddModel(models[i]);
	}

	cout << "Data clustering with T-linkage..." << endl;
	list<sClLnk *> clustersList = tlinkage.DoTLClusterization();

	convertClusters(clustersList, points1, points2);

	// memory clean-up
	cout << "Clean-up memory used by T-linkage..." << endl;
	for (unsigned int i = 0; i < pointPairs.size(); i++) {
		delete pointPairs[i];
	}

	for (list<sClLnk *>::iterator iter = clustersList.begin(); iter != clustersList.end(); iter++) {
		delete *iter;
	}

	return drawPoints(image1, points1, 2);
}

vector<PointPairCluster> PointPairClustering(const vector<CDataType*> &pointPairs,
	vector<Mat> &models, const vector<Mat> &normals) {
	 vector<PointPairCluster> pointPairClusters;
	 unsigned int nPtDimension = 4;
	 int mKDTreeRange = 3;
	 const int minimumSampleSet = 2;
	 double t = (double)getTickCount();

	 TLinkage<Mat> tlinkage(2.5f, &ReprojectionError, models.size(), true, nPtDimension, mKDTreeRange);

	 cout << "Initialize T-linkage..." << endl;
	 for (unsigned int i = 0; i < pointPairs.size(); i++) {
		 tlinkage.AddPoint(*pointPairs[i]);
	 }

	 for (unsigned int i = 0; i < models.size(); i++) {
		 tlinkage.AddModel(models[i]);
	 }

	 cout << "Data clustering with T-linkage..." << endl;
	 list<sClLnk *> clustersList = tlinkage.DoTLClusterization();

	 t = ((double)getTickCount() - t) / getTickFrequency();
	 cout << "*** Point pair clustering took " << t << " seconds: " << endl;
	 
	 pointPairClusters = convertClusters(clustersList, normals);

	 sort(pointPairClusters.begin(), pointPairClusters.end(), ClusterSort());

	 // Remove clusters such that the cardinarlity is less than or equal to the number of minimum sample set.
	 for (unsigned int i = 0; i < pointPairClusters.size(); i++) {
		 if (pointPairClusters[i].size() <= minimumSampleSet) {
			 pointPairClusters.erase(pointPairClusters.begin() + i, pointPairClusters.end());
			 break;
		 }
	 }

	 // memory clean-up
	 //cout << "Clean-up memory used by T-linkage..." << endl;
	 //for (list<sClLnk *>::iterator iter = clustersList.begin(); iter != clustersList.end(); iter++) {
		// clustersList.remove(*iter);
	 //}

	 return pointPairClusters;
}

PointPairClusters generatePointPairClusters(const vector<CDataType*> &pointPairs,
	const Mat &label, const Mat &VD, float threshold) {
	map<uchar, int> labelIndexer = PlaneFitting::findUniqueLabels(pointPairs, label);
	const int numOfLabels = labelIndexer.size();
	vector<vector<PointMatch>> temporalGroups(numOfLabels);
	vector<vector<Point2f>> tempPoints1(numOfLabels), tempPoints2(numOfLabels);
	vector<PointPairCluster> pointPairClusters;
	vector<Mat> globalNormals, normals;

	globalNormals.push_back(VD.col(PlaneFitting::getClosestDirectionalColumn((Mat_<float>(3, 1) << 1, 0, 0), VD)));
	globalNormals.push_back(VD.col(PlaneFitting::getClosestDirectionalColumn((Mat_<float>(3, 1) << 0, 1, 0), VD)));
	globalNormals.push_back(VD.col(PlaneFitting::getClosestDirectionalColumn((Mat_<float>(3, 1) << 0, 0, 1), VD)));

	// Categorize the point matches based on their labels
	for (CDataType* data : pointPairs) {
		const PointPair &pointPair = *dynamic_cast<PointPair*>(data);
		int index = labelIndexer[label.at<uchar>(pointPair.Point1)];

		if (index >= 0) {
			tempPoints1[index].push_back(pointPair.Point1);
			tempPoints2[index].push_back(pointPair.Point2);
		}
	}

	normals.resize(labelIndexer.size());

	for (map<uchar, int>::const_iterator citer = labelIndexer.cbegin(); citer != labelIndexer.cend(); citer++) {
		uchar value = citer->first;

		if (value >= 0 && value < 100) {
			normals[citer->second] = globalNormals[0].clone();
		}
		else if (value >= 100 && value < 200) {
			normals[citer->second] = globalNormals[1].clone();
		}
		else if (value >= 200 && value < 250) {
			normals[citer->second] = globalNormals[2].clone();
		}
	}

	// Remove outliers
	for (int ci = 0; ci < numOfLabels; ci++) {
		if (tempPoints1[ci].size() == 0) {
			continue;
		}
		else if (tempPoints1[ci].size() < 4) {
			vector<PointMatch> pointMatches;

			for (int pi = 0; pi < (int)tempPoints1[ci].size(); pi++) {
				pointMatches.push_back(PointMatch(tempPoints1[ci][pi], tempPoints2[ci][pi]));
			}

			pointPairClusters.push_back(PointPairCluster(normals[ci], pointMatches));
		}
		else {
			Mat H;
			vector<Point2f> warpedPoints;
			vector<PointMatch> pointMatches;

			H = findHomography(tempPoints1[ci], tempPoints1[ci], CV_RANSAC);

			// If not able to find good homography, skip the current cluster.
			if (H.empty()) {
				continue;
			}

			perspectiveTransform(tempPoints1[ci], warpedPoints, H);

			for (int pi = 0; pi < (int)tempPoints1[ci].size(); pi++) {
				if (norm(tempPoints1[ci][pi] - warpedPoints[pi]) < threshold) {
					pointMatches.push_back(PointMatch(tempPoints1[ci][pi], tempPoints2[ci][pi]));
				}
			}

			pointPairClusters.push_back(PointPairCluster(normals[ci], pointMatches));
		}
	}

	return PointPairClusters(pointPairClusters, pointPairs);
}
