#include "RegionSegmentation.h"

#include <map>
#include <functional>
#include <fstream>

using namespace std;
using namespace cv;

namespace REGION_DETECTION {
	vector<Point> findRegion(const Point2f &centerPoint, const LineClusters &lineClusters, const Mat &K,
		const Size &imageSize, bool findLineBetween, Rect &exclusionRegion, vector<pair<int, int>> &selectedLines) {
		vector<Point> polygon;
		pair<int, int> closestLineBase1, closestLineBase2;
		int closestLinePair1, closestLinePair2;
		int abc;

		// Find the closest line
		closestLineBase1 = findClosestLine(centerPoint, lineClusters, K, exclusionRegion);
		selectedLines.push_back(pair<int, int>(closestLineBase1.first, closestLineBase1.second));

		if (closestLineBase1.first < 0 || closestLineBase1.second < 0) {
			return polygon;
		}

		// Find the closest line in one of perpendicular clusters
		closestLineBase2 = findClosestLine(centerPoint, lineClusters, closestLineBase1.first, K, exclusionRegion);
		selectedLines.push_back(pair<int, int>(closestLineBase2.first, closestLineBase2.second));

		if (closestLineBase2.first < 0 || closestLineBase2.second < 0) {
			return polygon;
		}

		if (findLineBetween) {
			closestLinePair1 = findClosestLineOpposite(centerPoint, lineClusters[closestLineBase2.first].getVanishingPoint(),
				lineClusters[closestLineBase1.first], closestLineBase1.second);
			selectedLines.push_back(pair<int, int>(closestLineBase1.first, closestLinePair1));

			closestLinePair2 = findClosestLineOpposite(centerPoint, lineClusters[closestLineBase1.first].getVanishingPoint(),
				lineClusters[closestLineBase2.first], closestLineBase2.second);
			selectedLines.push_back(pair<int, int>(closestLineBase2.first, closestLinePair2));
		}
		else {
			closestLinePair1 = findClosestLineOpposite(centerPoint, lineClusters[closestLineBase2.first].getVanishingPoint(),
				lineClusters[closestLineBase1.first], closestLineBase1.second, exclusionRegion);
			selectedLines.push_back(pair<int, int>(closestLineBase1.first, closestLinePair1));

			closestLinePair2 = findClosestLineOpposite(centerPoint, lineClusters[closestLineBase1.first].getVanishingPoint(),
				lineClusters[closestLineBase2.first], closestLineBase2.second, exclusionRegion);
			selectedLines.push_back(pair<int, int>(closestLineBase2.first, closestLinePair2));
		}
		
		return computePolygon(centerPoint, lineClusters, selectedLines, imageSize);
	}

	pair<int, int> findClosestLine(const Point2f &centerPoint, const LineClusters &lineClusters,
		const Mat &K, const Rect &exclusionRegion) {
		vector<Vec3f> vanishingDirections;
		const int numOfClusters = lineClusters.size();
		const float cosineThreshold = cosd(10.f);
		pair<int, int> result(-1, -1);
		float minDistance = FLT_MAX;

		// Compute vanishing directions to figure out whether given pair of line-groups are perpendicular to each other
		for (const LineCluster& cluster : lineClusters.Clusters) {
			vanishingDirections.push_back(MatrixMultiply(K, cluster.getVanishingPoint(), true));
		}

		for (int ci = 0; ci < numOfClusters; ci++) {
			for (int cj = 0; cj < numOfClusters; cj++) {
				// Check if two vanishing directions are perpendicular
				if (ci == cj || vanishingDirections[ci].dot(vanishingDirections[cj]) > cosineThreshold) {
					continue;
				}

				const LineCluster &lineCluster = lineClusters[ci];
				const int numOfLines = lineCluster.size();		// the number of lines in this cluster
				Line centerDirLine(lineClusters[cj].getVanishingPoint(), centerPoint);	// A line connecting centerPoint and dirPoint.

				for (int li = 0; li < numOfLines; li++) {
					const Line& currentLine = lineCluster[li];

					// Check if current line is in the excluding region
					if (exclusionRegion.contains(currentLine.StartPoint) && exclusionRegion.contains(currentLine.EndPoint)) {
						continue;
					}

					// Does not care dummy lines
					if (currentLine.isDummyLine()) {
						continue;
					}

					Point2f crossPoint = currentLine.getIntersection(centerDirLine);

					// A line must be visible by centerPoint, and an intersect point must be actually on the line.
					if (currentLine.isProjectable(centerPoint) && currentLine.isPointOnLine(crossPoint)) {
						float dist = (float)cv::norm(crossPoint - centerPoint);

						if (dist < minDistance) {
							result.first = ci;
							result.second = li;
							minDistance = dist;
						}
					}
				}
			}
		}

		return result;
	}

	pair<int, int> findClosestLine(const Point2f &centerPoint, const LineClusters &lineClusters,
		const int &clusterIndex, const Mat &K, const Rect &exclusionRegion) {
		vector<Vec3f> vanishingDirections;
		const int numOfClusters = lineClusters.size();
		const float cosineThreshold = cosd(10.f);
		pair<int, int> result(-1, -1);
		float minDistance = FLT_MAX;

		// Compute vanishing directions to figure out whether given pair of line-groups are perpendicular to each other
		for (const LineCluster& cluster : lineClusters.Clusters) {
			vanishingDirections.push_back(MatrixMultiply(K, cluster.getVanishingPoint(), true));
		}

		Point2f baseVP = lineClusters[clusterIndex].getVanishingPoint();
		Vec3f baseVD = vanishingDirections[clusterIndex];

		for (int ci = 0; ci < numOfClusters; ci++) {
			// Check if current vanishing direction is perpendicular to the base vanishing direction
			if (clusterIndex == ci || baseVD.dot(vanishingDirections[ci]) > cosineThreshold) {
				continue;
			}

			const LineCluster &lineCluster = lineClusters[ci];
			const int numOfLines = lineCluster.size();		// the number of lines in this cluster
			Line centerDirLine(baseVP, centerPoint);	// A line connecting centerPoint and dirPoint.

			for (int li = 0; li < numOfLines; li++) {
				const Line& currentLine = lineCluster[li];

				// Check if current line is in the excluding region
				if (exclusionRegion.contains(currentLine.StartPoint) && exclusionRegion.contains(currentLine.EndPoint)) {
					continue;
				}

				// Does not care dummy lines
				if (currentLine.isDummyLine()) {
					continue;
				}

				Point2f crossPoint = currentLine.getIntersection(centerDirLine);

				// A line must be visible by centerPoint, and an intersect point must be actually on the line.
				if (currentLine.isPointOnLine(crossPoint)) {
					float dist = (float)cv::norm(crossPoint - centerPoint);

					if (dist < minDistance) {
						result.first = ci;
						result.second = li;
						minDistance = dist;
					}
				}
			}
		}

		return result;
	}

	int findClosestLineOpposite(const Point2f &centerPoint, const Point2f supVP,
		const LineCluster &lineCluster, const int &baseLineIndex, const Rect &exclusionRegion) {
		const int numberOfLines = (int)lineCluster.size();
		const Line &baseLine = lineCluster[baseLineIndex];
		const Point2f vanishingPoint = lineCluster.getVanishingPoint();
		const float baseCosine = computeSignedCosine(vanishingPoint, centerPoint, baseLine.EndPoint);
		const Line centerLine(supVP, centerPoint);

		int foundLineIdx = -1, prefLineIdx = -1;
		float maxCosine = 0.f, maxPrefCosine = 0.f;

		for (int li = 0; li < numberOfLines; li++) {
			// compute signed cosines of each line with respect to the center separating line,
			// then take only cosines of the opposite side.
			if (li == baseLineIndex) {
				continue;
			}

			const Line &testLine = lineCluster[li];

			// Check the exclude region criterion
			if (exclusionRegion.contains(testLine.StartPoint) && exclusionRegion.contains(testLine.EndPoint)) {
				continue;
			}

			float cosine = computeSignedCosine(vanishingPoint, centerPoint, testLine.EndPoint);

			// Proceed further only if current line is on the other side of given base line.
			if (baseCosine * cosine < 0) {
				cosine = abs(cosine);

				if (cosine > maxCosine) {
					if (testLine.isPointOnLine(testLine.getIntersection(centerLine))) {
						maxCosine = cosine;
						foundLineIdx = li;
					}
					else if (cosine > maxPrefCosine) {
						maxPrefCosine = cosine;
						prefLineIdx = li;
					}
				}
			}
		}

		// non-projectable line will be chosen only if no projectable line is available.
		/*if (foundLineIdx < 0 && maxPrefCosine != -1.f) {
			foundLineIdx = prefLineIdx;
		}*/

		return foundLineIdx;
	}

	int findClosestLineOpposite(const Point2f &centerPoint, const Point2f supVP,
		const LineCluster &lineCluster, const int &baseLineIndex) {
		const int numberOfLines = (int)lineCluster.size();
		const Line &baseLine = lineCluster[baseLineIndex];
		const Point2f vanishingPoint = lineCluster.getVanishingPoint();
		const float baseCosine = computeSignedCosine(vanishingPoint, centerPoint, baseLine.EndPoint);
		const Line centerLine(supVP, centerPoint);
		const Line LPt2VP(baseLine.StartPoint, supVP), RPt2VP(baseLine.EndPoint, supVP);

		int foundLineIdx = -1, prefLineIdx = -1;
		float maxCosine = 0.f, maxPrefCosine = 0.f;

		for (int li = 0; li < numberOfLines; li++) {
			// compute signed cosines of each line with respect to the center separating line,
			// then take only cosines of the opposite side.
			if (li == baseLineIndex) {
				continue;
			}

			const Line &testLine = lineCluster[li];

			float cosine = computeSignedCosine(vanishingPoint, centerPoint, testLine.EndPoint);

			// Proceed further only if current line is on the other side of given base line.
			if (baseCosine * cosine < 0) {
				cosine = abs(cosine);

				if (cosine > maxCosine) {
					if (testLine.isBetween(LPt2VP, RPt2VP)) {
						maxCosine = cosine;
						foundLineIdx = li;
					}
					else if (cosine > maxPrefCosine) {
						maxPrefCosine = cosine;
						prefLineIdx = li;
					}
				}
			}
		}

		// non-projectable line will be chosen only if no projectable line is available.
		/*if (foundLineIdx < 0 && maxPrefCosine != -1.f) {
		foundLineIdx = prefLineIdx;
		}*/

		return foundLineIdx;
	}

	vector<Point> computePolygon(const Point2f &centerPoint, const LineClusters &lineClusters,
		const vector<pair<int, int>> &selectedLines, const Size &imageSize) {
		// At least first two lines must be provided
		assert(selectedLines.size() == 4 &&
			selectedLines[0].first >= 0 && selectedLines[0].first < lineClusters.size() &&
			selectedLines[1].first >= 0 && selectedLines[1].first < lineClusters.size());

		vector<Point> polygon(4);

		const LineCluster &cluster1 = lineClusters[selectedLines[0].first];
		const LineCluster &cluster2 = lineClusters[selectedLines[1].first];

		const Line &group1Line1 = cluster1[selectedLines[0].second];
		const Line &group2Line1 = cluster2[selectedLines[1].second];

		// If only two lines are provided
		if (selectedLines[2].second < 0 && selectedLines[3].second < 0) {
			Point2f crossPoint = group2Line1.getIntersection(group1Line1);
			Point2f group1Line2_StartPoint, group2Line2_StartPoint;

			if (norm(crossPoint - group2Line1.StartPoint) > norm(crossPoint - group2Line1.EndPoint)) {
				group1Line2_StartPoint = group2Line1.StartPoint;
			}
			else {
				group1Line2_StartPoint = group2Line1.EndPoint;
			}

			if (norm(crossPoint - group1Line1.StartPoint) > norm(crossPoint - group1Line1.EndPoint)) {
				group2Line2_StartPoint = group1Line1.StartPoint;
			}
			else {
				group2Line2_StartPoint = group1Line1.EndPoint;
			}

			const Line group1Line2(group1Line2_StartPoint, cluster1.getVanishingPoint());
			const Line group2Line2(group2Line2_StartPoint, cluster2.getVanishingPoint());

			polygon[0] = group1Line1.getIntersection(group2Line1);
			polygon[1] = group2Line1.getIntersection(group1Line2);
			polygon[2] = group1Line2.getIntersection(group2Line2);
			polygon[3] = group2Line2.getIntersection(group1Line1);
		}
		else if (selectedLines[3].second < 0) {
			// Found three lines (two lines of the first group and one line of the pair group)
			const Line &group1Line2 = cluster1[selectedLines[2].second];
			const Line &group2Line2 = computeIncompleteLine(centerPoint, group2Line1, group1Line1, group1Line2, cluster2.getVanishingPoint());

			polygon[0] = group1Line1.getIntersection(group2Line1);
			polygon[1] = group2Line1.getIntersection(group1Line2);
			polygon[2] = group1Line2.getIntersection(group2Line2);
			polygon[3] = group2Line2.getIntersection(group1Line1);
		}
		else if (selectedLines[2].second < 0) {
			// Found three lines (one line of the first group and two lines of the pair group)
			const Line &group2Line2 = cluster2[selectedLines[3].second];
			const Line &group1Line2 = computeIncompleteLine(centerPoint, group1Line1, group2Line1, group2Line2, cluster1.getVanishingPoint());

			polygon[0] = group1Line1.getIntersection(group2Line1);
			polygon[1] = group2Line1.getIntersection(group1Line2);
			polygon[2] = group1Line2.getIntersection(group2Line2);
			polygon[3] = group2Line2.getIntersection(group1Line1);
		}
		else {
			// Found all of four lines
			const Line &group1Line2 = cluster1[selectedLines[2].second];
			const Line &group2Line2 = cluster2[selectedLines[3].second];

			polygon[0] = group1Line1.getIntersection(group2Line1);
			polygon[1] = group2Line1.getIntersection(group1Line2);
			polygon[2] = group1Line2.getIntersection(group2Line2);
			polygon[3] = group2Line2.getIntersection(group1Line1);
		}

		return polygon;
	}

	Mat computeNormal(const vector<Point> &polygon, const Mat &K, Mat &VD) {
		assert(polygon.size() >= 4 && K.type() == CV_32F);

		Point2f VP1 = Line(polygon[0], polygon[1]).getIntersection(Line(polygon[3], polygon[2]));
		Point2f VP2 = Line(polygon[1], polygon[2]).getIntersection(Line(polygon[0], polygon[3]));
		Mat invK = K.inv();
		Mat VD1, VD2;

		if (_isnanf(VP1.x) || isinf<float>(VP1.x) || _isnanf(VP1.y) || isinf<float>(VP1.y)) {
			VD1 = invK * (Mat_<float>(3, 1) << (polygon[0].x - polygon[1].x), (polygon[0].y - polygon[1].y), 1.f);
		}
		else {
			VD1 = invK * (Mat_<float>(3, 1) << VP1.x, VP1.y, 1.f);
		}

		if (_isnanf(VP2.x) || isinf<float>(VP2.x) || _isnanf(VP2.y) || isinf<float>(VP2.y)) {
			VD2 = invK * (Mat_<float>(3, 1) << (polygon[1].x - polygon[2].x), (polygon[1].y - polygon[2].y), 1.f);
		}
		else {
			VD2 = invK * (Mat_<float>(3, 1) << VP2.x, VP2.y, 1.f);
		}

		Mat VD3 = VD1.cross(VD2);

		VD3 = VD3 / norm(VD3);

		VD = (Mat_<float>(3, 3) <<
			VD1.at<float>(0), VD2.at<float>(0), VD3.at<float>(0),
			VD1.at<float>(1), VD2.at<float>(1), VD3.at<float>(1),
			VD1.at<float>(2), VD2.at<float>(2), VD3.at<float>(2));

		return VD3;
	}

	Line computeIncompleteLine(const Point2f centerPoint, const Line& baseLine,
		const Line& supportLine1, const Line& supportLine2, const Point2f &vanishingPoint) {
		vector<Line> lines;

		lines.push_back(Line(vanishingPoint, supportLine1.StartPoint));
		lines.push_back(Line(vanishingPoint, supportLine1.EndPoint));
		lines.push_back(Line(vanishingPoint, supportLine2.StartPoint));
		lines.push_back(Line(vanishingPoint, supportLine2.EndPoint));

		const float cosineBaseLine = computeSignedCosine(vanishingPoint, centerPoint, baseLine.EndPoint);

		int bestLineIndex = -1;
		float minCosine = 1.1f;

		for (int li = 0; li < 4; li++) {
			float cosine = computeSignedCosine(vanishingPoint, centerPoint, lines[li].EndPoint);
			if (cosine * cosineBaseLine < 0 && abs(cosine) < minCosine) {
				minCosine = abs(cosine);
				bestLineIndex = li;
			}
		}
		
		if (bestLineIndex < 0) {
			return lines[0];
		}

		return lines[bestLineIndex];
	}

	void lineExtension(const Point2f &centerPoint, const LineClusters &lineClusters,
		const pair<int, int> &extendingLine, const int &supIdx, const Size &imageSize) {
		const LineCluster& lineCluster = lineClusters[extendingLine.first];
		const int numOfClusters = lineClusters.size();
		const int numOfLines = lineCluster.size();
		const cv::Point2f supportingVP = lineClusters[supIdx].getVanishingPoint();
		const Line& baseLine = lineCluster[extendingLine.second];
		float cosineOfInput = computeSignedCosine(supportingVP, centerPoint, baseLine.StartPoint);
		multimap<float, int, std::greater<float>> cosines;
		Mat occupiedPixels = Mat::zeros(imageSize, CV_8U);
		vector<vector<Point2f>> candidateRegion(1, vector<Point2f>(4));

		// Set the first two points as given line
		candidateRegion[0][0] = baseLine.EndPoint;
		candidateRegion[0][1] = baseLine.StartPoint;

		// Compute cosines for each line
		for (int li = 0; li < numOfLines; li++) {
			// compute cosine
			float cosine = computeSignedCosine(supportingVP, centerPoint, lineCluster[li].StartPoint);

			if (cosineOfInput * cosine < 0) {	// on the opposite side
				cosines.insert(pair<float, int>(abs(cosine), li));
			}
		}

		// Draw pixels occupied by lines in other clusters
		for (int ci = 0; ci < numOfClusters; ci++) {
			// The lines in given cluster and supporting cluster do not regarded as 'blocking'.
			if (ci == extendingLine.first || ci == supIdx) {
				continue;
			}

			for (int li = 0; li < lineClusters[ci].size(); li++) {
				line(occupiedPixels, lineClusters[ci][li].StartPoint, lineClusters[ci][li].EndPoint, CV_RGB(255, 255, 255));
			}
		}

		// Extend the region generated by the line until it hits the blocking lines
		for (multimap<float, int, std::greater<float>>::const_iterator citer = cosines.cbegin();
			citer != cosines.cend(); citer++) {
			Mat chosenArea = Mat::zeros(occupiedPixels.size(), CV_8U);
			const Line &testLine = lineCluster[citer->second];

			// Compute the polygon
			const Line lineS_VP(baseLine.StartPoint, supportingVP);
			const Line lineE_VP(baseLine.EndPoint, supportingVP);

			candidateRegion[0][2] = testLine.getIntersection(lineS_VP);
			candidateRegion[0][3] = testLine.getIntersection(lineE_VP);

			fillPoly(chosenArea, candidateRegion, CV_RGB(255, 255, 255));

			bitwise_and(occupiedPixels, chosenArea, chosenArea);

			if (countNonZero(chosenArea) > 0) {
				// Hit the one of blocking lines
				break;
			}
			else {
				// Update the region
			}
		}
	}

	Vec3f MatrixMultiply(const Mat &mat, const Point2f &point, const bool normalize) {
		assert(mat.cols == 3 && mat.rows == 3);

		Vec3f res(mat.at<float>(0, 0) * point.x + mat.at<float>(0, 1) * point.y + mat.at<float>(0, 2),
			mat.at<float>(1, 0) * point.x + mat.at<float>(1, 1) * point.y + mat.at<float>(1, 2),
			mat.at<float>(2, 0) * point.x + mat.at<float>(2, 1) * point.y + mat.at<float>(2, 2));

		if (normalize) {
			res = cv::normalize(res);
		}

		return res;
	}

	inline float cosd(float degree) {
		return cos(degree * CV_PI / 180.0f);
	}

	inline Mat crossProductMatrix(const Mat &vector) {
		return (Mat_<float>(3, 3) << 0., -vector.at<float>(2), vector.at<float>(1),
			vector.at<float>(2), 0.f, -vector.at<float>(0),
			-vector.at<float>(1), vector.at<float>(0), 0.f);
	}

	Mat rotationBetween(const Mat &vec1, const Mat &vec2) {
		Mat v = vec1.cross(vec2);
		Mat v_cross = crossProductMatrix(v);
		float cosine = vec1.dot(vec2);

		return Mat::eye(3, 3, CV_32F) + v_cross + (1. / (1. + cosine)) * v_cross * v_cross;
	}

	Mat load3x3Matrix(const string &filename)
	{
		Mat mat(3, 3, CV_32F);
		fstream fin(filename, fstream::in);

		if (!fin.is_open())
		{
			printf("Not able to open the 3x3 matrix data file: %s\n", strerror(errno));
			return Mat();
		}

		fin >> mat.at<float>(0, 0);
		fin >> mat.at<float>(0, 1);
		fin >> mat.at<float>(0, 2);
		fin >> mat.at<float>(1, 0);
		fin >> mat.at<float>(1, 1);
		fin >> mat.at<float>(1, 2);
		fin >> mat.at<float>(2, 0);
		fin >> mat.at<float>(2, 1);
		fin >> mat.at<float>(2, 2);

		fin.close();

		return mat;
	}

	Mat load3x3MatrixD(const string &filename) {
		Mat mat(3, 3, CV_64F);
		fstream fin(filename, fstream::in);

		if (!fin.is_open())
		{
			printf("Not able to open the 3x3 matrix data file: %s\n", strerror(errno));
			return Mat();
		}

		fin >> mat.at<double>(0, 0);
		fin >> mat.at<double>(0, 1);
		fin >> mat.at<double>(0, 2);
		fin >> mat.at<double>(1, 0);
		fin >> mat.at<double>(1, 1);
		fin >> mat.at<double>(1, 2);
		fin >> mat.at<double>(2, 0);
		fin >> mat.at<double>(2, 1);
		fin >> mat.at<double>(2, 2);

		fin.close();

		return mat;
	}
}
