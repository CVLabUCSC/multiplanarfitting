#include "PointPairCluster.h"
#include "Homography.h"

using namespace std;
using namespace cv;

PointPairCluster::PointPairCluster() : PointPairs(m_pointPairs), PlaneNormal(m_normal) {
	// do nothing
}

PointPairCluster::PointPairCluster(const PointPairCluster& pointPairCluster) :
PointPairs(m_pointPairs), PlaneNormal(m_normal) {
	*this = pointPairCluster;
}

PointPairCluster::PointPairCluster(const Mat &normal) :
PointPairs(m_pointPairs), PlaneNormal(m_normal) {
	m_normal = normal.clone();
}

PointPairCluster::PointPairCluster(const Mat &normal, vector<pair<Point2f, Point2f>> &pointPairs) :
PointPairs(m_pointPairs), PlaneNormal(m_normal) {
	m_pointPairs = pointPairs;
	m_normal = normal.clone();
}

PointPairCluster& PointPairCluster::operator=(const PointPairCluster &pointPairCluster) {
	m_pointPairs = pointPairCluster.PointPairs;
	m_normal = pointPairCluster.PlaneNormal.clone();

	return *this;
}

PointMatch &PointPairCluster::operator[](const int index) {
	return m_pointPairs[index];
}

const PointMatch &PointPairCluster::operator[](const int index) const {
	return m_pointPairs[index];
}

void PointPairCluster::addPointPair(const pair<Point2f, Point2f> &pointPair) {
	m_pointPairs.push_back(pointPair);
}

void PointPairCluster::addPointPair(const Point2f &point1, const Point2f &point2) {
	m_pointPairs.push_back(pair<Point2f, Point2f>(point1, point2));
}

void PointPairCluster::addPointPairs(const vector<PointMatch> &pointPairs) {
	for (const PointMatch &pointMatch : pointPairs) {
		m_pointPairs.push_back(pointMatch);
	}
}

void PointPairCluster::setPlaneNormal(const cv::Mat &normal) {
	m_normal = normal.clone();
}

vector<Point2f> PointPairCluster::getArrayOfFirstPoints() const {
	vector<Point2f> points;

	for (const PointMatch &pointMatch : m_pointPairs) {
		points.push_back(pointMatch.first);
	}

	return points;
}

vector<Point2f> PointPairCluster::getArrayOfSecondPoints() const {
	vector<Point2f> points;

	for (const PointMatch &pointMatch : m_pointPairs) {
		points.push_back(pointMatch.second);
	}

	return points;
}

size_t PointPairCluster::size() const {
	return m_pointPairs.size();
}

vector<PointPairCluster> PointPairCluster::computeInliers(const vector<PointPairCluster> &pointPairClusters,
	const vector<PointMatch> &pointMatches, const float threshold) {
	vector<PointPairCluster> clusters_output;

	for (const PointPairCluster &pointPairCluster : pointPairClusters) {
		if (pointPairCluster.size() >= 4) {
			Mat H = findHomography(pointPairCluster.getArrayOfFirstPoints(), pointPairCluster.getArrayOfSecondPoints());
			clusters_output.push_back(computeInliers(H, pointMatches, threshold));
		}
	}

	return clusters_output;
}

PointPairCluster PointPairCluster::computeInliers(const Mat &H,
	const vector<PointMatch> &pointMatches, const float threshold) {
	vector<PointMatch> output;

	for (const PointMatch &pointMatch : pointMatches) {
		if (PlaneFitting::ReprojectionError(H, pointMatch) < threshold) {
			output.push_back(pointMatch);
		}
	}

	return PointPairCluster(Mat(), output);
}
