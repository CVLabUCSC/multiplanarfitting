#include "MultiPlanarFitting.h"

#include <unordered_set>

#include "RegionSegmentation.h"
#include "Homography.h"
#include "Utilities.h"
#include "TLinkage\SamplerInterface.h"

namespace PlaneFitting {
	using namespace cv;
	using namespace std;

	double computeJaccardDistance(const PointPairCluster &cluster1, const PointPairCluster &cluster2)
	{
		unordered_set<PointMatch, CustomHash> matchHashmap;

		// Insert matches of the first cluster to hashmap
		for (int i = 0; i < int(cluster1.size()); i++) {
			matchHashmap.insert(cluster1[i]);
		}

		// Compare existance
		int intersection = 0;
		for (int i = 0; i < int(cluster2.size()); i++) {
			if (matchHashmap.find(cluster2[i]) != matchHashmap.end()) {
				intersection++;
			}
		}

		int card_union = int(cluster1.size() + cluster2.size()) - intersection;

		return double(card_union - intersection) / double(card_union);
	}

	bool isCentroidOnSameSide(const PointPairCluster &cluster1, const PointPairCluster &cluster2, const Mat &VL) {
		Mat centroid1(3, 1, CV_64F);
		Mat centroid2(3, 1, CV_64F);

		centroid1.at<double>(0) = 0.0;
		centroid1.at<double>(1) = 0.0;
		centroid1.at<double>(2) = 1.0;

		centroid2.at<double>(0) = 0.0;
		centroid2.at<double>(1) = 0.0;
		centroid2.at<double>(2) = 1.0;

		for (const PointMatch &pointMatch : cluster1.PointPairs) {
			centroid1.at<double>(0) += pointMatch.first.x;
			centroid1.at<double>(1) += pointMatch.first.y;
		}
		centroid1.at<double>(0) /= double(cluster1.size());
		centroid1.at<double>(1) /= double(cluster1.size());

		for (const PointMatch &pointMatch : cluster2.PointPairs) {
			centroid2.at<double>(0) += pointMatch.first.x;
			centroid2.at<double>(1) += pointMatch.first.y;
		}
		centroid2.at<double>(0) /= double(cluster2.size());
		centroid2.at<double>(1) /= double(cluster2.size());

		if (VL.dot(centroid1) * VL.dot(centroid2) >= 0) {
			return true;
		}
		else {
			return false;
		}
	}

	double MPDMergeDistance(const PointPairCluster &cluster1, const PointPairCluster &cluster2)
	{
		unordered_set<PointMatch, CustomHash> matchHashmap;
		vector<PointMatch> pointMatches;

		// Insert matches of the first cluster to hashmap
		for (const PointMatch &pointMatch : cluster1.PointPairs) {
			matchHashmap.insert(pointMatch);
		}

		// Compare existance
		int num_union = (int)cluster1.size();
		for (const PointMatch &pointMatch : cluster2.PointPairs) {
			if (matchHashmap.find(pointMatch) == matchHashmap.end()) {
				pointMatches.push_back(pointMatch);
				num_union++;
			}
		}

		Mat H = findHomography(pointMatches);

		if (H.empty()) {
			return FLT_MAX;
		}

		double sum = 0.0;

		for (const PointMatch &pointMatch : pointMatches) {
			sum += ReprojectionError(H, pointMatch);
		}

		return sum / double(num_union);
	}

	vector<vector<unsigned int>> regionBasedSampling(const Mat &image, const LineClusters &lineClusters,
		const vector<CDataType*> pointPairs, const Mat &K) {
		vector<vector<unsigned int>> selectedPoints;

		for (int pi = 0; pi < pointPairs.size(); pi++) {
			Point2f centerPoint((*pointPairs[pi])[0], (*pointPairs[pi])[1]);
			vector<vector<Point>> polygon;
			Mat mask = Mat::zeros(image.size(), CV_8U);
			vector<unsigned int> selectedPointIndices;

			polygon.push_back(REGION_DETECTION::findRegion(centerPoint,
				lineClusters, K, image.size()));

			if (polygon[0].size() != 4) {
				continue;
			}

			// Generate a mask with found region polygon
			fillPoly(mask, polygon, CV_RGB(255, 255, 255));

			// Find points that are in the region
			for (unsigned int i = 0; i < pointPairs.size(); i++) {
				const PointPair *pointPair = dynamic_cast<PointPair*>(pointPairs[i]);

				if (mask.at<uchar>(pointPair->Point1) != 0) {
					selectedPointIndices.push_back(i);
				}
			}

			if (selectedPointIndices.size() > 0) {
				selectedPoints.push_back(selectedPointIndices);
			}
		}

		return selectedPoints;
	}

	vector<Mat> generateMWHomographies(const Mat &image,
		LineClusters &lineClusters, const vector<CDataType*> pointPairs,
		const Mat &K, const Mat &R, const Mat &VD, bool useGlobalNormals,
		bool findLineByExtending, vector<Mat> &normals) {
		const Size imageSize = image.size();
		vector<Mat> homographies;

		for (int pi = 0; pi < pointPairs.size(); pi++) {
			Point2f centerPoint((*pointPairs[pi])[0], (*pointPairs[pi])[1]);
			vector<vector<Point>> polygon(1);
			int inputKey;
			Mat mask = Mat::zeros(imageSize, CV_8U);
			vector<PointPair> selectedPointPairs;
			Mat normal;
			vector<pair<int, int>> selectedLines;

			polygon[0] = REGION_DETECTION::findRegion(centerPoint, lineClusters, K,
				imageSize, findLineByExtending, Rect(), selectedLines);

			if (polygon[0].size() != 4) {
				continue;
			}

			// Generate a mask with found region polygon
			fillPoly(mask, polygon, CV_RGB(255, 255, 255));

			// Find points that are in the region
			for (CDataType* dataPoint : pointPairs) {
				const PointPair *pointPair = dynamic_cast<PointPair*>(dataPoint);

				if (mask.at<uchar>(pointPair->Point1) != 0) {
					selectedPointPairs.push_back(*pointPair);
				}
			}

			if (selectedPointPairs.size() < 2) {
				continue;
			}

			// Compute normal
			normal = REGION_DETECTION::computeNormal(polygon[0], K);
			
			if (useGlobalNormals) {
				normal = VD.col(PlaneFitting::getClosestDirectionalColumn(normal, VD));
			}

			normals.push_back(normal);

			// Compute a MW homography with all the points in the region using computed normal
			homographies.push_back(PlaneFitting::computeMWHomography(selectedPointPairs, K, K, R, normal));
		}

		return homographies;
	}

	float generateMWHomographiesDebug(const Mat &image,
		LineClusters &lineClusters, const vector<CDataType*> pointPairs,
		const Mat &K, const Mat &R, const Mat &VD) {
		const Size imageSize = image.size();
		int numOfGoodRegions = 0;
		int cumNumOfPointsInRegion = 0;

		for (int pi = 0; pi < pointPairs.size(); pi++) {
			Point2f centerPoint((*pointPairs[pi])[0], (*pointPairs[pi])[1]);
			vector<vector<Point>> polygon(1);
			Mat mask = Mat::zeros(imageSize, CV_8U);
			vector<PointPair> selectedPointPairs;

			polygon[0] = REGION_DETECTION::findRegion(centerPoint, lineClusters, K,
				imageSize, true);

			if (polygon[0].size() != 4) {
				continue;
			}

			// Generate a mask with found region polygon
			fillPoly(mask, polygon, CV_RGB(255, 255, 255));

			// Find points that are in the region
			for (CDataType* dataPoint : pointPairs) {
				const PointPair *pointPair = dynamic_cast<PointPair*>(dataPoint);

				if (mask.at<uchar>(pointPair->Point1) != 0) {
					selectedPointPairs.push_back(*pointPair);
				}
			}

			if (selectedPointPairs.size() < 2) {
				continue;
			}

			cumNumOfPointsInRegion += (int)selectedPointPairs.size();
			numOfGoodRegions++;
		}

		return float(cumNumOfPointsInRegion) / float(numOfGoodRegions);
	}

	vector<Mat> generateMWHomographies(const Mat &image,
		vector<LineClusters> &lineClusters, const vector<CDataType*> pointPairs,
		const Mat &K, vector<Mat> &normals) {
		const Size imageSize = image.size();
		vector<Mat> homographies;

		assert(lineClusters.size() == 2);

		for (int pi = 0; pi < pointPairs.size(); pi++) {
			Point2f centerPoint((*pointPairs[pi])[0], (*pointPairs[pi])[1]);
			vector<vector<Point>> polygon(1);
			int inputKey;
			Mat mask = Mat::zeros(imageSize, CV_8U);
			vector<PointPair> selectedPointPairs;
			Mat normal;
			vector<pair<int, int>> selectedLines1, selectedLines2;
			Mat VD1, VD2;

			polygon[0] = REGION_DETECTION::findRegion(centerPoint, lineClusters[0], K, imageSize, true,
				Rect(), selectedLines1);

			if (polygon[0].size() != 4) {
				continue;
			}

			// Generate a mask with found region polygon
			fillPoly(mask, polygon, CV_RGB(255, 255, 255));

			// Find points that are in the region
			for (CDataType* dataPoint : pointPairs) {
				const PointPair *pointPair = dynamic_cast<PointPair*>(dataPoint);

				if (mask.at<uchar>(pointPair->Point1) != 0) {
					selectedPointPairs.push_back(*pointPair);
				}
			}

			if (selectedPointPairs.size() < 2) {
				continue;
			}

			polygon.push_back(REGION_DETECTION::findRegion(centerPoint, lineClusters[0], K, imageSize, true, Rect(), selectedLines2));

			// Compute normal and rotation between two cameras
			normal = REGION_DETECTION::computeNormal(polygon[0], K, VD1);
			REGION_DETECTION::computeNormal(polygon[1], K, VD2);
			
			normals.push_back(normal);

			// Compute a MW homography with all the points in the region using computed normal
			homographies.push_back(PlaneFitting::computeMWHomography(selectedPointPairs, K, K, VD2 * VD1.inv(), normal));
		}

		return homographies;
	}

	vector<Mat> generateMWHomographies(const Mat &image, const vector<CDataType*> pointPairs,
		const Mat &OM, const Mat &K, const Mat &R, const Mat &VD, int modelsPerGroup, vector<Mat> &normals) {
		vector<Mat> homographies;
		map<Point2f, int, CustomHash> pointIndexer;

		// Separate points into three different groups using the orientation map
		map<uchar, int> labelIndexer = findUniqueLabels(pointPairs, OM);
		vector<vector<PointPair>> temporalGroups(labelIndexer.size());

		for (CDataType* data : pointPairs) {
			const PointPair pointPair = *dynamic_cast<PointPair*>(data);
			int index = labelIndexer[OM.at<uchar>(pointPair.Point1)];

			if (index >= 0) {
				temporalGroups[index].push_back(pointPair);
			}
		}
		
		// compute vanishing lines to separate point clusters into two groups
		Mat VL = computeVanishingLines(K, VD);
		vector<vector<PointPair>> pointPairGroups;
		vector<Mat> precomputedNormals;

		for (map<uchar, int>::iterator iter = labelIndexer.begin(); iter != labelIndexer.end(); iter++) {
			vector<PointPair> group1, group2;
			int VDIndex = -1;

			// x-directional normal
			if (iter->first > 0 && iter->first <= 100) {
				VDIndex = getClosestDirectionalColumn((Mat_<float>(3, 1) << 1, 0, 0), VD);
				separateMatchesVL(temporalGroups[iter->second], VL.col(VDIndex), group1, group2);
			}
			// y-directional normal
			else if (iter->first > 100 && iter->first <= 200) {
				VDIndex = getClosestDirectionalColumn((Mat_<float>(3, 1) << 0, 1, 0), VD);
				separateMatchesVL(temporalGroups[iter->second], VL.col(VDIndex), group1, group2);
			}
			// z-directional normal
			else if (iter->first > 200 && iter->first < 250) {
				VDIndex = getClosestDirectionalColumn((Mat_<float>(3, 1) << 0, 0, 1), VD);
				separateMatchesVL(temporalGroups[iter->second], VL.col(VDIndex), group1, group2);
			}
			// If the intensity is not in the range of (1, 250), ignore the point
			else {
				continue;
			}

			if (VDIndex < 0) {
				continue;
			}

			if (group1.size() >= 2) {
				pointPairGroups.push_back(group1);
				precomputedNormals.push_back(VD.col(VDIndex));
			}

			if (group2.size() >= 2) {
				pointPairGroups.push_back(group2);
				precomputedNormals.push_back(VD.col(VDIndex));
			}
		}

		// Generate homographies for each group
		for (int gi = 0; gi < (int)pointPairGroups.size(); gi++) {
			const Mat &normal = precomputedNormals[gi];
			vector<PointPair> &pointPairGroup = pointPairGroups[gi];
			const int numOfPoints = pointPairGroup.size();
			const int numOfPossibleModels = numOfPoints * (numOfPoints - 1);

			int numOfModels = numOfPossibleModels / 2 < modelsPerGroup ?
				numOfPossibleModels / 2 : modelsPerGroup;
			
			vector<CDataType*> dataTypes(numOfPoints);
			transform(pointPairGroup.begin(), pointPairGroup.end(), dataTypes.begin(), getPointer<PointPair>);

			vector<vector<unsigned int>> indices;
			RandomSampler randomSampler(4, 2, &EuclideanDistancePointPair);
			initRandomSampler(randomSampler, dataTypes, numOfPoints);
			randomSampler.GetNSample(indices, numOfModels);

			for (int si = 0; si < indices.size(); si++) {
				vector<PointPair> selectedPointPairs;

				for (int j = 0; j < indices[si].size(); j++) {
					selectedPointPairs.push_back(pointPairGroup[indices[si][j]]);
				}

				homographies.push_back(PlaneFitting::computeMWHomography(selectedPointPairs, K, K, R, normal));
				normals.push_back(normal);
			}
		}
		
		return homographies;
	}

	vector<Mat> generateMWHomographies(const vector<CDataType*> &pointPairs,
		const vector<vector<unsigned int>> &indices, const int minimumSampleSet,
		const Mat &K1, const Mat &K2, const Mat &R, const Mat &VD1, vector<Mat> &normals) {

		vector<Mat> homograhpies;
		Mat VL = computeVanishingLines(K1, VD1);

		// Generate models
		for (int i = 0; i < (int)indices.size(); i++) {
			vector<PointPair> selectedPointPairs;

			for (int j = 0; j < minimumSampleSet; j++) {
				selectedPointPairs.push_back(*dynamic_cast<PointPair*>(pointPairs[indices[i][j]]));
			}

			for (int k = 0; k < VD1.cols; k++) {
				vector<PointPair> pointPairCandidates = pickLargerGroupVL(selectedPointPairs, VL);

				if (pointPairCandidates.size() >= minimumSampleSet) {
					homograhpies.push_back(computeMWHomography(pointPairCandidates, K1, K2, R, VD1.col(k)));
					normals.push_back(VD1.col(k));
				}
			}
		}

		return homograhpies;
	}

	vector<Mat> generateWMWHomographies(const vector<CDataType*> &pointPairs,
		const vector<vector<unsigned int>> &indices, const int minimumSampleSet,
		const Mat &K1, const Mat &K2, const Mat &R) {

		vector<Mat> homograhpies;

		// Generate models
		for (int i = 0; i < (int)indices.size(); i++) {
			vector<PointPair> selectedPointPairs;

			for (int j = 0; j < minimumSampleSet; j++) {
				selectedPointPairs.push_back(*dynamic_cast<PointPair*>(pointPairs[indices[i][j]]));
			}

			homograhpies.push_back(computeWeakMWHomography3pts(selectedPointPairs, K1, K2, R));
		}

		return homograhpies;
	}

	vector<Mat> generateHomographies(const vector<CDataType*> &pointPairs,
		const vector<vector<unsigned int>> &indices, const int minimumSampleSet) {
		vector<Mat> homograhpies;

		// Generate models
		for (int i = 0; i < (int)indices.size(); i++) {
			vector<Point2f> selectedPoints1, selectedPoints2;

			for (int j = 0; j < minimumSampleSet; j++) {
				PointPair pointPair = *dynamic_cast<PointPair*>(pointPairs[indices[i][j]]);

				selectedPoints1.push_back(pointPair.Point1);
				selectedPoints2.push_back(pointPair.Point2);
			}

			if (selectedPoints1.size() >= minimumSampleSet) {
				Mat H = findHomography(selectedPoints1, selectedPoints2);
				if (H.empty()) {
					H = Mat::eye(3, 3, CV_32F);
				}
				homograhpies.push_back(H);
			}
		}

		return homograhpies;
	}

	vector<Mat> computeHomographies(const PointPairClusters &pointPairClusters, const int method) {
		vector<Mat> homographies;

		for (const PointPairCluster &pointPairCluster : pointPairClusters.Clusters) {
			Mat H;
			if (pointPairCluster.size() > 4) {
				H = findHomography(pointPairCluster.getArrayOfFirstPoints(),
					pointPairCluster.getArrayOfSecondPoints(), method);
			}

			if (H.empty()) {
				H = Mat::eye(3, 3, CV_32F);
			}

			homographies.push_back(H);
		}
		
		return homographies;
	}

	vector<Mat> computeHomographies(const vector<PointPairCluster> pointPairClusters, const int method) {
		vector<Mat> homographies;

		for (const PointPairCluster &pointPairCluster : pointPairClusters) {
			Mat H;
			if (pointPairCluster.size() > 4) {
				H = findHomography(pointPairCluster.getArrayOfFirstPoints(),
					pointPairCluster.getArrayOfSecondPoints(), method);
			}
			else {
				H = Mat::eye(3, 3, CV_32F);
			}

			homographies.push_back(H);
		}

		return homographies;
	}

	vector<Mat> computeMWHomographies(const PointPairClusters &pointPairClusters,
		const Mat &K1, const Mat &K2, const Mat &R, const Mat &VD, vector<Mat> &normals) {
		vector<Mat> homographies;

		for (const PointPairCluster &pointPairCluster : pointPairClusters.Clusters) {
			Mat normal, H;

			if (!VD.empty()) {
				normal = VD.col(getClosestDirectionalColumn(pointPairCluster.PlaneNormal, VD));
			}
			else {
				normal = pointPairCluster.PlaneNormal;
			}

			if (pointPairCluster.PointPairs.size() >= 2) {
				H = PlaneFitting::computeMWHomography(pointPairCluster.PointPairs, K1, K2, R, normal);
			}
			else {
				H = Mat::eye(3, 3, CV_32F);
			}

			homographies.push_back(H);
			normals.push_back(normal);
		}

		return homographies;
	}

	vector<Mat> computeNormals(const PointPairClusters &pointPairClusters, const Mat &VD) {
		vector<Mat> normals;

		for (const PointPairCluster &pointPairCluster : pointPairClusters.Clusters) {
			Mat normal, H;

			if (!VD.empty()) {
				normal = VD.col(getClosestDirectionalColumn(pointPairCluster.PlaneNormal, VD));
			}
			else {
				normal = pointPairCluster.PlaneNormal;
			}

			normals.push_back(normal);
		}

		return normals;
	}

	PointPairClusters mergeClusters(const PointPairClusters &pointPairClusters,
		const Mat &K1, const Mat &K2, const Mat &R, const Mat &VD, const float threshold,
		const int method, const Mat debugImage1) {

		const float sameDirCosThreshold = cosf(15.f * CV_PI / 180.f);
		bool converged = false;
		Mat VL = computeVanishingLines(K1, VD);
		vector<Mat> normals;
		vector<Mat> homographies = computeMWHomographies(pointPairClusters, K1, K2, R, VD, normals);
		vector<Mat> VLs = computeVanishingLines(normals, VL, VD);

		vector<PointPairCluster> originalClusters = pointPairClusters.Clusters;
		vector<PointPairCluster> tmpClusters =
			pointPairClusters.computeInliers(homographies, VLs, normals, threshold);

		assert(tmpClusters.size() == pointPairClusters.size());

		//imwrite("results\\label_init.png", drawPointsL(debugImage1, tmpClusters, 2));

		while (!converged) {
			converged = true;

			// Compute pairwise Jaccard distances
			multimap<double, pair<int, int>, less<double>> priorityQueueJD;
			for (int i = 0; i < (int)tmpClusters.size() - 1; i++) {
				for (int j = i + 1; j < (int)tmpClusters.size(); j++) {
					
					// Check the following things:
					if (countNonZero(VLs[i] != VLs[j]) == 0 &&	// 1) If given clusters are on the same side of the vanishing line
						abs(normals[i].dot(normals[j])) > sameDirCosThreshold)	// 2) If given clusters share the same normal direction
					{
						double dist = computeJaccardDistance(tmpClusters[i], tmpClusters[j]);
						//double dist = computeOverlapCoefficient(clusters_tmp[i], clusters_tmp[j]);
						priorityQueueJD.insert(pair<double, pair<int, int>>(dist, pair<int, int>(i, j)));
					}
				}
			}

			// find cluster pair to be merged
			map<double, pair<int, int>>::iterator iter = priorityQueueJD.begin();
			while (iter != priorityQueueJD.end()) {
				int ind1 = iter->second.first;
				int ind2 = iter->second.second;

				// Merge two candidate clusters
				PointPairCluster clusterUnion = originalClusters[ind1];
				for (const PointMatch &pointMatch : originalClusters[ind2].PointPairs) {
					clusterUnion.addPointPair(pointMatch);
				}

				// Assume that there is no way to compute normal from point pairs
				Mat homography = computeMWHomography(clusterUnion.PointPairs, K1, K2, R, clusterUnion.PlaneNormal);

				PointPairCluster clusterUnionExt =
					pointPairClusters.computeInliers(homography, VLs[ind1], clusterUnion.PlaneNormal, threshold);

				if (clusterUnionExt.size() == 0) {
					iter++;
					continue;
				}

				double similarity = computeJaccardDistance(clusterUnion, clusterUnionExt);

				if (similarity < 0.5) {
					originalClusters[ind1].addPointPairs(originalClusters[ind2].PointPairs);
					tmpClusters[ind1] = clusterUnionExt;

					// Remove old clusters and its related variables
					originalClusters.erase(originalClusters.begin() + ind2);
					tmpClusters.erase(tmpClusters.begin() + ind2);
					VLs.erase(VLs.begin() + ind2);
					normals.erase(normals.begin() + ind2);
					converged = false;

					//imwrite("results\\merged.png", drawPointsL(debugImage1, tmpClusters, 2));
					break;
				}

				iter++;
			}
		}

		// Compute inliers of each cluster again
		/*PointPairClusters result(originalClusters, pointPairClusters.getAllPointMatches());
		homographies = computeHomographies(originalClusters, method);

		originalClusters = result.computeInliers(homographies, VLs, normals, threshold);

		//return PointPairClusters(tmpClusters, pointPairClusters.getAllPointMatches());*/
		return PointPairClusters(originalClusters, pointPairClusters.getAllPointMatches(), threshold);
	}

	PointPairClusters mergeClusters(const PointPairClusters &pointPairClusters, const Mat &K1,
		const Mat &VD, const float threshold, const int method, const Mat debugImage1) {

		const float sameDirCosThreshold = cosf(15.f * CV_PI / 180.f);
		bool converged = false;
		Mat VL = computeVanishingLines(K1, VD);
		vector<Mat> normals = computeNormals(pointPairClusters, VD);
		vector<Mat> homographies = computeHomographies(pointPairClusters, method);
		vector<Mat> VLs = computeVanishingLines(normals, VL, VD);

		vector<PointPairCluster> originalClusters = pointPairClusters.Clusters;
		vector<PointPairCluster> tmpClusters =
			pointPairClusters.computeInliers(homographies, VLs, normals, threshold);

		assert(tmpClusters.size() == pointPairClusters.size());

		while (!converged) {
			converged = true;

			//imwrite("results\\label_init.png", drawPointsL(debugImage1, originalClusters, 2));

			// Compute pairwise Jaccard distances
			multimap<double, pair<int, int>, less<double>> priorityQueueJD;
			for (int i = 0; i < (int)tmpClusters.size() - 1; i++) {
				for (int j = i + 1; j < (int)tmpClusters.size(); j++) {

					// Check the following things:
					if (isCentroidOnSameSide(tmpClusters[i], tmpClusters[j], VLs[i]) &&	// 1) If given clusters are on the same side of the vanishing line
						abs(normals[i].dot(normals[j])) > sameDirCosThreshold)	// 2) If given clusters share the same normal direction
					{
						double dist = computeJaccardDistance(tmpClusters[i], tmpClusters[j]);
						priorityQueueJD.insert(pair<double, pair<int, int>>(dist, pair<int, int>(i, j)));
					}
				}
			}

			// find cluster pair to be merged. The pair with smaller Jaccard Distance will be checked first.
			map<double, pair<int, int>>::iterator iter = priorityQueueJD.begin();
			while (iter != priorityQueueJD.end()) {
				int ind1 = iter->second.first;
				int ind2 = iter->second.second;

				// Merge two candidate clusters
				PointPairCluster clusterUnion = originalClusters[ind1];
				for (const PointMatch &pointMatch : originalClusters[ind2].PointPairs) {
					clusterUnion.addPointPair(pointMatch);
				}

				// Assume that there is no way to compute normal from point pairs
				Mat homography = findHomography(clusterUnion.getArrayOfFirstPoints(),
					clusterUnion.getArrayOfSecondPoints(), method);

				PointPairCluster clusterUnionExt =
					pointPairClusters.computeInliers(homography, VLs[ind1], clusterUnion.PlaneNormal, threshold);

				if (clusterUnionExt.size() == 0) {
					iter++;
					continue;
				}

				double similarity = computeJaccardDistance(clusterUnion, clusterUnionExt);

				//imwrite("results\\cluster_union.png", drawPoints(debugImage1, clusterUnion.PointPairs, 2));
				//imwrite("results\\cluster_unionExt.png", drawPoints(debugImage1, clusterUnionExt.PointPairs, 2));

				if (similarity < 0.7) {
					// Combine selected clusters
					originalClusters[ind1].addPointPairs(originalClusters[ind2].PointPairs);
					tmpClusters[ind1] = clusterUnionExt;

					// Remove old clusters and its related variables
					originalClusters.erase(originalClusters.begin() + ind2);
					tmpClusters.erase(tmpClusters.begin() + ind2);
					VLs.erase(VLs.begin() + ind2);
					normals.erase(normals.begin() + ind2);
					converged = false;

					//imwrite("results\\merged.png", drawPointsL(debugImage1, originalClusters, 2));
					//imwrite("results\\merged67.png", drawPoints(debugImage1, originalClusters[ind1].PointPairs, 2));

					break;
				}

				iter++;
			}
		}
		
		// Compute inliers of each cluster again
		PointPairClusters result(originalClusters, pointPairClusters.getAllPointMatches());
		homographies = computeHomographies(originalClusters, method);

		originalClusters = result.computeInliers(homographies, VLs, normals, threshold);

		//return PointPairClusters(tmpClusters, pointPairClusters.getAllPointMatches());
		return PointPairClusters(originalClusters, pointPairClusters.getAllPointMatches());
	}

	PointPairClusters mergeClusters(const PointPairClusters &pointPairClusters,
		const float threshold, const int method) {

		double mergeThreshold = 0.2;
		bool converged = false;
		vector<Mat> homographies = computeHomographies(pointPairClusters, method);

		vector<PointPairCluster> originalClusters = pointPairClusters.Clusters;
		vector<PointPairCluster> tmpClusters =
			pointPairClusters.computeInliers(homographies, vector<Mat>(), vector<Mat>(), threshold);

		assert(tmpClusters.size() == pointPairClusters.size());

		//imwrite("results\\label_init.png", drawPointsL(debugImage1, tmpClusters, 2));

		while (!converged) {
			converged = true;

			// Compute pairwise Jaccard distances
			multimap<double, pair<int, int>, less<double>> priorityQueueJD;
			for (int i = 0; i < (int)tmpClusters.size() - 1; i++) {
				for (int j = i + 1; j < (int)tmpClusters.size(); j++) {
					double dist = computeJaccardDistance(tmpClusters[i], tmpClusters[j]);
					//double dist = computeOverlapCoefficient(clusters_tmp[i], clusters_tmp[j]);
					priorityQueueJD.insert(pair<double, pair<int, int>>(dist, pair<int, int>(i, j)));
				}
			}

			// find cluster pair to be merged
			map<double, pair<int, int>>::iterator iter = priorityQueueJD.begin();
			while (iter != priorityQueueJD.end()) {
				int ind1 = iter->second.first;
				int ind2 = iter->second.second;

				// Merge two candidate clusters
				PointPairCluster clusterUnion = originalClusters[ind1];
				for (const PointMatch &pointMatch : originalClusters[ind2].PointPairs) {
					clusterUnion.addPointPair(pointMatch);
				}

				// Assume that there is no way to compute normal from point pairs
				Mat homography = findHomography(clusterUnion.getArrayOfFirstPoints(), clusterUnion.getArrayOfSecondPoints(), method);

				PointPairCluster clusterUnionExt =
					pointPairClusters.computeInliers(homography, Mat(), Mat(), threshold);

				if (clusterUnionExt.size() == 0) {
					iter++;
					continue;
				}

				double similarity = computeJaccardDistance(clusterUnion, clusterUnionExt);

				if (similarity < mergeThreshold) {
					originalClusters[ind1].addPointPairs(originalClusters[ind2].PointPairs);
					tmpClusters[ind1] = clusterUnionExt;

					// Remove old clusters and its related variables
					originalClusters.erase(originalClusters.begin() + ind2);
					tmpClusters.erase(tmpClusters.begin() + ind2);
					converged = false;

					//imwrite("results\\merged.png", drawPointsL(debugImage1, tmpClusters, 2));
					break;
				}

				iter++;
			}
		}

		// Compute inliers of each cluster again
		/*PointPairClusters result(originalClusters, pointPairClusters.getAllPointMatches());
		homographies = computeHomographies(originalClusters, method);

		originalClusters = result.computeInliers(homographies, VLs, normals, threshold);

		//return PointPairClusters(tmpClusters, pointPairClusters.getAllPointMatches());*/
		return PointPairClusters(originalClusters, pointPairClusters.getAllPointMatches(), threshold);
	}

	vector<PointPairCluster> multiplePlaneDetection(const PointPairClusters &pointPairClusters,
		const Size &imageSize, const float threshold) {
		bool is_updated = true;
		vector<PointPairCluster> clusters = pointPairClusters.Clusters;

		// Global merging
		while (is_updated) {
			is_updated = false;

			// Compute Jaccard distances
			multimap<double, pair<int, int>, greater<double>> hashmap;
			for (int i = 0; i < (int)clusters.size() - 1; i++) {
				for (int j = i + 1; j < (int)clusters.size(); j++) {
					if (clusters[i].size() < 4 || clusters[j].size() < 4) {
						continue;
					}

					double dist = MPDMergeDistance(clusters[i], clusters[j]);
					if (dist <= threshold) {
						hashmap.insert(std::pair<double, std::pair<int, int>>(dist, std::pair<int, int>(i, j)));
					}
				}
			}

			// find cluster pair to be merged
			if (!hashmap.empty()) {
				int ind1 = hashmap.begin()->second.first;
				int ind2 = hashmap.begin()->second.second;

				clusters[ind1].addPointPairs(clusters[ind2].PointPairs);
				clusters.erase(clusters.begin() + ind2);

				is_updated = true;
			}
		}

		// Make sure that all the points have the positive numbers
		double min_x = 0, min_y = 0, max_x = imageSize.width, max_y = imageSize.height;
		for (const PointPairCluster &pointPairCluster : pointPairClusters.Clusters) {
			for (const PointMatch &pointMatch : pointPairCluster.PointPairs) {
				if (pointMatch.first.x < min_x) {
					min_x = pointMatch.first.x;
				}
				if (pointMatch.first.y < min_y) {
					min_y = pointMatch.first.y;
				}
				if (pointMatch.first.x > max_x) {
					max_x = pointMatch.first.x;
				}
				if (pointMatch.first.y > max_y) {
					max_y = pointMatch.first.y;
				}
			}
		}

		if (min_x < 0) {
			for (int i = 0; i < clusters.size(); i++) {
				for (int j = 0; j < clusters[i].size(); j++) {
					clusters[i][j].first.x -= min_x;
				}
			}
			max_x -= min_x;
		}
		if (min_y < 0) {
			for (int i = 0; i < clusters.size(); i++) {
				for (int j = 0; j < clusters[i].size(); j++) {
					clusters[i][j].first.y -= min_y;
				}
			}
			max_y -= min_y;
		}

		// Spatial analysis
		vector<PointPairCluster> new_cluster;
		for (size_t i = 0; i < clusters.size(); i++)
		{
			Subdiv2D subdiv(Rect(0, 0, max_x, max_y));
			vector<Vec4f> edgeList;
			vector<float> distanceList;
			vector<int> label;
			float mean = 0.0;
			float stddev = 0.0;
			int num_label;

			for (const PointMatch &pointMatch : pointPairClusters[i].PointPairs) {
				subdiv.insert(pointMatch.first);
			}

			subdiv.getEdgeList(edgeList);

			for (size_t ei = 0; ei < edgeList.size(); ei++) {
				if (edgeList[ei][0] > imageSize.width || edgeList[ei][1] > imageSize.height ||
					edgeList[ei][0] < 0 || edgeList[ei][1] < 0 ||
					edgeList[ei][2] > imageSize.width || edgeList[ei][3] > imageSize.height ||
					edgeList[ei][2] < 0 || edgeList[ei][3] < 0) {
					continue;
				}

				distanceList.push_back(sqrt(pow((edgeList[ei][0] - edgeList[ei][2]), 2) + pow((edgeList[ei][1] - edgeList[ei][3]), 2)));
				mean += distanceList.back();
			}
			mean /= (float)edgeList.size();

			for (size_t ei = 0; ei < distanceList.size(); ei++)
				stddev += pow(distanceList[ei] - mean, 2);
			stddev = sqrt(stddev / (float)edgeList.size());

			num_label = partition(clusters[i].getArrayOfFirstPoints(), label, EqKeypoints(stddev));
			vector<PointPairCluster> split_cluster(num_label);

			for (size_t j = 0; j < label.size(); j++) {
				split_cluster[label[j]].addPointPair(clusters[i][j]);
			}

			for (const PointPairCluster &pointPairCluster : split_cluster) {
				if (pointPairCluster.size() >= 6) {
					new_cluster.push_back(pointPairCluster);
				}
			}
		}

		// restore the original coordinates
		if (min_x < 0) {
			for (int i = 0; i < new_cluster.size(); i++) {
				for (int j = 0; j < new_cluster[i].size(); j++) {
					new_cluster[i][j].first.x += min_x;
				}
			}
		}
		if (min_y < 0) {
			for (int i = 0; i < new_cluster.size(); i++) {
				for (int j = 0; j < new_cluster[i].size(); j++) {
					new_cluster[i][j].first.y += min_y;
				}
			}
		}
		clusters = new_cluster;

		// Robust fitting and stability checks
		for (int i = 10; i >= 0; i--) {
			if (clusters.empty()) {
				return vector<PointPairCluster>();
			}
			clusters = PointPairCluster::computeInliers(clusters, pointPairClusters.getAllPointMatches(),
				threshold * (1.0 + float(i) * 0.2));
		}

		return clusters;
	}
}
