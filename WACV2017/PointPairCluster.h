#pragma once

#include <vector>
#include <opencv2\opencv.hpp>

#include "types.h"

class PointPairCluster {
public:
	const std::vector<PointMatch> &PointPairs;

	const cv::Mat &PlaneNormal;

	void addPointPair(const PointMatch &pointPair);

	void addPointPair(const cv::Point2f &point1, const cv::Point2f &point2);

	void addPointPairs(const std::vector<PointMatch> &pointPairs);

	void setPlaneNormal(const cv::Mat &normal);

	// Get an array of first points in the point matches
	std::vector<cv::Point2f> getArrayOfFirstPoints() const;

	// Get an array of second points in the point matches
	std::vector<cv::Point2f> getArrayOfSecondPoints() const;

	size_t size() const;

	PointPairCluster();
	PointPairCluster(const PointPairCluster& pointPairCluster);
	PointPairCluster(const cv::Mat &normal);
	PointPairCluster(const cv::Mat &normal, std::vector<PointMatch> &pointPairs);

	PointPairCluster& operator=(const PointPairCluster &pointPairCluster);

	// Direct access to each line
	PointMatch &operator[](const int index);

	// Direct access to each line for read-only
	const PointMatch &operator[](const int index) const;

	static std::vector<PointPairCluster> computeInliers(const std::vector<PointPairCluster> &pointPairClusters,
		const std::vector<PointMatch> &pointMatches, const float threshold);

	static PointPairCluster computeInliers(const cv::Mat &H,
		const std::vector<PointMatch> &pointMatches, const float threshold);

private:
	std::vector<PointMatch> m_pointPairs;

	cv::Mat m_normal;
};
