#pragma once

#include "TLinkage\TLinkage.h"
#include "TLinkage\RandomSampler.h"
#include "LineClusters.h"

#include <opencv2\line_descriptor.hpp>

//-------------------------------------
// Utility functions
//-------------------------------------

// Compute distance between given model and line.
// It first computes the line connecting the mid-point of given line sample (line) and vanishing point (model).
// Then, residual of the starting point of given line with respect to the computed line will be defined as the distance.
inline double LineDistance(const cv::Mat &model, const CDataType &line);

// Compute distance between given model and line.
// The distance is define as the residual of given line points by computing a line connecting the mid-point of given line and vanishing point.
inline double LineDistance(const CDataType &model, const CDataType &line);

inline double LineDistance(const cv::Point2f &model, const CDataType &line);

// Compute distance between given point and line.
inline double LineDistance(const cv::Point2f &point, const float *line);

inline double LineDistance(const cv::Point2f &model, const cv::line_descriptor::KeyLine &line);

// Convert clusters list into vectors of point pairs.
void convertClustersType(const std::list<sClLnk *> &clustersList,
	std::vector<LineCluster> &clusterArray, int minCardinarity = 0);

// Compare x-coordinate of two lines' mid-points. If x coordinate of the first line is smaller,
// it will return true. Otherwise, it will return false.
bool compLinesX(const Line&, const Line&);

// Compare y-coordinate of two lines' mid-points. If y coordinate of the first line is smaller,
// it will return true. Otherwise, it will return false.
bool compLinesY(const Line&, const Line&);

bool compXCoordinates(const Line &line, const float &val);

bool compYCoordinates(const Line &line, const float &val);

//-------------------------------------
// Supportive utility functions
//-------------------------------------

void convertClustersType(const std::list<sClLnk *> &clustersList,
	LineClusters &lineClusters, int minCardinarity);

// For given cluster of lines, compute inliers using vanishing points of each cluster
void computeInlierLines(const std::list<sClLnk *> &clustersList, LineClusters &lineClusters,
	int minCardinarity, float threshold, bool allowDuplicates);

void computeInlierLines(const std::list<sClLnk *> &clustersList, std::vector<CDataType*> alllines,
	LineClusters &lineClusters, int minCardinarity, float threshold);

void computeInlierLines(const std::list<sClLnk *> &clustersList, std::vector<CDataType*> alllines,
	LineClusters &lineClusters, int minCardinarity, float threshold);

// Generate LineClusters from given vanishing points.
LineClusters generateClusters(const std::vector<CDataType*> &lines,
	const std::vector<cv::Point2f> &VPs, const float threshold);

// Generate LineClusters from given vanishing directions and the calibration matrix.
LineClusters generateClusters(const std::vector<CDataType*> &lines,
	const cv::Mat &VD, const cv::Mat &K, const float threshold);

// Find indices of clusters such that the vanishing direction of a cluster is aligned with
// one of given vanishing directions.
std::vector<int> selectClusters(const LineClusters &lineClusters, const cv::Mat &VDs, const cv::Mat &K);

//-------------------------------------
// Drawing functions
//-------------------------------------

// Draw lines on given image
cv::Mat drawLines(const cv::Mat &image, std::vector<CDataType*> &lines, int lineWidth);

cv::Mat drawLineCluster(const cv::Mat &image, const LineCluster &lineCluster,
	const int lineWidth, const cv::Scalar &color = CV_RGB(255, 0, 0));

// Draw line clusters on given image
cv::Mat drawLineClusters(const cv::Mat &image,
	const std::vector<LineCluster> &lineClusters, int lineWidth);

// Draw line clusters on given image
cv::Mat drawLineClusters(const cv::Mat &image,
	const LineClusters &lineClusters, const int lineWidth, const bool convertGray = false, const std::vector<int> &indices = std::vector<int>());

// Draw line clusters on given image
cv::Mat drawLineClustersL(const cv::Mat &image,
	const LineClusters &lineClusters, int lineWidth, const std::vector<int> &indices = std::vector<int>());

// Draw lines in cluster with colors corresponding to given values
cv::Mat drawLineCluster(const cv::Mat &image, const LineCluster &lineCluster, const std::vector<float> &values);

//-------------------------------------
// Line functions
//-------------------------------------

// Detect line from given image
std::vector<CDataType*> detectLines(const cv::Mat &image, float lineLengthThreshold = 50.f);

// Generate a mask of empty space, where the empty space is represented as a zero-intensity.
cv::Mat generateEmptySpaceMask(const cv::Mat &grayImage, int kernelSize = 3);

// Merge lines if a pair of lines have similar line property.
void mergeLines(std::vector<cv::line_descriptor::KeyLine> &lines,
	float lineGapThreshold = 30.f, float lineDistThreshold = 1.0f);

// Line merging using the algorithm in Paper 1995 JMRS Manuel A New Approach for Merging Edge Line Segments
// return: distance between the projected extrem points of two line segment onto the merged line -
//         total length of projected two line segment onto the merged line. If the value is negative,
//         then the two line segments were overlapped each other.
float mergeLines(const cv::line_descriptor::KeyLine &line1,
	const cv::line_descriptor::KeyLine &line2, cv::line_descriptor::KeyLine &newLine);

//-------------------------------------
// Line Clustering Functions
//-------------------------------------

// Generate vanishing points with given lines.
std::vector<cv::Mat> generateVPModels(std::vector<CDataType*> &lines, unsigned int numOfModels);

//-------------------------------------
// Main Entry
//-------------------------------------

// Test line clustering using T-linkage with given image
LineClusters LineClustering(cv::Mat image, int minCardinarity, float threshold = 2.0f,
	float lineLengthThreshold = 50.f, bool allowDuplicates = true);

// Cluster lines with given vanishing directions.
LineClusters LineClustering(const cv::Mat &image, const cv::Mat &VD, const cv::Mat &K,
	const float threshold, const float lineLengthThreshold, const bool allowDuplicates = false);
