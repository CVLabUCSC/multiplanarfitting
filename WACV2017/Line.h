#pragma once;

#include <opencv2\opencv.hpp>

class Line {
public:
	// Start point of the line (read-only)
	cv::Point2f &StartPoint;

	// End point of the line (read-only)
	cv::Point2f &EndPoint;

	// Vector representation of the line (read-only)
	cv::Mat &LineVector;

	// Constructors
	Line(const Line &l);
	Line(const cv::Point2f &startPoint, const cv::Point2f &endPoint, const bool isDummyLine = false);

	// Compute length of the line
	float length() const;

	// Compute mid-point
	cv::Point2f getMidPoint() const;

	// Set endpoints of the line
	void setPoints(const cv::Point2f &startPoint, const cv::Point2f &endPoint,
		const bool isDummyLine = false, bool isVerticalLine = false);

	// Swap start and end points
	void swapPoints();

	// Compute distance between this line and given point. If computeSign is set to true,
	// distance will be computed with sign.
	float computeDistance(const cv::Point2f &point, bool computeSign = false) const;

	// Compute sign with respect to this line.
	float computeSign(const cv::Point2f &point) const;

	// Compute an intersection point of the line and given line
	cv::Point2f getIntersection(const Line &line) const;

	// Compute an intersection point of the line and given line, which is expressed as its normal.
	cv::Point2f getIntersection(const cv::Mat &normal) const;

	// Return either StartPoint or EndPoint that is further from given point.
	cv::Point2f getFurtherPointFrom(const cv::Point2f &fromPoint) const;

	// If given point is on the line, return true.
	bool Line::isPointOnLine(cv::Point2f testPoint) const;

	// If projection of given point onto the line is actually on the range of the line,
	// it will return true. Otherwise, it will return false.
	bool isProjectable(const cv::Point2f &point) const;

	// If an intersection of this line and given line is on one of the lines, it will return true.
	bool isProjectable(const Line &line) const;

	// If extension of this line is intersect with given line, returns true.
	bool isIntersectable(const Line &line, float allowance = 0.f) const;

	// Check if at least some part of this line is between line1 and line2
	bool isBetween(const Line &line1, const Line &line2) const;

	// If the line is not a real line, returns true.
	bool isDummyLine() const;

	// Overload operators
	Line& operator=(const Line &l);
	bool operator== (const Line &l) const;

	// Compute normal of a line connecting pt1 and pt2.
	static cv::Mat computeLineNormal(const cv::Point2f &pt1, const cv::Point2f &pt2);

	// Compute distance between given model and line.
	// It first computes the line connecting the mid-point of given line sample (line) and vanishing point (model).
	// Then, residual of the starting point of given line with respect to the computed line will be defined as the distance.
	static float LineDistance(const cv::Point2f &model, const Line &line);


private:
	// Start point of the line
	cv::Point2f m_startPoint;

	// End point of the line
	cv::Point2f m_endPoint;

	// Vector representation of the line
	cv::Mat m_lineVector;

	// If the line is virtual line, set it true.
	bool m_isDummyLine;
};

// Compute cross-product of given 3D vectors
inline void vec_cross(float a1, float b1, float c1,
	float a2, float b2, float c2,
	float& a3, float& b3, float& c3);

// Normalize the given 3D vector
inline void vec_norm(float& a, float& b, float& c);

// Given three colinear points p, q, r, the function checks if point q lies on line segment 'pr'
inline bool onSegment(cv::Point2f p, cv::Point2f q, cv::Point2f r);

class LineHash
{
public:
	std::size_t operator()(const Line &l) const;
};