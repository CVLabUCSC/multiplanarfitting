#pragma once

#include "LineCluster.h"

#include <unordered_map>

struct ClusterLineIndices {
	int ClusterIndex;
	int Line1Index;
	int Line2Index;
};

class LineClusters {
public:
	// Read-only cluster reference
	const std::vector<LineCluster> &Clusters;

	// Temporary
	cv::Mat debugImage;

	// Constructors
	LineClusters();
	LineClusters(const LineClusters &lineClusters);
	LineClusters(const std::vector<LineCluster> &lineClusters);

	// Add dummy lines to vertical and horizontal lines.
	// No lines will be added to coplanar groups of lines
	void addDummyLines(const cv::Size &imageSize);

	// Remove dummy lines from each cluster
	void removeDummyLines();

	// Add one LineCluster
	void addLineCluster(const LineCluster &lineCluster);

	// Add a line as an outlier.
	void addLine(const Line& l);

	// Add lines as outliers. If a line already exists, it will not be added.
	void addLines(const std::vector<Line> &lines);

	// Clear line clusters
	void clear();

	// Sort clusters by their cardinality
	void sort();

	// Return the number of clusters
	size_t size() const;

	// Compute cardinality of given vanishing point
	int computeCardinality(cv::Point2f vanishingPoint, float threshold, bool fromOutliers = true) const;

	// Get sub-clusters of this clusters.
	// If recomputeInliers is set, it will recompute inliers of each cluster in the sub-clusters.
	LineClusters subCluster(const std::vector<int> &selectedIndices, const float threshold,
		const bool recomputeInliers = false) const;

	void findClosestVerticalLine(const cv::Point2f &point, int &lineIndex, int &VPIndex);

	void findClosestLine(const cv::Point2f &point, int &clusterIndex, int &lineIndex, int &VPIndex);

	// Find a region, which is extended from given point.
	std::vector<cv::Point> findRegion(const cv::Point2f &point,
		ClusterLineIndices &clusterLineIndices = ClusterLineIndices(),
		ClusterLineIndices &pairLineIndices = ClusterLineIndices());

	// Find a region, which is extended from the center point of given rectangle.
	// Lines that are completely contained by the region will not be considered.
	std::vector<cv::Point> findRegion(const cv::Point2f &point, const cv::Rect &region,
		ClusterLineIndices &clusterLineIndices = ClusterLineIndices(),
		ClusterLineIndices &pairLineIndices = ClusterLineIndices());

	bool findRegion(const cv::Point2f &centerPoint, const cv::Size &imageSize,
		std::vector<cv::Point2f> &points, const float minLineLength, ClusterLineIndices &clusterLineIndices,
		ClusterLineIndices &pairLineIndices) const;

	// Direct access to each cluster
	LineCluster &operator[](const int index);

	// Direct access to each cluster for read-only
	const LineCluster &operator[](const int index) const;

	// Overload operator
	LineClusters& operator=(const LineClusters &lineClusters);

private:
	// Vector of lines that are not include to the cluster
	std::vector<Line> m_outlierLines;

	// Vector of line clusters
	std::vector<LineCluster> m_clusters;

	// Indexer of a line. Note that each line can be added to more than one cluster.
	std::unordered_map<Line, int, LineHash> m_lineIndexer;

	// Construct line indexer.
	void constructLineIndexer();

	// Find the closest line, where the distance to given point is the minimum.
	// Distance between a line and centerPoint is measured in vanishing direction of one of another clusters.
	// Indices of found line and its cluster will be stores to lineIdx and clusterIdx, respectively.
	void findClosestLine(const cv::Point2f &centerPoint, int &clusterIdx, int &lineIdx, cv::Rect exclude = cv::Rect());

	// Find the closest line, where the distance to given point is the minimum.
	// Distance between a line and centerPoint is measured in the vanishing direction(VD) of cluster of given index.
	// Lines of the VD's cluster will not be considered. Also, only lines on the area that is formed by
	// connecting the VD and given lines will be considered.
	void findClosestLineBetween(const cv::Point2f &centerPoint, const int VDClusterIdx, const Line& line1,
		const Line& line2, int &clusterIdx, int &lineIdx, cv::Rect exclude = cv::Rect());

	// Find the closest line in the cluster of given clusterIndex. Only the lines of the opposite side of
	// a line connecting centerPoint and support vanishing point, which is the vanishing point of the cluster
	// of given index supClusterIndex, will be considered. It prefers to find a 'projectable' line,
	// but if no projectable line is not available, it will return the other closest line that meets
	// the all of other constraints.
	// Returns the index of found line. If no line was found, negative value will be returned.
	int findClosestLineOpposite(const cv::Point2f &centerPoint, const int clusterIndex, const int lineIndex,
		const int supClusterIndex, const int supLineIndex, cv::Rect exclude = cv::Rect());

	// Find the closest line of given point.
	// crossLineNormal: A line of normal that is used to compute intersection point. Basically, this line is
	//	computed by connecting a vanishing point of cluster and given point.
	// intersectPoint: intersection point of lineNormal and the selected closest line.
	void findClosestLine(const cv::Point2f &point, ClusterLineIndices &clusterLineIndices,
		cv::Mat &crossLineNormal, cv::Point2f &intersectPoint) const;

	// Find the closest line on opposite site of line, which is
	// m_cluster[clusterLineIndices.ClusterIndex][clusterLineIndices.line1Index],
	// with respect to given point.
	void findClosestOppositeLine(const cv::Point2f &point, ClusterLineIndices &clusterLineIndices,
		const cv::Mat &crossLineNormal, const cv::Point2f &intersectPoint) const;

	// Find the closest line of given point in different cluster, between given lines.
	// The finding line must be in different cluster of given lines.
	void findClosestLineBetween(const cv::Point2f &point,
		const ClusterLineIndices &clusterLineIndices, ClusterLineIndices &pairLineIndices) const;

	std::vector<cv::Point> findRegion(const cv::Point2f &point, const cv::Size imageSize,
		const ClusterLineIndices &clusterLineIndices, ClusterLineIndices &pairLineIndices) const;

	std::vector<cv::Point> computePolynomial(const cv::Point2f centerPoint,
		const ClusterLineIndices &clusterLineIndices, const ClusterLineIndices &pairLineIndices) const;

	Line computeIncompleteLine(const cv::Point2f centerPoint,
		const ClusterLineIndices &compLineIndices, const ClusterLineIndices &imcompLineIndices) const;
};

// Compute the maximum region of given lines.
// vanishingPoint is that of baseLine.
std::vector<cv::Point> computeMaximumRegion(const Line &baseLine, const cv::Point2f &vanishingPoint,
	const cv::Point2f &centerPoint, const Line &line1, const Line &line2);

//-------------------------------------
// Cluster Sorting
//-------------------------------------

// Sort clusters by the number of member points
bool sortClusterByPoints(const std::vector<std::pair<cv::Point2f, cv::Point2f>> &left,
	const std::vector<std::pair<cv::Point2f, cv::Point2f>> &right);

// Sort clusters by the number of lines
bool sortClusterByLines(const LineCluster &left, const LineCluster &right);
