#pragma once

#include <opencv2\opencv.hpp>
#include <iostream>
#include <vector>
#include <list>
#include "LineClustering.h"

// Fixed labels
const std::string GTSufix = "_GT";
const std::string VDSufix = "_VD";
const std::string OMSufix = "_OM";

const int KEY_ARROW_RIGHT = 2555904;
const int KEY_ARROW_LEFT = 2424832;
const int KEY_ARROW_UP = 2490368;
const int KEY_ARROW_DOWN = 2621440;

struct CallbackData {
	cv::Mat image;
	cv::String window_name;
	int roi_x0 = 0;
	int roi_y0 = 0;
	int roi_x1 = 0;
	int roi_y1 = 0;
	bool start_draw = false;
	bool need_update = false;
};

struct DataInfo {
	std::string dataFoldername;
	std::string datasetName;
	std::string filePrefix;
	std::string imageExtension;
	std::string dataExtension;
	int fileIndex;
	int secImgIdxBeg;
	int numOfImg;
	int imgIdxStep;
};

struct DataFiles {
	cv::String DataFolder;
	cv::String Image1filename;
	cv::String Image1GTfilename;
	cv::String Image1VDfilename;
	cv::String Image1OMfilename;
	cv::String CalibrationFilename;

	DataFiles(const DataInfo& dataInfo, int numZeros);
};

//----------------------------
// Utilities for experiments
//----------------------------

inline std::string buildFilename(std::string foldername, std::string filePrefix, char emptyCharacter,
	int emptylength, int fileIndex, std::string fileSufix, std::string fileExtension);

inline std::string buildOutputFolder(std::string dataname, int refFileIndex, int tarFileIndex);

inline std::string buildTestName(std::string dataname, int refFileIndex, int tarFileIndex);

inline std::string buildLogFilename();

std::vector<DataInfo> loadSetupFile(std::string setupFilename);

//----------------------------
// Experiment Functions
//----------------------------

bool testRegionalFitting(const cv::Mat &image1, const std::vector<CDataType*> &pointPairs,
	const LineClusters &lineClusters, const cv::Mat &K, const cv::Mat &R, const cv::Mat &VD);

bool vanishingPointDetection(const cv::Mat &image, cv::Mat K);

// Visualize the region generation algorithm
bool testRegionGenerator(const cv::Mat &image, const float threshold, const float lineLengthThreshold);

std::vector<cv::Point> testRegionGenerator(const cv::Mat &image, const LineClusters& lineClusters,
	const cv::Mat &K, std::vector<std::pair<int, int>> &selectedLines);

// Draw mouse windnow callback
void DrawRectangleMouseCallback(int event, int x, int y, int, void *data);

cv::Mat metricRectification(const cv::Point2f &vp1, const cv::Point2f &vp2,
	std::vector<std::pair<Line, Line>> &linePairs);

cv::Mat metricRectification(const std::vector<std::pair<Line, Line>> &pairLines);

//----------------------------
// Main entries
//----------------------------

void main_visualOdometry(std::string dataFilename);

void main_multiPlanarFitting(std::string dataFilename, int numZeros);

bool main_metricRectification();

void main_warpingTest(std::string dataFilename);

void main_testAll();

void main();
