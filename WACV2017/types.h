#pragma once

#include <cstdlib>
#include <opencv2\opencv.hpp>
#include <boost\functional\hash.hpp>

typedef std::pair<cv::Point2f, cv::Point2f> PointMatch;

template <class T>
class ReadOnly {
private:
	const T &data;
public:
	ReadOnly(const T & data_) : data(data_) {}
	T operator=(const T& arg) { data = arg; return data; }
	operator const T&() const { return data; }
};

class CDataType {
public:
	virtual unsigned int getPointDimension() const = 0;
	virtual float operator[] (std::size_t idx) = 0;
	virtual const float operator[] (std::size_t idx) const = 0;
};

class PointPair : public CDataType {
public:
	cv::Point2f Point1;
	cv::Point2f Point2;

	// Constructing the object with two points
	PointPair(const cv::Point2f &point1, const cv::Point2f &point2);

	// Get the number of total dimensions
	unsigned int getPointDimension() const;

	// Get an element of given index
	const float getElement(std::size_t idx) const;

	float operator[] (std::size_t idx);

	const float operator[] (std::size_t idx) const;
};

class CustomHash
{
public:
	std::size_t operator()(const PointMatch& lhs) const
	{
		std::size_t seed = 0;
		boost::hash_combine(seed, lhs.first.x);
		boost::hash_combine(seed, lhs.first.y);
		boost::hash_combine(seed, lhs.second.x);
		boost::hash_combine(seed, lhs.second.y);

		return seed;
	}

	std::size_t operator()(const cv::Point2f& lhs) const
	{
		std::size_t seed = 0;
		boost::hash_combine(seed, lhs.x);
		boost::hash_combine(seed, lhs.y);

		return seed;
	}
};
