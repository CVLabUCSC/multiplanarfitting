#include "Homography.h"
#include "Utilities.h"
#include "RegionSegmentation.h"

#include <type_traits>

namespace PlaneFitting {
	using namespace std;
	using namespace cv;

	Mat computeHomography(const std::vector<PointPair> &pointPairs) {
		assert(pointPairs.size() >= 4);

		vector<Point2f> points1, points2;

		for (const PointPair &pair : pointPairs) {
			points1.push_back(pair.Point1);
			points2.push_back(pair.Point2);
		}

		return findHomography(points1, points2);
	}

	Mat findHomography(const vector<PointMatch> &pointMatches) {
		assert(pointMatches.size() >= 4);

		vector<Point2f> points1, points2;

		for (const PointMatch &match : pointMatches) {
			points1.push_back(match.first);
			points2.push_back(match.second);
		}

		return findHomography(points1, points2);
	}

	Mat computeWeakMWHomography3pts(const vector<PointPair> &pointPairs,
		const Mat &K1, const Mat &K2, const Mat &R) {
		assert(pointPairs.size() >= 3);

		const int numOfPoints = (int)pointPairs.size();
		Mat p1, p2;

		// Construct matrices of homogeneous points
		pointPairsToHomogeneousMatrices(pointPairs, p1, p2);

		// Remove y-axis rotation from R
		Mat angleAxisRotation, R_xz;
		cv::Rodrigues(R, angleAxisRotation);
		angleAxisRotation.at<float>(1) = 0.0;	// remove rotation component around Y axis
		cv::Rodrigues(angleAxisRotation, R_xz);

		// Calculate 3D-2D points (rays)
		Mat P1 = R_xz * K1.inv() * p1;	// pre-rotate the points
		Mat P2 = K2.inv() * p2;

		// Normalize the computed 3D-2D points
		normalizeColumnWise(P1);
		normalizeColumnWise(P2);

		// Construct related matrices to compute a homography
		Mat A = Mat::zeros(numOfPoints * 2, 6, CV_32F);
		Mat b(numOfPoints * 2, 1, CV_32F);
		for (int i = 0; i < numOfPoints; i++)
		{
			float x1 = P1.at<float>(0, i);
			float x2 = P2.at<float>(0, i);
			float y1 = P1.at<float>(1, i);
			float y2 = P2.at<float>(1, i);

			A.at<float>(i * 2, 2) = -x1;
			A.at<float>(i * 2, 3) = -1.0;
			A.at<float>(i * 2, 4) = x1 * y2;
			A.at<float>(i * 2, 5) = y2;

			A.at<float>(i * 2 + 1, 0) = -x1 * y2;
			A.at<float>(i * 2 + 1, 1) = -y2;
			A.at<float>(i * 2 + 1, 2) = x1 * x2;
			A.at<float>(i * 2 + 1, 3) = x2;

			b.at<float>(i * 2, 0) = y1;
			b.at<float>(i * 2 + 1, 0) = -x2 * y1;
		}

		// In the original paper, they used only 2.5 points to compute the homography using
		// the difficient-rank system solver. In here, instead of using 2.5 points, it uses
		// 3 points using least-square for convenience.
		Mat h = (A.t()*A).inv()*A.t()*b;

		return K2 * (Mat_<float>(3, 3) << h.at<float>(0), 0, h.at<float>(1), h.at<float>(2), 1, h.at<float>(3),
			h.at<float>(4), 0, h.at<float>(5)) * R_xz * K1.inv();
	}

	Mat computeMWHomography(const vector<PointPair> &pointPairs,
		const Mat &K1, const Mat &K2, const Mat &R, const Mat &normal) {
		Mat p1, p2;

		pointPairsToHomogeneousMatrices(pointPairs, p1, p2);

		return computeMWHomography(p1, p2, K1, K2, R, normal);
	}

	Mat computeMWHomography(const vector<PointMatch> &pointPairs,
		const Mat &K1, const Mat &K2, const Mat &R, const Mat &normal, Mat &t_hat) {
		Mat p1, p2;

		pointPairsToHomogeneousMatrices(pointPairs, p1, p2);

		return computeMWHomography(p1, p2, K1, K2, R, normal, t_hat);
	}

	Mat computeMWHomography(const vector<Point2f> points1, const vector<Point2f> points2,
		const Mat &K1, const Mat &K2, const Mat &R, const Mat &normal) {

		Mat p1, p2;

		convertPointsToHomogeneous(points1, p1);
		convertPointsToHomogeneous(points2, p2);

		return computeMWHomography(p1, p2, K1, K2, R, normal);
	}

	Mat computeMWHomography(const Mat &p1, const Mat &p2,
		const Mat &K1, const Mat &K2, const Mat &R, const Mat &normal, Mat &t_hat) {
		assert(p1.cols >= 2 && p2.cols >= 2);

		const int numOfPoints = (int)p1.cols;
		Mat invKp1 = K1.inv() * p1;
		Mat invKp2 = K2.inv() * p2;
		Mat alpha = normal.t() * invKp1;
		Mat A, b;

		b = Mat::zeros(numOfPoints * 2, 1, CV_32F);
		A = Mat::zeros(numOfPoints * 2, 3, CV_32F);

		// Construct related matrices to compute a homography
		for (int pi = 0; pi < numOfPoints; pi++)
		{
			b.at<float>(pi * 2, 0) = R.at<float>(0, 0)*invKp1.at<float>(0, pi) + R.at<float>(0, 1)*invKp1.at<float>(1, pi) + R.at<float>(0, 2)
				- R.at<float>(2, 0)*invKp1.at<float>(0, pi)*invKp2.at<float>(0, pi) - R.at<float>(2, 1)*invKp1.at<float>(1, pi)*invKp2.at<float>(0, pi) - R.at<float>(2, 2)*invKp2.at<float>(0, pi);

			b.at<float>(pi * 2 + 1, 0) = R.at<float>(1, 0)*invKp1.at<float>(0, pi) + R.at<float>(1, 1)*invKp1.at<float>(1, pi) + R.at<float>(1, 2)
				- R.at<float>(2, 0)*invKp1.at<float>(0, pi)*invKp2.at<float>(1, pi) - R.at<float>(2, 1)*invKp1.at<float>(1, pi)*invKp2.at<float>(1, pi) - R.at<float>(2, 2)*invKp2.at<float>(1, pi);

			A.at<float>(pi * 2, 0) = -alpha.at<float>(pi);
			A.at<float>(pi * 2 + 1, 1) = -alpha.at<float>(pi);
			A.at<float>(pi * 2, 2) = alpha.at<float>(pi) * invKp2.at<float>(0, pi);
			A.at<float>(pi * 2 + 1, 2) = alpha.at<float>(pi) * invKp2.at<float>(1, pi);
		}

		t_hat = (A.t() * A).inv() * A.t() * b;

		return Mat(K2 * (R + t_hat * normal.t()) * K1.inv());
	}

	vector<Mat> generateMWHomograhpies(const vector<CDataType*> &pointPairs,
		const LineClusters &lineClusters, const Size &imageSize,
		const Mat &K1, const Mat &K2, const Mat &R, const Mat &VD1) {
		const int minimumSampleSet = 2;
		vector<Mat> homograhpies;

		// For each feature point, check its surrounding lines
		for (const CDataType* p1 : pointPairs) {
			Point2f centerPoint((*p1)[0], (*p1)[1]);
			vector<Point> polygon = REGION_DETECTION::findRegion(centerPoint, lineClusters, K1, imageSize);

			Mat regionMask(imageSize, CV_8U);

			// Search all the points in the region
			for (const CDataType* p2 : pointPairs) {

			}
		}

		return homograhpies;
	}

	//------------------------
	// Distance functions
	//------------------------

	// Point reprojection error computed by given homography.
	double ReprojectionError(const Mat &homography, const PointMatch &pointMatch) {
		if (homography.type() == CV_64F) {
			double x = pointMatch.first.x * homography.at<double>(0, 0)
				+ pointMatch.first.y * homography.at<double>(0, 1) + homography.at<double>(0, 2);
			double y = pointMatch.first.x * homography.at<double>(1, 0)
				+ pointMatch.first.y * homography.at<double>(1, 1) + homography.at<double>(1, 2);
			double z = pointMatch.first.x * homography.at<double>(2, 0)
				+ pointMatch.first.y * homography.at<double>(2, 1) + homography.at<double>(2, 2);

			x /= z;
			y /= z;

			return sqrt((pointMatch.second.x - x)*(pointMatch.second.x - x)
				+ (pointMatch.second.y - y)*(pointMatch.second.y - y));
		}
		else if (homography.type() == CV_32F) {
			float x = pointMatch.first.x * homography.at<float>(0, 0)
				+ pointMatch.first.y * homography.at<float>(0, 1) + homography.at<float>(0, 2);
			float y = pointMatch.first.x * homography.at<float>(1, 0)
				+ pointMatch.first.y * homography.at<float>(1, 1) + homography.at<float>(1, 2);
			float z = pointMatch.first.x * homography.at<float>(2, 0)
				+ pointMatch.first.y * homography.at<float>(2, 1) + homography.at<float>(2, 2);

			x /= z;
			y /= z;

			return sqrt((pointMatch.second.x - x)*(pointMatch.second.x - x)
				+ (pointMatch.second.y - y)*(pointMatch.second.y - y));
		}
		else {
			cout << "Invalid homography type: " << homography.type() << endl;
			assert(false);
		}
	}

	//------------------------
	// Utilities
	//------------------------

	void pointPairsToHomogeneousMatrices(const vector<PointPair> &pointPairs, Mat &p1, Mat &p2) {
		assert(pointPairs.size() > 0);

		const int numOfPoints = (int)pointPairs.size();
		
		p1 = Mat::zeros(3, numOfPoints, CV_32F);
		p2 = Mat::zeros(3, numOfPoints, CV_32F);

		// Construct matrices of homogeneous points
		for (int pi = 0; pi < numOfPoints; pi++) {
			p1.at<float>(0, pi) = pointPairs[pi].Point1.x;
			p1.at<float>(1, pi) = pointPairs[pi].Point1.y;
			p1.at<float>(2, pi) = 1.0;

			p2.at<float>(0, pi) = pointPairs[pi].Point2.x;
			p2.at<float>(1, pi) = pointPairs[pi].Point2.y;
			p2.at<float>(2, pi) = 1.0;
		}
	}

	void pointPairsToHomogeneousMatrices(const vector<PointMatch> &pointPairs, Mat &p1, Mat &p2) {
		assert(pointPairs.size() > 0);

		const int numOfPoints = (int)pointPairs.size();

		p1 = Mat::zeros(3, numOfPoints, CV_32F);
		p2 = Mat::zeros(3, numOfPoints, CV_32F);

		// Construct matrices of homogeneous points
		for (int pi = 0; pi < numOfPoints; pi++) {
			p1.at<float>(0, pi) = pointPairs[pi].first.x;
			p1.at<float>(1, pi) = pointPairs[pi].first.y;
			p1.at<float>(2, pi) = 1.0;

			p2.at<float>(0, pi) = pointPairs[pi].second.x;
			p2.at<float>(1, pi) = pointPairs[pi].second.y;
			p2.at<float>(2, pi) = 1.0;
		}
	}

	inline void normalizeColumnWise(Mat &mat) {
		for (int i = 0; i < mat.cols; i++) {
			mat.at<float>(0, i) = mat.at<float>(0, i) / mat.at<float>(2, i);
			mat.at<float>(1, i) = mat.at<float>(1, i) / mat.at<float>(2, i);
			mat.at<float>(2, i) = 1.0;
		}
	}
}