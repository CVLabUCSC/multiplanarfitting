#include "EssentialResidual.h"

#include <ceres\autodiff_cost_function.h>

using std::pair;
using cv::Mat;
using cv::Mat_;
using cv::Point3f;
using cv::Point3d;

Mat EssentialResidual::R = Mat::eye(3, 3, CV_64F);

EssentialResidual::EssentialResidual(const pair<Point3d, Point3d> &pointPair) {
	Point1 = (Mat_<double>(3, 1) << pointPair.first.x, pointPair.first.y, pointPair.first.z);
	Point2_t = (Mat_<double>(1, 3) << pointPair.second.x, pointPair.second.y, pointPair.second.z);
}

template <typename T>
bool EssentialResidual::operator()(const T* const x, T* residuals) const {
	// P2' * T_x
	T val1_1 = Point2_t.at<double>(1) * x[2] + Point2_t.at<double>(2) * (-x[1]);
	T val1_2 = Point2_t.at<double>(0) * (-x[2]) + Point2_t.at<double>(2) * x[0];
	T val1_3 = Point2_t.at<double>(0) * x[1] + Point2_t.at<double>(1) * (-x[0]);

	// P2' * T_x * R
	T val2_1 = val1_1 * (double)R.at<float>(0, 0) + val1_2 * (double)R.at<float>(1, 0) + val1_3 * (double)R.at<float>(2, 0);
	T val2_2 = val1_1 * (double)R.at<float>(0, 1) + val1_2 * (double)R.at<float>(1, 1) + val1_3 * (double)R.at<float>(2, 1);
	T val2_3 = val1_1 * (double)R.at<float>(0, 2) + val1_2 * (double)R.at<float>(1, 2) + val1_3 * (double)R.at<float>(2, 2);

	// P2' * T_x * R * P1
	residuals[0] = val2_1 * Point1.at<double>(0) + val2_2 * Point1.at<double>(1) + val2_3 * Point1.at<double>(2);

	return true;
}

ceres::CostFunction* EssentialResidual::Create(const pair<Point3d, Point3d> &pointPair) {
	return (new ceres::AutoDiffCostFunction<EssentialResidual, 1, 3>(new EssentialResidual(pointPair)));
}