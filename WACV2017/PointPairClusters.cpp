#include "PointPairClusters.h"
#include "Homography.h"
#include "Utilities.h"

using namespace std;
using namespace cv;

PointPairClusters::PointPairClusters() : Clusters(m_pointPairClusters), Outliers(m_outliers) {

}

PointPairClusters::PointPairClusters(const PointPairClusters& pointPairClusters) :
Clusters(m_pointPairClusters), Outliers(m_outliers) {
	*this = pointPairClusters;
}

PointPairClusters::PointPairClusters(const vector<PointPairCluster> &pointPairClusters,
	const vector<CDataType*> &pointPairs) :
	Clusters(m_pointPairClusters), Outliers(m_outliers) {
	updatePointMatches(pointPairClusters);

	updateIndexer();

	for (const CDataType* pointPair : pointPairs) {
		PointMatch pointMatch(Point2f((*pointPair)[0], (*pointPair)[1]), Point2f((*pointPair)[2], (*pointPair)[3]));

		if (m_indexer.find(pointMatch) == m_indexer.end()) {
			m_indexer[pointMatch] = OUTLIER_CLUSTER_INDEX;
			m_outliers.push_back(pointMatch);
		}
	}
}

PointPairClusters::PointPairClusters(const vector<PointPairCluster> &pointPairClusters,
	const vector<PointMatch> &pointMatches, float outlierThreshold) :
	Clusters(m_pointPairClusters), Outliers(m_outliers) {
	
	updatePointMatches(pointPairClusters);

	updateIndexer();

	for (const PointMatch &pointMatch : pointMatches) {
		if (m_indexer.find(pointMatch) == m_indexer.end()) {
			m_indexer[pointMatch] = OUTLIER_CLUSTER_INDEX;
			m_outliers.push_back(pointMatch);
		}
	}

	if (outlierThreshold > 0) {
		takeOutliers(outlierThreshold);
	}
}

size_t PointPairClusters::size() const {
	return m_pointPairClusters.size();
}

int PointPairClusters::findClusterIndex(const PointMatch &pointMatch) const {
	unordered_map<PointMatch, int, CustomHash>::const_iterator citer = m_indexer.find(pointMatch);

	if (citer == m_indexer.end()) {
		return POINT_DOES_NOT_EXIST;
	}

	return citer->second;
}

vector<PointMatch> PointPairClusters::getAllPointMatches() const {
	vector<PointMatch> pointMatches;

	for (unordered_map<PointMatch, int, CustomHash>::const_iterator iter = m_indexer.cbegin();
		iter != m_indexer.cend(); iter++) {

		pointMatches.push_back(iter->first);
	}

	return pointMatches;
}

PointPairCluster PointPairClusters::computeInliers(const Mat &H, const Mat &VL,
	const Mat &normal, const float threshold) const {
	vector<PointMatch> inlierPointPairs;

	// Collect inliers
	for (unordered_map<PointMatch, int, CustomHash>::const_iterator iter = m_indexer.cbegin();
		iter != m_indexer.cend(); iter++) {

		if (PlaneFitting::ReprojectionError(H, iter->first) < threshold) {
			inlierPointPairs.push_back(iter->first);
		}
	}

	// If VL is set, pick the larger group of points
	if (!VL.empty()) {
		inlierPointPairs = PlaneFitting::pickLargerGroupVL(inlierPointPairs, VL);
	}

	return PointPairCluster(normal, inlierPointPairs);
}

vector<PointPairCluster> PointPairClusters::computeInliers(const vector<Mat> &H, const vector<Mat> &VLs,
	const vector<Mat> &normals, const float threshold) const {
	vector<PointPairCluster> pointPairClusters;
	
	for (int i = 0; i < H.size(); i++) {
		Mat normal, VL;

		if ((int)normals.size() <= i) {
			normal = Mat();
		}
		else {
			normal = normals[i];
		}

		if ((int)VLs.size() <= i) {
			VL = Mat();
		}
		else {
			VL = VLs[i];
		}

		pointPairClusters.push_back(computeInliers(H[i], VL, normal, threshold));
	}

	return pointPairClusters;
}

PointPairClusters& PointPairClusters::operator=(const PointPairClusters &pointPairCluster) {
	m_pointPairClusters = pointPairCluster.m_pointPairClusters;
	m_outliers = pointPairCluster.m_outliers;
	m_indexer = pointPairCluster.m_indexer;

	return *this;
}

// Direct access to each line
PointPairCluster &PointPairClusters::operator[](const int index) {
	return m_pointPairClusters[index];
}

// Direct access to each line for read-only
const PointPairCluster &PointPairClusters::operator[](const int index) const {
	return m_pointPairClusters[index];
}

inline void PointPairClusters::updatePointMatches(const vector<PointPairCluster> &pointPairClusters) {
	unordered_map<PointMatch, pair<int, float>, CustomHash> hashPointPair;

	m_pointPairClusters.clear();
	m_pointPairClusters.resize(pointPairClusters.size());

	for (int i = 0; i < pointPairClusters.size(); i++) {
		Mat H;

		if (pointPairClusters[i].size() >= 4) {
			H = findHomography(pointPairClusters[i].getArrayOfFirstPoints(),
				pointPairClusters[i].getArrayOfSecondPoints());
		}

		if (H.empty()) {
			H = Mat::eye(3, 3, CV_32F);
		}

		for (const PointMatch &pointMatch : pointPairClusters[i].PointPairs) {
			float error = PlaneFitting::ReprojectionError(H, pointMatch);
			if (hashPointPair.find(pointMatch) == hashPointPair.end() ||
				error < hashPointPair[pointMatch].second) {
				hashPointPair[pointMatch] = pair<int, float>(i, error);
			}
		}

		m_pointPairClusters[i].setPlaneNormal(pointPairClusters[i].PlaneNormal);
	}

	for (unordered_map<PointMatch, pair<int, float>, CustomHash>::const_iterator citer = hashPointPair.cbegin();
		citer != hashPointPair.cend(); citer++) {
		m_pointPairClusters[citer->second.first].addPointPair(citer->first);
	}
}

inline void PointPairClusters::updateIndexer() {
	m_indexer.clear();

	for (int i = 0; i < m_pointPairClusters.size(); i++) {
		for (const PointMatch &pointMatch : m_pointPairClusters[i].PointPairs) {
			m_indexer[pointMatch] = i;
		}
	}

	// Do not allow duplicated membership
	for (std::vector<PointMatch>::iterator iter = m_outliers.begin();
		iter != m_outliers.end();) {
		if (m_indexer.find(*iter) == m_indexer.end()) {
			m_indexer[*iter] = OUTLIER_CLUSTER_INDEX;
			iter++;
		}
		else {
			iter = m_outliers.erase(iter);
		}
	}
}

void PointPairClusters::takeOutliers(float threshold) {
	unordered_map<PointMatch, pair<int, float>, CustomHash> hashPointPair;

	for (int i = 0; i < m_pointPairClusters.size(); i++) {
		Mat H;

		if (m_pointPairClusters[i].size() < 4) {
			continue;
		}
		else {
			H = findHomography(m_pointPairClusters[i].getArrayOfFirstPoints(),
				m_pointPairClusters[i].getArrayOfSecondPoints());
		}

		if (H.empty()) {
			H = Mat::eye(3, 3, CV_32F);
		}

		for (const PointMatch &pointMatch : m_outliers) {
			float error = PlaneFitting::ReprojectionError(H, pointMatch);
			if (error < threshold && (hashPointPair.find(pointMatch) == hashPointPair.end() ||
				error < hashPointPair[pointMatch].second)) {
				hashPointPair[pointMatch] = pair<int, float>(i, error);
			}
		}
	}

	for (vector<PointMatch>::iterator iter = m_outliers.begin(); iter != m_outliers.end(); ) {
		if (hashPointPair.find(*iter) != hashPointPair.end()) {
			// Move the point pair
			m_pointPairClusters[hashPointPair[*iter].first].addPointPair(*iter);
			m_indexer[*iter] = hashPointPair[*iter].first;

			// Erase the point pair from outliers list
			iter = m_outliers.erase(iter);
		}
		else {
			iter++;
		}
	}
}

double PointPairClusters::computeAdjustedRandIndex(const PointPairClusters &clusters1, const PointPairClusters &clusters2) {
	int num_of_clusters1 = (int)clusters1.size();
	int num_of_clusters2 = (int)clusters2.size();
	int **table = new int*[num_of_clusters1];
	int num_of_points;

	if (num_of_clusters1 == 0 || num_of_clusters2 == 0) {
		return 0.0;
	}

	// The last index of contingency table is for an outlier cluster
	for (int i = 0; i < num_of_clusters1; i++)
	{
		table[i] = new int[num_of_clusters2 + 1];
		memset(table[i], 0, sizeof(int)*(num_of_clusters2 + 1));
	}

	// Fill the values of contingency table for inlier points
	for (int ci = 0; ci < num_of_clusters1; ci++)
	{
		num_of_points = (int)clusters1[ci].size();

		for (int pi = 0; pi < num_of_points; pi++)
		{
			int cj = clusters2.findClusterIndex(clusters1[ci][pi]);

			assert(cj != POINT_DOES_NOT_EXIST);

			if (cj == OUTLIER_CLUSTER_INDEX) {
				table[ci][num_of_clusters2]++;
			}
			else {
				table[ci][cj]++;
			}
		}
	}

	// Compute the adjusted rand index
	// Calculate a, b, and n
	int a = 0, b = 0, c = 0, n = 0;
	for (int cj = 0; cj <= num_of_clusters2; cj++)
	{
		int sum = 0;
		if (cj < num_of_clusters2) {
			for (int ci = 0; ci < num_of_clusters1; ci++) {
				sum += table[ci][cj];

				// n combination 2 is always equal to (n * (n-1)) / 2
				if (table[ci][cj] >= 2)
					a += table[ci][cj] * (table[ci][cj] - 1) / 2;
			}

			// b is the number of pairs of objects in the same class in
			// cluster_est but not in the same cluster in cluster_gt
			b += sum * (sum - 1) / 2;
		}
		else {
			for (int ci = 0; ci < num_of_clusters1; ci++) {
				sum += table[ci][cj];
			}
		}

		n += sum;
	}
	b -= a;

	// Calculate c:
	// c is the number of pairs of objects in the same class in
	// cluster_est but not in the same cluster in cluster_gt
	for (int ci = 0; ci < num_of_clusters1; ci++) {
		int sum = 0;

		for (int cj = 0; cj < num_of_clusters2; cj++)
		{
			sum += table[ci][cj];
		}

		c += sum * (sum - 1) / 2;
	}
	c -= a;

	// Calculate d
	int n_comb_2 = (n * (n - 1) / 2);
	int d = n_comb_2 - a - b - c;

	double rand_index = double(a - (b + a)*(c + a) / n_comb_2) / double(0.5*(b + a + c + a) - (b + a)*(c + a) / n_comb_2);

	// Deallocate memory
	for (int i = 0; i < num_of_clusters1; i++)
		delete[] table[i];
	delete[] table;

	return rand_index;
}

double PointPairClusters::computeAdjustedRandIndex2(const PointPairClusters &clusters1, const PointPairClusters &clusters2) {
	int num_of_clusters1 = (int)clusters1.size();
	int num_of_clusters2 = (int)clusters2.size();
	int **table = new int*[num_of_clusters1];
	int num_of_points;

	if (num_of_clusters1 == 0 || num_of_clusters2 == 0) {
		return 0.0;
	}

	// The last index of contingency table is for an outlier cluster
	for (int i = 0; i < num_of_clusters1; i++)
	{
		table[i] = new int[num_of_clusters2 + 1];
		memset(table[i], 0, sizeof(int)*(num_of_clusters2 + 1));
	}

	// Fill the values of contingency table for inlier points
	for (int ci = 0; ci < num_of_clusters1; ci++)
	{
		num_of_points = (int)clusters1[ci].size();

		for (int pi = 0; pi < num_of_points; pi++)
		{
			int cj = clusters2.findClusterIndex(clusters1[ci][pi]);

			assert(cj != POINT_DOES_NOT_EXIST);

			if (cj == OUTLIER_CLUSTER_INDEX) {
				table[ci][num_of_clusters2]++;
			}
			else {
				table[ci][cj]++;
			}
		}
	}

	// Compute the adjusted rand index
	// Calculate a, b, and n
	int a = 0, b = 0, c = 0, n = 0, e = 0;
	for (int cj = 0; cj <= num_of_clusters2; cj++)
	{
		int sum = 0, ssum = 0;
		if (cj < num_of_clusters2) {
			for (int ci = 0; ci < num_of_clusters1; ci++) {
				sum += table[ci][cj];

				// a is the number of pairs of objects in the same class in both clusters
				// n combination 2 is always equal to (n * (n-1)) / 2
				if (table[ci][cj] >= 2) {
					a += table[ci][cj] * (table[ci][cj] - 1) / 2;
					if (abs(clusters1[ci].PlaneNormal.dot(clusters2[cj].PlaneNormal)) < 0.9) {
						e += table[ci][cj] * (table[ci][cj] - 1) / 2;
					}
				}
			}

			// b is the number of pairs of objects in the same class in
			// cluster_est but not in the same cluster in cluster_gt
			b += sum * (sum - 1) / 2;
		}
		else {
			for (int ci = 0; ci < num_of_clusters1; ci++) {
				sum += table[ci][cj];
			}
		}

		n += sum;
	}

	// Calculate c:
	// c is the number of pairs of objects in the same class in
	// cluster_est but not in the same cluster in cluster_gt
	for (int ci = 0; ci < num_of_clusters1; ci++) {
		int sum = 0;

		for (int cj = 0; cj < num_of_clusters2; cj++) {
			sum += table[ci][cj];
		}

		c += sum * (sum - 1) / 2;
	}

	a -= e;
	int fp = b - a;
	int fn = c - a;
	int tn = (n * (n - 1) / 2);

	double rand_index = double(a + tn) / double(a + fp + fn + tn);

	// Deallocate memory
	for (int i = 0; i < num_of_clusters1; i++)
		delete[] table[i];
	delete[] table;

	return rand_index;
}
