#include "VPDetector.h"

#include "VDResidual.h"

#include <vector>
#include <time.h>
#include <opencv2\opencv.hpp>

#include <ceres\ceres.h>

#include "Utilities.h"

namespace VPDetector {
	using namespace std;
	using namespace cv;

	Mat detectVP(const LineClusters &lineClusters, vector<int> &selectedIndices, Mat &K, float threshold) {
		Mat VD;

		selectedIndices = findOrthogonalClusters(lineClusters, K, threshold, VD);

		VD = refineCalibration(lineClusters.subCluster(selectedIndices, threshold, true), VD, K, threshold);

		cout << "K: \n" << K << endl;
		cout << "VD: \n" << VD << endl;

		return VD;
	}

	vector<Mat> detectVP(const vector<LineClusters> &lineClusters,
		vector<vector<int>> &selectedIndices, Mat &K, float inlierThreshold) {
		const int numOfImages = (int)lineClusters.size();
		vector<Mat> VDs(numOfImages);
		vector<LineClusters> subClusters;

		selectedIndices.resize(numOfImages);

		for (int i = 0; i < numOfImages; i++) {
			selectedIndices[i] = findOrthogonalClusters(lineClusters[i], K, inlierThreshold, VDs[i]);

			subClusters.push_back(lineClusters[i].subCluster(selectedIndices[i], inlierThreshold, true));

			// Orthogonalize the found vanishing directions, then collect inlier lines
			Orthogonalize(subClusters[i], VDs[i]);
		}
		
		VDs = refineCalibration2(subClusters, VDs, K, inlierThreshold);

		return VDs;
	}

	Mat refineCalibration(const LineClusters &lineClusters, Mat &K, const float inlierThreshold) {
		Mat VD(3, 3, CV_32F);
		Mat Kinv = K.inv();

		for (unsigned int ci = 0; ci < lineClusters.size(); ci++) {
			Point2f VP = lineClusters[ci].getVanishingPoint();

			VD.at<float>(0, ci) = Kinv.at<float>(0, 0) * VP.x + Kinv.at<float>(0, 1) * VP.y + Kinv.at<float>(0, 2) * 1.f;
			VD.at<float>(1, ci) = Kinv.at<float>(1, 0) * VP.x + Kinv.at<float>(1, 1) * VP.y + Kinv.at<float>(1, 2) * 1.f;
			VD.at<float>(2, ci) = Kinv.at<float>(2, 0) * VP.x + Kinv.at<float>(2, 1) * VP.y + Kinv.at<float>(2, 2) * 1.f;

			float normalizer = (float)sqrt(VD.col(ci).dot(VD.col(ci)));

			VD.at<float>(0, ci) /= normalizer;
			VD.at<float>(1, ci) /= normalizer;
			VD.at<float>(2, ci) /= normalizer;
		}

		VD = refineCalibration(lineClusters, VD, K, inlierThreshold);

		cout << "K: \n" << K << endl;
		cout << "VD: \n" << VD << endl;

		return VD;
	}

	Mat refineCalibration(const LineClusters &lineClusters, const cv::Mat &VD,
		Mat &K, const float threshold) {
		Mat rotation;
		Mat rotationComps;
		ceres::Problem problem;

		Rodrigues(VD, rotationComps);

		double x[3] = { rotationComps.at<float>(0), rotationComps.at<float>(1), rotationComps.at<float>(2) };
		double focalLength = K.at<float>(0, 0);

		for (int ci = 0; ci < 3; ci++) {
			for (const Line& l : lineClusters[ci].Lines) {
				const Mat &lineVector = l.LineVector;

				problem.AddResidualBlock(
					VDResidual::Create(lineVector.at<float>(0), lineVector.at<float>(1), lineVector.at<float>(2),
					K.at<float>(0, 2), K.at<float>(1, 2), ci),
					new ceres::CauchyLoss(0.5), x, &focalLength);
			}
		}

		ceres::Solver::Options options;
		ceres::Solver::Summary summary;

		options.linear_solver_type = ceres::DENSE_SCHUR; // Either DENSE_SCHUR or DENSE_QR
		options.minimizer_progress_to_stdout = false;
		ceres::Solve(options, &problem, &summary);

		//cout << summary.FullReport() << endl;

		rotationComps.at<float>(0) = x[0];
		rotationComps.at<float>(1) = x[1];
		rotationComps.at<float>(2) = x[2];

		Rodrigues(rotationComps, rotation);
		K.at<float>(0, 0) = focalLength;
		K.at<float>(1, 1) = focalLength;

		if (K.at<float>(0, 0) < 0.f) {
			VD.row(0) *= -1.f;
			VD.row(1) *= -1.f;
			K.at<float>(0, 0) *= -1.f;
			K.at<float>(1, 1) *= -1.f;
		}

		return rotation;
	}

	vector<Mat> refineCalibration(const vector<LineClusters> &lineClusters,
		const vector<Mat> &VDs, Mat &K, const float inlierThreshold) {

		const int numOfClusters = (int)lineClusters.size();
		ceres::Problem problem;
		double *angles = new double[3 * numOfClusters];	// camera orientations
		double focalLength = K.at<float>(0, 0);			// focal length to be optimized
		vector<Mat> rotations(numOfClusters);

		for (unsigned int ci = 0; ci < VDs.size(); ci++) {
			Mat rotationComps;

			Rodrigues(VDs[ci], rotationComps);

			angles[3 * ci] = rotationComps.at<float>(0);
			angles[3 * ci + 1] = rotationComps.at<float>(1);
			angles[3 * ci + 2] = rotationComps.at<float>(2);

			for (int cj = 0; cj < 3; cj++) {
				for (const Line& l : lineClusters[ci][cj].Lines) {
					const Mat &lineVector = l.LineVector;

					problem.AddResidualBlock(
						VDResidual::Create(lineVector.at<float>(0), lineVector.at<float>(1), lineVector.at<float>(2),
						K.at<float>(0, 2), K.at<float>(1, 2), cj),
						new ceres::CauchyLoss(0.5), &angles[3 * ci], &focalLength);
				}
			}
		}

		ceres::Solver::Options options;
		options.linear_solver_type = ceres::DENSE_QR; // Either DENSE_SCHUR or DENSE_QR
		options.minimizer_progress_to_stdout = false;
		ceres::Solver::Summary summary;
		ceres::Solve(options, &problem, &summary);

		for (unsigned int ci = 0; ci < VDs.size(); ci++) {
			Mat rotationComps = (Mat_<float>(3, 1) << angles[3 * ci], angles[3 * ci + 1], angles[3 * ci + 2]);

			Rodrigues(rotationComps, rotations[ci]);
		}

		K.at<float>(0, 0) = focalLength;
		K.at<float>(1, 1) = focalLength;

		delete[] angles;

		return rotations;
	}

	vector<Mat> refineCalibration2(const vector<LineClusters> &lineClusters,
		const vector<Mat> &VDs, const Mat &K, const float inlierThreshold) {

		const int numOfClusters = (int)lineClusters.size();
		ceres::Problem problem;
		double *angles = new double[3 * numOfClusters];	// camera orientations
		double focalLength = K.at<float>(0, 0);			// focal length to be optimized
		vector<Mat> rotations(numOfClusters);

		for (unsigned int ci = 0; ci < VDs.size(); ci++) {
			Mat rotationComps;

			Rodrigues(VDs[ci], rotationComps);

			angles[3 * ci] = rotationComps.at<float>(0);
			angles[3 * ci + 1] = rotationComps.at<float>(1);
			angles[3 * ci + 2] = rotationComps.at<float>(2);

			for (int cj = 0; cj < 3; cj++) {
				for (const Line& l : lineClusters[ci][cj].Lines) {
					const Mat &lineVector = l.LineVector;

					problem.AddResidualBlock(
						VDResidual2::Create(K.at<float>(0, 0), lineVector.at<float>(0), lineVector.at<float>(1), lineVector.at<float>(2),
						K.at<float>(0, 2), K.at<float>(1, 2), cj),
						new ceres::CauchyLoss(0.5), &angles[3 * ci]);
				}
			}
		}

		ceres::Solver::Options options;
		options.linear_solver_type = ceres::DENSE_QR; // Either DENSE_SCHUR or DENSE_QR
		options.minimizer_progress_to_stdout = false;
		ceres::Solver::Summary summary;
		ceres::Solve(options, &problem, &summary);

		for (unsigned int ci = 0; ci < VDs.size(); ci++) {
			Mat rotationComps = (Mat_<float>(3, 1) << angles[3 * ci], angles[3 * ci + 1], angles[3 * ci + 2]);

			Rodrigues(rotationComps, rotations[ci]);
		}

		delete[] angles;

		return rotations;
	}

	vector<int> findOrthogonalClusters(const LineClusters &lineClusters, const Mat &K,
		const float threshold, Mat &VD)
	{
		float perpCosThr = cos(75.0 * CV_PI / 180.0);
		float paraCosThr = cos(30.0 * CV_PI / 180.0);
		RNG rng(time(NULL));
		const int numOfClusters = (int)lineClusters.size();
		vector<Vec3f> VanishingDirections;
		vector<float> gravityConsistency;
		vector<int> gravityConsistencyIdx;
		Vec3f xDir(1.f, 0.f, 0.f);
		Vec3f yDir(0.f, 1.f, 0.f);
		Vec3f zDir(0.f, 0.f, 1.f);
		int maxNumOfInliers = 0;
		Mat Kinv = K.inv();
		vector<int> selectedIndices(3, -1);

		VD = Mat::zeros(3, 3, CV_32F);

		// Compute initial vanishing directions
		for (int ci = 0; ci < numOfClusters; ci++) {
			Point2f VP = lineClusters[ci].getVanishingPoint();
			Vec3f VD;

			VD[0] = Kinv.at<float>(0, 0) * VP.x + Kinv.at<float>(0, 1) * VP.y + Kinv.at<float>(0, 2) * 1.f;
			VD[1] = Kinv.at<float>(1, 0) * VP.x + Kinv.at<float>(1, 1) * VP.y + Kinv.at<float>(1, 2) * 1.f;
			VD[2] = Kinv.at<float>(2, 0) * VP.x + Kinv.at<float>(2, 1) * VP.y + Kinv.at<float>(2, 2) * 1.f;

			VD = normalize(VD);

			VanishingDirections.push_back(VD);
			gravityConsistency.push_back(abs(VD.dot(yDir)));
		}

		sortIdx(gravityConsistency, gravityConsistencyIdx, SORT_DESCENDING);

		for (int vdi = 0; vdi < numOfClusters; vdi++) {
			int y_idx = gravityConsistencyIdx[vdi];
			int numOfLines1 = lineClusters[y_idx].size();

			if (gravityConsistency[y_idx] > paraCosThr) {
				Vec3f &VD_y = VanishingDirections[y_idx];

				for (int vdj = 0; vdj < numOfClusters; vdj++) {
					int numOfLines2 = lineClusters[vdj].size();

					// Two selected vanishing directions must be as perpendicular as possible.
					if (vdj != y_idx && abs(VD_y.dot(VanishingDirections[vdj])) < perpCosThr) {
						for (int vdk = 0; vdk < numOfClusters; vdk++) {
							if (vdk != y_idx && vdk != vdj) {
								// Three selected vanishing directions must be as perpendicular as possible.
								if (abs(VD_y.dot(VanishingDirections[vdk])) < perpCosThr &&
									abs(VanishingDirections[vdj].dot(VanishingDirections[vdk])) < perpCosThr) {

									// Compute the number of inliers that are aligned with current setting
									int numOfInliers = numOfLines1 + numOfLines2 + lineClusters[vdk].size();

									if (numOfInliers > maxNumOfInliers) {
										maxNumOfInliers = numOfInliers;
										selectedIndices[0] = y_idx;
										selectedIndices[1] = vdj;
										selectedIndices[2] = vdk;
										Mat(VD_y).copyTo(VD.col(0));
										Mat(VanishingDirections[vdj]).copyTo(VD.col(1));
										Mat(VanishingDirections[vdk]).copyTo(VD.col(2));
									}
								}
								else {
									// Compute vanishing direction, which is perpendicular to the selected VDs.
									Vec3f vanishingDirection = VD_y.cross(VanishingDirections[vdj]);
									float normalizer = K.at<float>(2, 0) * vanishingDirection[0] +
										K.at<float>(2, 1) * vanishingDirection[1] +
										K.at<float>(2, 2) * vanishingDirection[2];
									Point2f vanishingPoint((K.at<float>(0, 0) * vanishingDirection[0] +
										K.at<float>(0, 1) * vanishingDirection[1] +
										K.at<float>(0, 2) * vanishingDirection[2]) / normalizer,
										(K.at<float>(1, 0) * vanishingDirection[0] +
										K.at<float>(1, 1) * vanishingDirection[1] +
										K.at<float>(1, 2) * vanishingDirection[2]) / normalizer);

									// Compute cardinality of computed vanishing point
									int numOfInliers = numOfLines1 + numOfLines2 +
										lineClusters.computeCardinality(vanishingPoint, threshold);

									if (numOfInliers > maxNumOfInliers) {
										maxNumOfInliers = numOfInliers;
										selectedIndices[0] = y_idx;
										selectedIndices[1] = vdj;
										selectedIndices[2] = -1;	// virtual cluster, might need to be computed
										Mat(VD_y).copyTo(VD.col(0));
										Mat(VanishingDirections[vdj]).copyTo(VD.col(1));
										Mat(vanishingDirection).copyTo(VD.col(2));
									}
								}
							}	// end_if (vdk != y_idx && vdk != vdj)
						}	// end_for (int vdk = 0; vdk < numOfClusters; vdk++)
					}	// end_if (vdj != y_idx && VD_y.dot(VanishingDirections[vdj]) < diffThr)
				}	// end_for (int vdj = 0; vdj < numOfClusters; vdj++)
			}	// end_if (gravityConsistency[y_idx] > equalThr)
			else {
				break;
			}
		}

		if (maxNumOfInliers == 0) {
			cout << "Failed to find orthogonal clusters. Find orthogonal clusters based on their cardinality" << endl;

			vector<int> numInliers;
			vector<int> sortedIndices;

			for (int i = 0; i < numOfClusters; i++) {
				numInliers.push_back((int)lineClusters.size());
			}

			sortIdx(numInliers, sortedIndices, SORT_DESCENDING);
			selectedIndices[0] = sortedIndices[0];
			selectedIndices[1] = sortedIndices[1];
			Vec3f VDTest = VanishingDirections[selectedIndices[0]].cross(VanishingDirections[selectedIndices[1]]);

			// Find the last cluster
			int bestIndex = -1;
			float maxCosine = 0., cosineValue;
			for (int i = 0; i < numOfClusters; i++) {
				if (i == selectedIndices[0] || i == selectedIndices[1]) {
					continue;
				}

				cosineValue = abs(VDTest.dot(VanishingDirections[i]));
				if (cosineValue > maxCosine) {
					maxCosine = cosineValue;
					bestIndex = i;
				}
			}

			selectedIndices[2] = bestIndex;

			Mat(VanishingDirections[selectedIndices[0]]).copyTo(VD.col(0));
			Mat(VanishingDirections[selectedIndices[1]]).copyTo(VD.col(1));
			Mat(VanishingDirections[selectedIndices[2]]).copyTo(VD.col(2));
		}
		else if (selectedIndices[2] == -1) {
			cout << "Selected from computed third VD" << endl;
			size_t maxInliers = 0;
			int bestIndex = -1;
			for (int i = 0; i < lineClusters.size(); i++) {
				if (i == selectedIndices[0] || i == selectedIndices[1]) {
					continue;
				}

				if (lineClusters.size() > maxInliers) {
					maxInliers = lineClusters.size();
					bestIndex = i;
				}
			}
			selectedIndices[2] = bestIndex;
		}

		return selectedIndices;
	}

	//-----------------------------
	// Utility Functions
	//-----------------------------

	Mat drawAxis(const cv::Mat &image, cv::Mat& VD, const cv::Mat& K)
	{
		Mat scalingVec = (Mat_<float>(3, 1) << 0.f, 0.f, 10.f);
		Point2f centerPoint(K.at<float>(0, 2), K.at<float>(1, 2));
		Mat output = image.clone();
		Scalar colors[3] = { CV_RGB(255, 0, 0), CV_RGB(0, 255, 0), CV_RGB(0, 0, 255) };

		for (int vi = 0; vi < 3; vi++) {
			Mat endPointVec = K * (VD.col(vi) + scalingVec);
			Point2f endPoint(endPointVec.at<float>(0) / endPointVec.at<float>(2),
				endPointVec.at<float>(1) / endPointVec.at<float>(2));
			line(output, centerPoint, endPoint, colors[vi], 3);
		}

		circle(output, centerPoint, 3, CV_RGB(125, 0, 125));

		return output;
	}

	void Orthogonalize(const LineClusters &lineClusters, Mat &VD) {
		vector<int> numOfInliers;
		vector<int> sortedIndices;
		Mat newVD(3, 3, VD.type());

		for (const LineCluster& lineCluster : lineClusters.Clusters) {
			numOfInliers.push_back(lineCluster.size());
		}

		sortIdx(numOfInliers, sortedIndices, SORT_DESCENDING);

		VD.col(sortedIndices[0]).copyTo(newVD.col(0));
		VD.col(sortedIndices[1]).copyTo(newVD.col(1));
		VD.col(sortedIndices[2]).copyTo(newVD.col(2));

		PlaneFitting::QRDecomp<float>(newVD.clone(), newVD);

		// Restore original order
		newVD.col(0).copyTo(VD.col(sortedIndices[0]));
		newVD.col(1).copyTo(VD.col(sortedIndices[1]));
		newVD.col(2).copyTo(VD.col(sortedIndices[2]));
	}
}