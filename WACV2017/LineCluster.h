#pragma once

#include "Line.h"

class LineCluster {
public:
	// Read-only array of lines
	const std::vector<Line> &Lines;

	// Constructors
	LineCluster(const LineCluster& lineCluster);
	LineCluster(const std::vector<Line> &lines);

	// Add a line to the cluster
	void add(const Line& line);

	// Remove dummy lines
	void removeDummyLines();

	// Return true if the cluster contains vertical lines
	bool isVertical() const;

	// Get vanishing point of the cluster
	cv::Point2f getVanishingPoint() const;

	// Find the closest line from given 'centerPoint' to the direction of 'dirPoint'.
	// Once it found the line, where the distance is less than minDistance, it will return true.
	bool findClosestLineFrom(const cv::Point2f &centerPoint, const cv::Point2f &dirPoint,
		float &minDistance, int &lineIndex, const cv::Rect exclude = cv::Rect()) const;

	// Find the closest line from given 'centerPoint' to the direction of 'line1' or 'line2',
	// where line1 and line2 are pointing the same point, 'dirPoint'. Once it found the line,
	// where the distance is less than minDistance, it will return true.
	bool findClosestLineBetween(const cv::Point2f &centerPoint, const cv::Point2f &dirPoint,
		const Line &line1, const Line &line2, float &minDistance, int &lineIndex, cv::Rect exclude = cv::Rect()) const;

	int findClosestLineOpposite(const cv::Point2f &centerPoint, const cv::Point2f supVP, int baseLineIndex,
		const Line &consLine1, const Line &consLine2, cv::Rect exclude = cv::Rect()) const;

	// Find the closest line between given lines.
	bool findClosestLineBetween(const Line &baseLine, const Line &line1, const Line &line2,
		int &lineIndex, float &distance, const float minimumLength = 0.f);

	// Find the closest line between given lines.
	bool findClosestLineBetween(const Line &line1, const Line &line2, int &lineIndex, float &distance);

	// Find the closest line of given line.
	bool findClosestLine(const Line &line, int &lineIndex, float &distance) const;

	// Find the closest line of given point. If it success to find the line where the distance from
	// point to the line is closer than given distance, returns true.
	bool findClosestLine(const cv::Point2f &point, int &lineIndex, float &distance,
		const float minimumLength = 0.f) const;

	// Find closest lines of given point. Each line will be on the each side of given point.
	// If it success to find the line, returns true.
	bool findClosestLines(const cv::Point2f &point, int &line1Index, int &line2Index,
		float &distance1, float &distance2, const float minimumLength = 0.f) const;

	// Compute cosine of each line with respect to the line connecting fixedPoint and basePoint.
	void computeCosineOfLines(const cv::Point2f &fixedPoint, const cv::Point2f &basePoint,
		std::vector<float> &cosines) const;

	// Get the number of lines in this cluster
	size_t size() const;

	// Overload operator
	LineCluster& operator=(const LineCluster &lineCluster);

	// Direct access to each line
	Line &operator[](const int index);

	// Direct access to each line for read-only
	const Line &operator[](const int index) const;

	//-------------------------------------
	// Static functions
	//-------------------------------------

	// Compute vanishing point with given lines
	static cv::Point2f computeVanishingPoint(const std::vector<Line> &lines);

private:
	// Indicates if the cluster contains vertical lines
	bool m_isVertical;

	// Vanishing point
	cv::Point2f m_vanishingPoint;

	// Actual array of lines
	std::vector<Line> m_lines;

	// Compute intersection point of lines
	void computeVanishingPoint();

	// Sort order of line endpoints, so that StartPoint is closer to the vanishing point than EndPoint.
	void sortLinePointOrder();
};

//-------------------------------------
// Utility functions
//-------------------------------------

// Compute cosine similarity between lines connecting fixedPoint and point1, and fixedPoint and point2.
inline float computeSignedCosine(const cv::Point2f &fixedPoint,
	const cv::Point2f &point1, const cv::Point2f &point2);
