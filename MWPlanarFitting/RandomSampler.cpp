#include "RandomSampler.h"

// Constructor - Set functions, MSS and points. Non uniform sampling type must be set later
RandomSampler::RandomSampler(unsigned int nPtDimension,
	unsigned int nMSS, 
	unsigned int nMaxAllocablePoints,
	bool nCopyPtsCoords
	):
	mKDTree(nPtDimension),
	mDataPoints(),
	mNeighCustomVecF(),
	mNeighCustomVecFCumSum()
	{

	mMSS = nMSS; // Minimal sample set

	if(nMaxAllocablePoints == 0){
		mGrowablePoints = true;
		mMaxAllocablePoints = 0;
	}
	else{
		mGrowablePoints = false;
		mMaxAllocablePoints = nMaxAllocablePoints;
		mFreeCells.resize(mMaxAllocablePoints);
		mDataPoints.resize(mMaxAllocablePoints);
		for(unsigned int i=0; i < mMaxAllocablePoints; i++){
			mFreeCells[i] = i;
			mDataPoints[i] = new sPt();
			mDataPoints[i]->mActive = false;
			mDataPoints[i]->mIndex = i;
		}
		
		mNeighCustomVecF.resize(mMaxAllocablePoints, 0.0f);
		mNeighCustomVecFCumSum.resize(mMaxAllocablePoints, 0.0f);
		SetCumSumHist (&mNeighCustomVecFCumSum, mNeighCustomVecF);

	}

	// Non-Uniform sampling parameters -- Set dummy values
	mSigmaExp = 1.0f;
	mDNeigh = 10;
	mNNPClose = 1.0f;
	mNNPFar = 2.0f;
	mNFSType = NFST_UNIFORM;
	mActivePoints = 0;
	mPtDimension = nPtDimension;
	mCopyPtsCoords = nCopyPtsCoords;

	#ifndef _DEBUG
		srand((unsigned int)time(NULL));
	#endif
}

// Destructor
RandomSampler::~RandomSampler(){	
	ClearDataPoints();
}

// Clear mDataPoints Vector
void RandomSampler::ClearDataPoints(){
	for(unsigned int i=0; i<mDataPoints.size(); i++){
		if(mDataPoints[i] != NULL){
			delete mDataPoints[i];
		}
	}
}

// Set the data Points
int RandomSampler::SetPoints(const std::vector<cv::Point2f> &points1, const std::vector<cv::Point2f> &points2)
{	
	// Dummy input check
	assert(!points1.empty() && !points2.empty() && points1.size() >= mMSS && points2.size() >= mMSS);
	if(points1.empty() || points2.empty() || points1.size() <= mMSS || points2.size() <= mMSS)
		return -1;

	// Allocation infos
	if(mGrowablePoints)
		mMaxAllocablePoints = (unsigned int)points1.size();

	mFreeCells.clear();
	mFreeCells.reserve(mMaxAllocablePoints);

	// Clear already existing data points
	ClearDataPoints();

	unsigned int numOfPoints = (unsigned int)points1.size();
	unsigned int numOfPointsAllocable = std::min<int>(mMaxAllocablePoints, numOfPoints);

	mDataPoints.resize(numOfPointsAllocable);

	// Allocate sampling probability
	mNeighCustomVecF.clear();
	mNeighCustomVecFCumSum.clear();
	mNeighCustomVecF.resize(numOfPointsAllocable, 1.0f);
	mNeighCustomVecFCumSum.resize(numOfPointsAllocable, 0.0f);
	SetCumSumHist (&mNeighCustomVecFCumSum, mNeighCustomVecF);

	// Allocate data
	for(unsigned int i=0; i < numOfPointsAllocable; i++)
	{
		mDataPoints[i] = new sPt();
		mDataPoints[i]->mIndex = i;

		// Copy coord
		mDataPoints[i]->mCoord1 = points1[i];
		mDataPoints[i]->mCoord2 = points2[i];

		if(i<numOfPoints){
			mDataPoints[i]->mActive = true;
		}
		else{
			mFreeCells.push_back(i);
			mDataPoints[i]->mActive = false;
		}

		// NFS infos
		mDataPoints[i]->mSecondSamplingProbability.clear();
		mDataPoints[i]->mSecondSamplingProbabilityCumSum.clear();

	}

	// Restore free cell indexes
	for(unsigned int i=numOfPointsAllocable; i < mMaxAllocablePoints; i++)
		mFreeCells[i-numOfPointsAllocable] = i;

	InitializeNFSampling();

	// Returns the number of points allocated
	mActivePoints = numOfPointsAllocable;

	return numOfPointsAllocable;
}

// Get point
const sPt *RandomSampler::GetPoint(unsigned int nIndex){

	return mDataPoints[nIndex];
}

// Manage probability updates if a new points is added
void RandomSampler::AddPointUpdateProbability(unsigned int nIndex){

	mNeighCustomVecF[nIndex] = 1.0f;
	SetCumSumHist (&mNeighCustomVecFCumSum, mNeighCustomVecF);

	if((mNFSType == NFST_NN || mNFSType == NFST_EXP) && mMaxAllocablePoints != mDataPoints[0]->mSecondSamplingProbability.size() ){
		// Check for reallocation
		for(unsigned int i=0; i<(unsigned int)mMaxAllocablePoints; i++){

			if(i<mDataPoints.size()){
				// Reallocate pw distance
				if(mNFSType == NFST_EXP)
					mDataPoints[i]->mPairwiseEuclideanDistance.resize(mMaxAllocablePoints,0.0f);
			
				// Reallocate histograms
				mDataPoints[i]->mSecondSamplingProbability.resize(mMaxAllocablePoints,0.0f);			
				mDataPoints[i]->mSecondSamplingProbabilityCumSum.resize(mMaxAllocablePoints,0.0f); 
			}

		}	
	}

	// Case KD-Tree
	if(mNFSType == NFST_NN || mNFSType == NFST_NN_ME){
		sPtPointer ptPointer; ptPointer.mPt = (mDataPoints[nIndex]); ptPointer.mPt->mAlreadyFound = false;
		mKDTree.insert(ptPointer);
		mKDTree.optimise();
	
		if(mNFSType == NFST_NN){

			std::list<sPtPointer> kNeigh = FindKNearest(ptPointer,std::numeric_limits<float>::max(),mDNeigh, (int)mActivePoints);
			
			for(unsigned int i=0; i< mDataPoints[nIndex]->mSecondSamplingProbability.size(); i++){
				if(mDataPoints[i]->mActive){
					(mDataPoints[i]->mSecondSamplingProbability)[nIndex] = mNNPFar;
					(mDataPoints[nIndex]->mSecondSamplingProbability)[i] = mNNPFar;
				}
			}

			for(std::list<sPtPointer>::iterator it = kNeigh.begin(); it != kNeigh.end(); ++it){
				(mDataPoints[nIndex]->mSecondSamplingProbability)[(*it).mPt->mIndex] = mNNPClose;
				(mDataPoints[(*it).mPt->mIndex]->mSecondSamplingProbability)[nIndex] = mNNPClose;
				SetCumSumHist (&mDataPoints[(*it).mPt->mIndex]->mSecondSamplingProbabilityCumSum, mDataPoints[(*it).mPt->mIndex]->mSecondSamplingProbability);				
			}
			
			for(unsigned int i=0; i< mDataPoints.size(); i++){
				if(mDataPoints[i]->mActive){
					SetCumSumHist (&mDataPoints[i]->mSecondSamplingProbabilityCumSum, mDataPoints[i]->mSecondSamplingProbability);				
				}
			}			
		}
	}

	// Exponential case
	if(mNFSType == NFST_EXP){
		// update pairwise euclidean distance beetween points
		float eucDist = 0.0f;
		float expTemp = 0.0f;
		(mDataPoints[nIndex]->mPairwiseEuclideanDistance)[nIndex] = 0.0f;
		for(unsigned int j=0; j < mDataPoints.size(); j++){
			if(!mDataPoints[j]->mActive)
				continue;
			// Compute euclidean distance
			eucDist = VecEuclideanDist(mDataPoints[nIndex]->mCoord1, mDataPoints[j]->mCoord1);
			(mDataPoints[nIndex]->mPairwiseEuclideanDistance)[j] = eucDist;
			(mDataPoints[j]->mPairwiseEuclideanDistance)[nIndex] = eucDist;
			
			if(j == nIndex)
				expTemp = 0.0f;
			else
				expTemp = exp(-mSigmaExp/eucDist);

			(mDataPoints[nIndex]->mSecondSamplingProbability)[j] = expTemp;
			(mDataPoints[j]->mSecondSamplingProbability)[nIndex] = expTemp;

			// Reset cumsum pdf
			SetCumSumHist (&mDataPoints[j]->mSecondSamplingProbabilityCumSum, mDataPoints[j]->mSecondSamplingProbability);
		}
		SetCumSumHist (&mDataPoints[nIndex]->mSecondSamplingProbabilityCumSum, mDataPoints[nIndex]->mSecondSamplingProbability);
	}
}

void RandomSampler::RemovePointUpdateProbability(unsigned int nIndex){

	mNeighCustomVecF[nIndex] = 0.0f;
	SetCumSumHist (&mNeighCustomVecFCumSum, mNeighCustomVecF);

	// Case KD-Tree
	if(mNFSType == NFST_NN || mNFSType == NFST_NN_ME){

		if(mNFSType == NFST_NN){
			for(unsigned int i=0; i< mDataPoints[nIndex]->mSecondSamplingProbability.size(); i++){
				(mDataPoints[i]->mSecondSamplingProbability)[nIndex] = 0.0f;
				(mDataPoints[nIndex]->mSecondSamplingProbability)[i] = 0.0f;
				SetCumSumHist (&mDataPoints[i]->mSecondSamplingProbabilityCumSum, mDataPoints[i]->mSecondSamplingProbability);
				
			}
			
			SetCumSumHist (&mDataPoints[nIndex]->mSecondSamplingProbabilityCumSum, mDataPoints[nIndex]->mSecondSamplingProbability);
		}
		sPtPointer ptPointer; ptPointer.mPt = (mDataPoints[nIndex]); ptPointer.mPt->mAlreadyFound = false;
		mKDTree.erase(ptPointer);
		mKDTree.optimise();
	}

	// Exponential case
	if(mNFSType == NFST_EXP){
		for(unsigned int j=0; j < mDataPoints.size(); j++){
			// Compute euclidean distance
			(mDataPoints[nIndex]->mSecondSamplingProbability)[j] = 0.0f;
			(mDataPoints[j]->mSecondSamplingProbability)[nIndex] = 0.0f;
			// Reset cumsum pdf
			SetCumSumHist (&mDataPoints[j]->mSecondSamplingProbabilityCumSum, mDataPoints[j]->mSecondSamplingProbability);
		}
	}

}


// Set probability for the non-first sampling as uniform
void RandomSampler::SetNFSamplingTypeUniform(){

	mNFSType = NFST_UNIFORM;
}

// Set Non first sampling type as Exp
void RandomSampler::SetNFSamplingTypeExp(float nSigmaExp){
	

	mSigmaExp = nSigmaExp;
	mNFSType = NFST_EXP;


	// Allocate necessary space
	for(unsigned int i=0; i < mMaxAllocablePoints; i++){
		
		mDataPoints[i]->mPairwiseEuclideanDistance.resize(mMaxAllocablePoints, 0.0f);

		mDataPoints[i]->mSecondSamplingProbability.resize(mMaxAllocablePoints, 0.0f);

		mDataPoints[i]->mSecondSamplingProbabilityCumSum.resize(mMaxAllocablePoints, 0.0f);
	}	

	// Some temp values
	float eucDist = 0.0f;
	float expTemp = 0.0f;

	// Compute pairwise euclidean distance beetween points
	for(unsigned int i=0; i<mMaxAllocablePoints; i++){
		if(!mDataPoints[i]->mActive)
			continue;
		(mDataPoints[i]->mPairwiseEuclideanDistance)[i] = 0.0f;
		for(unsigned int j=i+1; j < mMaxAllocablePoints; j++){
			if(!mDataPoints[j]->mActive)
				continue;
			// Compute euclidean distance
			eucDist = VecEuclideanDist(mDataPoints[i]->mCoord1, mDataPoints[j]->mCoord1);
			(mDataPoints[i]->mPairwiseEuclideanDistance)[j] = eucDist;
			(mDataPoints[j]->mPairwiseEuclideanDistance)[i] = eucDist;
			expTemp = exp(-nSigmaExp/eucDist);
			(mDataPoints[i]->mSecondSamplingProbability)[j] = expTemp;
			(mDataPoints[j]->mSecondSamplingProbability)[i] = expTemp;
		}
	}

	for(unsigned int i=0; i<mMaxAllocablePoints; i++){
		if(!mDataPoints[i]->mActive)
			continue;
		SetCumSumHist(&mDataPoints[i]->mSecondSamplingProbabilityCumSum, mDataPoints[i]->mSecondSamplingProbability);
	}

}

void RandomSampler::SetNFSamplingTypeNN(int nDNeigh, float nPClose, float nPFar, bool nMemoryEfficient ){

	if(nMemoryEfficient)
		mNFSType = NFST_NN_ME;
	else
		mNFSType = NFST_NN;
	
	mDNeigh = nDNeigh;
	mNNPClose = nPClose / (nPClose + nPFar);
	mNNPFar = nPFar / (nPClose + nPFar);

	
	mKDTree.clear();

	// Add points to the kd-tree
	for(unsigned int i=0; i < mMaxAllocablePoints; i++){
		if(mDataPoints[i]->mActive){
			sPtPointer ptPointer; ptPointer.mPt = (mDataPoints[i]); ptPointer.mPt->mAlreadyFound = false;
			mKDTree.insert(ptPointer);
		}
	}

	mKDTree.optimise();
	
	if(mNFSType == NFST_NN){
		std::vector<sPtPointer> nearPtsIdx;
		nearPtsIdx.reserve(mMaxAllocablePoints);
		for(unsigned int i=0; i < mMaxAllocablePoints; i++){
			if(mDataPoints[i]->mActive){
				nearPtsIdx.clear();
				
				mDataPoints[i]->mSecondSamplingProbability.resize(mMaxAllocablePoints, 0.0f);
				mDataPoints[i]->mSecondSamplingProbabilityCumSum.resize(mMaxAllocablePoints, 0.0f);
				
				sPtPointer ptPointer; ptPointer.mPt = (mDataPoints[i]); ptPointer.mPt->mAlreadyFound = false;

				std::list<sPtPointer> kNeigh = FindKNearest(ptPointer,std::numeric_limits<float>::max(),mDNeigh, (int)mActivePoints);
				
				for(unsigned int j=0; j< mDataPoints[i]->mSecondSamplingProbability.size(); j++){
					if(mDataPoints[j]->mActive)
						(mDataPoints[i]->mSecondSamplingProbability)[j] = mNNPFar ;
				}
		
				for(std::list<sPtPointer>::iterator it = kNeigh.begin(); it != kNeigh.end(); ++it){
					(mDataPoints[i]->mSecondSamplingProbability)[(*it).mPt->mIndex] = mNNPClose ;
				}
				
				SetCumSumHist (&mDataPoints[i]->mSecondSamplingProbabilityCumSum, mDataPoints[i]->mSecondSamplingProbability);
			}
		}	
	}
	

}

void RandomSampler::InitializeNFSampling(){

	switch(mNFSType){
		case NFST_UNIFORM:
				SetNFSamplingTypeUniform();
			break;
		case NFST_EXP:
				SetNFSamplingTypeExp(mSigmaExp);
			break;
		case NFST_NN:
				SetNFSamplingTypeNN(mDNeigh, mNNPClose, mNNPFar, false );
		case NFST_NN_ME:
			SetNFSamplingTypeNN(mDNeigh, mNNPClose, mNNPFar, true );
			break;
		default:
				assert(1 || printf("Invalid NFS"));
			break;
	}

}

	// Get(And Set) The vector of Custom sampling type for the first selection
double RandomSampler::GetFirstSamplingProb(unsigned int elIndex) const{
	assert(elIndex < mNeighCustomVecF.size());
	return mNeighCustomVecF[elIndex];
}
void RandomSampler::SetFirstSamplingProb(unsigned int elIndex, float newValue){
	assert(elIndex < mNeighCustomVecF.size());
	mNeighCustomVecF[elIndex] = newValue;
	SetCumSumHist (&mNeighCustomVecFCumSum, mNeighCustomVecF);
}
unsigned int RandomSampler::GetNumberOfLoadedDataPoints(){

	return mActivePoints;
}

unsigned int RandomSampler::GetActiveIndex(unsigned int nIndex) const{
	
	assert(nIndex < mActivePoints);

	unsigned int i = 0;
	unsigned int jIndex = 0;
	// mFreeCells are supposed to be sorted
	while(true){
		if(jIndex == nIndex && mDataPoints[i]->mActive)
			break;

		if(mDataPoints[i]->mActive)
			jIndex++;

		i++;
		
	}

	return i;

}

// Get preferences set from N samples
void RandomSampler::GetNSample(std::vector<std::vector<unsigned int>> &randomPoints, unsigned int nSampleN){

	if(this->mActivePoints < this->mMSS){
		return;
	}

#ifndef RS_NO_THREADS
	boost::thread_group *tg = new boost::thread_group();
#endif RS_NO_THREADS
		
	// Get all the samples
	for(unsigned int n=0; n<nSampleN; n++){

		unsigned int currentModelIndex = n;

		std::vector<unsigned int> nSample(mMSS);

	#ifdef RS_NO_THREADS

		#ifdef _DEBUG
		// If we are in debug mode, generate hypotesis in a non random way
			static unsigned int nCounter = 0;
			nCounter = nCounter % this->mActivePoints;
			nSample[0] = (unsigned int)GetActiveIndex(nCounter);
			nCounter++;
		#else
			nSample[0] = (unsigned int)RandomSampleOnCumSumHist(mNeighCustomVecFCumSum);
		#endif
		GetNonFirstSamples(&nSample);

		randomPoints.push_back(nSample);

	#else

		tg->create_thread( boost::bind(&RandomSampler::GetSampleMultiThreadWrapper,this, nSample, currentModelIndex,nPrevModels, -1));

		if(tg->size() >= MAXTHREADS){
			tg->join_all();
			delete tg;
			tg = new boost::thread_group();
		}
	#endif

	}

#ifndef RS_NO_THREADS
	if(tg->size() > 0)
		tg->join_all();
	delete tg;
#endif RS_NO_THREADS

	return;
}

// Get non first samples - The first sample must be set in the nSample vector
void RandomSampler::GetNonFirstSamples(std::vector<unsigned int> *nSample) {

	assert(nSample->size() == mMSS);

	unsigned int nDegenerateCases = 0;
	for(unsigned int i=1; i<mMSS; i++){
		bool foundUnique = false;
		while(!foundUnique){
			// Check if degenerate max n has been reached
			if(nDegenerateCases > MAXDEGENERATESAMPLES){
				assert(1 || printf("Max number of degenerate samples reached"));
				for(unsigned int i=1; i<mMSS; i++){
					(*nSample)[i] = GetActiveIndex(GetActiveIndex((*nSample)[i-1] + 1) % mActivePoints);
				}
				return;
			}

			if(mNFSType == NFST_UNIFORM){	
					#ifdef _DEBUG
					// If we are in debug mode, generate hypotesis in a non random way
						static unsigned int nCounter = 0;
						nCounter = nCounter % this->mActivePoints;
						(*nSample)[i] = (unsigned int)GetActiveIndex(nCounter);
						nCounter++;
					#else
						(*nSample)[i] = GetActiveIndex((unsigned int) (((float)rand()/(float)RAND_MAX)*(float)(mActivePoints-1)));			
					#endif

			}
			else if (mNFSType == NFST_EXP || mNFSType == NFST_NN ){	
						#ifdef _DEBUG
						// If we are in debug mode, generate hypotesis in a non random way
							static unsigned int nCounter = 0;
							nCounter = nCounter % this->mActivePoints;
							(*nSample)[i] = (unsigned int)GetActiveIndex(nCounter);
							nCounter++;
						#else
							(*nSample)[i] = (unsigned int)RandomSampleOnCumSumHist (mDataPoints[(*nSample)[0]]->mSecondSamplingProbabilityCumSum);
						#endif
			}
			else if (mNFSType == NFST_NN_ME){
				#ifdef _DEBUG
				static unsigned int nCounterOut = 0;
				nCounterOut = nCounterOut % 100;
				nCounterOut++;
				if((float)nCounterOut/100.f < mNNPClose){
				#else
				if(((float)rand()/(float)RAND_MAX) < mNNPClose){
				#endif
					
					
					sPtPointer ptPointer; ptPointer.mPt = ((mDataPoints[(*nSample)[0]])); ptPointer.mPt->mAlreadyFound = false;

					std::list<sPtPointer> kneigh = FindKNearest(ptPointer,std::numeric_limits<float>::max(),mDNeigh, (int)mActivePoints);
					int size = (int)kneigh.size();
					if(size > (int)mMSS){
						#ifdef _DEBUG
						// If we are in debug mode, generate hypotesis in a non random way
							static unsigned int nCounter = 0;
							nCounter = nCounter % size;
							std::list<sPtPointer>::iterator kneighIt = kneigh.begin();
							unsigned h = 0;
							while(h < nCounter && kneighIt != kneigh.end()){
								++kneighIt;
								++h;
							}

							(*nSample)[i] = (*kneighIt).mPt->mIndex;		
							nCounter++;
						#else			
							unsigned int idx = 1+(unsigned int) (((float)rand()/(float)RAND_MAX)*(float)(size-2));
							std::list<sPtPointer>::iterator kneighIt = kneigh.begin();
							unsigned h = 0;
							while(h < idx && kneighIt != kneigh.end()){
								++kneighIt;
								++h;
							}

							(*nSample)[i] = (*kneighIt).mPt->mIndex;		
						#endif					
					}
					else{
						#ifdef _DEBUG
						// If we are in debug mode, generate hypotesis in a non random way
							static unsigned int nCounter = 0;
							nCounter = nCounter % this->mActivePoints;
							(*nSample)[i] = (unsigned int)GetActiveIndex(nCounter);
							nCounter++;
						#else
							(*nSample)[i] = GetActiveIndex((unsigned int) (((float)rand()/(float)RAND_MAX)*(float)(mActivePoints-1)));					
						#endif
					}
				}
				else{
					#ifdef _DEBUG
					// If we are in debug mode, generate hypotesis in a non random way
						static unsigned int nCounter = 0;
						nCounter = nCounter % this->mActivePoints;
						(*nSample)[i] = (unsigned int)GetActiveIndex(nCounter);
						nCounter++;
					#else
						(*nSample)[i] = GetActiveIndex((unsigned int) (((float)rand()/(float)RAND_MAX)*(float)(mActivePoints-1)));					
					#endif
			}
							
				
			}
			foundUnique = true;
			for(unsigned int j=0; j<i; j++){
				if((*nSample)[i] == (*nSample)[j]){
					foundUnique = false;
					nDegenerateCases++;
					break;
				}
			}
		}

	}
}

std::list<sPtPointer> RandomSampler::FindKNearest(sPtPointer __V, float const __Max_R, int k, int nMaxSize){
		int msSizePRAF = 0;
		std::list<sPtPointer> msAlreadyFoundPRAF;
		__V.mPt->mAlreadyFound = true;
		while(msSizePRAF < k && msSizePRAF < nMaxSize ){
			std::pair<KDTree::KDTree<sPtPointer>::iterator,float> nif = mKDTree.find_nearest_if(__V,__Max_R,sPredicateAlreadyFoundRS());
			msAlreadyFoundPRAF.push_back(*nif.first);
			msAlreadyFoundPRAF.back().mPt->mAlreadyFound = true;
			msSizePRAF++;
		}
		// Reset values
		for(std::list<sPtPointer>::iterator it = msAlreadyFoundPRAF.begin(); it != msAlreadyFoundPRAF.end(); ++it)
			(*it).mPt->mAlreadyFound = false;
		__V.mPt->mAlreadyFound = false;

	  return msAlreadyFoundPRAF;
}

void RandomSampler::GetSampleMultiThreadWrapper(RandomSampler *nRandomSampler, std::vector<unsigned int> &nSample, unsigned int nCurModelIdx, std::vector<cv::Mat> *nModelPt, int nStartingPoint){
	
	if(nStartingPoint < 0)
		nSample[0] = (unsigned int)RandomSampleOnCumSumHist (nRandomSampler->mNeighCustomVecFCumSum);
	else
		nSample[0] = nStartingPoint;

	nRandomSampler->GetNonFirstSamples(&nSample);

	cv::Mat *nModelParams =	nRandomSampler->mGetFunction(nRandomSampler->mDataPoints, nSample);

	(*nModelPt)[nCurModelIdx] = *nModelParams;

}
