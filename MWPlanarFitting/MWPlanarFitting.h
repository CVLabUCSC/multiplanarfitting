#pragma once

#include "ImageData.h"
#include "types.h"
#include "RandomSampler.h"
#include "HomographyEstimator.h"

class MWPlanarFitting
{
private:
	std::vector<ImageData> mImageData;	// Array of image data

	// Load all image data from given folder
	bool loadImageDataFromFolder(const std::string &data_folder);

	// Load image data from given data file
	bool loadImageData(const std::string &data_file);

	// Load image data list fromm given data file
	bool loadDataList(const std::string &data_file);

	// Compute point matches of given image data
	void computeMatches(const ImageData imageData1, const ImageData imageData2,
		std::vector<cv::Point2f> &points1, std::vector<cv::Point2f> &points2) const;

	// Generate models with given point matches.
	std::vector<HomographyModel> generateModels(const int numOfModels, const HomographyEstimator &estimator,
		const std::vector<cv::Point2f> &points1, const std::vector<cv::Point2f> &points2) const;

	// Initialize random sampler to sample points of the minimum sample sets.
	void initRandomSampler(RandomSampler &randomSampler, const std::vector<cv::Point2f> &points1,
		const std::vector<cv::Point2f> &points2, int num_of_models, RS_NFSAMPLINGTYPE samplingType = NFST_UNIFORM) const;
	
public:
	MWPlanarFitting(const std::string &path, bool fromFolder = false);
	~MWPlanarFitting();

	// Compute clusters using T-linkage with Manhattan-world model
	Clusters clustering(const int imageIndex1, const int imageIndex2,
		const int numOfModels, const double reproj_threshold) const;

	// Get image of given index
	cv::Mat getImage(const int imageIndex) const;
};

