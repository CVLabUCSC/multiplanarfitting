#pragma once

#include <vector>
#include <string>

#include <opencv2\xfeatures2d\nonfree.hpp>
#include <opencv2\opencv.hpp>

class ImageData
{
private:
	cv::Mat mImage;
	cv::Mat mVanishingDirections;
	cv::Mat mCalibratrion;

	std::vector<cv::KeyPoint> mKeyPoints;
	cv::Mat mDescriptors;

	// Load image and vanishing directions from given file pathes
	bool loadData(const std::string &image_file, const std::string &vd_file, const std::string &calib_file);

	// Compute feature points from the stored image
	void computeFeaturePoints();

public:
	ImageData();
	ImageData(const std::string &image_file, const std::string &vd_file, const std::string &calib_file);
	~ImageData();

	bool initialize(const std::string &image_file, const std::string &vd_file, const std::string &calib_file);

	// Get a copy of key points
	std::vector<cv::KeyPoint> getKeyPoints() const;

	// Get a copy of descriptors
	cv::Mat getDescriptors() const;

	// Get a copy of vanishing directions matrix
	cv::Mat getVanishingDirections() const;

	// Get a copy of camera calibration matrix
	cv::Mat getCalibrationMatrix() const;

	// Get a copy of image
	cv::Mat getImage() const;
};

