#include "types.h"

namespace ClusterUtils
{
	// Transform a vector of Cluster into Clusters
	Clusters clusterArrayToClusters(const std::vector<Cluster> &input,
		const std::vector<cv::Point2f> points1, const std::vector<cv::Point2f> points2);

	// Draw points on given image
	cv::Mat DrawPointsOnImage(const Clusters &clusters, const cv::Mat &image, bool drawOutliers = true, int radius = 3);
}
