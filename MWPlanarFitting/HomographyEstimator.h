#pragma once

#include "ImageData.h"
#include"types.h"

static const int MINIMUM_SAMPLE_SET = 2;

class HomographyEstimator
{
private:
	cv::Mat mK1;	// Camera calibration matrix of first image
	cv::Mat mK2;	// Camera calibration matrix of second image
	cv::Mat mR;		// Rotation between two cameras
	cv::Mat mVD1;	// vanishing directions of the first image
	cv::Mat mVD2;	// vanishing directions of the second image

	// Compute vanishing lines from given calibration matrix K and vanishing
	// directions VD. Each column of VD represents the vanishing direction.
	// Each column of returning matrix will represents a vanishing line on
	// the image.
	cv::Mat computeVanishingLines(const cv::Mat &K, const cv::Mat &VD) const;

	// Normalize the P so that the last row of P has 1s. Each column of P
	// represents a point in a homogenous coordinate system.
	cv::Mat pointsFromHomogeneous(const cv::Mat &P) const;

	// Seperate point matches (points1_in, points2_in) into two groups using given vanishing
	// line VL, then pick the larger group and save them to points1_out and points2_out.
	// The point matches will be separated with only points in points1_in.
	void pickLargerGroupVL(const std::vector<cv::Point2f> &points1_in, const std::vector<cv::Point2f> &points2_in,
		const cv::Mat &VL, std::vector<cv::Point2f> &points1_out, std::vector<cv::Point2f> &points2_out) const;

	// Separte point matches with given vanishing line VL. Points on one side of
	// vanishing line will be stored to points1_out, and points on the other
	// side will be stored to points2_out.
	void separateMatchesVL(const std::vector<cv::Point2f> &points1_in, const std::vector<cv::Point2f> &points2_in,
		const cv::Mat &VL, std::vector<Match> &pointsNegGroup1_out, std::vector<Match> &pointsPosGroup2_out) const;

public:
	HomographyEstimator(const ImageData &imageData1, const ImageData &imageData2,
		const std::vector<cv::Point2f> &points1, const std::vector<cv::Point2f> &points2);
	~HomographyEstimator();

	// Compute homographies constrained on pre-computed rotation and normal vectors.
	// The result models will be added to the 'models'.
	void computeMWHomographies(const std::vector<cv::Point2f> &points1, const std::vector<cv::Point2f> &points2,
		std::vector<HomographyModel> &models) const;

	// Compute constrained homography using given matching points with given index of normal vectors.
	cv::Mat computeMWHomography(const std::vector<cv::Point2f> &points1, const std::vector<cv::Point2f> &points2,
		const int normal_index) const;
};
