#include "ImageData.h"

#include <iostream>

#include "IOUtils.h"

using namespace std;
using namespace cv;
using namespace cv::xfeatures2d;


ImageData::ImageData()
{
}

ImageData::ImageData(const string &image_file, const string &vd_file, const string &calib_file)
{
	assert(initialize(image_file, vd_file, calib_file));
}

ImageData::~ImageData()
{
}


bool ImageData::initialize(const string &image_file, const string &vd_file, const string &calib_file)
{
	if (!loadData(image_file, vd_file, calib_file)) {
		return false;
	}

	computeFeaturePoints();

	return true;
}

vector<KeyPoint> ImageData::getKeyPoints() const
{
	return mKeyPoints;
}

Mat ImageData::getDescriptors() const
{
	return mDescriptors.clone();
}

Mat ImageData::getVanishingDirections() const
{
	return mVanishingDirections.clone();
}

Mat ImageData::getCalibrationMatrix() const
{
	return mCalibratrion.clone();
}

cv::Mat ImageData::getImage() const
{
	return mImage.clone();
}

bool ImageData::loadData(const string &image_file, const string &vd_file, const string &calib_file)
{
	// Read an image file
	mImage = imread(image_file);
	if (mImage.empty()) {
		cout << "Failed to read image file: " << image_file << endl;
		system("pause");
		return false;
	}

	// Read vanishing directions data
	mVanishingDirections = load3x3Matrix(vd_file);
	if (mVanishingDirections.empty()) {
		cout << "Failed to read vanishing directions data from: " << vd_file << endl;
		system("pause");
		return false;
	}

	mCalibratrion = load3x3Matrix(calib_file);
	if (mCalibratrion.empty()) {
		cout << "Failed to read a camera calibration matrix from: " << vd_file << endl;
		system("pause");
		return false;
	}

	return true;
}

void ImageData::computeFeaturePoints()
{
	Ptr<SIFT> detector = SIFT::create();
	Mat grayImg;

	if (mImage.channels() == 3) {
		cvtColor(mImage, grayImg, CV_BGR2GRAY);
	}
	else {
		grayImg = mImage.clone();
	}

	detector->detect(grayImg, mKeyPoints);					// Compute key points
	detector->compute(grayImg, mKeyPoints, mDescriptors);	// Compute descriptors

#ifdef _DEBUG
	cout << mKeyPoints.size() << " features found" << endl;
#endif
}
