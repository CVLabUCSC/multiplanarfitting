#pragma once

#include <string>
#include <opencv2\opencv.hpp>

// Load 3x3 matrix from given file
cv::Mat load3x3Matrix(const std::string &filename);
