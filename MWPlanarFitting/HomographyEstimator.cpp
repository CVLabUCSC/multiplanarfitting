#include "HomographyEstimator.h"

using namespace std;
using namespace cv;

HomographyEstimator::HomographyEstimator(const ImageData &imageData1, const ImageData &imageData2,
	const vector<Point2f> &points1, const vector<Point2f> &points2)
{
	mK1 = imageData1.getCalibrationMatrix();
	mK2 = imageData2.getCalibrationMatrix();
	mVD1 = imageData1.getVanishingDirections();
	mVD2 = imageData2.getVanishingDirections();
	mR = mVD2 * mVD1.inv();
}

HomographyEstimator::~HomographyEstimator()
{
}

void HomographyEstimator::computeMWHomographies(const vector<Point2f> &points1, const vector<Point2f> &points2,
	vector<HomographyModel> &models) const
{
	// The calibration matrices, mNormals, rotation matrix and vanishing directions must not empty
	assert(mVD1.cols == 3 && mVD1.rows == 3 && mVD2.cols == 3 && mVD2.rows == 3);

	// Compute vanishing points
	Mat VL = computeVanishingLines(mK1, mVD1);

	for (unsigned int ni = 0; ni < mVD1.cols; ni++) {
		vector<Point2f> points1_subset, points2_subset;

		pickLargerGroupVL(points1, points2, VL.col(ni), points1_subset, points2_subset);

		if (points1_subset.size() >= MINIMUM_SAMPLE_SET) {
			HomographyModel model;

			model.Homography = computeMWHomography(points1_subset, points2_subset, ni);
			model.plane_normal = mVD1.col(ni);
			model.normal_index = ni;

			models.push_back(model);
		}
	}
}

Mat HomographyEstimator::computeMWHomography(const vector<Point2f> &points1,
	const vector<Point2f> &points2, const int normal_index) const
{
	assert(normal_index < mVD1.cols);
	int n_points = (int)points1.size();
	Mat p1(3, n_points, CV_64F), p2(3, n_points, CV_64F);
	Mat normal = mVD1.col(normal_index);

	// Points setting
	for (int i = 0; i < n_points; i++)
	{
		p1.at<double>(0, i) = points1[i].x;
		p1.at<double>(1, i) = points1[i].y;
		p1.at<double>(2, i) = 1.0;

		p2.at<double>(0, i) = points2[i].x;
		p2.at<double>(1, i) = points2[i].y;
		p2.at<double>(2, i) = 1.0;
	}

	Mat invKp1 = mK1.inv() * p1;
	Mat invKp2 = mK2.inv() * p2;
	Mat alpha = normal.t() * invKp1;
	Mat b(n_points * 2, 1, CV_64F);
	Mat A = Mat::zeros(n_points * 2, 3, CV_64F);
	for (int pi = 0; pi < n_points; pi++)
	{
		b.at<double>(pi * 2, 0) = mR.at<double>(0, 0)*invKp1.at<double>(0, pi) + mR.at<double>(0, 1)*invKp1.at<double>(1, pi) + mR.at<double>(0, 2)
			- mR.at<double>(2, 0)*invKp1.at<double>(0, pi)*invKp2.at<double>(0, pi) - mR.at<double>(2, 1)*invKp1.at<double>(1, pi)*invKp2.at<double>(0, pi) - mR.at<double>(2, 2)*invKp2.at<double>(0, pi);

		b.at<double>(pi * 2 + 1, 0) = mR.at<double>(1, 0)*invKp1.at<double>(0, pi) + mR.at<double>(1, 1)*invKp1.at<double>(1, pi) + mR.at<double>(1, 2)
			- mR.at<double>(2, 0)*invKp1.at<double>(0, pi)*invKp2.at<double>(1, pi) - mR.at<double>(2, 1)*invKp1.at<double>(1, pi)*invKp2.at<double>(1, pi) - mR.at<double>(2, 2)*invKp2.at<double>(1, pi);

		A.at<double>(pi * 2, 0) = -alpha.at<double>(pi);
		A.at<double>(pi * 2 + 1, 1) = -alpha.at<double>(pi);
		A.at<double>(pi * 2, 2) = alpha.at<double>(pi) * invKp2.at<double>(0, pi);
		A.at<double>(pi * 2 + 1, 2) = alpha.at<double>(pi) * invKp2.at<double>(1, pi);
	}

	Mat t_hat = (A.t() * A).inv() * A.t() * b;

	return Mat(mK2 * (mR + t_hat * normal.t()) * mK1.inv());
}

Mat HomographyEstimator::computeVanishingLines(const Mat &K, const Mat &VD) const
{
	Mat VP = K * VD;
	Mat VL(3, 3, CV_64F);

	VP = pointsFromHomogeneous(VP);

	VP.col(1).cross(VP.col(2)).copyTo(VL.col(0));
	VP.col(0).cross(VP.col(2)).copyTo(VL.col(1));
	VP.col(0).cross(VP.col(1)).copyTo(VL.col(2));

	return VL;
}

Mat HomographyEstimator::pointsFromHomogeneous(const Mat &P) const
{
	Mat P_normalized = P.clone();

	for (int i = 0; i < P.cols; i++) {
		P_normalized.at<double>(0, i) /= P_normalized.at<double>(2, i);
		P_normalized.at<double>(1, i) /= P_normalized.at<double>(2, i);
		P_normalized.at<double>(2, i) /= P_normalized.at<double>(2, i);
	}

	return P_normalized;
}

void HomographyEstimator::pickLargerGroupVL(const vector<Point2f> &points1_in, const vector<Point2f> &points2_in,
	const Mat &VL, vector<Point2f> &points1_out, vector<Point2f> &points2_out) const
{
	vector<Match> points_group1, points_group2;

	separateMatchesVL(points1_in, points2_in, VL, points_group1, points_group2);

	if (points_group1.size() > points_group2.size()) {
		for (unsigned int i = 0; i < points_group1.size(); i++) {
			points1_out.push_back(points_group1[i].first);
			points2_out.push_back(points_group1[i].second);
		}
	}
	else {
		for (unsigned int i = 0; i < points_group2.size(); i++) {
			points1_out.push_back(points_group2[i].first);
			points2_out.push_back(points_group2[i].second);
		}
	}
}

void HomographyEstimator::separateMatchesVL(const vector<Point2f> &points1_in, const vector<Point2f> &points2_in,
	const Mat &VL, vector<Match> &pointsNegGroup1_out, vector<Match> &pointsPosGroup2_out) const
{
	double a = VL.at<double>(0);
	double b = VL.at<double>(1);
	double c = VL.at<double>(2);

	pointsNegGroup1_out.clear();
	pointsPosGroup2_out.clear();

	for (unsigned int i = 0; i < points1_in.size(); i++) {
		double val = points1_in[i].x * a + points1_in[i].y * b + c;
		if (val < 0.0) {
			pointsNegGroup1_out.push_back(Match(points1_in[i], points2_in[i]));
		}
		else if (val > 0.0) {
			pointsPosGroup2_out.push_back(Match(points1_in[i], points2_in[i]));
		}
		else {
			pointsNegGroup1_out.push_back(Match(points1_in[i], points2_in[i]));
			pointsPosGroup2_out.push_back(Match(points1_in[i], points2_in[i]));
		}
	}
}
