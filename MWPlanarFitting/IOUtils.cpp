#include "IOUtils.h"

#include <fstream>

using namespace std;
using namespace cv;

// Load 3x3 matrix from given file
Mat load3x3Matrix(const string &filename)
{
	Mat mat(3, 3, CV_64F);
	ifstream fin(filename, ifstream::in);

	if (!fin.is_open()) {
		cout << "Not able to open the 3x3 matrix data file: " << filename << endl;
		return Mat();
	}

	fin >> mat.at<double>(0, 0);
	fin >> mat.at<double>(0, 1);
	fin >> mat.at<double>(0, 2);
	fin >> mat.at<double>(1, 0);
	fin >> mat.at<double>(1, 1);
	fin >> mat.at<double>(1, 2);
	fin >> mat.at<double>(2, 0);
	fin >> mat.at<double>(2, 1);
	fin >> mat.at<double>(2, 2);

	fin.close();

	return mat;
}
