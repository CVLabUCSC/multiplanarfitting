#include "MWPlanarFitting.h"

#include <fstream>
#include <algorithm>
#include <boost\filesystem.hpp>

#include "TLinkage.h"
#include "HomographyEstimator.h"
#include "ClusterUtils.h"

using namespace std;
using namespace cv;

static const double	NN_SQ_DIST_RATIO_THR = 0.49;

MWPlanarFitting::MWPlanarFitting(const string &path, bool fromFolder)
{
	if (fromFolder) {
		loadImageDataFromFolder(path);
	}
	else {
		loadDataList(path);
	}
}

MWPlanarFitting::~MWPlanarFitting()
{
}

Clusters MWPlanarFitting::clustering(const int imageIndex1, const int imageIndex2,
	const int numOfModels, const double reproj_threshold) const
{
	vector<Cluster> clusterArray;
	vector<Point2f> points1, points2;
	int KdTreeRange = 10;
	int minimumSampleSet = 2;

	// Compute point matches
	cout << "Compute point matches... ";
	computeMatches(mImageData[imageIndex1], mImageData[imageIndex2], points1, points2);
	cout << points1.size() << " matches were found." << endl;

	// Construct homography estimator
	HomographyEstimator estimator(mImageData[imageIndex1], mImageData[imageIndex2], points1, points2);

	// Generate models
	cout << "Generate models..." << endl;
	vector<HomographyModel> models = generateModels(numOfModels, estimator, points1, points2);

	TLinkage tlinkage(reproj_threshold, models.size(), true, 2, KdTreeRange);

	for (unsigned int i = 0; i < models.size(); i++) {
		tlinkage.AddModel(models[i]);
	}

	for (int i = 0; i < (int)points1.size(); i++) {
		tlinkage.AddPoint(points1[i], points2[i]);
	}

	// Do the J-linkage clustering
	cout << "Clustering with T-linkgae..." << endl;
	list<sClLnk *> tempClustersList = tlinkage.DoTLClusterization();

	// Copy Result Data
	tempClustersList.sort(ClusterSort());
	for (list<sClLnk *>::const_iterator citer = tempClustersList.begin(); citer != tempClustersList.end(); citer++)
	{
		if ((*citer)->mBelongingPts.size() <= minimumSampleSet)
			break;

		clusterArray.push_back(Cluster());

		vector<int> accumulator(3, 0);
		for (int i = 0; i < models.size(); i++) {
			if ((*citer)->mPreferenceFunction[i] > 0) {
				accumulator[models[i].normal_index]++;
			}
		}

		if (accumulator[0] > accumulator[1]) {
			if (accumulator[0] > accumulator[2]) {
				clusterArray.back().normal_index = 0;
				//cout << "normal: 0" << endl;
			}
			else {
				clusterArray.back().normal_index = 2;
				//cout << "normal: 2" << endl;
			}
		}
		else {
			if (accumulator[1] > accumulator[2]) {
				clusterArray.back().normal_index = 1;
				//cout << "normal: 1" << endl;
			}
			else {
				clusterArray.back().normal_index = 2;
				//cout << "normal: 2" << endl;
			}
		}

		for (list<sPtLnk *>::const_iterator piter = (*citer)->mBelongingPts.begin(); piter != (*citer)->mBelongingPts.end(); piter++) {
			clusterArray.back().points1.push_back((*piter)->mCoord1);
			clusterArray.back().points2.push_back((*piter)->mCoord2);
		}
	}

	return ClusterUtils::clusterArrayToClusters(clusterArray, points1, points2);
}

// Get image of given index
Mat MWPlanarFitting::getImage(const int imageIndex) const
{
	return mImageData[imageIndex].getImage();
}

bool MWPlanarFitting::loadImageDataFromFolder(const string &data_folder)
{
	// TODO: implement it
	assert(false);

	return true;
}

bool MWPlanarFitting::loadImageData(const string &data_file)
{
	// Validation
	if (data_file.substr(data_file.length() - 4, 4).compare(".txt") != 0) {
		cout << "Data file must be a text (*.txt) file" << endl;
		return false;
	}

	ifstream fin(data_file);
	string imagePath, VDPath, CalibPath;

	if (!fin.is_open())
	{
		cout << "Not able to open data file: " + data_file << endl;
		return false;
	}

	string rootFolder = boost::filesystem::path(data_file).parent_path().string() + '\\';

	for (string line; getline(fin, line);)
	{
		size_t firstIndex = line.find_first_not_of(' ');
		size_t lastIndex = line.find_last_of('#');
		char firstChar = line[firstIndex];
		if (firstChar == '#')
			continue;

		if (firstIndex != 0 || lastIndex != line.length())
			line = line.substr(firstIndex, lastIndex);

		size_t pos = line.find("=");
		string param_name = line.substr(0, pos++);
		string param_val = line.substr(pos, line.size());

		transform(param_name.begin(), param_name.end(), param_name.begin(), tolower);

		if (param_name.compare("image") == 0)
		{
			imagePath = rootFolder + param_val;
		}
		else if (param_name.compare("vd") == 0)
		{
			VDPath = rootFolder + param_val;
		}
		else if (param_name.compare("k") == 0)
		{
			CalibPath = rootFolder + param_val;
		}
		else
		{
			cout << "Undefined parameter: " << param_name << endl;
		}
	}

	if (imagePath.empty() || VDPath.empty() || CalibPath.empty()) {
		cout << "Not sufficient parameters were defined" << endl;
		return false;
	}

	fin.close();

	mImageData.push_back(ImageData(imagePath, VDPath, CalibPath));

	return true;
}

bool MWPlanarFitting::loadDataList(const string &data_file)
{
	ifstream fin(data_file);

	if (!fin.is_open())
	{
		cout << "Not able to open data file: " + data_file;
		return false;
	}

	for (string path; getline(fin, path);)
	{
		size_t firstIndex = path.find_first_not_of(' ');
		char firstChar = path[firstIndex];
		if (firstChar == '#')
			continue;

		if (firstIndex != 0)
			path = path.substr(firstIndex, path.size());

		loadImageData(path);
	}

	fin.close();

	return true;
}

void MWPlanarFitting::computeMatches(const ImageData imageData1, const ImageData imageData2,
	vector<Point2f> &points1, vector<Point2f> &points2) const
{
	// Match features
	FlannBasedMatcher matcher;
	vector<vector<DMatch>> matches;
	vector<KeyPoint> keypoints1 = imageData1.getKeyPoints();
	vector<KeyPoint> keypoints2 = imageData2.getKeyPoints();

	matcher.knnMatch(imageData1.getDescriptors(), imageData2.getDescriptors(), matches, 2);

	for (int i = 0; i < (int)matches.size(); i++)
	{
		if (matches[i].size() == 1) {
			points1.push_back(keypoints1[matches[i][0].queryIdx].pt);
			points2.push_back(keypoints2[matches[i][0].trainIdx].pt);
		}
		else if (matches[i].size() == 2) {
			if (matches[i][0].distance <= matches[i][1].distance * NN_SQ_DIST_RATIO_THR) {
				points1.push_back(keypoints1[matches[i][0].queryIdx].pt);
				points2.push_back(keypoints2[matches[i][0].trainIdx].pt);
			}
		}
	}
}

vector<HomographyModel> MWPlanarFitting::generateModels(const int numOfModels, const HomographyEstimator &estimator,
	const vector<Point2f> &points1, const vector<Point2f> &points2) const
{
	vector<HomographyModel> models;
	RandomSampler randomSampler(2, 4, points1.size());
	vector<vector<unsigned int>> indices;

	// Sample points
	initRandomSampler(randomSampler, points1, points2, points1.size());
	randomSampler.GetNSample(indices, numOfModels);

	// Generate models with sampled points
	for (int i = 0; i < (int)indices.size(); i++) {
		vector<Point2f> srcPoints1, srcPoints2;
		for (int j = 0; j < MINIMUM_SAMPLE_SET; j++) {
			srcPoints1.push_back(points1[indices[i][j]]);
			srcPoints2.push_back(points2[indices[i][j]]);
		}

		estimator.computeMWHomographies(srcPoints1, srcPoints2, models);
	}

	return models;
}

void MWPlanarFitting::initRandomSampler(RandomSampler &randomSampler, const vector<Point2f> &points1,
	const vector<Point2f> &points2, int num_of_models, RS_NFSAMPLINGTYPE samplingType) const
{
	float SigmaExp = 1.0;
	int KdTreeRange = 10;
	float KdTreeCloseProb = 0.8f;
	float KdTreeFarProb = 1.0f - KdTreeCloseProb;

	// Add points to the random sampler
	randomSampler.SetPoints(points1, points2);

	// Initialize non-first sampling type
	switch (samplingType)
	{
	case NFST_EXP:
		randomSampler.SetNFSamplingTypeExp(SigmaExp);
		break;

	case NFST_NN:
		randomSampler.SetNFSamplingTypeNN(KdTreeRange, KdTreeCloseProb, KdTreeFarProb, false);
		break;

	case NFST_NN_ME:
		randomSampler.SetNFSamplingTypeNN(KdTreeRange, KdTreeCloseProb, KdTreeFarProb, true);
		break;
	}
}
