#include "main.h"
#include "ClusterUtils.h"

using namespace std;

void planarFitting(string inputDataFile)
{
	MWPlanarFitting planarFitting(inputDataFile);

	Clusters clusters = planarFitting.clustering(0, 1, 2000, 2.5);

	imwrite("result.png", ClusterUtils::DrawPointsOnImage(clusters, planarFitting.getImage(0)));
}

void main()
{
	planarFitting("Input\\Entrance1.txt");

	system("pause");
}
