#include "ClusterUtils.h"

using namespace std;
using namespace cv;

namespace ClusterUtils
{
	Clusters clusterArrayToClusters(const vector<Cluster> &input,
		const vector<Point2f> points1, const vector<Point2f> points2)
	{
		Clusters clusters(input.size(), points1, points2);

		for (int i = 0; i < (int)input.size(); i++) {
			for (int j = 0; j < (int)input[i].size(); j++) {
				clusters.addPoint(input[i].points1[j], input[i].points2[j], i);
			}
		}

		clusters.removeEmptyClusters();

		return clusters;
	}

	Mat DrawPointsOnImage(const Clusters &clusters, const Mat &image, bool drawOutliers, int radius)
	{
		Mat result;
		RNG rng(0xFFFFFFFF);

		if (image.channels() == 3) {
			cvtColor(image, result, CV_BGR2GRAY);
			cvtColor(result, result, CV_GRAY2BGR);
		}
		else {
			result = image.clone();
		}

		for (size_t ci = 0; ci < clusters.size(); ci++)
		{
			int icolor = (unsigned)rng;
			Scalar color = Scalar(icolor & 255, (icolor >> 8) & 255, (icolor >> 16) & 255);

			for (size_t pi = 0; pi < clusters[ci].size(); pi++)
				circle(result, clusters[ci].points1[pi], radius, color, 2);
		}

		if (drawOutliers)
		{
			Cluster outliers = clusters.getOutliers();
			for (size_t pi = 0; pi < outliers.size(); pi++)
			{
				line(result, Point((int)outliers.points1[pi].x - radius, (int)outliers.points1[pi].y - radius),
					Point((int)outliers.points1[pi].x + radius, (int)outliers.points1[pi].y + radius), CV_RGB(255, 0, 0), 2);
				line(result, Point((int)outliers.points1[pi].x + radius, (int)outliers.points1[pi].y - radius),
					Point((int)outliers.points1[pi].x - radius, (int)outliers.points1[pi].y + radius), CV_RGB(255, 0, 0), 2);
			}
		}

		return result;
	}
}