#include "VPDetector.h"
#include "lsd.h"

#ifndef IOS
#include "Utility.h"
#endif






CVPDetector::CVPDetector(void)
{
   if(1)
    {
        // 288X352 (my iPhone 4)
        // Focal Length:          fc = [ 367.30534   368.01132 ] ± [ 1.01504   0.99361 ]
        // Principal point:       cc = [ 174.55167   146.07597 ] ± [ 0.90190   0.74748 ]
        // Skew:             alpha_c = [ 0.00000 ] ± [ 0.00000  ]   => angle of pixel axes = 90.00000 ± 0.00000 degrees
        // Distortion:            kc = [ 0.12234   -0.33467   0.00267   0.00089  0.00000 ] ± [ 0.00922   0.05994   0.00081   0.00109  0.00000 ]
        // Pixel error:          err = [ 0.06428   0.09663 ]        
//        m_K = (Mat_<double>(3,3) << 367.30534, 0., 174.55167,
//               0., 368.01132, 146.07597,
//               0., 0., 1.);
//        m_disto_coeff = (Mat_<double>(5,1) << 0.12234,   -0.33467,   0.00267,   0.00089,  0.00000);
        
        
        // iPhone 4S 640x480
        m_K = (Mat_<double>(3, 3) << 619.12047, 0, 319.50000, 0, 619.12047, 239.50000, 0, 0, 1);
        
        m_K = (Mat_<double>(3, 3) << 617.79435, 0, 310.89333, 0, 619.24783, 237.50111, 0, 0, 1);
        
        m_disto_coeff = (Mat_<double>(5,1) << 0.11106,   -0.32889,   0.00100,   -0.00092,  0.00000);
        
        initUndistortRectifyMap(m_K, m_disto_coeff,
                                Mat(), // optional rectification (none)
                                Mat(), // camera matrix to generate undistorted
                                Size(640, 480), // size of undistorted
                                CV_32FC1,  // type of output map
                                m_map1, m_map2);  // the x and y mapping functions
       
        
    }
    else
    {
        // 288X352 (German's iPhone 5)
        // Focal Length:          fc = [ 332.27511   332.35412 ] ± [ 0.35475   0.34814 ]
        // Principal point:       cc = [ 174.50851   143.09272 ] ± [ 0.34292   0.26447 ]
        // Skew:             alpha_c = [ 0.00000 ] ± [ 0.00000  ]   => angle of pixel axes = 90.00000 ± 0.00000 degrees
        // Distortion:            kc = [ 0.04825   0.02196   0.00102   -0.00117  0.00000 ] ± [ 0.00381   0.01990   0.00034   0.00045  0.00000 ]
        // Pixel error:          err = [ 0.05842   0.04690 ]
        
        m_K = (Mat_<double>(3,3) << 332.27511, 0., 174.50851,
                                    0., 332.35412, 143.09272,
                                    0., 0., 1.);
        m_disto_coeff = (Mat_<double>(5,1) << 0.04825,   0.02196,   0.00102,   -0.00117,  0.00000);
        
        initUndistortRectifyMap(m_K, m_disto_coeff,
                                Mat(), // optional rectification (none)
                                Mat(), // camera matrix to generate undistorted
                                Size(352, 288), // size of undistorted
                                CV_32FC1,  // type of output map
                                m_map1, m_map2);  // the x and y mapping functions
    }
    
    
    
    
        
	invert(m_K, m_Kinv);
	invert(m_K.t(), m_Ktinv);
    
    m_K_img1 = m_K.clone();
    m_K_img2 = m_K.clone();
    m_Kinv_img1 = m_Kinv.clone();
    m_Kinv_img2 = m_Kinv.clone();
    m_Ktinv_img1 = m_Ktinv.clone();
    m_Ktinv_img2 = m_Ktinv.clone();

	int cnt =0;
	for(int i=0; i<3; i++)
	{
		for(int j=0; j<3; j++)
		{
			md_K[cnt] = m_K.at<double>(i,j);
			md_Kt[cnt] = m_K.at<double>(j,i);
			md_Kinv[cnt] = m_Kinv.at<double>(i,j);
			md_Ktinv[cnt] = m_Ktinv.at<double>(i,j);
			cnt++;
		}
	}
	
}


CVPDetector::~CVPDetector(void)
{
	
}

void CVPDetector::setK(Mat K)
{
    m_K = K.clone();
    invert(m_K, m_Kinv);
	invert(m_K.t(), m_Ktinv);
    
    m_K_img1 = m_K.clone();
    m_K_img2 = m_K.clone();
    m_Kinv_img1 = m_Kinv.clone();
    m_Kinv_img2 = m_Kinv.clone();
    m_Ktinv_img1 = m_Ktinv.clone();
    m_Ktinv_img2 = m_Ktinv.clone();
}

// read R_b_a (b means camera body, a means arbitrary axis whose z axis along gravity vector
Mat CVPDetector::getR_c_wFromFile(string f_R_b_a)
{
    FILE *fp = fopen(f_R_b_a.data(), "r");
    float R[9];
    fscanf(fp, "%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n", &R[0], &R[1], &R[2], &R[3], &R[4], &R[5], &R[6], &R[7], &R[8]);
    fclose(fp);
    
    Mat R_b_a = (Mat_<double>(3,3) << R[0], R[1], R[2], R[3], R[4], R[5], R[6], R[7], R[8]);
    Mat R_c_b = (Mat_<double>(3,3) << 0, -1, 0, -1, 0, 0, 0, 0, -1);
    Mat R_c_w = R_c_b * R_b_a;  // initial value for R_c_w
    
    return R_c_w;
}

/*----------------------------------------------------------------------------*/
/** lineDetectionHP: Line detection function using cv::HoughLinesP
 
 @param img				Color image
 @param max_num_lines	maximum number of lines
 @param lines			lines = [s_x1, s_y1, e_x1, e_y1, ..., ].
 lines must be created before use this function by using
 lines = new double[max_num_lines*4];.
 @param num_lines		store the number of detected lines.
 @param scale            resize image
 
 */
void CVPDetector::lineDetectionHP(Mat img, int max_num_lines, double* lines, int &num_lines, double min_line_length, double max_line_gap, double scale)
{
	Mat small_img, imgCanny;
	int houghThreshold;
	vector<Vec4i> lines_vec;
	int cnt;
	double length;
	vector<double> length_;
	
    
    if(scale != 1.f)
    {
        resize(img, small_img, Size(img.cols*scale, img.rows*scale));
        Canny(small_img, imgCanny, 50, 150);        
    }
    else
        Canny(img, imgCanny, 50, 150);

	houghThreshold = 30;
	HoughLinesP(imgCanny, lines_vec, 1, CV_PI/180.0, houghThreshold, min_line_length, max_line_gap);
	
	if(lines_vec.size() > max_num_lines)
	{
		
		for(int i=0; i<lines_vec.size(); i++)
		{
			length = sqrt( double(lines_vec[i][0]-lines_vec[i][2])*double(lines_vec[i][0]-lines_vec[i][2]) 
				+ double(lines_vec[i][1]-lines_vec[i][3])*double(lines_vec[i][1]-lines_vec[i][3]) );
			length_.push_back(length);
		}
		Mat sorted_idx;
		cv::sortIdx(length_, sorted_idx, CV_SORT_DESCENDING);

		num_lines = max_num_lines;

		cnt = 0;
		for(int i=0; i<num_lines; i++)
		{		
			int idx = sorted_idx.at<int>(i);
			if(length_[idx] < min_line_length)
				break;
						
			lines[cnt*4    ] = double(lines_vec[idx][0])/scale;
			lines[cnt*4 + 1] = double(lines_vec[idx][1])/scale;
			lines[cnt*4 + 2] = double(lines_vec[idx][2])/scale;
			lines[cnt*4 + 3] = double(lines_vec[idx][3])/scale;   
			cnt++;
		}
		num_lines = cnt;
	}
	else
	{
		cnt = 0;
		for(int i=0; i<lines_vec.size(); i++)
		{		
			length = sqrt( double(lines_vec[i][0]-lines_vec[i][2])*double(lines_vec[i][0]-lines_vec[i][2]) 
				+ double(lines_vec[i][1]-lines_vec[i][3])*double(lines_vec[i][1]-lines_vec[i][3]) );

			if(length < min_line_length)
				continue;

			lines[cnt*4    ] = double(lines_vec[i][0])/scale;
			lines[cnt*4 + 1] = double(lines_vec[i][1])/scale;
			lines[cnt*4 + 2] = double(lines_vec[i][2])/scale;
			lines[cnt*4 + 3] = double(lines_vec[i][3])/scale;   
			cnt++;
		}
		num_lines = cnt;
	}	
}

//shot_th: do not strore lines whose length is less than short_th
void CVPDetector::lineDetectionLSD(double *image, int rows, int cols, 
							int max_num_lines, double* lines, int &num_lines, double short_th)
{
    double *out;
    int cnt;
	double length;
    double short_th2 = short_th * short_th;
	
	out = lsd_scale(&num_lines, image, cols, rows, 0.8f);

	// line merging
	out = lineMerging(out, 7, num_lines, 4*CV_PI/180.f, 3, 30);
	
	if(num_lines > max_num_lines)
	{
		vector<double> length_vec;
		for(int i=0; i<num_lines; i++)
		{
			length = ( (out[i*4  ]-out[i*4+2])*(out[i*4  ]-out[i*4+2]) +
						   (out[i*4+1]-out[i*4+3])*(out[i*4+1]-out[i*4+3]));
			length_vec.push_back(length);
		}
		Mat sorted_idx;
		cv::sortIdx(length_vec, sorted_idx, CV_SORT_DESCENDING);

		num_lines = max_num_lines;

		cnt = 0;
		for(int i=0; i<num_lines; i++)
		{		
			int idx = sorted_idx.at<int>(i);
			if(length_vec[idx] < short_th2)
				break;

			lines[cnt*4    ] = out[idx*4  ];
			lines[cnt*4 + 1] = out[idx*4+1];
			lines[cnt*4 + 2] = out[idx*4+2];
			lines[cnt*4 + 3] = out[idx*4+3];
			cnt++;
		}
		num_lines = cnt;
	}
	else
	{
		cnt = 0;
		for(int i=0; i<num_lines; i++)
		{		
			length = ( (out[i*4  ]-out[i*4+2])*(out[i*4  ]-out[i*4+2]) +
						   (out[i*4+1]-out[i*4+3])*(out[i*4+1]-out[i*4+3]));

			if(length < short_th2)
				continue;

			lines[cnt*4    ] = out[i*4  ];
			lines[cnt*4 + 1] = out[i*4+1];
			lines[cnt*4 + 2] = out[i*4+2];
			lines[cnt*4 + 3] = out[i*4+3];
			cnt++;
		}
		num_lines = cnt;
	}

    delete[] out;	
}

double *CVPDetector::lineLengthFilter(double *lines, int &num_lines, double th_length)
{
    int cnt = 0;
    double *new_lines = new double[4*num_lines];
    for(int i=0; i<num_lines; i++){
        double length = sqrt( (lines[4*i]-lines[4*i+2])*(lines[4*i]-lines[4*i+2]) + (lines[4*i+1]-lines[4*i+3])*(lines[4*i+1]-lines[4*i+3]) );
        if(length > th_length){
            memcpy(&new_lines[4*cnt], &lines[4*i], sizeof(double)*4);
            cnt++;
        }
    }
    
    num_lines = cnt;
    delete[] lines;
    return new_lines;
}

// return: -1: overlapped
//         >= 0: distance between cloest end points
// s, e:	if they are merged, the result will be a line joining s, e
double CVPDetector::areTheyOverlapped(double line1[4], double line2[4], double s[2], double e[2])
{
	double s1e1[2], s1s2[2], s1e2[2], e1s2[2], e1e2[2];
	double tolerance = 0.00001;
	s1e1[0] = line1[2] - line1[0];
	s1e1[1] = line1[3] - line1[1];
	unitVector(s1e1, s1e1, 2);
	
	s1s2[0] = line2[0] - line1[0];
	s1s2[1] = line2[1] - line1[1];
	unitVector(s1s2, s1s2, 2);

	s1e2[0] = line2[2] - line1[0];
	s1e2[1] = line2[3] - line1[1];
	unitVector(s1e2, s1e2, 2);

	e1s2[0] = line2[0] - line1[2];
	e1s2[1] = line2[1] - line1[3];
	unitVector(e1s2, e1s2, 2);

	e1e2[0] = line2[2] - line1[2];
	e1e2[1] = line2[3] - line1[3];
	unitVector(e1e2, e1e2, 2);
		

	double innerprod1, innerprod2;
	innerprod1 = s1e1[0]*s1s2[0] + s1e1[1]*s1s2[1];
	innerprod2 = s1e1[0]*s1e2[0] + s1e1[1]*s1e2[1];
	if( innerprod1 < tolerance && innerprod2 < tolerance)
	{
		if(innerprod1 > innerprod2)
		{
			// s1~s2
			s[0] = line2[2];
			s[1] = line2[3];
			e[0] = line1[2];
			e[1] = line1[3];
			return sqrt( (line1[0]-line2[0])*(line1[0]-line2[0]) + (line1[1]-line2[1])*(line1[1]-line2[1]) );
		}
		else
		{
			// s1~e2
			s[0] = line2[0];
			s[1] = line2[1];
			e[0] = line1[2];
			e[1] = line1[3];
			return sqrt( (line1[0]-line2[2])*(line1[0]-line2[2]) + (line1[1]-line2[3])*(line1[1]-line2[3]) );
		}
	}
	else if(innerprod1*innerprod2 <= 0)
		return -1;
	else
	{
		innerprod1 = -s1e1[0]*e1s2[0] - s1e1[1]*e1s2[1];
		innerprod2 = -s1e1[0]*e1e2[0] - s1e1[1]*e1e2[1];
		if( innerprod1 < tolerance && innerprod2 < tolerance)
		{
			if(innerprod1 > innerprod2)
			{
				// e1~s2
				s[0] = line1[0];
				s[1] = line1[1];
				e[0] = line2[2];
				e[1] = line2[3];
				return sqrt( (line1[2]-line2[0])*(line1[2]-line2[0]) + (line1[3]-line2[1])*(line1[3]-line2[1]) );
			}
			else
			{
				// e1~e2
				s[0] = line1[0];
				s[1] = line1[1];
				e[0] = line2[0];
				e[1] = line2[1];
				return sqrt( (line1[2]-line2[2])*(line1[2]-line2[2]) + (line1[3]-line2[3])*(line1[3]-line2[3]) );	
			}
		}
		else
			return -1;
	}
}
bool CVPDetector::areTheyOverlapped(LineFeature *line1, LineFeature *line2)
{
	double s1e1[2], s1s2[2], s1e2[2], e1s2[2], e1e2[2];

	s1e1[0] = line1->e[0] - line1->s[0];
	s1e1[1] = line1->e[1] - line1->s[1];
	
	s1s2[0] = line2->s[0] - line1->s[0];
	s1s2[1] = line2->s[1] - line1->s[1];

	s1e2[0] = line2->e[0] - line1->s[0];
	s1e2[1] = line2->e[1] - line1->s[1];

	e1s2[0] = line2->s[0] - line1->e[0];
	e1s2[1] = line2->s[1] - line1->e[1];

	e1e2[0] = line2->e[0] - line1->e[0];
	e1e2[1] = line2->e[1] - line1->e[1];

	double innerprod1, innerprod2;
	innerprod1 = s1e1[0]*s1s2[0] + s1e1[1]*s1s2[1];
	innerprod2 = s1e1[0]*s1e2[0] + s1e1[1]*s1e2[1];
	if( innerprod1 < 0 && innerprod2 < 0)
		return false;
	else if(innerprod1*innerprod2 <= 0)
		return true;
	else
	{
		innerprod1 = -s1e1[0]*e1s2[0] - s1e1[1]*e1s2[1];
		innerprod2 = -s1e1[0]*e1e2[0] - s1e1[1]*e1e2[1];
		if( innerprod1 < 0 && innerprod2 < 0)
			return false;
		else
			return true;
	}
}

// line merging using the algorithm in Paper 1995 JMRS Manuel A New Approach for Merging Edge Line Segments
bool CVPDetector::lineMerging(double s1[2], double e1[2], double s2[2], double e2[2], double *new_s, double *new_e, double th_gap)
{
    // Define the coordinates of the centroid
    double l1[2] = {e1[0] - s1[0], e1[1] - s1[1]};
    double l2[2] = {e2[0] - s2[0], e2[1] - s2[1]};
    
    
    double len1 = sqrt( l1[0]*l1[0] + l1[1]*l1[1] );
    double len2 = sqrt( l2[0]*l2[0] + l2[1]*l2[1] );
    
    double g[2];
    double w1 = len1 / (len1 + len2);
    double w2 = len2 / (len1 + len2);
    g[0] = w1*(s1[0] + e1[0]) + w2*(s2[0] + e2[0]);     g[0] *= 0.5;
    g[1] = w1*(s1[1] + e1[1]) + w2*(s2[1] + e2[1]);     g[1] *= 0.5;
    
    // Define the orientation of the merged line as the weighted sum of the orientations of the given segments
    if( m_util.innerProd(l1, l2, 2) < 0){
        l2[0] *= -1;
        l2[1] *= -1;
    }
    double rad = w1*atan2(l1[1], l1[0]) + w2*atan2(l2[1], l2[0]);
    double v[2] = {cos(rad), sin(rad)};
    
    vector<double> p;
    p.push_back( (s1[0]-g[0])*v[0] + (s1[1]-g[1])*v[1] );
    p.push_back( (e1[0]-g[0])*v[0] + (e1[1]-g[1])*v[1] );
    p.push_back( (s2[0]-g[0])*v[0] + (s2[1]-g[1])*v[1] );
    p.push_back( (e2[0]-g[0])*v[0] + (e2[1]-g[1])*v[1] );
    
    vector<int> idx;
    sortIdx(p, idx, CV_SORT_ASCENDING);
    double *addr[4] = {s1, e1, s2, e2};
    
    memcpy(new_s, addr[idx[0]], sizeof(double)*2);
    memcpy(new_e, addr[idx[3]], sizeof(double)*2);
    
    if( (p[idx[3]] - p[idx[0]]) - (len1 + len2) > th_gap)       // if two line segs are not overlapped each other, then return false
        return false;
    else
        return true;
}


// size: size of each element in double *lines, size must be 7 for LSD. 
// However, the size of the returned merged line is 4!
// th_angle: in radian
double* CVPDetector::lineMerging(double *lines, int size, int &num_lines, double th_angle, double th_dist, double th_gap)
{
	int num_merge;
	double new_s[2] = {0,0}, new_e[2] = {0,0};
	int *idx_del;

	idx_del = new int[num_lines];
	memset(idx_del, 0, sizeof(int)*num_lines);
    
    // compute line coefficients
    double *line_coef = new double[num_lines*3];
    for(int i=0; i<num_lines; i++){
        double s[3] = {lines[i*size], lines[i*size+1], 1};
        double e[3] = {lines[i*size+2], lines[i*size+3], 1};
        m_util.crossProd(s, e, &line_coef[i*3]);
    }
	while(1)
	{
		num_merge = 0;
		for(int i=0; i<num_lines; i++)
		{
            if(idx_del[i] == 1)
                continue;
			for(int j=0; j<num_lines; j++)
			{
                if(i==j)
                    continue;
                
				// check if j-th line is already merged 
				if(idx_del[j] == 1)
					continue;
                
                double dist;
                dist  = m_util.distPoint2Line(&line_coef[3*i], &lines[j*size]);
                dist += m_util.distPoint2Line(&line_coef[3*i], &lines[j*size+2]);
                dist += m_util.distPoint2Line(&line_coef[3*j], &lines[i*size]);
                dist += m_util.distPoint2Line(&line_coef[3*j], &lines[i*size+2]);
                dist *= 0.25;
                
                double rad; // angle in radian
                double line_norm1[2] = {line_coef[3*i], line_coef[3*i+1]};
                double line_norm2[2] = {line_coef[3*j], line_coef[3*j+1]};
                m_util.unitVector(line_norm1, line_norm1, 2);
                m_util.unitVector(line_norm2, line_norm2, 2);
                double ip =m_util.innerProd(line_norm1, line_norm2, 2);
                rad = acos(fabs(ip)); // acos(fabs(x)) returns [0, pi/2]
                
                if(rad < th_angle && dist < th_dist){
                    if(lineMerging(&lines[i*size], &lines[i*size+2], &lines[j*size], &lines[j*size+2], new_s, new_e, th_gap)){
                        lines[size*i  ] = new_s[0];
                        lines[size*i+1] = new_s[1];
                        lines[size*i+2] = new_e[0];
                        lines[size*i+3] = new_e[1];
                        num_merge++;
                        
                        idx_del[j] = 1;
                    }
                }
            
			}
		}
		if(num_merge == 0)
			break;
	}

	// create new line data
	// count new num_lines;
	int new_num_lines = 0;
	for(int i=0; i<num_lines; i++)
	{
		if(idx_del[i] != 1)
			new_num_lines++;
	}
	double *new_lines = new double[4*new_num_lines];
	new_num_lines = 0;
	for(int i=0; i<num_lines; i++)
	{
		if(idx_del[i] != 1)
		{
			memcpy(&new_lines[4*new_num_lines], &lines[size*i], sizeof(double)*4);
			new_num_lines++;
		}
	}
	delete[] lines;
	delete[] idx_del;

	num_lines = new_num_lines;
	return new_lines;
}

void CVPDetector::lineMerging(LineFeature *line_features, int num_lines, int &num_new_lines, double th)
{
	int *label_del, *idx_sel;
	label_del = new int[num_lines];
	idx_sel = new int[num_lines];
	int num_sel;

	memset(label_del, 0, sizeof(int)*num_lines);	
	for(int i=0; i<num_lines; i++)
	{
		if(label_del[i] == 1)
			continue;
		num_sel = 0;		// select close lines
		for(int j=i+1; j<num_lines; j++)
		{			
			if( fabs(line_features[i].line_norm[0]*line_features[j].line_norm[0] + 
				line_features[i].line_norm[1]*line_features[j].line_norm[1] +
				line_features[i].line_norm[2]*line_features[j].line_norm[2]) > th && label_del[j] != 1
				&& !areTheyOverlapped(&line_features[i], &line_features[j]))
			{
				idx_sel[num_sel] = j;
				num_sel++;				
			}			
		}

		if(num_sel)
		{
			idx_sel[num_sel] = i;
			num_sel++;

			// merging
			//lineMerging(line_features, idx_sel, num_sel, i);



			for(int k=0; k<num_sel-1; k++)
				label_del[idx_sel[k]] = 1;
		}			
	}
	
	// set .bUse as false 
	for(int i=0; i<num_lines; i++)
	{
		if(label_del[i])
			line_features[i].bUse = false;
	}	

	delete[] label_del;
	delete[] idx_sel;
}

// merge lines with index in idx_sel_lines. 
// The merged line is copied to line_features[idx_store]
void CVPDetector::lineMerging(LineFeature *line_features, int *idx_sel_lines, int num_sel_lines, int idx_store, double th)
{

	int idx, idx_max;
	double max_length;

	max_length = 0;
	idx_max = idx_store;
	for(int i=0; i<num_sel_lines; i++)
	{
		idx = idx_sel_lines[i];
		if(max_length < line_features[idx].length)
		{
			max_length = line_features[idx].length;
			idx_max = idx;
		}
	}

	// copy data
	if(idx_max != idx_store)
	{
		// delete data
		line_features[idx_store].clear();
		// copy 
		memcpy(&line_features[idx_store], &line_features[idx_max], sizeof(LineFeature));

		// copy MSLD data
		if(line_features[idx_max].MSLD)
		{
			line_features[idx_store].MSLD = new double[line_features[idx_max].len_desc];
			memcpy(line_features[idx_store].MSLD, line_features[idx_max].MSLD, 
													sizeof(double)*line_features[idx_max].len_desc);
		}		
	}
	

}



// e_th = 10.f/180.f*CV_PI;
vector<LineCluster> CVPDetector::findVP(LineFeature* line_features, int num_lines, double x_imu[3], double y_imu[3], double z_imu[3],
	double e_th, int num_trials, Mat &K, bool bCorrection, bool bLM)
{
	LineCluster g_v, g_h;
    LineCluster g_h1, g_h2;
	
	int rand_idx;	
	RNG rng(12345);
	double best_vd_h1[3], best_vd_h2[3], vd_x[3], vd_y[3];
    
	double s_h1, s_h2, best_s;
    vector<LineCluster> line_clusters;
	
	
	// rough separation based on the prior vertical vanishing point, vd_prior
	g_v.init(num_lines);
	g_h.init(num_lines);
    
	for(int i=0; i<num_lines; i++)
	{
		double e = distance(line_features[i], z_imu);
        
		if(e < e_th)
		{
			g_v.lines[g_v.num_lines] = &line_features[i];	
			g_v.num_lines++;
		}
		else
		{
			g_h.lines[g_h.num_lines] = &line_features[i];	
			g_h.num_lines++;
		}
	}

	memcpy(g_v.vd, z_imu, sizeof(double)*3);
    
    // if there are not sufficient lines, it is possible that there are no horizontal lines.
    // In this case, randomly generate two horizontal vanishign directions and return them with empty two line groups.
    if(g_h.num_lines == 0)
    {
        g_h1.init(num_lines);
        g_h2.init(num_lines);
        
		orthonormalVector(g_v.vd, g_h1.vd);
        crossProd(g_v.vd, g_h1.vd, g_h2.vd);
        
        line_clusters.push_back(g_v);
        line_clusters.push_back(g_h1);
        line_clusters.push_back(g_h2);
        
        g_h.clear();
        
        return line_clusters;
    }
        

	best_s = 0;
	for(int i=0; i<num_trials; i++)
	{
		double vd_h1[3], vd_h2[3];
        
		// randomly select a lines in g_h
		rand_idx = rng.uniform(0, g_h.num_lines);

		// generate two orthogonal vanishing directions that orthogonal to vertical vd.
        // if the line normal vector and the vd_v are coincide each other, vd_h1 will be zero!!
        //if(fabs(innerProd(g_h.lines[rand_idx]->line_norm, vd_v, 3)) == 1.f)
        //    continue;
        
		crossProd(g_h.lines[rand_idx]->line_norm, z_imu, vd_h1);
		unitVector(vd_h1, vd_h1, 3);				

		crossProd(z_imu, vd_h1, vd_h2);
		unitVector(vd_h2, vd_h2, 3);
        
        s_h1 = score(g_h, vd_h1, e_th);
		s_h2 = score(g_h, vd_h2, e_th);

		if(best_s < s_h1+s_h2)
		{
			best_s = s_h1+s_h2;
			memcpy(best_vd_h1, vd_h1, sizeof(double)*3);
			memcpy(best_vd_h2, vd_h2, sizeof(double)*3);
		}
	}
	
	double ip1 = innerProd(best_vd_h1, x_imu, 3);
	double ip2 = innerProd(best_vd_h2, x_imu, 3);
	if(fabs(ip1) > fabs(ip2))
	{
		if(ip1 < 0) // if( best_vd_h1[1] < 0)
		{
			best_vd_h1[0] *= -1;
			best_vd_h1[1] *= -1;
			best_vd_h1[2] *= -1;				
		}
		// following right-hand law
		crossProd(z_imu, best_vd_h1, best_vd_h2);
		unitVector(best_vd_h2, best_vd_h2, 3);
		
		memcpy(vd_x, best_vd_h1, sizeof(double)*3);
		memcpy(vd_y, best_vd_h2, sizeof(double)*3);	
	}
	else
	{
		if(ip2 < 0) //if( best_vd_h2[1] < 0)
		{
			best_vd_h2[0] *= -1;
			best_vd_h2[1] *= -1;
			best_vd_h2[2] *= -1;				
		}
		// following right-hand law
		crossProd(z_imu, best_vd_h2, best_vd_h1);
		unitVector(best_vd_h1, best_vd_h1, 3);

		memcpy(vd_x, best_vd_h2, sizeof(double)*3);
		memcpy(vd_y, best_vd_h1, sizeof(double)*3);	
	}
	
	
    g_h1.init(num_lines);
	g_h2.init(num_lines);

	double e_x, e_y;
	for(int i=0; i<g_h.num_lines; i++)
	{
        e_x = distance(*g_h.lines[i], vd_x);
        e_y = distance(*g_h.lines[i], vd_y);
		
		if(e_x < e_th && e_y < e_th)
		{
			if(e_x < e_y)
			{
				g_h1.lines[g_h1.num_lines] = g_h.lines[i];
				g_h1.num_lines++;
			}
			else
			{
				g_h2.lines[g_h2.num_lines] = g_h.lines[i];
				g_h2.num_lines++;	
			}
		}
		else if(e_x < e_th)
		{
			g_h1.lines[g_h1.num_lines] = g_h.lines[i];
			g_h1.num_lines++;
		}
		else if(e_y < e_th)
		{
			g_h2.lines[g_h2.num_lines] = g_h.lines[i];
			g_h2.num_lines++;
		}	
	}


	memcpy(g_h1.vd, vd_x, sizeof(double)*3);
	memcpy(g_h2.vd, vd_y, sizeof(double)*3);
    
    K = m_K.clone();
    if(bLM)
    {
//        Mat R_c_w = (Mat_<double>(3,3) << g_h1.vd[0], g_h2.vd[0], g_v.vd[0],
//                     g_h1.vd[1], g_h2.vd[1], g_v.vd[1],
//                     g_h1.vd[2], g_h2.vd[2], g_v.vd[2]);
//        
//        
//        //R_c_w = refine(R_c_w, g_h1, g_h2, g_v);
//        Mat R_c_w_new;
//        refine(R_c_w, m_K, g_h1, g_h2, g_v, R_c_w_new, K);
//        
//        g_h1.vd[0] = R_c_w_new.at<double>(0,0);
//        g_h1.vd[1] = R_c_w_new.at<double>(1,0);
//        g_h1.vd[2] = R_c_w_new.at<double>(2,0);
//        
//        g_h2.vd[0] = R_c_w_new.at<double>(0,1);
//        g_h2.vd[1] = R_c_w_new.at<double>(1,1);
//        g_h2.vd[2] = R_c_w_new.at<double>(2,1);
//        
//        g_v.vd[0] = R_c_w_new.at<double>(0,2);
//        g_v.vd[1] = R_c_w_new.at<double>(1,2);
//        g_v.vd[2] = R_c_w_new.at<double>(2,2);
//        m_K = K.clone();
    }
    
    
    Mat Kinv_mat = K.inv();
    double Kinv[9] = {Kinv_mat.at<double>(0,0), Kinv_mat.at<double>(0,1), Kinv_mat.at<double>(0,2),
        Kinv_mat.at<double>(1,0), Kinv_mat.at<double>(1,1), Kinv_mat.at<double>(1,2),
        Kinv_mat.at<double>(2,0), Kinv_mat.at<double>(2,1), Kinv_mat.at<double>(2,2)};
    
    // line correction (align the lines to intersect at the vanishing point while their mid points are preserved.
	lineCorrection(g_v, Kinv, bCorrection);
	lineCorrection(g_h1, Kinv, bCorrection);
    lineCorrection(g_h2, Kinv, bCorrection);
  
	// sorting lines
	sortLines(g_v, Kinv, g_h2.vd);
	//sortLines(g_v, Kinv, g_h1.vd);
	sortLines(g_h1, Kinv, g_h2.vd);
	sortLines(g_h2, Kinv, g_h1.vd);

    aboveBelowHorizon(g_h1, g_v.vd, Kinv_mat.t());
	aboveBelowHorizon(g_h2, g_v.vd, Kinv_mat.t());

	// compute vanishing points
	mul(K, g_v.vd, g_v.vp);
	g_v.vp[0] /= g_v.vp[2];
	g_v.vp[1] /= g_v.vp[2];
	g_v.vp[2]  = 1.0;

	mul(K, g_h1.vd, g_h1.vp);
	g_h1.vp[0] /= g_h1.vp[2];
	g_h1.vp[1] /= g_h1.vp[2];
	g_h1.vp[2]  = 1.0;

	mul(K, g_h2.vd, g_h2.vp);
	g_h2.vp[0] /= g_h2.vp[2];
	g_h2.vp[1] /= g_h2.vp[2];
	g_h2.vp[2]  = 1.0;
		
	line_clusters.push_back(g_h1);
	line_clusters.push_back(g_h2);
	line_clusters.push_back(g_v);


	g_h.clear();

	return line_clusters;
}



double CVPDetector::score(LineCluster line_cluster, double vd[3], double e_th)
{
	double s = 0;

	if(line_cluster.length_max > 0)
	{
		for(int i=0; i<line_cluster.num_lines; i++)
		{
			double e = distance(*(line_cluster.lines[i]), vd);
			if(e < e_th)
				s += line_cluster.lines[i]->length / line_cluster.length_max * (1 - e/e_th);
		}
	}
	else
	{
		for(int i=0; i<line_cluster.num_lines; i++)
		{
			double e = distance(*(line_cluster.lines[i]), vd);
			if(e < e_th)
				s += line_cluster.lines[i]->length * (1 - e/e_th);
		}
	}
	return s;
}

double CVPDetector::score(LineFeature** line_features, int num_lines, double vd[3], double e_th)
{
	double s = 0; 
	for(int i=0; i<num_lines; i++)
	{
		double e = distance(*line_features[i], vd);
		if(e < e_th)
			s += line_features[i]->length*(1 - e/e_th);
	}
	return s;
}

double CVPDetector::score(LineFeature* line_features, int num_lines, double vd[3], double e_th)
{
	double s = 0; 
	for(int i=0; i<num_lines; i++)
	{
		double e = distance(line_features[i], vd);
		if(e < e_th)
			s += line_features[i].length*(1 - e/e_th);
	}
	return s;
}





/*----------------------------------------------------------------------------*/
/** alignError: deviation angle between the detected line
 and a line joining its midpoint and the vanishing point
 
 @param line				line feature
 @param vd				vanishing direction, 3x1 vector
 */
double CVPDetector::alignError(LineFeature line, double *vd)
{
	double vp[3], a[2], b[2], e;

	// line a: s ~ e
	a[0] = line.s[0] - line.e[0];
	a[1] = line.s[1] - line.e[1];
	unitVector(a, a, 2);

	// line b: midpoint ~ vp
    if(projection(vd, vp, m_K))
    {
        b[0] = line.m[0] - vp[0];
		b[1] = line.m[1] - vp[1];
        
    }
    else
    {
        b[0] = vd[0];
        b[1] = vd[1];
    }
    
	unitVector(b, b, 2);

	e = fabs(a[0]*b[0] + a[1]*b[1]);	// [0~1]
	if(e>1)
		e = 1;

	return e;
}

// compute distance of end points of the line segment to the line joining the mid point of the line segment and the vanishing point
double CVPDetector::alignErrorEndPts(LineFeature line, double *vd)
{
    double vp[3], m[3], p[3], l[3], d;
    bool check_infinite;
    
	check_infinite = projection(vd, vp, m_K);
    
    // compute a line joining the mid pt and vp
    m[0] = line.m[0];
    m[1] = line.m[1];
    m[2] = 1;
    
    if(check_infinite)
    {
        p[0] = vp[0];
        p[1] = vp[1];
        p[2] = 1;
    }
    else
    {
        p[0] = m[0] + vd[0];
        p[1] = m[1] + vd[1];
        p[2] = 1;
    }
        
    crossProd(m, p, l);
    
    // compute distance of end points to the line
    normalizeLine(l);
    d = fabs(line.s[0]*l[0] + line.s[1]*l[1] + l[2]);     // sqrt(l[0]^2 + l[1]^2) is equal to one because of normalizeLine(l);
    d += fabs(line.e[0]*l[0] + line.e[1]*l[1] + l[2]);
    d *= 0.5;
        
    
    return d;
}

// compute cosine of angle between vd and line normal vector
// they are supposed to be orthogonal each other.
double CVPDetector::alignErrorLineNormal(LineFeature line, double *vd)
{
    double a[3];
    
    memcpy(a, line.line_norm, sizeof(double)*3);
    unitVector(a, a, 3);
    
    return fabs(innerProd(a, vd, 3));
}

/*----------------------------------------------------------------------------*/
/** distance: diatance measure between vanishing direction and line feature
 vd and line.line_norm must be unit vector.
 
 @param line				line feature
 @param vd				vanishing direction, 3x1 vector
 */
double CVPDetector::distance(LineFeature line, double *vd)
{
    double e = alignError(line, vd);
	return acos(e);
}

/*----------------------------------------------------------------------------*/
/** getAngle:
 
 @param a
 @param b
 @param dim
 */
double CVPDetector::getLineAngle(double *x1, double *x2)
{
	double line[2], length;
	
	line[0] = x1[0] - x2[0];
	line[1] = x1[1] - x2[1];

	length = sqrt(line[0]*line[0] + line[1]*line[1]);

	line[0] /= length;
	if(line[0] < -1)
		line[0] = -1.0f;
	else if(line[0] > 1)
		line[0] = 1.0f;

	return acos(line[0]); // [0~PI]
}

/*----------------------------------------------------------------------------*/
/** backProjection: direction = K^{-1}[point; 1]
 
 @param point			2x1 vector
 @param direction		3x1 vector
 */
void CVPDetector::backProjection(double *point, double *direction)
{
	Mat p, d;

	p = (Mat_<double>(3, 1) << point[0], point[1], 1);
	d = m_Kinv*p;

	direction[0] = d.at<double>(0) / d.at<double>(2);
	direction[1] = d.at<double>(1) / d.at<double>(2);
	direction[2] = 1.0f;
}

/*----------------------------------------------------------------------------*/
/** projection: point = K direction
 
 @param point			2x1 vector
 @param direction		3x1 vector
 return:					true:point in finte, false:point in infinte
 */
bool CVPDetector::projection(double *direction, double *point, Mat K)
{
	Mat p, d;

	d = (Mat_<double>(3, 1) << direction[0], direction[1], direction[2]);
	p = K*d;

	//double pp[3] = {p.at<double>(0), p.at<double>(1), p.at<double>(2)};

	if(fabs(p.at<double>(2)) > INFINITE_TEST_MIN)
	{
		point[0] = p.at<double>(0) / p.at<double>(2);
		point[1] = p.at<double>(1) / p.at<double>(2);
		return true;
	}
	else
	{
		point[0] = direction[0];
		point[1] = direction[1];
		return false;

	}		
}

void CVPDetector::normalizeLine(double *line)
{
	double len = sqrt(line[0]*line[0] +	line[1]*line[1]);
	line[0] /= len;
	line[1] /= len;
	line[2] /= len;
	if(line[2] < 0)
	{
		line[0] *= -1;
		line[1] *= -1;
		line[2] *= -1;
	}		
}

/*----------------------------------------------------------------------------*/
/** lineFeatureComputation: compute line features
 
 @param lines			lines = [s_x1, s_y1, e_x1, e_y1, ..., ].
 @param num_lines		the number of lines.
 @param line_features	line feature array
 */
void CVPDetector::lineFeatureComputation(double* lines, int &num_lines, LineFeature *line_features,
										double *image, int rows, int cols)
{	
	int S, N, W;
	double th;
    int cnt;
    int th_margin = 3; // ignore the line segment near the image boundary

	// S (5):			sampling size of points on a line segment
	// N (9):			num_sub_regions
	// W (5):			5x5 size of sub-region
	// th (0.4):		threshold for restraining maximum value in the unit vector
	S = 10;  // 10;         // ACCV2014 S, N, W => (10, 3, 5)
	N = 3;	// 3;
	W = 5;	// 5;
	th = 0.4f;

    cnt = 0;
	for(int i=0; i<num_lines; i++)
	{	
		// s, e, m
		line_features[cnt].s[0] = lines[i*4  ];
		line_features[cnt].s[1] = lines[i*4+1];
		line_features[cnt].e[0] = lines[i*4+2];
		line_features[cnt].e[1] = lines[i*4+3];
		line_features[cnt].m[0] = (line_features[cnt].s[0] + line_features[cnt].e[0])*0.5;
		line_features[cnt].m[1] = (line_features[cnt].s[1] + line_features[cnt].e[1])*0.5;
        
        if(line_features[cnt].m[0] < th_margin || line_features[cnt].m[0] > cols - th_margin ||
           line_features[cnt].m[1] < th_margin || line_features[cnt].m[1] > rows - th_margin)
            continue;

		// angle
	//	line_features[cnt].angle = getLineAngle(line_features[cnt].s, line_features[cnt].e);
		
		// line length
		line_features[cnt].length =
			sqrt( (line_features[cnt].s[0]-line_features[cnt].e[0])*(line_features[cnt].s[0]-line_features[cnt].e[0]) +
				  (line_features[cnt].s[1]-line_features[cnt].e[1])*(line_features[cnt].s[1]-line_features[cnt].e[1]) );

//        if(0)
//		{
//            // undistorte line coordinates.
//            undistortCoord(&lines[4*i], &lines[4*i]);
//            undistortCoord(&lines[4*i+2], &lines[4*i+2]);
//            
//            // update line->s, e, m, length
//            // s, e, m
//            line_features[cnt].s[0] = lines[i*4  ];
//            line_features[cnt].s[1] = lines[i*4+1];
//            line_features[cnt].e[0] = lines[i*4+2];
//            line_features[cnt].e[1] = lines[i*4+3];
//            line_features[cnt].m[0] = (line_features[cnt].s[0] + line_features[cnt].e[0])*0.5;
//            line_features[cnt].m[1] = (line_features[cnt].s[1] + line_features[cnt].e[1])*0.5;
//            
//            // angle
//            //line_features[cnt].angle = getLineAngle(line_features[cnt].s, line_features[cnt].e);
//            
//            // line length
//            line_features[cnt].length =
//			sqrt( (line_features[cnt].s[0]-line_features[cnt].e[0])*(line_features[cnt].s[0]-line_features[cnt].e[0]) +
//                 (line_features[cnt].s[1]-line_features[cnt].e[1])*(line_features[cnt].s[1]-line_features[cnt].e[1]) );
//		}
        
		// MSLD	(line->s, e, m, length are used)	
		line_features[cnt].MSLD = MSLD(&line_features[cnt], image, rows, cols, line_features[cnt].ln, S, N, W, th);
		line_features[cnt].len_desc = 8*N;
   
		

		// line in homogeneous
		double a[3], b[3];
		a[0] = line_features[cnt].s[0];
		a[1] = line_features[cnt].s[1];
		a[2] = 1.0f;

		b[0] = line_features[cnt].e[0];
		b[1] = line_features[cnt].e[1];
		b[2] = 1.0f;
		
		crossProd(a, b, line_features[cnt].line);
		normalizeLine(line_features[cnt].line);
		
						
		// line normal vector		
		double n[3];
		mul(md_Kt, line_features[cnt].line, n, 3, 3);
		unitVector(n, n, 3);
		memcpy(line_features[cnt].line_norm, n, sizeof(double)*3);
		
		
        // id
		line_features[cnt].id = cnt;

		// side
		line_features[cnt].side = 0;

		// above or below the horizon
		line_features[cnt].above_below = 0;

		// bUse
		line_features[cnt].bUse = false;	// used for homography computation
        
        
        // keep original values before the line correction
        memcpy(line_features[cnt].line_orig, line_features[cnt].line, sizeof(double)*3);
        memcpy(line_features[cnt].line_norm_orig, line_features[cnt].line_norm, sizeof(double)*3);
        memcpy(line_features[cnt].s_orig, line_features[cnt].s, sizeof(double)*2);
        memcpy(line_features[cnt].e_orig, line_features[cnt].e, sizeof(double)*2);
        memcpy(line_features[cnt].m_orig, line_features[cnt].m, sizeof(double)*2);
        memcpy(line_features[cnt].line_norm_orig, line_features[cnt].line_norm, sizeof(double)*3);
        
        // 3d coordinates
        line_features[cnt].s3d[0] = 0;
        line_features[cnt].s3d[1] = 0;
        line_features[cnt].s3d[2] = 0;
        
        line_features[cnt].e3d[0] = 0;
        line_features[cnt].e3d[1] = 0;
        line_features[cnt].e3d[2] = 0;
        
        
        cnt++;
     
	}
    num_lines = cnt;
}


/*----------------------------------------------------------------------------*/
/** drawLines:
 
 @param windowname		window name
 @param img				Color image
 @param max_num_lines	maximum number of lines
 @param lines			lines = [s_x1, s_y1, e_x1, e_y1, ..., ].
 lines must be created before use this function by using
 lines = new double[max_num_lines*4];.
 @param num_lines		store the number of detected lines.
 
 */
void CVPDetector::drawLines(string windowname, Mat img, double *lines, int num_lines, Scalar color)
{
	for(int i=0; i<num_lines; i++)
		line(img, Point(int(lines[i*4]), int(lines[i*4+1])), Point(int(lines[i*4+2]), int(lines[i*4+3])), color, 1);

#ifndef IOS
	imshow(windowname, img);
	//waitKey(0);
	//destroyWindow(windowname);
#endif
}



void CVPDetector::drawLines(string windowname, Mat img, LineFeature **lines, int num_lines, Scalar color)
{
	for(int i=0; i<num_lines; i++)
	{
		line(img, Point(int(lines[i]->s[0]), int(lines[i]->s[1])), Point(int(lines[i]->e[0]), int(lines[i]->e[1])), color, 2);
	}

#ifndef IOS
	imshow(windowname, img);
	//waitKey(0);
	//destroyWindow(windowname);
#endif
}

void CVPDetector::drawLines(string windowname, Mat img, LineFeature *line, Scalar color, double thickness, bool draw_text)
{
	cv::line(img, Point(int(line->s[0]), int(line->s[1])), Point(int(line->e[0]), int(line->e[1])), color, thickness);
	Point s;
	if(draw_text)
	{
		s.x = line->s[0];
		s.y = line->s[1];
		char id[50];
		sprintf(id, "%d", line->id);	
		//sprintf(id, "%d", line->side);
		putText(img, id, s, FONT_HERSHEY_SIMPLEX, 0.4, color, thickness);			
	}

#ifndef IOS
	imshow(windowname, img);
	//waitKey(0);
	//destroyWindow(windowname);
#endif
}

void CVPDetector::drawLines(string windowname, Mat img, LineFeature *line, int width, int height, Scalar color, bool draw_text)
{
	Point s, e;
	getEndPoints(line->line, s, e, width, height);
	cv::line(img, s, e, color, 1);
	
	if(draw_text)
	{		
		char id[50];
		sprintf(id, "%d", line->id);	
		putText(img, id, 0.5*(s+e), FONT_HERSHEY_SIMPLEX, 0.4, color, 1);			
	}

#ifndef IOS
	imshow(windowname, img);
	//waitKey(0);
	//destroyWindow(windowname);
#endif

}
	

void CVPDetector::drawLines(string windowname, Mat img, LineFeature *lines, int num_lines, Scalar color)
{
    for(int i=0; i<num_lines; i++)
	{
		//line(img, Point(int(lines[i].s[0]), int(lines[i].s[1])), Point(int(lines[i].e[0]), int(lines[i].e[1])), color, 1);
        
        
        LineIterator it(img, Point(int(lines[i].s[0]), int(lines[i].s[1])), Point(int(lines[i].e[0]), int(lines[i].e[1])), 8, true);
        for(int j=0; j<it.count; j++, it++)
        {
            if(j%2 != 0)
            {
                (*it)[2] = color.val[2];
                (*it)[1] = color.val[1];
                (*it)[0] = color.val[0];
                
            }
        }
        
        
	}
    
#ifndef IOS
	imshow(windowname, img);
	//waitKey(0);
	//destroyWindow(windowname);
#endif
    
}

void CVPDetector::drawLines(string windowname, Mat img, double *lines, int num_lines, int *idx_best_lines, int num_best_lines, Scalar color)
{
	for(int i=0; i<num_best_lines; i++)
	{
		int idx = idx_best_lines[i];
		line(img, Point(int(lines[idx*4]), int(lines[idx*4+1])), Point(int(lines[idx*4+2]), int(lines[idx*4+3])), color, 2);
	}
#ifndef IOS
	imshow(windowname, img);
	//waitKey(0);
	//destroyWindow(windowname);
#endif
}


void CVPDetector::drawLines(string windowname, Mat img, LineCluster line_cluster, Mat sorted_idx, Scalar color, bool draw_text)
{
	Point s, e;
	//for(int i=0; i<int(line_cluster.num_lines); i++)
	for(int i=0; i<sorted_idx.cols; i++)
	{		
		int idx = sorted_idx.at<int>(i);

		s.x = line_cluster.lines[idx]->s[0];
		s.y = line_cluster.lines[idx]->s[1];

		e.x = line_cluster.lines[idx]->e[0];
		e.y = line_cluster.lines[idx]->e[1];

		line(img, s, e, color, 1);

		if(draw_text)
		{
			char id[50];
			//sprintf(id, "%d", line_cluster.lines[idx]->id);
			sprintf(id, "%d", i);
			putText(img, id, s, FONT_HERSHEY_SIMPLEX, 0.4, color, 1);			
		}

	//	imshow(windowname, img);
	//	waitKey(0);
	}

	#ifndef IOS
	imshow(windowname, img);
	//waitKey(0);
	#endif
}

void CVPDetector::drawLines(string windowname, Mat img, LineCluster line_cluster, int width, int height, Scalar color, bool draw_text)
{
	Point s, e;
	for(int i=0; i<int(line_cluster.num_lines); i++)
	{		
		getEndPoints(line_cluster.lines[i]->line, s, e, width, height);
		
		line(img, s, e, color, 1);

		if(draw_text)
		{
			char id[50];
			//sprintf(id, "%d", line_cluster.lines[i]->id);
			sprintf(id, "%d", i);
			putText(img, id, 0.5*(s+e), FONT_HERSHEY_SIMPLEX, 0.4, color, 1);			
		}
	}

	#ifndef IOS
	imshow(windowname, img);
	//waitKey(0);
	#endif
}

void CVPDetector::drawLines(string windowname, Mat img, LineCluster line_cluster, Mat sorted_idx,
																	int width, int height, Scalar color, bool draw_text)
{
	Point s, e;
	for(int i=0; i<int(line_cluster.num_lines); i++)
	{		
		int idx = sorted_idx.at<int>(i);
		getEndPoints(line_cluster.lines[idx]->line, s, e, width, height);
		
		line(img, s, e, color, 1);

		if(draw_text)
		{
			char id[50];
			//sprintf(id, "%d", line_cluster.lines[idx]->id);
			sprintf(id, "%d", i);
			putText(img, id, 0.5*(s+e), FONT_HERSHEY_SIMPLEX, 0.4, color, 1);			
		}
	}

	#ifndef IOS
	imshow(windowname, img);
	//waitKey(0);
	#endif
}


void CVPDetector::drawLines(string windowname, Mat img, vector<LineCluster> line_clusters, bool draw_text)
{
	Scalar color[12] = { CV_RGB(255,0,0), CV_RGB(0, 255, 0), CV_RGB(0, 0, 255), CV_RGB(128, 0, 128),
		CV_RGB(128, 128, 0), CV_RGB(0, 128, 128), CV_RGB(255, 128, 0), CV_RGB(255, 0, 128), CV_RGB(128, 255, 0), 
		CV_RGB(128, 0, 255), CV_RGB(0, 128, 255), CV_RGB(0, 255, 128) };
		
    
	Point s, e;
	for(int i=0; i<int(line_clusters.size()); i++)
		drawLines(windowname, img, line_clusters[i], color[i - (i / 12)*12], draw_text);
}

void CVPDetector::drawLines(string windowname, Mat img, LineCluster line_cluster, Scalar color, bool draw_text)
{
	Point s, e;
	for(int i=0; i<int(line_cluster.num_lines); i++)
	{
        
		s.x = line_cluster.lines[i]->s_orig[0];
		s.y = line_cluster.lines[i]->s_orig[1];

		e.x = line_cluster.lines[i]->e_orig[0];
		e.y = line_cluster.lines[i]->e_orig[1];
        
//        s.x = line_cluster.lines[i]->s[0];
//        s.y = line_cluster.lines[i]->s[1];
//        
//        e.x = line_cluster.lines[i]->e[0];
//        e.y = line_cluster.lines[i]->e[1];
		
		line(img, s, e, color, 1);
        circle(img, s, 2,  color);
		if(draw_text)
		{
			char id[50];
			//sprintf(id, "%d", line_cluster.lines[i]->id);
			//sprintf(id, "%d", line_cluster.lines[i]->side);
			sprintf(id, "%d", i);
			//sprintf(id, "%d", line_cluster.lines[i]->above_below);
			putText(img, id, 0.5*(s+e), FONT_HERSHEY_SIMPLEX, 0.4, color, 1);			
				
		}
//		imshow(windowname, img);
//		waitKey(0);
	}

	#ifndef IOS
	imshow(windowname, img);
	#endif
}



void CVPDetector::drawHorizon(string windowname, Mat img, double vd_v[3], Mat K, int width, int height, Scalar color)
{

    Mat n, l;
    Point s, e;
    
    n = (Mat_<double>(3,1) << vd_v[0], vd_v[1], vd_v[2]);
    l = (K.inv()).t() * n;
    
    double line_[3];
    line_[0] = l.at<double>(0);
    line_[1] = l.at<double>(1);
    line_[2] = l.at<double>(2);
    getEndPoints(line_, s, e, width, height);

	if(0)
	{
		LineIterator it(img, s, e, 8, true);
		for(int i=0; i<it.count; i++, it++)
		{
			if(i%5 != 0)
			{
				(*it)[2] = 255;
				(*it)[1] = 255;

			}
		}
	}
	else
		line(img, s, e, color, 2);
       
    
#ifndef IOS
	imshow(windowname, img);
	//waitKey(0);
#endif
    
}

void CVPDetector::drawVanishingPoints(string windowname, Mat img, double x[3], double y[3], double z[3], Mat K)
{
    double vp[3];
    if(projection(x, vp, K))
    {
        circle(img, Point(vp[0], vp[1]), 10, CV_RGB(0, 0, 255), 2);
    }
    if(projection(y, vp, K))
	{
        circle(img, Point(vp[0], vp[1]), 10, CV_RGB(0, 255, 0), 2);
    }
    if(projection(z, vp, K))
	{
        circle(img, Point(vp[0], vp[1]), 10, CV_RGB(255, 0, 0), 2);
    }
    
#ifndef IOS
	imshow(windowname, img);
	//waitKey(0);
#endif
}

void CVPDetector::drawAxes(string windowname, Mat img, double x[3], double y[3], double z[3], Mat K)
{
    Mat S, E, s, e;
    double f;
        
    f = K.at<double>(0,0);
    S = (Mat_<double>(3,1) << 0, 0, f*10);
    s = K*S;
    s.at<double>(0) /= s.at<double>(2);
    s.at<double>(1) /= s.at<double>(2);
    
    // x axis
    E = (Mat_<double>(3,1) << S.at<double>(0) + f*x[0], S.at<double>(1) + f*x[1], S.at<double>(2)+ f*x[2]);
    e = K*E;
    e.at<double>(0) /= e.at<double>(2);
    e.at<double>(1) /= e.at<double>(2);
    line(img, Point(s.at<double>(0), s.at<double>(1)), Point(e.at<double>(0), e.at<double>(1)), CV_RGB(255,0,0), 3);
    
    // y axis
    E = (Mat_<double>(3,1) << S.at<double>(0) + f*y[0], S.at<double>(1) + f*y[1], S.at<double>(2)+ f*y[2]);
    e = K*E;
    e.at<double>(0) /= e.at<double>(2);
    e.at<double>(1) /= e.at<double>(2);
    line(img, Point(s.at<double>(0), s.at<double>(1)), Point(e.at<double>(0), e.at<double>(1)), CV_RGB(0,255,0), 3);
    
    // z axis
    E = (Mat_<double>(3,1) << S.at<double>(0) + f*z[0], S.at<double>(1) + f*z[1], S.at<double>(2)+ f*z[2]);
    e = K*E;
    e.at<double>(0) /= e.at<double>(2);
    e.at<double>(1) /= e.at<double>(2);
    line(img, Point(s.at<double>(0), s.at<double>(1)), Point(e.at<double>(0), e.at<double>(1)), CV_RGB(0,0,255), 3);
    
    circle(img, Point(s.at<double>(0), s.at<double>(1)), 3, CV_RGB(125,0,125));
    
    
#ifndef IOS
	//imshow(windowname, img);
#endif
}

void CVPDetector::getEndPoints(double line[3], cv::Point &s, cv::Point &e, int width, int height)
{
	double a, b;
	double x_, y_, x[2], y[2];
	int cnt = 0;

	a = line[0]/line[2];
	b = line[1]/line[2];

	x_ = 0;
	y_ = -1/b;
	if( y_ > 0 && y_ < height)
	{
		x[cnt] = x_;
		y[cnt] = y_;
		cnt++;
	}

	x_ = width;
	y_ = (-a * x_ - 1) / b;
	if( y_ > 0 && y_ < height)
	{
		x[cnt] = x_;
		y[cnt] = y_;
		cnt++;
	}

	y_ = 0;
	x_ = -1/a;	
	if( x_ > 0 && x_ < width)
	{
		x[cnt] = x_;
		y[cnt] = y_;
		cnt++;
	}

	y_ = height;
	x_ = (-b * y_ - 1) / a;	
	if( x_ > 0 && x_ < width)
	{
		x[cnt] = x_;
		y[cnt] = y_;
		cnt++;
	}
	
	if(cnt >= 2)
	{
		s = Point(x[0], y[0]);
		e = Point(x[1], y[1]);
	}
	else
	{
		s = Point(0,0);
		e = Point(0,0);
	}
}

void CVPDetector::crossProd(double *a, double *b, double *axb)
{
	axb[0] = -a[2]*b[1] + a[1]*b[2];
	axb[1] =  a[2]*b[0] - a[0]*b[2];
	axb[2] = -a[1]*b[0] + a[0]*b[1];
}

double CVPDetector::innerProd(double *a, double *b, int dim)
{
	double sum;
	sum = 0;
	for(int i=0; i<dim; i++)
	{
		sum += a[i]*b[i];
	}
	return sum; 
}

void CVPDetector::unitVector(double *x, double *y, int dim)
{	
	double length;

	length = 0;
	for(int i=0; i<dim; i++)
		length += x[i]*x[i];
	length = sqrt(length);

	if(length)
	{
		for(int i=0; i<dim; i++)
			y[i] = x[i] / length;
	}
	else
		memcpy(y, x, sizeof(double)*dim);
}


void CVPDetector::orthonormalVector(double *x, double *y)
{
    if(fabs(x[0]) > INFINITE_TEST_MIN)
    {
        y[1] = 1;
        y[2] = 1;
        y[0] = -(x[1]+x[2])/x[0];
    }
    else if(fabs(x[1]) > INFINITE_TEST_MIN)
    {
        y[0] = 1;
        y[2] = 1;
        y[1] = -(x[0]+x[2])/x[1];
    }
    else if(fabs(x[2]) > INFINITE_TEST_MIN)
    {
        y[0] = 1;
        y[1] = 1;
        y[2] = -(x[0]+x[1])/x[2];
    }
    unitVector(y, y, 3);

    
}

void CVPDetector::undistortCoord(double* x, double *undistorted_x)
{
    Mat xx = (Mat_<double>(2,1) << x[0], x[1]);
    Mat xxn = normalize(xx, m_K, m_disto_coeff);
    Mat xxn_ = (Mat_<double>(3,1) << xxn.at<double>(0), xxn.at<double>(1), 1);
    
    Mat undistorted_xx = m_K*xxn_;
    undistorted_x[0] = undistorted_xx.at<double>(0) / undistorted_xx.at<double>(2);
    undistorted_x[1] = undistorted_xx.at<double>(1) / undistorted_xx.at<double>(2);
}

// source: Matlab toolbox
Mat CVPDetector::comp_distortion_oulu(Mat xd, Mat distCoeffs)
{
	Mat x, delta_x;
	double k1, k2, k3, p1, p2;
	double r_2, k_radial;

	k1 = distCoeffs.at<double>(0);
	k2 = distCoeffs.at<double>(1);
	k3 = distCoeffs.at<double>(4);
	p1 = distCoeffs.at<double>(2);
	p2 = distCoeffs.at<double>(3);


	x = xd.clone();			// initial guess
	delta_x = Mat(2,1,CV_64F);
	for (int i=0; i<20; i++)
	{
		r_2 = xd.at<double>(0)*xd.at<double>(0) + xd.at<double>(1)*xd.at<double>(1);
		k_radial = 1 + k1*r_2 + k2*r_2*r_2 + k3*r_2*r_2*r_2;
		delta_x.at<double>(0) = 2*p1*x.at<double>(0)*x.at<double>(1) + p2*(r_2 + 2*x.at<double>(0)*x.at<double>(0));
		delta_x.at<double>(1) = 2*p2*x.at<double>(0)*x.at<double>(1) + p1*(r_2 + 2*x.at<double>(1)*x.at<double>(1));
		x = (xd - delta_x) / k_radial;
	}
	
	/* source
	k1 = k(1);
    k2 = k(2);
    k3 = k(5);
    p1 = k(3);
    p2 = k(4);
    
	
    x = xd; 				% initial guess
    
    for kk=1:20,
        
        r_2 = sum(x.^2);
        k_radial =  1 + k1 * r_2 + k2 * r_2.^2 + k3 * r_2.^3;
        delta_x = [2*p1*x(1,:).*x(2,:) + p2*(r_2 + 2*x(1,:).^2);
        p1 * (r_2 + 2*x(2,:).^2)+2*p2*x(1,:).*x(2,:)];
        x = (xd - delta_x)./(ones(2,1)*k_radial);
            
    end;
	*/
	return x;    
}


// source: Matlab toolbox
// K = [fx, fx*alpha_c, u0; 
//       0,         fy, v0;
//       0,          0,  1]
Mat CVPDetector::normalize(Mat x_kk, Mat K, Mat distCoeffs)
{
	Mat xn, x_distort;
	double fx, fy, u0, v0, alpha_c;

	fx = K.at<double>(0,0);
	fy = K.at<double>(1,1);
	u0 = K.at<double>(0,2);
	v0 = K.at<double>(1,2);
	alpha_c = K.at<double>(0,1) / fx;	

	x_distort = Mat(2,1,CV_64F);
	x_distort.at<double>(0) = (x_kk.at<double>(0) - u0)/fx;
	x_distort.at<double>(1) = (x_kk.at<double>(1) - v0)/fy;
	if(alpha_c)
		x_distort.at<double>(0) = x_distort.at<double>(0) - alpha_c * x_distort.at<double>(1);
	
	//if(distCoeffs.rows)
		xn = comp_distortion_oulu(x_distort, distCoeffs);
	//else
	//	xn = x_distort.clone();

	return xn;
	/* source
	if nargin < 5,
	   alpha_c = 0;
	   if nargin < 4;
		  kc = [0;0;0;0;0];
		  if nargin < 3;
			 cc = [0;0];
			 if nargin < 2,
				fc = [1;1];
			 end;
		  end;
	   end;
	end;


	% First: Subtract principal point, and divide by the focal length:
	x_distort = [(x_kk(1,:) - cc(1))/fc(1);(x_kk(2,:) - cc(2))/fc(2)];

	% Second: undo skew
	x_distort(1,:) = x_distort(1,:) - alpha_c * x_distort(2,:);

	if norm(kc) ~= 0,
		% Third: Compensate for lens distortion:
		xn = comp_distortion_oulu(x_distort,kc);
	else
	   xn = x_distort;
	end;
	*/
}

// line[3]:		line in homogeneous representation
// m[2]:		mid point
// pt[2]:		given point not on the line
// pt_dest[2]:	closest point on the line
void CVPDetector::findClosestPointOnALine(double line[3], double m[2], double pt[2], double pt_dest[2])
{
	double mp[2], n[2], length;

	mp[0] = pt[0] - m[0];
	mp[1] = pt[1] - m[1];

	// n: perpendicular direction to the line
	length = sqrt(line[0]*line[0] + line[1]*line[1]);
	n[0] = line[0] / length;
	n[1] = line[1] / length;

	length = innerProd(mp, n, 2);
	pt_dest[0] = pt[0] - length*n[0];
	pt_dest[1] = pt[1] - length*n[1];
}

double *CVPDetector::colorMatToGrayDouble(Mat img, Mat &gray_img)
{	   
	double *image;
	uchar* p;
	cvtColor(img, gray_img, CV_BGR2GRAY);
	image = new double[gray_img.rows*gray_img.cols];
    p = gray_img.data;
    for( unsigned int i =0; i < gray_img.rows*gray_img.cols; i++)
        image[i] = *p++;

	return image;
}

uchar *CVPDetector::colorMatToGrayUchar(Mat img, Mat &gray_img)
{	   
	uchar *image;
	uchar* p;
	cvtColor(img, gray_img, CV_BGR2GRAY);
	image = new uchar[gray_img.rows*gray_img.cols];
    p = gray_img.data;
    for( unsigned int i =0; i < gray_img.rows*gray_img.cols; i++)
        image[i] = *p++;

	return image;
}


void CVPDetector::lineCorrection(LineCluster line_cluster, double Kinv[9], bool bUpdate)
{	
	for(int i=0; i<line_cluster.num_lines; i++)
		lineCorrection(line_cluster.lines[i], line_cluster.vd, Kinv, bUpdate);

}
void CVPDetector::lineCorrection(LineFeature *line_feature, double vd[3], double Kinv[9], bool bUpdate)
{
    double ray[3], m_homo[3];

	// M: a ray from the mid point
	m_homo[0] = line_feature->m[0];
	m_homo[1] = line_feature->m[1];
	m_homo[2] = 1.0;
	mul(Kinv, m_homo, ray, 3, 3);
	crossProd(ray, vd, line_feature->line_norm);
	unitVector(line_feature->line_norm, line_feature->line_norm, 3);
	
    
	if(bUpdate)
	{
		// update all features according to the correction
			
        double Ktinv[9] = {Kinv[0], Kinv[3], Kinv[6], Kinv[1], Kinv[4], Kinv[7], Kinv[2], Kinv[5], Kinv[8]};
		mul(Ktinv, line_feature->line_norm, line_feature->line, 3, 3);
		normalizeLine(line_feature->line);				
    
		// correct end points & mid points
		findClosestPointOnALine(line_feature->line, line_feature->m, line_feature->s, line_feature->s);
		findClosestPointOnALine(line_feature->line, line_feature->m, line_feature->e, line_feature->e);
        line_feature->m[0] = 0.5*(line_feature->s[0] + line_feature->e[0]);
        line_feature->m[1] = 0.5*(line_feature->s[1] + line_feature->e[1]);
        
    
		// angle
//		line_feature->angle = getLineAngle(line_feature->s, line_feature->e);
    
		// line length
		line_feature->length =
		sqrt( (line_feature->s[0]-line_feature->e[0])*(line_feature->s[0]-line_feature->e[0]) +
			 (line_feature->s[1]-line_feature->e[1])*(line_feature->s[1]-line_feature->e[1]) );
	}		 
}

void CVPDetector::sortLines(LineCluster line_cluster, double Kinv[9], double ref_normal_vector[3])
{
	Mat angles_mat, sorted_idx, ref_line_;
	double *angles, orth_vector[3], ref_line[3], signed_dist;
    
    if(line_cluster.num_lines == 0)
        return;
		
	angles = new double[line_cluster.num_lines];
		
	crossProd(line_cluster.vd, ref_normal_vector, orth_vector);
	unitVector(orth_vector, orth_vector, 3);


	// reference_line
    double Ktinv[9] = {Kinv[0], Kinv[3], Kinv[6], Kinv[1], Kinv[4], Kinv[7], Kinv[2], Kinv[5], Kinv[8]};
	mul(Ktinv, ref_normal_vector, ref_line, 3, 3);
		

	for(int i=0; i<line_cluster.num_lines; i++)
	{
		// correct sign of line normal vector
        double *line_norm = line_cluster.lines[i]->line_norm;
		if( innerProd(orth_vector, line_norm, 3) < 0)
		{
			line_norm[0] = -line_norm[0];
			line_norm[1] = -line_norm[1];
			line_norm[2] = -line_norm[2];
		}
		angles[i] = innerProd(ref_normal_vector, line_norm, 3);
	}
	
	angles_mat = Mat(1, line_cluster.num_lines, CV_64F, angles).clone();
	sortIdx(angles_mat, sorted_idx, CV_SORT_ASCENDING);
	
	// reassign the id of each line according to the angle of the line w.r.t the reference normal
	// and assign the side of each line according to the signed distance of its mid point to the ref line
	for(int i=0; i<line_cluster.num_lines; i++)
	{
		int idx = sorted_idx.at<int>(i);
		line_cluster.lines[idx]->id = i;
		signed_dist = ref_line[0] * line_cluster.lines[idx]->m[0] + 
					ref_line[1] * line_cluster.lines[idx]->m[1] + 
					ref_line[2];
		if(signed_dist < 0)
			line_cluster.lines[idx]->side = 0;
		else
			line_cluster.lines[idx]->side = 1;	
	}

	delete[] angles;
}

// horizon_n: normal vector of horizon = z-vanishing direction
void CVPDetector::aboveBelowHorizon(LineCluster line_cluster, double horizon_n[3], Mat Ktinv)
{
	double signed_dist, horizon[3];
	
	mul(md_Ktinv, horizon_n, horizon, 3, 3);

	normalizeLine(horizon);
		
	for(int i=0; i<line_cluster.num_lines; i++)
	{
		signed_dist = horizon[0] * line_cluster.lines[i]->m[0] + horizon[1] * line_cluster.lines[i]->m[1] + horizon[2];
		if(signed_dist > 0)
		{
			line_cluster.lines[i]->above_below = ABOVE_HORIZON;
		}
		else
		{
			line_cluster.lines[i]->above_below = BELOW_HORIZON;
		}
	}
}

// S (5):			sampling size of points on a line segment
// N (9):			num_sub_regions
// W (5):			5x5 size of sub-region
// th (0.4):		threshold for restraining maximum value in the unit vector
// ln:				orthogonal vector to the line direction.
double *CVPDetector::MSLD(LineFeature *line, double* image, int rows, int cols, double ln[2],
							int S, int N, int W, double th)
{	
	double lx[2], ly[2];
	int p_x, p_y, o_x, o_y, x, y, x_img, y_img;
	double sigma;
	int num_sampling;
	double *V, *weight, *w_upper, *w_down;
	double *mean_V, *std_V, norm_mean_V, norm_std_V;
	double g_x, g_y, g_x_img, g_y_img, g_x_cont, g_y_cont;

	getLineAxis(line, image, rows, cols, lx, ly);
	ln[0] = ly[0];
	ln[1] = ly[1];
	memcpy(line->ln, ln, sizeof(double)*2); // orthonormal vector to the line direction
    
	/*
	num_sampling = int( line->length / double(S) );
	// num_sampling must be greater than 2
	if(num_sampling < 2)
	{
		S = int(line->length / 2.f);
		num_sampling = int( line->length / double(S) );
	}
	*/

	// fixed num_sampling
	num_sampling = S;
	S = int(line->length / double(num_sampling ));
	if(S < 1 )
		S = 1;

	sigma = W*N/4.f;
	V = new double[4*N*num_sampling];
	weight = new double[W*N];
	w_upper = new double[W];
	w_down = new double[W];
	mean_V = new double[4*N];
	std_V = new double[4*N];
		
	// initialze weight 
	double D = 1/ (sqrt(2*CV_PI)*sigma);
	double dist = - W*N*0.5f;
	for(int i=0; i<W*N; i++)
	{
		dist ++;
		weight[i] = D*exp(-dist*dist / (2*sigma*sigma));
	}
	memset(V, 0, sizeof(double)*4*N*num_sampling);
	memset(mean_V, 0, sizeof(double)*4*N);
	memset(std_V, 0, sizeof(double)*4*N);

	w_upper[0] = 1 + floor(0.5f*W) + W;
	w_down[0]  = 2 + floor(0.5f*W);
	for(int i=1; i<W; i++)
	{
		w_upper[i] = w_upper[i-1]- 1;
		w_down[i]  = w_down[i-1] + 1;
	}

	// start point of sampling
	p_x = -line->length*0.5;
	p_y = 0;	
	for(int t=0; t<num_sampling; t++)
	{
		// left top corner of each sub-region
		o_x = -floor(W*0.5) + p_x;
		o_y = -floor(W*N*0.5) + p_y;

		for(int i=0; i<N; i++)	// for each sub-region
		{
			for(int j=0; j<W; j++)
			{
				y = o_y + j;
				for(int k=0; k<W; k++)
				{
					x = o_x + k;

					// transform into image coordinate system
					x_img = lx[0]*x + ly[0]*y + line->m[0];
					y_img = lx[1]*x + ly[1]*y + line->m[1];

					if(x_img < 1 || x_img > cols-2 || y_img < 1 || y_img > rows-2)
						continue;

					// gradient
					g_x_img = 0.5 * (image[y_img*cols + x_img+1] - image[y_img*cols + x_img-1]);
					g_y_img = 0.5 * (image[(y_img+1)*cols + x_img] - image[(y_img-1)*cols + x_img]);
					
					// transform into line coordinate system
					g_x = g_x_img*lx[0] + g_y_img*lx[1];
					g_y = g_x_img*ly[0] + g_y_img*ly[1];

					g_x *= weight[i*W+j];
					g_y *= weight[i*W+j];

					if(g_x > 0)
						V[(4*i  )*num_sampling + t] += g_x;
					else
						V[(4*i+1)*num_sampling + t] -= g_x;

					if(g_y > 0)
						V[(4*i+2)*num_sampling + t] += g_y;
					else
						V[(4*i+3)*num_sampling + t] -= g_y;

					// contribution to its nearest subregions
					if(N == 1)
						continue;

					if(i == 0)
					{
						g_x_cont = g_x*w_down[j]/(2*W+2);
						g_y_cont = g_y*w_down[j]/(2*W+2);

						if(g_x_cont > 0)
							V[(4*(i+1)  )*num_sampling + t] += g_x_cont;
						else
							V[(4*(i+1)+1)*num_sampling + t] -= g_x_cont;

						if(g_y_cont > 0)
							V[(4*(i+1)+2)*num_sampling + t] += g_y_cont;
						else
							V[(4*(i+1)+3)*num_sampling + t] -= g_y_cont;

					}
					else if(i>0 && i<N-1)
					{
						g_x_cont = g_x*w_upper[j]/(2*W+2);
						g_y_cont = g_y*w_upper[j]/(2*W+2);

						if(g_x_cont > 0)
							V[(4*(i-1)  )*num_sampling + t] += g_x_cont;
						else
							V[(4*(i-1)+1)*num_sampling + t] -= g_x_cont;

						if(g_y_cont > 0)
							V[(4*(i-1)+2)*num_sampling + t] += g_y_cont;
						else
							V[(4*(i-1)+3)*num_sampling + t] -= g_y_cont;

						g_x_cont = g_x*w_down[j]/(2*W+2);
						g_y_cont = g_y*w_down[j]/(2*W+2);

						if(g_x_cont > 0)
							V[(4*(i+1)  )*num_sampling + t] += g_x_cont;
						else
							V[(4*(i+1)+1)*num_sampling + t] -= g_x_cont;

						if(g_y_cont > 0)
							V[(4*(i+1)+2)*num_sampling + t] += g_y_cont;
						else
							V[(4*(i+1)+3)*num_sampling + t] -= g_y_cont;
					}
					else if(i == N-1)
					{
						g_x_cont = g_x*w_upper[j]/(2*W+2);
						g_y_cont = g_y*w_upper[j]/(2*W+2);

						if(g_x_cont > 0)
							V[(4*(i-1)  )*num_sampling + t] += g_x_cont;
						else
							V[(4*(i-1)+1)*num_sampling + t] -= g_x_cont;

						if(g_y_cont > 0)
							V[(4*(i-1)+2)*num_sampling + t] += g_y_cont;
						else
							V[(4*(i-1)+3)*num_sampling + t] -= g_y_cont;
					}

				}
			}
			o_y += W;
		}
		p_x += S;
	}

	// compute mean_V & std_V
	norm_mean_V = 0;
	norm_std_V = 0;
	for(int i=0; i<4*N; i++)
	{
		// mean
		for(int j=0; j<num_sampling; j++)
			mean_V[i] += V[i*num_sampling + j];
		
		mean_V[i] /= num_sampling;
		norm_mean_V += mean_V[i]*mean_V[i];

		// std
		for(int j=0; j<num_sampling; j++)
			std_V[i] += (V[i*num_sampling + j] - mean_V[i])*(V[i*num_sampling + j] - mean_V[i]);
		
		std_V[i] = sqrt(std_V[i]/(num_sampling-1));
		norm_std_V += std_V[i]*std_V[i];
	}
	norm_mean_V = sqrt(norm_mean_V);
	norm_std_V = sqrt(norm_std_V);
		
	// restraining maximum value
	for(int i=0; i<4*N; i++)
	{
		mean_V[i] /= norm_mean_V;
		if(mean_V[i] > th)
			mean_V[i] = th;
		std_V[i] /= norm_std_V;
		if(std_V[i] > th)
			std_V[i] = th;
	}

	// renormalization
	norm_mean_V = 0;
	norm_std_V = 0;
	for(int i=0; i<4*N; i++)
	{
		norm_mean_V += mean_V[i]*mean_V[i];
		norm_std_V += std_V[i]*std_V[i];		
	}
	norm_mean_V = sqrt(norm_mean_V);
	norm_std_V = sqrt(norm_std_V);
	for(int i=0; i<4*N; i++)
	{
		mean_V[i] /= norm_mean_V;		
		std_V[i] /= norm_std_V;
	}	

	double *descriptor = new double[8*N];
	memcpy(descriptor, mean_V, sizeof(double)*4*N);
	memcpy(descriptor+4*N, std_V, sizeof(double)*4*N);
	
		
	delete[] V;
	delete[] weight;
	delete[] w_upper;
	delete[] w_down;
	delete[] mean_V;
	delete[] std_V;

	return descriptor;
}

void CVPDetector::getLineAxis(LineFeature *line, double* image, int rows, int cols, double lx[2], double ly[2])
{
	double l[2], length, step_size;
	int num_data;
	    
	int offset = 3;
	
	step_size = 2;	
		
	// line vector	
	l[0] = line->e[0] - line->s[0];
	l[1] = line->e[1] - line->s[1];
    length = sqrt(l[0]*l[0] + l[1]*l[1]);
    
	if(length < step_size)
		step_size = 1;

	num_data = int(length / step_size);

	l[0] /= length;	l[1] /= length;
    
    lx[0] = l[0];    lx[1] = l[1];
    ly[0] = l[1];    ly[1] = -l[0];
    
    
    int acc_flag = 0;
    for(int i=0; i<num_data; i++)
    {
        int pt[2], pt_s[2], pt_e[2];
        pt[0] = int( line->s[0] + i*step_size*l[0] );		pt[1] = int( line->s[1] + i*step_size*l[1] );
        pt_e[0] = int( pt[0] + offset*ly[0] );               pt_e[1] = int( pt[1] + offset*ly[1] );
        pt_s[0] = int( pt[0] - offset*ly[0] );               pt_s[1] = int( pt[1] - offset*ly[1] );
        
        // check invalid index
        if(pt_e[0] < 0 || pt_e[0] > cols-1 || pt_e[1] < 0 || pt_e[1] > rows-1)
            continue;
        if(pt_s[0] < 0 || pt_s[0] > cols-1 || pt_s[1] < 0 || pt_s[1] > rows-1)
            continue;
        
        if(image[pt_s[1]*cols + pt_s[0]] < image[pt_e[1]*cols + pt_e[0]])
            acc_flag++;
        else
            acc_flag--;
    }
    
    if(acc_flag < 0){
        ly[0] = -ly[0];
        ly[1] = -ly[1];
    }
    
	/*
    // compute gradient mean vector alone the line
    vector<double> radians;
    vector<double> gx, gy;
	for(int i=0; i<num_data; i++)
	{
		int pt[2], ptL[2], ptR[2];
		pt[0] = int( line->s[0] + i*step_size*l[0] );		pt[1] = int( line->s[1] + i*step_size*l[1] );
        ptL[0] = int( pt[0] + offset*ly[0] );               ptL[1] = int( pt[1] + offset*ly[1] );
        ptR[0] = int( pt[0] - offset*ly[0] );               ptR[1] = int( pt[1] - offset*ly[1] );
        
        // check invalid index
        if(ptL[0] < 0 || ptL[0] > cols-1 || ptL[1] < 0 || ptL[1] > rows-1)
            continue;
        if(ptR[0] < 0 || ptR[0] > cols-1 || ptR[1] < 0 || ptR[1] > rows-1)
            continue;
        
        double grad[2];
        grad[0] = 0.5 * (image[pt[1]*cols + pt[0]+offset] - image[pt[1]*cols + pt[0]-offset]);
        grad[1] = 0.5 * (image[(pt[1]+offset)*cols + pt[0]] - image[(pt[1]-offset)*cols + pt[0]]);
        radians.push_back(atan2(grad[1], grad[0]));
        
        m_util.unitVector(grad, grad, 2);
        gx.push_back(grad[0]);
        gy.push_back(grad[1]);
	}
    
    // find a median value.
    vector<double> sorted;
    cv::sort(radians, sorted, CV_SORT_ASCENDING);
    double med_radian = sorted[sorted.size()/2];
    double ref[2];
    ref[0] = cos(med_radian);
    ref[1] = sin(med_radian);
    
	// adjust the vector perpendicular to the line direction
    if( ly[0]*ref[0] + ly[1]*ref[1] < 0){
        ly[0] = -ly[0];
        ly[1] = -ly[1];
    }
    */
}

void CVPDetector::getLineAxis2(LineFeature *line, double* image, int rows, int cols, double lx[2], double ly[2])
{
	double l[2], length, step_size;
	int num_data;
	    
	int cnt;
	int offset = 3;
	
	step_size = 2;	
		
	// line vector	
	l[0] = line->e[0] - line->s[0];
	l[1] = line->e[1] - line->s[1];
	length = line->length;

	if(length < step_size)
		step_size = 1;

	num_data = int(length / step_size);

	l[0] /= length;
	l[1] /= length;
	ly[0] = 0;
	ly[1] = 0;
	cnt = 0;
	// compute gradient mean vector alone the line
	for(int i=0; i<num_data; i++)
	{
		int pt[2];
		pt[0] = int( line->s[0] + i*step_size*l[0] );
		pt[1] = int( line->s[1] + i*step_size*l[1] );

		if(pt[0] < offset || pt[0] > cols-offset-1 || pt[1] < offset || pt[1] > rows-offset-1)
			continue;
		ly[0] += 0.5 * (image[pt[1]*cols + pt[0]+offset] - image[pt[1]*cols + pt[0]-offset]);
		ly[1] += 0.5 * (image[(pt[1]+offset)*cols + pt[0]] - image[(pt[1]-offset)*cols + pt[0]]);
		cnt++;
	}
	ly[0] /= cnt;
	ly[1] /= cnt;

	lx[0] = ly[1];
	lx[1] = -ly[0];

	// normalization
	length = sqrt(lx[0]*lx[0] + lx[1]*lx[1]);
	lx[0] /= length;
	lx[1] /= length;

	length = sqrt(ly[0]*ly[0] + ly[1]*ly[1]);
	ly[0] /= length;
	ly[1] /= length;

	
	double temp[2];
	temp[0] = l[1];
	temp[1] = -l[0];

	if( temp[0]*ly[0] + temp[1]*ly[1] < 0)
	{
		ly[0] = -temp[0];
		ly[1] = -temp[1];
	}
	else
	{
		ly[0] = temp[0];
		ly[1] = temp[1];
	}

	lx[0] = ly[1];
	lx[1] = -ly[0];

	
		
}

// A b = c
// A: mxn
// b: nx1
// c: mx1
void CVPDetector::mul(double *A, double *b, double *c, int m, int n)
{
    for(int i=0; i<m; i++)
    {
        c[i] = 0;
        for(int j=0; j<n; j++)
            c[i] += A[i*n+j]*b[j];
        
    }
}

void CVPDetector::mul(Mat M, double a[3], double b[3])
{
    double temp[3];
    memcpy(temp, a, sizeof(double)*3);
    
    b[0] = M.at<double>(0,0) * temp[0] + M.at<double>(0,1) * temp[1] + M.at<double>(0,2) * temp[2];
    b[1] = M.at<double>(1,0) * temp[0] + M.at<double>(1,1) * temp[1] + M.at<double>(1,2) * temp[2];
    b[2] = M.at<double>(2,0) * temp[0] + M.at<double>(2,1) * temp[1] + M.at<double>(2,2) * temp[2];
}





//Mat CVPDetector::refine(Mat R_c_w, LineCluster line_cluster_x, LineCluster line_cluster_y, LineCluster line_cluster_z)
//{
//    Mat rot_m;
//    Rodrigues(R_c_w, rot_m);
//    
//    double rot[3] = {rot_m.at<double>(0), rot_m.at<double>(1), rot_m.at<double>(2)};
//    
//    Problem problem;
//    for (int i = 0; i < line_cluster_x.num_lines; ++i) {
//        ceres::CostFunction* cost_function =
//        Residual::Create(line_cluster_x.lines[i]->line_norm_orig[0], line_cluster_x.lines[i]->line_norm_orig[1], line_cluster_x.lines[i]->line_norm_orig[2], 0);
//        problem.AddResidualBlock(cost_function, NULL, rot);
//    }
//    
//    for (int i = 0; i < line_cluster_y.num_lines; ++i) {
//        ceres::CostFunction* cost_function =
//        Residual::Create(line_cluster_y.lines[i]->line_norm_orig[0], line_cluster_y.lines[i]->line_norm_orig[1], line_cluster_y.lines[i]->line_norm_orig[2], 1);
//        problem.AddResidualBlock(cost_function, NULL, rot);
//    }
//    
//    for (int i = 0; i < line_cluster_z.num_lines; ++i) {
//        ceres::CostFunction* cost_function =
//        Residual::Create(line_cluster_z.lines[i]->line_norm_orig[0], line_cluster_z.lines[i]->line_norm_orig[1], line_cluster_z.lines[i]->line_norm_orig[2], 2);
//        problem.AddResidualBlock(cost_function, NULL, rot);
//    }
//    
//    ceres::Solver::Options options;
//    options.linear_solver_type = ceres::DENSE_SCHUR;//DENSE_SCHUR; //DENSE_QR
//    options.minimizer_progress_to_stdout = true;
//    ceres::Solver::Summary summary;
//    ceres::Solve(options, &problem, &summary);
//    std::cout << summary.FullReport() << "\n";
//    
//    rot_m.at<double>(0) = rot[0];
//    rot_m.at<double>(1) = rot[1];
//    rot_m.at<double>(2) = rot[2];
//    
//    Mat R_c_w_final;
//    Rodrigues(rot_m, R_c_w_final);
//    
//    cout << "Initial R: " << R_c_w << "\n";
//    cout << "Final R: " << R_c_w_final << "\n";
//    
//    return R_c_w_final;
//}
//
//void CVPDetector::refine(Mat R_c_w, Mat K, LineCluster line_cluster_x, LineCluster line_cluster_y, LineCluster line_cluster_z, Mat &R_c_w_final, Mat &K_final)
//{
//    Mat rot_m;
//    Rodrigues(R_c_w, rot_m);
//    
//    double x[4] = {rot_m.at<double>(0), rot_m.at<double>(1), rot_m.at<double>(2), K.at<double>(0,0)};
//    
//    Problem problem;
//    for (int i = 0; i < line_cluster_x.num_lines; ++i) {
//        ceres::CostFunction* cost_function =
//        Residual2::Create(line_cluster_x.lines[i]->line[0], line_cluster_x.lines[i]->line[1], line_cluster_x.lines[i]->line[2], K.at<double>(0,2), K.at<double>(1,2), 0);
//        problem.AddResidualBlock(cost_function, new ceres::CauchyLoss(0.5), x);
//    }
//    
//    for (int i = 0; i < line_cluster_y.num_lines; ++i) {
//        ceres::CostFunction* cost_function =
//        Residual2::Create(line_cluster_y.lines[i]->line[0], line_cluster_y.lines[i]->line[1], line_cluster_y.lines[i]->line[2], K.at<double>(0,2), K.at<double>(1,2), 1);
//        problem.AddResidualBlock(cost_function, new ceres::CauchyLoss(0.5), x);
//    }
//    
//    for (int i = 0; i < line_cluster_z.num_lines; ++i) {
//        ceres::CostFunction* cost_function =
//        Residual2::Create(line_cluster_z.lines[i]->line[0], line_cluster_z.lines[i]->line[1], line_cluster_z.lines[i]->line[2], K.at<double>(0,2), K.at<double>(1,2), 2);
//        problem.AddResidualBlock(cost_function, new ceres::CauchyLoss(0.5), x);
//    }
//    
//    ceres::Solver::Options options;
//    options.linear_solver_type = ceres::DENSE_SCHUR;//DENSE_SCHUR; //DENSE_QR
//    options.minimizer_progress_to_stdout = false;
//    ceres::Solver::Summary summary;
//    ceres::Solve(options, &problem, &summary);
//   // std::cout << summary.FullReport() << "\n";
//    
//    rot_m.at<double>(0) = x[0];
//    rot_m.at<double>(1) = x[1];
//    rot_m.at<double>(2) = x[2];
//    
//    Rodrigues(rot_m, R_c_w_final);
//    
//    K_final = (Mat_<double>(3,3) << x[3], 0, K.at<double>(0,2), 0, x[3], K.at<double>(1,2), 0, 0, 1);
//    
////    cout << "Initial R: " << R_c_w << "\n";
////    cout << "Final R: " << R_c_w_final << "\n";
////    cout << "Initial K: " << K << "\n";
////    cout << "Final K: " << K_final << "\n";
//    
//}
//
