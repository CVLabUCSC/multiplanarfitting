#include <string>
#include <boost\filesystem.hpp>

#include "Proc.h"
#include "TR.h"
#include "VPDetector.h"

using namespace boost::filesystem;

void individualRefinement(const char *dirpath);

// Utilities

Mat load3x3Matrix(const std::string &filename);

double* removeOutOfRangeLines(cv::Mat image, double *lines);

void refinement(const char *dirpath, const char *first_R, int beg_index, int end_index, string extension);

void refinement(std::string dirpath, std::string extension, bool useEstimatedRotation = true);

int main();
