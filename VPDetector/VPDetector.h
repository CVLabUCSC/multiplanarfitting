/*----------------------------------------------------------------------------

  VPDetector - Vanishig Point Detector on images

  This code is part of the following publication:

    "Myung Hwangbo, "Vision-Based Navigation for a Small Fixed-Wing Airplane 
	in Urban Environment", Tech. Report CMU-RI-TR-12-11, Robotics Institute, 
	Carnegie Mellon University, 2012
    http://www.ri.cmu.edu/pub_files/2012/6/dissertation_myung.pdf
	 
  ----------------------------------------------------------------------------*/


#pragma once


#include "types.h"
#include "Utility.h"
#include "ceres/ceres.h"
#include "ceres/rotation.h"
#include "glog/logging.h"

using ceres::AutoDiffCostFunction;
using ceres::CostFunction;
using ceres::Problem;
using ceres::Solver;
using ceres::Solve;
//
//struct Residual{
//    Residual(double ux, double uy, double uz, int dir)
//    : ux(ux), uy(uy), uz(uz), dir(dir) {}
//    
//    template <typename T>
//    bool operator()(const T* const rot, T* residuals) const {
//        
//        T R[9]; // (in column major order)
//        ceres::AngleAxisToRotationMatrix(rot, R);
//        
//        
//        residuals[0] = R[3*dir]*T(ux) + R[3*dir+1]*T(uy) + R[3*dir+2]*T(uz);
//        
//        return true;
//    }
//    
//    static ceres::CostFunction* Create(const double ux, const double uy, const double uz, const int dir) {
//        return (new ceres::AutoDiffCostFunction<Residual, 1, 3>(new Residual(ux, uy, uz, dir)));
//    }
//    
//    double ux, uy, uz;  // line normal vector
//    int dir;
//};
//
//struct Residual2{
//    Residual2(double lx, double ly, double lz, double tx, double ty, int dir)
//    : lx(lx), ly(ly), lz(lz), tx(tx), ty(ty), dir(dir) {}
//    
//    template <typename T>
//    bool operator()(const T* const x, T* residuals) const {
//        
//        T rot[3] = {x[0], x[1], x[2]};
//        T R[9]; // (in column major order)
//        ceres::AngleAxisToRotationMatrix(rot, R);
//        
//        T K[9] = {x[3], T(0), T(tx), T(0), x[3], T(ty), T(0), T(0), T(1)};
//        T l[3] = {T(lx), T(ly), T(lz)};
//        // u = K.t() * l
//        T ux = K[0]*l[0] + K[3]*l[1] + K[6]*l[2];
//        T uy = K[1]*l[0] + K[4]*l[1] + K[7]*l[2];
//        T uz = K[2]*l[0] + K[5]*l[1] + K[8]*l[2];
//        
//        T leng = sqrt(ux*ux + uy*uy + uz*uz);
//        ux /= leng;
//        uy /= leng;
//        uz /= leng;
//        
//        
//        residuals[0] = R[3*dir]*ux + R[3*dir+1]*uy + R[3*dir+2]*uz;
//        
//        return true;
//    }
//    
//    static ceres::CostFunction* Create(const double lx, const double ly, const double lz, const double tx, const double ty, const int dir) {
//        return (new ceres::AutoDiffCostFunction<Residual2, 1, 4>(new Residual2(lx, ly, lz, tx, ty, dir)));
//    }
//    
//    double lx, ly, lz, tx, ty;  // line normal vector
//    int dir;
//};

//struct Residual2{
//    Residual2(double lx, double ly, double lz, int dir)
//    : lx(lx), ly(ly), lz(lz), dir(dir) {}
//    
//    template <typename T>
//    bool operator()(const T* const x, T* residuals) const {
//        
//        T rot[3] = {x[0], x[1], x[2]};
//        T R[9]; // (in column major order)
//        ceres::AngleAxisToRotationMatrix(rot, R);
//        
//        T K[9] = {x[3], T(0), x[5], T(0), x[4], x[6], T(0), T(0), T(1)};
//        T l[3] = {T(lx), T(ly), T(lz)};
//        // u = K.t() * l
//        T ux = K[0]*l[0] + K[3]*l[1] + K[6]*l[2];
//        T uy = K[1]*l[0] + K[4]*l[1] + K[7]*l[2];
//        T uz = K[2]*l[0] + K[5]*l[1] + K[8]*l[2];
//        
//        T leng = sqrt(ux*ux + uy*uy + uz*uz);
//        ux /= leng;
//        uy /= leng;
//        uz /= leng;
//        
//        
//        residuals[0] = R[3*dir]*ux + R[3*dir+1]*uy + R[3*dir+2]*uz;
//        
//        return true;
//    }
//    
//    static ceres::CostFunction* Create(const double lx, const double ly, const double lz, const int dir) {
//        return (new ceres::AutoDiffCostFunction<Residual2, 1, 7>(new Residual2(lx, ly, lz, dir)));
//    }
//    
//    double lx, ly, lz;  // line normal vector
//    int dir;
//};
//

using namespace std;
using namespace cv;

#define VERTICAL	0
#define HORIZONTAL	1
#define INFINITE_VALUE 1e10
#define INFINITE_TEST_MIN 1e-10
#define ABOVE_HORIZON	0
#define BELOW_HORIZON	1


//typedef struct
//{
//    double s[2]={0,0}, e[2]={0,0}, m[2]={0,0};	// start, end, and mid point
//	double length=0;                            // line length
//	double angle=0;                             // angle w.r.t. x-axis
//    double line_norm[3]={0,0,0};                // normal vector of a plane passing through the camera center and intersecting
//                                                // the image plane in the line
//    double line_norm_c[3]={0,0,0};              // corrected line nume according to detectec vd. This will be used in sorting lines
//    double line[3]={0,0,0};                     // line vector in homogeneous representation
//	int id=-1;                                  // id
//	int side=-1;                                // 0 or 1: indicator which side of the referece line the line lies
//	int above_below=-1;                         // 0 or 1: indicator whether the line is above or below the horizon
//	double *MSLD = NULL;                        // MSLD descriptor
//	int len_desc=0;                             // length of descriptor
//    double ln[2]={0,0};                         // orthonormal vector to the line direction
//	bool bUse = false;                          // reserved
//    double s3d[3]={0,0,0}, e3d[3]={0,0,0};      // 3D coordinates.
//
//    // keep original values before the line correction
//    double line_norm_orig[3]={0,0,0};
//    double line_orig[3]={0,0,0};
//    double s_orig[2]={0,0}, e_orig[2]={0,0}, m_orig[2]={0,0};
//    
//    // used for biilding a binary linked list
//    int frame_idx = -1;
//    void* child1 = NULL;
//    void* child2 = NULL;
//    void* last = NULL;  
//    int status = ROOT;
//    
//	void clear()
//	{
//        if(MSLD){
//			delete[] MSLD;
//            MSLD = NULL;
//        }
//	};
//}LineFeature;
//
//typedef struct
//{
//	double vd[3];					// vanishing direction
//	double vp[3];					// vanishing point (in homogeneous representation, but normalized (third component is one)).
//	LineFeature** lines;
//	int num_lines;
//	double length_max;				// maximum length of line in this group
//
//    void init(int max_num_lines)
//    {
//        if(max_num_lines)
//            lines = new LineFeature*[max_num_lines];
//        else
//            lines = NULL;
//        
//        num_lines = 0;
//        length_max = 0.f;
//    };
//
//	void computeMaxLength()
//	{
//		length_max = 0;
//		for(int i=0; i<num_lines; i++)
//		{
//			if(length_max > lines[i]->length)
//				length_max = lines[i]->length;
//		}
//	};
//
//	void clear()
//	{
//        if(lines)
//			delete[] lines;
//		lines = NULL;
//	};
//
//}LineCluster;



class CVPDetector
{
public:
	CVPDetector(void);
	~CVPDetector(void);

    void lineDetectionHP(Mat img, int max_num_lines, double* lines, int &num_lines, double min_line_length, double max_line_gap, double scale = 1.f);
    void lineDetectionLSD(double *image, int rows, int cols,
						int max_num_lines, double* lines, int &num_lines, double short_th);
	
    bool lineMerging(double s1[2], double e1[2], double s2[2], double e2[2], double *new_s, double *new_e, double th_gap);
	double *lineMerging(double *lines, int size, int &num_lines, double th_angle, double th_dist, double th_gap);
	void lineMerging(LineFeature *line_features, int num_lines, int &num_new_lines, double th);
	void lineMerging(LineFeature *line_features, int *idx_sel_lines, int num_sel_lines, int idx_store, double th);
    
    double *lineLengthFilter(double *lines, int &num_lines, double th_length);

	vector<LineCluster> findVP(LineFeature* line_features, int num_lines, double x_imu[3], double y_imu[3], double z_imu[3], 
		double e_th, int num_trials, Mat &K, bool bCorrection = true, bool bLM = false);
	
    
    void lineCorrection(LineCluster line_cluster, double Kinv[9], bool bUpdate = true);
		
	void lineCorrection(LineFeature *line_feature, double vd[3], double Kinv[9], bool bUpdate);

	void sortLines(LineCluster line_cluster, double Kinv[9], double ref_normal_vector[3]);

	void aboveBelowHorizon(LineCluster line_cluster, double horizon_n[3], Mat Ktinv);

	double score(LineCluster line_cluster, double vd[3], double e_th);
    double score(LineFeature** line_features, int num_lines, double vd[3], double e_th);
	double score(LineFeature* line_features, int num_lines, double vd[3], double e_th);

	void getLineAxis(LineFeature *line, double* image, int rows, int cols, double lx[2], double ly[2]);
	void getLineAxis2(LineFeature *line, double* image, int rows, int cols, double lx[2], double ly[2]);
	double *MSLD(LineFeature *line, double* image, int rows, int cols, double ln[2],  
							int S = 5, int N = 9, int W = 5, double th = 0.4f);

	double alignError(LineFeature line, double *vd);
    double alignErrorEndPts(LineFeature line, double *vd);
    double alignErrorLineNormal(LineFeature line, double *vd);
	double distance(LineFeature line, double *vd);
	double getLineAngle(double *x1, double *x2);
	void backProjection(double *point, double *direction);
	bool projection(double *direction, double *point, Mat K);

	void lineFeatureComputation(double* lines, int &num_lines, LineFeature *line_features,
								double *image, int rows, int cols);
    
    
    void setK(Mat K);
    Mat getR_c_wFromFile(string f_R_b_a);


    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Non linear optimization
    Mat refine(Mat R_c_w, LineCluster line_cluster_x, LineCluster line_cluster_y, LineCluster line_cluster_z);
    void refine(Mat R_c_w, Mat K, LineCluster line_cluster_x, LineCluster line_cluster_y, LineCluster line_cluster_z, Mat &R_c_w_final, Mat &K_final);
    
   // void computeVdFromRodrigue(Mat rot, double vd_x[3], double vd_y[3], double vd_z[3]);
    
    
    ////////////////////////////////////////////////////////////////////////////////////////////////
	void drawLines(string windowname, Mat img, double *lines, int num_lines, Scalar color);
	void drawLines(string windowname, Mat img, double *lines, int num_lines, int *idx_best_lines, int num_best_lines, Scalar color);
	
	void drawLines(string windowname, Mat img, LineFeature *line, Scalar color, double thickness, bool draw_text = false);
    void drawLines(string windowname, Mat img, LineFeature *lines, int num_lines, Scalar color);
	void drawLines(string windowname, Mat img, LineFeature **lines, int num_lines, Scalar color);
	void drawLines(string windowname, Mat img, LineFeature *line, int width, int height, Scalar color, bool draw_text = false);
	
	void drawLines(string windowname, Mat img, vector<LineCluster> line_clusters, bool draw_text = false);
	void drawLines(string windowname, Mat img, LineCluster line_cluster, Scalar color, bool draw_text = false);
	void drawLines(string windowname, Mat img, LineCluster line_cluster, Mat sorted_idx, Scalar color, bool draw_text = false);
	void drawLines(string windowname, Mat img, LineCluster line_cluster, int width, int height, Scalar color, bool draw_text = false);
	void drawLines(string windowname, Mat img, LineCluster line_cluster, Mat sorted_idx,
																	int width, int height, Scalar color, bool draw_text = false);

    void drawVanishingPoints(string windowname, Mat img, double x[3], double y[3], double z[3], Mat K);
    void drawHorizon(string windowname, Mat img, double vd_v[3], Mat K, int width, int height, Scalar color);
    static void drawAxes(string windowname, Mat img, double x[3], double y[3], double z[3], Mat K);

	void getEndPoints(double line[3], cv::Point &s, cv::Point &e, int width, int height);
	void crossProd(double *a, double *b, double *axb);
	double innerProd(double *a, double *b, int dim);
	void unitVector(double *x, double* y, int dim);
    void orthonormalVector(double *x, double *y);
    void undistortCoord(double* x, double *undistorted_x);
    Mat comp_distortion_oulu(Mat xd, Mat distCoeffs);
    Mat normalize(Mat x_kk, Mat K, Mat distCoeffs);
	void normalizeLine(double *line);
    void findClosestPointOnALine(double line[3], double m[2], double pt[2], double pt_dest[2]);

	double *colorMatToGrayDouble(Mat img, Mat &gray_img);
	uchar *colorMatToGrayUchar(Mat img, Mat &gray_img);

	bool areTheyOverlapped(LineFeature *line1, LineFeature *line2);
	double areTheyOverlapped(double line1[4], double line2[4], double s[2], double e[2]);

    void mul(double *A, double *b, double *c, int m, int n);
	void mul(Mat M, double a[3], double b[3]);

	Mat m_K;				// camera intrinsic matrix
    Mat m_K_img1, m_K_img2; // for refined K for each image
	Mat m_disto_coeff;		// Distortion coefficient
	Mat m_map1, m_map2;
	Mat m_Kinv, m_Ktinv;
    Mat m_Kinv_img1, m_Kinv_img2, m_Ktinv_img1, m_Ktinv_img2;

	double md_K[9], md_Kinv[9], md_Kt[9], md_Ktinv[9];
	

#ifndef IOS
    
	CUtility m_util;
#endif
};

