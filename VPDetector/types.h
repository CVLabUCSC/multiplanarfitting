#ifndef _TYPES_
#define _TYPES_

#include <stdio.h>
#include <cstring>


#define ROOT 0
#define CHILD 1
#define ABOVE_HORIZON 0
#define BELOW_HORIZON 1

struct LineFeature
{
    double s[2], e[2], m[2];	// start, end, and mid point
    double length;              // line length
    double angl;                // angle w.r.t. x-axis
    double line_norm[3];        // normal vector of a plane passing through the camera center and intersecting
                                // the image plane in the line
    
    double line[3];             // line vector in homogeneous representation
    int id;                     // id
    int side;                   // 0 or 1: indicator which side of the referece line the line lies
    int above_below;            // 0 or 1: indicator whether the line is above or below the horizon
    double *MSLD;               // MSLD descriptor
    int len_desc;               // length of descriptor
    double ln[2];               // orthonormal vector to the line direction
    bool bUse;                  // reserved
    double s3d[3], e3d[3];      // 3D coordinates.
    
    // keep original values before the line correction
    double line_norm_orig[3];
    double line_orig[3];
    double s_orig[2], e_orig[2], m_orig[2];
    
    // used for biilding a binary linked list
    int frame_idx;
    void* child1;
    void* child2;
    void* last;
    int status;

	LineFeature(): length(0.0), angl(0.0), id(-1), side(-1), above_below(-1), MSLD(NULL), len_desc(0), bUse(false), frame_idx(-1), child1(NULL), child2(NULL), last(NULL), status(ROOT)
	{
		s[0] = s[1] = 0.0;
		e[0] = e[1] = 0.0;
		m[0] = m[1] = 0.0;
		line_norm[0] = line_norm[1] = line_norm[2] = 0.0;
		line[0] = line[1] = line[2] = 0.0;
		ln[0] = ln[1] = 0.0;
		s3d[0] = s3d[1] = s3d[2] = 0.0;
		e3d[0] = e3d[1] = e3d[2] = 0.0;
		line_norm_orig[0] = line_norm_orig[1] = line_norm_orig[2] = 0.0;
		line_orig[0] = line_orig[1] = line_orig[2] = 0.0;
		s_orig[0] = s_orig[1] = 0.0;
		e_orig[0] = e_orig[1] = 0.0;
		m_orig[0] = m_orig[1] = 0.0;
	};
	    
    void clear()
    {
        if(MSLD){
            delete[] MSLD;
            MSLD = NULL;
        }
    };
};

struct LineCluster
{
    double vd[3];					// vanishing direction
    double vp[3];					// vanishing point (in homogeneous representation, but normalized (third component is one)).
    LineFeature** lines;
    int num_lines;
    double length_max;				// maximum length of line in this group
    
    void init(int max_num_lines)
    {
        if(max_num_lines)
            lines = new LineFeature*[max_num_lines];
        else
            lines = NULL;
        
        num_lines = 0;
        length_max = 0.f;
    };
    
    void computeMaxLength()
    {
        length_max = 0;
        for(int i=0; i<num_lines; i++)
        {
            if(length_max > lines[i]->length)
                length_max = lines[i]->length;
        }
    };
    
    void clear()
    {
        if(lines)
            delete[] lines;
        lines = NULL;
        num_lines = 0;
    };
    
};

struct Match
{
    int frame_idx[2];   // frame number;
    int idx1;			// line index in image i
    int idx2;			// line index in image i+1
    double similarity;
    int label;     // used for debugging.

	Match(): label(-1) {};
};


struct LineID
{
    int frame_idx;   // index of frame
    int line_idx;    // index of line feature
    LineFeature* addr;
};


struct TD
{
    LineFeature* lfs1[4];
    LineFeature* lfs2[4];
    double td[3];

	TD()
	{
		lfs1[0] = lfs1[1] = lfs1[2] = lfs1[3] = NULL;
		lfs2[0] = lfs2[1] = lfs2[2] = lfs2[3] = NULL;
		td[0] = td[1] = td[2] = 0.0;
	};
};

struct JTD
{
    TD *tds;
    int num_tds;
    double *td_array;

	JTD(): tds(NULL), num_tds(0), td_array(NULL) {};
    
    void init(int num_tds_){
        tds = new TD[num_tds_];        
        td_array = new double[num_tds_*3];
        memset(td_array, 0, sizeof(double)*num_tds_*3);
        num_tds = num_tds_;
    }
    
    void clear()
    {
        if(tds)
            delete[] tds;
        tds = NULL;
        if(td_array)
            delete[] td_array;
        td_array = NULL;
        num_tds = 0;
    }    
};


#endif // !_TYPES_
