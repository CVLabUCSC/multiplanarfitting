#pragma once

#include <opencv2/opencv.hpp>



using namespace cv;
using namespace std;
#define INFINITE_TEST_MIN 1e-10

class CUtility
{
public:
	CUtility(void);
	~CUtility(void);

	void writeData(const char *filename, float *data, int rows, int cols);
	void writeData(const char *filename, double *data, int rows, int cols);
	void writeData(const char *filename, int *data, int rows, int cols);
	void writeData(const char *filename, double **data, int rows, int cols);
	void writeData(const char *filename, Mat data);
	void writeDataAppend(const char *filename, Mat data);
	void writeDataF(const char *filename, Mat data);
	void writeData(const char *filename, vector<Point3f> data);
	void writeData(const char *filename, vector<Point3d> data);
	void writeData(const char *filename, vector<int> data);
	void writeData(const char *filename, int data);
	void writeData(const char *filename, vector<vector<uchar> > data);
	void writeData(const char *filename, double *x, double *y, double *z, int num_data);

	void printData(Mat data);
    void printData(double *data, int rows, int cols);
	
	
	bool readData(const char *filename, vector<int> &data);
    
    
    
    ///////////////////////
    void	mul(Mat M, double a[3], double b[3]);
	double	innerProd(double *a, double *b, int dim);
    void	crossProd(double *a, double *b, double *axb);
	Point3d crossProd(Point3d a, Point3d b);
    void	unitVector(double *x, double* y, int dim);
	void	unitize(double a1, double b1, double &a, double &b);
	double	EcDistance(double *a, double *b, int dim);
    Mat     cov(double *A, int num_data, int dim);
    void findClosestPointOnALine(double line[3], double m[2], double pt[2], double pt_dest[2]);
    void orthonormalVector(double *x, double *y);
    double distPoint2Line(double line[3], double p[2]);
    void perpendicularVector(double a[2], double b[2]);
    void normalizeLine(double *line);
    double* Mat2Double(Mat img);
};

