#include "Proc.h"
#include "lsd.h"

CProc::CProc(void)
{
}


CProc::~CProc(void)
{
    
}

double *CProc::lineDetection(double* img_d, int rows, int cols, int &num_lines, double th_dist_merge, double th_gap_merge, int th_short)
{
    // LSD detector
    double *out_lsd;
    out_lsd = lsd(&num_lines, img_d, cols, rows);
    
    double *lines = new double[num_lines*4];       // lsd_out has 7 paramters for each detected line segment
    for(int i=0; i<num_lines; i++)
        memcpy(&lines[4*i], &out_lsd[7*i], sizeof(double)*4);
    delete[] out_lsd;
    
    
    
    // remove short line segments
    int cnt = 0;
    int th_short2 = th_short*th_short;
    for(int i=0; i<num_lines; i++)
    {
        double length = ( (lines[i*4  ]-lines[i*4+2])*(lines[i*4  ]-lines[i*4+2]) +
                  (lines[i*4+1]-lines[i*4+3])*(lines[i*4+1]-lines[i*4+3]));
        
        if(length > th_short2){
            memcpy(&lines[cnt*4], &lines[i*4], sizeof(double)*4);
            cnt++;
        }
    }
    num_lines = cnt;
    
    // Line merging
    lines = lineMerging(lines, num_lines, th_dist_merge, th_gap_merge);
    
    
    return lines;
}


double* CProc::lineMerging(double *lines, int &num_lines, double th_dist, double th_gap)
{
    int *idx_del;   // which line will be deleted.
    idx_del = new int[num_lines];
    memset(idx_del, 0, sizeof(int)*num_lines);
    
    while(1)
    {
        int num_merge = 0;
        for(int i=0; i<num_lines; i++)
        {
            if(idx_del[i] == 1)
                continue;
            for(int j=0; j<num_lines; j++)
            {
                if(i==j || idx_del[j] == 1) // check if j-th line is already merged
                    continue;
                
                double s[3] = {0,0,1}, e[3] = {0,0,1};      // in homogeneous representation
                double gap = lineMerging(&lines[i*4], &lines[i*4+2], &lines[j*4], &lines[j*4+2], s, e);
                
                double lc[3];   // compute line coefficient (line in homogenous representation)
                m_util.crossProd(s, e, lc);
                
                double dist;
                dist  = m_util.distPoint2Line(lc, &lines[i*4]);
                dist += m_util.distPoint2Line(lc, &lines[i*4+2]);
                dist += m_util.distPoint2Line(lc, &lines[j*4]);
                dist += m_util.distPoint2Line(lc, &lines[j*4+2]);
                dist *= 0.25;
                
                if(dist < th_dist && gap < th_gap){
                    lines[4*i  ] = s[0];        // overwrite the i-th line coordinates
                    lines[4*i+1] = s[1];
                    lines[4*i+2] = e[0];
                    lines[4*i+3] = e[1];
                    num_merge++;
                    
                    idx_del[j] = 1;             // want to delete j-th line
                }
            }
        }
        if(num_merge == 0)
            break;
    }
    
    // create new line data
    // count new num_lines;
    int new_num_lines = 0;
    for(int i=0; i<num_lines; i++){
        if(idx_del[i] != 1)
            new_num_lines++;
    }
    double *new_lines = new double[4*new_num_lines];
    new_num_lines = 0;
    for(int i=0; i<num_lines; i++){
        if(idx_del[i] != 1){
            memcpy(&new_lines[4*new_num_lines], &lines[4*i], sizeof(double)*4);
            new_num_lines++;
        }
    }
    delete[] lines;
    delete[] idx_del;
    
    num_lines = new_num_lines;
    return new_lines;
}

// line merging using the algorithm in Paper 1995 JMRS Manuel A New Approach for Merging Edge Line Segments
// return: distance between the projected extrem points of two line segment onto the merged line -
//         total length of projected two line segment onto the merged line. If the value is negative,
//         then the two line segments were overlapped each other.
double CProc::lineMerging(double s1[2], double e1[2], double s2[2], double e2[2], double *new_s, double *new_e)
{
    // Define the coordinates of the centroid
    double l1[2] = {e1[0] - s1[0], e1[1] - s1[1]};
    double l2[2] = {e2[0] - s2[0], e2[1] - s2[1]};
    
    double len1 = sqrt( l1[0]*l1[0] + l1[1]*l1[1] );
    double len2 = sqrt( l2[0]*l2[0] + l2[1]*l2[1] );
    
    double g[2];
    double w1 = len1 / (len1 + len2);
    double w2 = len2 / (len1 + len2);
    g[0] = w1*(s1[0] + e1[0]) + w2*(s2[0] + e2[0]);     g[0] *= 0.5;
    g[1] = w1*(s1[1] + e1[1]) + w2*(s2[1] + e2[1]);     g[1] *= 0.5;
    
    // Define the orientation of the merged line as the weighted sum of the orientations of the given segments
    if( m_util.innerProd(l1, l2, 2) < 0){  // adjust line directions
        l2[0] *= -1;
        l2[1] *= -1;
    }
    double rad = w1*atan2(l1[1], l1[0]) + w2*atan2(l2[1], l2[0]);
    double v[2] = {cos(rad), sin(rad)};
    
    vector<double> p;
    p.push_back( (s1[0]-g[0])*v[0] + (s1[1]-g[1])*v[1] );
    p.push_back( (e1[0]-g[0])*v[0] + (e1[1]-g[1])*v[1] );
    p.push_back( (s2[0]-g[0])*v[0] + (s2[1]-g[1])*v[1] );
    p.push_back( (e2[0]-g[0])*v[0] + (e2[1]-g[1])*v[1] );
    
    vector<int> idx;
    sortIdx(p, idx, SORT_ASCENDING);
    double *addr[4] = {s1, e1, s2, e2};
    
    memcpy(new_s, addr[idx[0]], sizeof(double)*2);
    memcpy(new_e, addr[idx[3]], sizeof(double)*2);
    
    return (p[idx[3]] - p[idx[0]]) - (fabs(p[0]-p[1]) + fabs(p[2]-p[3]));
}



/*----------------------------------------------------------------------------*/
/** alignError: deviation angle between the detected line
 and a line joining its midpoint and the vanishing point
 
 @param line				line feature
 @param vd				vanishing direction, 3x1 vector
 */
double CProc::alignError(LineFeature line, double *vd, Mat K)
{
    // line a: s ~ e
    // compute vanishing point K*vd
    double vp[3];
    vp[0] = K.at<double>(0,0)*vd[0] + K.at<double>(0,1)*vd[1] + K.at<double>(0,2)*vd[2];
    vp[1] = K.at<double>(1,0)*vd[0] + K.at<double>(1,1)*vd[1] + K.at<double>(1,2)*vd[2];
    vp[2] = K.at<double>(2,0)*vd[0] + K.at<double>(2,1)*vd[1] + K.at<double>(2,2)*vd[2];
    
    
    double m[3] = {line.m[0], line.m[1], 1.0};
    double l[3];
    m_util.crossProd(vp, m, l);
    
    return m_util.distPoint2Line(l, line.s);
}

void CProc::sortLines(LineCluster line_cluster, Mat Kinv, double ref_normal_vector[3])
{
    Mat angles_mat, sorted_idx, ref_line_;
    double *angles, orth_vector[3], ref_line[3], signed_dist;
    
    if(line_cluster.num_lines == 0)
        return;
    
    angles = new double[line_cluster.num_lines];
    
    m_util.crossProd(line_cluster.vd, ref_normal_vector, orth_vector);
    m_util.unitVector(orth_vector, orth_vector, 3);
    
    
    // reference_line
    double *prt = Kinv.ptr<double>();

    // ref_line = Kinv^T * ref_normla_vector;
    ref_line[0] = prt[0]*ref_normal_vector[0] + prt[3]*ref_normal_vector[1] + prt[6]*ref_normal_vector[2];
    ref_line[1] = prt[1]*ref_normal_vector[0] + prt[4]*ref_normal_vector[1] + prt[7]*ref_normal_vector[2];
    ref_line[2] = prt[2]*ref_normal_vector[0] + prt[5]*ref_normal_vector[1] + prt[8]*ref_normal_vector[2];
    
    
    for(int i=0; i<line_cluster.num_lines; i++)
    {
        // correct sign of line normal vector
        double *line_norm = line_cluster.lines[i]->line_norm;
        if( m_util.innerProd(orth_vector, line_norm, 3) < 0)
        {
            line_norm[0] = -line_norm[0];
            line_norm[1] = -line_norm[1];
            line_norm[2] = -line_norm[2];
        }
        angles[i] = m_util.innerProd(ref_normal_vector, line_norm, 3);
    }
    
    angles_mat = Mat(1, line_cluster.num_lines, CV_64F, angles).clone();
    sortIdx(angles_mat, sorted_idx, CV_SORT_ASCENDING);
    
    // reassign the id of each line according to the angle of the line w.r.t the reference normal
    // and assign the side of each line according to the signed distance of its mid point to the ref line
    for(int i=0; i<line_cluster.num_lines; i++)
    {
        int idx = sorted_idx.at<int>(i);
        line_cluster.lines[idx]->id = i;
        signed_dist = ref_line[0] * line_cluster.lines[idx]->m[0] +
        ref_line[1] * line_cluster.lines[idx]->m[1] +
        ref_line[2];
        if(signed_dist < 0)
            line_cluster.lines[idx]->side = 0;
        else
            line_cluster.lines[idx]->side = 1;	
    }
    
    delete[] angles;
}

// horizon_n: normal vector of horizon = z-vanishing direction
void CProc::aboveBelowHorizon(LineCluster line_cluster, double vd_z[3], Mat Kinv)
{
    double signed_dist, horizon[3];
    
    double *ptr = Kinv.ptr<double>();
    // horizon = K^{-T} vd_z;
    horizon[0] = ptr[0]*vd_z[0] + ptr[3]*vd_z[1] + ptr[6]*vd_z[2];
    horizon[1] = ptr[1]*vd_z[0] + ptr[4]*vd_z[1] + ptr[7]*vd_z[2];
    horizon[2] = ptr[2]*vd_z[0] + ptr[5]*vd_z[1] + ptr[8]*vd_z[2];
    m_util.normalizeLine(horizon);
    
    for(int i=0; i<line_cluster.num_lines; i++)
    {
        signed_dist = horizon[0] * line_cluster.lines[i]->m[0] + horizon[1] * line_cluster.lines[i]->m[1] + horizon[2];
        if(signed_dist > 0)
        {
            line_cluster.lines[i]->above_below = ABOVE_HORIZON;
        }
        else
        {
            line_cluster.lines[i]->above_below = BELOW_HORIZON;
        }
    }
}

vector<LineCluster> CProc::LineClusteringByVP(LineFeature* line_features, int num_lines, Mat &R_c_w,
                                        double e_th, int num_trials, Mat &K, bool bRefine, bool bSort)
{
    
    int num_xy = 0, num_z = 0;
    int *idx_xy = new int[num_lines]; // this will contain indcies of line features associated to vanishing point vx or vy
    int *idx_z = new int[num_lines];  // this will contain indcies of line features associated to vanishing point vz
    
    // First divide the lines into two groups associated to vd_z and others.
    double vd_x_init[3] = {R_c_w.at<double>(0,0), R_c_w.at<double>(1,0), R_c_w.at<double>(2,0)}; // initial of vanishing directions
//    double vd_y_init[3] = {R_c_w.at<double>(0,1), R_c_w.at<double>(1,1), R_c_w.at<double>(2,1)};
    double vd_z_init[3] = {R_c_w.at<double>(0,2), R_c_w.at<double>(1,2), R_c_w.at<double>(2,2)};
    
    num_xy = 0;
    num_z = 0;
    for(int i=0; i<num_lines; i++){
        double e = alignError(line_features[i], vd_z_init, K);
        if(e < e_th){
            idx_z[num_z] = i;
            num_z++;
        }
        else{
            idx_xy[num_xy] = i; // keep indice of horizontal lines in idx_xy
            num_xy++;
        }
    }
    
    RNG rng(12345);
    double vd_x[3], vd_y[3];
    double vd_z[3] = {vd_z_init[0], vd_z_init[1], vd_z_init[2]};    // we use vd_z_init from the inertial sensor
    double vd_x_b[3], vd_y_b[3];    // temporary used for storing best results
    double max_s = 0;
    for(int i=0; i<num_trials; i++)
    {
        // randomly select a horizontal line
        int rand_idx = rng.uniform(0, num_xy);
        m_util.crossProd(line_features[idx_xy[rand_idx]].line_norm, vd_z, vd_x);
        m_util.unitVector(vd_x, vd_x, 3);
        
        m_util.crossProd(vd_z, vd_x, vd_y);
        m_util.unitVector(vd_y, vd_y, 3);
        
        double score = 0;
        for(int j=0; j<num_xy; j++){
            double e_x = alignError(line_features[idx_xy[j]], vd_x, K);
            double e_y = alignError(line_features[idx_xy[j]], vd_y, K);
            
            if(e_x < e_th)
                score += line_features[idx_xy[j]].length*(1 - e_x/e_th);
            if(e_y < e_th)
                score += line_features[idx_xy[j]].length*(1 - e_y/e_th);
        }
        
        if(max_s < score){
            max_s = score;
            memcpy(vd_x_b, vd_x, sizeof(double)*3);
            memcpy(vd_y_b, vd_y, sizeof(double)*3);
        }
    }
//    cout << "MAX_SCORE: " << max_s << endl;
    
    // adjust directions of vd to be along initial directions (R_c_w);
    double ip1 = m_util.innerProd(vd_x_b, vd_x_init, 3);
    double ip2 = m_util.innerProd(vd_y_b, vd_x_init, 3);
    if(fabs(ip1) > fabs(ip2)){  // if vd_x_init is close to vd_x_b
        if(ip1 < 0){
            vd_x_b[0] *= -1;
            vd_x_b[1] *= -1;
            vd_x_b[2] *= -1;
        }
    }
    else{                                         // if vd_x_init is close to vd_y_b
        memcpy(vd_x_b, vd_y_b, sizeof(double)*3); // then vd_x_b <- vd_y_b
        if(ip2 < 0){
            vd_x_b[0] *= -1;
            vd_x_b[1] *= -1;
            vd_x_b[2] *= -1;
        }
    }
    // following right-hand law
    m_util.crossProd(vd_z, vd_x_b, vd_y_b);
    m_util.unitVector(vd_y_b, vd_y_b, 3);
    
    memcpy(vd_x, vd_x_b, sizeof(double)*3);
    memcpy(vd_y, vd_y_b, sizeof(double)*3);
    
    // Grouping
    LineCluster lc_x, lc_y, lc_z;
    lc_z.init(num_z);
    for(int i=0; i<num_z; i++){
        lc_z.lines[i] = &line_features[idx_z[i]];
        lc_z.num_lines++;
    }
    
    lc_x.init(num_xy);
    lc_y.init(num_xy);
    for(int i=0; i<num_xy; i++){
        double e_x = alignError(line_features[idx_xy[i]], vd_x, K);
        double e_y = alignError(line_features[idx_xy[i]], vd_y, K);
        if(e_x < e_th && e_y < e_th){
            if(e_x < e_y){
                lc_x.lines[lc_x.num_lines] = &line_features[idx_xy[i]];
                lc_x.num_lines++;
            }
            else{
                lc_y.lines[lc_y.num_lines] = &line_features[idx_xy[i]];
                lc_y.num_lines++;
            }
        }
        else if(e_x < e_th){
            lc_x.lines[lc_x.num_lines] = &line_features[idx_xy[i]];
            lc_x.num_lines++;
        }
        else if(e_y < e_th){
            lc_y.lines[lc_y.num_lines] = &line_features[idx_xy[i]];
            lc_y.num_lines++;
        }
    }
    delete[] idx_xy;
    delete[] idx_z;
    
    memcpy(lc_x.vd, vd_x, sizeof(double)*3);
    memcpy(lc_y.vd, vd_y, sizeof(double)*3);
    memcpy(lc_z.vd, vd_z, sizeof(double)*3);
    
    double *ptr = R_c_w.ptr<double>();
    ptr[0] = lc_x.vd[0];    ptr[1] = lc_y.vd[0];    ptr[2] = lc_z.vd[0];
    ptr[3] = lc_x.vd[1];    ptr[4] = lc_y.vd[1];    ptr[5] = lc_z.vd[1];
    ptr[6] = lc_x.vd[2];    ptr[7] = lc_y.vd[2];    ptr[8] = lc_z.vd[2];
   
    // Refine K and vd_x, vd_y, vd_z
   if(bRefine)
   {
        refine(R_c_w, K, lc_x, lc_y, lc_z);
        
        // update line normal vectors = K^T * line
        ptr = K.ptr<double>();
        for(int i=0; i<num_lines; i++){
            line_features[i].line_norm[0] = ptr[0]*line_features[i].line[0] + ptr[3]*line_features[i].line[1] + ptr[6]*line_features[i].line[2];
            line_features[i].line_norm[1] = ptr[1]*line_features[i].line[0] + ptr[4]*line_features[i].line[1] + ptr[7]*line_features[i].line[2];
            line_features[i].line_norm[2] = ptr[2]*line_features[i].line[0] + ptr[5]*line_features[i].line[1] + ptr[8]*line_features[i].line[2];
            m_util.unitVector(line_features[i].line_norm, line_features[i].line_norm, 3);
            
            memcpy(line_features[i].line_norm_orig, line_features[i].line_norm, sizeof(double)*3);
        }
        
        // regrouping
        lc_x.clear();
        lc_y.clear();
        lc_z.clear();
        
        lc_x.init(num_lines);
        lc_y.init(num_lines);
        lc_z.init(num_lines);
        
        // update vanishing directions
        ptr = R_c_w.ptr<double>();
        vd_x[0] = ptr[0];   vd_y[0] = ptr[1];   vd_z[0] = ptr[2];
        vd_x[1] = ptr[3];   vd_y[1] = ptr[4];   vd_z[1] = ptr[5];
        vd_x[2] = ptr[6];   vd_y[2] = ptr[7];   vd_z[2] = ptr[8];
        memcpy(lc_x.vd, vd_x, sizeof(double)*3);
        memcpy(lc_y.vd, vd_y, sizeof(double)*3);
        memcpy(lc_z.vd, vd_z, sizeof(double)*3);
        
        for(int i=0; i<num_lines; i++){
            vector<double> e;
            e.push_back( alignError(line_features[i], vd_x, K) );
            e.push_back( alignError(line_features[i], vd_y, K) );
            e.push_back( alignError(line_features[i], vd_z, K) );
            
            vector<int> idx;
            sortIdx(e, idx, SORT_ASCENDING);
            if(e[idx[0]] < e_th)
            {
                switch(idx[0]){
                    case 0:
                        lc_x.lines[lc_x.num_lines] = &line_features[i];
                        lc_x.num_lines++;
                        break;
                    case 1:
                        lc_y.lines[lc_y.num_lines] = &line_features[i];
                        lc_y.num_lines++;
                        break;
                    case 2:
                        lc_z.lines[lc_z.num_lines] = &line_features[i];
                        lc_z.num_lines++;
                        break;
                }
            }
        }
    }


    // sorting lines
    Mat Kinv = K.inv();
    if(bSort){
        sortLines(lc_z, Kinv, lc_y.vd);
        sortLines(lc_x, Kinv, lc_y.vd);
        sortLines(lc_y, Kinv, lc_x.vd);
    }
    aboveBelowHorizon(lc_x, lc_z.vd, Kinv);
    aboveBelowHorizon(lc_y, lc_z.vd, Kinv);
    
    // compute vanishing point
    ptr = K.ptr<double>();
    lc_x.vp[0] = ptr[0]*lc_x.vd[0] + ptr[1]*lc_x.vd[1] + ptr[2]*lc_x.vd[2];
    lc_x.vp[1] = ptr[3]*lc_x.vd[0] + ptr[4]*lc_x.vd[1] + ptr[5]*lc_x.vd[2];
    lc_x.vp[2] = ptr[6]*lc_x.vd[0] + ptr[7]*lc_x.vd[1] + ptr[8]*lc_x.vd[2];
    if(fabs(lc_x.vp[2]) > 1e-15){
        lc_x.vp[0] /= lc_x.vp[2];
        lc_x.vp[1] /= lc_x.vp[2];
    }
    
    lc_y.vp[0] = ptr[0]*lc_y.vd[0] + ptr[1]*lc_y.vd[1] + ptr[2]*lc_y.vd[2];
    lc_y.vp[1] = ptr[3]*lc_y.vd[0] + ptr[4]*lc_y.vd[1] + ptr[5]*lc_y.vd[2];
    lc_y.vp[2] = ptr[6]*lc_y.vd[0] + ptr[7]*lc_y.vd[1] + ptr[8]*lc_y.vd[2];
    if(fabs(lc_y.vp[2]) > 1e-15){
        lc_y.vp[0] /= lc_y.vp[2];
        lc_y.vp[1] /= lc_y.vp[2];
    }

    lc_z.vp[0] = ptr[0]*lc_z.vd[0] + ptr[1]*lc_z.vd[1] + ptr[2]*lc_z.vd[2];
    lc_z.vp[1] = ptr[3]*lc_z.vd[0] + ptr[4]*lc_z.vd[1] + ptr[5]*lc_z.vd[2];
    lc_z.vp[2] = ptr[6]*lc_z.vd[0] + ptr[7]*lc_z.vd[1] + ptr[8]*lc_z.vd[2];
    if(fabs(lc_z.vp[2]) > 1e-15){
        lc_z.vp[0] /= lc_z.vp[2];
        lc_z.vp[1] /= lc_z.vp[2];
    }
    
    vector<LineCluster> lcs;
    lcs.push_back(lc_x);
    lcs.push_back(lc_y);
    lcs.push_back(lc_z);
    
    return lcs;
}

void CProc::updateWithNewK(vector<vector<LineCluster>> &lcs_list, vector<LineFeature*> lfs_list, vector<int> num_lines_list, vector<Mat> &R_c_ws, Mat K, double e_th, bool bSort)
{
    int N = int(lcs_list.size());
    Mat Kinv = K.inv();
    for(int i=0; i<N; i++){
        // update line normal vectors = K^T * line
        double *ptr = K.ptr<double>();
        for(int j=0; j<num_lines_list[i]; j++){
            lfs_list[i][j].line_norm[0] = ptr[0]*lfs_list[i][j].line[0] + ptr[3]*lfs_list[i][j].line[1] + ptr[6]*lfs_list[i][j].line[2];
            lfs_list[i][j].line_norm[1] = ptr[1]*lfs_list[i][j].line[0] + ptr[4]*lfs_list[i][j].line[1] + ptr[7]*lfs_list[i][j].line[2];
            lfs_list[i][j].line_norm[2] = ptr[2]*lfs_list[i][j].line[0] + ptr[5]*lfs_list[i][j].line[1] + ptr[8]*lfs_list[i][j].line[2];
            m_util.unitVector(lfs_list[i][j].line_norm, lfs_list[i][j].line_norm, 3);
            memcpy(lfs_list[i][j].line_norm_orig, lfs_list[i][j].line_norm, sizeof(double)*3);
        }
        
        // regrouping
        double vd_x[3], vd_y[3], vd_z[3];
        lcs_list[i][0].clear();
        lcs_list[i][1].clear();
        lcs_list[i][2].clear();
        lcs_list[i][0].init(num_lines_list[i]);
        lcs_list[i][1].init(num_lines_list[i]);
        lcs_list[i][2].init(num_lines_list[i]);
        
        // update vanishing directions
        ptr = R_c_ws[i].ptr<double>();
        vd_x[0] = ptr[0];   vd_y[0] = ptr[1];   vd_z[0] = ptr[2];
        vd_x[1] = ptr[3];   vd_y[1] = ptr[4];   vd_z[1] = ptr[5];
        vd_x[2] = ptr[6];   vd_y[2] = ptr[7];   vd_z[2] = ptr[8];
        memcpy(lcs_list[i][0].vd, vd_x, sizeof(double)*3);
        memcpy(lcs_list[i][1].vd, vd_y, sizeof(double)*3);
        memcpy(lcs_list[i][2].vd, vd_z, sizeof(double)*3);
        
        for(int j=0; j<num_lines_list[i]; j++){
            vector<double> e;
            e.push_back( alignError(lfs_list[i][j], vd_x, K) );
            e.push_back( alignError(lfs_list[i][j], vd_y, K) );
            e.push_back( alignError(lfs_list[i][j], vd_z, K) );
            
            vector<int> idx;
            sortIdx(e, idx, SORT_ASCENDING);
            if(e[idx[0]] < e_th)
            {
                switch(idx[0]){
                    case 0:
                        lcs_list[i][0].lines[lcs_list[i][0].num_lines] = &lfs_list[i][j];
                        lcs_list[i][0].num_lines++;
                        break;
                    case 1:
                        lcs_list[i][1].lines[lcs_list[i][1].num_lines] = &lfs_list[i][j];
                        lcs_list[i][1].num_lines++;
                        break;
                    case 2:
                        lcs_list[i][2].lines[lcs_list[i][2].num_lines] = &lfs_list[i][j];
                        lcs_list[i][2].num_lines++;
                        break;
                }
            }
        }
        
        // sorting lines        
        if(bSort){
            sortLines(lcs_list[i][2], Kinv, lcs_list[i][1].vd);
            sortLines(lcs_list[i][0], Kinv, lcs_list[i][1].vd);
            sortLines(lcs_list[i][1], Kinv, lcs_list[i][0].vd);
        }
        aboveBelowHorizon(lcs_list[i][0], lcs_list[i][2].vd, Kinv);
        aboveBelowHorizon(lcs_list[i][1], lcs_list[i][2].vd, Kinv);
        
        // compute vanishing point
        ptr = K.ptr<double>();
        lcs_list[i][0].vp[0] = ptr[0]*lcs_list[i][0].vd[0] + ptr[1]*lcs_list[i][0].vd[1] + ptr[2]*lcs_list[i][0].vd[2];
        lcs_list[i][0].vp[1] = ptr[3]*lcs_list[i][0].vd[0] + ptr[4]*lcs_list[i][0].vd[1] + ptr[5]*lcs_list[i][0].vd[2];
        lcs_list[i][0].vp[2] = ptr[6]*lcs_list[i][0].vd[0] + ptr[7]*lcs_list[i][0].vd[1] + ptr[8]*lcs_list[i][0].vd[2];
        if(fabs(lcs_list[i][0].vp[2]) > 1e-15){
            lcs_list[i][0].vp[0] /= lcs_list[i][0].vp[2];
            lcs_list[i][0].vp[1] /= lcs_list[i][0].vp[2];
        }
        
        lcs_list[i][1].vp[0] = ptr[0]*lcs_list[i][1].vd[0] + ptr[1]*lcs_list[i][1].vd[1] + ptr[2]*lcs_list[i][1].vd[2];
        lcs_list[i][1].vp[1] = ptr[3]*lcs_list[i][1].vd[0] + ptr[4]*lcs_list[i][1].vd[1] + ptr[5]*lcs_list[i][1].vd[2];
        lcs_list[i][1].vp[2] = ptr[6]*lcs_list[i][1].vd[0] + ptr[7]*lcs_list[i][1].vd[1] + ptr[8]*lcs_list[i][1].vd[2];
        if(fabs(lcs_list[i][1].vp[2]) > 1e-15){
            lcs_list[i][1].vp[0] /= lcs_list[i][1].vp[2];
            lcs_list[i][1].vp[1] /= lcs_list[i][1].vp[2];
        }
        
        lcs_list[i][2].vp[0] = ptr[0]*lcs_list[i][2].vd[0] + ptr[1]*lcs_list[i][2].vd[1] + ptr[2]*lcs_list[i][2].vd[2];
        lcs_list[i][2].vp[1] = ptr[3]*lcs_list[i][2].vd[0] + ptr[4]*lcs_list[i][2].vd[1] + ptr[5]*lcs_list[i][2].vd[2];
        lcs_list[i][2].vp[2] = ptr[6]*lcs_list[i][2].vd[0] + ptr[7]*lcs_list[i][2].vd[1] + ptr[8]*lcs_list[i][2].vd[2];
        if(fabs(lcs_list[i][2].vp[2]) > 1e-15){
            lcs_list[i][2].vp[0] /= lcs_list[i][2].vp[2];
            lcs_list[i][2].vp[1] /= lcs_list[i][2].vp[2];
        }
    }
}

void CProc::refine(Mat &R_c_w, Mat &K, LineCluster lc_x, LineCluster lc_y, LineCluster lc_z)
{
    Mat rot_m;
    Rodrigues(R_c_w, rot_m);
    
    double x[3] = {rot_m.at<double>(0), rot_m.at<double>(1), rot_m.at<double>(2)};
    double f = K.at<double>(0,0);
    
    Problem problem;
    for (int i = 0; i < lc_x.num_lines; ++i) {
        ceres::CostFunction* cost_function =
        Residual::Create(lc_x.lines[i]->line[0], lc_x.lines[i]->line[1], lc_x.lines[i]->line[2], K.at<double>(0,2), K.at<double>(1,2), 0);
        problem.AddResidualBlock(cost_function, new ceres::CauchyLoss(0.5), x, &f);
    }
    
    for (int i = 0; i < lc_y.num_lines; ++i) {
        ceres::CostFunction* cost_function =
        Residual::Create(lc_y.lines[i]->line[0], lc_y.lines[i]->line[1], lc_y.lines[i]->line[2], K.at<double>(0,2), K.at<double>(1,2), 1);
        problem.AddResidualBlock(cost_function, new ceres::CauchyLoss(0.5), x, &f);
    }
    
    for (int i = 0; i < lc_z.num_lines; ++i) {
        ceres::CostFunction* cost_function =
        Residual::Create(lc_z.lines[i]->line[0], lc_z.lines[i]->line[1], lc_z.lines[i]->line[2], K.at<double>(0,2), K.at<double>(1,2), 2);
        problem.AddResidualBlock(cost_function, new ceres::CauchyLoss(0.5), x, &f);
    }
    
    ceres::Solver::Options options;
    options.linear_solver_type = ceres::DENSE_SCHUR;//DENSE_SCHUR; //DENSE_QR
    options.minimizer_progress_to_stdout = false;
    ceres::Solver::Summary summary;
    ceres::Solve(options, &problem, &summary);
//    std::cout << summary.FullReport() << "\n";
    
    rot_m.at<double>(0) = x[0];
    rot_m.at<double>(1) = x[1];
    rot_m.at<double>(2) = x[2];
    
    Rodrigues(rot_m, R_c_w);
    K.at<double>(0,0) = f;
    K.at<double>(1,1) = f;
}

void CProc::refine2(Mat &R_c_w, Mat &K, LineCluster lc_x, LineCluster lc_y, LineCluster lc_z)
{
    Mat rot_m;
    Rodrigues(R_c_w, rot_m);
    
    double x[4] = {rot_m.at<double>(0), rot_m.at<double>(1), rot_m.at<double>(2), K.at<double>(0,0)};
    
    Problem problem;
    for (int i = 0; i < lc_x.num_lines; ++i) {
        ceres::CostFunction* cost_function =
        Residual2::Create(lc_x.lines[i]->s[0], lc_x.lines[i]->s[1], lc_x.lines[i]->m[0], lc_x.lines[i]->m[1], K.at<double>(0,2), K.at<double>(1,2), 0);
        problem.AddResidualBlock(cost_function, new ceres::CauchyLoss(0.5), x);
    }
    
    for (int i = 0; i < lc_y.num_lines; ++i) {
        ceres::CostFunction* cost_function =
        Residual2::Create(lc_y.lines[i]->s[0], lc_y.lines[i]->s[1], lc_y.lines[i]->m[0], lc_y.lines[i]->m[1], K.at<double>(0,2), K.at<double>(1,2), 1);
        problem.AddResidualBlock(cost_function, new ceres::CauchyLoss(0.5), x);
    }
    
    for (int i = 0; i < lc_z.num_lines; ++i) {
        ceres::CostFunction* cost_function =
        Residual2::Create(lc_z.lines[i]->s[0], lc_z.lines[i]->s[1], lc_z.lines[i]->m[0], lc_z.lines[i]->m[1], K.at<double>(0,2), K.at<double>(1,2), 2);
        problem.AddResidualBlock(cost_function, new ceres::CauchyLoss(0.5), x);
    }
    
    ceres::Solver::Options options;
    options.linear_solver_type = ceres::DENSE_SCHUR;//DENSE_SCHUR; //DENSE_QR
    options.minimizer_progress_to_stdout = false;
    ceres::Solver::Summary summary;
    ceres::Solve(options, &problem, &summary);
//        std::cout << summary.FullReport() << "\n";
    
    rot_m.at<double>(0) = x[0];
    rot_m.at<double>(1) = x[1];
    rot_m.at<double>(2) = x[2];
    
//    cout << "Initial R: " << R_c_w << "\n";
//    cout << "Initial K: " << K << "\n";
    
    Rodrigues(rot_m, R_c_w);
    K.at<double>(0,0) = x[3];
    K.at<double>(1,1) = x[3];
    
    
    
//    cout << "Final R: " << R_c_w << "\n";
//    cout << "Final K: " << K << "\n";
    
}

void CProc::refine(vector<Mat> &R_c_ws, Mat &K, vector<vector<LineCluster>> &lcs_list)
{
    int N = int(lcs_list.size());   // The number of frames
    
    double *x = new double[3*N];    // camera orientations
    double f = K.at<double>(0,0);   // focal length;
    double tx = K.at<double>(0,2);
    double ty = K.at<double>(1,2);
    Mat rot_m;
    Problem problem;
    for(int i=0; i<N; i++){
        Rodrigues(R_c_ws[i], rot_m);
        x[3*i  ] = rot_m.at<double>(0);
        x[3*i+1] = rot_m.at<double>(1);
        x[3*i+2] = rot_m.at<double>(2);
        
        for(int dir = 0; dir<3; dir++){
            for (int j = 0; j < lcs_list[i][dir].num_lines; j++) {
                ceres::CostFunction* cost_function =
                Residual::Create(lcs_list[i][dir].lines[j]->line[0], lcs_list[i][dir].lines[j]->line[1], lcs_list[i][dir].lines[j]->line[2],
                                 tx, ty, dir);
                problem.AddResidualBlock(cost_function, new ceres::CauchyLoss(0.5), &x[3*i], &f);
            }
        }
    }
    ceres::Solver::Options options;
    options.linear_solver_type = ceres::DENSE_QR;//DENSE_SCHUR; //DENSE_QR
    options.minimizer_progress_to_stdout = false;
    ceres::Solver::Summary summary;
    ceres::Solve(options, &problem, &summary);
    //    std::cout << summary.FullReport() << "\n";
    
    for(int i=0; i<N; i++){
        rot_m.at<double>(0) = x[3*i  ];
        rot_m.at<double>(1) = x[3*i+1];
        rot_m.at<double>(2) = x[3*i+2];
        
        
        Rodrigues(rot_m, R_c_ws[i]);
    }
    
    K.at<double>(0,0) = f;
    K.at<double>(1,1) = f;
    
    delete[] x;

}



void CProc::lineFeatureComputation(double* lines, int num_lines, double *image, int rows, int cols, int frame_idx, Mat K, LineFeature *lfs)
{
    // S (5):			sampling size of points on a line segment
    // N (9):			num_sub_regions
    // W (5):			5x5 size of sub-region
    // th (0.4):		threshold for restraining maximum value in the unit vector
    int S = 10;          // ACCV2014 S, N, W => (10, 3, 5)
    int N = 3;
    int W = 5;
    double th = 0.4f;
    
    
    for(int i=0; i<num_lines; i++)
    {
        // s, e, m
        lfs[i].s[0] = lines[i*4  ];
        lfs[i].s[1] = lines[i*4+1];
        lfs[i].e[0] = lines[i*4+2];
        lfs[i].e[1] = lines[i*4+3];
        lfs[i].m[0] = (lfs[i].s[0] + lfs[i].e[0])*0.5;
        lfs[i].m[1] = (lfs[i].s[1] + lfs[i].e[1])*0.5;
        
        // line length
        lfs[i].length =
        sqrt( (lfs[i].s[0]-lfs[i].e[0])*(lfs[i].s[0]-lfs[i].e[0]) + (lfs[i].s[1]-lfs[i].e[1])*(lfs[i].s[1]-lfs[i].e[1]) );
        
        // MSLD	(line->s, e, m, length are used)
        lfs[i].MSLD = MSLD(&lfs[i], image, rows, cols, lfs[i].ln, S, N, W, th);
        lfs[i].len_desc = 8*N;
       
        // line in homogeneous
        double a[3], b[3];
        a[0] = lfs[i].s[0];
        a[1] = lfs[i].s[1];
        a[2] = 1.0f;
        
        b[0] = lfs[i].e[0];
        b[1] = lfs[i].e[1];
        b[2] = 1.0f;
        
        m_util.crossProd(a, b, lfs[i].line);
        m_util.normalizeLine(lfs[i].line);
        
        
        // line normal vector = K^T * l
        double *ptr = K.ptr<double>();
        lfs[i].line_norm[0] = ptr[0]*lfs[i].line[0] + ptr[3]*lfs[i].line[1] + ptr[6]*lfs[i].line[2];
        lfs[i].line_norm[1] = ptr[1]*lfs[i].line[0] + ptr[4]*lfs[i].line[1] + ptr[7]*lfs[i].line[2];
        lfs[i].line_norm[2] = ptr[2]*lfs[i].line[0] + ptr[5]*lfs[i].line[1] + ptr[8]*lfs[i].line[2];
        m_util.unitVector(lfs[i].line_norm, lfs[i].line_norm, 3);
        
        
        // id
        lfs[i].id = i;
        
        // side
        lfs[i].side = 0;
        
        // above or below the horizon
        lfs[i].above_below = 0;
        
        // bUse
        lfs[i].bUse = false;	// used for homography computation
        
        
        // keep original values before the line correction
        memcpy(lfs[i].line_orig, lfs[i].line, sizeof(double)*3);
        memcpy(lfs[i].line_norm_orig, lfs[i].line_norm, sizeof(double)*3);
        memcpy(lfs[i].s_orig, lfs[i].s, sizeof(double)*2);
        memcpy(lfs[i].e_orig, lfs[i].e, sizeof(double)*2);
        memcpy(lfs[i].m_orig, lfs[i].m, sizeof(double)*2);
        memcpy(lfs[i].line_norm_orig, lfs[i].line_norm, sizeof(double)*3);
        
        // 3d coordinates
        lfs[i].s3d[0] = 0;
        lfs[i].s3d[1] = 0;
        lfs[i].s3d[2] = 0;
        
        lfs[i].e3d[0] = 0;
        lfs[i].e3d[1] = 0;
        lfs[i].e3d[2] = 0;
        
        lfs[i].frame_idx = frame_idx;
    }
    
}

void CProc::getLineAxis(LineFeature *line, double* image, int rows, int cols, double lx[2], double ly[2])
{
    double l[2], length, step_size;
    int num_data;
    
    int offset = 3;
    
    step_size = 2;
    
    // line vector
    l[0] = line->e[0] - line->s[0];
    l[1] = line->e[1] - line->s[1];
    length = sqrt(l[0]*l[0] + l[1]*l[1]);
    
    if(length < step_size)
        step_size = 1;
    
    num_data = int(length / step_size);
    
    l[0] /= length;	l[1] /= length;
    
    lx[0] = l[0];    lx[1] = l[1];
    ly[0] = l[1];    ly[1] = -l[0];
    
    
    int acc_flag = 0;
    for(int i=0; i<num_data; i++)
    {
        int pt[2], pt_s[2], pt_e[2];
        pt[0] = int( line->s[0] + i*step_size*l[0] );		pt[1] = int( line->s[1] + i*step_size*l[1] );
        pt_e[0] = int( pt[0] + offset*ly[0] );               pt_e[1] = int( pt[1] + offset*ly[1] );
        pt_s[0] = int( pt[0] - offset*ly[0] );               pt_s[1] = int( pt[1] - offset*ly[1] );
        
        // check invalid index
        if(pt_e[0] < 0 || pt_e[0] > cols-1 || pt_e[1] < 0 || pt_e[1] > rows-1)
            continue;
        if(pt_s[0] < 0 || pt_s[0] > cols-1 || pt_s[1] < 0 || pt_s[1] > rows-1)
            continue;
        
        if(image[pt_s[1]*cols + pt_s[0]] < image[pt_e[1]*cols + pt_e[0]])
            acc_flag++;
        else
            acc_flag--;
    }
    
    if(acc_flag < 0){
        ly[0] = -ly[0];
        ly[1] = -ly[1];
    }
}

// S (5):			sampling size of points on a line segment
// N (9):			num_sub_regions
// W (5):			5x5 size of sub-region
// th (0.4):		threshold for restraining maximum value in the unit vector
// ln:				orthogonal vector to the line direction.
double *CProc::MSLD(LineFeature *line, double* image, int rows, int cols, double ln[2],
                          int S, int N, int W, double th)
{
    double lx[2], ly[2];
    int p_x, p_y, o_x, o_y, x, y, x_img, y_img;
    double sigma;
    int num_sampling;
    double *V, *weight, *w_upper, *w_down;
    double *mean_V, *std_V, norm_mean_V, norm_std_V;
    double g_x, g_y, g_x_img, g_y_img, g_x_cont, g_y_cont;
    
    getLineAxis(line, image, rows, cols, lx, ly);
    ln[0] = ly[0];
    ln[1] = ly[1];
    memcpy(line->ln, ln, sizeof(double)*2); // orthonormal vector to the line direction
    
    /*
     num_sampling = int( line->length / double(S) );
     // num_sampling must be greater than 2
     if(num_sampling < 2)
     {
     S = int(line->length / 2.f);
     num_sampling = int( line->length / double(S) );
     }
     */
    
    // fixed num_sampling
    num_sampling = S;
    S = int(line->length / double(num_sampling ));
    if(S < 1 )
        S = 1;
    
    sigma = W*N/4.f;
    V = new double[4*N*num_sampling];
    weight = new double[W*N];
    w_upper = new double[W];
    w_down = new double[W];
    mean_V = new double[4*N];
    std_V = new double[4*N];
    
    // initialze weight
    double D = 1/ (sqrt(2*CV_PI)*sigma);
    double dist = - W*N*0.5f;
    for(int i=0; i<W*N; i++)
    {
        dist ++;
        weight[i] = D*exp(-dist*dist / (2*sigma*sigma));
    }
    memset(V, 0, sizeof(double)*4*N*num_sampling);
    memset(mean_V, 0, sizeof(double)*4*N);
    memset(std_V, 0, sizeof(double)*4*N);
    
    w_upper[0] = 1 + floor(0.5f*W) + W;
    w_down[0]  = 2 + floor(0.5f*W);
    for(int i=1; i<W; i++)
    {
        w_upper[i] = w_upper[i-1]- 1;
        w_down[i]  = w_down[i-1] + 1;
    }
    
    // start point of sampling
    p_x = -line->length*0.5;
    p_y = 0;
    for(int t=0; t<num_sampling; t++)
    {
        // left top corner of each sub-region
        o_x = -floor(W*0.5) + p_x;
        o_y = -floor(W*N*0.5) + p_y;
        
        for(int i=0; i<N; i++)	// for each sub-region
        {
            for(int j=0; j<W; j++)
            {
                y = o_y + j;
                for(int k=0; k<W; k++)
                {
                    x = o_x + k;
                    
                    // transform into image coordinate system
                    x_img = lx[0]*x + ly[0]*y + line->m[0];
                    y_img = lx[1]*x + ly[1]*y + line->m[1];
                    
                    if(x_img < 1 || x_img > cols-2 || y_img < 1 || y_img > rows-2)
                        continue;
                    
                    // gradient
                    g_x_img = 0.5 * (image[y_img*cols + x_img+1] - image[y_img*cols + x_img-1]);
                    g_y_img = 0.5 * (image[(y_img+1)*cols + x_img] - image[(y_img-1)*cols + x_img]);
                    
                    // transform into line coordinate system
                    g_x = g_x_img*lx[0] + g_y_img*lx[1];
                    g_y = g_x_img*ly[0] + g_y_img*ly[1];
                    
                    g_x *= weight[i*W+j];
                    g_y *= weight[i*W+j];
                    
                    if(g_x > 0)
                        V[(4*i  )*num_sampling + t] += g_x;
                    else
                        V[(4*i+1)*num_sampling + t] -= g_x;
                    
                    if(g_y > 0)
                        V[(4*i+2)*num_sampling + t] += g_y;
                    else
                        V[(4*i+3)*num_sampling + t] -= g_y;
                    
                    // contribution to its nearest subregions
                    if(N == 1)
                        continue;
                    
                    if(i == 0)
                    {
                        g_x_cont = g_x*w_down[j]/(2*W+2);
                        g_y_cont = g_y*w_down[j]/(2*W+2);
                        
                        if(g_x_cont > 0)
                            V[(4*(i+1)  )*num_sampling + t] += g_x_cont;
                        else
                            V[(4*(i+1)+1)*num_sampling + t] -= g_x_cont;
                        
                        if(g_y_cont > 0)
                            V[(4*(i+1)+2)*num_sampling + t] += g_y_cont;
                        else
                            V[(4*(i+1)+3)*num_sampling + t] -= g_y_cont;
                        
                    }
                    else if(i>0 && i<N-1)
                    {
                        g_x_cont = g_x*w_upper[j]/(2*W+2);
                        g_y_cont = g_y*w_upper[j]/(2*W+2);
                        
                        if(g_x_cont > 0)
                            V[(4*(i-1)  )*num_sampling + t] += g_x_cont;
                        else
                            V[(4*(i-1)+1)*num_sampling + t] -= g_x_cont;
                        
                        if(g_y_cont > 0)
                            V[(4*(i-1)+2)*num_sampling + t] += g_y_cont;
                        else
                            V[(4*(i-1)+3)*num_sampling + t] -= g_y_cont;
                        
                        g_x_cont = g_x*w_down[j]/(2*W+2);
                        g_y_cont = g_y*w_down[j]/(2*W+2);
                        
                        if(g_x_cont > 0)
                            V[(4*(i+1)  )*num_sampling + t] += g_x_cont;
                        else
                            V[(4*(i+1)+1)*num_sampling + t] -= g_x_cont;
                        
                        if(g_y_cont > 0)
                            V[(4*(i+1)+2)*num_sampling + t] += g_y_cont;
                        else
                            V[(4*(i+1)+3)*num_sampling + t] -= g_y_cont;
                    }
                    else if(i == N-1)
                    {
                        g_x_cont = g_x*w_upper[j]/(2*W+2);
                        g_y_cont = g_y*w_upper[j]/(2*W+2);
                        
                        if(g_x_cont > 0)
                            V[(4*(i-1)  )*num_sampling + t] += g_x_cont;
                        else
                            V[(4*(i-1)+1)*num_sampling + t] -= g_x_cont;
                        
                        if(g_y_cont > 0)
                            V[(4*(i-1)+2)*num_sampling + t] += g_y_cont;
                        else
                            V[(4*(i-1)+3)*num_sampling + t] -= g_y_cont;
                    }
                    
                }
            }
            o_y += W;
        }
        p_x += S;
    }
    
    // compute mean_V & std_V
    norm_mean_V = 0;
    norm_std_V = 0;
    for(int i=0; i<4*N; i++)
    {
        // mean
        for(int j=0; j<num_sampling; j++)
            mean_V[i] += V[i*num_sampling + j];
        
        mean_V[i] /= num_sampling;
        norm_mean_V += mean_V[i]*mean_V[i];
        
        // std
        for(int j=0; j<num_sampling; j++)
            std_V[i] += (V[i*num_sampling + j] - mean_V[i])*(V[i*num_sampling + j] - mean_V[i]);
        
        std_V[i] = sqrt(std_V[i]/(num_sampling-1));
        norm_std_V += std_V[i]*std_V[i];
    }
    norm_mean_V = sqrt(norm_mean_V);
    norm_std_V = sqrt(norm_std_V);
    
    // restraining maximum value
    for(int i=0; i<4*N; i++)
    {
        mean_V[i] /= norm_mean_V;
        if(mean_V[i] > th)
            mean_V[i] = th;
        std_V[i] /= norm_std_V;
        if(std_V[i] > th)
            std_V[i] = th;
    }
    
    // renormalization
    norm_mean_V = 0;
    norm_std_V = 0;
    for(int i=0; i<4*N; i++)
    {
        norm_mean_V += mean_V[i]*mean_V[i];
        norm_std_V += std_V[i]*std_V[i];		
    }
    norm_mean_V = sqrt(norm_mean_V);
    norm_std_V = sqrt(norm_std_V);
    for(int i=0; i<4*N; i++)
    {
        mean_V[i] /= norm_mean_V;		
        std_V[i] /= norm_std_V;
    }	
    
    double *descriptor = new double[8*N];
    memcpy(descriptor, mean_V, sizeof(double)*4*N);
    memcpy(descriptor+4*N, std_V, sizeof(double)*4*N);
    
    
    delete[] V;
    delete[] weight;
    delete[] w_upper;
    delete[] w_down;
    delete[] mean_V;
    delete[] std_V;
    
    return descriptor;
}


void CProc::rearrange(LineCluster &line_cluster)
{
    if(line_cluster.num_lines == 0)
        return;
    
    int N = line_cluster.num_lines;
    
    LineFeature **lines = new LineFeature*[N];
    
    vector<int> ids(N, 0);
    for(int i=0; i<N; i++){
        ids[i] = line_cluster.lines[i]->id;
    }
    Mat sorted_idx;
    cv::sortIdx(ids, sorted_idx, CV_SORT_ASCENDING);
    for(int i=0; i<N; i++){
        lines[i] = line_cluster.lines[sorted_idx.at<int>(i)];
        lines[i]->id = i;
    }
    delete [] line_cluster.lines;
    line_cluster.lines = lines;
    
}

double* CProc::computeMatches(LineCluster &line_cluster1, LineCluster &line_cluster2, double MSLD_th, double th_search_range)
{
    if(line_cluster1.num_lines == 0 || line_cluster2.num_lines == 0)
        return NULL;
    
    int N1 = line_cluster1.num_lines;
    int N2 = line_cluster2.num_lines;
    
    int num_cand = 2;
    double *M = new double[N1*N2];
    memset(M, 0, sizeof(double)*N1*N2);
    
    int len_desc = line_cluster1.lines[0]->len_desc;
    double ss = 2*(MSLD_th*0.5)*(MSLD_th*0.5);      // sim(d) = exp(-msld_dist^2 / (2*sig^2)) = exp(-d^2 / ss);
    
    for(int i=0; i<N1; i++){
        vector<double> sim(N2,0);  // This will contain the similarities of line segs in image 2 w.r.t i-th line seg in the first image.
        for(int j=0; j<N2; j++){
            // 1.. check if the angle between vectors perpendicular to the line directions
            if(m_util.innerProd(line_cluster1.lines[i]->ln, line_cluster2.lines[j]->ln, 2) > 0){
                // 2. check the distance between two line segments
                double dist;
                dist  = m_util.distPoint2Line(line_cluster1.lines[i]->line, line_cluster2.lines[j]->s);
                dist += m_util.distPoint2Line(line_cluster1.lines[i]->line, line_cluster2.lines[j]->e);
                dist += m_util.distPoint2Line(line_cluster2.lines[j]->line, line_cluster1.lines[i]->s);
                dist += m_util.distPoint2Line(line_cluster2.lines[j]->line, line_cluster1.lines[i]->e);
                dist *= 0.25;
                if(dist < th_search_range){
                    // 3. overlapping test
                    double l[2] = {line_cluster1.lines[i]->e[0] - line_cluster1.lines[i]->s[0], line_cluster1.lines[i]->e[1] - line_cluster1.lines[i]->s[1]};
                    m_util.unitVector(l, l, 2);
                    double ms[2] = {line_cluster2.lines[j]->s[0] - line_cluster1.lines[i]->m[0], line_cluster2.lines[j]->s[1] - line_cluster1.lines[i]->m[1]};
                    double me[2] = {line_cluster2.lines[j]->e[0] - line_cluster1.lines[i]->m[0], line_cluster2.lines[j]->e[1] - line_cluster1.lines[i]->m[1]};
                    double x1 = m_util.innerProd(ms, l, 2);
                    double x2 = m_util.innerProd(me, l, 2);
                    double alpha = 0.7;
                    if( ! ((x1 < -line_cluster1.lines[i]->length*alpha && x2 < -line_cluster1.lines[i]->length*alpha) || (x1 > line_cluster1.lines[i]->length*alpha && x2 > line_cluster1.lines[i]->length*alpha)) ){
                        // 4. MSLD distance
                        double msld = m_util.EcDistance(line_cluster1.lines[i]->MSLD, line_cluster2.lines[j]->MSLD, len_desc);
                        if(msld < MSLD_th)
                            sim[j] = exp(-msld*msld / ss);
                    }
                }
            }
        }
        // store two best matches from the similarity array
        Mat sorted_idx;
        cv::sortIdx(sim, sorted_idx, CV_SORT_DESCENDING);
        for(int k=0; k<num_cand; k++){
            if(sim[sorted_idx.at<int>(k)] > 0){
                M[i*N2 + sorted_idx.at<int>(k)] = sim[sorted_idx.at<int>(k)];
            }
        }
    }
    return M;
}

Match* CProc::removeEmptyLines(double *M, int N1, int N2, int &N1_new, int &N2_new)
{
    int *rem1 = new int[N1];            // tell which row is empty
    int *rem2 = new int[N2];
    memset(rem1, 0, sizeof(int)*N1);
    memset(rem2, 0, sizeof(int)*N2);
    N1_new = N1;
    for(int i=0; i<N1; i++){
        bool empty = true;
        for(int j=0; j<N2; j++){
            if(M[i*N2+j]){
                empty = false;
                break;
            }
        }
        if(empty){
            rem1[i] = 1;
            N1_new--;
        }
    }
    N2_new = N2;
    for(int j=0; j<N2; j++){
        bool empty = true;
        for(int i=0; i<N1; i++){
            if(M[i*N2+j]){
                empty = false;
                break;
            }
        }
        if(empty){
            rem2[j] = 1;
            N2_new--;
        }
    }
    
    Match *A = new Match[N1_new*N2_new];
    for(int i=0, ii=0; i<N1; i++){
        if(rem1[i])
            continue;
        for(int j=0, jj=0; j<N2; j++){
            if(rem2[j])
                continue;
            A[ii*N2_new+jj].similarity = M[i*N2+j];
            A[ii*N2_new+jj].idx1 = i;
            A[ii*N2_new+jj].idx2 = j;
            jj++;
        }
        ii++;
    }
    
    delete[] rem1;
    delete[] rem2;
    return A;
}

vector<Match*> CProc::removeEmptyLines(double *M21, double *M23, int N1, int N2, int N3, int &N1_new, int &N2_new, int &N3_new)
{
    int *rem1 = new int[N1];            // tell which row is empty
    int *rem2 = new int[N2];
    int *rem3 = new int[N3];
    memset(rem1, 0, sizeof(int)*N1);
    memset(rem2, 0, sizeof(int)*N2);
    memset(rem3, 0, sizeof(int)*N3);
    
    
    // rows of M21
    N2_new = N2;                    // counting N2 after removing elements to allocate right size of mem.
    for(int i=0; i<N2; i++){
        bool empty = true;
        for(int j=0; j<N1; j++){
            if(M21[i*N1+j]){
                empty = false;
                break;
            }
        }
        if(empty){
            for(int j=0; j<N3; j++){        // rows of M23
                if(M23[i*N3+j]){
                    empty = false;
                    break;
                }
            }
            if(empty){
                rem2[i] = 1;
                N2_new--;
            }
        }
    }
    
    N1_new = N1;
    for(int i=0; i<N1; i++){
        bool empty = true;
        for(int j=0; j<N2; j++){
            if(M21[j*N1+i]){
                empty = false;
                break;
            }
        }
        if(empty){
            rem1[i] = 1;
            N1_new--;
        }
    }
    
    N3_new = N3;
    for(int i=0; i<N3; i++){
        bool empty = true;
        for(int j=0; j<N2; j++){
            if(M23[j*N3+i]){
                empty = false;
                break;
            }
        }
        if(empty){
            rem3[i] = 1;
            N3_new--;
        }
    }
    
    Match *A21 = new Match[N2_new*N1_new];
    for(int i=0, ii=0; i<N2; i++){
        if(rem2[i])
            continue;
        for(int j=0, jj=0; j<N1; j++){
            if(rem1[j])
                continue;
            A21[ii*N1_new+jj].similarity = M21[i*N1+j];
            A21[ii*N1_new+jj].idx1 = i;
            A21[ii*N1_new+jj].idx2 = j;
            jj++;
        }
        ii++;
    }
    
    Match *A23 = new Match[N2_new*N3_new];
    for(int i=0, ii=0; i<N2; i++){
        if(rem2[i])
            continue;
        for(int j=0, jj=0; j<N3; j++){
            if(rem3[j])
                continue;
            A23[ii*N3_new+jj].similarity = M23[i*N3+j];
            A23[ii*N3_new+jj].idx1 = i;
            A23[ii*N3_new+jj].idx2 = j;
            jj++;
        }
        ii++;
    }
    
    vector<Match*> ptr_match;
    ptr_match.push_back(A21);
    ptr_match.push_back(A23);
    
    return ptr_match;
}

void CProc::lineMatchingDP(LineCluster &line_cluster1, LineCluster &line_cluster2, LineCluster &line_cluster3, double MSLD_th, double th_search_range)
{
    if(line_cluster1.num_lines == 0 || line_cluster2.num_lines == 0 || line_cluster3.num_lines == 0)
        return;
    
    int N1 = line_cluster1.num_lines;
    int N2 = line_cluster2.num_lines;
    int N3 = line_cluster3.num_lines;
    
    // re arrage the array
    rearrange(line_cluster1);
    rearrange(line_cluster2);
    rearrange(line_cluster3);
    
    double *M21_ = computeMatches(line_cluster2, line_cluster1, MSLD_th, th_search_range);
    double *M23_ = computeMatches(line_cluster2, line_cluster3, MSLD_th, th_search_range);
    
    // Each line should have at least one candidate match, otherwise it should be removed.
    int N1_new, N2_new, N3_new;
    vector<Match*> prt_matches = removeEmptyLines(M21_, M23_, N1, N2, N3, N1_new, N2_new, N3_new);
    delete[] M21_;
    delete[] M23_;
    
    if(N1_new == 0 || N2_new == 0 || N3_new == 0){
        delete[] prt_matches[0];
        delete[] prt_matches[1];
        return;
    }
    
    
    Match *M21 = prt_matches[0];
    Match *M23 = prt_matches[1];
    
    N1 = N1_new;    // don't need to keep N1, N2, and N3
    N2 = N2_new;
    N3 = N3_new;
    
    for(int i=0; i<N1*N2; i++){
        if(M21[i].similarity)
            M21[i].similarity = 1-M21[i].similarity;        // convert to cost value
        else
            M21[i].similarity = -1;
    }
    for(int i=0; i<N3*N2; i++){
        if(M23[i].similarity)
            M23[i].similarity = 1-M23[i].similarity;        // convert to cost value
        else
            M23[i].similarity = -1;
    }
    
    
    // compute the cumulative cost map
    double *C = new double[N1 * N2 * N3];
    memset(C, 0, sizeof(double)*N1 * N2 * N3);
    Point3i *P = new Point3i[N1 * N2 * N3];
    double w1 = 0, w2 = 1;
    double s_cost = 0.0;
    for(int i=0; i<N1; i++){
        for(int j=0; j<N2; j++){
            for(int k=0; k<N3; k++){
                int ijk = N1*N2*k + i*N2 + j;
                int ji = j*N1 + i;
                int jk = j*N3 + k;
                
                if(M21[ji].similarity != -1 && M23[jk].similarity != -1){  // C(i,j,k) = n(i,j,k) + min{ C(m,n,l) + e((i,j,k), (m,n,l)) } where m<i, n<j, l<k
                    double min_cost = 100000;
                    for(int m=0; m<i; m++){
                        for(int n=0; n<j; n++){
                            for(int l=0; l<k; l++){
                                
                                int mnl = N1*N2*l + m*N2 + n;
                                int nm = n*N1 + m;
                                int nl = n*N3 + l;
                                
                                if(M21[nm].similarity != -1 && M23[nl].similarity != -1){
                                    double edge_cost = 1 - pairwiseSimilarity(line_cluster2.lines[M21[ji].idx1], line_cluster2.lines[M21[nm].idx1],
                                                                              line_cluster1.lines[M21[ji].idx2], line_cluster1.lines[M21[nm].idx2]) ;
                                    edge_cost += 1 - pairwiseSimilarity(line_cluster2.lines[M23[jk].idx1], line_cluster2.lines[M23[nl].idx1],
                                                                        line_cluster3.lines[M23[jk].idx2], line_cluster3.lines[M23[nl].idx2]) ;
                                    edge_cost *= 0.5*w1;
                                    
                                    edge_cost += w2* ( i-m-1 + j-n-1 + k-l-1);
                                    
                                    if(min_cost > C[mnl] + edge_cost){
                                        min_cost = C[mnl] + edge_cost;
                                        P[ijk] = Point3i(m, n, l);
                                    }
                                }
                            }
                        }
                    }
                    // for the fake nose (source node)
                    double edge_cost = w2*(i + j + k);
                    if(min_cost > s_cost + edge_cost){
                        min_cost = s_cost + edge_cost;
                        P[ijk] = Point3i(-1,-1, -1);
                    }
                    
                    C[ijk] = (M21[ji].similarity + M23[jk].similarity)*0.5 + min_cost;
                }
            }
        }
    }
    
    // find start point
    Point3i p_cur;
    double min_cost = 100000;
    for(int i=0; i<N1; i++){
        for(int j=0; j<N2; j++){
            for(int k=0; k<N3; k++){
                int ijk = N1*N2*k  + i*N2 + j;
                int ji = j*N1 + i;
                int jk = j*N3 + k;
                if(M21[ji].similarity != -1 && M23[jk].similarity != -1){
                    double edge_cost = w2* (N1-i-1 + N2-j-1 + N3-k-1);
                    if(min_cost > C[ijk] + edge_cost){
                        min_cost = C[ijk] + edge_cost;
                        p_cur = Point3i(i,j,k);
                    }
                }
            }
        }
    }
    if(p_cur == Point3i(0,0,0) && P[0] == Point3i(0,0,0)){
        delete[] M21;
        delete[] M23;
        delete[] C;
        delete[] P;
        return;
    }
    
    while(1){
        int ijk = N1*N2 * p_cur.z  + p_cur.x*N2 + p_cur.y;
        int ji = p_cur.y*N1 + p_cur.x;
        int jk = p_cur.y*N3 + p_cur.z;
        
        if(M21[ji].similarity != -1 && M23[jk].similarity != -1){
            line_cluster1.lines[M21[ji].idx2]->child1 = (void*)line_cluster2.lines[M21[ji].idx1];
            line_cluster2.lines[M21[ji].idx1]->status = CHILD;
            
            line_cluster2.lines[M23[jk].idx1]->child1 = (void*)line_cluster3.lines[M23[jk].idx2];
            line_cluster3.lines[M23[jk].idx2]->status = CHILD;
        }
        
        p_cur = P[ijk];
        if(p_cur.x == -1 && p_cur.y == -1 && p_cur.z == -1)
            break;
    }
    
    delete[] M21;
    delete[] M23;
    delete[] C;
    delete[] P;
}

void CProc::lineMatchingDP(LineCluster &line_cluster1, LineCluster &line_cluster2, double MSLD_th, double th_search_range)
{
    if(line_cluster1.num_lines == 0 || line_cluster2.num_lines == 0)
        return;
    
    int N1 = line_cluster1.num_lines;
    int N2 = line_cluster2.num_lines;
    
    
    // re arrage the array
    rearrange(line_cluster1);
    rearrange(line_cluster2);
    
    double *M_ = computeMatches(line_cluster1, line_cluster2, MSLD_th, th_search_range);
    
    // Each line should have at least one candidate match, otherwise it should be removed.
    int N1_new, N2_new;
    Match *M = removeEmptyLines(M_, N1, N2, N1_new, N2_new);
    delete[] M_;
    
    N1 = N1_new;    // don't need to keep N1, N2
    N2 = N2_new;
    for(int i=0; i<N1*N2; i++){
        if(M[i].similarity)
            M[i].similarity = 1-M[i].similarity;        // convert to cost value
        else
            M[i].similarity = -1;
    }
    
//    Mat L = Mat::zeros(N1, N2, CV_8UC3);
//    uchar *p = L.data;
//    for(int i=0; i<N1; i++){
//        for(int j=0; j<N2; j++){
//            if(M[i*N2+j].similarity != -1){
//                p[3*(i*N2+j)+0] = 125;
//                p[3*(i*N2+j)+1] = 125;
//                p[3*(i*N2+j)+2] = 125;
//            }
//        }
//    }
    
    // compute the cumulative cost map
    //    double *S = new double[N1*N2];
    //    memset(S, 0, sizeof(double)*N1*N2);
    
    double *C = new double[N1 * N2];
    memset(C, 0, sizeof(double)*N1 * N2);
    Point *P = new Point[N1 * N2];
    double w1 = 0, w2 = 1;
    double s_cost = 0.0;
    for(int i=0; i<N1; i++){
        for(int j=0; j<N2; j++){
            int ij = i*N2 + j;
            
            if(M[ij].similarity != -1){  // C(i,j) = n(i,j) + min{ C(m,n) + e((i,j), (m,n)) } where m<i, n<j
                double min_cost = 100000;
                for(int m=0; m<i; m++){
                    for(int n=0; n<j; n++){
                        int mn = m*N2+n;
                        if(M[mn].similarity != -1){
                            double edge_cost = w1* ( 1 - pairwiseSimilarity(line_cluster1.lines[M[ij].idx1], line_cluster1.lines[M[mn].idx1], line_cluster2.lines[M[ij].idx2], line_cluster2.lines[M[mn].idx2]) );
                            edge_cost += w2* ( i-m-1 + j-n-1 );
                            
                            if(min_cost > C[mn] + edge_cost){
                                min_cost = C[mn] + edge_cost;
                                P[ij] = Point(m, n);
                            }
                        }
                    }
                }
                // for the fake nose (source node)
                double edge_cost = w2*(i + j);
                if(min_cost > s_cost + edge_cost){
                    min_cost = s_cost + edge_cost;
                    P[ij] = Point(-1,-1);
                }
                
                C[ij] = M[ij].similarity + min_cost;
                //                S[ij] = M[ij].similarity;
            }
        }
    }
    
    // find start point
    Point p_cur;
    double min_cost = 100000;
    for(int i=0; i<N1; i++){
        for(int j=0; j<N2; j++){
            int ij = i*N2+j;
            if(M[ij].similarity!= -1){
                double edge_cost = w2* (N1-i-1 + N2-j-1);
                if(min_cost > C[ij] + edge_cost){
                    min_cost = C[ij] + edge_cost;
                    p_cur = Point(i,j);
                }
            }
        }
    }
    
    while(1){
        int idx_cur  = p_cur.x *N2 + p_cur.y;
        if(M[idx_cur].similarity != -1){
            line_cluster1.lines[M[idx_cur].idx1]->child1 = (void*)line_cluster2.lines[M[idx_cur].idx2];
            line_cluster2.lines[M[idx_cur].idx2]->status = CHILD;
            
//            p[3*idx_cur+0] = 0;
//            p[3*idx_cur+1] = 255;
//            p[3*idx_cur+2] = 0;
            
        }
        
        p_cur = P[idx_cur];
        if(p_cur.x == -1 && p_cur.y == -1)
            break;
        
    }
    
    delete[] M;
    delete[] C;
    delete[] P;
    
//    imwrite("/Users/Chelhwon/Downloads/LL.png", L);
}

// potential correspondences: <line1a, line2a>, <line1b, line2b> (line1a: Line 'a' in Image 1)
double CProc::pairwiseSimilarity(LineFeature *line1a, LineFeature *line1b,  LineFeature *line2a, LineFeature *line2b)
{
    double angle1 = fabs(line1a->line_norm[0]*line1b->line_norm[0] + line1a->line_norm[1]*line1b->line_norm[1]
                         + line1a->line_norm[2]*line1b->line_norm[2]);
    
    double angle2 = fabs(line2a->line_norm[0]*line2b->line_norm[0] + line2a->line_norm[1]*line2b->line_norm[1]
                         + line2a->line_norm[2]*line2b->line_norm[2]);
    
    
    return 1 - fabs(angle1 - angle2);
}

// s: delete chains whose length is less than s*frame_length
void CProc::buildLineChains(vector<vector<LineCluster>> lcs_list, vector<vector<LineID>> chains[3], double s)
{
    int num_frames = int(lcs_list.size());
    // build chains of line correspondences
    for(size_t i=0; i<num_frames; i++){                  // for each image frame
        for(int j=0; j<3; j++){                                         // for each one of the three principle directions
            for(int k=0; k<lcs_list[i][j].num_lines; k++){    // for each line segment
                if(lcs_list[i][j].lines[k]->status == ROOT){  // if the line segment is a root then start collecting its children in a list
                    vector<LineID> chain;
                    int frame_idx = int(i);
                    
                    LineFeature* ptr = lcs_list[i][j].lines[k];
                    while(1){
                        LineID id;
                        id.frame_idx = frame_idx;
                        id.addr = ptr;
                        chain.push_back(id);
                        
                        if(ptr->child1 == NULL){
                            lcs_list[i][j].lines[k]->last = (void*)ptr;
                            break;
                        }
                        
                        ptr = (LineFeature*)(ptr->child1);
                        frame_idx++;
                    }
                    
                    if(chain.size() > num_frames*s)
                        chains[j].push_back(chain);
                }
            }
        }
    }
}

LineFeature** CProc::buildLineChains(vector<vector<LineCluster>> lcs_list, int dir, int &num_chains, double s)
{
    vector<LineFeature**> C;
    
    int num_frames = int(lcs_list.size());
    // build chains of line correspondences
    for(size_t i=0; i<num_frames; i++){                  // for each image frame
        for(int k=0; k<lcs_list[i][dir].num_lines; k++){    // for each line segment
            if(lcs_list[i][dir].lines[k]->status == ROOT){  // if the line segment is a root then start collecting its children in a list
                LineFeature **c = new LineFeature*[num_frames];
                memset(c, 0, sizeof(LineFeature*)*num_frames);
                int frame_idx = int(i);
                
                LineFeature* ptr = lcs_list[i][dir].lines[k];
                while(1){
                    c[frame_idx] = ptr;
                    
                    if(ptr->child1 == NULL){
                        lcs_list[i][dir].lines[k]->last = (void*)ptr;
                        break;
                    }
                    
                    ptr = (LineFeature*)(ptr->child1);
                    frame_idx++;
                }
                
                if(frame_idx - int(i)+1 > int(num_frames*s))
                    C.push_back(c);
                else
                    delete[] c;
            }
        }
    }
    
    
    // merge all elements of C[i] into one array
    num_chains = int(C.size());
    LineFeature** M = NULL;
    if(num_chains){
        M = new LineFeature*[num_chains*num_frames];
        for(int i=0; i<num_chains; i++){
            memcpy(&M[i*num_frames], C[i], sizeof(LineFeature*)*num_frames);
            delete[] C[i];
        }
    }
    
    return M;
}

// read R_b_a (b means camera body, a means arbitrary axis whose z axis along gravity vector
Mat CProc::getR_c_wFromFile(string f_R_b_a)
{
    FILE *fp = fopen(f_R_b_a.data(), "r");
    float R[9];
    fscanf(fp, "%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n", &R[0], &R[1], &R[2], &R[3], &R[4], &R[5], &R[6], &R[7], &R[8]);
    fclose(fp);
    
    Mat R_b_a = (Mat_<double>(3,3) << R[0], R[1], R[2], R[3], R[4], R[5], R[6], R[7], R[8]);
    Mat R_c_b = (Mat_<double>(3,3) << 0, -1, 0, -1, 0, 0, 0, 0, -1);
    Mat R_c_w = R_c_b * R_b_a;  // initial value for R_c_w
    
    return R_c_w;
}

void CProc::drawLineFeatures(string name, Mat img, LineFeature *lfs, int num_lines)
{
    Point s, e;
    Scalar color = CV_RGB(255,0,0);
    
    for(int i=0; i<num_lines; i++){
        s.x = lfs[i].s[0];
        s.y = lfs[i].s[1];
        
        e.x = lfs[i].e[0];
        e.y = lfs[i].e[1];
        
        line(img, s, e, color, 2);
    }
    
    imshow(name, img);
}
void CProc::drawLineClusters(string name, Mat img, vector<LineCluster> lcs)
{
    Point s, e;
    Scalar color[3] = { CV_RGB(255,0,0), CV_RGB(0, 255, 0), CV_RGB(0, 0, 255)};
    
    for(size_t i=0; i<lcs.size(); i++){
        for(int j=0; j<lcs[i].num_lines; j++){
            s.x = lcs[i].lines[j]->s[0];
            s.y = lcs[i].lines[j]->s[1];
            
            e.x = lcs[i].lines[j]->e[0];
            e.y = lcs[i].lines[j]->e[1];
            
            line(img, s, e, color[i], 1);
            
            char id[10];
            sprintf(id, "%d", lcs[i].lines[j]->id);
            putText(img, id, 0.5*(s+e), FONT_HERSHEY_SIMPLEX, 0.4, color[i], 1);
        }
    }
    imshow(name, img);
}

void CProc::drawLineMatches(string name1, Mat img1, string name2, Mat img2, vector<LineCluster> lcs1, vector<LineCluster> lcs2)
{
    for(int j=0; j<3; j++){
        for(int k=0; k<lcs2[j].num_lines; k++){
            LineFeature *first = lcs2[j].lines[k];
            Point s, e;
            s.x = first->s_orig[0];  s.y = first->s_orig[1];
            e.x = first->e_orig[0];  e.y = first->e_orig[1];
            cv::line(img2, s, e, Scalar(0,0,255), 1);
        }
    }
    
    for(int j=0; j<3; j++){
        for(int k=0; k<lcs1[j].num_lines; k++){
            
            Scalar color( rand()&255, rand()&255, rand()&255 );
            if(lcs1[j].lines[k]->child1 != NULL ){
                LineFeature *first = lcs1[j].lines[k];
                LineFeature *last = (LineFeature*)lcs1[j].lines[k]->child1;
                
                if( first->frame_idx + 1 != last->frame_idx )
                    continue;
                
                Point s, e;
                s.x = first->s_orig[0];  s.y = first->s_orig[1];
                e.x = first->e_orig[0];  e.y = first->e_orig[1];
                cv::line(img1, s, e, color, 2);
                //putText(img1, to_string(first->id), (s+e)*0.5, FONT_HERSHEY_SIMPLEX, 0.4, color, 1);
				
                s.x = last->s_orig[0];  s.y = last->s_orig[1];
                e.x = last->e_orig[0];  e.y = last->e_orig[1];
                cv::line(img2, s, e, color, 2);
                //putText(img2, to_string(last->id), (s+e)*0.5, FONT_HERSHEY_SIMPLEX, 0.4, color, 1);
                
                //putText(img1, to_string(first->frame_idx), Point(20,20), FONT_HERSHEY_SIMPLEX, 1, Scalar(0,0,255));
                //putText(img2, to_string(last->frame_idx), Point(20,20), FONT_HERSHEY_SIMPLEX, 1, Scalar(0,0,255));
            }
            else{
                LineFeature *first = lcs1[j].lines[k];
                Point s, e;
                s.x = first->s_orig[0];  s.y = first->s_orig[1];
                e.x = first->e_orig[0];  e.y = first->e_orig[1];
                cv::line(img1, s, e, Scalar(0,0,255), 1);
            }
        }
    }
    imshow(name1, img1);
    imshow(name2, img2);
    
}

void CProc::drawLineMatches(string name1, Mat img1, string name2, Mat img2, string name3, Mat img3, vector<LineCluster> lcs1, vector<LineCluster> lcs2, vector<LineCluster> lcs3)
{
    for(int j=0; j<3; j++){
        for(int k=0; k<lcs1[j].num_lines; k++){
            LineFeature *first = lcs1[j].lines[k];
            Point s, e;
            s.x = first->s_orig[0];  s.y = first->s_orig[1];
            e.x = first->e_orig[0];  e.y = first->e_orig[1];
            cv::line(img1, s, e, Scalar(0,0,255), 1);
            
        }
        for(int k=0; k<lcs2[j].num_lines; k++){
            LineFeature *first = lcs2[j].lines[k];
            Point s, e;
            s.x = first->s_orig[0];  s.y = first->s_orig[1];
            e.x = first->e_orig[0];  e.y = first->e_orig[1];
            cv::line(img2, s, e, Scalar(0,0,255), 1);
            
        }
        for(int k=0; k<lcs3[j].num_lines; k++){
            LineFeature *first = lcs3[j].lines[k];
            Point s, e;
            s.x = first->s_orig[0];  s.y = first->s_orig[1];
            e.x = first->e_orig[0];  e.y = first->e_orig[1];
            cv::line(img3, s, e, Scalar(0,0,255), 1);
            
        }
    }
    
//    putText(img1, to_string(lcs1[0].lines[0]->frame_idx), Point(20,20), FONT_HERSHEY_SIMPLEX, 1, Scalar(0,0,255));
//    putText(img2, to_string(lcs1[0].lines[0]->frame_idx+1), Point(20,20), FONT_HERSHEY_SIMPLEX, 1, Scalar(0,0,255));
//    putText(img3, to_string(lcs1[0].lines[0]->frame_idx+2), Point(20,20), FONT_HERSHEY_SIMPLEX, 1, Scalar(0,0,255));
    
    for(int j=0; j<3; j++){
        for(int k=0; k<lcs1[j].num_lines; k++){
            Scalar color( rand()&255, rand()&255, rand()&255 );
            
            LineFeature *lf1 = NULL, *lf2 = NULL, *lf3 = NULL;
            lf1 = lcs1[j].lines[k];
            lf2 = (LineFeature*)lcs1[j].lines[k]->child1;
            if(lf2)
                lf3 = (LineFeature*)lf2->child1;
            
            if(lf1 && lf2 && lf3){
                Point s, e;
                s.x = lf1->s_orig[0];  s.y = lf1->s_orig[1];
                e.x = lf1->e_orig[0];  e.y = lf1->e_orig[1];
                cv::line(img1, s, e, color, 2);
                //putText(img1, to_string(lf1->id), (s+e)*0.5, FONT_HERSHEY_SIMPLEX, 0.4, color, 1);
                
                s.x = lf2->s_orig[0];  s.y = lf2->s_orig[1];
                e.x = lf2->e_orig[0];  e.y = lf2->e_orig[1];
                cv::line(img2, s, e, color, 2);
                //putText(img2, to_string(lf2->id), (s+e)*0.5, FONT_HERSHEY_SIMPLEX, 0.4, color, 1);
                
                s.x = lf3->s_orig[0];  s.y = lf3->s_orig[1];
                e.x = lf3->e_orig[0];  e.y = lf3->e_orig[1];
                cv::line(img3, s, e, color, 2);
                //putText(img3, to_string(lf3->id), (s+e)*0.5, FONT_HERSHEY_SIMPLEX, 0.4, color, 1);
            }
        }
    }
    
    imshow(name1, img1);
    imshow(name2, img2);
    imshow(name3, img3);
}


void CProc::drawLineLineChains(string name1, Mat img1, int frame1, string name2, Mat img2, int frame2, vector<vector<LineID>> chains[3])
{
    for(int i=0; i<3; i++){
        for(size_t j=0; j<chains[i].size(); j++){
            LineFeature *first = (LineFeature*)chains[i][j][0].addr;
            LineFeature *last = (LineFeature*)chains[i][j][chains[i][j].size()-1].addr;
            int fidx_first = chains[i][j][0].frame_idx;
            int fidx_last = chains[i][j][chains[i][j].size()-1].frame_idx;
            
            if(fidx_first <= frame1 && fidx_last >= frame2){
                Scalar color( rand()&255, rand()&255, rand()&255 );
                
                while(1)
                {
                    if(first->frame_idx >= frame1)
                        break;                    
                    first = (LineFeature*)(first->child1);
                }
                
                last = first;
                while(1)
                {
                    if(last->frame_idx >= frame2)
                        break;
                    last = (LineFeature*)(last->child1);
                }
                
                Point s, e;
                s.x = first->s_orig[0];  s.y = first->s_orig[1];
                e.x = first->e_orig[0];  e.y = first->e_orig[1];
                cv::line(img1, s, e, color, 2);
                
                s.x = last->s_orig[0];  s.y = last->s_orig[1];
                e.x = last->e_orig[0];  e.y = last->e_orig[1];
                cv::line(img2, s, e, color, 2);
                
            }
        }
    }
    imshow(name1, img1);
    imshow(name2, img2);
}

void CProc::drawLineLineChains(string name1, Mat img1, int frame1, string name2, Mat img2, int frame2, LineFeature** chains[3], int num_chains[3], int num_frames)
{
    for(int i=0; i<3; i++){
        for(int j=0; j<num_chains[i]; j++){
            LineFeature* first = chains[i][j*num_frames + frame1];
            LineFeature* last  = chains[i][j*num_frames + frame2];
            if(first && last){
                Scalar color( rand()&255, rand()&255, rand()&255 );
                Point s, e;
                s.x = first->s_orig[0];  s.y = first->s_orig[1];
                e.x = first->e_orig[0];  e.y = first->e_orig[1];
                cv::line(img1, s, e, color, 2);
                
                s.x = last->s_orig[0];  s.y = last->s_orig[1];
                e.x = last->e_orig[0];  e.y = last->e_orig[1];
                cv::line(img2, s, e, color, 2);
                
            }
        }
    }
    imshow(name1, img1);
    imshow(name2, img2);
}


