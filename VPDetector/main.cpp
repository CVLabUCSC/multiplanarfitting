#include "main.h"

#include <iomanip>

using std::string;

void detectVanishingPoint() {

}

void individualRefinement(const char *dirpath)
{
    //-------------------------------
    // Variables
    //-------------------------------
	char filename[256];
    CProc proc;
    CTR TR;
    CUtility util;
	Mat img;
	Mat R_c_w;
    vector< vector <LineCluster>> lcs_list;
    vector<LineFeature*> lfs_list;
    vector<int> num_lines_list;
	vector<path> img_paths;

    //-------------------------------
    // paramters
    //-------------------------------
    double th_dist_merge = 0.0; // in pixel
    double th_gap_merge = 50;   // in pixel
    int th_short = 50;          // in pixel
    double e_th = 5;           // in pixel
    int num_trials = 50;
	//Mat K = (Mat_<double>(3, 3) << 620.536405508902360, 0, 315.196672910396270, 0, 621.389271362295290, 239.878886845642570, 0, 0, 1);
    
	path imgDir(dirpath);
	directory_iterator end_iter;
	
    //-------------------------------
    // Read images and rotations
    //-------------------------------
	if (exists(imgDir) && is_directory(imgDir))
	{
		for(directory_iterator dir_iter(imgDir) ; dir_iter != end_iter ; ++dir_iter)
		{
			if (is_regular_file(dir_iter->status()) && dir_iter->path().extension() == ".JPG")
			{
				img_paths.push_back(dir_iter->path());
			}
		}
	}

	CVPDetector vpd;
	for (int i = 0; i < static_cast<int>(img_paths.size()); i++)
	{
		img = imread(img_paths[i].relative_path().string());

		Mat K = (Mat_<double>(3, 3) << 617.79435, 0, img.cols / 2, 0, 619.24783, img.rows / 2, 0, 0, 1);

		sprintf(filename, "%s\\%s_R.txt", dirpath, img_paths[i].stem().string().c_str());

		//R_c_w = proc.getR_c_wFromFile(filename);      // read R_c_w
		R_c_w = (Mat_<double>(3, 3) << 1, 0, 0, 0, 1, 0, 0, 0, 1);

		double t = (double)getTickCount();

		double *img_d = util.Mat2Double(img);

		//---------line detection------------//
		int num_lines;
		double *lines = proc.lineDetection(img_d, img.rows, img.cols, num_lines, th_dist_merge, th_gap_merge, th_short);
        
		//---------line feature computation------------//
		LineFeature *lfs = new LineFeature[num_lines];
		proc.lineFeatureComputation(lines, num_lines, img_d, img.rows, img.cols, i, K, lfs);
		lfs_list.push_back(lfs);
		num_lines_list.push_back(num_lines);
        
		delete[] lines;
		delete[] img_d;

//---------compute vanishing point and line clusters------------//
vector<LineCluster> lcs = proc.LineClusteringByVP(lfs, num_lines, R_c_w, e_th, num_trials, K, true);
lcs_list.push_back(lcs);

vpd.drawLines("Axes", img, lcs_list[i], false);
CVPDetector::drawAxes("Axes", img, lcs[0].vd, lcs[1].vd, lcs[2].vd, K);
//waitKey();

t = ((double)getTickCount() - t) / getTickFrequency();
printf("Total elapsed time: %f sec\n", t);

//---------save result------------//
sprintf(filename, "%s\\%s_VD.jpg", dirpath, img_paths[i].stem().string().c_str());
imwrite(filename, img);

// Vanishing points
sprintf(filename, "%s\\%s_VD.txt", dirpath, img_paths[i].stem().string().c_str());
fstream fout(filename, fstream::out);
fout << R_c_w.at<double>(0, 0) << " " << R_c_w.at<double>(0, 1) << " " << R_c_w.at<double>(0, 2) << endl;
fout << R_c_w.at<double>(1, 0) << " " << R_c_w.at<double>(1, 1) << " " << R_c_w.at<double>(1, 2) << endl;
fout << R_c_w.at<double>(2, 0) << " " << R_c_w.at<double>(2, 1) << " " << R_c_w.at<double>(2, 2) << endl;
fout.close();

// Calibration matrix
sprintf(filename, "%s\\%s_K.txt", dirpath, img_paths[i].stem().string().c_str());
fout.open(filename, fstream::out);
fout << K.at<double>(0, 0) << " " << K.at<double>(0, 1) << " " << K.at<double>(0, 2) << endl;
fout << K.at<double>(1, 0) << " " << K.at<double>(1, 1) << " " << K.at<double>(1, 2) << endl;
fout << K.at<double>(2, 0) << " " << K.at<double>(2, 1) << " " << K.at<double>(2, 2) << endl;
fout.close();

img.release();
R_c_w.release();
	}

	for (int i = 0; i < lcs_list.size(); i++)
	{
		for (int j = 0; j < lcs_list[i].size(); j++)
			lcs_list[i][j].clear();
	}
	for (int i = 0; i < lfs_list.size(); i++)
	{
		for (int j = 0; j < num_lines_list[i]; j++)
			lfs_list[i][j].clear();
		delete[] lfs_list[i];
	}

	system("pause");
}

Mat load3x3Matrix(const std::string &filename)
{
	Mat mat(3, 3, CV_64F);
	fstream fin(filename, fstream::in);

	if (!fin.is_open())
	{
		printf("Not able to open the 3x3 matrix data file: %s\n", strerror(errno));
		return Mat();
	}

	fin >> mat.at<double>(0, 0);
	fin >> mat.at<double>(0, 1);
	fin >> mat.at<double>(0, 2);
	fin >> mat.at<double>(1, 0);
	fin >> mat.at<double>(1, 1);
	fin >> mat.at<double>(1, 2);
	fin >> mat.at<double>(2, 0);
	fin >> mat.at<double>(2, 1);
	fin >> mat.at<double>(2, 2);

	fin.close();

	return mat;
}

double* removeOutOfRangeLines(cv::Mat image, double *lines, int &num_lines)
{
	int cnt = 0;
	Vec3b emptyPixel(0, 0, 0);

	for (int i = 0, j = 0; i < num_lines; i++, j += 4) {
		Point pt1(lines[j], lines[j + 1]);
		Point pt2(lines[j + 2], lines[j + 3]);

		try {
			// Check the pixel intensity if it can
			if (lines[j] >= 0 && lines[j] < image.cols && lines[j + 1] >= 0 && lines[j + 1] < image.rows) {
				if (image.at<Vec3b>(Point(lines[j], lines[j + 1])) == emptyPixel) {
					continue;
				}
			}

			if (lines[j + 2] >= 0 && lines[j + 2] < image.cols && lines[j + 3] >= 0 && lines[j + 3] < image.rows) {
				if (image.at<Vec3b>(Point(lines[j + 2], lines[j + 3])) == emptyPixel) {
					continue;
				}
			}

			memcpy(&lines[cnt * 4], &lines[j], sizeof(double) * 4);
			cnt++;
		}
		catch (Exception ex) {
			std::cout << "out of bound!" << std::endl;
		}
	}

	num_lines = cnt;

	return lines;
}

void refinement(string dirpath, string extension, bool useEstimatedRotation)
{
    //-------------------------------
    // Variables
    //-------------------------------
	char filename[256];
    CProc proc;
	CUtility util;
    vector<vector<LineCluster>> lcs_list;
    vector<LineFeature*> lfs_list;
    vector<int> num_lines_list;
    vector<Mat> Ks;
    vector<Mat> R_c_ws;
	vector<Mat> imgs;
	fstream fout;
	
    //-------------------------------
    // paramters
    //-------------------------------
    double th_dist_merge = 0.0; // in pixel
    double th_gap_merge = 50;   // in pixel
    int th_short = 50;          // in pixel
    double e_th = 2;           // in pixel
    int num_trials = 50;
    double th_search_range = 50.0;
    double th_msld = 0.75;

	// Load calibration matrix
	Mat K = load3x3Matrix(dirpath + "\\K.txt");

	// Load rotation matrices
	vector<String> rotationFilenames;
	cv::glob(dirpath + "\\*_R.txt", rotationFilenames);

	switch (rotationFilenames.size()) {
	case 0:
		cout << "Not enough initial rotation files exist!" << endl;
		system("pause");
		return;

	case 1:
		R_c_ws.push_back(load3x3Matrix(rotationFilenames[0]));
		useEstimatedRotation = true;
		break;

	default:
		for (unsigned int i = 0; i < rotationFilenames.size(); i++) {
			R_c_ws.push_back(load3x3Matrix(rotationFilenames[i]));
		}
	}

	cout << "K = " << K << endl;
	
    //-------------------------------
    // Load images
    //-------------------------------
	path imgDir(dirpath);
	directory_iterator end_iter;
	vector<path> img_paths;
	if (exists(imgDir) && is_directory(imgDir))
	{
		for(directory_iterator dir_iter(imgDir) ; dir_iter != end_iter ; ++dir_iter)
		{
			if (is_regular_file(dir_iter->status()) && dir_iter->path().extension() == extension)
			{
				// The filename must not contain "_VP" prefix
				if (dir_iter->path().filename().string().find("_VD") != string::npos ||
					dir_iter->path().filename().string().find("_GT") != string::npos) {
					continue;
				}

				img_paths.push_back(dir_iter->path());
			}
		}
	}

	Mat guideImage = imread("OutOfRange.png");
	
    double t = (double)getTickCount();
	for (unsigned int i = 0; i < img_paths.size(); i++)
	{
		cout << "Process " << i+1 << "/" << img_paths.size() << endl;
		imgs.push_back(imread(img_paths[i].relative_path().string()));

		//---------line detection------------//
		int num_lines;
		double *img_d = util.Mat2Double(imgs[i]);
		double *lines = proc.lineDetection(img_d, imgs[i].rows, imgs[i].cols, num_lines, th_dist_merge, th_gap_merge, th_short);

		lines = removeOutOfRangeLines(guideImage, lines, num_lines);

		//---------line feature computation------------//
		LineFeature *lfs = new LineFeature[num_lines];
        Mat K_temp = K.clone();
		proc.lineFeatureComputation(lines, num_lines, img_d, imgs[i].rows, imgs[i].cols, i, K_temp, lfs);
		lfs_list.push_back(lfs);
		num_lines_list.push_back(num_lines);
        
		delete[] lines;
		delete[] img_d;
        
		//---------compute vanishing point and line clusters------------//
		//vector<LineCluster> lcs = proc.LineClusteringByVP(lfs, num_lines, prevR, e_th, num_trials, K_temp, true);
		vector<LineCluster> lcs = proc.LineClusteringByVP(lfs, num_lines, R_c_ws[i], e_th, num_trials, K_temp, true);
		lcs_list.push_back(lcs);
        Ks.push_back(K_temp);

		if (useEstimatedRotation && i != img_paths.size() - 1) {
			R_c_ws.push_back(R_c_ws[i]);
		}
		
        //--------line matching--------------//
        //if(i <= 1)  // requires three frames for line matching
        //    continue;
        //for(int j=0; j<3; j++)
        //    proc.lineMatchingDP(lcs_list[i-2][j], lcs_list[i-1][j], lcs_list[i][j], th_msld, th_search_range);
	}
    
    // refinement of K and R_c_ws
	cout << "Refining the lines..." << endl;
	proc.refine(R_c_ws, K, lcs_list);
 //   proc.updateWithNewK(lcs_list, lfs_list, num_lines_list, R_c_ws, K, e_th);
 //   for(size_t i=0; i<imgs.size(); i++)   // update Ks
 //       Ks[i] = K.clone();
    cout << "K = \n" << K << endl;
    t = ((double)getTickCount() - t)/getTickFrequency();
    printf("Elapsed time (%d frames): %f sec\n", int(imgs.size()), t);

	// Calibration matrix
	cout << dirpath + "\\K_opt.txt" << endl;
	fout.open(dirpath + "\\K_opt.txt", fstream::out);
	if (fout.is_open()) {
		fout << K.at<double>(0, 0) << " " << K.at<double>(0, 1) << " " << K.at<double>(0, 2) << endl;
		fout << K.at<double>(1, 0) << " " << K.at<double>(1, 1) << " " << K.at<double>(1, 2) << endl;
		fout << K.at<double>(2, 0) << " " << K.at<double>(2, 1) << " " << K.at<double>(2, 2) << endl;
		fout.close();
	}
	else {
		cout << "File is not opened: " << filename << endl;
	}

	CVPDetector vpd;
	for (size_t i = 0; i < img_paths.size(); i++)
	{
		Mat img = imgs[i].clone();
		
		vpd.drawLines("Axes", img, lcs_list[i], false);
		CVPDetector::drawAxes("Axes", img, lcs_list[i][0].vd, lcs_list[i][1].vd, lcs_list[i][2].vd, K);
		/*
		cout << "---Original rotation---" << endl;
		cout << R_c_ws[i].at<double>(0, 0) << " " << R_c_ws[i].at<double>(0, 1) << " " << R_c_ws[i].at<double>(0, 2) << endl;
		cout << R_c_ws[i].at<double>(1, 0) << " " << R_c_ws[i].at<double>(1, 1) << " " << R_c_ws[i].at<double>(1, 2) << endl;
		cout << R_c_ws[i].at<double>(2, 0) << " " << R_c_ws[i].at<double>(2, 1) << " " << R_c_ws[i].at<double>(2, 2) << endl;
		
		cout << "---Corrected rotation---" << endl;
		cout << lcs_list[i][0].vd[0] << " " << lcs_list[i][1].vd[0] << " " << lcs_list[i][2].vd[0] << endl;
		cout << lcs_list[i][0].vd[1] << " " << lcs_list[i][1].vd[1] << " " << lcs_list[i][2].vd[1] << endl;
		cout << lcs_list[i][0].vd[2] << " " << lcs_list[i][1].vd[2] << " " << lcs_list[i][2].vd[2] << endl;*/
		
		//---------save result------------//
		//sprintf(filename, "%s\\frame%03d.png", dirpath.c_str(), i + 1);
		//imwrite(filename, imgs[i]);

		vpd.drawVanishingPoints("VP", img, lcs_list[i][0].vd, lcs_list[i][1].vd, lcs_list[i][2].vd, K);

		sprintf(filename, "%s\\%s_VD.png", dirpath.c_str(), img_paths[i].stem().string().c_str());
		//sprintf(filename, "%s\\frame%03d_VD.png", dirpath.c_str(), i + 1);
		imwrite(filename, img);

		// Vanishing points
		sprintf(filename, "%s\\%s_VD.txt", dirpath.c_str(), img_paths[i].stem().string().c_str());
		//sprintf(filename, "%s\\frame%03d_VD.txt", dirpath.c_str(), i + 1);
		fout.open(filename, fstream::out);
		//fout << R_c_ws[i].at<double>(0, 0) << " " << R_c_ws[i].at<double>(0, 1) << " " << R_c_ws[i].at<double>(0, 2) << endl;
		//fout << R_c_ws[i].at<double>(1, 0) << " " << R_c_ws[i].at<double>(1, 1) << " " << R_c_ws[i].at<double>(1, 2) << endl;
		//fout << R_c_ws[i].at<double>(2, 0) << " " << R_c_ws[i].at<double>(2, 1) << " " << R_c_ws[i].at<double>(2, 2) << endl;
		fout << lcs_list[i][0].vd[0] << " " << lcs_list[i][1].vd[0] << " " << lcs_list[i][2].vd[0] << endl;
		fout << lcs_list[i][0].vd[1] << " " << lcs_list[i][1].vd[1] << " " << lcs_list[i][2].vd[1] << endl;
		fout << lcs_list[i][0].vd[2] << " " << lcs_list[i][1].vd[2] << " " << lcs_list[i][2].vd[2] << endl;
		fout.close();
	}

    for(int i=0; i<lcs_list.size(); i++)
    {
        for(int j=0; j<lcs_list[i].size(); j++)
            lcs_list[i][j].clear();
    }
    for(int i=0; i<lfs_list.size(); i++)
    {
        for(int j=0; j<num_lines_list[i]; j++)
            lfs_list[i][j].clear();
        delete[] lfs_list[i];
    }

	system("pause");
}

int main()
{
	individualRefinement("..\\WACV2017\\Images");
	
	//refinement("..\\Images\\UCSC\\EB1Corridor4", ".png", true);

	//refinement("..\\MichiganMilanIndoorDataset\\galaxyS2_720x480_dataset_Entrance_1",
	//	"..\\MichiganMilanIndoorDataset\\galaxyS2_720x480_dataset_Entrance_1\\frame00001_R.txt", ".png");

	return EXIT_SUCCESS;
}
