#pragma once

#include <opencv2/opencv.hpp>

#include "types.h"
#include "Utility.h"

using namespace std;
using namespace cv;

class CTR
{
public:
    CTR(void);
    ~CTR(void);
    
    
    vector<JTD> computeTd(LineFeature **chains[3], int C[3], int F, int idx_e[3], Point *pf_idx, int PF, vector<Mat> Ks, vector<Mat> R_c_ws, vector<vector<LineCluster>> &lcs_list);
    void computeTd(LineFeature **lfs1, LineFeature **lfs2, int N, Mat R_c1_c2, double n[3], double td[3]);
    
    void lineCorrection(LineFeature *lf, double vd[3], Mat K);
    
    // geometric constraits test
    bool geometricFiltering(LineFeature **lfs1, LineFeature **lfs2, int N, int idx_e[3], vector<LineCluster> &lcs1, vector<LineCluster> &lcs2, Mat &K1, Mat &K2);
    void getFourLines(LineFeature *lines[2], double vp[3], Mat K, LineFeature ext_lines[4], bool complete);
    void getEndPoints(Point2d *pts, int n);
    bool existPerpendicularLine(LineFeature lines_img1[4], LineFeature lines_img2[4],
                                LineCluster &line_cluster_y_img1, LineCluster &line_cluster_y_img2, Mat Kinv_img1, Mat Kinv_img2,
                                double vd_z_img1[3], double vd_z_img2[3], int min_num_lines_in);
    bool isInRect(double pt[2], LineFeature lines[4], Mat Kinv);
    
    CUtility m_util;
};