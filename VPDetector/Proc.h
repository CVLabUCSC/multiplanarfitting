
#pragma once

#include <opencv2/opencv.hpp>

#include <string>

#include "types.h"
#include "Utility.h"

#include "ceres/ceres.h"
#include "ceres/rotation.h"
#include "glog/logging.h"

using ceres::AutoDiffCostFunction;
using ceres::CostFunction;
using ceres::Problem;
using ceres::Solver;
using ceres::Solve;


// Refinement of focal length and camera rotations
struct Residual{
    Residual(double lx, double ly, double lz, double tx, double ty, int dir)
    : lx(lx), ly(ly), lz(lz), tx(tx), ty(ty), dir(dir) {}   // constructor
    
    template <typename T>
    bool operator()(const T* const x, const T* f, T* residuals) const {
        
        T rot[3] = {x[0], x[1], x[2]};
        T R[9]; // (in column major order)
        ceres::AngleAxisToRotationMatrix(rot, R);
        
        T K[9] = {f[0], T(0), T(tx), T(0), f[0], T(ty), T(0), T(0), T(1)};
        T l[3] = {T(lx), T(ly), T(lz)};
        // u = K.t() * l -> lever vector
        T ux = K[0]*l[0] + K[3]*l[1] + K[6]*l[2];
        T uy = K[1]*l[0] + K[4]*l[1] + K[7]*l[2];
        T uz = K[2]*l[0] + K[5]*l[1] + K[8]*l[2];
        
		// normalize the lever vector
        T leng = sqrt(ux*ux + uy*uy + uz*uz);
        ux /= leng;
        uy /= leng;
        uz /= leng;
        
        residuals[0] = R[3*dir]*ux + R[3*dir+1]*uy + R[3*dir+2]*uz;
        
        return true;
    }
    
    static ceres::CostFunction* Create(const double lx, const double ly, const double lz, const double tx, const double ty, const int dir) {
        return (new ceres::AutoDiffCostFunction<Residual, 1, 3, 1>(new Residual(lx, ly, lz, tx, ty, dir)));
    }
    
    double lx, ly, lz; // line normal vector
    double tx, ty;     // image center
    int dir;           // direction of line (0, 1, 2) == (x, y, z) direction
    
};


struct Residual2{
    // ex, ey: one of the line end points
    // mx, my: center of the line
    // tx, ty: principle point in the image e.g. (320, 240)
    // dir: x(0), y(1), z(2) direction
    Residual2(double ex, double ey, double mx, double my, double tx, double ty, int dir)
    : ex(ex), ey(ey), mx(mx), my(my), tx(tx), ty(ty), dir(dir) {}
    
    template <typename T>
    bool operator()(const T* const x, T* residuals) const {
        
        T rot[3] = {x[0], x[1], x[2]};
        T R[9]; // (in column major order)
        ceres::AngleAxisToRotationMatrix(rot, R);
        
        T K[9] = {x[3], T(0), T(tx), T(0), x[3], T(ty), T(0), T(0), T(1)};
        
        T vp[3];
        vp[0] = K[0]*R[3*dir] + K[1]*R[3*dir+1] + K[2]*R[3*dir+2];
        vp[1] = K[3]*R[3*dir] + K[4]*R[3*dir+1] + K[5]*R[3*dir+2];
        vp[2] = K[6]*R[3*dir] + K[7]*R[3*dir+1] + K[8]*R[3*dir+2];
        
        T m[3] = {T(mx), T(my), T(1)};
        T l[3];
        
        l[0] = -vp[2]*m[1] + vp[1]*m[2];
        l[1] =  vp[2]*m[0] - vp[0]*m[2];
        l[2] = -vp[1]*m[0] + vp[0]*m[1];
        
        T absquare = sqrt(l[0]*l[0] + l[1]*l[1]);
        T temp = (l[0]*ex + l[1]*ey + l[2])/absquare;
        
        residuals[0] = sqrt(temp*temp);
        
        return true;
    }
    
    static ceres::CostFunction* Create(const double ex, const double ey, const double mx, const double my, const double tx, const double ty, const int dir) {
        return (new ceres::AutoDiffCostFunction<Residual2, 1, 4>(new Residual2(ex, ey, mx, my, tx, ty, dir)));
    }
    
    double ex, ey, mx, my, tx, ty;
    int dir;
};


using namespace std;
using namespace cv;

class CProc
{
public:
    CProc(void);
    ~CProc(void);
    
    // LineDetection
    double* lineDetection(double* img_d, int rows, int cols, int &num_lines, double th_dist_merge, double th_gap_merge, int th_short);
    double* lineMerging(double *lines, int &num_lines, double th_dist, double th_gap);
    double  lineMerging(double s1[2], double e1[2], double s2[2], double e2[2], double *new_s, double *new_e);
    
    // LineFeature computations
    void lineFeatureComputation(double* lines, int num_lines, double *image, int rows, int cols, int frame_idx, Mat K, LineFeature *lfs);
    double* MSLD(LineFeature *line, double* image, int rows, int cols, double ln[2], int S, int N, int W, double th);
    void getLineAxis(LineFeature *line, double* image, int rows, int cols, double lx[2], double ly[2]);
    
    // LineClustering
    vector<LineCluster> LineClusteringByVP(LineFeature* line_features, int num_lines, Mat &R_c_w, double e_th, int num_trials, Mat &K, bool bRefine, bool bSort = true);
    void updateWithNewK(vector<vector<LineCluster>> &lcs_list, vector<LineFeature*> lfs_list, vector<int> num_lines_list, vector<Mat> &R_c_ws, Mat K, double e_th, bool bSort = true);
    double alignError(LineFeature line, double *vd, Mat K);
    void sortLines(LineCluster line_cluster, Mat Kinv, double ref_normal_vector[3]);
    void aboveBelowHorizon(LineCluster line_cluster, double horizon_n[3], Mat Kinv);
    void refine(Mat &R_c_w, Mat &K, LineCluster lc_x, LineCluster lc_y, LineCluster lc_z);
    void refine2(Mat &R_c_w, Mat &K, LineCluster lc_x, LineCluster lc_y, LineCluster lc_z);
    void refine(vector<Mat> &R_c_ws, Mat &K, vector<vector<LineCluster>> &lcs_list);
    
    // LineMatching
    void lineMatchingDP(LineCluster &line_cluster1, LineCluster &line_cluster2, LineCluster &line_cluster3, double MSLD_th, double th_search_range);
    void lineMatchingDP(LineCluster &line_cluster1, LineCluster &line_cluster2, double MSLD_th, double th_search_range);
    void rearrange(LineCluster &line_cluster);
    double* computeMatches(LineCluster &line_cluster1, LineCluster &line_cluster2, double MSLD_th, double th_search_range);
    Match* removeEmptyLines(double *M, int N1, int N2, int &N1_new, int &N2_new);
    vector<Match*> removeEmptyLines(double *M21, double *M23, int N1, int N2, int N3, int &N1_new, int &N2_new, int &N3_new);
    double pairwiseSimilarity(LineFeature *line1a, LineFeature *line1b,  LineFeature *line2a, LineFeature *line2b);
    
    // build line chains
    void buildLineChains(vector<vector<LineCluster>> lcs_list, vector<vector<LineID>> chains[3], double s);
    LineFeature** buildLineChains(vector<vector<LineCluster>> lcs_list, int dir, int &num_chains, double s);

    // Drawing
    void drawLineFeatures(string name, Mat img, LineFeature *lfs, int num_lines);
    void drawLineClusters(string name, Mat img, vector<LineCluster> lcs);
    void drawLineMatches(string name1, Mat img1, string name2, Mat img2, vector<LineCluster> lcs1, vector<LineCluster> lcs2);
    void drawLineMatches(string name1, Mat img1, string name2, Mat img2, string name3, Mat img3, vector<LineCluster> lcs1, vector<LineCluster> lcs2, vector<LineCluster> lcs3);
    void drawLineLineChains(string name1, Mat img1, int frame1, string name2, Mat img2, int frame2, vector<vector<LineID>> chains[3]);
    void drawLineLineChains(string name1, Mat img1, int frame1, string name2, Mat img2, int frame2, LineFeature** chains[3], int num_chains[3], int num_frames);
    
    
    // misc.
    Mat getR_c_wFromFile(string f_R_b_a);
    
    CUtility m_util;
};

