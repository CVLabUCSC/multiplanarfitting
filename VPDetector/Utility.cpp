#include "Utility.h"
#include <stdio.h>

CUtility::CUtility(void)
{
}


CUtility::~CUtility(void)
{
}


void CUtility::writeData(const char *filename, float *data, int rows, int cols)
{
	FILE *fp;
	fp = fopen(filename, "w");
	if(!fp)
	{
		printf("%s doesn't exist\n", filename);
		return;
	}
	for(int i=0; i<rows; i++)
	{
		int j;
		for(j=0; j<cols-1; j++)
           	fprintf(fp, "%e\t", data[i*cols+j]);
        fprintf(fp, "%e", data[i*cols+j]);
        
		fprintf(fp, "\n");
	}
	fclose(fp);
}

void CUtility::writeData(const char *filename, double *data, int rows, int cols)
{
	FILE *fp;
	fp = fopen(filename, "w");
	if(!fp)
	{
		printf("%s doesn't exist\n", filename);
		return;
	}
	for(int i=0; i<rows; i++)
	{
        int j;
		for(j=0; j<cols-1; j++)
           	fprintf(fp, "%e\t", data[i*cols+j]);
        fprintf(fp, "%e", data[i*cols+j]);
        
		fprintf(fp, "\n");
	}
	fclose(fp);
}

void CUtility::writeData(const char *filename, int *data, int rows, int cols)
{
	FILE *fp;
	fp = fopen(filename, "w");
	if(!fp)
	{
		printf("%s doesn't exist\n", filename);
		return;
	}
	for(int i=0; i<rows; i++)
	{
		int j;
		for(j=0; j<cols-1; j++)
           	fprintf(fp, "%d\t", data[i*cols+j]);
        fprintf(fp, "%d", data[i*cols+j]);
        
		fprintf(fp, "\n");
	}
	fclose(fp);
}

void CUtility::writeData(const char *filename, double **data, int rows, int cols)
{
	FILE *fp;
	fp = fopen(filename, "w");
	if(!fp)
	{
		printf("%s doesn't exist\n", filename);
		return;
	}
	for(int i=0; i<rows; i++)
	{
        int j;
		for(j=0; j<cols-1; j++)
           	fprintf(fp, "%e\t", data[i][j]);
        fprintf(fp, "%e", data[i][j]);

		fprintf(fp, "\n");
	}
	fclose(fp);
}

void CUtility::writeData(const char *filename, Mat data)
{
	FILE *fp;
	fp = fopen(filename, "w");
	if(!fp)
	{
		printf("%s doesn't exist\n", filename);
		return;
	}
	for(int i=0; i<data.rows; i++)
	{
        int j;
		for(j=0; j<data.cols-1; j++)
           	fprintf(fp, "%e\t", data.at<double>(i,j));
        fprintf(fp, "%e", data.at<double>(i,j));
        
		fprintf(fp, "\n");
	}
	fclose(fp);
}

void CUtility::writeDataAppend(const char *filename, Mat data)
{
	FILE *fp;
	fp = fopen(filename, "a+");
	if(!fp)
	{
		printf("%s doesn't exist\n", filename);
		return;
	}
	for(int i=0; i<data.rows; i++)
	{
        int j;
		for(j=0; j<data.cols-1; j++)
           	fprintf(fp, "%e\t", data.at<double>(i,j));
        fprintf(fp, "%e", data.at<double>(i,j));
        
		fprintf(fp, "\n");
	}
	fclose(fp);
}

void CUtility::writeDataF(const char *filename, Mat data)
{
	FILE *fp;
	fp = fopen(filename, "w");
	if(!fp)
	{
		printf("%s doesn't exist\n", filename);
		return;
	}
	for(int i=0; i<data.rows; i++)
	{
        int j;
		for(j=0; j<data.cols-1; j++)
           	fprintf(fp, "%e\t", data.at<float>(i,j));
        fprintf(fp, "%e", data.at<float>(i,j));
        
		fprintf(fp, "\n");
	}
	fclose(fp);
}

void CUtility::writeData(const char *filename, double *x, double *y, double *z, int num_data)
{
	FILE *fp;
	fp = fopen(filename, "w");
	if(!fp)
	{
		printf("%s doesn't exist\n", filename);
		return;
	}
	for(int i=0; i<num_data; i++)
		fprintf(fp, "%e\t%e\t%e\n", x[i], y[i], z[i]);
	fclose(fp);
}

void CUtility::writeData(const char *filename, vector<Point3f> data)
{
	FILE *fp;
	fp = fopen(filename, "w");
	if(!fp)
	{
		printf("%s doesn't exist\n", filename);
		return;
	}
	for(int i=0; i<int(data.size()); i++)
	{
		fprintf(fp, "%e\t%e\t%e", data[i].x, data[i].y, data[i].z);
		fprintf(fp, "\n");
	}
	fclose(fp);

}

void CUtility::writeData(const char *filename, vector<Point3d> data)
{
	FILE *fp;
	fp = fopen(filename, "w");
	if(!fp)
	{
		printf("%s doesn't exist\n", filename);
		return;
	}
	for(int i=0; i<int(data.size()); i++)
	{
		fprintf(fp, "%e\t%e\t%e", data[i].x, data[i].y, data[i].z);
		fprintf(fp, "\n");
	}
	fclose(fp);

}

void CUtility::writeData(const char *filename, vector<int> data)
{
	FILE *fp;
	fp = fopen(filename, "w");
	if(!fp)
	{
		printf("%s doesn't exist\n", filename);
		return;
	}
	for(int i=0; i<int(data.size()); i++)
		fprintf(fp, "%d\n", data[i]);
	
	fclose(fp);

}

void CUtility::writeData(const char *filename, int data)
{
	FILE *fp;
	fp = fopen(filename, "w");
	if(!fp)
	{
		printf("%s doesn't exist\n", filename);
		return;
	}
	fprintf(fp, "%d\n", data);
	
	fclose(fp);

}

void CUtility::writeData(const char *filename, vector<vector<uchar> > data)
{
	FILE *fp;
	fp = fopen(filename, "w");
	if(!fp)
	{
		printf("%s doesn't exist\n", filename);
		return;
	}
	for(int i=0; i<int(data.size()); i++)
	{
		for(int j=0; j<int(data[i].size()); j++)
			fprintf(fp, "%d\t", data[i][j]);
		fprintf(fp, "\n");
	}
	
	fclose(fp);

}

void CUtility::printData(Mat data)
{
	for(int i=0; i<data.rows; i++)
	{
		for(int j=0; j<data.cols; j++)
			printf("%e\t", data.at<double>(i,j));
		printf("\n");
	}
	printf("\n");
}

void CUtility::printData(double *data, int rows, int cols)
{
	for(int i=0; i<rows; i++)
	{
		for(int j=0; j<cols; j++)
			printf("%e\t", data[i*cols+j]);
		printf("\n");
	}
	printf("\n");
}

bool CUtility::readData(const char *filename, vector<int> &data)
{
	FILE *fp;
	fp = fopen(filename, "r");
	char buffer[100];

	if(!fp)
		return false;

	while(fgets(buffer,100,fp))
		data.push_back(atoi(buffer));
				
	fclose(fp);

	return true;
}

void CUtility::mul(Mat M, double a[3], double b[3])
{
    double temp[3];
    memcpy(temp, a, sizeof(double)*3);
    double *ptr = M.ptr<double>();    
    
    b[0] = ptr[0] * temp[0] + ptr[1] * temp[1] + ptr[2] * temp[2];
    b[1] = ptr[3] * temp[0] + ptr[4] * temp[1] + ptr[5] * temp[2];
    b[2] = ptr[6] * temp[0] + ptr[7] * temp[1] + ptr[8] * temp[2];
}

// axb should not be equal to a and b
void CUtility::crossProd(double *a, double *b, double *axb)
{
    axb[0] = -a[2]*b[1] + a[1]*b[2];
	axb[1] =  a[2]*b[0] - a[0]*b[2];
	axb[2] = -a[1]*b[0] + a[0]*b[1];
}

void CUtility::unitVector(double *x, double* y, int dim)
{
    double length;
    
	length = 0;
	for(int i=0; i<dim; i++)
		length += x[i]*x[i];
	length = sqrt(length);
    
	if(length)
	{
		for(int i=0; i<dim; i++)
			y[i] = x[i] / length;
	}
	else
		memcpy(y, x, sizeof(double)*dim);
}


void CUtility::unitize(double a1, double b1, double &a, double &b)
{
	double length;
	length = sqrt(a1*a1 + b1*b1);
    
	a = a1 / length;
	b = b1 / length;
}




Point3d CUtility::crossProd(Point3d a, Point3d b)
{
	Point3d pt;
	pt.x = -a.z*b.y + a.y*b.z;
	pt.y =  a.z*b.x - a.x*b.z;
	pt.z = -a.y*b.x + a.x*b.y;
    
	pt.x /= pt.z;
	pt.y /= pt.z;
	pt.z = 1.0;
    
	return pt;
    
}

double CUtility::innerProd(double *a, double *b, int dim)
{
	double sum;
	sum = 0;
	for(int i=0; i<dim; i++)
	{
		sum += a[i]*b[i];
	}
	return sum;
}



double CUtility::EcDistance(double *a, double *b, int dim)
{
	double dist = 0;
    
	for(int i=0; i<dim; i++)
		dist += (a[i]-b[i])*(a[i]-b[i]);
	dist = sqrt(dist);
	return dist;
}

// all the input vectors are stored as rows of the samples matrix.
Mat CUtility::cov(double *A, int num_data, int dim)
{
    Mat mu = Mat::zeros(dim, 1, CV_64F);
    Mat x_mu = Mat(dim,1,CV_64F);
    Mat S = Mat::zeros(dim, dim, CV_64F);
    Mat X = Mat(dim,num_data,CV_64F);
    
    for(int i=0; i<num_data; i++)
    {
        for(int j=0; j<dim; j++)
        {
            X.at<double>(j,i) = A[dim*i+j];
            mu.at<double>(j) += A[dim*i+j];
        }
    }
    mu /= num_data;
    
    for(int i=0; i<num_data; i++)
    {
        x_mu = X.col(i) - mu;
        S += x_mu * x_mu.t();
    }
    
    S /= (num_data - 1);
    
    return S;   
}

// line[3]:		line in homogeneous representation
// m[2]:		point on the line
// pt[2]:		given point not on the line
// pt_dest[2]:	closest point on the line
void CUtility::findClosestPointOnALine(double line[3], double m[2], double pt[2], double pt_dest[2])
{
	double mp[2], n[2], length;
    
	mp[0] = pt[0] - m[0];
	mp[1] = pt[1] - m[1];
    
	// n: perpendicular direction to the line
	length = sqrt(line[0]*line[0] + line[1]*line[1]);
	n[0] = line[0] / length;
	n[1] = line[1] / length;
    
	length = innerProd(mp, n, 2);
	pt_dest[0] = pt[0] - length*n[0];
	pt_dest[1] = pt[1] - length*n[1];
}

void CUtility::orthonormalVector(double *x, double *y)
{
    if(fabs(x[0]) > INFINITE_TEST_MIN)
    {
        y[1] = 1;
        y[2] = 1;
        y[0] = -(x[1]+x[2])/x[0];
    }
    else if(fabs(x[1]) > INFINITE_TEST_MIN)
    {
        y[0] = 1;
        y[2] = 1;
        y[1] = -(x[0]+x[2])/x[1];
    }
    else if(fabs(x[2]) > INFINITE_TEST_MIN)
    {
        y[0] = 1;
        y[1] = 1;
        y[2] = -(x[0]+x[1])/x[2];
    }
    unitVector(y, y, 3);
    
    
}

double CUtility::distPoint2Line(double line[3], double p[2])
{
    double absquare = sqrt(line[0]*line[0] + line[1]*line[1]);
    double dist = fabs(line[0]*p[0] + line[1]*p[1] + line[2])/absquare;
    return dist;
}


void CUtility::perpendicularVector(double a[2], double b[2])
{
    b[0] = a[1];
    b[1] = -a[0];
    
//    if(fabs(a[0]) > INFINITE_TEST_MIN){
//        b[0] = - a[1] / a[0];
//        b[1] = 1;
//    }
//    else{
//        b[1] = -a[0] / a[1];
//        b[0] = 1;
//    }
    unitVector(b, b, 2);
}


void CUtility::normalizeLine(double *line)
{
    double len = sqrt(line[0]*line[0] +	line[1]*line[1]);
    line[0] /= len;
    line[1] /= len;
    line[2] /= len;
    if(line[2] < 0)
    {
        line[0] *= -1;
        line[1] *= -1;
        line[2] *= -1;
    }		
}

double* CUtility::Mat2Double(Mat img)
{
    // Mat to double
    double *img_d = new double[img.rows*img.cols];
    double a = 1.0/3.0;
    
    uchar* p = img.data;
    for(int i=0; i<img.rows; i++){
        for(int j=0; j<img.cols; j++){
            int idx = i*img.cols + j;
            int idx2 = 3*idx;
            img_d[idx] = (p[idx2] + p[idx2+1] + p[idx2+2])*a; // a is 1/3, average of R, G, and B values
        }
    }
    return img_d;
}










