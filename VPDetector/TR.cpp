#include "TR.h"

CTR::CTR(void)
{
}


CTR::~CTR(void)
{
}


// This function computes t/d's from all possible four line matches between a pair of image.
// All image pairs defined in pf_idx array are considered.
// INPUT:
//  chains[3]: Each element corresponds to one of the principle directions, x, y, and z.
//             Eahc element is a 2d matrix and chains[][i*F + j] is the i-th line feature in j-th image
//             frame, where F is the number of the image frames.
//  C[3]:      The number of chains in each direction
//  F:         The number of the image frames
//  idx_e[3]:  The first two elements are the directions along two orthogonal lines lying on the plane of interest
//             E.g. If lines in x and z directions are used for computing t/d, then
//             idx_e[3] = {0, 2, 1};
//  pt_idx:    This contains indices of image pairs in Point structure.
//  PF:        The number of image pairs considered.
//  lcs_list:  double array of line clusters. lcs_list[i][j] is the line cluster in j direction (one of x, y, z) in i-th image
vector<JTD> CTR::computeTd(LineFeature **chains[3], int C[3], int F, int idx_e[3], Point *pf_idx, int PF, vector<Mat> Ks, vector<Mat> R_c_ws, vector<vector<LineCluster>> &lcs_list)
{
    vector<JTD> jtds;
    // For each possible four line matches,
    for(int i=0; i<C[ idx_e[0] ]-1; i++){
        for(int j=i+1; j<C[ idx_e[0] ]; j++){
            
            for(int k=0; k<C[ idx_e[1] ]-1; k++){
                for(int n=k+1; n<C[ idx_e[1] ]; n++){
                    
                    // For all image pairs in pf_idx;
                    JTD jtd;    jtd.init(PF);
                    
                    for(int m=0; m<PF; m++){
                        LineFeature* lfs1[4], *lfs2[4]; // Four LineFeatures in the first and second image.
                        
                        lfs1[0] = chains[ idx_e[0] ][i*F + pf_idx[m].x];    if(lfs1[0] == NULL) continue;
                        lfs1[1] = chains[ idx_e[0] ][j*F + pf_idx[m].x];    if(lfs1[1] == NULL) continue;
                        lfs1[2] = chains[ idx_e[1] ][k*F + pf_idx[m].x];    if(lfs1[2] == NULL) continue;
                        lfs1[3] = chains[ idx_e[1] ][n*F + pf_idx[m].x];    if(lfs1[3] == NULL) continue;
                        
                        lfs2[0] = chains[ idx_e[0] ][i*F + pf_idx[m].y];    if(lfs2[0] == NULL) continue;
                        lfs2[1] = chains[ idx_e[0] ][j*F + pf_idx[m].y];    if(lfs2[1] == NULL) continue;
                        lfs2[2] = chains[ idx_e[1] ][k*F + pf_idx[m].y];    if(lfs2[2] == NULL) continue;
                        lfs2[3] = chains[ idx_e[1] ][n*F + pf_idx[m].y];    if(lfs2[3] == NULL) continue;
                        
//                        // line normal vector correction
//                        for(int mm=0; mm<2; mm++)
//                            lineCorrection(lfs1[mm], lcs_list[pf_idx[m].x][idx_e[0]].vd, Ks[pf_idx[m].x]);
//                        
//                        for(int mm=2; mm<4; mm++)
//                            lineCorrection(lfs1[mm], lcs_list[pf_idx[m].x][idx_e[1]].vd, Ks[pf_idx[m].x]);
//                        
//                        for(int mm=0; mm<2; mm++)
//                            lineCorrection(lfs2[mm], lcs_list[pf_idx[m].y][idx_e[0]].vd, Ks[pf_idx[m].y]);
//                        
//                        for(int mm=2; mm<4; mm++)
//                            lineCorrection(lfs2[mm], lcs_list[pf_idx[m].y][idx_e[1]].vd, Ks[pf_idx[m].y]);
                        
                        // Test geometric constraint
                        if(!geometricFiltering(lfs1, lfs2, 4, idx_e, lcs_list[pf_idx[m].x], lcs_list[pf_idx[m].y], Ks[pf_idx[m].x], Ks[pf_idx[m].y]))
                            continue;
                        
                        // compute t/d
                        Mat R_c1_c2 = R_c_ws[pf_idx[m].x] * R_c_ws[pf_idx[m].y].t();
                        double n[3] = {R_c_ws[pf_idx[m].x].at<double>(0, idx_e[2]), R_c_ws[pf_idx[m].x].at<double>(1, idx_e[2]),
                                       R_c_ws[pf_idx[m].x].at<double>(2, idx_e[2])};    // Plane normal vector
                        
                        
                        computeTd(lfs1, lfs2, 4, R_c1_c2, n, jtd.tds[m].td);
                        memcpy(&jtd.td_array[3*m], jtd.tds[m].td, sizeof(double)*3);
                        memcpy(jtd.tds[m].lfs1, lfs1, sizeof(LineFeature*)*4);
                        memcpy(jtd.tds[m].lfs2, lfs2, sizeof(LineFeature*)*4);
                    }
                    jtds.push_back(jtd);
                }
            }
        }
    }
    
    return jtds;
}

// DLT solution of t/d given K, R, n, four line matches
// (R_c1_c2 u_2) x u_1 = u_1 x n u_2^T t/d
// lfs1:    The array containing pointers of line features in image1
// lfs2:    The array containing pointers of line features in image2
// N:       The number of line matches, should be greater than 2
// R_c1_c2
// n[3]:    Plane normal w.r.t the first camera reference frame, unit vector
// td[3]:   t/d
void CTR::computeTd(LineFeature **lfs1, LineFeature **lfs2, int N, Mat R_c1_c2, double n[3], double td[3])
{
    Mat b = Mat(N*3, 1, CV_64F);
    Mat A = Mat(N*3, 3, CV_64F);
    
    for(int i=0; i<N; i++){
        double Ru2[3];
        m_util.mul(R_c1_c2, lfs2[i]->line_norm, Ru2);
        double Ru2xu1[3];
        m_util.crossProd(Ru2, lfs1[i]->line_norm, Ru2xu1);
        b.at<double>(i*3  , 1) = Ru2xu1[0];
        b.at<double>(i*3+1, 1) = Ru2xu1[1];
        b.at<double>(i*3+2, 1) = Ru2xu1[2];
        
        double u1xn[3];
        m_util.crossProd(lfs1[i]->line_norm, n, u1xn);
        A.at<double>(i*3  ,0) = lfs2[i]->line_norm[0] * u1xn[0];
        A.at<double>(i*3+1,0) = lfs2[i]->line_norm[0] * u1xn[1];
        A.at<double>(i*3+2,0) = lfs2[i]->line_norm[0] * u1xn[2];
        
        A.at<double>(i*3  ,1) = lfs2[i]->line_norm[1] * u1xn[0];
        A.at<double>(i*3+1,1) = lfs2[i]->line_norm[1] * u1xn[1];
        A.at<double>(i*3+2,1) = lfs2[i]->line_norm[1] * u1xn[2];
        
        A.at<double>(i*3  ,2) = lfs2[i]->line_norm[2] * u1xn[0];
        A.at<double>(i*3+1,2) = lfs2[i]->line_norm[2] * u1xn[1];
        A.at<double>(i*3+2,2) = lfs2[i]->line_norm[2] * u1xn[2];
    }
    Mat AtA_inv;
    invert(A.t()*A, AtA_inv);
    Mat est = AtA_inv*A.t()*b;
    
    td[0] = est.at<double>(0);
    td[1] = est.at<double>(1);
    td[2] = est.at<double>(2);
}

void CTR::lineCorrection(LineFeature *lf, double vd[3], Mat K)
{
    double m[3] = {lf->m[0], lf->m[1], 1.0};
    double ray[3];
    m_util.mul(K.inv(), m, ray);
    m_util.crossProd(ray, vd, lf->line_norm);
    m_util.unitVector(lf->line_norm, lf->line_norm, 3);
}


// lfs1,2:  The first two of line matches should along one direction (e.g x) and the rest ones should along
//          the orthogonal direction (e.g. z). E.g: lfs1 = [lf_a_x, lf_b_x, lf_a_z, lf_b_z] in the first image
// N:   The number of line matches, should be 2 or 3 or 4
// idx_e[3]:  The first two elements are the directions along two orthogonal lines lying on the plane of interest
//             E.g. If lines in x and z directions are used for computing t/d, then
// lcs1:  line clusters of the first image
// lcs2:  line clusters of the second image
bool CTR::geometricFiltering(LineFeature **lfs1, LineFeature **lfs2, int N, int idx_e[3], vector<LineCluster> &lcs1, vector<LineCluster> &lcs2, Mat &K1, Mat &K2)
{
    // paramters
    double th_dist = 30;
    double dist = 0;
    dist = m_util.distPoint2Line(lfs1[0]->line, lfs1[1]->s);
    dist += m_util.distPoint2Line(lfs1[0]->line, lfs1[1]->e);
    if(dist < th_dist)
        return false;
    dist = m_util.distPoint2Line(lfs2[0]->line, lfs2[1]->s);
    dist += m_util.distPoint2Line(lfs2[0]->line, lfs2[1]->e);
    if(dist < th_dist)
        return false;
    
    if(N == 4){
        dist = m_util.distPoint2Line(lfs1[2]->line, lfs1[3]->s);
        dist += m_util.distPoint2Line(lfs1[2]->line, lfs1[3]->e);
        if(dist < th_dist)
            return false;
        dist = m_util.distPoint2Line(lfs2[2]->line, lfs2[3]->s);
        dist += m_util.distPoint2Line(lfs2[2]->line, lfs2[3]->e);
        if(dist < th_dist)
            return false;
    }
    
    
    LineFeature ext_lfs_a1[4], ext_lfs_a2[4];

    getFourLines(lfs1, lcs1[idx_e[2]].vp, K1, ext_lfs_a1, true);
    getFourLines(lfs2, lcs2[idx_e[2]].vp, K2, ext_lfs_a2, true);
    // idx_e[1] is always 2 .. fix this later.
    if(existPerpendicularLine(ext_lfs_a1, ext_lfs_a2, lcs1[idx_e[2]], lcs2[idx_e[2]], K1.inv(), K2.inv(), lcs1[idx_e[1]].vd, lcs2[idx_e[1]].vd, 2))
    {
       return false;
    }
    
    if(N==4){
        LineFeature ext_lfs_b1[4], ext_lfs_b2[4];
        getFourLines(&lfs1[2], lcs1[idx_e[2]].vp, K1, ext_lfs_b1, true);
        getFourLines(&lfs2[2], lcs2[idx_e[2]].vp, K2, ext_lfs_b2, true);
        // idx_e[1] is always 2 .. fix this later.
        if(existPerpendicularLine(ext_lfs_b1, ext_lfs_b2, lcs1[idx_e[2]], lcs2[idx_e[2]], K1.inv(), K2.inv(), lcs1[idx_e[1]].vd, lcs2[idx_e[1]].vd, 2))
        {
            return false;
        }

    }
    
    
    
    
    return true;
}

void CTR::getFourLines(LineFeature *lines[2], double vp[3], Mat K, LineFeature ext_lines[4], bool complete)
{
    double line[3], pt[3], sp[3], ep[3], fourpts[4][3];
    Point2d pts[4];
    
    // find a intersection point between lines[1] and a line joining lines[0]->s and vp
    pt[0] = lines[0]->s[0];
    pt[1] = lines[0]->s[1];
    pt[2] = 1;
    m_util.crossProd(vp, pt, line);
    
    m_util.crossProd(lines[1]->line, line, sp);
    sp[0] /= sp[2];
    sp[1] /= sp[2];
    sp[2] = 1;
    
    // find a intersection point between lines[1] and a line joining lines[0]->e and vp
    pt[0] = lines[0]->e[0];
    pt[1] = lines[0]->e[1];
    pt[2] = 1;
    m_util.crossProd(vp, pt, line);
    
    m_util.crossProd(lines[1]->line, line, ep);
    ep[0] /= ep[2];
    ep[1] /= ep[2];
    ep[2] = 1;
    
    // find farthest point between lines[1]->s, lines[1]->e, sp, ep
    pts[0].x = lines[1]->s[0];
    pts[0].y = lines[1]->s[1];
    pts[1].x = lines[1]->e[0];
    pts[1].y = lines[1]->e[1];
    pts[2].x = sp[0];
    pts[2].y = sp[1];
    pts[3].x = ep[0];
    pts[3].y = ep[1];
    getEndPoints(pts, 4);
    
    // copy the two farthest point to result array
    fourpts[0][0] = pts[0].x;
    fourpts[0][1] = pts[0].y;
    fourpts[0][2] = 1.0;
    fourpts[1][0] = pts[1].x;
    fourpts[1][1] = pts[1].y;
    fourpts[1][2] = 1.0;
    
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    // find a intersection point between lines[0] and a line joining lines[1]->s and vx
    pt[0] = lines[1]->s[0];
    pt[1] = lines[1]->s[1];
    pt[2] = 1;
    m_util.crossProd(vp, pt, line);
    
    m_util.crossProd(lines[0]->line, line, sp);
    sp[0] /= sp[2];
    sp[1] /= sp[2];
    sp[2] = 1;
    
    // find a intersection point between lines[0] and a line joining lines[1]->e and vp
    pt[0] = lines[1]->e[0];
    pt[1] = lines[1]->e[1];
    pt[2] = 1;
    m_util.crossProd(vp, pt, line);
    
    m_util.crossProd(lines[0]->line, line, ep);
    ep[0] /= ep[2];
    ep[1] /= ep[2];
    ep[2] = 1;
    
    // find farthest point between lines[0]->s, lines[0]->e, sp, ep
    pts[0].x = lines[0]->s[0];
    pts[0].y = lines[0]->s[1];
    pts[1].x = lines[0]->e[0];
    pts[1].y = lines[0]->e[1];
    pts[2].x = sp[0];
    pts[2].y = sp[1];
    pts[3].x = ep[0];
    pts[3].y = ep[1];
    getEndPoints(pts, 4);
    
    // copy the two farthest point to result array
    fourpts[2][0] = pts[0].x;
    fourpts[2][1] = pts[0].y;
    fourpts[2][2] = 1.0;
    fourpts[3][0] = pts[1].x;
    fourpts[3][1] = pts[1].y;
    fourpts[3][2] = 1.0;
    
    // check vp~fourpts[0] and vp~fourpts[2] are aligned
    double v1[2] = {vp[0] - fourpts[0][0], vp[1] - fourpts[0][1]};
    double v2[2] = {vp[0] - fourpts[2][0], vp[1] - fourpts[2][1]};
    m_util.unitVector(v1, v1, 2);
    m_util.unitVector(v2, v2, 2);
    
    double v3[2] = {vp[0] - fourpts[1][0], vp[1] - fourpts[1][1]};
    m_util.unitVector(v3, v3, 2);
    if( fabs(m_util.innerProd(v1, v2, 2)) < fabs(m_util.innerProd(v3, v2, 2)) )
    {
        // swap
        double temp[2] = {fourpts[1][0], fourpts[1][1]};
        fourpts[1][0] = fourpts[0][0];
        fourpts[1][1] = fourpts[0][1];
        fourpts[0][0] = temp[0];
        fourpts[0][1] = temp[1];
    }
    
    // create four lines
    // store the two line segments first that are orthogonal to the original lines
    memcpy(ext_lines[0].s, fourpts[0], sizeof(double)*2);
    memcpy(ext_lines[0].e, fourpts[2], sizeof(double)*2);
    m_util.crossProd(fourpts[0], fourpts[2], ext_lines[0].line);
    m_util.mul(K.t(), ext_lines[0].line, ext_lines[0].line_norm);
    m_util.unitVector(ext_lines[0].line_norm, ext_lines[0].line_norm, 3);
    
    memcpy(ext_lines[1].s, fourpts[1], sizeof(double)*2);
    memcpy(ext_lines[1].e, fourpts[3], sizeof(double)*2);
    m_util.crossProd(fourpts[1], fourpts[3], ext_lines[1].line);
    m_util.mul(K.t(), ext_lines[1].line, ext_lines[1].line_norm);
    m_util.unitVector(ext_lines[1].line_norm, ext_lines[1].line_norm, 3);
    
    if(complete)
    {
        memcpy(ext_lines[2].s, fourpts[0], sizeof(double)*2);
        memcpy(ext_lines[2].e, fourpts[1], sizeof(double)*2);
        memcpy(ext_lines[2].line, lines[0]->line, sizeof(double)*3);
        memcpy(ext_lines[2].line_norm, lines[0]->line_norm, sizeof(double)*3);
        
        memcpy(ext_lines[3].s, fourpts[2], sizeof(double)*2);
        memcpy(ext_lines[3].e, fourpts[3], sizeof(double)*2);
        memcpy(ext_lines[3].line, lines[1]->line, sizeof(double)*3);
        memcpy(ext_lines[3].line_norm, lines[1]->line_norm, sizeof(double)*3);
    }
    else
    {
        memcpy(ext_lines[2].s, lines[0]->s, sizeof(double)*2);
        memcpy(ext_lines[2].e, lines[0]->e, sizeof(double)*2);
        memcpy(ext_lines[2].line, lines[0]->line, sizeof(double)*3);
        memcpy(ext_lines[2].line_norm, lines[0]->line_norm, sizeof(double)*3);
        
        memcpy(ext_lines[3].s, lines[1]->s, sizeof(double)*2);
        memcpy(ext_lines[3].e, lines[1]->e, sizeof(double)*2);
        memcpy(ext_lines[3].line, lines[1]->line, sizeof(double)*3);
        memcpy(ext_lines[3].line_norm, lines[1]->line_norm, sizeof(double)*3);
    }
}

// This function takes points on a line and finds two end points
// Input:
//      pts: 2d points lying on a line
//      n: num of points. Must be gte 2
// Output: find two end points and they will store into pts[0], pts[1]
void CTR::getEndPoints(Point2d *pts, int n)
{
    // compute rotation and translation matrix
    double v1[2], v2[2], t[2];
    v1[0] = pts[1].x - pts[0].x;
    v1[1] = pts[1].y - pts[0].y;
    m_util.unitVector(v1, v1, 2);
    
    if(fabs(v1[1]) > 1e-5)  // find an orthogonal vector to v1
    {
        v2[0] = 1.0;
        v2[1] = -v1[0]/v1[1];
    }
    else
    {
        v2[0] = -v1[1]/v1[0];
        v2[1] = 1.0;
    }
    m_util.unitVector(v2, v2, 2);
    
    t[0] = pts[0].x;
    t[1] = pts[0].y;
    
    // transform from the original coord. to the one aligned with v1, v2.
    // and find max and min
    double min = 10000000, max = -10000000;
    Point2d min_pt, max_pt;
    for(int i=0; i<n; i++)
    {
        double x[2] = {pts[i].x, pts[i].y};
        double x_coord = m_util.innerProd(v1, x, 2) - m_util.innerProd(v1, t, 2);
        if(x_coord < min)
        {
            min = x_coord;
            min_pt.x = pts[i].x;
            min_pt.y = pts[i].y;
        }
        if(x_coord > max)
        {
            max = x_coord;
            max_pt.x = pts[i].x;
            max_pt.y = pts[i].y;
        }
    }
    pts[0].x = min_pt.x;
    pts[0].y = min_pt.y;
    
    pts[1].x = max_pt.x;
    pts[1].y = max_pt.y;
}


// lines_img1 = {x1, x2, z1, z2}
// vd_z_img1 and vd_z_img2 are used to detect lines close to horizon.
bool CTR::existPerpendicularLine(LineFeature lines_img1[4], LineFeature lines_img2[4],
                                 LineCluster &line_cluster_y_img1, LineCluster &line_cluster_y_img2, Mat Kinv_img1, Mat Kinv_img2,
                                 double vd_z_img1[3], double vd_z_img2[3], int min_num_lines_in)
{
    //min_num_lines_in = 5;    
    
    // find four points
    double pt[3], fourpts[4][2];
    m_util.crossProd(lines_img1[0].line, lines_img1[2].line, pt);
    fourpts[0][0] = pt[0]/pt[2];
    fourpts[0][1] = pt[1]/pt[2];
    m_util.crossProd(lines_img1[0].line, lines_img1[3].line, pt);
    fourpts[1][0] = pt[0]/pt[2];
    fourpts[1][1] = pt[1]/pt[2];
    
    m_util.crossProd(lines_img1[1].line, lines_img1[2].line, pt);
    fourpts[2][0] = pt[0]/pt[2];
    fourpts[2][1] = pt[1]/pt[2];
    m_util.crossProd(lines_img1[1].line, lines_img1[3].line, pt);
    fourpts[3][0] = pt[0]/pt[2];
    fourpts[3][1] = pt[1]/pt[2];
    
    int cnt = 0;
    for(int i=0; i<line_cluster_y_img1.num_lines; i++)
    {
        // if the line in y direction is close to horizon, then we skip it because it might be misclassified.
        if(fabs(m_util.innerProd(vd_z_img1, line_cluster_y_img1.lines[i]->line_norm, 3)) > 0.9998) // 0.9998 = cos(1*CV_PI/180.0))
            continue;
        
        if(isInRect(line_cluster_y_img1.lines[i]->m, lines_img1, Kinv_img1))
        {
            cnt++;
            if(cnt >= min_num_lines_in)
                return true;
            else
                continue;
        }
        if(isInRect(line_cluster_y_img1.lines[i]->s, lines_img1, Kinv_img1))
        {
            cnt++;
            if(cnt >= min_num_lines_in)
                return true;
            else
                continue;
        }
        if(isInRect(line_cluster_y_img1.lines[i]->e, lines_img1, Kinv_img1))
        {
            cnt++;
            if(cnt >= min_num_lines_in)
                return true;
            else
                continue;
        }
        
        /*	if(isIntersectLineSeg(line_cluster_y_img1.lines[i]->s, line_cluster_y_img1.lines[i]->e, fourpts))
         {
         cnt++;
         if(cnt >= min_num_lines_in)
         return true;
         else
         continue;
         }
         */
    }
    
    m_util.crossProd(lines_img2[0].line, lines_img2[2].line, pt);
    fourpts[0][0] = pt[0]/pt[2];
    fourpts[0][1] = pt[1]/pt[2];
    m_util.crossProd(lines_img2[0].line, lines_img2[3].line, pt);
    fourpts[1][0] = pt[0]/pt[2];
    fourpts[1][1] = pt[1]/pt[2];
    
    m_util.crossProd(lines_img2[1].line, lines_img2[2].line, pt);
    fourpts[2][0] = pt[0]/pt[2];
    fourpts[2][1] = pt[1]/pt[2];
    m_util.crossProd(lines_img2[1].line, lines_img2[3].line, pt);
    fourpts[3][0] = pt[0]/pt[2];
    fourpts[3][1] = pt[1]/pt[2];
    
    cnt = 0;
    for(int i=0; i<line_cluster_y_img2.num_lines; i++)
    {
        // if the line in y direction is close to horizon, then we skip it because it might be misclassified.
        if(fabs(m_util.innerProd(vd_z_img2, line_cluster_y_img2.lines[i]->line_norm, 3)) > cos(1*CV_PI/180.0))
            continue;
        
        if(isInRect(line_cluster_y_img2.lines[i]->m, lines_img2, Kinv_img2))
        {
            cnt++;
            if(cnt >= min_num_lines_in)
                return true;
            else
                continue;
        }
        if(isInRect(line_cluster_y_img2.lines[i]->s, lines_img2, Kinv_img2))
        {
            cnt++;
            if(cnt >= min_num_lines_in)
                return true;
            else
                continue;
        }
        if(isInRect(line_cluster_y_img2.lines[i]->e, lines_img2, Kinv_img2))
        {
            cnt++;
            if(cnt >= min_num_lines_in)
                return true;
            else
                continue;
        }
        
        /*	if(isIntersectLineSeg(line_cluster_y_img2.lines[i]->s, line_cluster_y_img2.lines[i]->e, fourpts))
         {
         cnt++;
         if(cnt >= min_num_lines_in)
         return true;
         else
         continue;
         }*/
    }
    
    return false;
}

bool CTR::isInRect(double pt[2], LineFeature lines[4], Mat Kinv)
{
    double tolerance = 0.5*CV_PI/180.0;
    double n1[3], n2[3], n3[3], vd[3], pt_[3], m[3], c1[3], c2[3];
    int cnt = 0;
    
    memcpy(n1, lines[0].line_norm, sizeof(double)*3);
    memcpy(n2, lines[1].line_norm, sizeof(double)*3);
    
    m_util.crossProd(n1, n2, vd);
    pt_[0] = pt[0];
    pt_[1] = pt[1];
    pt_[2] = 1.0;
    m_util.mul(Kinv, pt_, m);
    
    m_util.crossProd(vd, m, n3);
    m_util.unitVector(n3, n3, 3);
    
    if(m_util.innerProd(n1, n3, 3) < 0)
    {
        n1[0] = -n1[0];
        n1[1] = -n1[1];
        n1[2] = -n1[2];
    }
    
    if(m_util.innerProd(n2, n3, 3) < 0)
    {
        n2[0] = -n2[0];
        n2[1] = -n2[1];
        n2[2] = -n2[2];
    }
    
    m_util.crossProd(n3, n1, c1);
    m_util.crossProd(n3, n2, c2);
    
    if(m_util.innerProd(c1, c2, 3) < 0)
    {
        double angle1 = acos(m_util.innerProd(n1,n3,3));
        double angle2 = acos(m_util.innerProd(n2,n3,3));
        double min_angle = min(angle1, angle2);
        if(min_angle > tolerance)
            cnt++;
        
    }
    
    memcpy(n1, lines[2].line_norm, sizeof(double)*3);
    memcpy(n2, lines[3].line_norm, sizeof(double)*3);
    
    m_util.crossProd(n1, n2, vd);
    pt_[0] = pt[0];
    pt_[1] = pt[1];
    pt_[2] = 1.0;
    m_util.mul(Kinv, pt_, m);
    
    m_util.crossProd(vd, m, n3);
    m_util.unitVector(n3, n3, 3);
    
    if(m_util.innerProd(n1, n3, 3) < 0)
    {
        n1[0] = -n1[0];
        n1[1] = -n1[1];
        n1[2] = -n1[2];
    }
    
    if(m_util.innerProd(n2, n3, 3) < 0)
    {
        n2[0] = -n2[0];
        n2[1] = -n2[1];
        n2[2] = -n2[2];
    }
    
    m_util.crossProd(n3, n1, c1);
    m_util.crossProd(n3, n2, c2);
    
    if(m_util.innerProd(c1, c2, 3) < 0)
    {
        double angle1 = acos(m_util.innerProd(n1,n3,3));
        double angle2 = acos(m_util.innerProd(n2,n3,3));
        double min_angle = min(angle1, angle2);
        if(min_angle > tolerance)
            cnt++;
        
    }
    
    if(cnt == 2)
        return true;
    else
        return false;
}
