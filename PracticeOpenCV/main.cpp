#include "main.h"

#include <iostream>

using namespace std;
using namespace cv;

Mat crossMatrix3d(Mat vec) {
	assert(vec.rows == 3 || vec.cols == 1);

	return (Mat_<double>(3, 3) <<
		0., -vec.at<double>(3), vec.at<double>(2),
		vec.at<double>(3), 0., -vec.at<double>(1),
		-vec.at<double>(2), vec.at<double>(1), 0.);
}

int main() {
	Mat t = (Mat_<double>(3, 1) << 1.5, 2.5, 0.8);
	Mat R = (Mat_<double>(3, 3) << 1., 0., 0., 0., cos(30), sin(30), 0., -sin(30), cos(30));
	Mat P1 = (Mat_<double>(3, 1) << 2.3, 3.3, 1.7);
	Mat P2t = (Mat_<double>(1, 3) << 3.1, 4.5, 3.0);

	Mat r = P2t * crossMatrix3d(t) * R * P1;
	cout << r << endl;
	cout << r.at<double>(0) << endl;

	system("pause");
}
