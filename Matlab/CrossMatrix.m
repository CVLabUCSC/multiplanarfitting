function X = CrossMatrix(v)
    X = zeros(3, 3);
    
    X(1, 2) = v(3);
    X(1, 3) = -v(2);
    X(2, 1) = -v(3);
    X(2, 3) = v(1);
    X(3, 1) = v(2);
    X(3, 2) = -v(1);
end