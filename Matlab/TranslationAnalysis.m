function error = TranslationAnalysis(P1, P2, K, R, translations)

    % Make the homogenuous points
    if size(P1, 1) == 2
        P1 = makehomogeneous(P1);
    end
    
    if size(P2, 1) == 2
        P2 = makehomogeneous(P2);
    end
    
    % Calibrate the points
    if ~isempty(K)
        P1 = K \ P1; 
        P2 = K \ P2;
    end
    
    num_of_translations = size(translations, 2);
    error = zeros(num_of_translations, size(P1, 2));
    
    for i=1:num_of_translations
        T_hat = translations(:, i);% ./ norm(translations(:, i));
        E = R * CrossMatrix(T_hat);
        error(i, :) = sum(abs(P2.*(E*P1)), 1);
    end
end