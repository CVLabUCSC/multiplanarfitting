function delta = SampsonError(H, p1, p2)
%UNTITLED2 이 함수의 요약 설명 위치
%   자세한 설명 위치

    num_of_points = size(p1, 2);

    p2_est = H * p1;

    e = [p2(2, :) .* p2_est(3, :) - p2(3, :) .* p2_est(2, :);...
        p2(3, :) .* p2_est(1, :) - p2(1, :) .* p2_est(3, :)];
    
    h1 = H(1, :)';
    h2 = H(2, :)';
    h3 = H(3, :)';
    
    delta = zeros(num_of_points, 1);
    for i = 1:num_of_points
        J = [h3(1) * p2(2, i) - p2(3, i) * h2(1),...
            h3(2) * p2(2, i) - h2(2) * p2(3, i),...
            0, p1(:, i)' * h3;
            p2(3, i) * h1(1) - p2(1, i) * h3(1),...
            p2(3, i) * h1(2) - p2(1, i) * h3(2),...
            -p1(:, i)' * h3, 0];
        
        delta(i) = e(:, i)' / (J * J') * e(:, i);
    end
end

