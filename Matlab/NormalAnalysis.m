% Load output of algorithm
JLinkage_cons_globalParams;
JLinkage_cons_result;

cluster_index = 2;  % Specify the index of cluster

% Transform points into 
p1 = [clusters{cluster_index}(:,1:2)';...
    ones(1, size(clusters{cluster_index}, 1))];
p2 = [clusters{cluster_index}(:,3:4)';...
    ones(1, size(clusters{cluster_index}, 1))];

% Compute constrained homographies
H1 = CalibratedDLT(K1, K2, R, VD(:,1), p1, p2);
H2 = CalibratedDLT(K1, K2, R, VD(:,2), p1, p2);
H3 = CalibratedDLT(K1, K2, R, VD(:,3), p1, p2);

% Compute Sampson Error for each homography
dist1 = SampsonError(H1, p1, p2);
dist2 = SampsonError(H2, p1, p2);
dist3 = SampsonError(H3, p1, p2);
