function H = CalibratedDLT(K1, K2, R, n, p1, p2)
    num_points = size(p1, 2);

    if num_points == 0
        H = zeros(3, 3);
        return;
    end

    invKp1 = K1 \ p1;
    invKp2 = K2 \ p2;

    alpha = n' * invKp1;

    Rrx = R * invKp1;
    b = [Rrx(1, :) - Rrx(3, :) .* invKp2(1,:); Rrx(2, :) - Rrx(3, :) .* invKp2(2,:)];
    b = reshape(b, num_points*2, 1);

    A = zeros(2 * num_points, 3);
    for pi = 1:num_points
        A(pi*2-1, 1) = -alpha(pi);
        A(pi*2, 2) = -alpha(pi);
        A(pi*2-1, 3) = alpha(pi) * invKp2(1,pi);
        A(pi*2, 3) = alpha(pi) * invKp2(2,pi);
    end

    t_hat = A \ b;

    H = K2 * (R + t_hat * n') / K1;
    
    H = H ./ H(3, 3);
end

