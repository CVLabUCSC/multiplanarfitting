function [x1, x2, o1, o2, img1, img2, P1, P2, H, H_E] =...
    GenerateSyntheticData(num_of_points, translation_scale, show_figures)
% Generate point clusters on the corridor-like structure as follow:
%         Top-View                   Side-View
%   ========(1)=========|       ========(4)=========|
%                       |                           |
%   C                  (2)      C                  (2)
%                       |                           |
%   ========(3)=========|       ========(5)=========|
% where C represents the camera center.
%
% [x1, x2, P1, P2, H, H_E] = GenerateSyntheticData(num_of_clusters,
%   num_of_points, outliers_perc)
%
% Parameters:
%   num_of_clusters : The number of clusters, from 1 to 5.
%   num_of_points   : The number of points for each cluster.
%   outliers_perc   : Percentage of outlier points with respect to the
%                   total number of generated points. The value must
%                   between 0 and 1.
%   show_figures    : If this value is set to non-zero value, visualize the
%                   generated data.
%
% Outputs:
%   x1, x2          : Point correspondences in image coordinates. Each cell
%                   represents a cluster.
%   o1, o2          : Outliers in image coordinates.
%   P1, P2          : Point correspondences in world coordinates.
%

    close all;
    
    t = [];
    K = [];
    R = [];
    VD = [];

    load('parameters');
    
    if isempty(t)
        disp('Translation vector is empty!');
        return;
    end
    
    if isempty(K)
        disp('Camera calibration matrix is empty!');
        return;
    end
    
    if isempty(R)
        disp('Rotation matrix is empty!');
    end
    
    if isempty(VD)
        disp('Vanishing directions are not defined!');
    end

%% Initialize parameters    
    switch nargin
        case 0
            num_of_points = 90;
            translation_scale = 1;
            show_figures = 0;
        case 1
            translation_scale = 1;
            show_figures = 0;
        case 2
            show_figures = 0;
    end
    
    t = t .* translation_scale;
    
    %% Set constant values
    num_of_clusters = 5;
    
    num_of_outlier = 500;
    
    % Set distance values for each plane
    d = [-3, -8, 3, 2, -2];

    % Set normal vectors for each plane
    n_c = [VD(:,1), VD(:,3), VD(:,1), VD(:,2), VD(:,2)];

    %% Initialize the output matrices
    P1 = cell(1, num_of_clusters);
    P2 = cell(1, num_of_clusters);
    x1 = cell(1, num_of_clusters);
    x2 = cell(1, num_of_clusters);
    H = zeros(3, 3, num_of_clusters);
    H_E = zeros(3, 3, num_of_clusters);

    %% Generate points

    % Generate points on the left wall
    if num_of_clusters >= 1
        H_E(:,:,1) = R - (t/d(1)) * n_c(:, 1)';
        for i=1:num_of_points
            P1{1}(:,i) = [d(1); randi([d(5), d(4)] .* 100)/100; randi([min(d(2)/4, d(2)), max(d(2)/4, d(2))] .* 100)/100];
        end
    end

    % Generate points on the front wall
    if num_of_clusters >= 2
        H_E(:,:,2) = R + (t/d(2)) * n_c(:, 2)';
        for i=1:num_of_points
            P1{2}(:,i) = [randi([d(1), d(3)] .* 100)/100; randi([d(5), d(4)] .* 100)/100; d(2)];
        end
    end

    % Generate points on the right wall
    if num_of_clusters >= 3
        H_E(:,:,3) = R - (t/d(3)) * n_c(:, 3)';
        for i=1:num_of_points
            P1{3}(:,i) = [d(3); randi([d(5), d(4)] .* 100)/100; randi([min(d(2)/4, d(2)), max(d(2)/4, d(2))] .* 100)/100];
        end
    end

    % Generate points on the ceiling
    if num_of_clusters >= 4
        H_E(:,:,4) = R - (t/d(4)) * n_c(:, 4)';
        for i=1:num_of_points
            % modifiy here
            P1{4}(:,i) = [randi([d(1), d(3)] .* 100)/100; d(4); randi([min(d(2)/4, d(2)), max(d(2)/4, d(2))] .* 100)/100];
        end
    end

    % Generate points on the floor
    if num_of_clusters >= 5
        H_E(:,:,5) = R - (t/d(5)) * n_c(:, 5)';
        for i=1:num_of_points
            % modifiy here
            P1{5}(:,i) = [randi([d(1), d(3)] .* 100)/100; d(5); randi([min(d(2)/4, d(2)), max(d(2)/4, d(2))] .* 100)/100];
        end
    end

    for i=1:num_of_clusters
        % Project original points using the homography
        P2{i} = H_E(:,:,i) * P1{i};

        % Generate image points from original 3d points, and normalize them
        x1{i} = K * P1{i};
        x1{i}(1,:) = x1{i}(1,:) ./ x1{i}(3,:);
        x1{i}(2,:) = x1{i}(2,:) ./ x1{i}(3,:);
        x1{i}(3,:) = x1{i}(3,:) ./ x1{i}(3,:);

        % Generate image points from target 3d points, and normalize them
        x2{i} = K * P2{i};
        x2{i}(1,:) = x2{i}(1,:) ./ x2{i}(3,:);
        x2{i}(2,:) = x2{i}(2,:) ./ x2{i}(3,:);
        x2{i}(3,:) = x2{i}(3,:) ./ x2{i}(3,:);
        
        H(:,:,i) = K * H_E(:,:,i) / K;
        %H(:,:,i) = H(:,:,i) ./ H(3,3,i);
    end
    
    % Add outliers
    if ~isempty(num_of_outlier)
        o1 = randi([0, 1000], 2, num_of_outlier) .* repmat([K(1, 3)*2/1000; K(2, 3)*2/1000], 1, num_of_outlier);
        o2 = randi([0, 1000], 2, num_of_outlier) .* repmat([K(1, 3)*2/1000; K(2, 3)*2/1000], 1, num_of_outlier);
    end
    
    % Draw synthetic image
    [img1, img2] = drawSyntheticImage(H, K, d);

    if show_figures
        colors = fixedcolor;

        % draw figure to see original 3d points
        figure(1); hold on; axis equal
        for i=1:num_of_clusters
            plot3(P1{i}(1,:), P1{i}(2,:), P1{i}(3,:), '.', 'Color', colors(i,:));
        end
        plot3([0, VD(1,1)], [0, VD(2,1)], [0, VD(3,1)], 'r', 'LineWidth', 2);
        plot3([0, VD(1,2)], [0, VD(2,2)], [0, VD(3,2)], 'g', 'LineWidth', 2);
        plot3([0, VD(1,3)], [0, VD(2,3)], [0, VD(3,3)], 'b', 'LineWidth', 2);
        hold off;

        % draw figure to see target 3d points
        figure(2); hold on; axis equal
        for i=1:num_of_clusters
            plot3(P2{i}(1,:), P2{i}(2,:), P2{i}(3,:), '.', 'Color', colors(i,:));
        end
        plot3([0, VD(1,1)], [0, VD(2,1)], [0, VD(3,1)], 'r', 'LineWidth', 2);
        plot3([0, VD(1,2)], [0, VD(2,2)], [0, VD(3,2)], 'g', 'LineWidth', 2);
        plot3([0, VD(1,3)], [0, VD(2,3)], [0, VD(3,3)], 'b', 'LineWidth', 2);
        hold off;

        % draw figure to see original image points
        figure(3); hold on; axis([0, K(1, 3) * 2, 0, K(2, 3) * 2]);
        for i=1:num_of_clusters
            plot(x1{i}(1,:), x1{i}(2,:), '.', 'Color', colors(i,:));
        end
        
        % draw outliers
        if ~isempty(num_of_outlier)
            plot(o1(1,:), o1(2,:),'xr');
        end
        hold off;

        % draw figure to see target image points
        figure(4); hold on; axis([0, K(1, 3) * 2, 0, K(2, 3) * 2]);
        for i=1:num_of_clusters
            plot(x2{i}(1,:), x2{i}(2,:), '.', 'Color', colors(i,:));
        end
        if ~isempty(num_of_outlier)
            plot(o2(1,:), o2(2,:),'xr');
        end
        hold off;
    
        figure(5); imshow(flip(img1, 1));
        figure(6); imshow(flip(img2, 1));
    end
end
