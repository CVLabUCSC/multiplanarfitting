function AutoDataGenerationVaryingTranslation(err_var_range, t_ratio_range)
    if numel(err_var_range) ~= 3
        disp(['Error variance range must have three elements: \n',...
            '[[minimum_variance] [variance_step] [maximum_variance]]']);
    end
    
    if numel(t_ratio_range) ~= 3
        disp(['Error translation range must have three elements: \n',...
            '[[minimum_multiplier] [multiplier_step] [maximum_multiplier]]']);
    end
    
    num_planes = 4;
    num_of_points = 90;
    outliers_ratio = 0.2;
    
    for var=err_var_range(1):err_var_range(2):err_var_range(3)
        for t_ratio=t_ratio_range(1):t_ratio_range(2):t_ratio_range(3)
            for i=1:numel(outliers_ratio)
                [x1, x2, ~, ~, P1, P2] = GenerateSyntheticData(num_planes, num_of_points, var, outliers_ratio(i), t_ratio, 0);
                SaveCorrespondences(['SyntheticData_4Planes_r', num2str(var, '%.1f'), '_o', num2str(outliers_ratio(i), '%.1f'), '_t', num2str(t_ratio, '%.1f'), '.txt'], x1, x2);
                SavePointClusters(['SyntheticData_4Planes_r', num2str(var, '%.1f'), '_o', num2str(outliers_ratio(i), '%.1f'), '_t', num2str(t_ratio, '%.1f'), '_GT.txt'], x1, x2, num_planes);
                save(['SyntheticData_', num2str(num_planes), 'Planes_r', num2str(var, '%.1f'), '_o', num2str(outliers_ratio(i), '%.1f'), '_t', num2str(t_ratio, '%.1f'), '_3D'], 'P1', 'P2');
            end
        end
    end

end
