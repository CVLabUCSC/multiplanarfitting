function [img1, img2] = drawSyntheticImage(H, K, d)
% Draw synthetic image from the given information

    num_of_clusters = size(H, 3);

    % Initialize the objects
    shapeInserter = vision.ShapeInserter('Shape', 'Polygons');
    
    % Initialize the image to draw the structure
    img1 = uint8(ones(K(2, 3) * 2, K(1, 3) * 2, 3) * 255);
    img2 = uint8(ones(K(2, 3) * 2, K(1, 3) * 2, 3) * 255);
    
    % lines on the left wall
    if num_of_clusters >= 1
        pt = [d(1), d(1), d(1), d(1), d(1);
            d(5), d(5), d(4), d(4), d(5);
            d(2)/4, d(2), d(2), d(2)/4, d(2)/4];
        pt = K * pt;
        pt(1,:) = pt(1,:) ./ pt(3,:);
        pt(2,:) = pt(2,:) ./ pt(3,:);
        pt(3,:) = pt(3,:) ./ pt(3,:);
        img1 = step(shapeInserter, img1, reshape(int32([pt(1, :); pt(2, :)]), 1, size(pt, 2)*2));
        pt = H(:, :, 1) * pt;
        pt(1,:) = pt(1,:) ./ pt(3,:);
        pt(2,:) = pt(2,:) ./ pt(3,:);
        pt(3,:) = pt(3,:) ./ pt(3,:);
        img2 = step(shapeInserter, img2, reshape(int32([pt(1, :); pt(2, :)]), 1, size(pt, 2)*2));
    end
    
    % lines on the front wall
    if num_of_clusters >= 2
        pt = [d(1), d(1), d(3), d(3), d(1);
            d(5), d(4), d(4), d(5), d(5);
            d(2), d(2), d(2), d(2), d(2)];
        pt = K * pt;
        pt(1,:) = pt(1,:) ./ pt(3,:);
        pt(2,:) = pt(2,:) ./ pt(3,:);
        pt(3,:) = pt(3,:) ./ pt(3,:);
        img1 = step(shapeInserter, img1, reshape(int32([pt(1, :); pt(2, :)]), 1, size(pt, 2)*2));
        pt = H(:, :, 2) * pt;
        pt(1,:) = pt(1,:) ./ pt(3,:);
        pt(2,:) = pt(2,:) ./ pt(3,:);
        pt(3,:) = pt(3,:) ./ pt(3,:);
        img2 = step(shapeInserter, img2, reshape(int32([pt(1, :); pt(2, :)]), 1, size(pt, 2)*2));
    end
    
    % lines on the right wall
    if num_of_clusters >= 3
        pt = [d(3), d(3), d(3), d(3), d(3);
            d(4), d(5), d(5), d(4), d(4);
            d(2), d(2), d(2)/4, d(2)/4, d(2)];
        pt = K * pt;
        pt(1,:) = pt(1,:) ./ pt(3,:);
        pt(2,:) = pt(2,:) ./ pt(3,:);
        pt(3,:) = pt(3,:) ./ pt(3,:);
        img1 = step(shapeInserter, img1, reshape(int32([pt(1, :); pt(2, :)]), 1, size(pt, 2)*2));
        pt = H(:, :, 3) * pt;
        pt(1,:) = pt(1,:) ./ pt(3,:);
        pt(2,:) = pt(2,:) ./ pt(3,:);
        pt(3,:) = pt(3,:) ./ pt(3,:);
        img2 = step(shapeInserter, img2, reshape(int32([pt(1, :); pt(2, :)]), 1, size(pt, 2)*2));
    end
    
    % lines on the ceiling
    if num_of_clusters >= 4
        pt = [d(1), d(3), d(3), d(1), d(1);
            d(5), d(5), d(5), d(5), d(5);
            d(2), d(2), d(2)/4, d(2)/4, d(2)];
        pt = K * pt;
        pt(1,:) = pt(1,:) ./ pt(3,:);
        pt(2,:) = pt(2,:) ./ pt(3,:);
        pt(3,:) = pt(3,:) ./ pt(3,:);
        img1 = step(shapeInserter, img1, reshape(int32([pt(1, :); pt(2, :)]), 1, size(pt, 2)*2));
        pt = H(:, :, 4) * pt;
        pt(1,:) = pt(1,:) ./ pt(3,:);
        pt(2,:) = pt(2,:) ./ pt(3,:);
        pt(3,:) = pt(3,:) ./ pt(3,:);
        img2 = step(shapeInserter, img2, reshape(int32([pt(1, :); pt(2, :)]), 1, size(pt, 2)*2));
    end
    
    % lines on the bottom
    if num_of_clusters == 5
        pt = [d(1), d(3), d(3), d(1), d(1);
            d(4), d(4), d(4), d(4), d(4);
            d(2), d(2), d(2)/4, d(2)/4, d(2)];
        pt = K * pt;
        pt(1,:) = pt(1,:) ./ pt(3,:);
        pt(2,:) = pt(2,:) ./ pt(3,:);
        pt(3,:) = pt(3,:) ./ pt(3,:);
        img1 = step(shapeInserter, img1, reshape(int32([pt(1, :); pt(2, :)]), 1, size(pt, 2)*2));
        pt = H(:, :, 5) * pt;
        pt(1,:) = pt(1,:) ./ pt(3,:);
        pt(2,:) = pt(2,:) ./ pt(3,:);
        pt(3,:) = pt(3,:) ./ pt(3,:);
        img2 = step(shapeInserter, img2, reshape(int32([pt(1, :); pt(2, :)]), 1, size(pt, 2)*2));
    end
end