function num_points = SaveCorrespondences(filename, p1, p2)
    num_clusters = numel(p1);
    num_points = 0;
    
    for i=1:num_clusters
        num_points = num_points + size(p1{i}, 2);
    end

    % save the result
    fid=fopen(filename, 'w');
    fprintf(fid, '%d\n', num_points);
    for ci=1:num_clusters
        if size(p1{ci}, 2) == 0
            continue;
        end
        fprintf(fid, '%.2f %.2f %.2f %.2f\n', [p1{ci}(1:2,:); p2{ci}(1:2,:)]);
    end
    fclose(fid);
end