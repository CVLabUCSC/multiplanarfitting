function AutoDataGeneration(num_of_plane_range, num_of_points,...
    err_var_range, outliers_ratio)
% Auto-generate script
%
% Parameters:
%   err_var_range	: Set the Gaussian error variance range. For example,
%                   if the variance starting from 0.0 and has step of 0.5,
%                   up to 1.0, then this value should have [0.0, 0.5, 1.0].
%   outliers_ratio  : Ratio of ouliers with respect to the total number of
%                   generated points.
%                   
    if numel(err_var_range) ~= 3
        disp(['Error variance range must have three elements: \n',...
            '[[minimum_variance] [variance_step] [maximum_variance]]']);
    end
    
    [x1_original, x2_original, outliers1, outliers2, img1, img2, P1, P2] =...
        GenerateSyntheticData(num_of_points, 1.0, 0);
    
    save('original_points.mat', 'x1_original', 'x2_original', 'outliers1', 'outliers2', 'img1', 'img2', 'P1', 'P2');
    
    % Add noise for each correspondence
    for var=err_var_range(1):err_var_range(2):err_var_range(3)
        [x1_noisy, x2_noisy] = addNoiseToPointClusters(x1_original, x2_original, var);
        
        for n_planes=num_of_plane_range(1):num_of_plane_range(2)
            for i=1:numel(outliers_ratio)
                
                % Select outliers
                num_of_outliers = num_of_points * n_planes * outliers_ratio(i);
                if (num_of_outliers <= 0)
                    x1 = x1_noisy(1:n_planes);
                    x2 = x2_noisy(1:n_planes);
                else
                    x1 = [x1_noisy(1:n_planes), {outliers1(:, 1:num_of_outliers)}];
                    x2 = [x2_noisy(1:n_planes), {outliers2(:, 1:num_of_outliers)}];
                end
                
                % Save generated correspondences
                SaveCorrespondences(['SyntheticData_', num2str(n_planes), 'Planes_r', num2str(var, '%.1f'), '_o', num2str(outliers_ratio(i), '%.1f'), '.txt'], x1, x2);
                SavePointClusters(['SyntheticData_', num2str(n_planes), 'Planes_r', num2str(var, '%.1f'), '_o', num2str(outliers_ratio(i), '%.1f'), '_GT.txt'], x1, x2, n_planes);
                save(['SyntheticData_', num2str(n_planes), 'Planes_r', num2str(var, '%.1f'), '_o', num2str(outliers_ratio(i), '%.1f'), '_3D.mat'], 'P1', 'P2');
            end
            
        end
    end
end


