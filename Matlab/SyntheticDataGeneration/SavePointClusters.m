function SavePointClusters(filename, x1, x2, num_of_clusters)
    if ~iscell(x1) || ~iscell(x2)
        disp('Invalid input arguments');
    end

    fid = fopen(filename, 'w');
    for i=1:num_of_clusters
        if size(x1{i}, 2) == 0
            continue;
        end
        fprintf(fid, 'num=%d\n', size(x1{i}, 2));
        P = [x1{i}(1:2,:); x2{i}(1:2,:)];
        fprintf(fid, '%.2f %.2f %.2f %.2f\n', P);
    end
    
    % write outliers if they exist
    if length(x1) > num_of_clusters && size(x1{num_of_clusters}, 2) ~= 0
        fprintf(fid, 'outliers=%d\n', size(x1{num_of_clusters+1}, 2));
        P = [x1{num_of_clusters+1}(1:2,:); x2{num_of_clusters+1}(1:2,:)];
        fprintf(fid, '%.2f %.2f %.2f %.2f\n', P);
    end
    fclose(fid);
end
