#include <bitset>
#include <unordered_set>

#include <opencv2\xfeatures2d\nonfree.hpp>

#include "MultiPlanarFitting.h"

using namespace std;
using namespace cv;

using boost::math::binomial;

using IOUtils::showMessage;
using IOUtils::TwoViewParamPathes;

bool ClusterSizeSort(const PointPairCluster &left, const PointPairCluster &right)
{
	return left.point1.size() > right.point1.size();
}

int MultiPlanarFitting::test_sequence = 0;

MultiPlanarFitting::MultiPlanarFitting(void)
{
}

MultiPlanarFitting::~MultiPlanarFitting(void)
{
}

//------------------------------------------
// INTERNAL FUNCTIONS
//------------------------------------------

Clusters MultiPlanarFitting::JLinkageClustering(HomographyEstimator &estimator, float inlierThreshold)
{
	SamplingParams params;
	unsigned int minimumSampleSet = 0;
	
	switch (estimator.getModelType())
	{
	case HOMOGRAPHY_MANHATTAN_WORLD:
		minimumSampleSet = 2;
		break;
	case HOMOGRAPHY_WEAK_MANHATTAN_WORLD:
		minimumSampleSet = 3;
		break;
	case HOMOGRAPHY_UNCONSTRAINED:
	default:
		minimumSampleSet = 4;
	}

	// Generate models
	vector<HomographyModel> models;
	GenerateModels(models, estimator, (int)m_points1.size() * 15);

	printf("The number of generated random samples is %d\n", models.size());

	// Compute the J-Linkage Clusterization
	JLinkage jlinkage(inlierThreshold, models.size(), true, 2, params.KdTreeRange);
	unsigned int counter = 0;

	cout << "Loading Models" << endl;
	for (unsigned int i = 0; i < models.size(); i++) {
		jlinkage.AddModel(new Mat(models[i].Homography));
	}

	cout << "Loading Points" << endl;
	for (int i = 0; i < (int)m_points1.size(); i++) {
		jlinkage.AddPoint(m_points1[i], m_points2[i]);
	}
	
	cout << "J-Clusterizing" << endl;
	list<sClLnk *> tempClustersList = jlinkage.DoJLClusterization();
	
	// Copy Result Data
	tempClustersList.sort(ClusterSort());
	int clusterIndex = 0;
	Clusters clusters(m_points1, m_points2);
	for (list<sClLnk *>::const_iterator citer = tempClustersList.begin(); citer != tempClustersList.end(); citer++)
	{
		if ((*citer)->mBelongingPts.size() <= minimumSampleSet)
			break;

		for (list<sPtLnk *>::const_iterator piter = (*citer)->mBelongingPts.begin(); piter != (*citer)->mBelongingPts.end(); piter++)
			clusters.addPoint((*piter)->mCoord1, (*piter)->mCoord2, clusterIndex);
		
		clusterIndex++;
	}

	return clusters;
}

Clusters MultiPlanarFitting::IterativeJLinkage(vector<Mat> &homographies, float mInlierThreshold) {
	unsigned int nPtDimension = 2;
	int mKDTreeRange = 10;
	Clusters clusters(m_points1, m_points2);

	// Compute the jLinkage Clusterization
	JLinkage jlinkage(mInlierThreshold, homographies.size(), true, nPtDimension, mKDTreeRange);

	for (int i = 0; i < homographies.size(); i++) {
		jlinkage.AddModel(&homographies[i]);
	}

	for (int i = 0; i < (int)m_points1.size(); i++) {
		jlinkage.AddPoint(m_points1[i], m_points2[i]);
	}

	list<sClLnk *> tempClustersList = jlinkage.DoJLClusterization();

	tempClustersList.sort(ClusterSort());
		
	// Copy Result Data
	int clusterIndex = 0;
	for (list<sClLnk *>::const_iterator citer = tempClustersList.begin(); citer != tempClustersList.end(); citer++)
	{
		if ((*citer)->mBelongingPts.size() <= 4)
			break;

		for (list<sPtLnk *>::const_iterator piter = (*citer)->mBelongingPts.begin(); piter != (*citer)->mBelongingPts.end(); piter++)
			clusters.addPoint((*piter)->mCoord1, (*piter)->mCoord2, clusterIndex);

		clusterIndex++;
	}

	return clusters;
}

//------------------------------------------
// CLUSTER MODIFICATION FUNCTIONS
//------------------------------------------

bool MultiPlanarFitting::mergeCluster(vector<PointPairCluster> &clusters, int type, const double thr)
{
	bool updated = false;
	vector<bitset<MAX_POINTS_NUM>> point_inclusion_info(clusters.size());

	// Initialize bitsets
	for (size_t ci = 0; ci < clusters.size(); ci++)
	{
		for (size_t pi = 0; pi < clusters[ci].size(); pi++)
		{
			point_inclusion_info[ci][point_indexer[clusters[ci].point1[pi]]] = 1;
		}
	}

	while (true)
	{
		int num_clusters = (int)clusters.size();
		double best_j_dist = 1.0;
		int best_cluster1, best_cluster2;

		for (int ci = 0; ci < num_clusters-1; ci++)
		{
			for (int cj = ci+1; cj < num_clusters; cj++)
			{
				int num_intersection = (point_inclusion_info[ci] & point_inclusion_info[cj]).count();
				int num_union = (int)clusters[ci].point1.size() + (int)clusters[cj].point1.size() - num_intersection;
				double cur_j_dist = ((double)num_union - (double)num_intersection) / (double)num_union;

				if (cur_j_dist < best_j_dist)
				{
					best_j_dist = cur_j_dist;
					best_cluster1 = ci;
					best_cluster2 = cj;
				}
			}
		}

		// If best jaccard distance is less than the threshold, merge the clusters
		if (best_j_dist <= thr)
		{
			clusters[best_cluster1].point1.insert(clusters[best_cluster1].point1.begin(), clusters[best_cluster2].point1.begin(), clusters[best_cluster2].point1.end());
			clusters[best_cluster1].point2.insert(clusters[best_cluster1].point2.begin(), clusters[best_cluster2].point2.begin(), clusters[best_cluster2].point2.end());
			point_inclusion_info[best_cluster1] |= point_inclusion_info[best_cluster2];
			clusters.erase(clusters.begin() + best_cluster2);
			updated = true;
		}
		else
		{
			break;
		}
	}

	return updated;
}

bool MultiPlanarFitting::mergeClusterDebug(vector<PointPairCluster> &clusters, int type, const double thr)
{
	bool *b_matched_list;
	int n_points;
	bool updated = false;

	if (type == MERGE_JACCARD || type == MERGE_NORMAL_CHECK)
	{
		if (type == MERGE_NORMAL_CHECK)
			setNormals(clusters);
	
		for (int ci = 0; ci < static_cast<int>(clusters.size())-1; ci++)
		{
			if (clusters[ci].point1.size() == 0)
				continue;

			n_points = static_cast<int>(clusters[ci].point1.size());

			for (int cj = ci+1; cj < static_cast<int>(clusters.size()); cj++)
			{
				if (clusters[cj].point1.size() == 0)
					continue;

				if (type == MERGE_NORMAL_CHECK && clusters[ci].normal != clusters[cj].normal)
					continue;

				int n_intersection = 0;

				b_matched_list = new bool[clusters[cj].point1.size()];

				for (int pi = 0; pi < n_points; pi++)
				{
					for (int pj = 0; pj < static_cast<int>(clusters[cj].point1.size()); pj++)
					{
						if ((clusters[ci].point1[pi] == clusters[cj].point1[pj]) && (clusters[ci].point2[pi] == clusters[cj].point2[pj]))
						{
							n_intersection++;
							b_matched_list[pj] = false;
						}
						else
						{
							b_matched_list[pj] = true;
						}
					}
				}
			
				double d_union = (double)(n_points - n_intersection + static_cast<int>(clusters[cj].point1.size()));
				double ratio_crt = (d_union - (double)n_intersection) / d_union;

				if (ratio_crt <= thr)
				{
					for (int pj = 0; pj < static_cast<int>(clusters[cj].point1.size()); pj++)
					{
						if (b_matched_list[pj])
						{
							clusters[ci].point1.push_back(clusters[cj].point1[pj]);
							clusters[ci].point2.push_back(clusters[cj].point2[pj]);
						}
					}

					clusters[cj].point1.clear();
					clusters[cj].point2.clear();
					clusters[cj].H.release();

					updated = true;
				}

				delete[] b_matched_list;
			}
		}
	}
	else if (type == MERGE_T_EUCLIDEAN)
	{
		for (int ci = 0; ci < (int)clusters.size()-1; ci++)
		{
			if (clusters[ci].point1.size() == 0)
				continue;

			for (int cj = ci+1; cj < (int)clusters.size(); cj++)
			{
				if (clusters[cj].point1.size() == 0)
					continue;

				if (norm(clusters[ci].t - clusters[cj].t) <= thr)
				{
					for (int pj = 0; pj < (int)clusters[cj].point1.size(); pj++)
					{
						clusters[ci].point1.push_back(clusters[cj].point1[pj]);
						clusters[ci].point2.push_back(clusters[cj].point2[pj]);
					}

					clusters[cj].point1.clear();
					clusters[cj].point2.clear();
					clusters[cj].H.release();
				}
			}
		}
	}
	else if (type == MERGE_T_ALIGN)
	{
		cout << "NOt yet implemented" << endl;
		return updated;
	}
	else
	{
		cout << "ERROR! Unknown merge type!" << endl;
		return updated;
	}

	// Remove empty clusters
	vector<PointPairCluster>::iterator iter = clusters.begin();
	while (iter != clusters.end())
	{
		if (iter->point1.size() <= 4)
			iter = clusters.erase(iter);
		else
			iter++;
	}

	return updated;
}

void MultiPlanarFitting::iterativeRefinement1(vector<PointPairCluster> &clusters, const double thr_reproj,
	const double thr_merge, const int merge_type)
{
	while (mergeCluster(clusters, merge_type, thr_merge))
	{
		//findHomography(clusters);
		collectInliers(clusters, false, thr_reproj);
	}
}

void MultiPlanarFitting::iterativeRefinement2(vector<PointPairCluster> &clusters, const double thr_reproj,
	const double thr_merge, const int merge_type)
{
	int n_points;
	m_points1.size();
	vector<bitset<MAX_POINTS_NUM>> point_inclusion_info(clusters.size());

	// Initialize bitsets
	setNormals(clusters);
	for (size_t ci = 0; ci < clusters.size(); ci++)
	{
		//clusters[ci].normal = computeNormal(cv::findHomography(clusters[ci].point1, clusters[ci].point2));
		clusters[ci].H = m_modelEstimator.getHomography(clusters[ci].point1, clusters[ci].point2, clusters[ci].normal, clusters[ci].t);
		collectInliers(clusters[ci], thr_reproj);

		for (size_t pi = 0; pi < clusters[ci].size(); pi++)
		{
			point_inclusion_info[ci][point_indexer[clusters[ci].point1[pi]]] = 1;
		}
	}

	while (true)
	{
		int num_clusters = (int)clusters.size();
		double best_j_dist = 1.0;
		int best_cluster1, best_cluster2;

		for (int ci = 0; ci < num_clusters-1; ci++)
		{
			for (int cj = ci+1; cj < num_clusters; cj++)
			{
				int num_intersection = (point_inclusion_info[ci] & point_inclusion_info[cj]).count();
				int num_union = (int)clusters[ci].point1.size() + (int)clusters[cj].point1.size() - num_intersection;
				double cur_j_dist = ((double)num_union - (double)num_intersection) / (double)num_union;

				if (cur_j_dist < best_j_dist)
				{
					best_j_dist = cur_j_dist;
					best_cluster1 = ci;
					best_cluster2 = cj;
				}
			}
		}

		// If best jaccard distance is less than the threshold, merge the clusters
		if (best_j_dist <= thr_merge)
		{
			// Combine point lists
			clusters[best_cluster1].point1.insert(clusters[best_cluster1].point1.begin(), clusters[best_cluster2].point1.begin(),
				clusters[best_cluster2].point1.end());
			clusters[best_cluster1].point2.insert(clusters[best_cluster1].point2.begin(), clusters[best_cluster2].point2.begin(),
				clusters[best_cluster2].point2.end());

			// Remove the merged cluster and update bitset
			clusters.erase(clusters.begin() + best_cluster2);
			//point_inclusion_info[best_cluster1] |= point_inclusion_info[best_cluster2];

			// Estimate homography and collect inliers
			clusters[best_cluster1].normal = computeNormal(cv::findHomography(clusters[best_cluster1].point1, clusters[best_cluster1].point2));
			clusters[best_cluster1].H = m_modelEstimator.getHomography(clusters[best_cluster1].point1, clusters[best_cluster1].point2,
				clusters[best_cluster1].normal, clusters[best_cluster1].t);
			collectInliers(clusters[best_cluster1], thr_reproj);

			// Update point inclusion information as a bitset
			point_inclusion_info[best_cluster1].reset();
			for (size_t pi = 0; pi < clusters[best_cluster1].size(); pi++)
			{
				point_inclusion_info[best_cluster1][point_indexer[clusters[best_cluster1].point1[pi]]] = 1;
			}
		}
		else
		{
			break;
		}
	}
}

void MultiPlanarFitting::collectInliers(PointPairCluster &cluster, const double reproj_thr)
{
	vector<Point2f> points2_proj;

	cluster.point1.clear();
	cluster.point2.clear();

	// Recalculate inliers for all correspondences
	perspectiveTransform(m_points1, points2_proj, cluster.H);

	// Calculate average differences and the number of inliers
 	for (size_t pi = 0; pi < m_points1.size(); pi++)
	{
		double dist = norm(points2_proj[pi] - m_points2[pi]);

		if (dist < reproj_thr)
		{
			cluster.point1.push_back(m_points1[pi]);
			cluster.point2.push_back(m_points2[pi]);
		}
	}
}

void MultiPlanarFitting::collectInliers(vector<PointPairCluster> &mClustersList, bool computeHomography, const double reproj_thr)
{
	for (size_t ci = 0; ci < mClustersList.size(); ci++)
	{
		if (mClustersList[ci].point1.size() < 4)
			continue;

		if (computeHomography)
			mClustersList[ci].H = cv::findHomography(mClustersList[ci].point1, mClustersList[ci].point2);

		mClustersList[ci].normal = computeNormal(mClustersList[ci].H);
		vector<Point2f> points2_proj;

		mClustersList[ci].point1.clear();
		mClustersList[ci].point2.clear();

		// Recalculate inliers for all correspondences
		perspectiveTransform(m_points1, points2_proj, mClustersList[ci].H);

		// Calculate average differences and the number of inliers
 		for (size_t pi = 0; pi < m_points1.size(); pi++)
		{
			double dist = norm(points2_proj[pi] - m_points2[pi]);

			if (dist < reproj_thr)
			{
				mClustersList[ci].point1.push_back(m_points1[pi]);
				mClustersList[ci].point2.push_back(m_points2[pi]);
			}
		}
	}

	// Remove empty clusters
	vector<PointPairCluster>::iterator iter = mClustersList.begin();
	while (iter != mClustersList.end())
	{
		if (iter->point1.size() <= 4)
			iter = mClustersList.erase(iter);
		else
			iter++;
	}
}

void MultiPlanarFitting::collectInliersUnique(Clusters &clusters_, const double reproj_thr)
{
	int n_clusters = (int)clusters_.size();

	int *best_idx = new int[m_points1.size()];
	double *reproj_err = new double[m_points1.size()];

	// Initialization
	memset(best_idx, -1, sizeof(int)*m_points1.size());
	for (size_t pi = 0; pi < m_points1.size(); pi++)
		reproj_err[pi] = reproj_thr + 1.0;

	for (int ci = 0; ci < n_clusters; ci++)
	{
		vector<Point2f> points_proj;

		Mat H = cv::findHomography(clusters_[ci].points1, clusters_[ci].points2, CV_RANSAC, reproj_thr);

		// Recalculate inliers for all correspondences
		perspectiveTransform(m_points1, points_proj, H);

		// Calculate average differences and the number of inliers
		for (size_t pi = 0; pi < m_points1.size(); pi++)
		{
			double dist = norm(points_proj[pi] - m_points2[pi]);

			if (dist < reproj_err[pi])
			{
				reproj_err[pi] = dist;
				best_idx[pi] = ci;
			}
		}

		clusters_.clear(ci);
	}

	for (size_t pi = 0; pi < m_points1.size(); pi++)
		clusters_.addPoint(m_points1[pi], m_points2[pi], best_idx[pi]);

	delete[] best_idx;
	delete[] reproj_err;
}

void MultiPlanarFitting::outliersRejection(vector<PointPairCluster> &clusters, int MSS, double p, double alpha_thr)
{
	int min_inliers = 0;
	size_t rem_beg_idx = clusters.size();
	double sum = 0.0;

	binomial prob((double)m_points1.size(), p);

	for (int k = 0; k < (int)m_points1.size(); k++)
	{
		sum += boost::math::pdf(prob, k);

		if (1.0-sum <= alpha_thr)
		{
			min_inliers = k;
			break;
		}
	}

	sort(clusters.begin(), clusters.end(), ClusterSizeSort);

	for (size_t i = 0; i < clusters.size(); i++)
	{
		if (clusters[i].point1.size() <= min_inliers)
		{
			rem_beg_idx = i;
			break;
		}
	}

	clusters.erase(clusters.begin() + rem_beg_idx, clusters.end());
	/*
	// Add dummy cluster
	PointPairCluster dummy(2);
	clusters.push_back(dummy);
	int max_size = 0;
	rem_beg_idx = clusters.size();

	for (size_t i = 0; i < clusters.size()-1; i++)
	{
		if (max_size <= clusters[i].size() - clusters[i+1].size())
		{
			max_size = clusters[i].size() - clusters[i+1].size();
			rem_beg_idx = i+1;
		}
	}

	clusters.erase(clusters.begin() + rem_beg_idx, clusters.end());
	*/
}

//------------------------------------------
// INTERFACE FUNCTION
//------------------------------------------

Mat MultiPlanarFitting::decomposeRotationMatrix(const Mat &R)
{
	double roration_angle_x = atan2(R.at<double>(2, 1), R.at<double>(2, 2));
	double roration_angle_z = atan2(R.at<double>(1, 0), R.at<double>(0, 0));

	Mat R_x = (Mat_<double>(3, 3) << 1, 0, 0, 0, cos(roration_angle_x), -sin(roration_angle_x), 0, sin(roration_angle_x), cos(roration_angle_x));
	Mat R_z = (Mat_<double>(3, 3) << cos(roration_angle_z), -sin(roration_angle_z), 0, sin(roration_angle_z), cos(roration_angle_z), 0, 0, 0, 1);
	
	return R_x*R_z;
}

Clusters MultiPlanarFitting::RANSACClustering(const HomographyEstimator &estimator, int n_iter, double reproj_thr)
{
	int n_points = (int)m_points1.size();
	int n_clusters = 0;
	int minimumSampleSet;
	int *new_take_map = new int[n_points], *opt_take_map = new int[n_points];
	int mKdTreeRange = 10;
	double mKdTreeCloseProb = 0.8;
	double mKdTreeFarProb = 0.2;
	Clusters clusters(m_points1, m_points2);

	// Copy correspondences
	vector<Point2f> allpoints1 = m_points1;
	vector<Point2f> allpoints2 = m_points2;
	
	// Set the number of minimal sample set
	switch (estimator.getModelType())
	{
	case HOMOGRAPHY_MANHATTAN_WORLD:
		minimumSampleSet = 2;
		break;
	case HOMOGRAPHY_WEAK_MANHATTAN_WORLD:
		minimumSampleSet = 3;
		break;
	case HOMOGRAPHY_UNCONSTRAINED:
	default:
		minimumSampleSet = 4;
	}

	// Iteratively generate clusters
	for (int it = 0; it < 10; it++)
	{
		int n_inliers_opt = 0, n_inliers_crt;

		n_points = (int)allpoints1.size();
		if (n_points <= mKdTreeRange || n_points < minimumSampleSet)
			break;

		memset(opt_take_map, 0, sizeof(int)*n_points);

		// Random sampling of points -- closer points will be selected together in high probability
		vector<vector<unsigned int>> pointSamples;
		RandomSampler mRandomSampler(2, minimumSampleSet, allpoints1.size(), true);
		mRandomSampler.SetPoints(m_points1, m_points2);
		mRandomSampler.SetNFSamplingTypeNN(mKdTreeRange, (float)mKdTreeCloseProb, (float)mKdTreeFarProb, false);
		mRandomSampler.GetNSample(pointSamples, n_iter);

		// Iterate many times to get the best consensus set
		for (int it2 = 0; it2 < n_iter; it2++)
		{
			vector<Point2f> crt_points1, crt_points2, points_t;

			for (size_t i = 0; i < pointSamples[it2].size(); i++)
			{
				crt_points1.push_back(allpoints1[pointSamples[it2][i]]);
				crt_points2.push_back(allpoints2[pointSamples[it2][i]]);
			}
			
			// Calculate consensus set
			perspectiveTransform(allpoints1, points_t, estimator.getHomography(crt_points1, crt_points2));
			n_inliers_crt = 0;
			memset(new_take_map, 0, sizeof(int)*n_points);
			for (int pi = 0; pi < n_points; pi++)
			{
				if (norm(points_t[pi] - allpoints2[pi]) < reproj_thr)
				{
					new_take_map[pi] = 1;
					n_inliers_crt++;
				}
			}

			// If the number of points in computed consensus set is
			// less than the minimal sample set, discard the cluster
			if (crt_points1.size() < minimumSampleSet)
				continue;

			// If the number of points in current consensus set is larger 
			// than the number of points in the optimal consensus set,
			// replace the optimal consensus set with the current consensus set.
			if (n_inliers_crt > n_inliers_opt)
			{
				memcpy(opt_take_map, new_take_map, sizeof(int)*n_points);
				n_inliers_opt = n_inliers_crt;
			}
		}

		// Add the optimal cluster
		if (n_inliers_opt >= minimumSampleSet)
		{
			// Add taken points to new cluster, and remove them from points vector
			vector<Point2f>::iterator iter1 = allpoints1.begin();
			vector<Point2f>::iterator iter2 = allpoints2.begin();
			for (int pi = 0; pi < n_points; pi++)
			{
				if (opt_take_map[pi])
				{
					clusters.addPoint(*iter1, *iter2, n_clusters);
					iter1 = allpoints1.erase(iter1);
					iter2 = allpoints2.erase(iter2);
				}
				else
				{
					iter1++;
					iter2++;
				}
			}
		
			n_clusters++;
		}
		else
			break;
	}

	clusters.sort();

	delete[] new_take_map;
	delete[] opt_take_map;

	return clusters;
}

//------------------------------------------
// HOMOGRAPHY RELATED FUNCTIONS
//------------------------------------------

int MultiPlanarFitting::computeNormal(const Mat &H)
{
	Mat w, u, vt, v1;
	Mat H_E = m_K[1].inv() * H * m_K[0];
	int n_ret;

	// Normalize the Euclidean homography
	SVD::compute(H_E, w);
	H_E /= w.at<double>(1);

	SVD::compute(H_E - m_R, w, u, vt);

	v1 = vt.rowRange(0, 1).t();		// choose the domininant singular vector
				
	double n1 = abs(v1.dot(m_n[0]));
	double n2 = abs(v1.dot(m_n[1]));
	double n3 = abs(v1.dot(m_n[2]));

	//cout << n1 << " " << n2 << " " << n3 << endl;
		
	// Choose the largest value to find a normal vector.
	if (n1 > n2) {
		if (n1 > n3) {
			n_ret = 0;
		} else {
			n_ret = 2;
		}
	} else {
		if (n2 > n3) {
			n_ret = 1;
		} else {
			n_ret = 2;
		}
	}

	return n_ret;
}

void MultiPlanarFitting::setNormals(vector<PointPairCluster> &mClustersList)
{
	for (size_t i = 0; i < mClustersList.size(); i++)
	{		
		if (mClustersList[i].point1.size() < 4)
			mClustersList[i].H = m_modelEstimator.getHomography(mClustersList[i].point1, mClustersList[i].point2);
		else
			mClustersList[i].H = cv::findHomography(mClustersList[i].point1, mClustersList[i].point2);

		mClustersList[i].normal = computeNormal(mClustersList[i].H);
	}
}

void MultiPlanarFitting::setHomography(const HomographyEstimator &estimator, Clusters &clusters)
{
	switch (estimator.getModelType())
	{
	case HOMOGRAPHY_UNCONSTRAINED:
		for (int i = 0; i < (int)clusters.size(); i++) {
			clusters[i].Homography = cv::findHomography(clusters[i].points1, clusters[i].points2);
		}
		break;
	case HOMOGRAPHY_MANHATTAN_WORLD:
		for (int i = 0; i < (int)clusters.size(); i++) {
			clusters[i].Homography = estimator.computeParameters(clusters[i].points1,
				clusters[i].points2, clusters[i].translation, clusters[i].plane_normal);
		}
		break;
	case HOMOGRAPHY_WEAK_MANHATTAN_WORLD:
		for (int i = 0; i < (int)clusters.size(); i++) {
			clusters[i].Homography = estimator.getHomography(clusters[i].points1, clusters[i].points2);
		}
		break;
	default:
		throw new exception("Not implemented");
		break;
	}
}

// Compute homographies using point correspondences of each cluster,
// and point correspondences of all possible pair of clusters.
vector<Mat> MultiPlanarFitting::computeSingleNPairHomographies(const Clusters &clusters, const HomographyEstimator &estimator)
{
	vector<Mat> homographies;

	// First compute a homography for each cluster
	for (int i = 0; i < int(clusters.size()); i++) {
		Mat homography = estimator.getHomography(clusters[i].points1, clusters[i].points2);

		if (!homography.empty()) {
			homographies.push_back(homography);
		}
	}

	// Then, compute a homography for each pair of clusters
	for (int i = 0; i < int(clusters.size()) - 1; i++) {
		for (int j = i + 1; j < int(clusters.size()); j++) {
			Cluster cluster_tmp = ClusterUtils::mergeClusters(clusters[i], clusters[j]);
			Mat homography = estimator.getHomography(cluster_tmp.points1, cluster_tmp.points2);

			if (!homography.empty()) {
				homographies.push_back(homography);
			}
		}
	}

	return homographies;
}

Clusters MultiPlanarFitting::MultiplePlaneDetection(Clusters &original_clusters, const HomographyEstimator &estimator, const double thr_reproj)
{
	bool is_updated = true;

	vector<Cluster> clusters = ClusterUtils::clustersToClusterArray(original_clusters);

	// Global merging
	while (is_updated)
	{
		is_updated = false;

		// Compute Jaccard distances
		std::multimap<double, std::pair<int, int>, std::greater<double>> hashmap;
		for (int i = 0; i < (int)clusters.size() - 1; i++) {
			for (int j = i + 1; j < (int)clusters.size(); j++) {
				double dist = computeMergeDistance(clusters[i], clusters[j], thr_reproj);
				if (dist <= thr_reproj) {
					hashmap.insert(std::pair<double, std::pair<int, int>>(dist, std::pair<int, int>(i, j)));
				}
			}
		}

		// find cluster pair to be merged
		if (!hashmap.empty()) {
			int ind1 = hashmap.begin()->second.first;
			int ind2 = hashmap.begin()->second.second;

			ClusterUtils::mergeClusters(clusters, ind1, ind2);
			is_updated = true;
		}
	}
	
	// Make sure that all the points have the positive numbers
	double min_x = 0, min_y = 0, max_x = images[0].cols, max_y = images[0].rows;
	for (int i = 0; i < m_points1.size(); i++) {
		if (m_points1[i].x < min_x) {
			min_x = m_points1[i].x;
		}
		if (m_points1[i].y < min_y) {
			min_y = m_points1[i].y;
		}
		if (m_points1[i].x > max_x) {
			max_x = m_points1[i].x;
		}
		if (m_points1[i].y > max_y) {
			max_y = m_points1[i].y;
		}
	}
	if (min_x < 0) {
		for (int i = 0; i < clusters.size(); i++) {
			for (int j = 0; j < clusters[i].size(); j++) {
				clusters[i].points1[j].x -= min_x;
			}
		}
		max_x -= min_x;
	}
	if (min_y < 0) {
		for (int i = 0; i < clusters.size(); i++) {
			for (int j = 0; j < clusters[i].size(); j++) {
				clusters[i].points1[j].y -= min_y;
			}
		}
		max_y -= min_y;
	}
	
	// Spatial analysis
	vector<Cluster> new_cluster;
	for (size_t i = 0; i < clusters.size(); i++)
	{
		Subdiv2D subdiv(Rect(0, 0, max_x, max_y));
		vector<Vec4f> edgeList;
		vector<float> distanceList;
		vector<int> label;
		float mean = 0.0;
		float stddev = 0.0;
		int num_label;

		for (size_t pi = 0; pi < clusters[i].points1.size(); pi++)
			subdiv.insert(clusters[i].points1[pi]);

		subdiv.getEdgeList(edgeList);

		for (size_t ei = 0; ei < edgeList.size(); ei++)
		{
			if (edgeList[ei][0] > images[0].cols || edgeList[ei][1] > images[0].rows || edgeList[ei][0] < 0 || edgeList[ei][1] < 0 ||
				edgeList[ei][2] > images[0].cols || edgeList[ei][3] > images[0].rows || edgeList[ei][2] < 0 || edgeList[ei][3] < 0)
				continue;

			distanceList.push_back(sqrt(pow((edgeList[ei][0] - edgeList[ei][2]), 2) + pow((edgeList[ei][1] - edgeList[ei][3]), 2)));
			mean += distanceList.back();
		}
		mean /= (float)edgeList.size();
		
		for (size_t ei = 0; ei < distanceList.size(); ei++)
			stddev += pow(distanceList[ei] - mean, 2);
		stddev = sqrt(stddev / (float)edgeList.size());
		
		num_label = partition(clusters[i].points1, label, EqKeypoints(stddev));
		vector<Cluster> split_cluster(num_label);
		for (size_t j = 0; j < label.size(); j++)
		{
			split_cluster[label[j]].points1.push_back(clusters[i].points1[j]);
			split_cluster[label[j]].points2.push_back(clusters[i].points2[j]);
		}

		for (size_t j = 0; j < split_cluster.size(); j++) {
			if (split_cluster[j].points1.size() >= 6) {
				new_cluster.push_back(split_cluster[j]);
			}
		}
	}
	if (min_x < 0) {
		for (int i = 0; i < new_cluster.size(); i++) {
			for (int j = 0; j < new_cluster[i].size(); j++) {
				new_cluster[i].points1[j].x += min_x;
			}
		}
	}
	if (min_y < 0) {
		for (int i = 0; i < new_cluster.size(); i++) {
			for (int j = 0; j < new_cluster[i].size(); j++) {
				new_cluster[i].points1[j].y += min_y;
			}
		}
	}
	clusters = new_cluster;

	// Robust fitting and stability checks
	for (int i = 10; i >= 0; i--) {
		if (clusters.empty()) {
			return Clusters();
		}
		ClusterUtils::computeInliers(clusters, m_points1, m_points2, thr_reproj * (1.0 + double(i) * 0.2));
	}
	return ClusterUtils::computeUniqueInliers(clusters, m_points1, m_points2, thr_reproj);
}

// Compute distance between two clusters
// The function is appeared in Multiple Plane Detection in Image Pairs using J-Linkage
double MultiPlanarFitting::computeMergeDistance(const Cluster &cluster1, const Cluster &cluster2, const double threshold)
{
	std::unordered_set<Match, MatchHash> matchHashmap;
	vector<Point2f> points1 = cluster1.points1, points2 = cluster1.points2;

	// Insert matches of the first cluster to hashmap
	for (int i = 0; i < int(cluster1.size()); i++) {
		matchHashmap.insert(cluster1.getMatch(i));
	}

	// Compare existance
	int num_union = int(cluster1.size());
	for (int i = 0; i < int(cluster2.size()); i++) {
		if (matchHashmap.find(cluster2.getMatch(i)) == matchHashmap.end()) {
			points1.push_back(cluster2.points1[i]);
			points2.push_back(cluster2.points2[i]);
			num_union++;
		}
	}

	vector<double> residuals = ClusterUtils::computeResiduals(cv::findHomography(points1, points2), points2, points2, threshold);
	double sum = 0.0;

	for (int i = 0; i < (int)residuals.size(); i++) {
		sum += residuals[i];
	}

	return sum / double(num_union);
}

void MultiPlanarFitting::RANSACClusterRefinement(vector<PointPairCluster> &mClustersList, const double reproj_thr)
{
	for (size_t i = 0; i < mClustersList.size(); i++)
	{
		vector<Point2f> points1, points2, points2_proj;

		mClustersList[i].H = cv::findHomography(mClustersList[i].point1, mClustersList[i].point2, CV_RANSAC);

		// Recalculate inliers for all correspondences
		perspectiveTransform(m_points1, points2_proj, mClustersList[i].H);

		// Calculate average differences and the number of inliers
 		for (size_t pi = 0; pi < m_points1.size(); pi++)
		{
			double dist = norm(points2_proj[pi] - m_points2[pi]);

			if (dist < reproj_thr)
			{
				points1.push_back(m_points1[pi]);
				points2.push_back(m_points2[pi]);
			}
		}

		mClustersList[i].point1 = points1;
		mClustersList[i].point2 = points2;
	}
}

//------------------------------------------
// UTILITY FUNCTIONS
//------------------------------------------


// Calculate euclid distance
double MultiPlanarFitting::euclidDistance(Mat& vec1, Mat& vec2)
{
	double sum = 0.0;
	int dim = vec1.cols;
	for (int i = 0; i < dim; i++)
		sum += (vec1.at<uchar>(0, i) - vec2.at<uchar>(0, i)) * (vec1.at<uchar>(0, i) - vec2.at<uchar>(0, i));

	return sqrt(sum);
}

// Find the index of nearest neighbor point from keypoints.
int MultiPlanarFitting::nearestNeighbor(Mat& vec, vector<KeyPoint>& keypoints, Mat& descriptors)
{
	int neighbor = -1;
	double minDist = 1e6;

	for (int i = 0; i < descriptors.rows; i++) {
		KeyPoint pt = keypoints[i];
		Mat v = descriptors.row(i);
		double d = euclidDistance(vec, v);
		//printf("%d %f\n", v.cols, d);
		if (d < minDist) {
			minDist = d;
			neighbor = i;
		}
	}

	if (minDist < MATCH_THRESHOLD)
		return neighbor;

	return -1;
}

//------------------------------------------
// FILE INPUT/OUTPUT INTERFACE FUNCTIONS
//------------------------------------------

bool MultiPlanarFitting::loadNormals(const char *filename) {
	Mat normal = load3x3Matrix(filename);

	if (normal.empty()) {
		return false;
	}

	// Initialize normal vectors
	m_n[0] = (Mat_<double>(3, 1) << normal.at<double>(0, 0), normal.at<double>(1, 0), normal.at<double>(2, 0));
	m_n[1] = (Mat_<double>(3, 1) << normal.at<double>(0, 1), normal.at<double>(1, 1), normal.at<double>(2, 1));
	m_n[2] = (Mat_<double>(3, 1) << normal.at<double>(0, 2), normal.at<double>(1, 2), normal.at<double>(2, 2));

	return true;
}

Mat MultiPlanarFitting::load3x3Matrix(const std::string &filename)
{
	Mat mat(3, 3, CV_64F);
	fstream fin(filename, fstream::in);
	
	if (!fin.is_open())
	{
		printf ("Not able to open the 3x3 matrix data file: %s\n", strerror(errno));
		return Mat();
	}

	fin >> mat.at<double>(0, 0);
	fin >> mat.at<double>(0, 1);
	fin >> mat.at<double>(0, 2);
	fin >> mat.at<double>(1, 0);
	fin >> mat.at<double>(1, 1);
	fin >> mat.at<double>(1, 2);
	fin >> mat.at<double>(2, 0);
	fin >> mat.at<double>(2, 1);
	fin >> mat.at<double>(2, 2);

	fin.close();

	return mat;
}

bool MultiPlanarFitting::loadClusters(const char *filename, vector<PointPairCluster> &clusters)
{
	fstream fin;

	fin.open(filename, fstream::in);
	int num, ci = 0;
	Point2f pt1, pt2;

	if (!fin.is_open())
	{
		cout << "Not able to open clusters data file: " << strerror(errno) << endl;;
		return false;
	}

	while (fin >> num)
	{
		clusters.push_back(PointPairCluster());

		for (int i = 0; i < num; i++)
		{
			fin >> pt1.x >> pt1.y >> pt2.x >> pt2.y;
			clusters[ci].point1.push_back(pt1);
			clusters[ci].point2.push_back(pt2);
		}

		ci++;
	}

	fin.close();

	return true;
}

void MultiPlanarFitting::DrawPointsOnImage(Mat &image, const PointPairCluster &cluster, const Scalar &color)
{
	int n_points = cluster.point1.size() < cluster.point2.size() ? cluster.point1.size() : cluster.point2.size();

	for (int pi = 0; pi < n_points; pi++)
		circle(image, cluster.point1[pi], 3, color, 2);
}

void MultiPlanarFitting::saveClusters(const char *filename, const vector<PointPairCluster> &mClustersList)
{
	int n_points;
	fstream fout(filename, fstream::out);

	for (int ci = 0; ci < static_cast<int>(mClustersList.size()); ci++)
	{
		n_points = static_cast<int>(mClustersList[ci].point1.size());
		fout << n_points << endl;

		for (int pi = 0; pi < n_points; pi++)
		{
			fout << mClustersList[ci].point1[pi].x << " " << mClustersList[ci].point1[pi].y << " " << mClustersList[ci].point2[pi].x << " " << mClustersList[ci].point2[pi].y << endl;
		}
	}

	fout.close();
}

void MultiPlanarFitting::saveEstimatedParameters(const char *filename, const vector<PointPairCluster> &mClustersList)
{
	fstream fout(filename, fstream::out);

	fout << "K=" << m_K[0] << ";" << endl;
	for (size_t i = 0; i < mClustersList.size(); i++)
	{
		fout << "t(:," << i+1 << ")=" << mClustersList[i].t << ";" << endl;
		fout << "n(:," << i+1 << ")=" << m_n[mClustersList[i].normal] << ";" << endl;
	}

	fout.close();
}

void MultiPlanarFitting::DoExperiments(HomographyEstimator &estimator, const char *resultFolderName, const ExperimentParameters &params)
{
	char output_file_base_name[255];
	char filename[255];
	char filename_appendix[16];
	int num_ransac_iter = 500;
	int iteration;

	Clusters clusters;
	vector<PointPairCluster> clustersList, clustersListClone;

	if (!m_clusterGroundTruth.empty()) {
		sprintf(filename, "Result\\%s\\ground-truth.png", resultFolderName);
		imwrite(filename, ClusterUtils::DrawPointsOnImage(m_clusterGroundTruth, images[0], true));
	}

	if (!m_clusterAGroundTruth.empty()) {
		sprintf(filename, "Result\\%s\\ground-truth-achievable.png", resultFolderName);
		imwrite(filename, ClusterUtils::DrawPointsOnImage(m_clusterAGroundTruth, images[0], true));
	}

	// Make result folder if it does not exist
	boost::filesystem::path result_folder(string("Result\\") + resultFolderName);
	if (!boost::filesystem::exists(result_folder)) {
		boost::filesystem::create_directory(result_folder);
	}

	// Initialize output file names
	cout << "***" << resultFolderName << "***" << endl;
	switch (estimator.getModelType())
	{
	case HOMOGRAPHY_MANHATTAN_WORLD:
		sprintf(filename_appendix, "_cons");
		break;
	case HOMOGRAPHY_WEAK_MANHATTAN_WORLD:
		sprintf(filename_appendix, "_weakcons");
		break;
	case HOMOGRAPHY_UNCONSTRAINED:
		sprintf(filename_appendix, "");
		break;
	default:
		sprintf(filename_appendix, "");
	}
	
	//------------------------------------------
	// Generate initial clusters
	//------------------------------------------
	switch (params.clustering_type) {
	// Sequential RANSAC
	case CLUSTERING_SEQ_RANSAC:
		sprintf(output_file_base_name, "%s%s", "seqRANSAC", filename_appendix);

		clusters = RANSACClustering(params.homography_type, num_ransac_iter, params.threshold_reprojection_error);

		break;

	// CC-RANSAC: Load a clustered result
	case CLUSTERING_CC_RANSAC:

		sprintf(output_file_base_name, "%s%s", "CCRANSAC", filename_appendix);

		// CC-RANSAC is implemented in Matlab. It loads the result of CC-RANSAC done on Matlab
		sprintf(filename, "Result\\%s%s00.txt", resultFolderName, output_file_base_name);
		loadClusters(filename, clustersList);

		break;

	// SA-RCM: Load a clustered result
	case CLUSTERING_SA_RCM:
			
		sprintf(output_file_base_name, "%s%s", "SA-RCM", filename_appendix);
	
		// Load the result of SA-RCM
		sprintf(filename, "Result\\%s%s00.txt", resultFolderName, output_file_base_name);
		imwrite(filename, ClusterUtils::DrawPointsOnImage(clusters, images[0], true));
		sprintf(filename, "%s%s00.txt", resultFolderName, output_file_base_name);
		loadClusters(filename, clustersList);

		break;

	// J-Linkage
	case CLUSTERING_J_LINKAGE:
			
		sprintf(output_file_base_name, "%s%s", "JLinkage", filename_appendix);

		clusters = JLinkageClustering(estimator, (float)params.threshold_reprojection_error);

		break;
		
	case CLUSTERING_T_LINKAGE:
			
		sprintf(output_file_base_name, "%s%s", "TLinkage", filename_appendix);
	
		// Load the result of SA-RCM
		sprintf(filename, "Result\\%s%s00.txt", resultFolderName, output_file_base_name);
		loadClusters(filename, clustersList);

		break;
	}

	if (!clusters.empty() && !m_clusterGroundTruth.empty()) {
		cout << "1) Computed Adjusted Rand Index w.r.t. GT is " << ClusterUtils::computeAdjustedRandIndex(m_clusterGroundTruth, clusters) << endl;
	}

	if (!clusters.empty() && !m_clusterAGroundTruth.empty()) {
		cout << "2) Computed Adjusted Rand Index w.r.t. AGT is " << ClusterUtils::computeAdjustedRandIndex(m_clusterAGroundTruth, clusters) << endl;
	}

	// Compute parameters
	if (params.homography_type == HOMOGRAPHY_MANHATTAN_WORLD) {
		for (int i = 0; i < clusters.size(); i++) {
			// Compute planar parameters -- plane normal vector and translation vector
			estimator.computeParameters(clusters[i].points1, clusters[i].points2,
				clusters[i].translation, clusters[i].plane_normal);
		}
	}

	// Save an intermediate result right after initial clustering
	//sprintf(filename, "Result\\%s\\%s00.txt", resultFolderName, output_file_base_name);
	//clusters.savePointClusters(filename);
	//sprintf(filename, "Result\\%s\\%s_result.m", resultFolderName, output_file_base_name);
	//clusters.save(filename, true);
	//sprintf(filename, "Result\\%s\\%s_globalParams.m", resultFolderName, output_file_base_name);
	//estimator.save(filename);
	//sprintf(filename, "Result\\%s\\%s_matches.m", resultFolderName, output_file_base_name);
	//IOUtils::saveMatchesToMatlabScript(filename, m_points1, m_points2);
	sprintf(filename, "Result\\%s\\%s00.png", resultFolderName, output_file_base_name);
	imwrite(filename, ClusterUtils::DrawPointsOnImageL(clusters, images[0], true));
		
	//------------------------------------------
	// Clustering Refinement (Model Reduction)
	//------------------------------------------
	Clusters tempClusters = clusters;
	vector<Cluster> clustersTemp;
	switch (params.type_refinement)
	{
	// Fouhey's method
	case MULTIPLE_PLANE_DETECTION:
		clusters = MultiplePlaneDetection(clusters, m_modelEstimator, params.threshold_reprojection_error);
		
		sprintf(output_file_base_name, "%s%s", output_file_base_name, "01_MPD");

		break;

	// Proposed iterative refinement
	case NORMAL_AWARE_MERGE:
		clustersTemp = ClusterUtils::clustersToClusterArray(clusters);
		ClusterUtils::computeInliers(clustersTemp, m_points1, m_points2, 2.5);
		ClusterUtils::mergeClusters(clustersTemp, estimator, m_points1, m_points2, 3.0);
		clusters = ClusterUtils::computeUniqueInliers(clustersTemp, m_points1, m_points2, 2.5);

		sprintf(output_file_base_name, "%s%s", output_file_base_name, "01_NAM");

		//iterativeRefinement1(clustersList, params.threshold_reprojection_error,
		//	params.threshold_merge_jaccard_dist, MERGE_JACCARD);
		/*
		iterativeRefinement2(clustersList, params.threshold_reprojection_error,
			params.threshold_merge_jaccard_dist, MERGE_JACCARD);
		
		sprintf(output_file_base_name, "%s%s", output_file_base_name, "01_IR");
		*/
		break;

	// Proposed Iterative J-Linkage
	case ITERATIVE_JLINKAGE:
		cout << "Begin iterative J-linkage" << endl;
		for (iteration = 0; iteration < params.num_max_refine_iteration; iteration++)
		{
			//setHomography(estimator, tempClusters);
			tempClusters = IterativeJLinkage(computeSingleNPairHomographies(tempClusters, estimator),
				(float)params.threshold_reprojection_error);
			if (clusters == tempClusters)
				break;
			clusters = tempClusters;
		}
		cout << "The number iterated: " << iteration << endl;

		sprintf(output_file_base_name, "%s%s", output_file_base_name, "01_IJ");
		 
		break;

	// Do nothing
	default:
		break;
	}

	if (!clusters.empty() && !m_clusterGroundTruth.empty()) {
		cout << "3) Computed Adjusted Rand Index w.r.t. GT is " << ClusterUtils::computeAdjustedRandIndex(m_clusterGroundTruth, clusters) << endl;
	}

	if (!clusters.empty() && !m_clusterAGroundTruth.empty()) {
		cout << "4) Computed Adjusted Rand Index w.r.t. AGT is " << ClusterUtils::computeAdjustedRandIndex(m_clusterAGroundTruth, clusters) << endl;
	}

	// Save an intermediate result right after the iterative refinement
	//sprintf(filename, "Result\\%s\\%s.txt", resultFolderName, output_file_base_name);
	//clusters.savePointClusters(filename);
	sprintf(filename, "Result\\%s\\%s.png", resultFolderName, output_file_base_name);
	imwrite(filename, ClusterUtils::DrawPointsOnImage(clusters, images[0], true));

	/*
	if (!clusters.empty() && !clustersGT.empty()) {
		cout << "2) Computed Adjusted Rand Index is " << ClusterUtils::computeAdjustedRandIndex(clustersGT, clusters) << endl;
	}
	*/
	return;
	/*
	//------------------------------------------
	// Outliers Removal
	//------------------------------------------
	if (params.do_outlier_rejection)
	{
		outliersRejection(clustersList, 2, 0.05, 0.01);	
		sprintf(output_file_base_name, "%s%s", output_file_base_name, "_OR");
	}

	m_modelEstimator.setModelType(HOMOGRAPHY_UNCONSTRAINED);
	collectInliersUnique(clustersList, params.threshold_reprojection_error);
	
	sprintf(filename, "%s%s_res.png", resultFolderName, output_file_base_name);
	DrawPointsOnImage(filename, clustersList, images[0], true, params.homography_type == HOMOGRAPHY_MANHATTAN_WORLD);
	sprintf(filename, "%s%s_res.txt", resultFolderName, output_file_base_name);
	saveClusters(filename, clustersList);
	
	//------------------------------------------
	// Follow-ups
	//------------------------------------------
	if (params.homography_type == HOMOGRAPHY_MANHATTAN_WORLD)
	{
		sprintf(filename, "%s%s_params.txt", resultFolderName, output_file_base_name);
		saveEstimatedParameters(filename, clustersList);
	}
	*/
}

int computeNumOfInliers(const cv::Mat &H, const vector<Point2f> &points1, const vector<Point2f> &points2, double threshold)
{
	int num_of_inliers = 0;
	vector<Point2f> points2_proj;

	// Recalculate inliers for all correspondences
	perspectiveTransform(points1, points2_proj, H);

	// Calculate average differences and the number of inliers
	for (int pi = 0; pi < (int)points1.size(); pi++)
	{
		// use reprojection error instead of Sampson error
		double dist = norm(points2_proj[pi] - points2[pi]);

		if (dist < threshold) {
			num_of_inliers++;
		}
	}

	return num_of_inliers;
}
