#include "HypergraphModeSeeking.h"
#include "ClusterUtils.h"

#include <boost\math\distributions\normal.hpp>
#include <boost\filesystem.hpp>

using std::vector;

using cv::Mat;

using std::cout;
using std::endl;
using std::string;

using boost::math::normal;
using boost::math::quantile;

namespace HypergraphModeSeeking {
	
	MSHClustering::MSHClustering() {

	}

	MSHClustering::MSHClustering(const cv::Mat &image1, const cv::Mat &image2, const std::vector<cv::Point2f> &points1,
		const std::vector<cv::Point2f> &points2) : BaseClustering(image1, image2, points1, points2)
	{
		
	}

	double MSHClustering::computeEpanechnikovKernel(double val, double bandwidth)
	{
		double u = val / bandwidth;
		if (abs(u) > 1.0)
			return 0.0;

		return 0.75 * (1.0 - u * u);
	}

	// Pick models with autoriy peaks
	vector<int> MSHClustering::pickModels(const vector<double> &mtd_vales)
	{
		vector<int> best_ind;
		vector<double> sorted_mtd_vales = mtd_vales;
		double threshold = 0.0;
		double max_jump = 0.0;
		int num_of_models = int(mtd_vales.size());

		// Sort T-distance
		sort(sorted_mtd_vales.begin(), sorted_mtd_vales.end(), std::greater<double>());

		for (int i = 1; i < (int)sorted_mtd_vales.size(); i++) {
			double jump = sorted_mtd_vales[i - 1] - sorted_mtd_vales[i];
			if (jump > max_jump) {
				max_jump = jump;
				threshold = sorted_mtd_vales[i];
			}
		}

		for (int i = 0; i < num_of_models; i++) {
			if (mtd_vales[i] > threshold) {
				best_ind.push_back(i);
			}
		}

		return best_ind;
	}

	// Pick models with autoriy peaks
	vector<int> MSHClustering::pickModels(const vector<double> &mtd_vales, int num_of_models_to_peak)
	{
		vector<int> best_ind;
		vector<double> sorted_mtd_vales = mtd_vales;
		double threshold = 0.0;
		int num_of_models = int(mtd_vales.size());

		// Sort T-distance
		sort(sorted_mtd_vales.begin(), sorted_mtd_vales.end(), std::greater<double>());
		
		threshold = sorted_mtd_vales[num_of_models_to_peak];

		cout << "-----Minimum T-distances----" << endl;
		for (int i = 0; i < num_of_models; i++) {
			if (mtd_vales[i] > threshold) {
				best_ind.push_back(i);
				cout << "(" << i << ") " << mtd_vales[i] << endl;
			}
		}
		cout << "----------------------------" << endl;

		return best_ind;
	}

	void MSHClustering::doClustering(vector<Cluster> &clusters, HomographyEstimator &estimator, int num_of_models)
	{
		// Generate random models
		vector<HomographyModel> models;
		GenerateModels(models, estimator, num_of_models);

		doClustering(clusters, models);
	}
	
	void MSHClustering::doClustering(vector<Cluster> &clusters, const vector<HomographyModel> &models, double constant_scale)
	{
		int num_of_points = int(m_points1.size());
		int K = int(m_points1.size()) * 0.1;
		int num_of_models = int(models.size());
		vector<vector<double>> residuals(num_of_models);
		vector<double> scales(num_of_models);
		vector<double> weighting_score(num_of_models);
		double bandwidth = 0.0;
		double highest_score = 0.0;
		vector<int> num_of_inliers(num_of_models);
		vector<boost::dynamic_bitset<>> incidentMatrix(num_of_models, boost::dynamic_bitset<>(num_of_points));
		
		// Compute the inlier scale for each homography
		for (int i = 0; i < num_of_models; i++) {
			// compute residuals
			computeResiduals(residuals[i], models[i].Homography);

			// estimate the inlier scale
			if (constant_scale > 0.0) {
				scales[i] = constant_scale;
			}
			else {
				scales[i] = ScaleEstimator(models[i].Homography, residuals[i], K);
			}
			if (scales[i] > bandwidth) {
				bandwidth = scales[i];
			}

			num_of_inliers[i] = 0;
			for (int j = 0; j < num_of_points; j++) {
				if (residuals[i][j] < scales[i]) {
					num_of_inliers[i]++;
				}
			}
		}

		// Generate an incident matrix and compute weighting scores.
		for (int i = 0; i < num_of_models; i++) {
			double numerator = 0.0;

			// Generate an incident matrix
			double denominator = scales[i] * bandwidth;
			for (int j = 0; j < num_of_points; j++) {
				incidentMatrix[i][j] = residuals[i][j] < scales[i];

				// Accumulate weighting score
				if (incidentMatrix[i][j]) {
					numerator += computeEpanechnikovKernel(residuals[i][j], bandwidth) / denominator;
				}
			}

			// Compute weighting score
			weighting_score[i] = numerator / double(num_of_inliers[i]);
			// check the highest weighting score
			if (weighting_score[i] > highest_score) {
				highest_score = weighting_score[i];
			}
		}

		//------------------------------
		// Mode-Seeking on Hypergraphs
		//------------------------------

		vector<Mat> preferences;
		vector<double> normSquares;
		vector<double> minimum_t_dist(num_of_models);

		// compute preferences of each vertex (model)
		for (int i = 0; i < num_of_models; i++) {
			Mat preference(num_of_points, 1, CV_64F);

			for (int j = 0; j < num_of_points; j++) {
				if (incidentMatrix[i][j]) {
					preference.at<double>(j) = exp(-residuals[i][j] / scales[i]);
				}
				else {
					preference.at<double>(j) = 0.0;
				}
			}

			preferences.push_back(preference);
			double preference_norm2 = norm(preference);
			normSquares.push_back(preference_norm2 * preference_norm2);
		}

		// Compute T-distance between two vertices
		for (int i = 0; i < num_of_models; i++) {
			if (weighting_score[i] == highest_score) {
				minimum_t_dist[i] = 0.0;

				for (int j = 0; j < num_of_models; j++) {
					if (i == j) {
						continue;
					}

					double dot_prod = preferences[i].dot(preferences[j]);
					double t_dist = 1.0 - dot_prod / (normSquares[i] + normSquares[j] - dot_prod);

					if (t_dist > minimum_t_dist[i]) {
						minimum_t_dist[i] = t_dist;
					}
				}
			}
			else {
				minimum_t_dist[i] = DBL_MAX;

				for (int j = 0; j < num_of_models; j++) {
					if (i == j) {
						continue;
					}

					if (weighting_score[j] > weighting_score[i]) {
						double dot_prod = preferences[i].dot(preferences[j]);
						double t_dist = 1.0 - dot_prod / (normSquares[i] + normSquares[j] - dot_prod);

						if (t_dist < minimum_t_dist[i]) {
							minimum_t_dist[i] = t_dist;
						}
					}
				}
			}
		}

		vector<int> best_ind = pickModels(minimum_t_dist);

		// Add points to a proper cluster
		clusters.clear();
		clusters.resize(int(best_ind.size()));
		Cluster outliers;
		for (int j = 0; j < num_of_points; j++) {
			bool isAdded = false;
			for (int i = 0; i < (int)best_ind.size(); i++) {
				if (incidentMatrix[best_ind[i]][j]) {
					clusters[i].points1.push_back(m_points1[j]);
					clusters[i].points2.push_back(m_points2[j]);
					isAdded = true;
				}
			}

			if (!isAdded) {
				outliers.points1.push_back(m_points1[j]);
				outliers.points2.push_back(m_points2[j]);
			}
		}
	}

	Clusters MSHClustering::doClustering(HomographyEstimator &estimator, const ExperimentParameters &params, const string &resultFolderName)
	{
		int num_of_points = int(m_points1.size());
		std::ostringstream stringStream;

#ifdef _DEBUG
		// Draw GT and AGT output
		if (!m_clusterGroundTruth.empty()) {
			stringStream << "Result\\" << resultFolderName << "\\GroundTruth_Original.png";
			imwrite(stringStream.str(), ClusterUtils::DrawPointsOnImage(m_clusterGroundTruth, images[0], true));
		}
		if (!m_clusterAGroundTruth.empty()) {
			stringStream.str(""); stringStream << "Result\\" << resultFolderName << "\\GroundTruth_Achievable.png";
			imwrite(stringStream.str(), ClusterUtils::DrawPointsOnImage(m_clusterAGroundTruth, images[0], true));
		}
#endif

		// Make result folder if it does not exist
		boost::filesystem::path result_folder(string("Result\\") + resultFolderName);
		if (!boost::filesystem::exists(result_folder)) {
			boost::filesystem::create_directory(result_folder);
		}
		// Initialize output file names
		char filename_appendix[16];
		switch (estimator.getModelType())
		{
		case HOMOGRAPHY_MANHATTAN_WORLD:
			sprintf(filename_appendix, "_cons");
			break;
		case HOMOGRAPHY_WEAK_MANHATTAN_WORLD:
			sprintf(filename_appendix, "_weakcons");
			break;
		case HOMOGRAPHY_UNCONSTRAINED:
			sprintf(filename_appendix, "");
			break;
		default:
			sprintf(filename_appendix, "");
		}

		// Generate random models
		vector<HomographyModel> models;
		GenerateModels(models, estimator, 3000);

		int K = int(m_points1.size()) / 20;
		int num_of_models = int(models.size());
		vector<double> scales(num_of_models);
		vector<double> weighting_score(num_of_models);
		vector<vector<double>> residuals(num_of_models);
		vector<int> num_of_inliers(num_of_models);
		bool **incidentMatrix;
		double bandwidth = 0.0;
		double highest_score = 0.0;

		// Allocate memory for incident matrix
		incidentMatrix = new bool*[num_of_models];
		for (int i = 0; i < num_of_models; i++)
			incidentMatrix[i] = new bool[num_of_points];

		// Compute the inlier scale for each homography
		for (int i = 0; i < num_of_models; i++) {
			// compute residuals
			computeResiduals(residuals[i], models[i].Homography);

			// estimate the inlier scale
			scales[i] = ScaleEstimator(models[i].Homography, residuals[i], K);
			if (scales[i] > bandwidth) {
				bandwidth = scales[i];
			}

			num_of_inliers[i] = 0;
			for (int j = 0; j < num_of_points; j++) {
				if (residuals[i][j] < scales[i]) {
					num_of_inliers[i]++;
				}
			}
		}

		// Generate an incident matrix and compute weighting scores.
		for (int i = 0; i < num_of_models; i++) {
			double numerator = 0.0;

			// Generate an incident matrix
			double denominator = scales[i] * bandwidth;
			for (int j = 0; j < num_of_points; j++) {
				incidentMatrix[i][j] = residuals[i][j] < scales[i];

				// Accumulate weighting score
				if (incidentMatrix[i][j]) {
					numerator += computeEpanechnikovKernel(residuals[i][j], bandwidth) / denominator;
				}
			}

			// Compute weighting score
			weighting_score[i] = numerator / double(num_of_inliers[i]);
			// check the highest weighting score
			if (weighting_score[i] > highest_score) {
				highest_score = weighting_score[i];
			}
		}

		//------------------------------
		// Mode-Seeking on Hypergraphs
		//------------------------------

		vector<Mat> preferences;
		vector<double> normSquares;
		vector<double> minimum_t_dist(num_of_models);

		// compute preferences of each vertex (model)
		for (int i = 0; i < num_of_models; i++) {
			Mat preference(num_of_points, 1, CV_64F);

			for (int j = 0; j < num_of_points; j++) {
				if (incidentMatrix[i][j]) {
					preference.at<double>(j) = exp(-residuals[i][j] / scales[i]);
				}
				else {
					preference.at<double>(j) = 0.0;
				}
			}

			preferences.push_back(preference);
			double preference_norm2 = norm(preference);
			normSquares.push_back(preference_norm2 * preference_norm2);
		}

		// Compute T-distance between two vertices
		for (int i = 0; i < num_of_models; i++) {
			if (weighting_score[i] == highest_score) {
				minimum_t_dist[i] = 0.0;

				for (int j = 0; j < num_of_models; j++) {
					if (i == j) {
						continue;
					}

					double dot_prod = preferences[i].dot(preferences[j]);
					double t_dist = 1.0 - dot_prod / (normSquares[i] + normSquares[j] - dot_prod);

					if (t_dist > minimum_t_dist[i]) {
						minimum_t_dist[i] = t_dist;
					}
				}
			}
			else {
				minimum_t_dist[i] = DBL_MAX;

				for (int j = 0; j < num_of_models; j++) {
					if (i == j) {
						continue;
					}

					if (weighting_score[j] > weighting_score[i]) {
						double dot_prod = preferences[i].dot(preferences[j]);
						double t_dist = 1.0 - dot_prod / (normSquares[i] + normSquares[j] - dot_prod);

						if (t_dist < minimum_t_dist[i]) {
							minimum_t_dist[i] = t_dist;
						}
					}
				}
			}
		}

		vector<int> best_ind = pickModels(minimum_t_dist);

		// Add points to a proper cluster
		vector<Cluster> clusters(int(best_ind.size()));
		Cluster outliers;
		for (int j = 0; j < num_of_points; j++) {
			bool isAdded = false;
			for (int i = 0; i < (int)best_ind.size(); i++) {
				if (incidentMatrix[best_ind[i]][j]) {
					clusters[i].points1.push_back(m_points1[j]);
					clusters[i].points2.push_back(m_points2[j]);
					isAdded = true;
				}
			}

			if (!isAdded) {
				outliers.points1.push_back(m_points1[j]);
				outliers.points2.push_back(m_points2[j]);
			}
		}
		ClusterUtils::removeClusters(clusters, 4);

		// Draw MSH output
		stringStream.str(""); stringStream << "Result\\" << resultFolderName << "\\MSH" << filename_appendix << "00.png";
		imwrite(stringStream.str(), ClusterUtils::DrawPointsOnImageL(clusters, images[0], outliers));

		// Compute unique inliers without special treatment
		vector<Mat> homographies = ClusterUtils::computeHomographies(clusters);
		//Clusters output = ClusterUtils::computeUniqueInliers(homographies, m_points1, m_points2, 2.5);
		Clusters output = ClusterUtils::computeUniqueInliers(homographies, m_points1, m_points2, 2.5);
		stringStream.str(""); stringStream << "Result\\" << resultFolderName << "\\MSH" << filename_appendix << "01.png";
		imwrite(stringStream.str(), ClusterUtils::DrawPointsOnImageL(output, images[0], true));
		cout << "------------ Analysis of MSH --------------" << endl;
		if (!m_clusterGroundTruth.empty()) {
			cout << "1) Computed Adjusted Rand Index w.r.t. GT is " << ClusterUtils::computeAdjustedRandIndex(m_clusterGroundTruth, output) << endl;
		}
		if (!m_clusterAGroundTruth.empty()) {
			cout << "2) Computed Adjusted Rand Index w.r.t. AGT is " << ClusterUtils::computeAdjustedRandIndex(m_clusterAGroundTruth, output) << endl;
		}
		cout << "-------------------------------------------" << endl;

		// Compute unique inliers with preceding merge process
		if (estimator.getModelType() == HOMOGRAPHY_MANHATTAN_WORLD) {
			vector<Cluster> clustersTemp = clusters;
			ClusterUtils::mergeClusters(clusters, estimator, m_points1, m_points2, 3.0);
			output = ClusterUtils::computeUniqueInliers(clusters, m_points1, m_points2, 2.5);
			stringStream.str(""); stringStream << "Result\\" << resultFolderName << "\\MSH" << filename_appendix << "02.png";
			imwrite(stringStream.str(), ClusterUtils::DrawPointsOnImageL(output, images[0], true));

			cout << "--- Analysis after the merge procedure ---" << endl;
			if (!m_clusterGroundTruth.empty()) {
				if (!m_clusterGroundTruth.empty()) {
					cout << "3) Computed Adjusted Rand Index w.r.t. GT is " << ClusterUtils::computeAdjustedRandIndex(m_clusterGroundTruth, output) << endl;
				}
			}
			if (!m_clusterAGroundTruth.empty()) {
				if (!m_clusterAGroundTruth.empty()) {
					cout << "4) Computed Adjusted Rand Index w.r.t. AGT is " << ClusterUtils::computeAdjustedRandIndex(m_clusterAGroundTruth, output) << endl;
				}
			}
			cout << "-------------------------------------------" << endl;
		}

		// deallocate memory
		for (int i = 0; i < num_of_models; i++)
			delete[] incidentMatrix[i];
		delete[] incidentMatrix;

		/*
		for (int i = 0; i < clusters.size(); i++) {
			Mat E = estimator.computeEssentialMatrix(clusters[i]);
			cout << "Epipolar error of " << i << ": " << ClusterUtils::computeEpipolarError(E, m_points1, m_points2) << endl;
		}
		*/

		//output.save("result.m", true);
		//estimator.save("globalParams.m");
		//IOUtils::saveMatchesToMatlabScript("matches.m", m_points1, m_points2);

		return output;
	}

	double ScaleEstimator(const Mat &H, vector<double> residuals, int K, double E)
	{
		int num_of_points = int(residuals.size());

		assert(K < num_of_points);

		// make sure that all the residuals are sorted in ascending order
		sort(residuals.begin(), residuals.end());

		// initialize the parameter
		normal s(0.0, 1.0);		// initialize the standard normal distribution
		double kappa = K / double(num_of_points);
		double scale = 1.0, scale_new = 0.0;
		int n_k;

		while (scale_new != scale && kappa < 1.0)
		{
			scale = scale_new;

			// Estimate the scale
			scale_new = residuals[K] / quantile(s, 0.5 * (1.0 + kappa));

			// Calculate the number of inliers
			n_k = 0;
			for (int i = 0; i < num_of_points; i++) {
				if ((abs(residuals[i]) / scale_new) < E) {
					n_k++;
				}
			}

			// Compute kappa
			kappa = K / double(n_k);
		}

		return scale;
	}
}