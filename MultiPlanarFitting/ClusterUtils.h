#pragma once

#include <cv.hpp>
#include <opencv2\highgui\highgui.hpp>

#include <vector>
#include <fstream>
#include <iostream>

#include "types.h"
#include "HomographyEstimator.h"
#include "JLinkage\RandomSampler.h"

class PointPairCluster
{
public:
	std::vector<cv::Point2f> point1;
	std::vector<cv::Point2f> point2;
	cv::Mat H;
	cv::Mat t;
	int normal;
	int tag;
	
	PointPairCluster();
	PointPairCluster(int size);

	size_t size();
};

cv::Scalar randomColor(cv::RNG& rng);

namespace ClusterUtils
{
	static const double	NN_SQ_DIST_RATIO_THR = 0.49;

	struct EqKeypoints
	{
		EqKeypoints(const double &threshold)
			: m_threshold(threshold) {}

		bool operator()(const cv::Point2f& a, const cv::Point2f& b) const
		{
			return norm(a - b) < m_threshold;
		}

		const double m_threshold;
	};

	/**
	 Pick random points from given points.
	 @params points1 Feature points of first image. Indices for points1 and points2 must be matched.
	 @params points2 Feature points of second image.  Indices for points1 and points2 must be matched.
	 @params numOfSamples The number of samples to be picked.
	 @params minimumSampleSet For each sampling, this number of points will be picked.
	 @params samplingType Type of point sampling. It could be either NFST_UNIFORM, NFST_EXP, NFST_NN, or NFST_NN_ME.
	 @returns The function will return the indices of randomly picked points.
	 */
	std::vector<std::vector<unsigned int>> pickRandomPoints(std::vector<cv::Point2f> points1, std::vector<cv::Point2f> points2,
		int numOfSamples, const int minimumSampleSet, RS_NFSAMPLINGTYPE samplingType = NFST_UNIFORM);
	
	// Generate models using given homography estimator and point matches.
	std::vector<HomographyModel> generateModels(const HomographyEstimator &estimator, const std::vector<std::vector<unsigned int>> &indices,
		const std::vector<cv::Point2f> &points1, const std::vector<cv::Point2f> &points2);

	// Compute matches with SIFT detector and FLANN matcher
	void computeMatches(const cv::Mat image1, const cv::Mat image2, std::vector<cv::Point2f> &points1, std::vector<cv::Point2f> &points2);

	// Compute matches with SIFT detector and FLANN matcher
	void computeMatches(const std::vector<cv::KeyPoint> keypoints1, const std::vector<cv::KeyPoint> keypoints2,
		cv::Mat descriptors1, cv::Mat descriptors2, std::vector<cv::Point2f> &points1, std::vector<cv::Point2f> &points2);

	// Remove matches if the mask value of corresponding coordinate is non-zero.
	void removeMatches(std::vector<cv::Point2f> &points1, std::vector<cv::Point2f> &points2, const cv::Mat &mask);

	// compute adjusted rand index of given clusters
	double computeAdjustedRandIndex(Clusters cluster_gt, Clusters cluster_est, bool considerOutliers = true);

	// compute adjusted rand index of given clusters
	double computeAdjustedRandIndexWithOurlierCluster(Clusters cluster_gt, Clusters cluster_est);

	// Compute Jaccard distance between two clusters
	double computeJaccardDistance(const Cluster &cluster1, const Cluster &cluster2);

	// Compute overlap coefficient between two clusters
	double computeOverlapCoefficient(const Cluster &cluster1, const Cluster &cluster2);

	// Test if centroid of each cluster are on the same side with respect to given vanishing line.
	bool isCentroidAtSameSide(const Cluster &cluster1, const Cluster &cluster2, const cv::Mat &VL);

	// Compute inlier scale
	double ScaleEstimator(const cv::Mat &H, const std::vector<cv::Point2f> points1, const std::vector<cv::Point2f> points2, int K, double E = 2.5);

	// Compute resiaul of each point w.r.t. given homography
	std::vector<double> computeResiduals(const cv::Mat &homography, const std::vector<cv::Point2f> points1,
		const std::vector<cv::Point2f> points2, double threshold = -1);

	// Compute a homography for each cluster
	void computeHomographies(Clusters &clusters, int type = CV_RANSAC);

	// Compute and set homography for each cluster
	void computeHomographies(Clusters &clusters, const HomographyEstimator &estimator);

	// Compute homography for each cluster
	std::vector<cv::Mat> computeHomographies(const std::vector<Cluster> &clusters, int type = CV_RANSAC);

	// Compute homographies using point correspondences of each cluster,
	// and point correspondences of all possible pair of clusters.
	std::vector<cv::Mat> computeSingleNPairHomographies(const Clusters &clusters, const HomographyEstimator &estimator);

	// Transform a vector of Cluster into Clusters
	Clusters clusterArrayToClusters(const std::vector<Cluster> &input,
		const std::vector<cv::Point2f> points1, const std::vector<cv::Point2f> points2);

	// Transform Clusters into a vector of Cluster
	std::vector<Cluster> clustersToClusterArray(const Clusters &input);

	//--------------------------------
	// Inliers Computation
	//--------------------------------
	
	// Compute inliers with given homography and reprojection error threshold, for given point matches.
	Cluster computeInliers(const cv::Mat &homography, const std::vector<cv::Point2f> &points1,
		const std::vector<cv::Point2f> &points2, const double threshold);

	// Compute inliers with given homography and reprojection error threshold, for given point matches.
	Cluster computeInliers(const cv::Mat &homography, const std::vector<Match> &pointMatches, const double threshold);

	// Compute inliers for each cluster with given reprojection error threshold.
	std::vector<Cluster> computeInliers(const std::vector<Cluster> &clusters_input, const double threshold);

	// Compute inliers with homographies of given clusters. If a homography is empty, it will compute
	// homography with points in each cluster
	std::vector<Cluster> computeInliers(const std::vector<Cluster> &clusters_input, const std::vector<cv::Point2f> &points1,
		const std::vector<cv::Point2f> &points2, const double threshold);

	// Compute inliers, using given vector of homographies. Each homography will generate a cluster.
	// The number of output clusters will be exactly same with the number of input homographies.
	std::vector<Cluster> computeInliers(const std::vector<cv::Mat> &homographies, const std::vector<cv::Point2f> &points1,
		const std::vector<cv::Point2f> &points2, const double threshold);

	// Compute inliers with point matches and homography of given cluster.
	// When it computes inliers, vanishing line constraints will be inforced.
	// VanishingLine must represents the line parameters of vanishing line, which is a 3-dimensional vector.
	Cluster computeInliers(const Cluster &cluster_input, const cv::Mat &VanishingLine,
		const std::vector<cv::Point2f> &points1, const std::vector<cv::Point2f> &points2, const double threshold);

	// For each cluster, compute inliers with point matches and homography of the cluster.
	// When it computes inliers, vanishing line constraints will be inforced.
	// Each column of VanishingLines must be a 3x3 matrix, and should represents the line parameters of vanishing line.
	std::vector<Cluster> computeInliers(const std::vector<Cluster> &cluster_input, const cv::Mat &VanishingLines,
		const std::vector<cv::Point2f> &points1, const std::vector<cv::Point2f> &points2, const double threshold);

	// Compute inliers for each homography. Each point match will be added to only one cluster, where the homography of 
	// that cluster has the minimum reprojection error among all the other clusters' homographies.
	Clusters computeUniqueInliers(const std::vector<cv::Mat> &homographies, const std::vector<cv::Point2f> &points1,
		const std::vector<cv::Point2f> &points2, const double threshold);

	// Compute inliers for each homography computed with each cluster. Each point match will be added to only one cluster,
	// where the homography of that cluster has the minimum reprojection error among all the other clusters' homographies.
	Clusters computeUniqueInliers(const std::vector<Cluster> &clusters, const std::vector<cv::Point2f> &points1,
		const std::vector<cv::Point2f> &points2, const double threshold);

	// Compute inliers for each cluster. Each point will be added to either one of clusters, or regarded as a outlier.
	// Vanishing line constraints will be enforced.
	Clusters computeUniqueInliers(const std::vector<Cluster> &clusters, const cv::Mat &VanishingLines,
		const std::vector<cv::Point2f> &points1, const std::vector<cv::Point2f> &points2, const double threshold);

	// Compute inliers for each cluster. Each point will be added to either one of clusters, or regarded as a outlier.
	// Vanishing line constraints will be enforced, and all parameters, including homographies, normal indices, will be
	// computed in prior to compute inliers.
	Clusters computeUniqueInliers(std::vector<Cluster> clusters, const HomographyEstimator &estimator,
		const std::vector<cv::Point2f> &points1, const std::vector<cv::Point2f> &points2, const double threshold);
	
	// Merge two clusters
	Cluster mergeClusters(Cluster cluster1, Cluster cluster2);

	// Merge designated clusters
	void mergeClusters(std::vector<Cluster> &clusters, int ind1, int ind2);

	// First compute homographies for each cluster, and merge them one by one.
	std::vector<Cluster> mergeClusters(std::vector<Cluster> clusters, const HomographyEstimator &estimator,
		const std::vector<cv::Point2f> &points1, const std::vector<cv::Point2f> &points2, double threshold = -1, cv::Mat img = cv::Mat());

	std::vector<Cluster> mergeClusters(std::vector<Cluster> clusters, const std::vector<cv::Point2f> &points1,
		const std::vector<cv::Point2f> &points2, double threshold);

	// Separate clusters using vanishing line constraint.
	std::vector<Cluster> separateClusters(std::vector<Cluster> clusters, const HomographyEstimator &estimator);

	// Merge clusters
	Clusters MultiplePlaneDetection(std::vector<Cluster> clusters, const std::vector<cv::Point2f> &points1,
		const std::vector<cv::Point2f> &points2, const cv::Size &size, const double thr_reproj);

	// Compute merge distance for Multiple Plane Detection algorithm
	double computeMergeDistance(const Cluster &cluster1, const Cluster &cluster2, const double threshold);

	// Merge two point vectors
	std::vector<cv::Point2f> mergePoints(const std::vector<cv::Point2f> &points1, const std::vector<cv::Point2f> &points2);

	// Remove clusters from the point where the jump of inlier points is the maximum.
	void removeMinorClusters(std::vector<Cluster> &clusters, int minimumSampleSet);

	double computeEpipolarError(const cv::Mat &transformation, const std::vector<cv::Point2f> &points1, const std::vector<cv::Point2f> &points2);

	// Remove clusters where the number of inliers are less than given threshold
	void removeClusters(std::vector<Cluster> &clusters, int threshold);

	// Draw point matches on vertically combined image of given images
	cv::Mat drawMatchesOnImage(const cv::Mat &image1, const cv::Mat &image2, const std::vector<cv::Point2f> &points1,
		const std::vector<cv::Point2f> &points2);

	// Test epipolar constraints
	// ||x2'Ex1'|| where E = R*[t]x
	double testEpipolarConnstraint(const Cluster& cluster, const std::vector<cv::Point2f> &points1, const std::vector<cv::Point2f> &points2);

	// Draw points on given image
	cv::Mat DrawPointsOnImage(const std::vector<cv::Point2f> points, const cv::Mat &image, int radius = 3);

	// Draw points on given image
	cv::Mat DrawPointsOnImage(const Clusters &clusters, const cv::Mat &image, bool drawOutliers, int radius = 3);

	// Draw points on given image with color legend
	cv::Mat DrawPointsOnImageL(const Clusters &clusters, const cv::Mat &image, bool drawOutliers, int radius = 3);

	// Draw points on given image with color legend
	cv::Mat DrawPointsOnImage(const std::vector<Cluster> &clusters, const cv::Mat &image, const Cluster &outliers, int radius = 3);

	// Draw points on given image with color legend
	cv::Mat DrawPointsOnImageL(const std::vector<Cluster> &clusters, const cv::Mat &image, const Cluster &outliers);

	// Draw points on given image with color legend.
	// If the last cluster of input is a cluster of outlier points, then set drawOutliers to true.
	cv::Mat DrawPointsOnImage(const std::vector<Cluster> &clusters, const cv::Mat &image, bool drawOutliers, int radius = 3);

	// Draw points on given image with color legend.
	// If the last cluster of input is a cluster of outlier points, then set drawOutliers to true.
	cv::Mat DrawPointsOnImageL(const std::vector<Cluster> &clusters, const cv::Mat &image, bool drawOutliers, int radius = 3);

	std::vector<cv::Scalar> getColors(int num_of_colors);
}
