#pragma once

#include <unordered_map>
#include <string>

#include <boost\functional\hash.hpp>
#include <opencv2\opencv.hpp>

const int OUTLIER_CLUSTER_INDEX = -1;
const int POINT_DOES_NOT_EXIST = -2;

typedef std::pair<cv::Point2f, cv::Point2f> Match;

std::ostream& operator<<(std::ostream& os, const Match &match);

enum GROUND_TRUTH_TYPE {
	GROUND_TRUTH_GENERAL = 0,
	GROUND_TRUTH_ACCHIEVABLE = 1
};

struct HomographyModel {
	cv::Mat Homography;		// Homography
	cv::Mat translation;	// Translation between two cameras
	cv::Mat plane_normal;	// Normal vector of plane inducing the homography
	int normal_index;		// Normal index
	int tag;
	double scale;			// inlier scale

	HomographyModel() : normal_index(0), tag(0), scale(0.0) {}

	HomographyModel(const cv::Mat &homography) : normal_index(0), tag(0), scale(0.0) {
		Homography = homography.clone();
	}

	HomographyModel(const cv::Mat &homography, const double threshold) : normal_index(0), tag(0), scale(threshold) {
		Homography = homography.clone();
	}
};

struct Cluster {
	std::vector<cv::Point2f> points1;
	std::vector<cv::Point2f> points2;

	// Homography matrix of the cluster
	cv::Mat Homography;

	// Plane translation matrix
	cv::Mat translation;
	
	// Plane normal
	cv::Mat plane_normal;

	// Normal index
	int normal_index;

	// Get the number of point correspondences
	size_t size() const;

	// Get the match object of given index of point correspondence
	Match getMatch(int index) const;

	// Print the cluster information
	void print();

	Cluster();

	Cluster(std::vector<Match> points);
};

bool ClusterSortByPoints(const Cluster &left, const Cluster &right);

std::ostream& operator<<(std::ostream& os, const Cluster &cluster);

class PointHash
{
public:
	std::size_t operator()(const cv::Point2f& lhs) const
	{
		std::size_t seed = 0;
		boost::hash_combine(seed, lhs.x);
		boost::hash_combine(seed, lhs.y);

		return seed;
	}
};

class MatchHash
{
public:
	std::size_t operator()(const Match& lhs) const
	{
		std::size_t seed = 0;
		boost::hash_combine(seed, lhs.first.x);
		boost::hash_combine(seed, lhs.first.y);
		boost::hash_combine(seed, lhs.second.x);
		boost::hash_combine(seed, lhs.second.y);

		return seed;
	}
};

class Clusters {
private:
	// vector of cluster instances
	std::vector<Cluster> matches;

	// Cluster instance denoting the outlier matches
	Cluster outliers;

	// Indexer to find which cluster includes given point
	std::unordered_map<Match, int, MatchHash> indexer;

public:

	//-------------------------------
	// Constructors
	//-------------------------------
	// Basic constructor: do nothing
	Clusters();

	Clusters(int num_of_clusters);

	// Construct the clusters with point vectors.
	Clusters(const std::vector<cv::Point2f> &points1, const std::vector<cv::Point2f> &points2);

	// Construct the clusters with point vectors.
	Clusters(int num_of_clusters, const std::vector<cv::Point2f> &points1, const std::vector<cv::Point2f> &points2);

	//-------------------------------
	// Getters
	//-------------------------------

	// Get a vector of homographies.
	std::vector<cv::Mat> getHomographies();

	// Get a vector of non-empty homographies
	std::vector<cv::Mat> getNonEmptyHomographies();

	// Get an index of cluster that contains the given match.
	int getClusterIndex(const Match &match) const;

	const Cluster &getOutliers() const;

	//-------------------------------
	// Utility functions
	//-------------------------------

	// Get the number of clusters
	size_t size() const;

	// Get the number of points
	size_t points() const;

	// Resize the number of clusters
	void initialize(int num_clusters);

	// Construct the clusters with point vectors.
	void initialize(int num_of_clusters, const std::vector<cv::Point2f> &points1, const std::vector<cv::Point2f> &points2);

	// Update point indexer
	void refreshIndices();

	// Add an empty cluster
	void addCluster();

	// Add a point correspondence to the designated cluster. If the index of cluster is negative,
	// the point correspondence will be added to the outlier cluster. If the index of cluster is
	// larger than the number of clusters, it will throw the out-of-range exception.
	void addPoint(cv::Point2f point1, cv::Point2f point2, int cluster_index);

	// Remove clusters of given range [beg_index, end_index).
	// @param beg_index first index of cluster that will be removed
	// @param end_index last index of cluster that will be removed
	void remove(int beg_index, int end_index);

	// Remove clusters that have inliers less than given threshold
	void remove(int num_of_inliers);

	// Remove point match from the cluster
	void remove(Match match);

	// Remove empty clusters
	int removeEmptyClusters();

	// clear selected cluster
	void clear(int cluster_index);

	// Sort clusters
	void sort();

	// draw clusters on given image
	cv::Mat toImage(const cv::Mat &image, bool draw_outlier = true);

	// Check if no clusters exist.
	// returns false if at least one of cluster is exist.
	bool empty() const;

	// save clusters to plane text file
	bool savePointClusters(const std::string filename);

	// save clusters and its parameters to given filename
	bool save(const std::string filename, bool use_matlab_format);

	//-------------------------------
	// Overloaded operators
	//-------------------------------
	bool operator==(const Clusters& lhs) const;

	// Direct access to each cluster
	Cluster &operator[](const int index);

	// Direct access to each cluster for read-only
	const Cluster &operator[](const int index) const;
};
