#pragma once

#include <string>
#include <vector>

#include <opencv2\opencv.hpp>

#include "types.h"

namespace IOUtils
{
	struct FilePathes
	{
		std::vector<std::string> pathCalibrations;
		std::vector<std::string> pathImages;
		std::vector<std::string> pathMatches;
	};

	struct TwoViewParamPathes
	{
		std::string dataname;
		std::string pairname;
		std::string pathCamCalib1;
		std::string pathCamCalib2;
		std::string pathImg1;
		std::string pathImg2;
		std::string pathMatches;
		std::string pathVD1;
		std::string pathVD2;
		std::string pathGTCluster;
		std::string pathExcludeMap;

		void print();
	};

	//-------------------------
	// Utilities
	//-------------------------

	// Retrieve folder name from given path
	std::string retrieveFoldername(const std::string &path);

	// Show given message and pause the program
	void showMessage(const std::string &msg);

	//-------------------------
	// Input functions
	//-------------------------

	// Load standard parameter structures from given information file.
	std::vector<FilePathes> loadStdParamStructs(const std::string &filename);

	// Load parameters for two view case that contains two elements for each parameter.
	std::vector<TwoViewParamPathes> loadTwoViewSturucture(const std::string &filename);

	// Load parameter file pathes of two view case
	TwoViewParamPathes loadTwoViewParams(std::string rootFolder);

	// Load clusters from given text file
	Clusters loadClusters(const std::string &filename);

	// Load 3x3 matrix from given file
	cv::Mat load3x3Matrix(const std::string &filename);

	// Load point correspondences from given file
	bool loadMatches(const std::string &filename, std::vector<cv::Point2f> &points1, std::vector<cv::Point2f> &points2);

	//-------------------------
	// Output functions
	//-------------------------

	// Save the given matches as a matlab script file
	void saveMatchesToMatlabScript(const std::string &filename, const std::vector<cv::Point2f> &points1,
		const std::vector<cv::Point2f> &points2);

	// Save the given models as a matlab script file
	void saveModels(const std::string &filename, const std::vector<HomographyModel> &models);
};
