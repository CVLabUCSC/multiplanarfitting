#include <opencv2\opencv.hpp>
#include <opencv2\highgui.hpp>

#include "MultiPlanarFitting.h"

#include "JLinkage\JLinkage.h"
#include "JLinkage\RandomSampler.h"

enum CLUSTERING_TYPE {
	MSH = 0,
	JLINKAGE = 1,
	TLINKAGE = 2,
	RANSAC = 3
};

enum MODEL_TYPE {
	UNCONSTRAINED = 0,
	WEAK_MANHATTAN_WORLD = 1,
	CONSTRAINED = 2
};

class Experiments {
private:
	// output clusters
	std::vector<Cluster> mClusters;

	// Accumulated rand index for each test case
	std::vector<std::vector<double>> mRandIndex;

	// The best rand index for each test case
	std::vector<std::vector<double>> mBestRandIndex;

	// Number of successful iteration for each test case
	std::vector<std::vector<int>> mNumAccumulated;

	// Data name to show logs and save output
	std::string mDataname;

	// Image to compute matches and output
	cv::Mat mImage1;
	cv::Mat mImage2;

	// Point correspondences
	std::vector<cv::Point2f> mPoints1;
	std::vector<cv::Point2f> mPoints2;

	// Base clustering to compute Rand Index
	BaseClustering mBaseClustering;

	std::vector<double> mInlierThresholds;

public:
	Experiments(const std::string dataname, const BaseClustering &baseClustering,
		const cv::Mat &image1, const cv::Mat &image2,
		const std::vector<cv::Point2f> &points1, const std::vector<cv::Point2f> &points2);
	~Experiments();

	// Test Mode Seeking on Hypergraph fitting method
	void testMSH(const HomographyEstimator &estimator, std::vector<std::vector<unsigned int>> sampleIndices, double threshold);

	// Test J-linkage clustering
	void testJLinkage(const HomographyEstimator &estimator, std::vector<std::vector<unsigned int>> sampleIndices, double threshold);

	// Test J-linkage clustering
	Clusters testJLinkage(std::vector<cv::Mat> &homographies, double threshold);

	// Test T-linkage clustering
	void testTLinkage(const HomographyEstimator &estimator, std::vector<std::vector<unsigned int>> sampleIndices, double threshold, bool useVLConsOnClustering = false);

	// Test J-linkage clustering
	Clusters testTLinkage(Clusters &clusters_input, const HomographyEstimator &estimator, double threshold, bool useVLConsOnClustering = false);

	// Test RANSAC clustering
	void testRANSAC(const HomographyEstimator &estimator, std::vector<std::vector<unsigned int>> sampleIndices, double threshold);

	// Test priority J-linkage. Priority of normal vectors should be given to normalPriorities, and each value should be between
	// arbitrary negative value to 2. The number larger 2 will be ignored. If a normal priority has negetive value, that normal will
	// be deactivated for entire process.
	void testPriorityJLinkage(HomographyEstimator &estimator, std::vector<std::vector<unsigned int>> sampleIndices,
		std::vector<int> normalPriorities, double threshold);

	// Test T-linkage clustering
	void testPriorityTLinkage(HomographyEstimator &estimator, std::vector<std::vector<unsigned int>> sampleIndices,
		std::vector<int> normalPriorities, double threshold, int numOfSamples, CLUSTERING_TYPE type);

	// Accumulate rand index for each test case
	void accumulateRandIndex(const std::string resultFoldername, const HomographyEstimator &estimator,
		CLUSTERING_TYPE type, double threshold, bool considerOutliers);

	// Print accumulated Rand Index
	void print(CLUSTERING_TYPE type);

	void print(std::vector<double> &randIndex, std::vector<int> &numAccumulated, std::vector<double> bestRandIndex, const std::string method);

	// Save rand index data as a file
	void saveBestRandIndicesAGT(std::ofstream &fout);

	// Save rand index data as a file
	void saveBestRandIndicesGT(std::ofstream &fout);

	// Save rand index data as a file
	void saveMeanRandIndicesGT(std::ofstream &fout);

	// Save rand index data as a file
	void saveMeanRandIndicesAGT(std::ofstream &fout);

	// Save clusters as list of 3D points
	bool saveClustersTo3DPoints(const std::string &filename, const HomographyEstimator &estimator);

	// Save clusters as list of 3D points
	bool saveClustersTo3DPointsMatlab(Clusters &clusters, const std::string &filename, const HomographyEstimator &estimator);

	bool saveClustersTo3DPoints(const std::string &filename, const Clusters &clusters, const HomographyEstimator &estimator);
};

void initRandomSampler(RandomSampler &randomSampler, const std::vector<cv::Point2f> &points1,
	const std::vector<cv::Point2f> &points2, int num_of_models, RS_NFSAMPLINGTYPE samplingType = NFST_UNIFORM);

std::string getAppendix(HOMOGRAPHY_TYPE type);

void printResult(std::vector<double> &randIndex, std::vector<int> &numAccumulated, const std::string method, const std::string dataname);

//------------------------------------------------
//	Global Functions
//------------------------------------------------

void myTest(std::string inputDataFilename, int testStep = 1, int numOfTestPairsPerData = 1, bool considerOutliers = true);

int main();
