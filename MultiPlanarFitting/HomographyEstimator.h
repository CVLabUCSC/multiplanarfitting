#pragma once

#include <opencv2\opencv.hpp>

#include "types.h"
#include "IOUtils.h"

enum HOMOGRAPHY_TYPE {
	HOMOGRAPHY_UNCONSTRAINED = 1,
	HOMOGRAPHY_MANHATTAN_WORLD = 2,
	HOMOGRAPHY_WEAK_MANHATTAN_WORLD = 3
};

enum PARAMETER_COMPUTATION {
	PARAMS_MIN_ERROR,
	PARAMS_MAX_INLIERS
};

class HomographyEstimator
{
private:
	// Rotation matrix from world coordinate system to camera 1 coordinate system
	cv::Mat R1;

	// Rotation matrix from world coordinate system to camera 2 coordinate system
	cv::Mat R2;

	// Pre-computed rotation matrix from Camera 1 to Camera 2
	cv::Mat R;

	// Camera 1 calibration matrix
	cv::Mat K1;

	// Camera 2 calibration matrix
	cv::Mat K2;

	// Active normals that will be used to compute homographies
	std::vector<bool> mActiveNormals;

	// Three orthogonal plane normal vectors in Manhattan-world
	std::vector<cv::Mat> mNormals;

	// Inlier threshold for each normal
	std::vector<double> mInlierThreshold;

	// Compute constrained homographies with given matching points and three different orthogonal normal vectors.
	// It will return one of homographies which minimizes the sum of reprojection error over all input point matches.
	cv::Mat computeMWHomographyMinError(const std::vector<cv::Point2f> &points1,
		const std::vector<cv::Point2f> &points2, int &normal_index, cv::Mat &t_hat = cv::Mat()) const;

	// Compute constrained homographies and corresponding parameters with given matching points and three different
	// orthogonal normal vectors.
	void computeMWHomographyMaxInliers(Cluster &cluster, bool useMultipleThresholds) const;

	// Compute constrained homography using given matching points with given index of normal vectors.
	cv::Mat computeMWHomography(const std::vector<cv::Point2f> &points1, const std::vector<cv::Point2f> &points2,
		const unsigned int normal_index, cv::Mat &t_hat = cv::Mat()) const;

	// Compute constrained homography with weak Manhattan-world assumption.
	// The method comes from the following paper:
	// Saurer, F. Fraundorfer, and M. Pollefeys. Homography based visual odometry with known vertical direction
	// and weak manhattan world assumption. In Proc. IEEE/IROS Workshop on Visual Control of Mobile Robots, 2012.
	cv::Mat computeWMWHomographyMinError(const std::vector<cv::Point2f> &points1, const std::vector<cv::Point2f> &points2) const;

	// Compute constrained homography with weak Manhattan-world assumption using 3 points correspondences algorithm.
	cv::Mat computeWMWHomography(const std::vector<cv::Point2f> &points1, const std::vector<cv::Point2f> &points2) const;

	// Compute constrained homography with weak Manhattan-world assumption.
	// The method comes from the following paper:
	// Saurer, F. Fraundorfer, and M. Pollefeys. Homography based visual odometry with known vertical direction
	// and weak manhattan world assumption. In Proc. IEEE/IROS Workshop on Visual Control of Mobile Robots, 2012.
	cv::Mat computeWMWHomography(const std::vector<cv::Point2f> &points1, const std::vector<cv::Point2f> &points2, cv::Mat normal) const;

	// Load calibration matrices from given files
	// @params filename1 File path for first calibration matrix
	// @params filename2 File path for second calibration matrix
	// @return true if succeed
	bool loadCalibrations(const std::string &filename1, const std::string &filename2);

	// Load pre-esticv::Mated rotation cv::Matrix from file
	bool loadRotations(const std::string &filename);

	// Load pre-esticv::Mated rotation cv::Matrices from file
	bool loadRotations(const std::string &filename1, const std::string &filename2);

protected:

	// This variable designates a model type. The value of this variable would be one of follow:
	// HOMOGRAPHY_UNCONSTRAINED, HOMOGRAPHY_MANHATTAN_WORLD, HOMOGRAPHY_WEAK_MANHATTAN_WORLD
	HOMOGRAPHY_TYPE m_modelType;

	// Reprojection threshold
	double reprojection_thr;

public:
	HomographyEstimator(void);
	HomographyEstimator(double reprojection_threshold);
	HomographyEstimator(const HOMOGRAPHY_TYPE model_type);
	HomographyEstimator(const HOMOGRAPHY_TYPE model_type, const cv::Mat &K, const cv::Mat R1, const cv::Mat R2,
		const std::vector<cv::Mat> normals);
	HomographyEstimator(const HOMOGRAPHY_TYPE model_type, const cv::Mat &K1, const cv::Mat &K2, const cv::Mat R1,
		const cv::Mat R2, const std::vector<cv::Mat> normals);

	~HomographyEstimator(void);

	// Get the minimum number of required samples to construct a homography
	int getMinimumSampleSet() const;

	// Get homography type
	HOMOGRAPHY_TYPE getModelType() const;

	// Set homography type
	void setModelType(HOMOGRAPHY_TYPE model_type);

	// Set homography parameters
	// arguments:
	//	K		: Camera calibration matrix (assumed that K of both cameras are the same
	//	R1		: Rotation matrix from world coordinate system to camera 1 coordinate system
	//	R2		: Rotation matrix from world coordinate system to camera 2 coordinate system
	//	normal	: Three orthogonal plane normal vectors in Manhattan-world
	void setParameters(const cv::Mat &K, const cv::Mat R1, const cv::Mat R2, const std::vector<cv::Mat> normals);
	
	// Set homography parameters
	// arguments:
	//	K1		: Camera 1 calibration matrix
	//	K2		: Camera 2 calibration matrix
	//	R1		: Rotation matrix from world coordinate system to camera 1 coordinate system
	//	R2		: Rotation matrix from world coordinate system to camera 2 coordinate system
	//	normal	: Three orthogonal plane normal vectors in Manhattan-world
	void setParameters(const cv::Mat &K1, const cv::Mat &K2, const cv::Mat R1, const cv::Mat R2, const std::vector<cv::Mat> normals);

	// Compute homography
	cv::Mat getHomography(const std::vector<cv::Point2f> &points1, const std::vector<cv::Point2f> &points2, int normal_index = -1) const;

	// Compute homography and parameters
	cv::Mat getHomography(const std::vector<cv::Point2f> &points1, const std::vector<cv::Point2f> &points2, int &normal_index, cv::Mat &t);

	// Compute weak Manhattan world homgraphies using 2.5 points algorithm.
	// To avoid underestimated estimation, this function actually uses at least 3 points.
	cv::Mat findWMWHomographies(const std::vector<cv::Point2f> &points1, const std::vector<cv::Point2f> &points2) const;

	// Compute weak Manhattan world homgraphies using 2.5 points algorithm.
	void findWMWHomographies(const std::vector<cv::Point2f> &points1, const std::vector<cv::Point2f> &points2,
		std::vector<HomographyModel> &models) const;

	// Compute homographies constrained on pre-computed rotation and given normal vector.
	cv::Mat findMWHomography(const std::vector<cv::Point2f> &points1,
		const std::vector<cv::Point2f> &points2, const int normal_index) const;
	
	// Compute homographies constrained on pre-computed rotation and normal vectors.
	// The result models will be added to the 'models'.
	void findMWHomographies(const std::vector<cv::Point2f> &points1,
		const std::vector<cv::Point2f> &points2, std::vector<cv::Mat> &models) const;

	// Compute homographies constrained on pre-computed rotation and normal vectors.
	// The result models will be added to the 'models'.
	void findMWHomographies(const std::vector<cv::Point2f> &points1,
		const std::vector<cv::Point2f> &points2, std::vector<HomographyModel> &models) const;

	// Compute translation and plane normal vector of the plane that contains the given points.
	// This function assumes that the plane normal is one of the three Manhattan-world plane normal vector
	void computeParameters(Cluster &cluster, int type, bool useMultipleThresholds) const;

	// Compute translation and plane normal vector of the plane that contains the given points.
	// This function assumes that the plane normal is one of the three Manhattan-world plane normal vector
	void computeParameters(std::vector<Cluster> &clusters, int type = PARAMS_MIN_ERROR, bool useMultipleThresholds = false) const;

	// Compute translation and plane normal vector of the plane that contains the given points.
	// This function assumes that the plane normal is one of the three Manhattan-world plane normal vector
	void computeParameters(Clusters &clusters, int type = PARAMS_MIN_ERROR, bool useMultipleThresholds = false) const;

	// Compute translation and plane normal vector of the plane that contains the given points.
	// This function assumes that the plane normal is one of the three Manhattan-world plane normal vector
	// arguments:
	// points1	: points from the first image
	// points2	: points from the second image
	// t_hat	: (output) computed translation vector
	// plane_normal: (output) computed plane normal vector
	// returns constrained homography
	cv::Mat computeParameters(const std::vector<cv::Point2f> &points1, const std::vector<cv::Point2f> &points2,
		cv::Mat &t_hat, cv::Mat &plane_normal) const;

	// Compute essential matrix with given cluster
	cv::Mat computeEssentialMatrix(Cluster& cluster) const;

	// Compute essential matrix with given translation vector
	cv::Mat computeEssentialMatrix(const cv::Mat &translation) const;

	// Compute fundamental matrix with given cluster
	cv::Mat computeFundamentalMatrix(Cluster& cluster);

	// Get vanishing line of given normal. The normal should be one of three
	// stored normals. The returning vanishing line is for the first image.
	cv::Mat getVanishingLine(const int normalIndex) const;

	// Get vanishing line of given normal. The normal should be one of three
	// stored normals. The returning vanishing line is for the first image.
	cv::Mat getVanishingLine(const cv::Mat& normal) const;

	// Get vanishing lines. The returning vanishing lines are for the first image.
	cv::Mat getVanishingLines() const;

	// Compute 3D points using given normal and distance
	std::vector<cv::Point3f> compute3DPoints(const std::vector<cv::Point2f> &points, const cv::Mat &normal, const double d) const;

	// Compute 3D points using given normal and distance
	cv::Point3f compute3DPoint(const cv::Point2f &point, const cv::Mat &normal, const double d) const;

	// Compute depth of given point on the plane
	double computeDepth(const double x, const double y, const cv::Mat &normal, const float d) const;

	// Get normal index
	int getNormalIndex(char dir) const;

	// Activate a designated normal
	void activateNormal(int ind);

	// Direction should be either 'x', 'y', or 'z'.
	void activateNormal(char dir);

	// Activate all of normals
	void activateAllNormals();

	// Deactivate a designated normal. Should be used in special perpose.
	void deactivateNormal(int ind);

	// Direction should be either 'x', 'y', or 'z'.
	void deactivateNormal(char dir);

	// Deactivate all of normals
	void deactivateAllNormals();

	// Activate/deactivated designated normals
	void setNormalActivation(std::vector<bool> activation);

	// Get the reprojection threshold
	double getThreshold() const;

	// Get the reprojection of known index
	double getThreshold(int normalIndex) const;

	// Direction should be either 'x', 'y', or 'z'.
	void setThreshold(char dir, double threshold);

	// Order of threshold must be 'x', 'y', and 'z'.
	void setThresholds(std::vector<double> thresholds);

	// Print normal vectors
	void printNormals() const;

	// Load rotations and a calibration matrix from given file path
	bool loadParameters(const IOUtils::TwoViewParamPathes &twoViewParamPathes);

	// Save parameters as a Matlab script file
	bool save(const std::string filename);

	void drawVanishingLines(cv::Mat image1, cv::Mat image2, std::string filename1, std::string filename2) const;
};
