﻿#include "utility.h"

#include <map>

using cv::Point;
using cv::Point2f;
using cv::Mat;
using cv::Scalar;
using std::string;
using std::vector;

using std::pow;

double computeCosineSimilarity(const Mat &vec1, const Mat &vec2)
{
	Mat vec1_normalized, vec2_normalized;
	
	cv::normalize(vec1, vec1_normalized);
	cv::normalize(vec2, vec2_normalized);

	return vec1_normalized.dot(vec2_normalized);
}

double computeLineDistance(const cv::Mat &line, const Point2f point)
{
	return line.at<double>(0) * (double)point.x + line.at<double>(1) * (double)point.y + line.at<double>(2);
}

Point2f computeCentroid(const vector<Point2f> &points)
{
	Point2f centroid(0.f, 0.f);

	for (unsigned int pi = 0; pi < points.size(); pi++) {
		centroid.x += points[pi].x;
		centroid.y += points[pi].y;
	}

	centroid.x /= (float)points.size();
	centroid.y /= (float)points.size();

	return centroid;
}

vector<double> solveDeficientRankSystem(Mat Vy, Mat v)
{
	vector<double> solutions;
	double temp;

	double vy1 = Vy.at<double>(0);
	double vy2 = Vy.at<double>(1);
	double vy3 = Vy.at<double>(2);
	double vy4 = Vy.at<double>(3);
	double vy5 = Vy.at<double>(4);
	double vy6 = Vy.at<double>(5);

	double v1 = v.at<double>(0);
	double v2 = v.at<double>(1);
	double v3 = v.at<double>(2);
	double v4 = v.at<double>(3);
	double v5 = v.at<double>(4);
	double v6 = v.at<double>(5);

	double a = -pow(v1, 2) * pow(v4, 2) + 2 * v1*v2*v3*v4 - pow(v2, 2) * pow(v3, 2) - pow(v3, 2) * pow(v6, 2) + 2 * v3*v4*v5*v6 - pow(v4, 2) * pow(v5, 2);
	double b = -2 * vy4*pow(v1, 2) * v4 + 2 * vy4*v1*v2*v3 + 2 * vy3*v1*v2*v4 + 2 * vy2*v1*v3*v4 - 2 * vy1*v1*pow(v4, 2) - 2 * vy3*pow(v2, 2) * v3 - 2 * vy2*v2*pow(v3, 2) + 2 * vy1*v2*v3*v4 - 2 * vy6*pow(v3, 2) * v6 + 2 * vy6*v3*v4*v5 + 2 * vy5*v3*v4*v6 + 2 * vy4*v3*v5*v6 - 2 * vy3*v3*pow(v6, 2) - 2 * vy5*pow(v4, 2) * v5 - 2 * vy4*v4*pow(v5, 2) + 2 * vy3*v4*v5*v6;
	double c = -pow(v1, 2) * pow(vy4, 2) + 2 * v1*v2*vy3*vy4 + 2 * v1*v3*vy2*vy4 - 4 * v1*v4*vy1*vy4 + 2 * v1*v4*vy2*vy3 - pow(v2, 2) * pow(vy3, 2) + 2 * v2*v3*vy1*vy4 - 4 * v2*v3*vy2*vy3 + 2 * v2*v4*vy1*vy3 - pow(v3, 2) * pow(vy2, 2) - pow(v3, 2) * pow(vy6, 2) + pow(v3, 2) + 2 * v3*v4*vy1*vy2 + 2 * v3*v4*vy5*vy6 + 2 * v3*v5*vy4*vy6 - 4 * v3*v6*vy3*vy6 + 2 * v3*v6*vy4*vy5 - pow(v4, 2) * pow(vy1, 2) - pow(v4, 2) * pow(vy5, 2) + pow(v4, 2) + 2 * v4*v5*vy3*vy6 - 4 * v4*v5*vy4*vy5 + 2 * v4*v6*vy3*vy5 - pow(v5, 2) * pow(vy4, 2) + 2 * v5*v6*vy3*vy4 - pow(v6, 2) * pow(vy3, 2);
	double d = -2 * v4*pow(vy1, 2) * vy4 + 2 * v4*vy1*vy2*vy3 + 2 * v3*vy1*vy2*vy4 + 2 * v2*vy1*vy3*vy4 - 2 * v1*vy1*pow(vy4, 2) - 2 * v3*pow(vy2, 2) * vy3 - 2 * v2*vy2*pow(vy3, 2) + 2 * v1*vy2*vy3*vy4 - 2 * v6*pow(vy3, 2) * vy6 + 2 * v6*vy3*vy4*vy5 + 2 * v5*vy3*vy4*vy6 + 2 * v4*vy3*vy5*vy6 - 2 * v3*vy3*pow(vy6, 2) + 2 * v3*vy3 - 2 * v5*pow(vy4, 2) * vy5 - 2 * v4*vy4*pow(vy5, 2) + 2 * v3*vy4*vy5*vy6 + 2 * v4*vy4;
	double e = -pow(vy1, 2) * pow(vy4, 2) + 2 * vy1*vy2*vy3*vy4 - pow(vy2, 2) * pow(vy3, 2) - pow(vy3, 2) * pow(vy6, 2) + pow(vy3, 2) + 2 * vy3*vy4*vy5*vy6 - pow(vy4, 2) * pow(vy5, 2) + pow(vy4, 2);

	double p1 = 2*pow(c, 3) - 9 *b*c*d + 27*a*pow(d, 2) + 27*pow(b, 2)*e - 72*a*c*e;

	temp = -4 * pow(pow(c, 2) - 3 * b*d + 12 * a*e, 3) + pow(p1, 2);
	if (temp < 0) {
		return solutions;
	}
	double p2 = p1 + sqrt(temp);

	if (p2 < 0) {
		return solutions;
	}
	temp = pow(p2 / 2.0, 1.0 / 3.0);
	double p3 = (c * c - 3 * b * d + 12 * a * e) / (3 * a * temp) + temp / (3 * a);

	temp = pow(b, 2) / (4 * pow(a, 2)) - (2 * c) / (3 * a) + p3;
	if (temp < 0) {
		return solutions;
	}
	double p4 = sqrt(temp);

	double p5 = pow(b, 2) / (a * pow(a, 2)) - (4 * c) / (3 * a) - p3;
	double p6 = (-pow(b, 3) / pow(a, 3) + (4 * b*c) / pow(a, 2) - (8 * d) / a) / (4 * p4);

	if (p5 - p6 >= 0) {
		solutions.push_back(-b / (4 * a) - p4 / 2 - sqrt(p5 - p6) / 2);
		solutions.push_back(-b / (4 * a) - p4 / 2 + sqrt(p5 - p6) / 2);
	}

	if (p5 + p6 >= 0) {
		solutions.push_back(-b / (4 * a) + p4 / 2 - sqrt(p5 - p6) / 2);
		solutions.push_back(-b / (4 * a) + p4 / 2 + sqrt(p5 - p6) / 2);
	}

	return solutions;
}

//-----------------------------
// Operations with Vanishing Lines
//-----------------------------

Mat computeVanishingLines(const Mat &K, const Mat &VD)
{
	Mat VP = K * VD;
	Mat VL(3, 3, CV_64F);

	VP = pointsFromHomogeneous(VP);
	
	VP.col(1).cross(VP.col(2)).copyTo(VL.col(0));
	VP.col(0).cross(VP.col(2)).copyTo(VL.col(1));
	VP.col(0).cross(VP.col(1)).copyTo(VL.col(2));

	return VL;
}

void separatePointsVL(const vector<Point2f> &points_in, Mat VL,
	vector<Point2f> &points1_out, vector<Point2f> &points2_out)
{
	double a = VL.at<double>(0);
	double b = VL.at<double>(1);
	double c = VL.at<double>(2);

	points1_out.clear();
	points2_out.clear();

	for (unsigned int i = 0; i < points_in.size(); i++) {
		double val = points_in[i].x * a + points_in[i].y * b + c;
		if (val < 0.0) {
			points1_out.push_back(points_in[i]);
		}
		else if (val > 0.0) {
			points2_out.push_back(points_in[i]);
		}
		else {
			points1_out.push_back(points_in[i]);
			points2_out.push_back(points_in[i]);
		}
	}
}

void separateMatchesVL(const vector<Point2f> &points1_in, const vector<Point2f> &points2_in,
	const Mat &VL, vector<Match> &pointsNegGroup1_out, vector<Match> &pointsPosGroup2_out)
{
	double a = VL.at<double>(0);
	double b = VL.at<double>(1);
	double c = VL.at<double>(2);

	pointsNegGroup1_out.clear();
	pointsPosGroup2_out.clear();

	for (unsigned int i = 0; i < points1_in.size(); i++) {
		double val = points1_in[i].x * a + points1_in[i].y * b + c;
		if (val < 0.0) {
			pointsNegGroup1_out.push_back(Match(points1_in[i], points2_in[i]));
		}
		else if (val > 0.0) {
			pointsPosGroup2_out.push_back(Match(points1_in[i], points2_in[i]));
		}
		else {
			pointsNegGroup1_out.push_back(Match(points1_in[i], points2_in[i]));
			pointsPosGroup2_out.push_back(Match(points1_in[i], points2_in[i]));
		}
	}
}

// Seperate point matches into two groups using given vanishing line VL, then
// pick the larger group and save them to points1_out and points2_out.
void pickLargerGroupVL(const vector<Point2f> &points1_in, const vector<Point2f> &points2_in,
	const Mat &VL, vector<Point2f> &points1_out, vector<Point2f> &points2_out)
{
	vector<Match> points_group1, points_group2;

	separateMatchesVL(points1_in, points2_in, VL, points_group1, points_group2);

	if (points_group1.size() > points_group2.size()) {
		for (unsigned int i = 0; i < points_group1.size(); i++) {
			points1_out.push_back(points_group1[i].first);
			points2_out.push_back(points_group1[i].second);
		}
	}
	else {
		for (unsigned int i = 0; i < points_group2.size(); i++) {
			points1_out.push_back(points_group2[i].first);
			points2_out.push_back(points_group2[i].second);
		}
	}
}

// Seperate point matches into two groups using given vanishing lines VL1 and VL2,
// then pick the larger group and save them to points1_out and points2_out.
void pickLargerGroupVL(const vector<Point2f> &points1_in, const vector<Point2f> &points2_in,
	Mat VL1, Mat VL2, vector<Point2f> &points1_out, vector<Point2f> &points2_out)
{
}

//-----------------------------
// Homogeneous System Utilities
//-----------------------------

Mat pointsFromHomogeneous(const Mat &P)
{
	Mat P_normalized = P.clone();

	for (int i = 0; i < P.cols; i++) {
		P_normalized.at<double>(0, i) /= P_normalized.at<double>(2, i);
		P_normalized.at<double>(1, i) /= P_normalized.at<double>(2, i);
		P_normalized.at<double>(2, i) /= P_normalized.at<double>(2, i);
	}

	return P_normalized;
}

//-----------------------------
// Debug Utilities
//-----------------------------

string type2str(int type)
{
	string r;

	uchar depth = type & CV_MAT_DEPTH_MASK;
	uchar chans = 1 + (type >> CV_CN_SHIFT);

	switch (depth) {
	case CV_8U:  r = "8U"; break;
	case CV_8S:  r = "8S"; break;
	case CV_16U: r = "16U"; break;
	case CV_16S: r = "16S"; break;
	case CV_32S: r = "32S"; break;
	case CV_32F: r = "32F"; break;
	case CV_64F: r = "64F"; break;
	default:     r = "User"; break;
	}

	r += "C";
	r += (chans + '0');

	return r;
}

// Draw lines on given image. Each column of L represents a line vector.
Mat drawLinesOnImage(const cv::Mat &image, const cv::Mat &L, int thickness, bool convertToGray)
{
	Mat result;
	vector<Scalar> colors = generateRandomColors(L.cols);

	if (convertToGray) {
		cv::cvtColor(image, result, CV_BGR2GRAY);
		cv::cvtColor(result, result, CV_GRAY2BGR);
	}
	else {
		result = image.clone();
	}

	for (int i = 0; i < L.cols; i++)
	{
		Point l1, l2;

		l1.x = 0;
		l1.y = -L.at<double>(2, i) / L.at<double>(1, i);

		l2.x = image.cols;
		l2.y = -(L.at<double>(0, i) * (double)l2.x + L.at<double>(2, i)) / L.at<double>(1, i);

		if (l1.y >= 0 && l1.y <= image.rows) {
			cv::line(result, l1, l2, colors[i], thickness);
		}
		else {
			l1.y = 0;
			l1.x = -L.at<double>(2, i) / L.at<double>(0, i);

			l2.y = image.rows;
			l2.x = -(L.at<double>(1, i) * (double)l2.y + L.at<double>(2, i)) / L.at<double>(0, i);

			cv::line(result, l1, l2, colors[i], thickness);
		}
	}

	return result;
}

vector<Scalar> generateRandomColors(int numOfColors, uint64 seed)
{
	vector<Scalar> colors;
	cv::RNG rng(0xFFFFFFFF);

	for (size_t ci = 0; ci < numOfColors; ci++)
	{
		int icolor = (unsigned)rng;
		colors.push_back(Scalar(icolor & 255, (icolor >> 8) & 255, (icolor >> 16) & 255));
	}

	return colors;
}