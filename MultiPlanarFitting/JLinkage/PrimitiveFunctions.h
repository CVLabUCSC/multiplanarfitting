#ifndef PRIMITIVEFUNCTIONS_H
#define PRIMITIVEFUNCTIONS_H

#include <cv.h>
#include <calib3d\calib3d.hpp>

cv::Mat GetFunction_Plane(const std::vector<sPt *> &nDataPtXMss, const std::vector<unsigned int>  &nSelectedPts)
{
	vector<cv::Point2f> points1; points1.resize(4);
	vector<cv::Point2f> points2; points2.resize(4);
    cv::Mat H12;

	// Points setting
	points1[0].x = nDataPtXMss[nSelectedPts[0]]->mCoord1.x;	points1[0].y = nDataPtXMss[nSelectedPts[0]]->mCoord1.y;
	points2[0].x = nDataPtXMss[nSelectedPts[0]]->mCoord2.x;	points2[0].y = nDataPtXMss[nSelectedPts[0]]->mCoord2.y;

	points1[1].x = nDataPtXMss[nSelectedPts[1]]->mCoord1.x;	points1[1].y = nDataPtXMss[nSelectedPts[1]]->mCoord1.y;
	points2[1].x = nDataPtXMss[nSelectedPts[1]]->mCoord2.x;	points2[1].y = nDataPtXMss[nSelectedPts[1]]->mCoord2.y;

	points1[2].x = nDataPtXMss[nSelectedPts[2]]->mCoord1.x;	points1[2].y = nDataPtXMss[nSelectedPts[2]]->mCoord1.y;
	points2[2].x = nDataPtXMss[nSelectedPts[2]]->mCoord2.x;	points2[2].y = nDataPtXMss[nSelectedPts[2]]->mCoord2.y;

	points1[3].x = nDataPtXMss[nSelectedPts[3]]->mCoord1.x;	points1[3].y = nDataPtXMss[nSelectedPts[3]]->mCoord1.y;
	points2[3].x = nDataPtXMss[nSelectedPts[3]]->mCoord2.x;	points2[3].y = nDataPtXMss[nSelectedPts[3]]->mCoord2.y;

	H12 = cv::findHomography( cv::Mat(points1), cv::Mat(points2), 0 );
	
	return H12;
}
	
float DistanceFunction_Plane(const std::vector<float> &nModel, const std::vector<float>  &nDataPt)
{
	return fabs(nModel[0] * nDataPt[0] + nModel[1] * nDataPt[1] + nModel[2] * nDataPt[2] + nModel[3]);
}

#endif