#include "main.h"

#include "HypergraphModeSeeking.h"
#include "TLinkage.h"
#include "utility.h"

#include <fstream>

#include <opencv2\xfeatures2d\nonfree.hpp>

using std::cout;
using std::endl;
using std::setprecision;
using std::fixed;
using std::vector;
using std::list;
using std::string;
using std::stringstream;
using std::ostringstream;
using std::ofstream;
using std::fstream;

using cv::Point3f;
using cv::Point2f;
using cv::Mat;
using cv::String;
using cv::KeyPoint;
using cv::Ptr;
using cv::xfeatures2d::SIFT;
using cv::imread;
using cv::imwrite;

using HypergraphModeSeeking::MSHClustering;

Experiments::Experiments(const string dataname, const BaseClustering &baseClustering, const Mat &image1, const Mat &image2,
	const vector<Point2f> &points1, const vector<Point2f> &points2) : mDataname(dataname), mImage1(image1),
	mImage2(image2), mPoints1(points1), mPoints2(points2), mBaseClustering(baseClustering),
	mRandIndex(6, vector<double>(8, 0.0)), mBestRandIndex(6, vector<double>(8, 0.0)), mNumAccumulated(6, vector<int>(8, 0)),
	mInlierThresholds(3)
{
	mInlierThresholds[0] = 10.;
	mInlierThresholds[1] = 10.;
	mInlierThresholds[2] = 2.5;
}

Experiments::~Experiments()
{

}

void Experiments::testMSH(const HomographyEstimator &estimator, vector<vector<unsigned int>> sampleIndices, double threshold)
{
	// Initailize
	MSHClustering modelFitting(mImage1, mImage2, mPoints1, mPoints2);

	// Generate models
	vector<HomographyModel> models = ClusterUtils::generateModels(estimator, sampleIndices, mPoints1, mPoints2);

	// Do clustering
	mClusters.clear();
	modelFitting.doClustering(mClusters, models);
}

void Experiments::testJLinkage(const HomographyEstimator &estimator, vector<vector<unsigned int>> sampleIndices, double threshold)
{
	int KdTreeRange = 10;
	unsigned int minimumSampleSet = estimator.getMinimumSampleSet();

	estimator.printNormals();

	// Generate models
	vector<HomographyModel> models = ClusterUtils::generateModels(estimator, sampleIndices, mPoints1, mPoints2);
	//vector<HomographyModel> models = ClusterUtils::generateConstrainedModels(estimator, sampleIndices, mPoints1, mPoints2, 2);
	
	// Initialize the clustering
	JLinkage jlinkage(threshold, models.size(), true, 2, KdTreeRange);

	for (unsigned int i = 0; i < models.size(); i++) {
		jlinkage.AddModel(new Mat(models[i].Homography));
	}

	for (int i = 0; i < (int)mPoints1.size(); i++) {
		jlinkage.AddPoint(mPoints1[i], mPoints2[i]);
	}

	// Do the J-linkage clustering
	list<sClLnk *> tempClustersList = jlinkage.DoJLClusterization();

	// Copy Result Data
	tempClustersList.sort(ClusterSort());
	mClusters.clear();
	for (list<sClLnk *>::const_iterator citer = tempClustersList.begin(); citer != tempClustersList.end(); citer++)
	{
		if ((*citer)->mBelongingPts.size() <= minimumSampleSet)
			break;

		mClusters.push_back(Cluster());

		for (list<sPtLnk *>::const_iterator piter = (*citer)->mBelongingPts.begin(); piter != (*citer)->mBelongingPts.end(); piter++) {
			mClusters.back().points1.push_back((*piter)->mCoord1);
			mClusters.back().points2.push_back((*piter)->mCoord2);
		}
	}

	mClusters = ClusterUtils::separateClusters(mClusters, estimator);

	std::sort(mClusters.begin(), mClusters.end(), ClusterSortByPoints);
}

// Test J-linkage clustering
Clusters Experiments::testJLinkage(std::vector<cv::Mat> &homographies, double threshold)
{
	unsigned int nPtDimension = 2;
	int mKDTreeRange = 10;
	Clusters clusters(mPoints1, mPoints2);

	// Compute the jLinkage Clusterization
	// Initialize the clustering
	JLinkage jlinkage(threshold, homographies.size(), true, nPtDimension, mKDTreeRange);

	for (int i = 0; i < homographies.size(); i++) {
		jlinkage.AddModel(&homographies[i]);
	}

	for (int i = 0; i < (int)mPoints1.size(); i++) {
		jlinkage.AddPoint(mPoints1[i], mPoints2[i]);
	}

	list<sClLnk *> tempClustersList = jlinkage.DoJLClusterization();

	tempClustersList.sort(ClusterSort());

	// Copy Result Data
	int clusterIndex = 0;
	for (list<sClLnk *>::const_iterator citer = tempClustersList.begin(); citer != tempClustersList.end(); citer++)
	{
		if ((*citer)->mBelongingPts.size() <= 4)
			break;

		for (list<sPtLnk *>::const_iterator piter = (*citer)->mBelongingPts.begin(); piter != (*citer)->mBelongingPts.end(); piter++)
			clusters.addPoint((*piter)->mCoord1, (*piter)->mCoord2, clusterIndex);

		clusterIndex++;
	}

	return clusters;
}

void Experiments::testTLinkage(const HomographyEstimator &estimator, vector<vector<unsigned int>> sampleIndices,
	double threshold, bool useVLConsOnClustering)
{
	int KdTreeRange = 10;
	unsigned int minimumSampleSet = estimator.getMinimumSampleSet();

	estimator.printNormals();

	// Generate models
	vector<HomographyModel> models = ClusterUtils::generateModels(estimator, sampleIndices, mPoints1, mPoints2);
	//vector<HomographyModel> models = ClusterUtils::generateConstrainedModels(estimator, sampleIndices, mPoints1, mPoints2, 2);
	
	// Initialize the clustering
	TLinkageClustering::TLinkage tlinkage(threshold, models.size(), true, 2, KdTreeRange);

	// Activate multi threshold
	if (estimator.getModelType() == HOMOGRAPHY_MANHATTAN_WORLD) {
		tlinkage.setMultiInliersThreshold(true);
	}

	for (unsigned int i = 0; i < models.size(); i++) {
		TLinkageClustering::Homography homography;

		homography.homography = models[i].Homography.clone();
		homography.normalIndex = models[i].normal_index;
		homography.inlierThreshold = models[i].scale;
		if (useVLConsOnClustering) {
			homography.isPositiveSide = models[i].tag >= 0;
		}

		tlinkage.AddModel(homography);
	}

	Mat VL = estimator.getVanishingLines();

	for (int i = 0; i < (int)mPoints1.size(); i++) {
		unsigned char indicator = 0;
		if (useVLConsOnClustering) {
			indicator += computeLineDistance(VL.col(0), mPoints1[i]) >= 0 ? 1 : 0;
			indicator += computeLineDistance(VL.col(1), mPoints1[i]) >= 0 ? 2 : 0;
			indicator += computeLineDistance(VL.col(2), mPoints1[i]) >= 0 ? 4 : 0;
		}
		tlinkage.AddPoint(mPoints1[i], mPoints2[i], indicator);
	}

	/*
	Mat img = tlinkage.drawPointTestVanishingLine(mImage1, 0);
	imwrite("VPTest1.png", drawLinesOnImage(img, VL.col(0), 2, false));
	img = tlinkage.drawPointTestVanishingLine(mImage1, 1);
	imwrite("VPTest2.png", drawLinesOnImage(img, VL.col(1), 2, false));
	img = tlinkage.drawPointTestVanishingLine(mImage1, 2);
	imwrite("VPTest3.png", drawLinesOnImage(img, VL.col(2), 2, false));
	*/

	// Do the J-linkage clustering
	list<TLinkageClustering::sClLnk *> tempClustersList = tlinkage.DoTLClusterization();

	// Copy Result Data
	tempClustersList.sort(TLinkageClustering::ClusterSort());
	mClusters.clear();
	for (list<TLinkageClustering::sClLnk *>::const_iterator citer = tempClustersList.begin(); citer != tempClustersList.end(); citer++)
	{
		if ((*citer)->mBelongingPts.size() <= minimumSampleSet)
			break;

		mClusters.push_back(Cluster());

		vector<int> accumulator(3, 0);
		for (int i = 0; i < models.size(); i++) {
			if ((*citer)->mPreferenceFunction[i] > 0) {
				accumulator[models[i].normal_index]++;
			}
		}
		
		if (accumulator[0] > accumulator[1]) {
			if (accumulator[0] > accumulator[2]) {
				mClusters.back().normal_index = 0;
				//cout << "normal: 0" << endl;
			}
			else {
				mClusters.back().normal_index = 2;
				//cout << "normal: 2" << endl;
			}
		}
		else {
			if (accumulator[1] > accumulator[2]) {
				mClusters.back().normal_index = 1;
				//cout << "normal: 1" << endl;
			}
			else {
				mClusters.back().normal_index = 2;
				//cout << "normal: 2" << endl;
			}
		}
		
		for (list<TLinkageClustering::sPtLnk *>::const_iterator piter = (*citer)->mBelongingPts.begin(); piter != (*citer)->mBelongingPts.end(); piter++) {
			mClusters.back().points1.push_back((*piter)->mCoord1);
			mClusters.back().points2.push_back((*piter)->mCoord2);
		}
	}

	mClusters = ClusterUtils::separateClusters(mClusters, estimator);

	std::sort(mClusters.begin(), mClusters.end(), ClusterSortByPoints);
}

// Test J-linkage clustering
Clusters Experiments::testTLinkage(Clusters &clusters_input, const HomographyEstimator &estimator, double threshold, bool useVLConsOnClustering)
{
	unsigned int nPtDimension = 2;
	int mKDTreeRange = 10;
	Clusters clusters_output(mPoints1, mPoints2);
	Mat VL = estimator.getVanishingLines();

	// Compute the TLinkage Clusterization
	// Initialize the clustering
	TLinkageClustering::TLinkage tlinkage(threshold, clusters_input.size(), true, nPtDimension, mKDTreeRange);

	if (estimator.getModelType() == HOMOGRAPHY_MANHATTAN_WORLD) {
		for (unsigned int ci = 0; ci < clusters_input.size(); ci++) {
			if (clusters_input[ci].normal_index < 0) {
				estimator.computeParameters(clusters_input[ci], PARAMS_MAX_INLIERS, true);
			}
			else {
				clusters_input[ci].Homography =
					estimator.getHomography(clusters_input[ci].points1, clusters_input[ci].points2, clusters_input[ci].normal_index);
			}

			TLinkageClustering::Homography homography;

			homography.homography = clusters_input[ci].Homography.clone();
			homography.normalIndex = clusters_input[ci].normal_index;
			homography.inlierThreshold = threshold;

			if (useVLConsOnClustering) {
				homography.isPositiveSide =
					round(computeLineDistance(VL.col(clusters_input[ci].normal_index), computeCentroid(clusters_input[ci].points1))) >= 0;
			}

			tlinkage.AddModel(homography);
		}
	}
	else {
		ClusterUtils::computeHomographies(clusters_input, 0);
		for (unsigned int i = 0; i < clusters_input.size(); i++) {
			TLinkageClustering::Homography homography;

			homography.homography = clusters_input[i].Homography;
			homography.inlierThreshold = threshold;

			tlinkage.AddModel(homography);
		}
	}

	for (int i = 0; i < (int)mPoints1.size(); i++) {
		unsigned char indicator = 0;
		if (useVLConsOnClustering) {
			indicator += computeLineDistance(VL.col(0), mPoints1[i]) >= 0 ? 1 : 0;
			indicator += computeLineDistance(VL.col(1), mPoints1[i]) >= 0 ? 2 : 0;
			indicator += computeLineDistance(VL.col(2), mPoints1[i]) >= 0 ? 4 : 0;
		}
		tlinkage.AddPoint(mPoints1[i], mPoints2[i], indicator);
	}

	list<TLinkageClustering::sClLnk *> tempClustersList = tlinkage.DoTLClusterization();

	// Copy Result Data
	int clusterIndex = 0;
	for (list<TLinkageClustering::sClLnk *>::const_iterator citer = tempClustersList.begin(); citer != tempClustersList.end(); citer++)
	{
		if ((*citer)->mBelongingPts.size() <= 4)
			continue;

		for (list<TLinkageClustering::sPtLnk *>::const_iterator piter = (*citer)->mBelongingPts.begin(); piter != (*citer)->mBelongingPts.end(); piter++)
			clusters_output.addPoint((*piter)->mCoord1, (*piter)->mCoord2, clusterIndex);

		clusterIndex++;
	}

	cout << clusters_input.size() << " " << clusters_output.size() << endl;

	clusters_output.sort();

	return clusters_output;
}

void Experiments::testRANSAC(const HomographyEstimator &estimator, std::vector<std::vector<unsigned int>> sampleIndices, double threshold)
{
	int KdTreeRange = 10;
	int max_iter = 10;
	unsigned int minimumSampleSet = estimator.getMinimumSampleSet();

	mClusters.clear();

	// Generate models
	vector<HomographyModel> models = ClusterUtils::generateModels(estimator, sampleIndices, mPoints1, mPoints2);

	vector<Point2f> points1 = mPoints1, points2 = mPoints2;

	for (int iter = 0; iter < max_iter; iter++)
	{
		Cluster cluster_opt;

		for (unsigned int modelInd = 0; modelInd < models.size(); modelInd++) 
		{
			vector<Point2f> crt_points1, crt_points2, points_t;

			Cluster cluster = ClusterUtils::computeInliers(models[modelInd].Homography, points1, points2, threshold);

			if (cluster.size() > cluster_opt.size()) {
				cluster_opt = cluster;
			}
		}

		if (cluster_opt.size() > estimator.getMinimumSampleSet()) {
			mClusters.push_back(cluster_opt);

			Clusters clusters(points1, points2);

			clusters.addCluster();
			clusters[0] = cluster_opt;
			clusters.refreshIndices();

			Cluster outliers = clusters.getOutliers();
			points1 = outliers.points1;
			points2 = outliers.points2;
		}

		sampleIndices.clear();
		RandomSampler randomSampler(2, 4, points1.size());
		initRandomSampler(randomSampler, points1, points2, points1.size());
		randomSampler.GetNSample(sampleIndices, points1.size() * 20);
	}
}

void Experiments::testPriorityJLinkage(HomographyEstimator &estimator, std::vector<std::vector<unsigned int>> sampleIndices,
	std::vector<int> normalPriorities, double threshold)
{
	// Should have three priority values for each normal
	assert(normalPriorities.size() == 3);

	// Must be used with our Manhattan-world homography model
	assert(estimator.getModelType() == HOMOGRAPHY_MANHATTAN_WORLD);

	int KdTreeRange = 10;
	unsigned int minimumSampleSet = estimator.getMinimumSampleSet();
	mClusters.clear();

	vector<Point2f> points1 = mPoints1, points2 = mPoints2;

	for (int priority = 0; priority < 3; priority++)
	{
		// Activate only selected normals
		estimator.deactivateAllNormals();
		bool activated = false;
		for (int normalIndex = 0; normalIndex < normalPriorities.size(); normalIndex++) {
			if (normalPriorities[normalIndex] == priority) {
				estimator.activateNormal(normalIndex);
				activated = true;
			}
		}

		// If nothing activated, continue to the next stage
		if (!activated) {
			continue;
		}

		// Generate models with only activated normals
		vector<HomographyModel> models = ClusterUtils::generateModels(estimator, sampleIndices, points1, points2);

		// Initialize the clustering
		JLinkage jlinkage(threshold, models.size(), true, 2, KdTreeRange);

		for (unsigned int i = 0; i < models.size(); i++) {
			jlinkage.AddModel(new Mat(models[i].Homography));
		}

		for (int i = 0; i < (int)points1.size(); i++) {
			jlinkage.AddPoint(points1[i], points2[i]);
		}

		// Do the J-linkage clustering
		list<sClLnk *> tempClustersList = jlinkage.DoJLClusterization();

		// Copy Result Data
		tempClustersList.sort(ClusterSort());
		vector<Cluster> clusterArray;
		for (list<sClLnk *>::const_iterator citer = tempClustersList.begin(); citer != tempClustersList.end(); citer++)
		{
			if ((*citer)->mBelongingPts.size() <= minimumSampleSet)
				break;

			clusterArray.push_back(Cluster());
			for (list<sPtLnk *>::const_iterator piter = (*citer)->mBelongingPts.begin(); piter != (*citer)->mBelongingPts.end(); piter++) {
				clusterArray.back().points1.push_back((*piter)->mCoord1);
				clusterArray.back().points2.push_back((*piter)->mCoord2);
			}
		}

		// Remove (assumed) outliers clusters
		ClusterUtils::removeMinorClusters(clusterArray, estimator.getMinimumSampleSet());
		Clusters clusters = ClusterUtils::computeUniqueInliers(clusterArray, points1, points2, threshold);
		
		for (unsigned int ci = 0; ci < clusters.size(); ci++) {
			mClusters.push_back(clusters[ci]);
		}

		Cluster outliers = clusters.getOutliers();
		points1 = outliers.points1;
		points2 = outliers.points2;

		if (points1.size() < 4) {
			break;
		}

		sampleIndices.clear();
		RandomSampler randomSampler(2, 4, points1.size());
		initRandomSampler(randomSampler, points1, points2, points1.size());
		randomSampler.GetNSample(sampleIndices, points1.size() * 3);
	}

	estimator.activateAllNormals();
}

void Experiments::testPriorityTLinkage(HomographyEstimator &estimator, vector<vector<unsigned int>> sampleIndices,
	vector<int> normalPriorities, double threshold, int numOfSamples, CLUSTERING_TYPE type)
{
	// Should have three priority values for each normal
	assert(normalPriorities.size() == 3);

	// Must be used with our Manhattan-world homography model
	assert(estimator.getModelType() == HOMOGRAPHY_MANHATTAN_WORLD);

	string method;
	switch (type) {
	case MSH:
		method = "MSH";
		break;

	case JLINKAGE:
		method = "Jlinkage";
		break;

	case TLINKAGE:
		method = "Tlinkage";
		break;
	}

	unsigned int minimumSampleSet = estimator.getMinimumSampleSet();
	mClusters.clear();

	vector<Point2f> points1 = mPoints1, points2 = mPoints2;
	const char *directionNames = "xyz";

	for (int priority = 0; priority < 3; priority++)
	{
		// Activate only selected normals
		estimator.deactivateAllNormals();
		bool activated = false;
		for (int directionIndex = 0; directionIndex < normalPriorities.size(); directionIndex++) {
			if (normalPriorities[directionIndex] == priority) {
				estimator.activateNormal(directionNames[directionIndex]);
				activated = true;
			}
		}

		// If nothing activated, continue to the next stage
		if (!activated) {
			continue;
		}

		// Generate models with only activated normals
		vector<HomographyModel> models = ClusterUtils::generateModels(estimator, sampleIndices, mPoints1, mPoints2);
		//vector<HomographyModel> models = ClusterUtils::generateConstrainedModels(estimator, sampleIndices, points1, points2, 2);

		// Initialize the clustering
		int KdTreeRange = 10 < points1.size() ? 10 : points1.size();
		TLinkageClustering::TLinkage tlinkage(threshold, models.size(), true, 2, KdTreeRange);

		for (unsigned int i = 0; i < models.size(); i++) {
			TLinkageClustering::Homography homography;

			homography.homography = models[i].Homography;
			homography.normalIndex = models[i].normal_index;
			homography.isPositiveSide = models[i].tag >= 0;

			tlinkage.AddModel(homography);
		}

		Mat VL = estimator.getVanishingLines();

		for (int i = 0; i < (int)points1.size(); i++) {
			unsigned char indicator = 0;
			indicator += computeLineDistance(VL.col(0), mPoints1[i]) >= 0 ? 1 : 0;
			indicator += computeLineDistance(VL.col(1), mPoints1[i]) >= 0 ? 2 : 0;
			indicator += computeLineDistance(VL.col(2), mPoints1[i]) >= 0 ? 4 : 0;
			tlinkage.AddPoint(mPoints1[i], mPoints2[i], indicator);
		}

		// Do the J-linkage clustering
		list<TLinkageClustering::sClLnk *> tempClustersList = tlinkage.DoTLClusterization();

		// Copy Result Data
		tempClustersList.sort(TLinkageClustering::ClusterSort());
		vector<Cluster> clusterArray;
		for (list<TLinkageClustering::sClLnk *>::const_iterator citer = tempClustersList.begin(); citer != tempClustersList.end(); citer++)
		{
			if ((*citer)->mBelongingPts.size() <= minimumSampleSet)
				break;

			clusterArray.push_back(Cluster());
			for (list<TLinkageClustering::sPtLnk *>::const_iterator piter = (*citer)->mBelongingPts.begin(); piter != (*citer)->mBelongingPts.end(); piter++) {
				clusterArray.back().points1.push_back((*piter)->mCoord1);
				clusterArray.back().points2.push_back((*piter)->mCoord2);
			}
		}

		// If more than clusters are generated, we can do further 
		if (clusterArray.size() != 0) {
			ostringstream stringStream;
			stringStream << "Result\\" << mDataname << "\\" << method << getAppendix(estimator.getModelType()) << "_priority" << priority << "-0.png";
			imwrite(stringStream.str(), ClusterUtils::DrawPointsOnImage(clusterArray, mImage1, false, 2));

			// Remove (assumed) outliers clusters
			ClusterUtils::removeMinorClusters(clusterArray, estimator.getMinimumSampleSet());
			Clusters clusters = ClusterUtils::computeUniqueInliers(clusterArray, points1, points2, threshold);

			stringStream.str("");
			stringStream << "Result\\" << mDataname << "\\" << method << getAppendix(estimator.getModelType()) << "_priority" << priority << "-1.png";
			imwrite(stringStream.str(), ClusterUtils::DrawPointsOnImage(clusters, mImage1, false, 2));

			for (unsigned int ci = 0; ci < clusters.size(); ci++) {
				mClusters.push_back(clusters[ci]);
			}

			Cluster outliers = clusters.getOutliers();
			points1 = outliers.points1;
			points2 = outliers.points2;

			if (points1.size() < 4) {
				break;
			}

			stringStream.str("");
			stringStream << "Result\\" << mDataname << "\\" << method << getAppendix(estimator.getModelType()) << "_priority" << priority << "-or.png";
			imwrite(stringStream.str(), ClusterUtils::DrawPointsOnImage(mClusters, mImage1, outliers, 2));
		}

		sampleIndices.clear();
		RandomSampler randomSampler(2, 2, points1.size());
		initRandomSampler(randomSampler, points1, points2, points1.size());
		randomSampler.GetNSample(sampleIndices, numOfSamples/*sqrt(numOfSamples) > double(points1.size()) ? numOfSamples : points1.size() * 2*/);
	}

	estimator.activateAllNormals();
}

void Experiments::accumulateRandIndex(const string resultFoldername, const HomographyEstimator &estimator,
	CLUSTERING_TYPE type, double threshold, bool considerOutliers)
{
	Clusters clusters;
	vector<Cluster> clusterArray;
	ostringstream stringStream;
	double val;
	string method;
	int methodIndex = 0;

	switch (type) {
	case MSH:
		method = "MSH";
		methodIndex = 3;
		break;

	case JLINKAGE:
		method = "Jlinkage";
		methodIndex = 0;
		break;

	case TLINKAGE:
		method = "Tlinkage";
		methodIndex = 0;
		break;

	case RANSAC:
		method = "RANSAC";
		methodIndex = 0;
	}

	if (estimator.getModelType() == HOMOGRAPHY_MANHATTAN_WORLD) {
		methodIndex += 0;
	}
	else if (estimator.getModelType() == HOMOGRAPHY_WEAK_MANHATTAN_WORLD) {
		methodIndex += 1;
	}
	else {
		methodIndex += 2;
	}

	ClusterUtils::removeClusters(mClusters, estimator.getMinimumSampleSet());
	
	if (mClusters.size() == 0) {
		cout << "Clustering Failed!" << endl;
		return;
	}

	// Save the results and compute RandIndex values
	clusters = ClusterUtils::clusterArrayToClusters(mClusters, mPoints1, mPoints2);
	stringStream << resultFoldername << "\\" << method << getAppendix(estimator.getModelType()) << "00.png";
	imwrite(stringStream.str(), ClusterUtils::DrawPointsOnImage(clusters, mImage1, true, 2));
	//stringStream.str("");
	//stringStream << resultFoldername << method << getAppendix(estimator.getModelType()) << "00.txt";
	//saveClustersTo3DPoints(stringStream.str(), estimator);

	// Compute Rand Index
	if (!mBaseClustering.checkClusterConsistancy(clusters)) {
		cout << "ERROR! The number of points are not matched!: " << endl;
	}
	// Compute Rand Index w.r.t. GT
	val = mBaseClustering.computeRandIndex(clusters, false, considerOutliers);
	if (val != -1.0) {
		mRandIndex[methodIndex][0] += abs(val);
		mNumAccumulated[methodIndex][0]++;
		if (mBestRandIndex[methodIndex][0] < abs(val)) {
			mBestRandIndex[methodIndex][0] = abs(val);
		}
	}
	// Compute Rand Index w.r.t. AGT
	val = mBaseClustering.computeRandIndex(clusters, true, considerOutliers);
	if (val != -1.0) {
		mRandIndex[methodIndex][1] += abs(val);
		mNumAccumulated[methodIndex][1]++;
		if (mBestRandIndex[methodIndex][1] < abs(val)) {
			mBestRandIndex[methodIndex][1] = abs(val);
		}
	}

	// Save the results after recomputing inlier points
	clusters = ClusterUtils::computeUniqueInliers(mClusters, mPoints1, mPoints2, threshold);
	clusters.remove(estimator.getMinimumSampleSet());
	stringStream.str("");
	stringStream << resultFoldername << "\\" << method << getAppendix(estimator.getModelType()) << "01.png";
	imwrite(stringStream.str(), ClusterUtils::DrawPointsOnImage(clusters, mImage1, true, 2));

	//-----------------------------
	// Normal-Aware Merge
	//-----------------------------
	if (estimator.getModelType() == HOMOGRAPHY_MANHATTAN_WORLD) {
		imwrite("testLabeled1.png", ClusterUtils::DrawPointsOnImageL(mClusters, mImage1, false, 2));
		/*
		vector<Cluster> tempClusters = mClusters;
		estimator.computeParameters(tempClusters);
		imwrite("testLabeled_minError.png", ClusterUtils::DrawPointsOnImageL(tempClusters, mImage1, false, 2));

		tempClusters = ClusterUtils::computeInliers(mClusters, estimator, mPoints1, mPoints2, 3., true);
		estimator.computeParameters(tempClusters, PARAMS_MAX_INLIERS, false);
		imwrite("testLabeled_maxInliers_singleThreshold.png", ClusterUtils::DrawPointsOnImageL(tempClusters, mImage1, false, 2));

		tempClusters = ClusterUtils::computeInliers(mClusters, estimator, mPoints1, mPoints2, 3., true);
		estimator.computeParameters(tempClusters, PARAMS_MAX_INLIERS, true);
		imwrite("testLabeled_maxInliers_multiThresholds.png", ClusterUtils::DrawPointsOnImageL(mClusters, mImage1, false, 2));
		*/
		clusterArray = ClusterUtils::mergeClusters(mClusters, estimator, mPoints1, mPoints2, threshold, mImage1);

		clusters = ClusterUtils::clusterArrayToClusters(clusterArray, mPoints1, mPoints2);
		clusters.remove(estimator.getMinimumSampleSet());

		stringStream.str("");
		stringStream << resultFoldername << "\\" << method << getAppendix(estimator.getModelType()) << "02_NAM.png";
		imwrite(stringStream.str(), ClusterUtils::DrawPointsOnImage(clusters, mImage1, true, 2));

		//clusters = ClusterUtils::computeUniqueInliers(clusterArray, mPoints1, mPoints2, threshold, 4);
		//clusters = ClusterUtils::computeUniqueInliers(clusterArray, estimator, mPoints1, mPoints2, threshold, false);
		//clusters = ClusterUtils::computeUniqueInliers(clusterArray, estimator, mPoints1, mPoints2);
		//clusters.remove(estimator.getMinimumSampleSet());

		stringStream.str("");
		stringStream << resultFoldername << "\\" << method << getAppendix(estimator.getModelType()) << "03_NAM.png";
		imwrite(stringStream.str(), ClusterUtils::DrawPointsOnImage(clusters, mImage1, true, 2));

		// Compute Rand Index
		if (!mBaseClustering.checkClusterConsistancy(clusters)) {
			cout << "ERROR! The number of points are not matched!: " << endl;
		}
		// Compute Rand Index w.r.t. GT
		val = mBaseClustering.computeRandIndex(clusters, false, considerOutliers);
		if (val != -1.0) {
			mRandIndex[methodIndex][2] += abs(val);
			mNumAccumulated[methodIndex][2]++;
			if (mBestRandIndex[methodIndex][2] < abs(val)) {
				mBestRandIndex[methodIndex][2] = abs(val);
			}
		}
		// Compute Rand Index w.r.t. AGT
		val = mBaseClustering.computeRandIndex(clusters, true, considerOutliers);
		if (val != -1.0) {
			mRandIndex[methodIndex][3] += abs(val);
			mNumAccumulated[methodIndex][3]++;
			if (mBestRandIndex[methodIndex][3] < abs(val)) {
				mBestRandIndex[methodIndex][3] = abs(val);
			}
		}
	}
	else if (type != RANSAC) {
		clusterArray = ClusterUtils::mergeClusters(mClusters, mPoints1, mPoints2, threshold);
		clusters = ClusterUtils::clusterArrayToClusters(clusterArray, mPoints1, mPoints2);
		clusters.remove(estimator.getMinimumSampleSet());

		stringStream.str("");
		stringStream << resultFoldername << "\\" << method << getAppendix(estimator.getModelType()) << "02_NAM.png";
		imwrite(stringStream.str(), ClusterUtils::DrawPointsOnImage(clusters, mImage1, true, 2));

		// Compute Rand Index
		if (!mBaseClustering.checkClusterConsistancy(clusters)) {
			cout << "ERROR! The number of points are not matched!: " << endl;
		}
		// Compute Rand Index w.r.t. GT
		val = mBaseClustering.computeRandIndex(clusters, false, considerOutliers);
		if (val != -1.0) {
			mRandIndex[methodIndex][2] += abs(val);
			mNumAccumulated[methodIndex][2]++;
			if (mBestRandIndex[methodIndex][2] < abs(val)) {
				mBestRandIndex[methodIndex][2] = abs(val);
			}
		}
		// Compute Rand Index w.r.t. AGT
		val = mBaseClustering.computeRandIndex(clusters, true, considerOutliers);
		if (val != -1.0) {
			mRandIndex[methodIndex][3] += abs(val);
			mNumAccumulated[methodIndex][3]++;
			if (mBestRandIndex[methodIndex][3] < abs(val)) {
				mBestRandIndex[methodIndex][3] = abs(val);
			}
		}
	}

	//-----------------------------
	// MPD Merge Procedure
	//-----------------------------

	clusters = ClusterUtils::MultiplePlaneDetection(mClusters, mPoints1, mPoints2, mImage1.size(), threshold);
	stringStream.str("");
	stringStream << resultFoldername << "\\" << method << getAppendix(estimator.getModelType()) << "02_MPD.png";
	imwrite(stringStream.str(), ClusterUtils::DrawPointsOnImage(clusters, mImage1, true, 2));

	// Compute Rand Index
	if (!mBaseClustering.checkClusterConsistancy(clusters)) {
		cout << "ERROR! The number of points are not matched!: " << endl;
	}
	// Compute Rand Index w.r.t. GT
	if (clusters.empty()) {
		return;
	}

	val = mBaseClustering.computeRandIndex(clusters, false, considerOutliers);
	if (val != -1.0) {
		mRandIndex[methodIndex][4] += abs(val);
		mNumAccumulated[methodIndex][4]++;
		if (mBestRandIndex[methodIndex][4] < abs(val)) {
			mBestRandIndex[methodIndex][4] = abs(val);
		}
	}
	// Compute Rand Index w.r.t. AGT
	val = mBaseClustering.computeRandIndex(clusters, true, considerOutliers);
	if (val != -1.0) {
		mRandIndex[methodIndex][5] += abs(val);
		mNumAccumulated[methodIndex][5]++;
		if (mBestRandIndex[methodIndex][5] < abs(val)) {
			mBestRandIndex[methodIndex][5] = abs(val);
		}
	}

	//-----------------------------
	// Iterative T-linkage
	//-----------------------------
	/*
	// Compute Rand Index
	if (type == TLINKAGE)
	{
		//mRandIndex[methodIndex].resize(8);
		//mNumAccumulated[methodIndex].resize(8);
		//mBestRandIndex[methodIndex].resize(8);

		int iteration;
		//clusters = ClusterUtils::computeUniqueInliers(mClusters, mPoints1, mPoints2, threshold);
		//clusters.remove(estimator.getMinimumSampleSet());
		clusters = ClusterUtils::clusterArrayToClusters(mClusters, mPoints1, mPoints2);
		Clusters tempClusters = clusters;
		tempClusters.remove(estimator.getMinimumSampleSet());
		for (iteration = 0; iteration < 10; iteration++)
		{
			estimator.computeParameters(tempClusters);
			//Clusters tempTest = ClusterUtils::computeInliers(tempClusters.getHomographies(), mPoints1, mPoints2, threshold);
			//imwrite("test.png", ClusterUtils::DrawPointsOnImage(tempTest, mImage1, true, 2));
			//tempClusters = testJLinkage(tempClusters.getHomographies(), threshold);
			tempClusters = testTLinkage(tempClusters, estimator, threshold);
			tempClusters.remove(estimator.getMinimumSampleSet());
			if (clusters == tempClusters)
				break;
			clusters = tempClusters;
		}
		cout << "The number iterated: " << iteration << endl;

		stringStream.str("");
		stringStream << resultFoldername << "\\" << method << getAppendix(estimator.getModelType()) << "02_IT.png";
		//stringStream << "Result\\" << mDataname << "\\" << method << getAppendix(estimator.getModelType()) << "02_IT.png";
		imwrite(stringStream.str(), ClusterUtils::DrawPointsOnImage(clusters, mImage1, true, 2));

		if (!mBaseClustering.checkClusterConsistancy(clusters)) {
			cout << "ERROR! The number of points are not matched!: " << endl;
		}
		val = mBaseClustering.computeRandIndex(clusters, false);
		if (val != -1.0) {
			mRandIndex[methodIndex][6] += abs(val);
			mNumAccumulated[methodIndex][6]++;
			if (mBestRandIndex[methodIndex][6] < abs(val)) {
				mBestRandIndex[methodIndex][6] = abs(val);
			}
		}
		val = mBaseClustering.computeRandIndex(clusters, true);
		if (val != -1.0) {
			mRandIndex[methodIndex][7] += abs(val);
			mNumAccumulated[methodIndex][7]++;
			if (mBestRandIndex[methodIndex][7] < abs(val)) {
				mBestRandIndex[methodIndex][7] = abs(val);
			}
		}

		//stringStream.str("");
		//stringStream << "Result\\" << mDataname << "\\" << method << getAppendix(estimator.getModelType()) << "02_IT.m";
		//saveClustersTo3DPointsMatlab(clusters, stringStream.str(), estimator);
	}*/
}

void Experiments::print(CLUSTERING_TYPE type)
{
	if (type == TLINKAGE) {
		cout << "\n=== T-Linkage Clustering ===" << endl;
		cout << "*** Model: Constrained homography" << endl;
		print(mRandIndex[0], mNumAccumulated[0], mBestRandIndex[0], "Tlinkage");
		cout << "\n*** Model: Weak Manhattan-world 2.5 points homography" << endl;
		print(mRandIndex[1], mNumAccumulated[1], mBestRandIndex[1], "Tlinkage");
		cout << "\n*** Model: Unconstrained homography" << endl;
		print(mRandIndex[2], mNumAccumulated[2], mBestRandIndex[2], "Tlinkage");
	}
	else if (type == JLINKAGE) {
		cout << "\n=== J-Linkage Clustering ===" << endl;
		cout << "*** Model: Constrained homography" << endl;
		print(mRandIndex[0], mNumAccumulated[0], mBestRandIndex[0], "Jlinkage");
		cout << "\n*** Model: Weak Manhattan-world 2.5 points homography" << endl;
		print(mRandIndex[1], mNumAccumulated[1], mBestRandIndex[1], "Jlinkage");
		cout << "\n*** Model: Unconstrained homography" << endl;
		print(mRandIndex[2], mNumAccumulated[2], mBestRandIndex[2], "Jlinkage");
	}
	else if (type == MSH) {
		cout << "\n=== Mode-Seeking on Hypergraphs ===" << endl;
		cout << "*** Model: Constrained homography" << endl;
		print(mRandIndex[3], mNumAccumulated[3], mBestRandIndex[3], "MSH");
		cout << "\n*** Model: Weak Manhattan-world 2.5 points homography" << endl;
		print(mRandIndex[4], mNumAccumulated[4], mBestRandIndex[4], "MSH");
		cout << "\n*** Model: Unconstrained homography" << endl;
		print(mRandIndex[5], mNumAccumulated[5], mBestRandIndex[5], "MSH");
	}
}

void Experiments::print(vector<double> &randIndex, vector<int> &numAccumulated, std::vector<double> bestRandIndex, const string method)
{
	cout << "--- Rand Index of pure " << method << " clustering-- - " << endl;/*
	if (numAccumulated[0] > 0) {
		cout << method << ") Computed average adjusted Rand Index w.r.t. GT is " << randIndex[0] / (double)numAccumulated[0] << endl;
	}
	if (numAccumulated[1] > 0) {
		cout << method << ") Computed average adjusted Rand Index w.r.t. AGT is " << randIndex[1] / (double)numAccumulated[1] << endl;
	}
	if (bestRandIndex[0] > 0) {
		cout << method << ") Computed best adjusted Rand Index w.r.t. GT is " << bestRandIndex[0] << endl;
	}*/
	if (bestRandIndex[1] > 0) {
		cout << method << ") Computed best adjusted Rand Index w.r.t. AGT is " << bestRandIndex[1] << endl;
	}
	cout << "--- Rand Index after our merge procedure ---" << endl;/*
	if (numAccumulated[2] > 0) {
		cout << method << "-Merge) Computed average adjusted Rand Index w.r.t. GT is " << randIndex[2] / (double)numAccumulated[2] << endl;
	}
	if (numAccumulated[3] > 0) {
		cout << method << "-Merge) Computed average adjusted Rand Index w.r.t. AGT is " << randIndex[3] / (double)numAccumulated[3] << endl;
	}
	if (bestRandIndex[2] > 0) {
		cout << method << ") Computed best adjusted Rand Index w.r.t. GT is " << bestRandIndex[2] << endl;
	}*/
	if (bestRandIndex[3] > 0) {
		cout << method << ") Computed best adjusted Rand Index w.r.t. AGT is " << bestRandIndex[3] << endl;
	}
	cout << "--- Rand Index after MPD procedure ---" << endl;/*
	if (numAccumulated[4] > 0) {
		cout << method << "-MPD) Computed average adjusted Rand Index w.r.t. GT is " << randIndex[4] / (double)numAccumulated[4] << endl;
	}
	if (numAccumulated[5] > 0) {
		cout << method << "-MPD) Computed average adjusted Rand Index w.r.t. AGT is " << randIndex[5] / (double)numAccumulated[5] << endl;
	}
	if (bestRandIndex[4] > 0) {
		cout << method << ") Computed best adjusted Rand Index w.r.t. GT is " << bestRandIndex[4] << endl;
	}*/
	if (bestRandIndex[5] > 0) {
		cout << method << ") Computed best adjusted Rand Index w.r.t. AGT is " << bestRandIndex[5] << endl;
	}
}

// Save rand index data as a file
void Experiments::saveBestRandIndicesAGT(ofstream &fout)
{
	// T-linkage
	fout << fixed << setprecision(6) << mBestRandIndex[2][1] << " " << mBestRandIndex[1][1] << " " << mBestRandIndex[0][1] << " ";	// T-linkage
	fout << fixed << setprecision(6) << mBestRandIndex[2][3] << " " << mBestRandIndex[1][3] << " " << mBestRandIndex[0][3] << " ";	// J-linkage + merge
	//fout << mBestRandIndex[2][7] << " " << mBestRandIndex[1][7] << " " << mBestRandIndex[0][7] << " ";	// Iterative T-linkage
	fout << fixed << setprecision(6) << mBestRandIndex[2][5] << " " << mBestRandIndex[1][5] << " " << mBestRandIndex[0][5] << " ";	// T-linkage + MPD

	// MSH
	//fout << fixed << setprecision(6) << mBestRandIndex[5][1] << " " << mBestRandIndex[4][1] << " " << mBestRandIndex[3][1] << " ";	// w/o merge
	//fout << fixed << setprecision(6) << mBestRandIndex[3][3] << " ";																// w/ our merge
	//fout << fixed << setprecision(6) << mBestRandIndex[5][5] << " " << mBestRandIndex[4][5] << " " << mBestRandIndex[3][5] << endl;	// w/ MPD

	fout << endl;
}

// Save rand index data as a file
void Experiments::saveBestRandIndicesGT(ofstream &fout)
{
	// J-linkage/T-linkage, unconstrained, WMW, constrained
	fout << fixed << setprecision(6) << mBestRandIndex[2][0] << " " << mBestRandIndex[1][0] << " " << mBestRandIndex[0][0] << " ";	// J-linkage
	fout << fixed << setprecision(6) << mBestRandIndex[2][2] << " " << mBestRandIndex[1][2] << " " << mBestRandIndex[0][2] << " ";	// J-linkage + merge
	//fout << mBestRandIndex[2][7] << " " << mBestRandIndex[1][7] << " " << mBestRandIndex[0][7] << " ";	// Iterative T-linkage
	fout << fixed << setprecision(6) << mBestRandIndex[2][4] << " " << mBestRandIndex[1][4] << " " << mBestRandIndex[0][4] << " ";	// J-linkage + MPD

	// MSH
	//fout << fixed << setprecision(6) << mBestRandIndex[5][0] << " " << mBestRandIndex[4][0] << " " << mBestRandIndex[3][0] << " ";	// MSH
	//fout << fixed << setprecision(6) << mBestRandIndex[3][2] << " ";																// MSH + merge
	//fout << fixed << setprecision(6) << mBestRandIndex[5][4] << " " << mBestRandIndex[4][4] << " " << mBestRandIndex[3][4] << endl;	// MSH + MPD

	fout << endl;
}

// Save rand index data as a file
void Experiments::saveMeanRandIndicesGT(ofstream &fout)
{
	// J-linkage/T-linkage, unconstrained, WMW, constrained
	fout << fixed << setprecision(6) << mRandIndex[2][0] / double(mNumAccumulated[2][0]) << " " << mRandIndex[1][0] / double(mNumAccumulated[1][0]) << " " << mRandIndex[0][0] / double(mNumAccumulated[0][0]) << " ";	// J-linkage
	fout << fixed << setprecision(6) << mRandIndex[2][2] / double(mNumAccumulated[2][2]) << " " << mRandIndex[1][2] / double(mNumAccumulated[1][2]) << " " << mRandIndex[0][2] / double(mNumAccumulated[0][2]) << " ";	// J-linkage + merge
	//fout << mBestRandIndex[2][7] << " " << mBestRandIndex[1][7] << " " << mBestRandIndex[0][7] << " ";	// Iterative T-linkage
	fout << fixed << setprecision(6) << mRandIndex[2][4] / double(mNumAccumulated[2][4]) << " " << mRandIndex[1][4] / double(mNumAccumulated[1][4]) << " " << mRandIndex[0][4] / double(mNumAccumulated[0][4]) << " ";	// J-linkage + MPD

	// MSH
	//fout << fixed << setprecision(6) << mBestRandIndex[5][0] << " " << mBestRandIndex[4][0] << " " << mBestRandIndex[3][0] << " ";	// MSH
	//fout << fixed << setprecision(6) << mBestRandIndex[3][2] << " ";																// MSH + merge
	//fout << fixed << setprecision(6) << mBestRandIndex[5][4] << " " << mBestRandIndex[4][4] << " " << mBestRandIndex[3][4] << endl;	// MSH + MPD

	fout << endl;
}

// Save rand index data as a file
void Experiments::saveMeanRandIndicesAGT(ofstream &fout)
{
	// J-linkage/T-linkage, unconstrained, WMW, constrained
	fout << fixed << setprecision(6) << mRandIndex[2][1] / double(mNumAccumulated[2][1]) << " " << mRandIndex[1][1] / double(mNumAccumulated[1][1]) << " " << mRandIndex[0][1] / double(mNumAccumulated[0][1]) << " ";	// J-linkage
	fout << fixed << setprecision(6) << mRandIndex[2][3] / double(mNumAccumulated[2][3]) << " " << mRandIndex[1][3] / double(mNumAccumulated[1][3]) << " " << mRandIndex[0][3] / double(mNumAccumulated[0][3]) << " ";	// J-linkage + merge
	//fout << mBestRandIndex[2][7] << " " << mBestRandIndex[1][7] << " " << mBestRandIndex[0][7] << " ";	// Iterative T-linkage
	fout << fixed << setprecision(6) << mRandIndex[2][4] / double(mNumAccumulated[2][5]) << " " << mRandIndex[1][5] / double(mNumAccumulated[1][5]) << " " << mRandIndex[0][5] / double(mNumAccumulated[0][5]) << " ";	// J-linkage + MPD

	// MSH
	//fout << fixed << setprecision(6) << mBestRandIndex[5][1] << " " << mBestRandIndex[4][1] << " " << mBestRandIndex[3][1] << " ";	// w/o merge
	//fout << fixed << setprecision(6) << mBestRandIndex[3][3] << " ";																// w/ our merge
	//fout << fixed << setprecision(6) << mBestRandIndex[5][5] << " " << mBestRandIndex[4][5] << " " << mBestRandIndex[3][5] << endl;	// w/ MPD

	fout << endl;
}

// Save clusters as list of 3D points
bool Experiments::saveClustersTo3DPoints(const string &filename, const HomographyEstimator &estimator)
{
	ofstream fout(filename, ofstream::out);

	if (!fout.is_open()) {
		cout << "Cannot open file to write the result: " << filename << endl;
		return false;
	}

	fout << "clusters=" << mClusters.size() << endl;
	for (unsigned int ci = 0; ci < mClusters.size(); ci++) {
		estimator.computeParameters(mClusters[ci].points1, mClusters[ci].points2, mClusters[ci].translation, mClusters[ci].plane_normal);
		vector<Point3f> points = estimator.compute3DPoints(mClusters[ci].points1, mClusters[ci].plane_normal, norm(mClusters[ci].translation));
		
		fout << "num=" << points.size() << endl;
		for (unsigned int pi = 0; pi < points.size(); pi++) {
			fout << points[pi].x << " " << points[pi].y << " " << points[pi].z << endl;
		}
	}

	fout.close();

	return true;
}

// Save clusters as list of 3D points
bool Experiments::saveClustersTo3DPointsMatlab(Clusters &clusters, const string &filename, const HomographyEstimator &estimator)
{
	ofstream fout(filename, ofstream::out);

	if (!fout.is_open()) {
		cout << "Cannot open file to write the result: " << filename << endl;
		return false;
	}

	fout << "% 3D Points" << clusters.size() << endl;
	fout << "P = cell(1, " << clusters.size() << ");" << endl;
	fout << "planes = zeros(3, 4, " << clusters.size() << ");" << endl;
	for (unsigned int ci = 0; ci < clusters.size(); ci++) {
		fout << "% Cluster #" << ci << endl;
		fout << "P{" << ci + 1 << "}=zeros(3, " << clusters.size() << ");" << endl;
		estimator.computeParameters(clusters[ci].points1, clusters[ci].points2, clusters[ci].translation, clusters[ci].plane_normal);
		double sign = clusters[ci].translation.at<double>(0) * clusters[ci].translation.at<double>(1) * clusters[ci].translation.at<double>(2);
		vector<Point3f> points;
		if (sign < 0) {
			points = estimator.compute3DPoints(clusters[ci].points1, clusters[ci].plane_normal, -norm(clusters[ci].translation));
		}
		else {
			points = estimator.compute3DPoints(clusters[ci].points1, clusters[ci].plane_normal, norm(clusters[ci].translation));
		}

		float min_x = FLT_MAX, min_y = FLT_MAX, max_x = FLT_MIN, max_y = FLT_MIN;

		// write a point coordinate
		for (unsigned int pi = 0; pi < points.size(); pi++) {
			fout << "P{" << ci + 1 << "}(:, " << pi + 1 << ")=[";
			fout << points[pi].x << ";" << points[pi].y << ";" << points[pi].z << "];" << endl;

			// Get the minimum values for each component
			if (points[pi].x < min_x) {
				min_x = points[pi].x;
			}
			if (points[pi].y < min_y) {
				min_y = points[pi].y;
			}
			if (points[pi].x > max_x) {
				max_x = points[pi].x;
			}
			if (points[pi].y > max_y) {
				max_y = points[pi].y;
			}
		}

		fout << "% Plane corners" << ci << endl;
		float depth;

		// Compute top-left corner of the plane
		if (sign < 0) {
			depth = estimator.computeDepth(min_x, min_y, clusters[ci].plane_normal, -norm(clusters[ci].translation));
		}
		else {
			depth = estimator.computeDepth(min_x, min_y, clusters[ci].plane_normal, norm(clusters[ci].translation));
		}
		fout << "planes(:, 1, " << ci + 1 << ") = [" << min_x << "," << min_y << "," << depth << "];" << endl;

		// Compute top-right corner of the plane
		if (sign < 0) {
			depth = estimator.computeDepth(min_x, max_y, clusters[ci].plane_normal, -norm(clusters[ci].translation));
		}
		else {
			depth = estimator.computeDepth(min_x, max_y, clusters[ci].plane_normal, norm(clusters[ci].translation));
		}
		fout << "planes(:, 2, " << ci + 1 << ") = [" << min_x << "," << max_y << "," << depth << "];" << endl;

		// Compute buttom-right corner of the plane
		if (sign < 0) {
			depth = estimator.computeDepth(max_x, max_y, clusters[ci].plane_normal, -norm(clusters[ci].translation));
		}
		else {
			depth = estimator.computeDepth(max_x, max_y, clusters[ci].plane_normal, norm(clusters[ci].translation));
		}
		fout << "planes(:, 3, " << ci + 1 << ") = [" << max_x << "," << max_y << "," << depth << "];" << endl;

		// Compute buttom-left corner of the plane
		if (sign < 0) {
			depth = estimator.computeDepth(max_x, min_y, clusters[ci].plane_normal, -norm(clusters[ci].translation));
		}
		else {
			depth = estimator.computeDepth(max_x, min_y, clusters[ci].plane_normal, norm(clusters[ci].translation));
		}
		fout << "planes(:, 4, " << ci + 1 << ") = [" << max_x << "," << min_y << "," << depth << "];" << endl;
	}

	fout.close();

	return true;
}

string getAppendix(HOMOGRAPHY_TYPE type)
{
	switch (type)
	{
	case HOMOGRAPHY_MANHATTAN_WORLD:
		return "_cons";
	case HOMOGRAPHY_WEAK_MANHATTAN_WORLD:
		return "_weakcons";
	case HOMOGRAPHY_UNCONSTRAINED:
		return "";
	default:
		return "";
	}
}

void initRandomSampler(RandomSampler &randomSampler, const vector<Point2f> &points1,
	const vector<Point2f> &points2, int num_of_models, RS_NFSAMPLINGTYPE samplingType)
{
	float SigmaExp = 1.0;
	int KdTreeRange = 10;
	float KdTreeCloseProb = 0.8f;
	float KdTreeFarProb = 1.0f - KdTreeCloseProb;

	// Add points to the random sampler
	randomSampler.SetPoints(points1, points2);

	// Initialize non-first sampling type
	switch (samplingType)
	{
	case NFST_EXP:
		randomSampler.SetNFSamplingTypeExp(SigmaExp);
		break;

	case NFST_NN:
		randomSampler.SetNFSamplingTypeNN(KdTreeRange, KdTreeCloseProb, KdTreeFarProb, false);
		break;

	case NFST_NN_ME:
		randomSampler.SetNFSamplingTypeNN(KdTreeRange, KdTreeCloseProb, KdTreeFarProb, true);
		break;
	}
}

void dataAnalysis(char *inputDataFilename)
{
	vector<IOUtils::TwoViewParamPathes> pathParams;

	pathParams = IOUtils::loadTwoViewSturucture(inputDataFilename);

	double sumPointsPerPlane = 0;
	int sumNumOfOutliers = 0;
	int sumNumOfPlanes = 0;
	for (size_t i = 0; i < pathParams.size(); i++)
	{
		// Local variables
		vector<Point2f> points1, points2;
		Mat image1, image2;
		double threshold = 2.5;

#ifdef _DEBUG
		int num_iteration = 1;
#else
		int num_iteration = 5;
#endif

		// Initialize model estimator
		HomographyEstimator estimator(threshold);
		estimator.loadParameters(pathParams[i]);

		// Load images and compute matches
		image1 = imread(pathParams[i].pathImg1);
		image2 = imread(pathParams[i].pathImg2);
		if (image1.empty()) {
			cout << "Not able to open image file: " << pathParams[i].pathImg1 << endl;
		}
		if (image2.empty()) {
			cout << "Not able to open image file: " << pathParams[i].pathImg2 << endl;
		}

		// Point matches
		if (!pathParams[i].pathMatches.empty()) {
			if (!IOUtils::loadMatches(pathParams[i].pathMatches, points1, points2)) {
				cout << "Not able to open match file: " << pathParams[i].pathMatches << endl;
			}
		}
		else {
			ClusterUtils::computeMatches(image1, image2, points1, points2);
		}

		if (!pathParams[i].dataname.empty()) {
			// Generate result folder if it does not exist
			boost::filesystem::path result_folder(string("Result\\") + pathParams[i].dataname);
			if (!boost::filesystem::exists(result_folder)) {
				boost::filesystem::create_directory(result_folder);
			}
		}
		else {
			cout << "Dataname is empty!" << endl;
			system("pause");
			return;
		}

		// Generate result folder if it does not exist
		boost::filesystem::path result_folder(string("Result\\") + pathParams[i].dataname + "\\" + pathParams[i].pairname);
		if (!boost::filesystem::exists(result_folder)) {
			boost::filesystem::create_directory(result_folder);
		}

		// Initialize ground-truth clusters
		BaseClustering baseClustering(image1, image2, points1, points2);
		baseClustering.loadGTClusters(pathParams[i].pathGTCluster, threshold);

		// Analysis
		double pointsPerPlane;
		int numOfOutliers, numOfPlanes;
		baseClustering.Analysis(pointsPerPlane, numOfOutliers, numOfPlanes);
		sumPointsPerPlane += pointsPerPlane;
		sumNumOfOutliers += numOfOutliers;
		sumNumOfPlanes += numOfPlanes;
	}

	cout << "The average number of points per plane: " << sumPointsPerPlane / (double)pathParams.size() << endl;
	cout << "The average number of outliers: " << (double)sumNumOfOutliers / (double)pathParams.size() << endl;
	cout << "The average number of planes: " << (double)sumNumOfPlanes / (double)pathParams.size() << endl;
}

void myTest(string inputDataFilename, int testStep, int numOfTestPairsPerData, bool considerOutliers)
{
	vector<IOUtils::TwoViewParamPathes> pathParams;
	const int num_of_models = 2000;
	const double threshold = 2.5;

#ifdef _DEBUG
	const int num_iteration = 1;
#else
	const int num_iteration = 10;
#endif

	pathParams = IOUtils::loadTwoViewSturucture(inputDataFilename);

	ofstream foutBestGT("bestGT.txt", fstream::out);
	if (!foutBestGT.is_open()) {
		cout << "File does not open: output.txt" << endl;
		system("pause");
		return;
	}

	ofstream foutBestAGT("bestAGT.txt", fstream::out);
	if (!foutBestAGT.is_open()) {
		cout << "File does not open: output.txt" << endl;
		system("pause");
		return;
	}


	ofstream foutMeanGT("meanGT.txt", fstream::out);
	if (!foutMeanGT.is_open()) {
		cout << "File does not open: output.txt" << endl;
		system("pause");
		return;
	}

	ofstream foutMeanAGT("meanAGT.txt", fstream::out);
	if (!foutMeanAGT.is_open()) {
		cout << "File does not open: output.txt" << endl;
		system("pause");
		return;
	}

	for (size_t i = 0; i < pathParams.size(); i++)
	{
		// Local variables
		vector<KeyPoint> keypoints1, keypoints2;
		Mat descriptors1, descriptors2;
		Ptr<SIFT> detector = SIFT::create();

		// Write the data name to output log file
		foutBestGT << pathParams[i].dataname << endl;
		foutBestAGT << pathParams[i].dataname << endl;
		foutMeanGT << pathParams[i].dataname << endl;
		foutMeanAGT << pathParams[i].dataname << endl;

		// Load the first image
		Mat image1 = imread(pathParams[i].pathImg1);
		if (image1.empty()) {
			cout << "Not able to open image file: " << pathParams[i].pathImg1 << endl;
			system("pause");
			return;
		}
		else {
			Mat grayImg;
			if (image1.channels() == 3) {
				cvtColor(image1, grayImg, CV_BGR2GRAY);
			}
			else {
				grayImg = image1.clone();
			}
			detector->detect(grayImg, keypoints1);
			detector->compute(grayImg, keypoints1, descriptors1);
		}

		// Load target images to evaluate
		vector<String> imagelist;
		string imageFoldername;
		unsigned int beginIndex = 0;
		if (pathParams[i].pathImg2.empty()) {
			cout << "Image2 is empty!" << endl;
			system("pause");
			return;
		}
		else {
			boost::filesystem::path imagePath(pathParams[i].pathImg2);
			imageFoldername = imagePath.parent_path().string() + "\\";
			cv::String pattern = imageFoldername + "*" + imagePath.extension().string();
			glob(pattern, imagelist);

			// Remove vanishing direction files from the image list
			vector<String>::iterator iter = imagelist.begin();
			while (iter != imagelist.end()) {
				if (iter->find("_VD") == String::npos && iter->find("_GT") == String::npos) {
					iter++;
				}
				else {
					iter = imagelist.erase(iter);
				}
			}
		}

		if (imagelist.empty()) {
			cout << "Image2 list is empty!" << endl;
			system("pause");
			return;
		}
		else {
			while (imagelist[beginIndex].compare(pathParams[i].pathImg2)) {
				beginIndex++;
			}
		}

		// Generate result folder if it does not exist
		if (!pathParams[i].dataname.empty()) {
			boost::filesystem::path result_folder(string("Result\\") + pathParams[i].dataname);
			if (!boost::filesystem::exists(result_folder)) {
				boost::filesystem::create_directory(result_folder);
			}
		}
		else {
			cout << "Dataname is empty!" << endl;
			system("pause");
			return;
		}

		for (unsigned int imageIndex = beginIndex, iteration = 0;
			imageIndex < imagelist.size() && iteration < numOfTestPairsPerData; imageIndex += testStep, iteration++)
		{
			// Load images and compute matches
			pathParams[i].pathImg2 = imagelist[imageIndex];
			Mat image2 = imread(imagelist[imageIndex]);
			if (image2.empty()) {
				cout << "Not able to open image file: " << imagelist[imageIndex] << endl;
				continue;
			}
			else {
				boost::filesystem::path image1Path(pathParams[i].pathImg1);
				boost::filesystem::path image2Path(imagelist[imageIndex].c_str());
				pathParams[i].pairname = image1Path.filename().stem().string() + "-" + image2Path.filename().stem().string();

				// Update vanishing direction file of second image
				pathParams[i].pathVD2 = image2Path.parent_path().string() + "\\" + image2Path.filename().stem().string() + "_VD.txt";

				// Compute feature points
				Mat grayImg;
				if (image2.channels() == 3) {
					cvtColor(image2, grayImg, CV_BGR2GRAY);
				}
				else {
					grayImg = image2.clone();
				}
				detector->detect(grayImg, keypoints2);
				detector->compute(grayImg, keypoints2, descriptors2);
			}

			// Generate result folder if it does not exist
			boost::filesystem::path result_folder(string("Result\\") + pathParams[i].dataname + "\\" + pathParams[i].pairname);
			if (!boost::filesystem::exists(result_folder)) {
				boost::filesystem::create_directory(result_folder);
			}

			vector<Point2f> points1, points2;
			ClusterUtils::computeMatches(keypoints1, keypoints2, descriptors1, descriptors2, points1, points2);

			// Draw computed matches
			imwrite(result_folder.string() + "\\matches1.png", ClusterUtils::DrawPointsOnImage(points1, image1, 2));
			imwrite(result_folder.string() + "\\matches2.png", ClusterUtils::DrawPointsOnImage(points2, image2, 2));

			// Load and print new parameters
			HomographyEstimator estimator;
			estimator.loadParameters(pathParams[i]);
			estimator.setThreshold('x', 2.5);
			estimator.setThreshold('y', 2.5);
			estimator.setThreshold('z', 2.5);
			pathParams[i].print();

			// Initialize ground-truth clusters
			//estimator.setModelType(HOMOGRAPHY_MANHATTAN_WORLD);
			BaseClustering baseClustering(image1, image2, points1, points2);
			baseClustering.loadGTClusters(pathParams[i].pathGTCluster, estimator, threshold);
			baseClustering.drawGTClusters(result_folder.string(), "ground-truth");

			continue;

			//------------------------------
			// Experiments
			//------------------------------

			cout << "\n+++ Clustering Tests on " << pathParams[i].dataname << " (" << pathParams[i].pairname << ") +++" << endl;
			Experiments experiments(pathParams[i].dataname, baseClustering, image1, image2, points1, points2);

			RandomSampler randomSampler(2, 4, points1.size());
			vector<vector<unsigned int>> indices;
			initRandomSampler(randomSampler, points1, points2, points1.size());

			for (int j = 0; j < num_iteration; j++)
			{
				cout << "Iteration #" << j << endl;

				randomSampler.GetNSample(indices, num_of_models);

				estimator.drawVanishingLines(image1, image2, result_folder.string() + "\\VD1.png", result_folder.string() + "\\VD2.png");

				//------------------------------
				// T-linkage
				//------------------------------

				//estimator.deactivateNormal('z');
				estimator.setModelType(HOMOGRAPHY_MANHATTAN_WORLD);
				experiments.testTLinkage(estimator, indices, threshold);
				experiments.accumulateRandIndex(result_folder.string(), estimator, TLINKAGE, threshold, considerOutliers);
				//estimator.activateAllNormals();

				estimator.setModelType(HOMOGRAPHY_WEAK_MANHATTAN_WORLD);
				//experiments.testRANSAC(estimator, indices, threshold);
				experiments.testTLinkage(estimator, indices, threshold);
				experiments.accumulateRandIndex(result_folder.string(), estimator, TLINKAGE, threshold, considerOutliers);

				estimator.setModelType(HOMOGRAPHY_UNCONSTRAINED);
				experiments.testTLinkage(estimator, indices, threshold);
				experiments.accumulateRandIndex(result_folder.string(), estimator, TLINKAGE, threshold, considerOutliers);
			}

			//------------------------------
			// Write results
			//------------------------------
			experiments.saveBestRandIndicesGT(foutBestGT);
			experiments.saveBestRandIndicesAGT(foutBestAGT);
			experiments.saveMeanRandIndicesGT(foutMeanGT);
			experiments.saveMeanRandIndicesGT(foutMeanAGT);
		}

		detector.release();
	}

	foutBestGT.close();
	foutBestAGT.close();
	foutMeanGT.close();
	foutMeanAGT.close();
	
	return;
}

//------------------------------------------------
//	Main Entry
//------------------------------------------------
int main()
{
	//dataAnalysis("Input\\MichiganIndoor_Object.txt");
	myTest("Input\\MichiganIndoor_Library.txt", 1, 30, true);
	//DataSetEvaluation("Input\\MichiganIndoor_Library.txt");
	system("pause");

	return EXIT_SUCCESS;
}
