#include "TLinkage.h"

#ifndef JL_NO_THREADS
#define MAXTHREADS 8
#include <boost/thread/thread.hpp>
#include <boost/bind.hpp>
#include <boost/thread/mutex.hpp>
#endif

#ifndef JL_NO_THREADS
// Mutex for shared variables over threads
boost::mutex mMutexMCurrentInvalidatingDistance;
#endif

using std::list;
using std::vector;

using cv::Mat;
using cv::Point2f;

namespace TLinkageClustering {
	// Costructor
	TLinkage::TLinkage(float nInliersThreshold, // Scale of the algorithm
		unsigned int nModelBufferSize, // Model buffer size, set to 0 to allow growable size
		bool nCopyPtsCoords, // Copy the coords value of the new points or use directly the pointer? (The second choice is faster but you need to deallocate memory outside the class)
		unsigned int nPtDimension, //Used by Kd-Tree
		int nKNeighboards,  //K-neighboards for each points to be used for the clusterization (neg value means use all the dataset)
		bool(*nClusterClusterDiscardTest)(const sClLnk *, const sClLnk *), // Function handler to perform a cluster-cluster test. Should return true if the cluster can be merged by some additional condition, false otherwise.
		void(*nInitializeAdditionalData)(sClLnk *), // Function handler -  initialization routine when additional data are created
		void(*nClusterMergeAdditionalOperation)(const sClLnk *), // Function handler - called when two clusters are merged
		void(*nDestroyAdditionalData)(sClLnk *) // Function handler -  called when additional data are destroyed
		) :
		mDataPoints(), mDataClusters(), mModels(), mDistancesToBeUpdated(), mKDTree(nPtDimension){
		// Max models sizes, set to 0 to allow list
		mModelBufferSize = nModelBufferSize;
		mCurrentModelPointer = 0;
		mCopyPtsCoords = nCopyPtsCoords;
		mInliersThreshold = nInliersThreshold;
		if (mModelBufferSize != 0)
			mModels.resize(mModelBufferSize, NULL);

		mDistancesToBeUpdatedSize = 0;
		mKNeighboards = nKNeighboards;
		mClusterClusterDiscardTest = nClusterClusterDiscardTest;
		mClusterMergeAdditionalOperation = nClusterMergeAdditionalOperation;
		mInitializeAdditionalData = nInitializeAdditionalData;
		mDestroyAdditionalData = nDestroyAdditionalData;
		mDataPointsSize = 0;

		mUseVanishingLineConstraint = false;
		mUseIndividualThresholds = false;
	}

	TLinkage::~TLinkage()
	{
	}

	void TLinkage::setMultiInliersThreshold(bool setTrue)
	{
		mUseIndividualThresholds = setTrue;
	}

	float TLinkage::DistanceFunction(const Homography &model, const Point2f &dataPt, const Point2f &dataPtT)
	{
		double x = dataPt.x * model.homography.at<double>(0, 0) + dataPt.y * model.homography.at<double>(0, 1) + model.homography.at<double>(0, 2);
		double y = dataPt.x * model.homography.at<double>(1, 0) + dataPt.y * model.homography.at<double>(1, 1) + model.homography.at<double>(1, 2);
		double z = dataPt.x * model.homography.at<double>(2, 0) + dataPt.y * model.homography.at<double>(2, 1) + model.homography.at<double>(2, 2);
		x /= z;
		y /= z;
		return (float)sqrt((dataPtT.x - x)*(dataPtT.x - x) + (dataPtT.y - y)*(dataPtT.y - y));
	}

	// Compute preference function
	float TLinkage::PreferenceFunction(const Homography &model, const Point2f &coord1, const Point2f &coord2)
	{
		// consider VL constraints while doing clustring
		if ((pointIndexer[Match(coord1, coord2)] >> model.normalIndex & 1) != model.isPositiveSide) {
			std::cout << "Skipped!" << std::endl;
			return 0.f;
		}

		float distance = DistanceFunction(model, coord1, coord2);
		float preference = 0.f;

		if (mUseIndividualThresholds) {
			if (distance < model.inlierThreshold) {
				preference = std::exp(-distance / (mInliersThreshold / 5.0f));
			}
		}
		else {
			if (distance < mInliersThreshold) {
				preference = std::exp(-distance / (mInliersThreshold / 5.0f));
			}
		}

		return preference;
	}

	// Add a new point to the class
	sPtLnk *TLinkage::AddPoint(const Point2f &nCoordinates1, const Point2f &nCoordinates2, unsigned char indicator){
		// Create a new point and a new cluster as well
		sPtLnk *nPt = new sPtLnk();
		sClLnk *nPtCl = new sClLnk();
		nPt->mBelongingCluster = nPtCl;
		nPtCl->mBelongingPts.push_back(nPt);

		if (mModelBufferSize > 0) {
			nPt->mPreferenceFunction.resize(mModelBufferSize);
			nPtCl->mPreferenceFunction.resize(mModelBufferSize);
		}
		else {
			nPt->mPreferenceFunction.resize((unsigned int)mModels.size());
			nPtCl->mPreferenceFunction.resize((unsigned int)mModels.size());
		}

		nPt->mCoord1 = nCoordinates1;
		nPt->mCoord2 = nCoordinates2;

		// Create the Preference Set -> check the distance of every points from the loaded models.
		nPt->mPreferenceFunction.resize((unsigned int)mModels.size());
		nPtCl->mPreferenceFunction.resize((unsigned int)mModels.size());

		// Calculate the PF of the new point
		unsigned int cModelCount = 0;
		for (vector<Homography *>::iterator tIter = mModels.begin(); tIter != mModels.end(); ++tIter){
			if ((*tIter) == NULL)
				continue;

			float preference = PreferenceFunction((**tIter), nPt->mCoord1, nPt->mCoord2);
			nPt->mPreferenceFunction[cModelCount] = preference;
			nPtCl->mPreferenceFunction[cModelCount] = preference;
			cModelCount++;
		}

		// Add the point to the kd-tree
		sPtLnkPointer nPtPointer;
		nPtPointer.mPtLnk = nPt;
		nPtPointer.mPtLnk->mAlreadyFound = false;
		mKDTree.insert(nPtPointer);
		//mKDTree.optimise();
		nPt->mToBeUpdateKDTree = true;

		// Add the point and the new cluster to the list
		mDataPoints.push_back(nPt);
		mDataClusters.push_back(nPtCl);
		nPt->mAddedIdx = mDataPointsSize;
		mDataPointsSize++;

		if (mInitializeAdditionalData != NULL)
			mInitializeAdditionalData(nPtCl);

		// Add point pair to the indexer
		pointIndexer[Match(nCoordinates1, nCoordinates2)] = indicator;

		return nPt;
	}

	// Add a new model to the buffer - update the ps of all points and clusters
	unsigned int TLinkage::AddModel(Homography &homography)
	{
		// If a model already exist in the position, remove it
		if (mModelBufferSize != 0 && mCurrentModelPointer < mModels.size()){
			RemoveModel(mCurrentModelPointer);
		}

		// Add the model to the buffer list
		if (mModelBufferSize == 0) {
			if (mCopyPtsCoords) {
				mModels.push_back(new Homography(homography));
			}
			else {
				mModels.push_back(&homography);
			}
		}
		else {
			if (mCopyPtsCoords) {
				mModels[mCurrentModelPointer] = new Homography(homography);
			}
			else {
				mModels[mCurrentModelPointer] = &homography;
			}
		}

		// Update the preference set of all the points
		for (list<sPtLnk *>::iterator tIter = mDataPoints.begin(); tIter != mDataPoints.end(); tIter++){
			if (mModelBufferSize == 0) {
				(*tIter)->mPreferenceFunction.resize(mCurrentModelPointer + 1);
			}

			(*tIter)->mPreferenceFunction[mCurrentModelPointer] = PreferenceFunction(homography, (*tIter)->mCoord1, (*tIter)->mCoord2);
		}

		// Update the preference set of all the clusters
		for (list<sClLnk *>::iterator tIterCl = mDataClusters.begin(); tIterCl != mDataClusters.end(); tIterCl++)
		{
			if (mModelBufferSize == 0)
				(*tIterCl)->mPreferenceFunction.resize(mCurrentModelPointer + 1);

			bool allTrue = true;

			for (list<sPtLnk *>::iterator tIterPt = (*tIterCl)->mBelongingPts.begin(); tIterPt != (*tIterCl)->mBelongingPts.end(); tIterPt++) {
				if ((*tIterPt)->mPreferenceFunction[mCurrentModelPointer] == 0.f) {
					allTrue = false;
					break;
				}
			}

			// If the model set all to true we need a distance update
			if (allTrue) {
				float preference = 1.f;
				for (list<sPtLnk *>::iterator tIterPt = (*tIterCl)->mBelongingPts.begin(); tIterPt != (*tIterCl)->mBelongingPts.end(); tIterPt++) {
					float val = PreferenceFunction(homography, (*tIterPt)->mCoord1, (*tIterPt)->mCoord2);
					if (val < preference) {
						preference = val;
					}
				}
				(*tIterCl)->mPreferenceFunction[mCurrentModelPointer] = preference;
				for (list<sDist *>::iterator tIterDist = (*tIterCl)->mPairwiseTanimotoDistance.begin(); tIterDist != (*tIterCl)->mPairwiseTanimotoDistance.end(); tIterDist++){
					if (!(*tIterDist)->mToBeUpdated) {
						mDistancesToBeUpdated.push_back((*tIterDist));
						mDistancesToBeUpdatedSize++;
						(*tIterDist)->mToBeUpdated = true;
					}
					// else already added to the update list
				}
			}
			else {
				(*tIterCl)->mPreferenceFunction[mCurrentModelPointer] = 0.f;
			}
		}

		unsigned int toReturn = mCurrentModelPointer;

		// Update the adder pointer
		if (mModelBufferSize == 0)
			++mCurrentModelPointer;
		else
			mCurrentModelPointer = (++mCurrentModelPointer) % mModelBufferSize;


		return toReturn;
	}

	void TLinkage::RemoveModel(unsigned int nModelN){

		assert(nModelN < mModels.size());

		// Check if already removed
		if (mModels[nModelN] == NULL){
			return;
		}

		delete mModels[nModelN];

		mModels[nModelN] = NULL;

		// Update all the pts preference functions
		for (list<sPtLnk *>::iterator tIter = mDataPoints.begin(); tIter != mDataPoints.end(); tIter++){
			(*tIter)->mPreferenceFunction[nModelN] = 0.f;
		}

		// And all the cluster preferences functions
		// Update the preference set of all the clusters
		for (list<sClLnk *>::iterator tIterCl = mDataClusters.begin(); tIterCl != mDataClusters.end(); tIterCl++){
			// If the preferencet of the cluster for the model was true, we need a distance update
			if ((*tIterCl)->mPreferenceFunction[nModelN] > 0.f) {
				(*tIterCl)->mPreferenceFunction[mCurrentModelPointer] = 0.f;
				for (list<sDist *>::iterator tIterDist = (*tIterCl)->mPairwiseTanimotoDistance.begin(); tIterDist != (*tIterCl)->mPairwiseTanimotoDistance.end(); tIterDist++) {
					if (!(*tIterDist)->mToBeUpdated){
						mDistancesToBeUpdated.push_back((*tIterDist));
						mDistancesToBeUpdatedSize++;
						(*tIterDist)->mToBeUpdated = true;
					}
					// else already added to the update list
				}
			}
		}
	}

	const list<sClLnk *> TLinkage::DoTLClusterization(void(*OnProgress)(float))
	{
		float tEps = (float)1.0f / (1.0f + (float)mModels.size()); // Just a distance to see if the tanimoto distance is equal to one
		float tOneEpsDistance = 1.0f - tEps; // Just a distance to see if the tanimoto distance is equal to one

		// Update neighboardhood information
		mKDTree.optimise();

		for (list<sPtLnk *>::iterator tIterPt = mDataPoints.begin(); tIterPt != mDataPoints.end(); ++tIterPt)
		{
			if (!(*tIterPt)->mToBeUpdateKDTree)
				continue;
			// Threadable build Distance list for kdtree
			sPtLnkPointer nPtPointer; nPtPointer.mPtLnk = *tIterPt; nPtPointer.mPtLnk->mAlreadyFound = false;
			list<sPtLnkPointer> kneigh = FindKNearest(nPtPointer, std::numeric_limits<float>::max(), mKNeighboards, (int)mDataPointsSize);

			// Find K-Nearest
			for (list<sPtLnkPointer>::iterator tIter = kneigh.begin(); tIter != kneigh.end(); ++tIter)
			{
				//for(std::vector<sPtLnkPointer>::iterator tIter = nearPtsIdx.begin(); tIter != nearPtsIdx.end(); ++tIter){
				if ((*tIter).mPtLnk->mBelongingCluster == (*tIterPt)->mBelongingCluster)
					continue;

				bool alreadyPresent = false;
				for (list<sDist *>::iterator distIter = (*tIter).mPtLnk->mBelongingCluster->mPairwiseTanimotoDistance.begin();
					distIter != (*tIter).mPtLnk->mBelongingCluster->mPairwiseTanimotoDistance.end();
					distIter++){

					if ((*distIter)->mCluster1 == (*tIterPt)->mBelongingCluster || (*distIter)->mCluster2 == (*tIterPt)->mBelongingCluster)
						alreadyPresent = true;
				}

				if (alreadyPresent)
					continue;

				sDist *tDist = new sDist;
				tDist->mCluster1 = (*tIterPt)->mBelongingCluster;
				tDist->mCluster2 = (*tIter).mPtLnk->mBelongingCluster;

				(*tIterPt)->mBelongingCluster->mPairwiseTanimotoDistance.push_back(tDist);
				(*tIter).mPtLnk->mBelongingCluster->mPairwiseTanimotoDistance.push_back(tDist);

				tDist->mToBeUpdated = true;
				mDistancesToBeUpdated.push_back(tDist);
				mDistancesToBeUpdatedSize++;
			}

			(*tIterPt)->mToBeUpdateKDTree = false;
		}

		if (OnProgress != NULL)
			OnProgress(0.01f);

		// First step: update all the pw distances that needs an update
		// Please Note: If a distance don't need to be updated it means that it would be certainly equal to 1 from the previous JLClusterization
		// --> Store also a list with ALL the pairwise unique jaccard distance
		vector<sDist *> mHeapDistanceList(mDistancesToBeUpdatedSize);

		unsigned int counter = 0;
		while (mDistancesToBeUpdatedSize > 0)
		{
			sDist *tDist = mDistancesToBeUpdated.back();
			mDistancesToBeUpdated.pop_back();
			mDistancesToBeUpdatedSize--;
			tDist->mPairwiseTanimotoDistance =
				PFTanimotoDist(tDist->mCluster1->mPreferenceFunction,
				tDist->mCluster2->mPreferenceFunction);
			tDist->mToBeUpdated = false;
			if (tDist->mPairwiseTanimotoDistance < tOneEpsDistance){
				mHeapDistanceList[counter] = tDist;
				++counter;
			}
		}

		if (OnProgress != NULL)
			OnProgress(0.02f);

		mHeapDistanceList.resize(counter);

		// A distance that will invalidate the heap, needing a heap resort
		float mCurrentInvalidatingDistance = 1.0f;

		// Make the heap
		std::sort(mHeapDistanceList.begin(), mHeapDistanceList.end(), sDist());

		unsigned int currentSortIdx = 0;
		unsigned int initialDistance = (unsigned int)mHeapDistanceList.size();

		while (mHeapDistanceList.size() > 0)
		{
			sDist *sCurrentMinDist = NULL;
			if (currentSortIdx < mHeapDistanceList.size())
				sCurrentMinDist = mHeapDistanceList[currentSortIdx];
			// TODO speed up previous line!

			if (sCurrentMinDist == NULL || sCurrentMinDist->mPairwiseTanimotoDistance > tOneEpsDistance && mCurrentInvalidatingDistance > tOneEpsDistance)
			{
				// All the distance will be equals to one - clusterization is finished
				// We've finished since all distances have been processed or are equal to 1.0f
				mHeapDistanceList.clear();
			}
			else if (sCurrentMinDist->mCluster1 == NULL || sCurrentMinDist->mCluster2 == NULL)
			{
				// Eliminate the non-valid distance(belong to an eliminated clusters
				for (std::vector<sDist *>::iterator tIterDist = mHeapDistanceList.begin() + currentSortIdx + 1; tIterDist != mHeapDistanceList.end(); tIterDist++){
					assert((*tIterDist) != sCurrentMinDist);
				}
				delete sCurrentMinDist;
				++currentSortIdx;
			}
			else if (sCurrentMinDist->mPairwiseTanimotoDistance > (mCurrentInvalidatingDistance - tEps))
			{
				// We need an heap resort
				mHeapDistanceList.erase(mHeapDistanceList.begin(), mHeapDistanceList.begin() + (currentSortIdx));

				if (mHeapDistanceList.size() > 1){

					// First eliminate all the distance equals to one from the heap
					// Push all these distances to the end of the vector...
					unsigned int idxElementProcessing = 0;
					unsigned int idxLastUsefullElement = (unsigned int)mHeapDistanceList.size() - 1;

					while (idxElementProcessing <= idxLastUsefullElement && idxLastUsefullElement > 0)
					{
						if (mHeapDistanceList[idxElementProcessing]->mPairwiseTanimotoDistance > tOneEpsDistance || mHeapDistanceList[idxElementProcessing]->mCluster1 == NULL || mHeapDistanceList[idxElementProcessing]->mCluster2 == NULL){
							// In this case we need to move the distance to the end

							// Swap elements
							sDist *temp = mHeapDistanceList[idxElementProcessing];
							mHeapDistanceList[idxElementProcessing] = mHeapDistanceList[idxLastUsefullElement];
							mHeapDistanceList[idxLastUsefullElement] = temp;

							// If the distance belongs to one eliminated cluster, delete it
							if (mHeapDistanceList[idxLastUsefullElement]->mCluster1 == NULL || mHeapDistanceList[idxLastUsefullElement]->mCluster2 == NULL)
								delete mHeapDistanceList[idxLastUsefullElement];

							idxLastUsefullElement--;
						}
						else{
							idxElementProcessing++;
						}
					}

					if (idxLastUsefullElement > 0){
						// ... and then erase them
						if (idxLastUsefullElement < mHeapDistanceList.size() - 1)
							mHeapDistanceList.erase(mHeapDistanceList.begin() + idxLastUsefullElement + 1, mHeapDistanceList.end());
						// Re-Set the heap
						std::sort(mHeapDistanceList.begin(), mHeapDistanceList.end(), sDist());
					}
					else{
						// Ok we finished
						mHeapDistanceList.clear();
					}

				}

				mCurrentInvalidatingDistance = 2.0f;
				currentSortIdx = 0;
			}
			else {
				// The distance is less than the invalidating distance, merge the two cluster and update the other distances accordingly

				// if kd-tree is used, merge the distances of the clusters for adding the new ones of the new created cluster
				list<sClLnk *> distancesToBeAdded;

				sCurrentMinDist->mCluster2->mPairwiseTanimotoDistance.remove(sCurrentMinDist);
				sCurrentMinDist->mCluster1->mPairwiseTanimotoDistance.remove(sCurrentMinDist);

				for (list<sDist *>::iterator distIter1 = sCurrentMinDist->mCluster2->mPairwiseTanimotoDistance.begin(); distIter1 != sCurrentMinDist->mCluster2->mPairwiseTanimotoDistance.end(); ++distIter1){
					// Threadable Check distance to merge KD-Tree
					bool add = true;
					// Check if the distance already exists
					for (std::list<sDist *>::iterator distIter2 = sCurrentMinDist->mCluster1->mPairwiseTanimotoDistance.begin(); distIter2 != sCurrentMinDist->mCluster1->mPairwiseTanimotoDistance.end(); ++distIter2){
						if ((*distIter1)->mCluster1 == (*distIter2)->mCluster1 || (*distIter1)->mCluster2 == (*distIter2)->mCluster2
							|| (*distIter1)->mCluster2 == (*distIter2)->mCluster1 || (*distIter1)->mCluster1 == (*distIter2)->mCluster2){
							add = false; // Point already present
							break;
						}
					}
					if (add){
						if ((*distIter1)->mCluster1 != sCurrentMinDist->mCluster2)
							distancesToBeAdded.push_back((*distIter1)->mCluster1);
						else if ((*distIter1)->mCluster2 != sCurrentMinDist->mCluster2)
							distancesToBeAdded.push_back((*distIter1)->mCluster2);
					}

				}


				// Update the cluster pointer of all the points of the deleted cluster
				for (std::list<sPtLnk *>::iterator ptIter = sCurrentMinDist->mCluster2->mBelongingPts.begin(); ptIter != sCurrentMinDist->mCluster2->mBelongingPts.end(); ptIter++)
					(*ptIter)->mBelongingCluster = sCurrentMinDist->mCluster1;


				// Merge the two clusters into cluster 1
				for (unsigned int i = 0; i < sCurrentMinDist->mCluster1->mPreferenceFunction.size(); i++) {
					sCurrentMinDist->mCluster1->mPreferenceFunction[i] =
						sCurrentMinDist->mCluster1->mPreferenceFunction[i] < sCurrentMinDist->mCluster2->mPreferenceFunction[i] ?
						sCurrentMinDist->mCluster1->mPreferenceFunction[i] : sCurrentMinDist->mCluster2->mPreferenceFunction[i];
				}
				sCurrentMinDist->mCluster1->mBelongingPts.merge(sCurrentMinDist->mCluster2->mBelongingPts);

				// Delete cluster 2
				mDataClusters.remove(sCurrentMinDist->mCluster2);

				for (std::list<sClLnk *>::iterator clIter = distancesToBeAdded.begin(); clIter != distancesToBeAdded.end(); ++clIter)
				{
					// Threadable Add kd-tree distances
					sDist *tDist = new sDist;
					tDist->mCluster1 = sCurrentMinDist->mCluster1;
					tDist->mCluster2 = *clIter;

					sCurrentMinDist->mCluster1->mPairwiseTanimotoDistance.push_back(tDist);
					(*clIter)->mPairwiseTanimotoDistance.push_back(tDist);
					mHeapDistanceList.push_back(tDist);
				}

				// Update all the distances of the old cluster -- delete
				for (std::list<sDist *>::iterator tIterDist = sCurrentMinDist->mCluster2->mPairwiseTanimotoDistance.begin(); tIterDist != sCurrentMinDist->mCluster2->mPairwiseTanimotoDistance.end(); tIterDist++)
				{
					// Threadable: delete distance for the old cluster
					if (sCurrentMinDist != (*tIterDist)){
						if ((*tIterDist)->mCluster1 != sCurrentMinDist->mCluster2)
							(*tIterDist)->mCluster1->mPairwiseTanimotoDistance.remove((*tIterDist));
						else if ((*tIterDist)->mCluster2 != sCurrentMinDist->mCluster2)
							(*tIterDist)->mCluster2->mPairwiseTanimotoDistance.remove((*tIterDist));
						(*tIterDist)->mCluster1 = NULL;
						(*tIterDist)->mCluster2 = NULL;
					}
				}

				// Do additional user-defined update steps if specified
				if (mClusterMergeAdditionalOperation != NULL)
					mClusterMergeAdditionalOperation(sCurrentMinDist->mCluster1);

#ifndef JL_NO_THREADS
				boost::thread_group *tg = new boost::thread_group();
				// Update all the distances of the new cluster
				for (std::list<sDist *>::iterator tIterDist = sCurrentMinDist->mCluster1->mPairwiseTanimotoDistance.begin(); tIterDist != sCurrentMinDist->mCluster1->mPairwiseTanimotoDistance.end(); tIterDist++){
					// Update distances
					tg->create_thread(boost::bind(&(UpdateDistance), *tIterDist, &mCurrentInvalidatingDistance, mClusterClusterDiscardTest));
					if (tg->size() >= MAXTHREADS){
						tg->join_all();
						delete tg;
						tg = new boost::thread_group();
					}
				}
				tg->join_all();
				delete tg;
#else
				for (std::list<sDist *>::iterator tIterDist = sCurrentMinDist->mCluster1->mPairwiseJaccardDistance.begin(); tIterDist != sCurrentMinDist->mCluster1->mPairwiseJaccardDistance.end(); tIterDist++){
					// Update distances
					JLinkage::UpdateDistance(*tIterDist, &mCurrentInvalidatingDistance, mClusterClusterDiscardTest);
				}

#endif

				if (OnProgress != NULL)
					OnProgress(1.0f - ((float)(mHeapDistanceList.size() - currentSortIdx) / (float)initialDistance));

				// Delete old cluster
				if (sCurrentMinDist->mCluster2 && mDestroyAdditionalData)
					mDestroyAdditionalData(sCurrentMinDist->mCluster2);

				delete sCurrentMinDist->mCluster2;
				delete sCurrentMinDist;

				++currentSortIdx;

			}
		}

		if (OnProgress != NULL)
			OnProgress(1.0f);

		// return the list of clusters
		return mDataClusters;
	}

	void TLinkage::UpdateDistance(sDist *tIterDist, float *nCurrentInvalidatingDistance,
		bool(*nClusterClusterDiscardTest)(const sClLnk *, const sClLnk *))
	{
		tIterDist->mPairwiseTanimotoDistance =
			PFTanimotoDist(tIterDist->mCluster1->mPreferenceFunction,
			tIterDist->mCluster2->mPreferenceFunction);

		// If pw distance is < 1, Do also additional test if a function is specified
		if (tIterDist->mPairwiseTanimotoDistance < 1.0f
			&& (nClusterClusterDiscardTest != NULL && !nClusterClusterDiscardTest(tIterDist->mCluster1, tIterDist->mCluster2)))
			tIterDist->mPairwiseTanimotoDistance = 1.0f;

#ifndef JL_NO_THREADS
		boost::mutex::scoped_lock
			lock(mMutexMCurrentInvalidatingDistance);
#endif
		// Store the minimum updated distance
		if (tIterDist->mPairwiseTanimotoDistance < *nCurrentInvalidatingDistance)
			*nCurrentInvalidatingDistance = tIterDist->mPairwiseTanimotoDistance;
	}

	// Find K-nearest neighboard from the kd-tree
	list<sPtLnkPointer> TLinkage::FindKNearest(sPtLnkPointer __V, float const __Max_R, int k, int nMaxSize){
		int msSizePRAF = 0;
		std::list<sPtLnkPointer> msAlreadyFoundPRAF;
		__V.mPtLnk->mAlreadyFound = true;
		while (msSizePRAF < k && msSizePRAF < nMaxSize){
			std::pair<KDTree::KDTree<sPtLnkPointer>::iterator, float> nif = mKDTree.find_nearest_if(__V, __Max_R, sPredicateAlreadyFoundJL());
			msAlreadyFoundPRAF.push_back(*nif.first);
			msAlreadyFoundPRAF.back().mPtLnk->mAlreadyFound = true;
			msSizePRAF++;
		}
		// Reset values
		for (std::list<sPtLnkPointer>::iterator it = msAlreadyFoundPRAF.begin(); it != msAlreadyFoundPRAF.end(); ++it)
			(*it).mPtLnk->mAlreadyFound = false;
		__V.mPtLnk->mAlreadyFound = false;

		return msAlreadyFoundPRAF;
	}

	// given two Preference function Vectors compute the tanimoto distance
	float PFTanimotoDist(const vector<float> &nB1, const vector<float> &nB2)
	{
		assert(nB1.size() == nB2.size());

		float dot_prod = 0.f;
		float B1square = 0.f;
		float B2square = 0.f;

		for (unsigned int i = 0; i < nB1.size(); i++) {
			dot_prod += nB1[i] * nB2[i];
			B1square += nB1[i] * nB1[i];
			B2square += nB2[i] * nB2[i];
		}

		return 1.0f - dot_prod / (B1square + B2square - dot_prod);
	}

	// Debug functions
	Mat TLinkage::drawPointTestVanishingLine(Mat image, int normal_index)
	{
		Mat gray;

		cv::cvtColor(image, gray, CV_BGR2GRAY);
		cv::cvtColor(gray, gray, CV_GRAY2BGR);

		std::unordered_map<Match, unsigned char, MatchHash>::iterator iter = pointIndexer.begin();
		while (iter != pointIndexer.end()) {
			if (iter->second >> normal_index & 1) {
				cv::circle(gray, iter->first.first, 2, CV_RGB(255, 0, 0), 2);
			}
			else {
				cv::circle(gray, iter->first.first, 2, CV_RGB(0, 255, 0), 2);
			}
			iter++;
		}

		return gray;
	}
}
