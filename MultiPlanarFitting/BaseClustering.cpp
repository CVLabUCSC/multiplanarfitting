#include "BaseClustering.h"
#include "ClusterUtils.h"

#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include <map>

#include <opencv2\highgui.hpp>
#include <opencv2\xfeatures2d\nonfree.hpp>

#include <boost\filesystem.hpp>

using std::string;
using std::ifstream;
using std::vector;
using std::map;
using std::to_string;
using std::cout;
using std::endl;

using cv::Mat;
using cv::Point2f;
using cv::Ptr;
using cv::KeyPoint;
using cv::xfeatures2d::SIFT;
using cv::FlannBasedMatcher;
using cv::DMatch;
using cv::imread;
using cv::imwrite;
using cv::cvtColor;

using IOUtils::TwoViewParamPathes;
using IOUtils::showMessage;

static const double	NN_SQ_DIST_RATIO_THR = 0.49;

BaseClustering::BaseClustering()
{
}

BaseClustering::BaseClustering(const cv::Mat &image1, const cv::Mat &image2, const std::vector<cv::Point2f> &points1,
	const std::vector<cv::Point2f> &points2)
{
	images[0] = image1.clone();
	images[1] = image2.clone();
	m_points1 = points1;
	m_points2 = points2;
}

BaseClustering::~BaseClustering()
{
}

// Compute matches with SIFT detector and FLANN matcher
bool BaseClustering::computeMatches()
{
	// clear point matches
	m_points1.clear();
	m_points2.clear();

	Mat grayImg1, grayImg2;
	cvtColor(images[0], grayImg1, CV_BGR2GRAY);
	cvtColor(images[1], grayImg2, CV_BGR2GRAY);

	// Compute keypoints and descriptors for first image
	Ptr<SIFT> detector = SIFT::create();
	vector<KeyPoint> keypoints1, keypoints2;
	Mat descriptors1, descriptors2;
	detector->detect(grayImg1, keypoints1);
	detector->compute(grayImg1, keypoints1, descriptors1);

	// Compute keypoints and descriptors for second image
	detector->detect(grayImg2, keypoints2);
	detector->compute(grayImg2, keypoints2, descriptors2);

	// Match features
	FlannBasedMatcher matcher;
	vector<vector<DMatch>> matches;
	matcher.knnMatch(descriptors1, descriptors2, matches, 2);

	for (int i = 0; i < (int)matches.size(); i++)
	{
		if (matches[i].size() == 1) {
			m_points1.push_back(keypoints1[matches[i][0].queryIdx].pt);
			m_points2.push_back(keypoints2[matches[i][0].trainIdx].pt);
		}
		else if (matches[i].size() == 2) {
			if (matches[i][0].distance <= matches[i][1].distance * NN_SQ_DIST_RATIO_THR) {
				m_points1.push_back(keypoints1[matches[i][0].queryIdx].pt);
				m_points2.push_back(keypoints2[matches[i][0].trainIdx].pt);
			}
		}
	}

	//imwrite("matches.png", drawMatchesOnImage());

	detector.release();

	return true;
}

// Generate ground-truth from given image file
bool BaseClustering::GenerateGroundTruth(const cv::Mat &ground_truth_img, double threshold)
{
	map<uchar, int> labels;
	int label = 0;

	// Find unique labels
	for (int pi = 0; pi < m_points1.size(); pi++) {
		uchar label_current = (int)ground_truth_img.at<uchar>(m_points1[pi]);
		if (labels.find(label_current) == labels.end()) {
			if (label_current == 255) {
				labels[label_current] = -1;
			}
			else {
				labels[label_current] = label;
				label++;
			}
		}
	}

	vector<Cluster> clusters(label);

	// Add points to clusters
	for (size_t pi = 0; pi < m_points1.size(); pi++)
	{
		int ind = labels[ground_truth_img.at<uchar>(m_points1[pi])];

		if (ind < 0) {
			continue;
		}

		clusters[ind].points1.push_back(m_points1[pi]);
		clusters[ind].points2.push_back(m_points2[pi]);
	}
	
	// Make sure that each cluster contains at least four points
	for (vector<Cluster>::iterator iter = clusters.begin(); iter != clusters.end(); )
	{
		if (iter->size() < 4) {
			iter = clusters.erase(iter);
		}
		else {
			iter++;
		}
	}

	// Generate ground-truth
	m_clusterGroundTruth = ClusterUtils::clusterArrayToClusters(clusters, m_points1, m_points2);

	// Generate achievable ground-truth
	m_clusterAGroundTruth = ClusterUtils::clusterArrayToClusters(ClusterUtils::computeInliers(clusters, threshold), m_points1, m_points2);
	//m_clusterAGroundTruth = ClusterUtils::computeUniqueInliers(clusters, m_points1, m_points2, threshold);

	return true;
}

bool BaseClustering::GenerateGroundTruth(const cv::Mat &ground_truth_img, const HomographyEstimator &estimator, double threshold)
{
	map<uchar, int> labels;
	int num_label = 0;
	int normal_index;

	// Find unique labels
	for (int pi = 0; pi < m_points1.size(); pi++) {
		uchar label_current = (int)ground_truth_img.at<uchar>(m_points1[pi]);
		if (labels.find(label_current) == labels.end()) {
			if (label_current == 255) {
				labels[label_current] = -1;
			}
			else {
				labels[label_current] = num_label;
				num_label++;
			}
		}
	}

	vector<Cluster> clusters(num_label);

	for (map<uchar, int>::iterator iter = labels.begin(); iter != labels.end(); iter++) {
		if (iter->second == OUTLIER_CLUSTER_INDEX) {
			continue;
		}

		if (iter->first < 100) {
			clusters[iter->second].normal_index = estimator.getNormalIndex('x');
		}
		else if (iter->first < 200) {
			clusters[iter->second].normal_index = estimator.getNormalIndex('y');
		}
		else if (iter->first < 250) {
			clusters[iter->second].normal_index = estimator.getNormalIndex('z');
		}
	}

	// Add points to clusters
	for (unsigned int pi = 0; pi < m_points1.size(); pi++)
	{
		uchar label = ground_truth_img.at<uchar>(m_points1[pi]);
		int ind = labels[label];

		if (ind < 0) {
			continue;
		}

		clusters[ind].points1.push_back(m_points1[pi]);
		clusters[ind].points2.push_back(m_points2[pi]);
	}

	// Make sure that each cluster contains at least four points
	for (vector<Cluster>::iterator iter = clusters.begin(); iter != clusters.end();)
	{
		if (iter->size() < 4) {
			iter = clusters.erase(iter);
		}
		else {
			iter++;
		}
	}

	// Generate ground-truth
	m_clusterGroundTruth = ClusterUtils::clusterArrayToClusters(clusters, m_points1, m_points2);

	// Generate achievable ground-truth with constrained homography
	estimator.computeParameters(clusters);
	m_clusterAGroundTruth = ClusterUtils::clusterArrayToClusters(ClusterUtils::computeInliers(clusters, threshold), m_points1, m_points2);

	//m_clusterAGroundTruth = ClusterUtils::computeInliers(clusters, estimator, threshold);
	//m_clusterAGroundTruth = ClusterUtils::computeUniqueInliers(clusters, estimator, m_points1, m_points2, threshold);

	return true;
}

// Generate homoraphies using given homography estimator
bool BaseClustering::GenerateModels(vector<Mat *> *&models, HomographyEstimator &estimator)
{
	int minimumSampleSet;
	SamplingParams params;
	unsigned int num_of_samples = m_points1.size() * 20;	// the number of models to be sampled

	switch (estimator.getModelType())
	{
	case HOMOGRAPHY_MANHATTAN_WORLD:
		minimumSampleSet = 2;
		break;
	case HOMOGRAPHY_WEAK_MANHATTAN_WORLD:
		minimumSampleSet = 3;
		break;
	case HOMOGRAPHY_UNCONSTRAINED:
	default:
		minimumSampleSet = 4;
	}

	RandomSampler mRandomSampler(2, minimumSampleSet, m_points1.size());

	// Add points to the random sampler
	mRandomSampler.SetPoints(m_points1, m_points2);

	// Initialize the sampling probabilities
	if (params.FirstSamplingVector != NULL)
		for (unsigned int i = 0; i < m_points1.size(); i++)
			mRandomSampler.SetFirstSamplingProb(i, (float)params.FirstSamplingVector[i]);

	// Initialize non-first sampling type
	switch (params.NFSamplingType)
	{
	case NFST_EXP:
		mRandomSampler.SetNFSamplingTypeExp((float)params.SigmaExp);
		break;

	case NFST_NN:
		mRandomSampler.SetNFSamplingTypeNN(params.KdTreeRange, params.KdTreeCloseProb, params.KdTreeFarProb, false);
		break;

	case NFST_NN_ME:
		mRandomSampler.SetNFSamplingTypeNN(params.KdTreeRange, params.KdTreeCloseProb, params.KdTreeFarProb, true);
		break;
	}

	// Set homography generator
	mRandomSampler.setModelEstimator(estimator);

	// Generate models
	vector<Mat *> *mModels;
	if (estimator.getModelType() == HOMOGRAPHY_MANHATTAN_WORLD)
		models = mRandomSampler.GetNSample(num_of_samples, 0, true);
	else
		models = mRandomSampler.GetNSample(num_of_samples, 0, false);

	return true;
}

bool BaseClustering::GenerateModels(std::vector<HomographyModel> &models, HomographyEstimator &estimator, int num_of_models)
{
	int minimumSampleSet;
	SamplingParams params;

	//params.NFSamplingType = NFST_EXP;

	switch (estimator.getModelType())
	{
	case HOMOGRAPHY_MANHATTAN_WORLD:
		minimumSampleSet = 2;
		break;
	case HOMOGRAPHY_WEAK_MANHATTAN_WORLD:
		minimumSampleSet = 3;
		break;
	case HOMOGRAPHY_UNCONSTRAINED:
	default:
		minimumSampleSet = 4;
	}

	RandomSampler mRandomSampler(2, minimumSampleSet, m_points1.size());

	// Add points to the random sampler
	mRandomSampler.SetPoints(m_points1, m_points2);

	// Initialize the sampling probabilities
	if (params.FirstSamplingVector != NULL)
		for (unsigned int i = 0; i < m_points1.size(); i++)
			mRandomSampler.SetFirstSamplingProb(i, (float)params.FirstSamplingVector[i]);

	// Initialize non-first sampling type
	switch (params.NFSamplingType)
	{
	case NFST_EXP:
		mRandomSampler.SetNFSamplingTypeExp((float)params.SigmaExp);
		break;

	case NFST_NN:
		mRandomSampler.SetNFSamplingTypeNN(params.KdTreeRange, params.KdTreeCloseProb, params.KdTreeFarProb, false);
		break;

	case NFST_NN_ME:
		mRandomSampler.SetNFSamplingTypeNN(params.KdTreeRange, params.KdTreeCloseProb, params.KdTreeFarProb, true);
		break;
	}

	// Generate random indices
	vector<vector<unsigned int>> samples;
#ifdef _DEBUG
	num_of_models = (int)m_points1.size() - minimumSampleSet + 1;
#endif

	mRandomSampler.GetNSample(samples, num_of_models);

	// Generate models
	for (int i = 0; i < num_of_models; i++) {
		vector<cv::Point2f> points1, points2;
		for (int j = 0; j < minimumSampleSet; j++) {
			points1.push_back(m_points1[samples[i][j]]);
			points2.push_back(m_points2[samples[i][j]]);
		}

		// Generate homography based on its homograpy type
		if (estimator.getModelType() == HOMOGRAPHY_MANHATTAN_WORLD) {
			estimator.findMWHomographies(points1, points2, models);
		}
		else {
			models.push_back(HomographyModel(estimator.getHomography(points1, points2)));
		}
	}

	return true;
}

// Compute resiaul of each point w.r.t. given homography
int BaseClustering::computeResiduals(vector<double> &residuals, const Mat &homography, double threshold)
{
	int num_of_inliers = 0;
	vector<Point2f> points2_proj;

	if (residuals.size() != 0)
		residuals.clear();

	// Recalculate inliers for all correspondences
	perspectiveTransform(m_points1, points2_proj, homography);

	// Calculate average differences and the number of inliers
	for (int pi = 0; pi < (int)m_points1.size(); pi++) {
		residuals.push_back(norm(points2_proj[pi] - m_points2[pi]));

		if (threshold >= 0.0 && residuals.back() < threshold) {
			num_of_inliers++;
		}
	}

	return num_of_inliers;
}

// Load point correspondences from given file
bool BaseClustering::loadMatches(const string &filename)
{
	// Validation
	if (filename.empty())
		return false;

	ifstream fin;

	fin.open(filename, ifstream::in);
	int num;
	Point2f pt1, pt2;

	if (!fin.is_open())
	{
		cout << "Not able to open the match data file: " << strerror(errno) << endl;;
		return false;
	}

	fin >> num;

	for (int i = 0; i < num; i++)
	{
		fin >> pt1.x >> pt1.y >> pt2.x >> pt2.y;
		m_points1.push_back(pt1);
		m_points2.push_back(pt2);
	}

	fin.close();

	return true;
}

// Load parameters from given file pathes. It will load mendatory parameters first.
bool BaseClustering::loadParameters(const TwoViewParamPathes &twoViewParamPathes)
{
	// load images
	images[0] = imread(twoViewParamPathes.pathImg1);
	images[1] = imread(twoViewParamPathes.pathImg2);
	if (images[0].empty() || images[1].empty())
	{
		showMessage("Not able to load image files.");
		return false;
	}
	
	// load matched feature points
	if (!loadMatches(twoViewParamPathes.pathMatches))
		computeMatches();

	// Load ground-truth clusters
	if (!twoViewParamPathes.pathGTCluster.empty())
	{
		string ext = boost::filesystem::path(twoViewParamPathes.pathGTCluster).extension().string();

		if (ext.compare(".txt") == 0) {
			cout << "Load ground-truth cluters from text file" << endl;
			m_clusterGroundTruth = IOUtils::loadClusters(twoViewParamPathes.pathGTCluster);
		}
		else if (ext.compare(".jpg") == 0 || ext.compare(".png") == 0) {
			if (!GenerateGroundTruth(imread(twoViewParamPathes.pathGTCluster, 0), 2.5)) {
				showMessage("Failed to initialize ground-truth clusters from labels");
			}
		}
		else {
			showMessage("Invalid ground truth file extension: " + ext);
		}

#ifdef _DEBUG
		// test the ground-truth cluster if all of the points are in the gt cluster
		for (int i = 0; i < m_points1.size(); i++) {
			int index = m_clusterGroundTruth.getClusterIndex(Match(m_points1[i], m_points2[i]));
			if (index == POINT_DOES_NOT_EXIST) {
				showMessage(to_string(i) + "'th point does not exist in ground-truth clusters: ");
			}
		}
#endif
	}
}

bool BaseClustering::loadGTClusters(const string path, double threshold)
{
	if (path.empty()) {
		return false;
	}

	string ext = boost::filesystem::path(path).extension().string();

	if (ext.compare(".txt") == 0) {
		cout << "Load ground-truth cluters from text file" << endl;
		m_clusterGroundTruth = IOUtils::loadClusters(path);
	}
	else if (ext.compare(".jpg") == 0 || ext.compare(".png") == 0) {
		if (!GenerateGroundTruth(imread(path, 0), 2.5)) {
			showMessage("Failed to initialize ground-truth clusters from labels");
		}
	}
	else {
		showMessage("Invalid ground truth file extension: " + ext);
		return false;
	}

#ifdef _DEBUG
	// test the ground-truth cluster if all of the points are in the gt cluster
	for (int i = 0; i < m_points1.size(); i++) {
		int index = m_clusterGroundTruth.getClusterIndex(Match(m_points1[i], m_points2[i]));
		if (index == POINT_DOES_NOT_EXIST) {
			showMessage(to_string(i) + "'th point does not exist in ground-truth clusters: ");
		}
	}
#endif

	return true;
}

// Load ground-truth clusters
bool BaseClustering::loadGTClusters(const std::string path, const HomographyEstimator &estimator, double threshold)
{
	if (path.empty()) {
		return false;
	}

	string ext = boost::filesystem::path(path).extension().string();

	if (ext.compare(".txt") == 0) {
		cout << "Load ground-truth cluters from text file" << endl;
		m_clusterGroundTruth = IOUtils::loadClusters(path);
	}
	else if (ext.compare(".jpg") == 0 || ext.compare(".png") == 0) {
		if (!GenerateGroundTruth(imread(path, 0), estimator, 2.5)) {
			showMessage("Failed to initialize ground-truth clusters from labels");
		}
	}
	else {
		showMessage("Invalid ground truth file extension: " + ext);
		return false;
	}

#ifdef _DEBUG
	// test the ground-truth cluster if all of the points are in the gt cluster
	for (int i = 0; i < m_points1.size(); i++) {
		int index = m_clusterGroundTruth.getClusterIndex(Match(m_points1[i], m_points2[i]));
		if (index == POINT_DOES_NOT_EXIST) {
			showMessage(to_string(i) + "'th point does not exist in ground-truth clusters: ");
		}
	}
#endif

	return true;
}

void BaseClustering::drawGTClusters(const string resultFolderName, const std::string filename)
{
	if (!m_clusterGroundTruth.empty()) {
		imwrite(resultFolderName + "\\" + filename + ".png", ClusterUtils::DrawPointsOnImage(m_clusterGroundTruth, images[0], true, 2));
	}

	if (!m_clusterAGroundTruth.empty()) {
		imwrite(resultFolderName + "\\" + filename + "-achievable.png", ClusterUtils::DrawPointsOnImage(m_clusterAGroundTruth, images[0], true, 2));
	}
}

bool BaseClustering::checkClusterConsistancy(const Clusters &clusters) const
{
	if (!m_clusterGroundTruth.empty()) {
		return clusters.points() == m_clusterGroundTruth.points();
	}

	if (!m_clusterAGroundTruth.empty()) {
		return clusters.points() == m_clusterGroundTruth.points();
	}

	return false;
}

// compute rand index
double BaseClustering::computeRandIndex(Clusters &clusters, bool useAchievable, bool considerOutliers) const
{
	if (useAchievable) {
		if (m_clusterAGroundTruth.empty()) {
			return -1.0;
		}

		return ClusterUtils::computeAdjustedRandIndex(m_clusterAGroundTruth, clusters, considerOutliers);
	}
	else {
		if (m_clusterGroundTruth.empty()) {
			return -1.0;
		}

		return ClusterUtils::computeAdjustedRandIndex(m_clusterGroundTruth, clusters, considerOutliers);
	}
}

void BaseClustering::Analysis(double &pointPerPlane, int &numOfOutliers, int &numOfPlanes)
{
	pointPerPlane = 0.0;
	for (unsigned int ci = 0; ci < m_clusterGroundTruth.size(); ci++) {
		pointPerPlane += (double)m_clusterGroundTruth[ci].points1.size();
	}
	pointPerPlane /= (double)m_clusterGroundTruth.size();
	numOfOutliers = m_clusterGroundTruth.getOutliers().size();
	numOfPlanes = m_clusterGroundTruth.size();
}