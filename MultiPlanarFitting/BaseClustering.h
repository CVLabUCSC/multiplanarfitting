#pragma once

#include <opencv2\opencv.hpp>

#include "JLinkage\RandomSampler.h"
#include "HomographyEstimator.h"
#include "IOUtils.h"

enum Refinement {
	ITERATIVE_REFINEMENT,
	ITERATIVE_JLINKAGE,
	NORMAL_AWARE_MERGE,
	MULTIPLE_PLANE_DETECTION
};

struct ExperimentParameters {
	// If reprojection error of a correspondence for a projection
	// cv::Matrix is less this value, it will be considered as an inlier.
	double threshold_reprojection_error;

	// If jaccard distance of two clusters is less than this value,
	// they will be merged into one cluster.
	double threshold_merge_jaccard_dist;

	// Do rejection of outlier cluster. If this value set to true,
	// then clusters with a small number of points will be removed.
	bool do_outlier_rejection;

	// Do point normalization to alleviate biased data problem.
	bool do_point_normalization;

	// The maximum number of iteration for iterative refinement.
	int num_max_refine_iteration;

	// The type of clustering. This value could be one of followings:
	// CLUSTERING_SEQ_RANSAC: Clustering with sequencial RANSAC;
	// CLUSTERING_CC_RANSAC: Clustering with CC-RANSAC;
	// CLUSTERING_SA_RCM: Clustering with SA-RCM;
	// CLUSTERING_J_LINKAGE: Clustering with J-Linkage;
	int clustering_type;

	// If set to true, use constrained homography to calculate clusters.
	// Ohterwise, use unconstrained homography.
	HOMOGRAPHY_TYPE homography_type;

	Refinement type_refinement;

	// Default constructor
	ExperimentParameters() {
		threshold_reprojection_error = 2.5;
		threshold_merge_jaccard_dist = 0.7;
	}
};

struct SamplingParams
{
	// Non first sampling type: 0 - Uniform(def) 1 - Exp 2 - Kd-Tree
	RS_NFSAMPLINGTYPE NFSamplingType;

	// Sigma Exp(def = 1.0)
	double SigmaExp;

	// neighbor search for Kd-Tree (def = 10)
	int KdTreeRange;

	// only for kd-tree non first sampling: close points probability (def = 0.8)
	float KdTreeCloseProb;

	// Only for kd-tree non first sampling: far points probability (def = 0.2)
	float KdTreeFarProb;

	float *FirstSamplingVector;

	SamplingParams() : NFSamplingType(NFST_UNIFORM), SigmaExp(1.0), KdTreeRange(10), KdTreeCloseProb(0.8f), FirstSamplingVector(NULL)
	{
		KdTreeFarProb = 1.0f - KdTreeCloseProb;
	};
};

class BaseClustering
{
private:

	// Compute matches with SIFT detector and FLANN matcher
	bool computeMatches();

	// Load pre-computed features from file
	bool loadMatches(const std::string &filename);

	// Generate ground-truth from given image file. If threshold is set to greater than zero,
	// it will generate achievable ground truth
	bool GenerateGroundTruth(const cv::Mat &ground_truth_img, double threshold);

	// Generate ground-truth from given image file. If threshold is set to greater than zero,
	// it will generate achievable ground truth
	bool GenerateGroundTruth(const cv::Mat &ground_truth_img, const HomographyEstimator &estimator, double threshold);

protected:

	cv::Mat images[2];

	// Point correspondences
	std::vector<cv::Point2f> m_points1, m_points2;	// Original point matches

	// Clustered point matches
	Clusters m_clusters;

	// Ground-truth clusters
	Clusters m_clusterGroundTruth;

	// Achievable Ground-truth clusters
	Clusters m_clusterAGroundTruth;

	// Output folder name to store the results
	std::string mResultFolderName;

	// Generate homographies using given homography estimator
	bool GenerateModels(std::vector<cv::Mat *> *&models, HomographyEstimator &estimator);

	// Generate homographies using given homography estimator
	bool GenerateModels(std::vector<HomographyModel> &models, HomographyEstimator &estimator, int num_of_models);

	// Compute resiaul of each point w.r.t. given homography
	// If threshold is set the greater than or equal to zero,
	// it will return the number of inliers
	int computeResiduals(std::vector<double> &residuals, const cv::Mat &homography, double threshold = -1.0);

public:
	BaseClustering();

	BaseClustering(const cv::Mat &image1, const cv::Mat &image2, const std::vector<cv::Point2f> &points1,
		const std::vector<cv::Point2f> &points2);

	~BaseClustering();

	// Load parameters from given structur of data pathes
	bool loadParameters(const IOUtils::TwoViewParamPathes &twoViewParamPathes);

	// Load ground-truth clusters
	bool loadGTClusters(const std::string path, double threshold = 3.0);

	// Load ground-truth clusters
	bool loadGTClusters(const std::string path, const HomographyEstimator &estimator, double threshold = 3.0);

	// Load ground-truth clusters

	// draw ground-truth clusters
	void drawGTClusters(const std::string path, const std::string filename = "ground-truth");

	bool checkClusterConsistancy(const Clusters &clusters) const;

	// compute rand index
	double computeRandIndex(Clusters &clusters, bool useAchievable, bool considerOutliers = true) const;

	void Analysis(double &pointPerPlane, int &numOfOutliers, int &numOfPlanes);
};

