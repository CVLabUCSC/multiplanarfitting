#include "ClusterUtils.h"
#include "HomographyEstimator.h"
#include "utility.h"

#include <iomanip>
#include <unordered_set>

#include <opencv2\xfeatures2d\nonfree.hpp>

#include <boost\math\distributions\normal.hpp>

using std::vector;
using std::string;
using std::fstream;
using std::cout;
using std::endl;

using cv::Point;
using cv::Point2f;
using cv::Mat;
using cv::Scalar;
using cv::RNG;
using cv::imwrite;
using cv::cvtColor;
using cv::Size;
using cv::Mat;
using cv::Rect;
using cv::norm;
using cv::findHomography;
using cv::Ptr;
using cv::KeyPoint;
using cv::xfeatures2d::SIFT;
using cv::FlannBasedMatcher;
using cv::DMatch;


PointPairCluster::PointPairCluster()
{

}

PointPairCluster::PointPairCluster(int size)
{
	point1.resize(size);
	point2.resize(size);
}

size_t PointPairCluster::size()
{
	return point1.size();
}

namespace ClusterUtils
{

	using boost::math::normal;
	using boost::math::quantile;

	using std::unordered_set;

	vector<vector<unsigned int>> pickRandomPoints(vector<Point2f> points1, vector<Point2f> points2,
		int numOfSamples, const int minimumSampleSet, RS_NFSAMPLINGTYPE samplingType)
	{
		RandomSampler mRandomSampler(2, minimumSampleSet, points1.size());
		float SigmaExp = 1.0;
		int KdTreeRange = 10;
		float KdTreeCloseProb = 0.8f;
		float KdTreeFarProb = 1.0f - KdTreeCloseProb;

		// Add points to the random sampler
		mRandomSampler.SetPoints(points1, points2);

		// Initialize non-first sampling type
		switch (samplingType)
		{
		case NFST_EXP:
			mRandomSampler.SetNFSamplingTypeExp(SigmaExp);
			break;

		case NFST_NN:
			mRandomSampler.SetNFSamplingTypeNN(KdTreeRange, KdTreeCloseProb, KdTreeFarProb, false);
			break;

		case NFST_NN_ME:
			mRandomSampler.SetNFSamplingTypeNN(KdTreeRange, KdTreeCloseProb, KdTreeFarProb, true);
			break;
		}

		// Generate random indices
		vector<vector<unsigned int>> samples;

		mRandomSampler.GetNSample(samples, numOfSamples);

		return samples;
	}

	vector<HomographyModel> generateModels(const HomographyEstimator &estimator, const vector<vector<unsigned int>> &indices,
		const vector<Point2f> &points1, const vector<Point2f> &points2)
	{
		int minimumSampleSet = estimator.getMinimumSampleSet();
		vector<HomographyModel> models;
		
		// Generate models
		for (int i = 0; i < (int)indices.size(); i++) {
			vector<Point2f> srcPoints1, srcPoints2;
			for (int j = 0; j < minimumSampleSet; j++) {
				srcPoints1.push_back(points1[indices[i][j]]);
				srcPoints2.push_back(points2[indices[i][j]]);
			}

			// Generate homography based on its homograpy type
			if (estimator.getModelType() == HOMOGRAPHY_MANHATTAN_WORLD) {
				estimator.findMWHomographies(srcPoints1, srcPoints2, models);
				//models.push_back(HomographyModel(estimator.getHomography(srcPoints1, srcPoints2)));
			}
			else if (estimator.getModelType() == HOMOGRAPHY_WEAK_MANHATTAN_WORLD) {
				//estimator.findWMWHomographies(srcPoints1, srcPoints2, models);
				models.push_back(HomographyModel(estimator.findWMWHomographies(srcPoints1, srcPoints2)));
			}
			else {
				models.push_back(HomographyModel(estimator.getHomography(srcPoints1, srcPoints2), estimator.getThreshold()));
			}
		}

		return models;
	}

	void computeMatches(const Mat image1, const Mat image2, vector<Point2f> &points1, vector<Point2f> &points2)
	{
		// clear point matches
		points1.clear();
		points2.clear();

		Mat grayImg1, grayImg2;
		if (image1.channels() == 3) {
			cvtColor(image1, grayImg1, CV_BGR2GRAY);
			cvtColor(image2, grayImg2, CV_BGR2GRAY);
		}
		else {
			grayImg1 = image1.clone();
			grayImg2 = image2.clone();
		}

		// Compute keypoints and descriptors for first image
		Ptr<SIFT> detector = SIFT::create();
		vector<KeyPoint> keypoints1, keypoints2;
		Mat descriptors1, descriptors2;
		detector->detect(grayImg1, keypoints1);
		detector->compute(grayImg1, keypoints1, descriptors1);

		// Compute keypoints and descriptors for second image
		detector->detect(grayImg2, keypoints2);
		detector->compute(grayImg2, keypoints2, descriptors2);

		// Match features
		FlannBasedMatcher matcher;
		vector<vector<DMatch>> matches;
		matcher.knnMatch(descriptors1, descriptors2, matches, 2);

		for (int i = 0; i < (int)matches.size(); i++)
		{
			if (matches[i].size() == 1) {
				points1.push_back(keypoints1[matches[i][0].queryIdx].pt);
				points2.push_back(keypoints2[matches[i][0].trainIdx].pt);
			}
			else if (matches[i].size() == 2) {
				if (matches[i][0].distance <= matches[i][1].distance * NN_SQ_DIST_RATIO_THR) {
					points1.push_back(keypoints1[matches[i][0].queryIdx].pt);
					points2.push_back(keypoints2[matches[i][0].trainIdx].pt);
				}
			}
		}

		detector.release();
	}

	void computeMatches(const vector<KeyPoint> keypoints1, const vector<KeyPoint> keypoints2,
		Mat descriptors1, Mat descriptors2, vector<Point2f> &points1, vector<Point2f> &points2)
	{
		// Match features
		FlannBasedMatcher matcher;
		vector<vector<DMatch>> matches;
		matcher.knnMatch(descriptors1, descriptors2, matches, 2);

		for (int i = 0; i < (int)matches.size(); i++)
		{
			if (matches[i].size() == 1) {
				points1.push_back(keypoints1[matches[i][0].queryIdx].pt);
				points2.push_back(keypoints2[matches[i][0].trainIdx].pt);
			}
			else if (matches[i].size() == 2) {
				if (matches[i][0].distance <= matches[i][1].distance * NN_SQ_DIST_RATIO_THR) {
					points1.push_back(keypoints1[matches[i][0].queryIdx].pt);
					points2.push_back(keypoints2[matches[i][0].trainIdx].pt);
				}
			}
		}

	}

	void removeMatches(vector<Point2f> &points1, vector<Point2f> &points2, const Mat &mask)
	{
		vector<Point2f>::iterator iter1 = points1.begin();
		vector<Point2f>::iterator iter2 = points2.begin();
		while (iter1 != points1.end()) {
			if (mask.at<unsigned char>(*iter1) != 0) {
				iter1 = points1.erase(iter1);
				iter2 = points2.erase(iter2);
			}
			else {
				iter1++;
				iter2++;
			}
		}
	}

	double computeAdjustedRandIndex(Clusters cluster_gt, Clusters cluster_est, bool considerOutliers)
	{
		if (considerOutliers) {
			return computeAdjustedRandIndexWithOurlierCluster(cluster_gt, cluster_est);
		}

		// First of all, remove outlier points from given clusters
		Cluster outliers = cluster_gt.getOutliers();
		for (unsigned int i = 0; i < outliers.size(); i++) {
			cluster_est.remove(outliers.getMatch(i));
		}

		int num_of_gt_clusters = (int)cluster_gt.size();
		int num_of_est_clusters = (int)cluster_est.size();
		int **table = new int*[num_of_gt_clusters];
		int num_of_points;

		// The last index of contingency table is for an outlier cluster
		for (int i = 0; i < num_of_gt_clusters; i++)
		{
			table[i] = new int[num_of_est_clusters + 1];
			memset(table[i], 0, sizeof(int)*(num_of_est_clusters + 1));
		}

		// Fill the values of contingency table for inlier points
		for (int ci = 0; ci < num_of_gt_clusters; ci++)
		{
			num_of_points = (int)cluster_gt[ci].size();

			for (int pi = 0; pi < num_of_points; pi++)
			{
				int cj = cluster_est.getClusterIndex(cluster_gt[ci].getMatch(pi));

				if (cj == OUTLIER_CLUSTER_INDEX) {
					table[ci][num_of_est_clusters]++;
				}
				else if (cj == POINT_DOES_NOT_EXIST) {
					cout << "Point does not exist!" << endl;
					return -1;
				}
				else {
					table[ci][cj]++;
				}
			}
		}

		// Compute the adjusted rand index
		// Calculate a, b, and n
		int a = 0, b = 0, c = 0, n = 0;
		for (int cj = 0; cj <= num_of_est_clusters; cj++)
		{
			int sum = 0;
			for (int ci = 0; ci < num_of_gt_clusters; ci++)
			{
				sum += table[ci][cj];

				// n combination 2 is always equal to (n * (n-1)) / 2
				if (table[ci][cj] >= 2)
					a += table[ci][cj] * (table[ci][cj] - 1) / 2;
			}

			// b is the number of pairs of objects in the same class in
			// cluster_est but not in the same cluster in cluster_gt
			b += sum * (sum - 1) / 2;
			n += sum;
		}
		b -= a;

		// Calculate c:
		// c is the number of pairs of objects in the same class in
		// cluster_est but not in the same cluster in cluster_gt
		for (int ci = 0; ci < num_of_gt_clusters; ci++)
		{
			int sum = 0;

			for (int cj = 0; cj <= num_of_est_clusters; cj++)
			{
				sum += table[ci][cj];
			}

			c += sum * (sum - 1) / 2;
		}
		c -= a;

		// Calculate d
		int n_comb_2 = (n * (n - 1) / 2);
		int d = n_comb_2 - a - b - c;

		double rand_index = double(a - (b + a)*(c + a) / n_comb_2) / double(0.5*(b + a + c + a) - (b + a)*(c + a) / n_comb_2);

		// Deallocate memory
		for (int i = 0; i < num_of_gt_clusters; i++)
			delete[] table[i];
		delete[] table;

		return rand_index;
	}

	double computeAdjustedRandIndexWithOurlierCluster(Clusters cluster_gt, Clusters cluster_est)
	{
		int num_of_gt_clusters = (int)cluster_gt.size();
		int num_of_est_clusters = (int)cluster_est.size();
		int **table = new int*[num_of_gt_clusters + 1];
		int num_of_points;

		cluster_est.refreshIndices();

		// The last index of contingency table is for an outlier cluster
		for (int gt_ci = 0; gt_ci <= num_of_gt_clusters; gt_ci++)
		{
			table[gt_ci] = new int[num_of_est_clusters + 1];
			memset(table[gt_ci], 0, sizeof(int)*(num_of_est_clusters + 1));
		}

		// Fill the values of contingency table for inlier points
		for (int gt_ci = 0; gt_ci < num_of_gt_clusters; gt_ci++)
		{
			num_of_points = (int)cluster_gt[gt_ci].size();

			for (int pi = 0; pi < num_of_points; pi++)
			{
				int cj = cluster_est.getClusterIndex(cluster_gt[gt_ci].getMatch(pi));

				if (cj >= num_of_est_clusters) {
					cout << "Wrong cluster index!: " << cj << endl;
				}

				if (cj == OUTLIER_CLUSTER_INDEX) {
					table[gt_ci][num_of_est_clusters]++;
				}
				else if (cj == POINT_DOES_NOT_EXIST) {
					cout << "Point does not exist!" << endl;
					return -1;
				}
				else {
					table[gt_ci][cj]++;
				}
			}
		}

		// Fill the values of contingency table for outlier points
		Cluster outliers = cluster_gt.getOutliers();
		num_of_points = outliers.size();
		for (int pi = 0; pi < num_of_points; pi++)
		{
			int cj = cluster_est.getClusterIndex(outliers.getMatch(pi));

			if (cj >= num_of_est_clusters) {
				cout << "Wrong cluster index!: " << cj << endl;
			}

			if (cj == OUTLIER_CLUSTER_INDEX) {
				table[num_of_gt_clusters][num_of_est_clusters]++;
			}
			else if (cj == POINT_DOES_NOT_EXIST) {
				cout << "Point does not exist!" << endl;
				return -1;
			}
			else {
				table[num_of_gt_clusters][cj]++;
			}
		}

		// Compute the adjusted rand index
		// Calculate a, b, and n
		int a = 0, b = 0, c = 0, n = 0;
		for (int cj = 0; cj <= num_of_est_clusters; cj++)
		{
			int sum = 0;
			for (int ci = 0; ci <= num_of_gt_clusters; ci++)
			{
				sum += table[ci][cj];

				// n combination 2 is always equal to (n * (n-1)) / 2
				if (table[ci][cj] >= 2)
					a += table[ci][cj] * (table[ci][cj] - 1) / 2;
			}

			// b is the number of pairs of objects in the same class in
			// cluster_est but not in the same cluster in cluster_gt
			b += sum * (sum - 1) / 2;
			n += sum;
		}
		b -= a;

		// Calculate c:
		// c is the number of pairs of objects in the same class in
		// cluster_est but not in the same cluster in cluster_gt
		for (int ci = 0; ci <= num_of_gt_clusters; ci++)
		{
			int sum = 0;

			for (int cj = 0; cj <= num_of_est_clusters; cj++)
			{
				sum += table[ci][cj];
			}

			c += sum * (sum - 1) / 2;
		}
		c -= a;

		// Calculate d
		int n_comb_2 = (n * (n - 1) / 2);
		int d = n_comb_2 - a - b - c;

		double rand_index = double(a - (b + a)*(c + a) / n_comb_2) / double(0.5*(b + a + c + a) - (b + a)*(c + a) / n_comb_2);

		// Deallocate memory
		for (int i = 0; i <= num_of_gt_clusters; i++)
			delete[] table[i];
		delete[] table;

		return rand_index;
	}

	double computeJaccardDistance(const Cluster &cluster1, const Cluster &cluster2)
	{
		unordered_set<Match, MatchHash> matchHashmap;

		// Insert matches of the first cluster to hashmap
		for (int i = 0; i < int(cluster1.size()); i++) {
			matchHashmap.insert(cluster1.getMatch(i));
		}

		// Compare existance
		int intersection = 0;
		for (int i = 0; i < int(cluster2.size()); i++) {
			if (matchHashmap.find(cluster2.getMatch(i)) != matchHashmap.end()) {
				intersection++;
			}
		}

		int card_union = int(cluster1.size() + cluster2.size()) - intersection;

		return double(card_union - intersection) / double(card_union);
	}

	double computeOverlapCoefficient(const Cluster &cluster1, const Cluster &cluster2)
	{
		unordered_set<Match, MatchHash> matchHashmap;

		// Insert matches of the first cluster to hashmap
		for (int i = 0; i < int(cluster1.size()); i++) {
			matchHashmap.insert(cluster1.getMatch(i));
		}

		// Compare existance
		int intersect = 0;
		for (int i = 0; i < int(cluster2.size()); i++) {
			if (matchHashmap.find(cluster2.getMatch(i)) != matchHashmap.end()) {
				intersect++;
			}
		}

		return intersect / double(cv::min(cluster1.size(), cluster2.size()));
	}

	bool isCentroidAtSameSide(const Cluster &cluster1, const Cluster &cluster2, const Mat &VL)
	{
		Mat centroid1(3, 1, CV_64F);
		Mat centroid2(3, 1, CV_64F);

		centroid1.at<double>(0) = 0.0;
		centroid1.at<double>(1) = 0.0;
		centroid1.at<double>(2) = 1.0;

		centroid2.at<double>(0) = 0.0;
		centroid2.at<double>(1) = 0.0;
		centroid2.at<double>(2) = 1.0;

		for (unsigned int pi = 0; pi < cluster1.points1.size(); pi++) {
			centroid1.at<double>(0) += cluster1.points1[pi].x;
			centroid1.at<double>(1) += cluster1.points1[pi].y;
		}
		centroid1.at<double>(0) /= double(cluster1.points1.size());
		centroid1.at<double>(1) /= double(cluster1.points1.size());

		for (unsigned int pi = 0; pi < cluster2.points1.size(); pi++) {
			centroid2.at<double>(0) += cluster2.points1[pi].x;
			centroid2.at<double>(1) += cluster2.points1[pi].y;
		}
		centroid2.at<double>(0) /= double(cluster2.points1.size());
		centroid2.at<double>(1) /= double(cluster2.points1.size());
		
		if (VL.dot(centroid1) * VL.dot(centroid2) >= 0) {
			return true;
		}
		else {
			return false;
		}
	}

	double ScaleEstimator(const Mat &H, const vector<Point2f> points1, const vector<Point2f> points2, int K, double E)
	{
		vector<double> residuals = computeResiduals(H, points1, points2);
		int num_of_points = int(residuals.size());

		assert(K < num_of_points);

		// make sure that all the residuals are sorted in ascending order
		sort(residuals.begin(), residuals.end());

		// initialize the parameter
		normal s(0.0, 1.0);		// initialize the standard normal distribution
		double kappa = K / double(num_of_points);
		double scale = 1.0, scale_new = 0.0;
		int n_k;

		while (scale_new != scale && kappa < 1.0)
		{
			scale = scale_new;

			// Estimate the scale
			scale_new = residuals[K] / quantile(s, 0.5 * (1.0 + kappa));

			// Calculate the number of inliers
			n_k = 0;
			for (int i = 0; i < num_of_points; i++) {
				if ((abs(residuals[i]) / scale_new) < E) {
					n_k++;
				}
			}

			// Compute kappa
			kappa = K / double(n_k);
		}

		return scale;
	}

	vector<double> computeResiduals(const Mat &homography, const vector<Point2f> points1,
		const vector<Point2f> points2, double threshold)
	{
		vector<double> residuals;
		int num_of_inliers = 0;
		vector<Point2f> points2_proj;

		// Recalculate inliers for all correspondences
		perspectiveTransform(points1, points2_proj, homography);

		// Calculate average differences and the number of inliers
		for (int pi = 0; pi < (int)points1.size(); pi++) {
			residuals.push_back(norm(points2_proj[pi] - points2[pi]));

			if (threshold >= 0.0 && residuals.back() < threshold) {
				num_of_inliers++;
			}
		}

		return residuals;
	}
	
	void computeHomographies(Clusters &clusters, int type)
	{
		for (int i = 0; i < (int)clusters.size(); i++) {
			clusters[i].Homography = findHomography(clusters[i].points1, clusters[i].points2, type);
		}
	}

	void computeHomographies(Clusters &clusters, const HomographyEstimator &estimator)
	{
		for (int i = 0; i < (int)clusters.size(); i++) {
			clusters[i].Homography = estimator.getHomography(clusters[i].points1, clusters[i].points2);
		}
	}

	vector<Mat> computeHomographies(const vector<Cluster> &clusters, int type)
	{
		vector<Mat> homographies;

		for (int i = 0; i < (int)clusters.size(); i++) {
			if (clusters[i].points1.size() >= 4) {
				homographies.push_back(findHomography(clusters[i].points1, clusters[i].points2, type));
			}
		}

		return homographies;
	}

	vector<Mat> computeSingleNPairHomographies(const Clusters &clusters, const HomographyEstimator &estimator)
	{
		vector<Mat> homographies;

		// First compute a homography for each cluster
		for (int i = 0; i < int(clusters.size()); i++) {
			Mat homography = estimator.getHomography(clusters[i].points1, clusters[i].points2);

			if (!homography.empty()) {
				homographies.push_back(homography);
			}
		}

		// Then, compute a homography for each pair of clusters
		for (int i = 0; i < int(clusters.size()) - 1; i++) {
			for (int j = i + 1; j < int(clusters.size()); j++) {
				Cluster cluster_tmp = ClusterUtils::mergeClusters(clusters[i], clusters[j]);
				Mat homography = estimator.getHomography(cluster_tmp.points1, cluster_tmp.points2);

				if (!homography.empty()) {
					homographies.push_back(homography);
				}
			}
		}

		return homographies;
	}

	Clusters clusterArrayToClusters(const vector<Cluster> &input,
		const vector<Point2f> points1, const vector<Point2f> points2)
	{
		Clusters clusters(input.size(), points1, points2);

		for (int i = 0; i < (int)input.size(); i++) {
			for (int j = 0; j < (int)input[i].size(); j++) {
				clusters.addPoint(input[i].points1[j], input[i].points2[j], i);
			}
		}

		clusters.removeEmptyClusters();

		return clusters;
	}

	vector<Cluster> clustersToClusterArray(const Clusters &input)
	{
		std::vector<Cluster> clusters;

		for (int i = 0; i < (int)input.size(); i++) {
			Cluster cluster;
			for (int j = 0; j < (int)input[i].size(); j++) {
				cluster.points1.push_back(input[i].points1[j]);
				cluster.points2.push_back(input[i].points2[j]);
			}
			clusters.push_back(cluster);
		}

		return clusters;
	}

	Cluster computeInliers(const Mat &homography, const vector<Point2f> &points1,
		const vector<Point2f> &points2, const double threshold)
	{
		vector<Point2f> points2_proj;
		Cluster cluster_output;

		// Recalculate inliers for all correspondences
		perspectiveTransform(points1, points2_proj, homography);

		for (unsigned int pi = 0; pi < points1.size(); pi++)
		{
			double error = norm(points2_proj[pi] - points2[pi]);

			if (error < threshold) {
				cluster_output.points1.push_back(points1[pi]);
				cluster_output.points2.push_back(points2[pi]);
			}
		}

		return cluster_output;
	}

	Cluster computeInliers(const Mat &homography, const vector<Match> &pointMatches, const double threshold)
	{
		vector<Point2f> points1, points2, points2_proj;
		Cluster cluster_output;

		for (int pi = 0; pi < pointMatches.size(); pi++) {
			points1.push_back(pointMatches[pi].first);
			points2.push_back(pointMatches[pi].second);
		}

		vector<double> residuals = computeResiduals(homography, points1, points2, threshold);

		for (int pi = 0; pi < residuals.size(); pi++) {
			if (residuals[pi] < threshold) {
				cluster_output.points1.push_back(points1[pi]);
				cluster_output.points2.push_back(points2[pi]);
			}
		}

		return cluster_output;

		// Recalculate inliers for all correspondences
		for (int pi = 0; pi < pointMatches.size(); pi++) {
			double x = pointMatches[pi].first.x * homography.at<double>(0, 0) + pointMatches[pi].first.y * homography.at<double>(0, 1) + homography.at<double>(0, 2);
			double y = pointMatches[pi].first.x * homography.at<double>(1, 0) + pointMatches[pi].first.y * homography.at<double>(1, 1) + homography.at<double>(1, 2);
			double z = pointMatches[pi].first.x * homography.at<double>(2, 0) + pointMatches[pi].first.y * homography.at<double>(2, 1) + homography.at<double>(2, 2);
			x /= z;
			y /= z;
			double error = sqrt((pointMatches[pi].second.x - x)*(pointMatches[pi].second.x - x) + (pointMatches[pi].second.y - y)*(pointMatches[pi].second.y - y));

			if (error < threshold) {
				cluster_output.points1.push_back(pointMatches[pi].first);
				cluster_output.points2.push_back(pointMatches[pi].second);
			}
		}

		return cluster_output;
	}

	vector<Cluster> computeInliers(const vector<Mat> &homographies, const vector<Point2f> &points1,
		const vector<Point2f> &points2, const double threshold)
	{
		vector<Cluster> clusters_output;

		for (unsigned int ci = 0; ci < homographies.size(); ci++) {
			clusters_output.push_back(computeInliers(homographies[ci], points1, points2, threshold));
		}

		return clusters_output;
	}

	vector<Cluster> computeInliers(const vector<Cluster> &clusters_input, const double threshold)
	{
		vector<Cluster> clusters_output;

		for (unsigned int ci = 0; ci < clusters_input.size(); ci++) {
			if (clusters_input[ci].Homography.empty()) {
				if (clusters_input[ci].size() < 4) {
					clusters_output.push_back(clusters_input[ci]);
					continue;
				}
				else {
					Mat homography = findHomography(clusters_input[ci].points1, clusters_input[ci].points2);

					clusters_output.push_back(computeInliers(homography,
						clusters_input[ci].points1, clusters_input[ci].points2, threshold));
				}
			}
			else {
				clusters_output.push_back(computeInliers(clusters_input[ci].Homography,
					clusters_input[ci].points1, clusters_input[ci].points2, threshold));
			}
		}

		return clusters_output;
	}

	vector<Cluster> computeInliers(const vector<Cluster> &clusters_input, const vector<cv::Point2f> &points1,
		const vector<Point2f> &points2, const double threshold)
	{
		vector<Cluster> clusters_output;

		for (int ci = 0; ci < clusters_input.size(); ci++) {
			if (clusters_input[ci].points1.size() >= 4) {
				Mat homography = findHomography(clusters_input[ci].points1, clusters_input[ci].points2);
				clusters_output.push_back(computeInliers(homography, points1, points2, threshold));
			}
		}

		return clusters_output;
	}

	Cluster computeInliers(const Cluster &cluster_input, const Mat &VanishingLine,
		const vector<Point2f> &points1, const vector<Point2f> &points2, const double threshold)
	{
		assert(VanishingLine.cols == 3 || VanishingLine.rows == 3);

		Cluster cluster_out;
		vector<Match> match_negGroup;
		vector<Match> match_posGroup;

		separateMatchesVL(points1, points2, VanishingLine, match_negGroup, match_posGroup);

		double dist = computeLineDistance(VanishingLine, computeCentroid(cluster_input.points1));

		if (dist < 0) {
			cluster_out = computeInliers(cluster_input.Homography, match_negGroup, threshold);
		}
		else {
			cluster_out = computeInliers(cluster_input.Homography, match_posGroup, threshold);
		}

		return cluster_out;
	}

	vector<Cluster> computeInliers(const vector<Cluster> &clusters_input, const Mat &VanishingLines,
		const vector<Point2f> &points1, const vector<Point2f> &points2, const double threshold)
	{
		std::vector<Cluster> clusters_output;

		for (unsigned int ci = 0; ci < clusters_input.size(); ci++) {
			assert(clusters_input[ci].normal_index >= 0 && clusters_input[ci].normal_index < VanishingLines.rows);
			clusters_output.push_back(computeInliers(clusters_input[ci], VanishingLines.col(clusters_input[ci].normal_index),
				points1, points2, threshold));
		}

		return clusters_output;
	}

	Clusters computeUniqueInliers(const vector<Mat> &homographies, const vector<Point2f> &points1,
		const vector<Point2f> &points2, const double threshold)
	{
		Clusters clusters((int)homographies.size());
		vector<double> errors(points1.size(), threshold);
		vector<int> indices(points1.size(), OUTLIER_CLUSTER_INDEX);

		for (int ci = 0; ci < (int)homographies.size(); ci++)
		{
			if (homographies[ci].empty()) {
				continue;
			}

			vector<Point2f> points1_temp, points2_temp, points2_proj;

			// Recalculate inliers for all correspondences
			perspectiveTransform(points1, points2_proj, homographies[ci]);

			for (size_t pi = 0; pi < points1.size(); pi++)
			{
				double error = norm(points2_proj[pi] - points2[pi]);

				if (error < errors[pi])
				{
					errors[pi] = error;
					indices[pi] = ci;
				}
			}
		}

		for (size_t pi = 0; pi < points1.size(); pi++) {
			clusters.addPoint(points1[pi], points2[pi], indices[pi]);
		}

		return clusters;
	}

	Clusters computeUniqueInliers(const vector<Cluster> &clusters, const vector<Point2f> &points1,
		const vector<Point2f> &points2, const double threshold)
	{
		vector<Mat> homographies;

		for (int ci = 0; ci < clusters.size(); ci++) {
			if (clusters[ci].Homography.empty()) {
				homographies.push_back(findHomography(clusters[ci].points1, clusters[ci].points2));
			}
			else {
				homographies.push_back(clusters[ci].Homography);
			}
		}

		return computeUniqueInliers(homographies, points1, points2, threshold);
	}

	Clusters computeUniqueInliers(const vector<Cluster> &clusters, const Mat &VanishingLines,
		const vector<Point2f> &points1, const vector<Point2f> &points2, const double threshold)
	{
		Clusters clusters_out(clusters.size());
		vector<unsigned char> indicators(points1.size());
		vector<bool> isPositiveSide(clusters.size(), false);
		vector<double> errors(points1.size(), DBL_MAX);
		vector<int> indices(points1.size(), OUTLIER_CLUSTER_INDEX);

		// Compute parameters for each cluster
		for (unsigned int ci = 0; ci < clusters.size(); ci++) {
			isPositiveSide[ci] = computeLineDistance(VanishingLines.col(clusters[ci].normal_index), clusters[ci].points1[0]) >= 0;
		}

		// Check the vanishing line constraint for each points
		for (unsigned int pi = 0; pi < (int)points1.size(); pi++) {
			unsigned char indicator = 0;
			indicator += computeLineDistance(VanishingLines.col(0), points1[pi]) >= 0 ? 1 : 0;
			indicator += computeLineDistance(VanishingLines.col(1), points1[pi]) >= 0 ? 2 : 0;
			indicator += computeLineDistance(VanishingLines.col(2), points1[pi]) >= 0 ? 4 : 0;
			indicators[pi] = indicator;
		}

		// Find the best cluster for each point
		for (unsigned int ci = 0; ci < clusters.size(); ci++)
		{
			if (clusters[ci].Homography.empty()) {
				continue;
			}

			vector<Point2f> points1_temp, points2_temp, points2_proj;

			// Compute reprojection error for each point w.r.t the currect homography
			perspectiveTransform(points1, points2_proj, clusters[ci].Homography);

			for (unsigned int pi = 0; pi < points1.size(); pi++)
			{
				// Check if point pair is on the same side with given model
				if ((indicators[pi] >> clusters[ci].normal_index & 1) != isPositiveSide[ci]) {
					continue;
				}

				double error = norm(points2_proj[pi] - points2[pi]);

				if (error < threshold && error < errors[pi])
				{
					errors[pi] = error;
					indices[pi] = ci;
				}
			}
		}

		for (size_t pi = 0; pi < points1.size(); pi++) {
			clusters_out.addPoint(points1[pi], points2[pi], indices[pi]);
		}

		return clusters_out;
	}

	Clusters computeUniqueInliers(std::vector<Cluster> clusters, const HomographyEstimator &estimator,
		const std::vector<cv::Point2f> &points1, const std::vector<cv::Point2f> &points2, const double threshold)
	{
		// Compute parameters for each cluster
		for (unsigned int ci = 0; ci < clusters.size(); ci++) {
			clusters[ci].Homography = estimator.getHomography(clusters[ci].points1, clusters[ci].points2, clusters[ci].normal_index);
		}

		return computeUniqueInliers(clusters, estimator.getVanishingLines(), points1, points2, threshold);
	}
	
	// Merge two clusters
	Cluster mergeClusters(Cluster cluster1, Cluster cluster2)
	{
		unordered_set<Match, MatchHash> hashmap;
		Cluster output = cluster1;
		for (unsigned int i = 0; i < cluster1.size(); i++) {
			hashmap.insert(cluster1.getMatch(i));
		}

		for (unsigned int i = 0; i < cluster2.size(); i++) {
			if (hashmap.find(cluster2.getMatch(i)) == hashmap.end()) {
				output.points1.push_back(cluster2.points1[i]);
				output.points2.push_back(cluster2.points2[i]);
			}
		}

		return output;
	}

	void mergeClusters(vector<Cluster> &clusters, int ind1, int ind2)
	{
		clusters[ind1] = mergeClusters(clusters[ind1], clusters[ind2]);
		clusters.erase(clusters.begin() + ind2);
	}

	vector<Cluster> mergeClusters(vector<Cluster> clusters, const HomographyEstimator &estimator, const vector<Point2f> &points1,
		const vector<Point2f> &points2, double threshold, Mat img)
	{
		bool converged = false;

		assert(estimator.getModelType() == HOMOGRAPHY_MANHATTAN_WORLD);

		// Remove clusters
		ClusterUtils::removeClusters(clusters, estimator.getMinimumSampleSet());
		
		while (!converged) {
			converged = true;

			estimator.computeParameters(clusters);
			vector<Cluster> clusters_tmp = computeInliers(clusters, estimator.getVanishingLines(), points1, points2, threshold);

			// Compute Jaccard distances
			std::multimap<double, std::pair<int, int>, std::less<double>> hashmap;
			for (int i = 0; i < (int)clusters.size() - 1; i++) {
				for (int j = i + 1; j < (int)clusters.size(); j++) {
					if (clusters_tmp[i].normal_index == clusters_tmp[j].normal_index) {
						double dist = computeJaccardDistance(clusters_tmp[i], clusters_tmp[j]);
						//double dist = computeOverlapCoefficient(clusters_tmp[i], clusters_tmp[j]);
						hashmap.insert(std::pair<double, std::pair<int, int>>(dist, std::pair<int, int>(i, j)));
					}
				}
			}

			// find cluster pair to be merged
			std::map<double, std::pair<int, int>>::iterator iter = hashmap.begin();
			while (iter != hashmap.end())
			{
				int ind1 = iter->second.first;
				int ind2 = iter->second.second;
				if (isCentroidAtSameSide(clusters[ind1], clusters[ind2], estimator.getVanishingLine(clusters[ind1].normal_index)))
				{
					//imwrite("cluster1.png", DrawPointsOnImage(clusters[ind1].points1, img, 2));
					//imwrite("cluster2.png", DrawPointsOnImage(clusters[ind2].points1, img, 2));
					Cluster cluster_union = mergeClusters(clusters[ind1], clusters[ind2]);
					Mat homography =
						estimator.findMWHomography(cluster_union.points1, cluster_union.points2, clusters[ind1].normal_index);

					if (threshold < 0) {
						//threshold = ScaleEstimator(homography, points1, points2, int(points1_comb.size()));
						vector<double> residuals = computeResiduals(homography, cluster_union.points1, cluster_union.points2);
						threshold = residuals.back();
					}

					//Cluster cluster_temp = computeInliers(homography, points1, points2, threshold);
					Cluster cluster_comb_ext = computeInliers(homography, points1, points2, estimator.getThreshold(clusters[ind1].normal_index));
					cluster_comb_ext.normal_index = clusters[ind1].normal_index;
					estimator.computeParameters(cluster_comb_ext.points1, cluster_comb_ext.points2, cluster_comb_ext.translation, cluster_comb_ext.plane_normal);
					if (cluster_comb_ext.translation.empty() || cluster_comb_ext.plane_normal.empty()) {
						iter++;
						continue;
					}

					// DEBUG
					//Cluster clusters1_tmp = computeInliers(clusters[ind1], estimator, points1, points2, estimator.getThreshold(clusters[ind1].normal_index));
					//Cluster clusters2_tmp = computeInliers(clusters[ind2], estimator, points1, points2, estimator.getThreshold(clusters[ind2].normal_index));
					// DEBUG

					//bool isNormalKept = norm(cluster_temp.plane_normal - clusters[ind1].plane_normal) <= 1e-6;
					//double similarity_ratio = computeOverlapCoefficient(cluster_temp, cluster_comb);
					double similarity_ratio = computeJaccardDistance(cluster_union, cluster_comb_ext);
					//double similarity_ratio = computeJaccardDistance(clusters1_tmp, clusters2_tmp);
					//double similarity_ratio = double(cluster_temp.size()) / double(cluster_comb.size());
					//double cosignSimilarity = computeCosineSimilarity(clusters[ind1].translation, clusters[ind2].translation);

					if (/*isNormalKept && */similarity_ratio < 0.5 /* && abs(cosignSimilarity) >= 0.7*/) {
						/* DEBUG */
						/*
						Cluster tmp;
						imwrite("cluster1.png", DrawPointsOnImage(clusters[ind1].points1, img, 2));
						imwrite("cluster2.png", DrawPointsOnImage(clusters[ind2].points1, img, 2));
						clusters[ind1].Homography = estimator.findConstrainedHomography(clusters[ind1].points1, clusters[ind1].points2, clusters[ind1].plane_normal);
						tmp = computeInliers(clusters[ind1].Homography, points1, points2, threshold);
						imwrite("cluster1_ext.png", DrawPointsOnImage(tmp.points1, img, 2));
						clusters[ind2].Homography = estimator.findConstrainedHomography(clusters[ind2].points1, clusters[ind2].points2, clusters[ind2].plane_normal);
						tmp = computeInliers(clusters[ind2].Homography, points1, points2, threshold);
						imwrite("cluster2_ext.png", DrawPointsOnImage(tmp.points1, img, 2));
						imwrite("cluster_merge.png", DrawPointsOnImage(cluster_temp.points1, img, 2));
						imwrite("cluster_comb.png", DrawPointsOnImage(cluster_comb.points1, img, 2));
						*/
						/* DEBUG */

						clusters[ind1] = cluster_comb_ext;
						clusters.erase(clusters.begin() + ind2);

						/*mergeClusters(clusters, ind1, ind2);
						clusters[ind1].normal_index = clusters[ind1].normal_index;
						clusters[ind1].translation = cluster_comb_ext.translation;*/
						converged = false;
						break;
					}
				}

				iter++;
			}
		}

		return clusters;
	}

	vector<Cluster> mergeClusters(vector<Cluster> clusters, const vector<Point2f> &points1,
		const vector<Point2f> &points2, double threshold)
	{
		bool converged = false;

		// Remove clusters
		ClusterUtils::removeClusters(clusters, 4);

		while (!converged) {
			converged = true;

			vector<Cluster> clusters_tmp;

			vector<Mat> homographies = ClusterUtils::computeHomographies(clusters, 0);
			clusters_tmp = ClusterUtils::computeInliers(homographies, points1, points2, threshold);

			// Compute Jaccard distances
			std::multimap<double, std::pair<int, int>, std::less<double>> hashmap;
			for (int i = 0; i < (int)clusters.size() - 1; i++) {
				for (int j = i + 1; j < (int)clusters.size(); j++) {
					double dist = computeJaccardDistance(clusters_tmp[i], clusters_tmp[j]);
					//double dist = computeOverlapCoefficient(clusters_tmp[i], clusters_tmp[j]);
					hashmap.insert(std::pair<double, std::pair<int, int>>(dist, std::pair<int, int>(i, j)));
				}
			}

			// find cluster pair to be merged
			std::map<double, std::pair<int, int>>::iterator iter = hashmap.begin();
			while (iter != hashmap.end())
			{
				int ind1 = iter->second.first;
				int ind2 = iter->second.second;

				Mat H1 = cv::findHomography(clusters[ind1].points1, clusters[ind1].points2);
				Mat H2 = cv::findHomography(clusters[ind2].points1, clusters[ind2].points2);

				if (H1.empty() || H2.empty()) {
					iter++;
					continue;
				}

				Cluster clusters1_tmp = computeInliers(H1, points1, points2, threshold);
				Cluster clusters2_tmp = computeInliers(H2, points1, points2, threshold);

				double similarity_ratio = computeJaccardDistance(clusters1_tmp, clusters2_tmp);

				if (similarity_ratio < 0.75 /* && abs(cosignSimilarity) >= 0.7*/) {
					mergeClusters(clusters, ind1, ind2);
					converged = false;
					break;
				}

				iter++;
			}
		}

		return clusters;
	}

	Clusters MultiplePlaneDetection(vector<Cluster> clusters, const vector<Point2f> &points1,
		const vector<Point2f> &points2, const Size &size, const double thr_reproj)
	{
		bool is_updated = true;

		// Global merging
		while (is_updated)
		{
			is_updated = false;

			// Compute Jaccard distances
			std::multimap<double, std::pair<int, int>, std::greater<double>> hashmap;
			for (int i = 0; i < (int)clusters.size() - 1; i++) {
				for (int j = i + 1; j < (int)clusters.size(); j++) {
					if (clusters[i].size() < 4 || clusters[j].size() < 4) {
						continue;
					}

					double dist = computeMergeDistance(clusters[i], clusters[j], thr_reproj);
					if (dist <= thr_reproj) {
						hashmap.insert(std::pair<double, std::pair<int, int>>(dist, std::pair<int, int>(i, j)));
					}
				}
			}

			// find cluster pair to be merged
			if (!hashmap.empty()) {
				int ind1 = hashmap.begin()->second.first;
				int ind2 = hashmap.begin()->second.second;

				ClusterUtils::mergeClusters(clusters, ind1, ind2);
				is_updated = true;
			}
		}

		// Make sure that all the points have the positive numbers
		double min_x = 0, min_y = 0, max_x = size.width, max_y = size.height;
		for (int i = 0; i < points1.size(); i++) {
			if (points1[i].x < min_x) {
				min_x = points1[i].x;
			}
			if (points1[i].y < min_y) {
				min_y = points1[i].y;
			}
			if (points1[i].x > max_x) {
				max_x = points1[i].x;
			}
			if (points1[i].y > max_y) {
				max_y = points1[i].y;
			}
		}
		if (min_x < 0) {
			for (int i = 0; i < clusters.size(); i++) {
				for (int j = 0; j < clusters[i].size(); j++) {
					clusters[i].points1[j].x -= min_x;
				}
			}
			max_x -= min_x;
		}
		if (min_y < 0) {
			for (int i = 0; i < clusters.size(); i++) {
				for (int j = 0; j < clusters[i].size(); j++) {
					clusters[i].points1[j].y -= min_y;
				}
			}
			max_y -= min_y;
		}

		// Spatial analysis
		vector<Cluster> new_cluster;
		for (size_t i = 0; i < clusters.size(); i++)
		{
			cv::Subdiv2D subdiv(Rect(0, 0, max_x, max_y));
			vector<cv::Vec4f> edgeList;
			vector<float> distanceList;
			vector<int> label;
			float mean = 0.0;
			float stddev = 0.0;
			int num_label;

			for (size_t pi = 0; pi < clusters[i].points1.size(); pi++)
				subdiv.insert(clusters[i].points1[pi]);

			subdiv.getEdgeList(edgeList);

			for (size_t ei = 0; ei < edgeList.size(); ei++)
			{
				if (edgeList[ei][0] > size.width || edgeList[ei][1] > size.height || edgeList[ei][0] < 0 || edgeList[ei][1] < 0 ||
					edgeList[ei][2] > size.width || edgeList[ei][3] > size.height || edgeList[ei][2] < 0 || edgeList[ei][3] < 0)
					continue;

				distanceList.push_back(sqrt(pow((edgeList[ei][0] - edgeList[ei][2]), 2) + pow((edgeList[ei][1] - edgeList[ei][3]), 2)));
				mean += distanceList.back();
			}
			mean /= (float)edgeList.size();

			for (size_t ei = 0; ei < distanceList.size(); ei++)
				stddev += pow(distanceList[ei] - mean, 2);
			stddev = sqrt(stddev / (float)edgeList.size());

			num_label = partition(clusters[i].points1, label, EqKeypoints(stddev));
			vector<Cluster> split_cluster(num_label);
			for (size_t j = 0; j < label.size(); j++)
			{
				split_cluster[label[j]].points1.push_back(clusters[i].points1[j]);
				split_cluster[label[j]].points2.push_back(clusters[i].points2[j]);
			}

			for (size_t j = 0; j < split_cluster.size(); j++) {
				if (split_cluster[j].points1.size() >= 6) {
					new_cluster.push_back(split_cluster[j]);
				}
			}
		}
		if (min_x < 0) {
			for (int i = 0; i < new_cluster.size(); i++) {
				for (int j = 0; j < new_cluster[i].size(); j++) {
					new_cluster[i].points1[j].x += min_x;
				}
			}
		}
		if (min_y < 0) {
			for (int i = 0; i < new_cluster.size(); i++) {
				for (int j = 0; j < new_cluster[i].size(); j++) {
					new_cluster[i].points1[j].y += min_y;
				}
			}
		}
		clusters = new_cluster;

		// Robust fitting and stability checks
		for (int i = 10; i >= 0; i--) {
			if (clusters.empty()) {
				return Clusters();
			}

			clusters = computeInliers(clusters, points1, points2, thr_reproj * (1.0 + double(i) * 0.2));
		}
		return computeUniqueInliers(clusters, points1, points2, thr_reproj);
	}

	double computeMergeDistance(const Cluster &cluster1, const Cluster &cluster2, const double threshold)
	{
		std::unordered_set<Match, MatchHash> matchHashmap;
		vector<Point2f> points1 = cluster1.points1, points2 = cluster1.points2;

		// Insert matches of the first cluster to hashmap
		for (int i = 0; i < int(cluster1.size()); i++) {
			matchHashmap.insert(cluster1.getMatch(i));
		}

		// Compare existance
		int num_union = int(cluster1.size());
		for (int i = 0; i < int(cluster2.size()); i++) {
			if (matchHashmap.find(cluster2.getMatch(i)) == matchHashmap.end()) {
				points1.push_back(cluster2.points1[i]);
				points2.push_back(cluster2.points2[i]);
				num_union++;
			}
		}

		vector<double> residuals = ClusterUtils::computeResiduals(cv::findHomography(points1, points2), points2, points2, threshold);
		double sum = 0.0;

		for (int i = 0; i < (int)residuals.size(); i++) {
			sum += residuals[i];
		}

		return sum / double(num_union);
	}

	// Separate clusters using vanishing line constraint.
	vector<Cluster> separateClusters(vector<Cluster> clusters, const HomographyEstimator &estimator)
	{
		int minimumSampleSet = estimator.getMinimumSampleSet();
		Mat VL = estimator.getVanishingLines();
		vector<Cluster> clusters_out;

		for (unsigned int ci = 0; ci < clusters.size(); ci++)
		{
			vector<Match> pointsGroup1, pointsGroup2;

			estimator.computeParameters(clusters[ci], PARAMS_MAX_INLIERS, false);
			separateMatchesVL(clusters[ci].points1, clusters[ci].points2, VL.col(clusters[ci].normal_index), pointsGroup1, pointsGroup2);

			if (pointsGroup1.size() > minimumSampleSet) {
				clusters_out.push_back(Cluster(pointsGroup1));
				clusters_out.back().normal_index = clusters[ci].normal_index;
			}

			if (pointsGroup2.size() > minimumSampleSet) {
				clusters_out.push_back(Cluster(pointsGroup2));
				clusters_out.back().normal_index = clusters[ci].normal_index;
			}
		}

		return clusters_out;
	}

	vector<Point2f> mergePoints(const vector<Point2f> &points1, const vector<Point2f> &points2)
	{
		unordered_set<Point2f, PointHash> hashmap;
		vector<Point2f> output = points1;
		for (unsigned int i = 0; i < points1.size(); i++) {
			hashmap.insert(points1[i]);
		}

		for (unsigned int i = 0; i < points2.size(); i++) {
			if (hashmap.find(points2[i]) == hashmap.end()) {
				output.push_back(points2[i]);
			}
		}

		return output;
	}

	void removeMinorClusters(std::vector<Cluster> &clusters, int minimumSampleSet)
	{
		std::sort(clusters.begin(), clusters.end(), ClusterSortByPoints);

		int max_diff = (int)clusters.back().size() - minimumSampleSet, max_diff_ind = -1;
		for (unsigned int ci = 1; ci < clusters.size(); ci++) {
			int diff = clusters[ci - 1].size() - clusters[ci].size();
			if (diff > max_diff) {
				max_diff = diff;
				max_diff_ind = ci;
			}
		}

		if (max_diff_ind > 0) {
			clusters.erase(clusters.begin() + max_diff_ind, clusters.end());
		}
	}

	double computeEpipolarError(const Mat &transformation, const vector<Point2f> &points1, const vector<Point2f> &points2)
	{
		vector<Point2f> points1_temp, points2_temp, points2_proj;
		double error = 0.0;

		// Recalculate inliers for all correspondences
		perspectiveTransform(points1, points2_proj, transformation);
		
		for (size_t pi = 0; pi < points1.size(); pi++) {
			error += norm(points2[pi].ddot(points2_proj[pi]));
		}

		return error / (double)points1.size();
	}

	void removeClusters(std::vector<Cluster> &clusters, int threshold)
	{
		std::vector<Cluster>::iterator iter = clusters.begin();
		while (iter != clusters.end()) {
			if (iter->size() < threshold) {
				iter = clusters.erase(iter);
			}
			else {
				iter++;
			}
		}
	}

	Mat drawMatchesOnImage(const Mat &image1, const Mat &image2, const vector<Point2f> &points1, const vector<Point2f> &points2)
	{
		Size sz = Size(image1.cols + image2.cols, image1.rows >= image2.rows ? image1.rows : image2.rows);
		Mat matchingImage = Mat::zeros(sz, CV_8UC3);

		// Copy first image
		Mat roi1 = Mat(matchingImage, Rect(0, 0, image1.cols, image1.rows));
		image1.copyTo(roi1);
		for (int i = 0; i < (int)points1.size(); i++){
			circle(roi1, points1[i], 3, Scalar(255, 255, 0), 2, 8, 0);
		}

		// Copy second image
		Mat roi2 = Mat(matchingImage, Rect(image1.cols, 0, image2.cols, image2.rows));
		image2.copyTo(roi2);
		for (int i = 0; i < (int)points2.size(); i++){
			circle(roi2, points2[i], 3, Scalar(255, 255, 0), 2, 8, 0);
		}

		// Draw line between nearest neighbor pairs
		for (int i = 0; i < (int)points1.size(); ++i) {
			line(matchingImage, points1[i], Point(image1.cols + points2[i].x, points2[i].y), Scalar(0, 255, 255));
		}

		return matchingImage;
	}

	Mat DrawPointsOnImage(const vector<Point2f> points, const cv::Mat &image, int radius)
	{
		Mat result;

		if (image.channels() == 3) {
			cvtColor(image, result, CV_BGR2GRAY);
			cvtColor(result, result, CV_GRAY2BGR);
		}
		else {
			result = image.clone();
		}

		for (size_t pi = 0; pi < points.size(); pi++) {
			circle(result, points[pi], radius, CV_RGB(0, 255, 0), 2);
		}

		return result;
	}

	Mat DrawPointsOnImage(const Clusters &clusters, const Mat &image, bool drawOutliers, int radius)
	{
		Mat result;
		RNG rng(0xFFFFFFFF);

		if (image.channels() == 3) {
			cvtColor(image, result, CV_BGR2GRAY);
			cvtColor(result, result, CV_GRAY2BGR);
		}
		else {
			result = image.clone();
		}

		for (size_t ci = 0; ci < clusters.size(); ci++)
		{
			int icolor = (unsigned)rng;
			Scalar color = Scalar(icolor & 255, (icolor >> 8) & 255, (icolor >> 16) & 255);

			for (size_t pi = 0; pi < clusters[ci].size(); pi++)
				circle(result, clusters[ci].points1[pi], radius, color, 2);
		}

		if (drawOutliers)
		{
			Cluster outliers = clusters.getOutliers();
			for (size_t pi = 0; pi < outliers.size(); pi++)
			{
				line(result, Point((int)outliers.points1[pi].x - radius, (int)outliers.points1[pi].y - radius),
					Point((int)outliers.points1[pi].x + radius, (int)outliers.points1[pi].y + radius), CV_RGB(255, 0, 0), 2);
				line(result, Point((int)outliers.points1[pi].x + radius, (int)outliers.points1[pi].y - radius),
					Point((int)outliers.points1[pi].x - radius, (int)outliers.points1[pi].y + radius), CV_RGB(255, 0, 0), 2);
			}
		}

		return result;
	}

	Mat DrawPointsOnImageL(const Clusters &clusters, const Mat &image, bool drawOutliers, int radius)
	{
		Mat result;
		Mat legend = Mat::zeros(image.rows, 140, CV_8UC3);
		RNG rng(0xFFFFFFFF);

		if (image.channels() == 3) {
			cvtColor(image, result, CV_BGR2GRAY);
			cvtColor(result, result, CV_GRAY2BGR);
		}
		else {
			result = image.clone();
		}

		for (size_t ci = 0; ci < clusters.size(); ci++)
		{
			int icolor = (unsigned)rng;
			Scalar color = Scalar(icolor & 255, (icolor >> 8) & 255, (icolor >> 16) & 255);

			for (size_t pi = 0; pi < clusters[ci].size(); pi++)
				circle(result, clusters[ci].points1[pi], radius, color, 2);

			circle(legend, Point(10, 5 + ci * 10), 3, color, 2);
			std::ostringstream text;
			text << "cluster " << std::setfill('0') << std::setw(2) << ci;
			putText(legend, text.str(), Point(20, 10 + ci * 10), cv::FONT_HERSHEY_PLAIN, 1, CV_RGB(255, 255, 255));
		}

		if (drawOutliers)
		{
			Cluster outliers = clusters.getOutliers();
			for (size_t pi = 0; pi < outliers.size(); pi++)
			{
				line(result, Point((int)outliers.points1[pi].x - radius, (int)outliers.points1[pi].y - radius),
					Point((int)outliers.points1[pi].x + radius, (int)outliers.points1[pi].y + radius), CV_RGB(255, 0, 0), 2);
				line(result, Point((int)outliers.points1[pi].x + radius, (int)outliers.points1[pi].y - radius),
					Point((int)outliers.points1[pi].x - radius, (int)outliers.points1[pi].y + radius), CV_RGB(255, 0, 0), 2);
			}
		}

		Mat combined(result.rows, result.cols + legend.cols, result.type());
		combined.adjustROI(0, 0, 0, -legend.cols);
		result.copyTo(combined);
		combined.adjustROI(0, 0, -result.cols, legend.cols);
		legend.copyTo(combined);
		combined.adjustROI(0, 0, result.cols, 0);
		result = combined;

		return result;
	}

	Mat DrawPointsOnImage(const vector<Cluster> &clusters, const Mat &image, const Cluster &outliers, int radius)
	{
		Mat result;
		RNG rng(0xFFFFFFFF);

		if (image.channels() == 3) {
			cvtColor(image, result, CV_BGR2GRAY);
			cvtColor(result, result, CV_GRAY2BGR);
		}
		else {
			result = image.clone();
		}

		for (size_t ci = 0; ci < clusters.size(); ci++)
		{
			int icolor = (unsigned)rng;
			Scalar color = Scalar(icolor & 255, (icolor >> 8) & 255, (icolor >> 16) & 255);

			for (size_t pi = 0; pi < clusters[ci].size(); pi++)
				circle(result, clusters[ci].points1[pi], radius, color, 2);
		}

		if (outliers.size() != 0)
		{
			for (size_t pi = 0; pi < outliers.size(); pi++)
			{
				line(result, Point((int)outliers.points1[pi].x - radius, (int)outliers.points1[pi].y - radius),
					Point((int)outliers.points1[pi].x + radius, (int)outliers.points1[pi].y + radius), CV_RGB(255, 0, 0), 2);
				line(result, Point((int)outliers.points1[pi].x + radius, (int)outliers.points1[pi].y - radius),
					Point((int)outliers.points1[pi].x - radius, (int)outliers.points1[pi].y + radius), CV_RGB(255, 0, 0), 2);
			}
		}

		return result;
	}

	Mat DrawPointsOnImageL(const vector<Cluster> &clusters, const Mat &image, const Cluster &outliers)
	{
		Mat result;
		Mat legend = Mat::zeros(image.rows, 140, CV_8UC3);
		RNG rng(0xFFFFFFFF);
		int radius = 3;

		if (image.channels() == 3) {
			cvtColor(image, result, CV_BGR2GRAY);
			cvtColor(result, result, CV_GRAY2BGR);
		}
		else {
			result = image.clone();
		}

		for (size_t ci = 0; ci < clusters.size(); ci++)
		{
			int icolor = (unsigned)rng;
			Scalar color = Scalar(icolor & 255, (icolor >> 8) & 255, (icolor >> 16) & 255);

			for (size_t pi = 0; pi < clusters[ci].size(); pi++)
				circle(result, clusters[ci].points1[pi], 3, color, 2);

			circle(legend, Point(10, 5 + ci * 10), 3, color, 2);
			std::ostringstream text;
			text << "cluster " << std::setfill('0') << std::setw(2) << ci;
			putText(legend, text.str(), Point(20, 10 + ci * 10), cv::FONT_HERSHEY_PLAIN, 1, CV_RGB(255, 255, 255));
		}

		if (outliers.size() != 0)
		{
			for (size_t pi = 0; pi < outliers.size(); pi++)
			{
				line(result, Point((int)outliers.points1[pi].x - radius, (int)outliers.points1[pi].y - radius),
					Point((int)outliers.points1[pi].x + radius, (int)outliers.points1[pi].y + radius), CV_RGB(255, 0, 0), 2);
				line(result, Point((int)outliers.points1[pi].x + radius, (int)outliers.points1[pi].y - radius),
					Point((int)outliers.points1[pi].x - radius, (int)outliers.points1[pi].y + radius), CV_RGB(255, 0, 0), 2);
			}
		}

		Mat combined(result.rows, result.cols + legend.cols, result.type());
		combined.adjustROI(0, 0, 0, -legend.cols);
		result.copyTo(combined);
		combined.adjustROI(0, 0, -result.cols, legend.cols);
		legend.copyTo(combined);
		combined.adjustROI(0, 0, result.cols, 0);
		result = combined;

		return result;
	}

	Mat DrawPointsOnImage(const vector<Cluster> &clusters, const Mat &image, bool drawOutliers, int radius)
	{
		Mat result;
		RNG rng(0xFFFFFFFF);
		int num_of_cluster = drawOutliers ? int(clusters.size() - 1) : (int)clusters.size();

		if (image.channels() == 3) {
			cvtColor(image, result, CV_BGR2GRAY);
			cvtColor(result, result, CV_GRAY2BGR);
		}
		else {
			result = image.clone();
		}

		for (int ci = 0; ci < num_of_cluster; ci++)
		{
			int icolor = (unsigned)rng;
			Scalar color = Scalar(icolor & 255, (icolor >> 8) & 255, (icolor >> 16) & 255);

			for (size_t pi = 0; pi < clusters[ci].size(); pi++)
				circle(result, clusters[ci].points1[pi], radius, color, 2);
		}

		if (drawOutliers)
		{
			for (size_t pi = 0; pi < clusters.back().size(); pi++)
			{
				line(result, Point((int)clusters.back().points1[pi].x - radius, (int)clusters.back().points1[pi].y - radius),
					Point((int)clusters.back().points1[pi].x + radius, (int)clusters.back().points1[pi].y + radius), CV_RGB(255, 0, 0), 2);
				line(result, Point((int)clusters.back().points1[pi].x + radius, (int)clusters.back().points1[pi].y - radius),
					Point((int)clusters.back().points1[pi].x - radius, (int)clusters.back().points1[pi].y + radius), CV_RGB(255, 0, 0), 2);
			}
		}

		return result;
	}

	Mat DrawPointsOnImageL(const vector<Cluster> &clusters, const Mat &image, bool drawOutliers, int radius)
	{
		Mat result;
		Mat legend = Mat::zeros(image.rows, 140, CV_8UC3);
		RNG rng(0xFFFFFFFF);
		int num_of_cluster = drawOutliers ? int(clusters.size() - 1) : (int)clusters.size();

		if (image.channels() == 3) {
			cvtColor(image, result, CV_BGR2GRAY);
			cvtColor(result, result, CV_GRAY2BGR);
		}
		else {
			result = image.clone();
		}

		for (int ci = 0; ci < num_of_cluster; ci++)
		{
			int icolor = (unsigned)rng;
			Scalar color = Scalar(icolor & 255, (icolor >> 8) & 255, (icolor >> 16) & 255);

			for (size_t pi = 0; pi < clusters[ci].size(); pi++)
				circle(result, clusters[ci].points1[pi], radius, color, 2);

			circle(legend, Point(10, 5 + ci * 10), radius, color, 2);
			std::ostringstream text;
			text << "cluster " << std::setfill('0') << std::setw(2) << ci;
			if (clusters[ci].normal_index >= 0 && clusters[ci].normal_index <= 2) {
				text << "-" << clusters[ci].normal_index;
			}
			putText(legend, text.str(), Point(20, 10 + ci * 10), cv::FONT_HERSHEY_PLAIN, 1, CV_RGB(255, 255, 255));
		}

		if (drawOutliers)
		{
			for (size_t pi = 0; pi < clusters.back().size(); pi++)
			{
				line(result, Point((int)clusters.back().points1[pi].x - radius, (int)clusters.back().points1[pi].y - radius),
					Point((int)clusters.back().points1[pi].x + radius, (int)clusters.back().points1[pi].y + radius), CV_RGB(255, 0, 0), 2);
				line(result, Point((int)clusters.back().points1[pi].x + radius, (int)clusters.back().points1[pi].y - radius),
					Point((int)clusters.back().points1[pi].x - radius, (int)clusters.back().points1[pi].y + radius), CV_RGB(255, 0, 0), 2);
			}
		}

		Mat combined(result.rows, result.cols + legend.cols, result.type());
		combined.adjustROI(0, 0, 0, -legend.cols);
		result.copyTo(combined);
		combined.adjustROI(0, 0, -result.cols, legend.cols);
		legend.copyTo(combined);
		combined.adjustROI(0, 0, result.cols, 0);
		result = combined;

		return result;
	}

	vector<Scalar> getColors(int num_of_colors)
	{
		RNG rng(0xFFFFFFFF);
		vector<Scalar> colors;

		for (size_t ci = 0; ci < num_of_colors; ci++)
		{
			int icolor = (unsigned)rng;
			colors.push_back(Scalar(icolor & 255, (icolor >> 8) & 255, (icolor >> 16) & 255));
		}

		return colors;
	}
}
