#include "HomographyEstimator.h"
#include "utility.h"

#include <exception>
#include <fstream>

// openCV structures
using cv::Mat;
using cv::Mat_;
using cv::Point2f;
using cv::Point3f;

// openCV functions
using cv::mean;
using cv::perspectiveTransform;

// standard library
using std::vector;
using std::cout;
using std::cerr;
using std::endl;
using std::out_of_range;

using std::ofstream;

HomographyEstimator::HomographyEstimator(void) : m_modelType(HOMOGRAPHY_UNCONSTRAINED), reprojection_thr(3.0),
	mInlierThreshold(3, 3.0)
{
}

HomographyEstimator::HomographyEstimator(double reprojection_threshold) :
	reprojection_thr(reprojection_threshold), mInlierThreshold(3, reprojection_threshold)
{
}

HomographyEstimator::HomographyEstimator(const HOMOGRAPHY_TYPE model_type)
{
	m_modelType = model_type;
}

HomographyEstimator::HomographyEstimator(const HOMOGRAPHY_TYPE model_type, const Mat &K, const Mat R1, const Mat R2,
	const vector<Mat> normals) : reprojection_thr(2.5), mInlierThreshold(3, 2.5)
{
	m_modelType = model_type;

	setParameters(K, K, R1, R2, normals);
}

HomographyEstimator::HomographyEstimator(const HOMOGRAPHY_TYPE model_type, const Mat &K1, const Mat &K2, const Mat R1,
	const Mat R2, const vector<Mat> normals) : reprojection_thr(2.5), mInlierThreshold(3, 2.5)
{
	m_modelType = model_type;

	setParameters(K1, K2, R1, R2, normals);
}

HomographyEstimator::~HomographyEstimator(void)
{
}

Mat HomographyEstimator::computeMWHomographyMinError(const vector<Point2f> &points1,
	const vector<Point2f> &points2, int &normal_index, Mat &t_hat) const
{
	if (points1.size() < 2 || points2.size() < 2) {
		return Mat();
	}

    Mat H_opt;
	Mat current_t_hat;
	double opt_diff = DBL_MAX;

	for (unsigned int ni = 0; ni < mNormals.size(); ni++) {
		if (mActiveNormals[ni]) {
			Mat H = computeMWHomography(points1, points2, ni, current_t_hat);

			vector<Point2f> points1_t;
			perspectiveTransform(points1, points1_t, H);
			double diff = cv::norm(points1_t, points2);
			//double diff = norm(points1_t[0] - points2[0]) + norm(points1_t[1] - points2[1]);
			if (diff < opt_diff)
			{
				opt_diff = diff;
				H_opt = H.clone();
				t_hat = current_t_hat;
				normal_index = ni;
			}
		}
	}

	if (H_opt.empty()) {
		if (points1.size() >= 4) {
			H_opt = cv::findHomography(points1, points2);
		}
		else {
			H_opt = Mat::eye(3, 3, CV_64F);
		}
	}

	return H_opt;
}

void HomographyEstimator::computeMWHomographyMaxInliers(Cluster &cluster, bool useMultipleThresholds) const
{
	assert(cluster.points1.size() >= 2 && cluster.points2.size() >= 2);

	int num_inliers_opt = 0;

	for (unsigned int ni = 0; ni < mNormals.size(); ni++)
	{
		if (mActiveNormals[ni])
		{
			Mat t_hat;
			Mat H = computeMWHomography(cluster.points1, cluster.points2, ni, t_hat);
			double threshold;

			if (useMultipleThresholds) {
				threshold = getThreshold(ni);
			}
			else {
				threshold = getThreshold();
			}

			vector<Point2f> points1_t;
			perspectiveTransform(cluster.points1, points1_t, H);

			int num_inliers = 0;
			for (int pi = 0; pi < cluster.points1.size(); pi++) {
				if (cv::norm(points1_t[pi] - cluster.points2[pi]) < threshold) {
					num_inliers++;
				}
			}

			if (num_inliers_opt < num_inliers)
			{
				num_inliers_opt = num_inliers;
				cluster.Homography = H.clone();
				cluster.normal_index = ni;
				cluster.translation = t_hat.clone();
			}
		}
	}

	if (cluster.Homography.empty()) {
		if (cluster.points1.size() >= 4) {
			cluster.Homography = cv::findHomography(cluster.points1, cluster.points2);
		}
		else {
			cluster.Homography = Mat::eye(3, 3, CV_64F);
		}
	}
}

Mat HomographyEstimator::computeMWHomography(const vector<Point2f> &points1, const vector<Point2f> &points2,
	const unsigned int normal_index, Mat &t_hat) const
{
	assert(normal_index < mNormals.size());
	int n_points = (int)points1.size();
	Mat p1(3, n_points, CV_64F), p2(3, n_points, CV_64F);

	// Points setting
	for (int i = 0; i < n_points; i++)
	{
		p1.at<double>(0, i) = points1[i].x;
		p1.at<double>(1, i) = points1[i].y;
		p1.at<double>(2, i) = 1.0;
		
		p2.at<double>(0, i) = points2[i].x;
		p2.at<double>(1, i) = points2[i].y;
		p2.at<double>(2, i) = 1.0;
	}

	Mat invKp1 = K1.inv() * p1;
	Mat invKp2 = K2.inv() * p2;
	Mat alpha = mNormals[normal_index].t() * invKp1;
	Mat b(n_points*2, 1, CV_64F);
	Mat A = Mat::zeros(n_points*2, 3, CV_64F);
	for (int pi = 0; pi < n_points; pi++)
	{
		b.at<double>(pi*2, 0) = R.at<double>(0,0)*invKp1.at<double>(0,pi) + R.at<double>(0,1)*invKp1.at<double>(1,pi) + R.at<double>(0,2)
			- R.at<double>(2,0)*invKp1.at<double>(0,pi)*invKp2.at<double>(0,pi) - R.at<double>(2,1)*invKp1.at<double>(1,pi)*invKp2.at<double>(0,pi) - R.at<double>(2,2)*invKp2.at<double>(0,pi);

		b.at<double>(pi*2+1, 0) = R.at<double>(1,0)*invKp1.at<double>(0,pi) + R.at<double>(1,1)*invKp1.at<double>(1,pi) + R.at<double>(1,2)
			- R.at<double>(2,0)*invKp1.at<double>(0,pi)*invKp2.at<double>(1,pi) - R.at<double>(2,1)*invKp1.at<double>(1,pi)*invKp2.at<double>(1,pi) - R.at<double>(2,2)*invKp2.at<double>(1,pi);

		A.at<double>(pi*2, 0) = -alpha.at<double>(pi);
		A.at<double>(pi*2+1, 1) = -alpha.at<double>(pi);
		A.at<double>(pi*2, 2) = alpha.at<double>(pi) * invKp2.at<double>(0, pi);
		A.at<double>(pi*2+1, 2) = alpha.at<double>(pi) * invKp2.at<double>(1, pi);
	}

	t_hat = (A.t() * A).inv() * A.t() * b;

	return Mat(K2 * (R + t_hat * mNormals[normal_index].t()) * K1.inv());
}

Mat HomographyEstimator::computeWMWHomographyMinError(const vector<Point2f> &points1, const vector<Point2f> &points2) const
{
	Mat H_opt;
	Mat current_t_hat;
	double opt_diff = DBL_MAX;

	for (unsigned int ni = 0; ni < mNormals.size(); ni++) {
		if (mActiveNormals[ni]) {
			Mat H = computeWMWHomography(points1, points2, mNormals[ni]);

			vector<Point2f> points1_t;
			perspectiveTransform(points1, points1_t, H);
			double diff = cv::norm(points1_t, points2);
			if (diff < opt_diff)
			{
				opt_diff = diff;
				H_opt = H.clone();
			}
		}
	}

	if (H_opt.empty()) {
		if (points1.size() >= 4) {
			H_opt = cv::findHomography(points1, points2);
		}
		else {
			H_opt = Mat::eye(3, 3, CV_64F);
		}
	}

	return H_opt;
}

Mat HomographyEstimator::computeWMWHomography(const vector<Point2f> &points1, const vector<Point2f> &points2) const
{
	int n_points = (int)points1.size();
	Mat p1(3, n_points, CV_64F), p2(3, n_points, CV_64F);

	assert(points1.size() >= 3 && points2.size() >= 3);

	// Points setting
	for (int i = 0; i < n_points; i++)
	{
		p1.at<double>(0, i) = points1[i].x;
		p1.at<double>(1, i) = points1[i].y;
		p1.at<double>(2, i) = 1.0;

		p2.at<double>(0, i) = points2[i].x;
		p2.at<double>(1, i) = points2[i].y;
		p2.at<double>(2, i) = 1.0;
	}

	// Retrieve X and Z axes
	Mat angleAxisRotation, R_xz;
	cv::Rodrigues(R, angleAxisRotation);
	angleAxisRotation.at<double>(1) = 0.0;	// remove rotation component around Y axis
	cv::Rodrigues(angleAxisRotation, R_xz);

	// Calculate 3D-2D points (rays)
	Mat r1 = R_xz * K1.inv() * p1;	// pre-rotate the points
	Mat r2 = K2.inv() * p2;

	// Normalization
	for (int i = 0; i < n_points; i++)
	{
		r1.at<double>(0, i) = r1.at<double>(0, i) / r1.at<double>(2, i);
		r1.at<double>(1, i) = r1.at<double>(1, i) / r1.at<double>(2, i);
		r1.at<double>(2, i) = r1.at<double>(2, i) / r1.at<double>(2, i);
		r2.at<double>(0, i) = r2.at<double>(0, i) / r2.at<double>(2, i);
		r2.at<double>(1, i) = r2.at<double>(1, i) / r2.at<double>(2, i);
		r2.at<double>(2, i) = r2.at<double>(2, i) / r2.at<double>(2, i);
	}

	// Estimate a homography
	Mat A = Mat::zeros(n_points * 2, 6, CV_64F);
	Mat b(n_points * 2, 1, CV_64F);
	for (int i = 0; i < n_points; i++)
	{
		double x1 = r1.at<double>(0, i);
		double x2 = r2.at<double>(0, i);
		double y1 = r1.at<double>(1, i);
		double y2 = r2.at<double>(1, i);

		A.at<double>(i * 2, 2) = -x1;
		A.at<double>(i * 2, 3) = -1.0;
		A.at<double>(i * 2, 4) = x1 * y2;
		A.at<double>(i * 2, 5) = y2;

		A.at<double>(i * 2 + 1, 0) = -x1 * y2;
		A.at<double>(i * 2 + 1, 1) = -y2;
		A.at<double>(i * 2 + 1, 2) = x1 * x2;
		A.at<double>(i * 2 + 1, 3) = x2;

		b.at<double>(i * 2, 0) = y1;
		b.at<double>(i * 2 + 1, 0) = -x2 * y1;
	}

	Mat h = (A.t()*A).inv()*A.t()*b;
	return K2 * (Mat_<double>(3, 3) << h.at<double>(0), 0, h.at<double>(1), h.at<double>(2), 1, h.at<double>(3),
		h.at<double>(4), 0, h.at<double>(5)) * R_xz * K1.inv();
}

Mat HomographyEstimator::computeWMWHomography(const vector<Point2f> &points1, const vector<Point2f> &points2, Mat normal) const
{
	int n_points = (int)points1.size();
	Mat p1(3, n_points, CV_64F), p2(3, n_points, CV_64F);

	assert(points1.size() >= 3 && points2.size() >= 3);

	// Points setting
	for (int i = 0; i < n_points; i++)
	{
		p1.at<double>(0, i) = points1[i].x;
		p1.at<double>(1, i) = points1[i].y;
		p1.at<double>(2, i) = 1.0;

		p2.at<double>(0, i) = points2[i].x;
		p2.at<double>(1, i) = points2[i].y;
		p2.at<double>(2, i) = 1.0;
	}

	// Retrieve X and Z axes
	Mat angleAxisRotation, R_xz;
	cv::Rodrigues(R, angleAxisRotation);
	angleAxisRotation.at<double>(1) = 0.0;	// remove rotation component around Y axis
	cv::Rodrigues(angleAxisRotation, R_xz);

	// Calculate 3D-2D points (rays)
	Mat r1 = R_xz * K1.inv() * p1;	// pre-rotate the points
	Mat r2 = K2.inv() * p2;

	// Rotate normal vector to compute n_x/n_z
	Mat normal_rotated = normal.t() * R_xz;
	double nx_nz_ratio = normal_rotated.at<double>(0) / normal_rotated.at<double>(2);

	// Normalization
	for (int i = 0; i < n_points; i++)
	{
		r1.at<double>(0, i) = r1.at<double>(0, i) / r1.at<double>(2, i);
		r1.at<double>(1, i) = r1.at<double>(1, i) / r1.at<double>(2, i);
		r1.at<double>(2, i) = r1.at<double>(2, i) / r1.at<double>(2, i);
		r2.at<double>(0, i) = r2.at<double>(0, i) / r2.at<double>(2, i);
		r2.at<double>(1, i) = r2.at<double>(1, i) / r2.at<double>(2, i);
		r2.at<double>(2, i) = r2.at<double>(2, i) / r2.at<double>(2, i);
	}

	// Estimate a homography
	Mat A = Mat::zeros(n_points * 2, 5, CV_64F);
	Mat b(n_points * 2, 1, CV_64F);
	for (int i = 0; i < n_points; i++)
	{
		double x1 = r1.at<double>(0, i);
		double x2 = r2.at<double>(0, i);
		double y1 = r1.at<double>(1, i);
		double y2 = r2.at<double>(1, i);

		A.at<double>(i * 2, 2) = -x1 - nx_nz_ratio;
		A.at<double>(i * 2, 3) = x1 * y2;
		A.at<double>(i * 2, 4) = y2;

		A.at<double>(i * 2 + 1, 0) = -x1 * y2;
		A.at<double>(i * 2 + 1, 1) = -y2;
		A.at<double>(i * 2 + 1, 2) = x1 * x2 + nx_nz_ratio * x2;

		b.at<double>(i * 2, 0) = y1;
		b.at<double>(i * 2 + 1, 0) = -x2 * y1;
	}

	Mat h = (A.t()*A).inv()*A.t()*b;
	return K2 * (Mat_<double>(3, 3) << h.at<double>(0), 0, h.at<double>(1), h.at<double>(2), 1, nx_nz_ratio * h.at<double>(2),
		h.at<double>(3), 0, h.at<double>(4)) * R_xz * K1.inv();
}

//--------------------------------------
// Public Functions
//--------------------------------------

int HomographyEstimator::getMinimumSampleSet() const
{
	switch (m_modelType)
	{
	case HOMOGRAPHY_MANHATTAN_WORLD:
		return 2;
	case HOMOGRAPHY_WEAK_MANHATTAN_WORLD:
		return 3;
	case HOMOGRAPHY_UNCONSTRAINED:
	default:
		return 4;
	}
}

HOMOGRAPHY_TYPE HomographyEstimator::getModelType() const
{
	return m_modelType;
}

void HomographyEstimator::setModelType(HOMOGRAPHY_TYPE model_type)
{
	if ((int)model_type >= 4 || (int)model_type <= 0)
		m_modelType = HOMOGRAPHY_UNCONSTRAINED;
	else
		m_modelType = model_type;
}

bool HomographyEstimator::loadCalibrations(const std::string &filename1, const std::string &filename2)
{
	K1 = IOUtils::load3x3Matrix(filename1);
	if (K1.empty())
		return false;

	K2 = IOUtils::load3x3Matrix(filename2);
	if (K2.empty())
		return false;

	cout << "--Calibration matrices--" << endl;
	cout << K1 << endl;
	cout << K2 << endl;
	cout << "------------------------" << endl;

	return true;
}

bool HomographyEstimator::loadRotations(const std::string &filename) {

	R = IOUtils::load3x3Matrix(filename);

	if (R.empty()) {
		return false;
	}

	return true;
}

bool HomographyEstimator::loadRotations(const std::string &filename1, const std::string &filename2)
{
	R1 = IOUtils::load3x3Matrix(filename1);
	if (R1.empty())
		return false;

	R2 = IOUtils::load3x3Matrix(filename2);
	if (R2.empty())
		return false;

	// DEBUG
	/*
	{
		Mat angles = (cv::Mat_<double>(3, 1) << 0.0101, 0.0101, 0.0101);
		Mat R_;
		cv::Rodrigues(angles, R_);
		R1 = R1 * R_;
	}*/

	// Initialize normal vectors
	mNormals = vector<Mat>(3);
	mActiveNormals = vector<bool>(3, true);
	mNormals[0] = (Mat_<double>(3, 1) << R1.at<double>(0, 0), R1.at<double>(1, 0), R1.at<double>(2, 0));
	mNormals[1] = (Mat_<double>(3, 1) << R1.at<double>(0, 1), R1.at<double>(1, 1), R1.at<double>(2, 1));
	mNormals[2] = (Mat_<double>(3, 1) << R1.at<double>(0, 2), R1.at<double>(1, 2), R1.at<double>(2, 2));

	// Initialize rotation matrix
	R = R2 * R1.inv();

	cout << R1 << endl;
	cout << R2 << endl;
	cout << R << endl;

	return true;
}

void HomographyEstimator::setParameters(const Mat &K, const Mat R1, const Mat R2, const vector<Mat> normals)
{
	setParameters(K, K, R1, R2, normals);
}

void HomographyEstimator::setParameters(const Mat &K1, const Mat &K2, const Mat R1, const Mat R2, const vector<Mat> normals)
{
	this->K1 = K1.clone();
	this->K2 = K2.clone();

	this->R1 = R1.clone();
	this->R2 = R2.clone();

	this->R = R2 * R1.inv();

	mNormals = normals;
	mActiveNormals = vector<bool>(normals.size(), true);
}

Mat HomographyEstimator::getHomography(const vector<Point2f> &points1, const std::vector<Point2f> &points2, int normal_index) const
{
	Mat H;

	// Validation
	assert(points1.size() == points2.size());

	switch (m_modelType)
	{
	case HOMOGRAPHY_WEAK_MANHATTAN_WORLD:

		H = findWMWHomographies(points1, points2);
		//H = getWMWHomographyMinError(points1, points2);

	case HOMOGRAPHY_MANHATTAN_WORLD:

		if (normal_index < 0 || normal_index > 2)
			H = computeMWHomographyMinError(points1, points2, normal_index);
		else
			H = computeMWHomography(points1, points2, normal_index);
		
	case HOMOGRAPHY_UNCONSTRAINED:
	default:
		H = cv::findHomography(points1, points2);
		if (H.empty()) {
			H = Mat::eye(3, 3, CV_64F);
		}
	}

	return H;
}

Mat HomographyEstimator::getHomography(const vector<Point2f> &points1, const vector<Point2f> &points2, int &normal_index, Mat &t)
{
	// Validation
	assert(points1.size() == points2.size());

	switch (m_modelType)
	{
	case HOMOGRAPHY_WEAK_MANHATTAN_WORLD:

		return computeWMWHomography(points1, points2);

	case HOMOGRAPHY_MANHATTAN_WORLD:

		if (normal_index < 0 || normal_index > 2)
			return computeMWHomographyMinError(points1, points2, normal_index, t);
		else
			return computeMWHomography(points1, points2, normal_index, t);
		
	case HOMOGRAPHY_UNCONSTRAINED:
	default:

		return cv::findHomography(points1, points2);
	}
}

cv::Mat HomographyEstimator::findWMWHomographies(const vector<Point2f> &points1, const vector<Point2f> &points2) const
{
	return computeWMWHomography(points1, points2);
}

void HomographyEstimator::findWMWHomographies(const vector<Point2f> &points1, const vector<Point2f> &points2,
	vector<HomographyModel> &models) const
{
	int n_points = (int)points1.size();
	Mat p1(3, n_points, CV_64F), p2(3, n_points, CV_64F);

	assert(points1.size() > 2 && points2.size() > 2);

	// Points setting
	for (int i = 0; i < n_points; i++)
	{
		p1.at<double>(0, i) = points1[i].x;
		p1.at<double>(1, i) = points1[i].y;
		p1.at<double>(2, i) = 1.0;

		p2.at<double>(0, i) = points2[i].x;
		p2.at<double>(1, i) = points2[i].y;
		p2.at<double>(2, i) = 1.0;
	}

	// Retrieve X and Z axes
	Mat angleAxisRotation, R_xz;
	cv::Rodrigues(R, angleAxisRotation);
	angleAxisRotation.at<double>(1) = 0.0;	// remove rotation component around Y axis
	cv::Rodrigues(angleAxisRotation, R_xz);

	// Calculate 3D-2D points (rays)
	Mat r1 = R_xz * K1.inv() * p1;	// pre-rotate the points
	Mat r2 = K2.inv() * p2;

	// Normalization
	for (int i = 0; i < n_points; i++)
	{
		r1.at<double>(0, i) = r1.at<double>(0, i) / r1.at<double>(2, i);
		r1.at<double>(1, i) = r1.at<double>(1, i) / r1.at<double>(2, i);
		r1.at<double>(2, i) = r1.at<double>(2, i) / r1.at<double>(2, i);
		r2.at<double>(0, i) = r2.at<double>(0, i) / r2.at<double>(2, i);
		r2.at<double>(1, i) = r2.at<double>(1, i) / r2.at<double>(2, i);
		r2.at<double>(2, i) = r2.at<double>(2, i) / r2.at<double>(2, i);
	}

	// Fill up matrix A and B
	Mat A = Mat::zeros(5, 6, CV_64F);
	Mat b(5, 1, CV_64F);
	for (int i = 0; i < 2; i++)
	{
		double x1 = r1.at<double>(0, i);
		double x2 = r2.at<double>(0, i);
		double y1 = r1.at<double>(1, i);
		double y2 = r2.at<double>(1, i);

		A.at<double>(i * 2, 2) = -x1;
		A.at<double>(i * 2, 3) = -1.0;
		A.at<double>(i * 2, 4) = x1 * y2;
		A.at<double>(i * 2, 5) = y2;

		A.at<double>(i * 2 + 1, 0) = -x1 * y2;
		A.at<double>(i * 2 + 1, 1) = -y2;
		A.at<double>(i * 2 + 1, 2) = x1 * x2;
		A.at<double>(i * 2 + 1, 3) = x2;

		b.at<double>(i * 2, 0) = y1;
		b.at<double>(i * 2 + 1, 0) = -x2 * y1;
	}
	A.at<double>(4, 2) = -r1.at<double>(0, 2);
	A.at<double>(4, 3) = -1.0;
	A.at<double>(4, 4) = r1.at<double>(0, 2) * r2.at<double>(1, 2);
	A.at<double>(4, 5) = r2.at<double>(1, 2);
	b.at<double>(4, 0) = r1.at<double>(1, 2);

	// Deficient rank least squares
	Mat W, U, VT;
	cv::SVDecomp(A, W, U, VT, cv::SVD::FULL_UV);

	Mat b_prime = U.t() * b;
	Mat y = Mat::zeros(6, 1, CV_64F);
	y.at<double>(0) = b_prime.at<double>(0) / W.at<double>(0);
	y.at<double>(1) = b_prime.at<double>(1) / W.at<double>(1);
	y.at<double>(2) = b_prime.at<double>(2) / W.at<double>(2);
	y.at<double>(3) = b_prime.at<double>(3) / W.at<double>(3);
	y.at<double>(4) = b_prime.at<double>(4) / W.at<double>(4);

	// h = Vy + w*v5, where w is the unknown
	Mat Vy = VT.t() * y;
	Mat v5 = VT.row(5);

	// Compute solution of det(H'H - I)
	vector<double> solutions = solveDeficientRankSystem(Vy, v5);

	for (unsigned int i = 0; i < solutions.size(); i++) {
		Mat h = Vy + v5.t() * solutions[i];
		models.push_back(HomographyModel(K2 * (Mat_<double>(3, 3) << h.at<double>(0), 0, h.at<double>(1), h.at<double>(2), 1, h.at<double>(3),
			h.at<double>(4), 0, h.at<double>(5)) * R_xz * K1.inv(), reprojection_thr));
	}
}

Mat HomographyEstimator::findMWHomography(const vector<Point2f> &points1,
	const vector<Point2f> &points2, const int normal_index) const
{
	assert(normal_index >= 0 && normal_index <= 2);
	return computeMWHomography(points1, points2, normal_index);
}

void HomographyEstimator::findMWHomographies(const vector<Point2f> &points1,
	const vector<Point2f> &points2, vector<Mat> &models) const
{
	// The calibration matrices, mNormals, rotation matrix and vanishing directions must not empty
	assert(!mNormals[0].empty() && !mNormals[1].empty() && !mNormals[2].empty() && !R.empty());

	for (unsigned int ni = 0; ni < mNormals.size(); ni++) {
		if (mActiveNormals[ni]) {
			models.push_back(computeMWHomography(points1, points2, ni));
			models.push_back(computeMWHomography(points1, points2, ni));
			models.push_back(computeMWHomography(points1, points2, ni));
		}
	}
}

void HomographyEstimator::findMWHomographies(const vector<Point2f> &points1,
	const vector<Point2f> &points2, vector<HomographyModel> &models) const
{
	// The calibration matrices, mNormals, rotation matrix and vanishing directions must not empty
	assert(!mNormals[0].empty() && !mNormals[1].empty() && !mNormals[2].empty() && !R.empty());

	HomographyModel model;

	// Compute vanishing points
	Mat VL = computeVanishingLines(K1, R1);

	for (unsigned int ni = 0; ni < mNormals.size(); ni++) {
		if (mActiveNormals[ni]) {
			vector<Point2f> points1_subset, points2_subset;

			pickLargerGroupVL(points1, points2, VL.col(ni), points1_subset, points2_subset);
			//points1_subset = points1; points2_subset = points2;

			if (points1_subset.size() >= getMinimumSampleSet()) {
				model.Homography = computeMWHomography(points1_subset, points2_subset, ni, model.translation);
				model.plane_normal = mNormals[ni];
				model.normal_index = ni;
				model.tag = round(computeLineDistance(VL.col(ni), points1_subset[0]));
				model.scale = mInlierThreshold[ni];
				
				models.push_back(model);
			}
		}
	}
}

void HomographyEstimator::drawVanishingLines(Mat image1, Mat image2, std::string filename1, std::string filename2) const
{
	// Compute vanishing points
	cout << R1 << endl;
	Mat VL1 = computeVanishingLines(K1, R1);
	Mat VL2 = computeVanishingLines(K2, R2);
	
	imwrite(filename1, drawLinesOnImage(image1, VL1));
	imwrite(filename2, drawLinesOnImage(image2, VL2));
}

void HomographyEstimator::computeParameters(Cluster &cluster, int type, bool useMultipleThresholds) const
{
	int minimumSampleSet = getMinimumSampleSet();
	assert(cluster.points1.size() >= minimumSampleSet && cluster.points2.size() >= minimumSampleSet);

	if (type == PARAMS_MIN_ERROR) {
		cluster.Homography = computeMWHomographyMinError(cluster.points1, cluster.points2, cluster.normal_index, cluster.translation);
		cluster.plane_normal = mNormals[cluster.normal_index].clone();
	}
	else if (type == PARAMS_MAX_INLIERS) {
		computeMWHomographyMaxInliers(cluster, useMultipleThresholds);
	}
	else {
		throw std::exception("Unknown parameter value:" + type);
	}
}

void HomographyEstimator::computeParameters(vector<Cluster> &clusters, int type, bool useMultipleThresholds) const
{
	for (unsigned int ci = 0; ci < clusters.size(); ci++) {
		computeParameters(clusters[ci], type, useMultipleThresholds);
	}
}

void HomographyEstimator::computeParameters(Clusters &clusters, int type, bool useMultipleThresholds) const
{
	for (unsigned int ci = 0; ci < clusters.size(); ci++) {
		computeParameters(clusters[ci], type, useMultipleThresholds);
	}
}

Mat HomographyEstimator::computeParameters(const std::vector<cv::Point2f> &points1, const std::vector<cv::Point2f> &points2,
	Mat &t_hat, cv::Mat &plane_normal) const
{
	if (points1.size() < 2 || points2.size() < 2) {
		return Mat();
	}

	int normal_index;
	Mat H = computeMWHomographyMinError(points1, points2, normal_index, t_hat);
	plane_normal = mNormals[normal_index].clone();

	return H;
}

Mat HomographyEstimator::computeEssentialMatrix(Cluster& cluster) const
{
	computeParameters(cluster.points1, cluster.points2, cluster.translation, cluster.plane_normal);

	Mat crossTranslation = Mat::zeros(3, 3, CV_64F);

	crossTranslation.at<double>(0, 1) = -cluster.translation.at<double>(2);
	crossTranslation.at<double>(0, 2) = cluster.translation.at<double>(1);
	crossTranslation.at<double>(1, 0) = cluster.translation.at<double>(2);
	crossTranslation.at<double>(1, 2) = -cluster.translation.at<double>(0);
	crossTranslation.at<double>(2, 0) = -cluster.translation.at<double>(1);
	crossTranslation.at<double>(2, 1) = cluster.translation.at<double>(0);

	return crossTranslation * R;
}

Mat HomographyEstimator::computeEssentialMatrix(const Mat &translation) const
{
	Mat crossTranslation = Mat::zeros(3, 3, CV_64F);

	crossTranslation.at<double>(0, 1) = -translation.at<double>(2);
	crossTranslation.at<double>(0, 2) = translation.at<double>(1);
	crossTranslation.at<double>(1, 0) = translation.at<double>(2);
	crossTranslation.at<double>(1, 2) = -translation.at<double>(0);
	crossTranslation.at<double>(2, 0) = -translation.at<double>(1);
	crossTranslation.at<double>(2, 1) = translation.at<double>(0);
	
	return K2 * crossTranslation * R * K1.inv();
}

Mat HomographyEstimator::computeFundamentalMatrix(Cluster& cluster)
{
	computeParameters(cluster.points1, cluster.points2, cluster.translation, cluster.plane_normal);

	Mat crossTranslation = Mat::zeros(3, 3, CV_64F);

	crossTranslation.at<double>(0, 1) = -cluster.translation.at<double>(2);
	crossTranslation.at<double>(0, 2) = cluster.translation.at<double>(1);
	crossTranslation.at<double>(1, 0) = cluster.translation.at<double>(2);
	crossTranslation.at<double>(1, 2) = -cluster.translation.at<double>(0);
	crossTranslation.at<double>(2, 0) = -cluster.translation.at<double>(1);
	crossTranslation.at<double>(2, 1) = cluster.translation.at<double>(0);

	return (K2.inv()).t() * (crossTranslation * R) * K1.inv();
}

Mat HomographyEstimator::getVanishingLine(const int normalIndex) const
{
	Mat VL = computeVanishingLines(K1, R1);

	return VL.col(normalIndex);
}

Mat HomographyEstimator::getVanishingLine(const Mat& normal) const
{
	Mat VL = computeVanishingLines(K1, R1);

	if (norm(R1.col(0) - normal) <= 1e-6) {
		return VL.col(0);
	}
	else if (norm(R1.col(1) - normal) <= 1e-6) {
		return VL.col(1);
	}
	else if (norm(R1.col(2) - normal) <= 1e-6) {
		return VL.col(2);
	}
	else {
		throw std::invalid_argument("Given normal does not exist in Manhattan world");
	}
}

Mat HomographyEstimator::getVanishingLines() const
{
	return computeVanishingLines(K1, R1);
}

vector<Point3f> HomographyEstimator::compute3DPoints(const vector<Point2f> &points, const Mat &normal, const double d) const
{
	vector<Point3f> points3D;

	for (unsigned int pi = 0; pi < points.size(); pi++) {
		Point3f point3D;
		double x = (double(points[pi].x) - K1.at<double>(0, 2)) / K1.at<double>(0, 0);
		double y = (double(points[pi].y) - K1.at<double>(1, 2)) / K1.at<double>(1, 1);
		point3D.z = float(d / (normal.at<double>(0) * x + normal.at<double>(1) * y + normal.at<double>(2)));
		point3D.x = float(x) * point3D.z;
		point3D.y = float(y) * point3D.z;
		points3D.push_back(point3D);
	}

	return points3D;
}

Point3f HomographyEstimator::compute3DPoint(const Point2f &point, const Mat &normal, const double d) const
{
	Point3f point3D;

	double x = (double(point.x) - K1.at<double>(0, 2)) / K1.at<double>(0, 0);
	double y = (double(point.y) - K1.at<double>(1, 2)) / K1.at<double>(1, 1);
	point3D.z = float(d / (normal.at<double>(0) * x + normal.at<double>(1) * y + normal.at<double>(2)));
	point3D.x = float(x) * point3D.z;
	point3D.y = float(y) * point3D.z;

	return point3D;
}

double HomographyEstimator::computeDepth(const double x, const double y, const cv::Mat &normal, const float d) const
{
	return (d - normal.at<double>(0) * x - normal.at<double>(1) * y) / normal.at<double>(2);
}

int HomographyEstimator::getNormalIndex(char dir) const
{
	int targetIndex = -1;
	int vectorIndex = -1;
	double max_val = 0.f;

	switch (dir)
	{
	case 'x':
	case 'X':
		vectorIndex = 0;

		break;

	case 'y':
	case 'Y':
		vectorIndex = 1;

		break;

	case 'z':
	case 'Z':
		vectorIndex = 2;

		break;

	default:
		return -1;
	}

	for (int normalIndex = 0; normalIndex < mNormals.size(); normalIndex++) {
		if (abs(mNormals[normalIndex].at<double>(vectorIndex)) > max_val) {
			max_val = abs(mNormals[normalIndex].at<double>(vectorIndex));
			targetIndex = normalIndex;
		}
	}

	return targetIndex;
}

void HomographyEstimator::activateNormal(int ind)
{
	assert(ind < mActiveNormals.size());
	mActiveNormals[ind] = true;
}

void HomographyEstimator::activateNormal(char dir)
{
	activateNormal(getNormalIndex(dir));
}

void HomographyEstimator::activateAllNormals()
{
	assert(!mActiveNormals.empty() && !mNormals.empty());
	for (unsigned int i = 0; i < mActiveNormals.size(); i++) {
		mActiveNormals[i] = true;
	}
}

void HomographyEstimator::deactivateNormal(int ind)
{
	assert(ind >= 0 && ind < mActiveNormals.size() && !mNormals.empty());
	mActiveNormals[ind] = false;
}

void HomographyEstimator::deactivateNormal(char dir)
{
	int targetIndex = -1;
	int vectorIndex = -1;
	double max_val = 0.f;

	switch (dir)
	{
	case 'x':
	case 'X':
		vectorIndex = 0;

		break;

	case 'y':
	case 'Y':
		vectorIndex = 1;

		break;

	case 'z':
	case 'Z':
		vectorIndex = 2;

		break;
	}

	for (int normalIndex = 0; normalIndex < mNormals.size(); normalIndex++) {
		if (abs(mNormals[normalIndex].at<double>(vectorIndex)) > max_val) {
			max_val = abs(mNormals[normalIndex].at<double>(vectorIndex));
			targetIndex = normalIndex;
		}
	}

	deactivateNormal(targetIndex);
}

void HomographyEstimator::deactivateAllNormals()
{
	assert(!mActiveNormals.empty() && !mNormals.empty());
	for (unsigned int i = 0; i < mActiveNormals.size(); i++) {
		mActiveNormals[i] = false;
	}
}

void HomographyEstimator::setNormalActivation(std::vector<bool> activation)
{
	assert(activation.size() == mNormals.size());
	mActiveNormals = activation;
}

double HomographyEstimator::getThreshold() const
{
	return reprojection_thr;
}

double HomographyEstimator::getThreshold(int normalIndex) const
{
	return mInlierThreshold[normalIndex];
}

void HomographyEstimator::setThreshold(char dir, double threshold)
{
	int targetIndex = -1;
	int vectorIndex = -1;
	double max_val = 0.f;

	switch (dir)
	{
	case 'x':
	case 'X':
		vectorIndex = 0;

		break;

	case 'y':
	case 'Y':
		vectorIndex = 1;

		break;

	case 'z':
	case 'Z':
		vectorIndex = 2;

		break;
	}

	// Find the actual normal index
	for (int normalIndex = 0; normalIndex < mNormals.size(); normalIndex++) {
		if (abs(mNormals[normalIndex].at<double>(vectorIndex)) > max_val) {
			max_val = abs(mNormals[normalIndex].at<double>(vectorIndex));
			targetIndex = normalIndex;
		}
	}

	if (mInlierThreshold.size() != 3) {
		mInlierThreshold.resize(3, 2.5);
	}

	mInlierThreshold[targetIndex] = threshold;
}

void HomographyEstimator::setThresholds(vector<double> thresholds)
{
	assert(thresholds.size() == 3);

	setThreshold('x', thresholds[0]);
	setThreshold('y', thresholds[1]);
	setThreshold('z', thresholds[2]);
}

void HomographyEstimator::printNormals() const
{
	for (unsigned int i = 0; i < mNormals.size(); i++) {
		if (mActiveNormals[i]) {
			cout << "(Active) ";
		}
		else {
			cout << "(Inactive) ";
		}
		cout << mNormals[i].t() << endl;
	}
}

bool HomographyEstimator::loadParameters(const IOUtils::TwoViewParamPathes &twoViewParamPathes)
{
	// Load calibration matrices for each camera corresponding each image.
	// It could be the same
	if (!loadCalibrations(twoViewParamPathes.pathCamCalib1, twoViewParamPathes.pathCamCalib2))
	{
		IOUtils::showMessage("Failed to load calibration matrix files.");
		return false;
	}

	// Load rotation matrices
	if (twoViewParamPathes.pathVD2.empty())
	{
		if (!loadRotations(twoViewParamPathes.pathVD1))
			IOUtils::showMessage("Failed to load Vanishing Direction file.");
	}
	else
	{
		if (!loadRotations(twoViewParamPathes.pathVD1, twoViewParamPathes.pathVD2))
			IOUtils::showMessage("Failed to load Vanishing Direction files.");
	}
}

bool HomographyEstimator::save(const std::string filename)
{
	ofstream fout(filename, ofstream::out);

	if (!fout.is_open())
		return false;

	fout << "% First calibration matrix" << endl;
	fout << "K1=" << K1 << ";" << endl;

	fout << "% Second calibration matrix" << endl;
	fout << "K2=" << K2 << ";" << endl;

	fout << "% Rotation matrix" << endl;
	fout << "R=" << R << ";" << endl;

	fout << "% Vanishing directions" << endl;
	fout << "VD=" << R1 << ";" << endl;

	fout.close();

	return true;
}