#pragma once

#include <cv.hpp>
#include <opencv2\core\core.hpp>
#include <opencv2\stitching\warpers.hpp>
#include <opencv2\stitching\detail\seam_finders.hpp>
#include <opencv2\stitching\detail\blenders.hpp>

#include <boost\filesystem.hpp>
#include <boost\Math\distributions\binomial.hpp>
#include <boost\functional\hash.hpp>

#include <fstream>
#include <unordered_map>

#include "JLinkage\JLinkage.h"
#include "JLinkage\RandomSampler.h"

#include "ClusterUtils.h"
#include "IOUtils.h"
#include "BaseClustering.h"

static const int	MAX_POINTS_NUM = 516;
static const double	MATCH_THRESHOLD = 400;

static const int CLUSTER_UNCONSTRAINED_JLINKAGE = 0;
static const int CLUSTER_CONSTRAINED_JLINKAGE = 1;
static const int CLUSTER_UNCONSTRAINED_RANSAC = 2;
static const int CLUSTER_CONSTRAINED_RANSAC = 3;

enum CLUSTERING_METHOD {
	CLUSTERING_SEQ_RANSAC	= 1,
	CLUSTERING_CC_RANSAC	= 2,
	CLUSTERING_SA_RCM		= 3,
	CLUSTERING_J_LINKAGE	= 4,
	CLUSTERING_T_LINKAGE	= 5
};

const int MERGE_JACCARD = 0;
const int MERGE_NORMAL_CHECK = 1;
const int MERGE_T_ALIGN = 2;
const int MERGE_T_EUCLIDEAN = 3;

const int REFINE_UNCONSTRAINED = 0;
const int REFINE_CONSTRAINED = 1;
const int REFINE_BOTH = 2;

const int STITCH_TYPE_GRAPH_CUT = 1;
const int STITCH_TYPE_MEAN = 2;

class ClusterSort
{
public:
	bool operator() (const sClLnk *left, const sClLnk *right) const
	{
		return left->mBelongingPts.size() > right->mBelongingPts.size();
	}
};


struct EqKeypoints
{
    EqKeypoints(const double &threshold)
    : m_threshold(threshold) {}

    bool operator()(const cv::Point2f& a, const cv::Point2f& b) const
    {
        return norm(a - b) < m_threshold;
    }

	const double m_threshold;
};

bool ClusterSizeSort(const PointPairCluster &left, const PointPairCluster &right);

//--------------------------------------
// Image Stitching class
//--------------------------------------

class MultiPlanarFitting: public BaseClustering
{
private:
	cv::Mat m_R1, m_R2;		// Rotation cv::Matrix w.r.t Manhattan-World coordinates
	cv::Mat m_R;			// Rotation cv::Matrix
	cv::Mat m_VD[2];		// Vanishing Direction
	cv::Mat m_K[2];			// Calibration cv::Matrix
	cv::Mat m_n[3];			// normal std::vector of three perpecdicular direction i Manhattan-World

	// Model esticv::Mator
	HomographyEstimator m_modelEstimator;
	
	// Point index mapper (used to find global point index)
	std::unordered_map<cv::Point2f, unsigned int, PointHash> point_indexer;
	
	// J-Linkage Clustering
	Clusters JLinkageClustering(HomographyEstimator &estimator, float inlierThreshold = 2.0);

	Clusters IterativeJLinkage(std::vector<cv::Mat> &homographies, float mInlierThreshold);

	// Iterative Refinement of clusters
	void IterativeRefinement(std::vector<PointPairCluster> &mClustersList);

	Clusters RANSACClustering(const HomographyEstimator &estimator, int n_iter, double reproj_thr);

	//------------------------------------------
	// CLUSTER MODIFICATION FUNCTIONS
	//------------------------------------------

	// Merge two clusters where the Jaccard distance of two clusters is higher than the given threshold, that the range is [0, 1].
	// The function will merge clusters until no cluster cn be merged.
	bool mergeCluster(std::vector<PointPairCluster> &clusters, int type, const double thr);

	// Merge two clusters where the Jaccard distance of two clusters is higher than the given threshold, that the range is [0, 1].
	// The function will merge clusters by order of cardinality.
	bool mergeClusterDebug(std::vector<PointPairCluster> &clusters, int type, const double thr);
	
	// Iterative refinement of clusters:
	// For each iteration, the function computes homography and its consensus set. Then merge clusters until no clusters can be merged.
	void iterativeRefinement1(std::vector<PointPairCluster> &clusters, const double thr_reproj, const double thr_merge, const int merge_type);

	// Iterative refinement of clusters:
	// For each iteration, the function computes homography and its consensus set. Then merge clusters with the lowest Jaccard distance.
	void iterativeRefinement2(std::vector<PointPairCluster> &clusters, const double thr_reproj, const double thr_merge, const int merge_type);

	void collectInliers(PointPairCluster &cluster, const double reproj_thr);

	void collectInliers(std::vector<PointPairCluster> &mClustersList, bool computeHomography, const double reproj_thr = 2.0);

	void collectInliersUnique(Clusters &clusters_, const double reproj_thr = 3.0);
	
	void outliersRejection(std::vector<PointPairCluster> &clusters, int MSS, double p, double alpha_thr = 0.01);

	//------------------------------------------
	// HOMOGRAPHY RELATED FUNCTIONS
	//------------------------------------------

	// Compute normal std::vector
	int computeNormal(const cv::Mat &H);

	void setNormals(std::vector<PointPairCluster> &mClustersList);

	void setHomography(const HomographyEstimator &estimator, Clusters &clusters);
	
	// Compute homographies using point correspondences of each cluster,
	// and point correspondences of all possible pair of clusters.
	std::vector<cv::Mat> computeSingleNPairHomographies(const Clusters &clusters, const HomographyEstimator &estimator);

	Clusters MultiplePlaneDetection(Clusters &clusters, const HomographyEstimator &estimator, const double thr_reproj);

	// Compute distance between two clusters
	// The function is appeared in Multiple Plane Detection in Image Pairs using J-Linkage
	double computeMergeDistance(const Cluster &cluster1, const Cluster &cluster2, const double threshold);

	void RANSACClusterRefinement(std::vector<PointPairCluster> &mClustersList, const double reproj_thr);

	//------------------------------------------
	// UTILITY FUNCTIONS
	//------------------------------------------

	// Calculate euclid distance
	double euclidDistance(cv::Mat& vec1, cv::Mat& vec2);

	// Find the index of nearest neighbor point from keypoints.
	int nearestNeighbor(cv::Mat& vec, std::vector<cv::KeyPoint>& keypoints,
		cv::Mat& descriptors);

	// Decompose R_xz cv::Matrices from the rotation cv::Matrix
	cv::Mat decomposeRotationMatrix(const cv::Mat &R);

public:

	static int test_sequence;
	
	//------------------------------------------
	// Constructor/Destructor
	//------------------------------------------

	MultiPlanarFitting(void);
	~MultiPlanarFitting(void);
	
	//------------------------------------------
	// Interface Functions
	//------------------------------------------

	void DoExperiments(HomographyEstimator &estimator, const char *resultFolderName, const ExperimentParameters &params);
	
	//------------------------------------------
	// FILE INPUT INTERFACE FUNCTIONS
	//------------------------------------------

	// Load Manhattan-world normal std::vectors from file
	bool loadNormals(const char *filename);

	// Load 3x3 cv::Matrix from file
	cv::Mat load3x3Matrix(const std::string &filename);

	// Load pre-computed clusters from file
	bool loadClusters(const char *filename, std::vector<PointPairCluster> &mClustersList);

	// Load GT clusters
	bool loadGTClusters(const std::vector<cv::String> &imgPathes, int type);
	
	//------------------------------------------
	// FILE OUTPUT INTERFACE FUNCTIONS
	//------------------------------------------

	void saveClusters(const char *filename, const std::vector<PointPairCluster> &mClustersList);

	void saveEstimatedParameters(const char *filename, const std::vector<PointPairCluster> &mClustersList);
	
	// Draw points of the cluster on given image
	void DrawPointsOnImage(cv::Mat &image, const PointPairCluster &cluster, const cv::Scalar &color);
};

// Compute the number of inliers of given points with respect to the given homography.
// Error is computed based on arithmetic error (re-projection error)
int computeNumOfInliers(const cv::Mat &H, const std::vector<cv::Point2f> &points1,
	const std::vector<cv::Point2f> &points2, double threshold);
