#include "types.h"

#include <stdexcept>
#include <fstream>
#include <string>

using std::cout;
using std::endl;
using std::ofstream;
using std::string;
using std::vector;

using cv::Mat;
using cv::Point;
using cv::Point2f;
using cv::Scalar;
using cv::RNG;
using cv::cvtColor;
using cv::circle;

std::ostream& operator<<(std::ostream& os, const Match &match)
{
	os << "[" << match.first.x << ", " << match.first.y << ", "
		<< match.second.x << ", " << match.second.y << "]";
	return os;
}

bool ClusterSortByPoints(const Cluster &left, const Cluster &right)
{
	return left.points1.size() > right.points1.size();
}

Cluster::Cluster()
{

}

Cluster::Cluster(vector<Match> points)
{
	for (unsigned int pi = 0; pi < points.size(); pi++) {
		points1.push_back(points[pi].first);
		points2.push_back(points[pi].second);
	}
}

size_t Cluster::size() const
{
	return points1.size();
}

Match Cluster::getMatch(int index) const
{
	return Match(points1[index], points2[index]);
}

void Cluster::print()
{
	cout << "---Translation vector----" << endl;
	cout << translation.t() << endl;
	cout << "---Plane normal vector---" << endl;
	cout << plane_normal.t() << endl;
	cout << "-------------------------" << endl;

}

std::ostream& operator << (std::ostream& os, const Cluster &cluster)
{
	os << cluster.translation << endl;
	os << cluster.plane_normal << endl;

	return os;
}

//-------------------------------
// Constructors
//-------------------------------

// Basic constructor: do nothing
Clusters::Clusters() {}

Clusters::Clusters(int num_of_clusters)
{
	matches.resize(num_of_clusters);
}

// Construct the clusters with point vectors.
Clusters::Clusters(const vector<Point2f> &points1, const vector<Point2f> &points2)
{
	if (points1.size() != points2.size())
		throw new std::invalid_argument("The number of points in input vectors should be the same");

	for (size_t pi = 0; pi < points1.size(); pi++)
		addPoint(points1[pi], points2[pi], OUTLIER_CLUSTER_INDEX);
}

// Construct the clusters with point vectors.
Clusters::Clusters(int num_of_clusters, const vector<Point2f> &points1, const vector<Point2f> &points2)
{
	initialize(num_of_clusters, points1, points2);
}

//-------------------------------
// Getters
//-------------------------------

// Get a vector of homographies.
vector<Mat> Clusters::getHomographies()
{
	vector<Mat> result;

	for each (Cluster cluster in matches) {
		result.push_back(cluster.Homography);
	}

	return result;
}

vector<Mat> Clusters::getNonEmptyHomographies()
{
	vector<Mat> result;

	for each (Cluster cluster in matches) {
		if (!cluster.Homography.empty()) {
			result.push_back(cluster.Homography);
		}
	}

	return result;
}

// Get an index of cluster that contains the given match.
int Clusters::getClusterIndex(const Match &match) const
{
	if (indexer.find(match) == indexer.end())
		return POINT_DOES_NOT_EXIST;

	return indexer.find(match)->second;
}

const Cluster &Clusters::getOutliers() const
{
	return outliers;
}

size_t Clusters::size() const
{
	return matches.size();
}

// Get the number of points
size_t Clusters::points() const
{
	size_t num = 0;
	for (int i = 0; i < matches.size(); i++) {
		num += matches[i].size();
	}
	num += outliers.size();

	return num;
}

void Clusters::initialize(int num_clusters)
{
	matches.resize(num_clusters);
}

// Construct the clusters with point vectors.
void Clusters::initialize(int num_of_clusters, const vector<Point2f> &points1, const vector<Point2f> &points2)
{
	matches.resize(num_of_clusters);

	if (points1.size() != points2.size())
		throw new std::invalid_argument("The number of points in input vectors should be the same");

	for (size_t pi = 0; pi < points1.size(); pi++)
		addPoint(points1[pi], points2[pi], OUTLIER_CLUSTER_INDEX);
}

void Clusters::addCluster()
{
	matches.push_back(Cluster());
}

// add point to the designated cluster.
void Clusters::addPoint(Point2f point1, Point2f point2, int cluster_index)
{
	if (cluster_index == size())
		matches.push_back(Cluster());
	else if (cluster_index >= (int)size())
		throw new std::out_of_range("Cluster index is out of range with index " + cluster_index);

	// If the key value is alread assigned, remove it
	std::unordered_map<Match, int, MatchHash>::iterator iter = indexer.find(Match(point1, point2));
	if (iter != indexer.end())
	{
		if (iter->second >= 0)
		{
			int ind = iter->second;
			for (size_t i = 0; i < matches[ind].size(); i++)
			{
				if (matches[ind].points1[i] == point1 && matches[ind].points2[i] == point2)
				{
					matches[ind].points1.erase(matches[ind].points1.begin() + (int)i);
					matches[ind].points2.erase(matches[ind].points2.begin() + (int)i);
					break;
				}
			}
		}
		else
		{
			for (size_t i = 0; i < outliers.size(); i++)
			{
				if (outliers.points1[i] == point1 && outliers.points2[i] == point2)
				{
					outliers.points1.erase(outliers.points1.begin() + (int)i);
					outliers.points2.erase(outliers.points2.begin() + (int)i);
					break;
				}
			}
		}
	}

	// If given cluster index is correct cluster index, add the given points to the cluster
	if (cluster_index >= 0 && cluster_index < size())
	{
		matches[cluster_index].points1.push_back(point1);
		matches[cluster_index].points2.push_back(point2);
		indexer[Match(point1, point2)] = cluster_index;
	}
	// If given cluster index is negative, add the given points to the outliers list
	else
	{
		outliers.points1.push_back(point1);
		outliers.points2.push_back(point2);
		indexer[Match(point1, point2)] = OUTLIER_CLUSTER_INDEX;
	}
}

// Remove clusters of given range [beg_index, end_index).
// @param beg_index first index of cluster that will be removed
// @param end_index last index of cluster that will be removed
void Clusters::remove(int beg_index, int end_index)
{
	assert(beg_index >= 0 && beg_index <= end_index && end_index <= size());

	for (int i = beg_index; i < end_index; i++)
	{
		// Change the indexer
		for (int j = 0; j < matches[i].size(); j++) {
			indexer[matches[i].getMatch(j)] = -1;
		}

		// Move the matches to the outlier cluster
		outliers.points1.insert(outliers.points1.end(), matches[i].points1.begin(), matches[i].points1.end());
		outliers.points2.insert(outliers.points2.end(), matches[i].points2.begin(), matches[i].points2.end());
	}

	matches.erase(matches.begin() + beg_index, matches.begin() + end_index);
}

// Remove clusters that have inliers less than given threshold
void Clusters::remove(int num_of_inliers)
{
	vector<Cluster>::iterator iter = matches.begin();

	while (iter != matches.end()) {
		if (iter->size() < num_of_inliers) {
			for (size_t i = 0; i < iter->size(); i++) {
				indexer[iter->getMatch(i)] = -1;
			}
			iter = matches.erase(iter);
		}
		else {
			iter++;
		}
	}
}

// Remove point match from the cluster
void Clusters::remove(Match match)
{
	int index = indexer[match];
	
	if (index == OUTLIER_CLUSTER_INDEX) {
		for (unsigned int i = 0; i < outliers.size(); i++) {
			if (outliers.points1[i] == match.first && outliers.points2[i] == match.second) {
				outliers.points1.erase(outliers.points1.begin() + i);
				outliers.points2.erase(outliers.points2.begin() + i);
				indexer.erase(match);
				return;
			}
		}
	}
	else {
		for (unsigned int i = 0; i < matches[index].points1.size(); i++) {
			if (matches[index].points1[i] == match.first && matches[index].points2[i] == match.second) {
				matches[index].points1.erase(matches[index].points1.begin() + i);
				matches[index].points2.erase(matches[index].points2.begin() + i);
				indexer.erase(match);
				return;
			}
		}
	}
}

// Remove empty clusters
int Clusters::removeEmptyClusters()
{
	int num_removed = 0;
	vector<Cluster>::iterator iter = matches.begin();

	while (iter != matches.end()) {
		if (iter->size() == 0) {
			iter = matches.erase(iter);
			num_removed++;
		}
		else {
			iter++;
		}
	}

	// Update point indexer
	refreshIndices();

	return num_removed;
}

void Clusters::clear(int cluster_index)
{
	for (size_t i = 0; i < matches[cluster_index].size(); i++)
		indexer[matches[cluster_index].getMatch(i)] = -1;

	matches[cluster_index].points1.clear();
	matches[cluster_index].points2.clear();
}

// Sort clusters
void Clusters::sort()
{
	std::sort(matches.begin(), matches.end(), ClusterSortByPoints);

	refreshIndices();
}

Mat Clusters::toImage(const Mat &image, bool draw_outlier)
{
	Mat result;
	RNG rng(0xFFFFFFFF);
	int radius = 3;

	cvtColor(image, result, CV_BGR2GRAY);
	cvtColor(result, result, CV_GRAY2BGR);

	for (size_t ci = 0; ci < matches.size(); ci++)
	{
		int icolor = (unsigned)rng;
		Scalar color = Scalar(icolor & 255, (icolor >> 8) & 255, (icolor >> 16) & 255);

		for (size_t pi = 0; pi < matches[ci].size(); pi++)
			circle(result, matches[ci].points1[pi], 3, color, 2);
	}

	if (draw_outlier)
	{
		for (size_t pi = 0; pi < outliers.size(); pi++)
		{
			line(result, Point((int)outliers.points1[pi].x - radius, (int)outliers.points1[pi].y - radius),
				Point((int)outliers.points1[pi].x + radius, (int)outliers.points1[pi].y + radius), CV_RGB(255, 0, 0), 2);
			line(result, Point((int)outliers.points1[pi].x + radius, (int)outliers.points1[pi].y - radius),
				Point((int)outliers.points1[pi].x - radius, (int)outliers.points1[pi].y + radius), CV_RGB(255, 0, 0), 2);
		}
	}

	return result;
}

// return true if the clusters are empty
bool Clusters::empty() const
{
	if (matches.size() == 0 && outliers.size() == 0)
		return true;

	return false;
}

// save clusters to plane text file
bool Clusters::savePointClusters(const string filename)
{
	ofstream fout(filename, ofstream::out);

	if (!fout.is_open())
		return false;

	for each (Cluster cluster in matches)
	{
		size_t n_points = cluster.size();
		fout << n_points << endl;

		for (size_t pi = 0; pi < n_points; pi++)
		{
			fout << cluster.points1[pi].x << " " << cluster.points1[pi].y << " " << 
				cluster.points2[pi].x << " " << cluster.points2[pi].y << endl;
		}
	}

	fout.close();

	return true;
}

bool Clusters::save(const std::string filename, bool use_matlab_format)
{
	ofstream fout(filename, ofstream::out);

	if (!fout.is_open())
		return false;

	if (use_matlab_format) {
		int num_of_clusters = matches.size();
		// Save point matches
		fout << "% Clusters hard-coding" << endl;
		fout << "clusters=cell(1," << num_of_clusters << ");" << endl;
		for (int i = 0; i < num_of_clusters; i++)
		{
			fout << "clusters{" << i + 1 << "}=zeros(" << matches[i].size() << ", 4);" << endl;
			for (int j = 0; j < matches[i].size(); j++) {
				fout << "clusters{" << i + 1 << "}(" << j + 1 << ",:)="
					<< matches[i].getMatch(j) << ";" << endl;
			}
		}

		fout << "% Normal vectors" << endl;
		fout << "normals=zeros(3," << num_of_clusters << ");" << endl;
		for (int i = 0; i < num_of_clusters; i++)
		{
			if (matches[i].plane_normal.empty())
				continue;

			fout << "normals(:," << i + 1 << ")=" << matches[i].plane_normal << ";" << endl;
		}

		fout << "% Translation vectors" << endl;
		fout << "translations=zeros(3," << num_of_clusters << ");" << endl;
		for (int i = 0; i < num_of_clusters; i++)
		{
			if (matches[i].translation.empty())
				continue;

			fout << "translations(:," << i + 1 << ")=" << matches[i].translation << ";" << endl;
		}

		fout << "% Homograhpies" << endl;
		fout << "homograhpies=zeros(" << num_of_clusters << ",3,3);" << endl;
		for (int i = 0; i < num_of_clusters; i++)
		{
			if (matches[i].Homography.empty())
				continue;

			fout << "translations(" << i + 1 << ",:,:)=" << matches[i].Homography << ";" << endl;
		}
	}
	else {

	}

	fout.close();

	return true;
}

bool Clusters::operator== (const Clusters& lhs) const
{
	if (size() != lhs.size())
		return false;

	std::unordered_map<Match, int, MatchHash>::const_iterator iter;

	for (int i = 0; i < size(); i++)
	{
		for (int j = 0; j < matches[i].size(); j++)
		{
			iter = lhs.indexer.find(matches[i].getMatch(j));
			if (iter == lhs.indexer.end() || iter->second != i)
				return false;
		}
	}

	return true;
}

Cluster &Clusters::operator[](const int index)
{
	return matches[index];
}

const Cluster &Clusters::operator[](const int index) const
{
	return matches[index];
}

// Update point indexer
void Clusters::refreshIndices()
{
	for (size_t i = 0; i < matches.size(); i++)
	{
		for (size_t j = 0; j < matches[i].size(); j++)
		{
			indexer[matches[i].getMatch(j)] = i;
		}
	}
}
