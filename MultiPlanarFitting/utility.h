#pragma once

#include "types.h"

#include <opencv2\opencv.hpp>
#include <string>

double computeCosineSimilarity(const cv::Mat &vec1, const cv::Mat &vec2);

double computeLineDistance(const cv::Mat &line, const cv::Point2f point);

cv::Point2f computeCentroid(const std::vector<cv::Point2f> &points);

std::vector<double> solveDeficientRankSystem(cv::Mat Vy, cv::Mat v);

//-----------------------------
// Operations with Vanishing Lines
//-----------------------------

// Compute vanishing lines from given calibration matrix K and vanishing
// directions VD. Each column of VD represents the vanishing direction.
// Each column of returning matrix will represents a vanishing line on
// the image.
cv::Mat computeVanishingLines(const cv::Mat &K, const cv::Mat &VD);

// Separte points with given vanishing line VL. Points on one side of
// vanishing line will be stored to points1_out, and points on the other
// side will be stored to points2_out.
void separatePointsVL(const std::vector<cv::Point2f> &points_in, const cv::Mat &VL,
	std::vector<cv::Point2f> &points1_out, std::vector<cv::Point2f> &points2_out);

// Separte point matches with given vanishing line VL. Points on one side of
// vanishing line will be stored to points1_out, and points on the other
// side will be stored to points2_out.
void separateMatchesVL(const std::vector<cv::Point2f> &points1_in, const std::vector<cv::Point2f> &points2_in,
	const cv::Mat &VL, std::vector<Match> &pointsGroup1_out, std::vector<Match> &pointsGroup2_out);

// Separte point matches with given vanishing line VL. Points on one side of
// vanishing line will be stored to points1_out, and points on the other
// side will be stored to points2_out.
void separateMatchesVL(const std::vector<cv::Point2f> &points1_in, const std::vector<cv::Point2f> &points2_in,
	const cv::Mat &VL1, const cv::Mat &VL2, std::vector<Match> &pointsGroup1_out, std::vector<Match> &pointsGroup2_out);

// Seperate point matches (points1_in, points2_in) into two groups using given vanishing
// line VL, then pick the larger group and save them to points1_out and points2_out.
// The point matches will be separated with only points in points1_in.
void pickLargerGroupVL(const std::vector<cv::Point2f> &points1_in, const std::vector<cv::Point2f> &points2_in,
	const cv::Mat &VL, std::vector<cv::Point2f> &points1_out, std::vector<cv::Point2f> &points2_out);

// Seperate point matches into two groups using given vanishing lines VL1 and VL2,
// then pick the larger group and save them to points1_out and points2_out.
void pickLargerGroupVL(const std::vector<cv::Point2f> &points1_in, const std::vector<cv::Point2f> &points2_in,
	const cv::Mat &VL1, const cv::Mat &VL2, std::vector<cv::Point2f> &points1_out, std::vector<cv::Point2f> &points2_out);

//-----------------------------
// Homogeneous System Utilities
//-----------------------------

// Normalize the P so that the last row of P has 1s. Each column of P
// represents a point in a homogenous coordinate system.
cv::Mat pointsFromHomogeneous(const cv::Mat &P);

//-----------------------------
// Debug Utilities
//-----------------------------

// Convert opencv style data type to readable string. For example,
// if type == CV_64F, result string will contain 'CV_64F'.
std::string type2str(int type);

// Draw lines on given image. Each column of L represents a line vector.
cv::Mat drawLinesOnImage(const cv::Mat &image, const cv::Mat &L, int thickness = 1, bool convertToGray = true);

// Generate random vector of colors.
std::vector<cv::Scalar> generateRandomColors(int numOfColors, uint64 seed = 0xFFFFFFFF);
