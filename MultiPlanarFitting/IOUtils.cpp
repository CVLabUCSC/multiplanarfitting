#include "IOUtils.h"

#include <fstream>

#include <boost\filesystem.hpp>

using std::ifstream;
using std::ofstream;
using std::string;
using std::vector;

using std::cout;
using std::endl;

using cv::Mat;
using cv::Point2f;

#if defined(_WIN32) || defined(WIN32) || defined(__CYGWIN__) || defined(__MINGW32__) || defined(__BORLANDC__)
#define OS_WIN
#endif

namespace IOUtils
{
	void TwoViewParamPathes::print()
	{
		cout << "--- TwoViewParamPathes ---" << endl;
		cout << "First camera calibration file path: " << pathCamCalib1 << endl;
		cout << "Second camera calibration file path: " << pathCamCalib2 << endl;
		cout << "First image file path: " << pathImg1 << endl;
		cout << "Second image file path: " << pathImg2 << endl;
		cout << "Matching points file path: " << pathMatches << endl;
		cout << "First vanishing direction file path: " << pathVD1 << endl;
		cout << "Second vanishing direction file path: " << pathVD2 << endl;
		cout << "Ground-Truth image path: " << pathGTCluster << endl;
		cout << "Exclude-Map image path: " << pathExcludeMap << endl;
		cout << "--------------------------" << endl;
	}

	// Retrieve folder name from given path
	string retrieveFoldername(const string &path)
	{
		boost::filesystem::path rootpath(path);
		
		if (!boost::filesystem::is_directory(rootpath) || rootpath.filename() == ".") {
			return rootpath.parent_path().stem().string();
		}
		else
			return rootpath.stem().string();
	}

	void showMessage(const string &msg)
	{
		cout << msg << endl;
#ifdef OS_WIN
		system("pause");
#endif
	}

	// Load parameters for two view case that contains two elements for each parameter.
	vector<TwoViewParamPathes> loadTwoViewSturucture(const string &filename)
	{
		vector<TwoViewParamPathes> result;
		ifstream fin(filename);

		if (!fin.is_open())
		{
			showMessage("Not able to open given file: " + filename);
			return result;
		}

		for (std::string path; std::getline(fin, path);)
		{
			size_t firstIndex = path.find_first_not_of(' ');
			char firstChar = path[firstIndex];
			if (firstChar == '#')
				continue;

			if (firstIndex != 0)
				path = path.substr(firstIndex, path.size());
						
			result.push_back(loadTwoViewParams(path));
		}

		fin.close();

		return result;
	}

	// Load parameter file pathes of two view case
	TwoViewParamPathes loadTwoViewParams(string rootFolder)
	{
		TwoViewParamPathes twoViewParamPathes;
		string ext = rootFolder.substr(rootFolder.length() - 4, 4);
		string filename;
		char lastChar = rootFolder[rootFolder.length() - 1];
		
		if (ext.compare(".txt") == 0) {
			filename = rootFolder;
			boost::filesystem::path currentPath(rootFolder);
			rootFolder = currentPath.parent_path().string() + '\\';
			//twoViewParamPathes.pairname = currentPath.filename().string();
		}
		else {
			if (lastChar != '\\' || lastChar != '/') {
				rootFolder = rootFolder + '\\';
				filename = rootFolder + "setup.txt";
			}
			else {
				filename = rootFolder + "setup.txt";
			}
		}

		ifstream fin(filename);

		if (!fin.is_open())
		{
			showMessage("Not able to open 'setup.txt' file from given path: " + rootFolder);
			return twoViewParamPathes;
		}

		twoViewParamPathes.dataname = retrieveFoldername(rootFolder);

		for (std::string line; std::getline(fin, line);)
		{
			size_t firstIndex = line.find_first_not_of(' ');
			size_t lastIndex = line.find_last_of('#');
			char firstChar = line[firstIndex];
			if (firstChar == '#')
				continue;

			if (firstIndex != 0 || lastIndex != line.length())
				line = line.substr(firstIndex, lastIndex);

			size_t pos = line.find("=");
			string param_name = line.substr(0, pos++);
			string param_val = line.substr(pos, line.size());

			if (param_name.compare("K1") == 0)
			{
				twoViewParamPathes.pathCamCalib1 = rootFolder + param_val;
			}
			else if (param_name.compare("K2") == 0)
			{
				twoViewParamPathes.pathCamCalib2 = rootFolder + param_val;
			}
			else if (param_name.compare("image1") == 0)
			{
				twoViewParamPathes.pathImg1 = rootFolder + param_val;
			}
			else if (param_name.compare("image2") == 0)
			{
				twoViewParamPathes.pathImg2 = rootFolder + param_val;
			}
			else if (param_name.compare("match") == 0)
			{
				twoViewParamPathes.pathMatches = rootFolder + param_val;
			}
			else if (param_name.compare("VD1") == 0)
			{
				twoViewParamPathes.pathVD1 = rootFolder + param_val;
			}
			else if (param_name.compare("VD2") == 0)
			{
				twoViewParamPathes.pathVD2 = rootFolder + param_val;
			}
			else if (param_name.compare("GroundTruth") == 0)
			{
				twoViewParamPathes.pathGTCluster = rootFolder + param_val;
			}
			else if (param_name.compare("ExcludeMap") == 0)
			{
				twoViewParamPathes.pathExcludeMap = rootFolder + param_val;
			}
			else
			{
				cout << "Undefined parameter: " << param_name << endl;
			}
		}

		if (twoViewParamPathes.pairname.empty() && !twoViewParamPathes.pathImg1.empty() && !twoViewParamPathes.pathImg2.empty()) {
			boost::filesystem::path image1Path(twoViewParamPathes.pathImg1);
			boost::filesystem::path image2Path(twoViewParamPathes.pathImg2);

			twoViewParamPathes.pairname = image1Path.filename().string() + "-" + image2Path.filename().string();
		}

		twoViewParamPathes.print();

		fin.close();

		return twoViewParamPathes;
	}

	// Load clusters from given text file
	Clusters loadClusters(const string &filename)
	{
		ifstream fin(filename, ifstream::in);
		Clusters clusters;
		int cluster_ind = 0;
		string line;

		if (!fin.is_open())
		{
			cout << "Not able to open clusters data file: " << strerror(errno) << endl;;
			return clusters;
		}

		for (std::string line; std::getline(fin, line);)
		{
			if (line.empty())
				continue;

			size_t pos = line.find("=");
			string param_name = line.substr(0, pos++);
			int num_of_points = stoi(line.substr(pos, line.size()));
			cv::Point2f pt1, pt2;

			if (param_name.compare("num") == 0)
			{
				clusters.addCluster();

				for (int i = 0; i < num_of_points; i++)
				{
					fin >> pt1.x >> pt1.y >> pt2.x >> pt2.y;
					clusters.addPoint(pt1, pt2, cluster_ind);
				}

				cluster_ind++;
			}
			else if (param_name.compare("outliers") == 0)
			{
				for (int i = 0; i < num_of_points; i++)
				{
					fin >> pt1.x >> pt1.y >> pt2.x >> pt2.y;
					clusters.addPoint(pt1, pt2, OUTLIER_CLUSTER_INDEX);
				}
			}
			else
			{
				showMessage("File was corrupted! Read line: " + line);
				return clusters;
			}
		}

		fin.close();

		return clusters;
	}
	
	// Load point correspondences from given file
	bool loadMatches(const string &filename, vector<Point2f> &points1, vector<Point2f> &points2)
	{
		// Validation
		if (filename.empty())
			return false;

		ifstream fin;

		fin.open(filename, ifstream::in);
		int num;
		Point2f pt1, pt2;

		if (!fin.is_open())
		{
			cout << "Not able to open the match data file: " << strerror(errno) << endl;;
			return false;
		}

		fin >> num;

		for (int i = 0; i < num; i++)
		{
			fin >> pt1.x >> pt1.y >> pt2.x >> pt2.y;
			points1.push_back(pt1);
			points2.push_back(pt2);
		}

		fin.close();

		return true;
	}


	// Load 3x3 matrix from given file
	Mat load3x3Matrix(const string &filename)
	{
		Mat mat(3, 3, CV_64F);
		ifstream fin(filename, ifstream::in);

		if (!fin.is_open())
		{
			printf("Not able to open the 3x3 matrix data file: %s\n", strerror(errno));
			return Mat();
		}

		fin >> mat.at<double>(0, 0);
		fin >> mat.at<double>(0, 1);
		fin >> mat.at<double>(0, 2);
		fin >> mat.at<double>(1, 0);
		fin >> mat.at<double>(1, 1);
		fin >> mat.at<double>(1, 2);
		fin >> mat.at<double>(2, 0);
		fin >> mat.at<double>(2, 1);
		fin >> mat.at<double>(2, 2);

		fin.close();

		return mat;
	}

	// Save the given matches as a matlab script file
	void saveMatchesToMatlabScript(const string &filename, const vector<Point2f> &points1, const vector<Point2f> &points2)
	{
		ofstream fout(filename, ofstream::out);

		int num_of_points = (int)points1.size();

		fout << "% Point matches hard-coding" << endl;
		fout << "P1=zeros(2," << num_of_points << ");" << endl;
		fout << "P2=zeros(2," << num_of_points << ");" << endl;

		for (int i = 0; i < num_of_points; i++)
		{
			fout << "P1(1, " << i + 1 << ")=" << points1[i].x << ";" << endl;
			fout << "P1(2, " << i + 1 << ")=" << points1[i].y << ";" << endl;
			fout << "P2(1, " << i + 1 << ")=" << points2[i].x << ";" << endl;
			fout << "P2(2, " << i + 1 << ")=" << points2[i].y << ";" << endl;
		}

		fout.close();
	}

	void saveModels(const string &filename, const vector<HomographyModel> &models)
	{
		ofstream fout(filename, ofstream::out);
		int num_of_models = (int)models.size();

		fout << "% Models hard-coding" << endl;
		fout << "homographies=zeros(3, 3, " << num_of_models << ");" << endl;
		fout << "translations=zeros(3," << num_of_models << ");" << endl;
		fout << "normals=zeros(3," << num_of_models << ");" << endl;
		for (int i = 0; i < num_of_models; i++)
		{
			fout << "homographies(:, :, " << i+1 << ") = " << models[i].Homography << ";" << endl;
			fout << "translations(:, " << i+1 << ") = " << models[i].translation << ";" << endl;
			fout << "normals(:, " << i+1 << ") = " << models[i].plane_normal << ";" << endl;
		}

		fout.close();
	}
};