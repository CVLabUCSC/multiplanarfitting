#include <vector>

#include <opencv2\opencv.hpp>

#include <boost/dynamic_bitset.hpp>

#include "types.h"
#include "BaseClustering.h"
#include "HomographyEstimator.h"

namespace HypergraphModeSeeking {

	class MSHClustering: public BaseClustering {
	private:
		double computeEpanechnikovKernel(double val, double bandwidth);

		// Compute MTD values for each vertex (model)
		void computeMinimumTDistance(std::vector<double> &t_dists);

		// Pick models with autoriy peaks
		std::vector<int> pickModels(const std::vector<double> &mtd_vales);

		// Pick models with autoriy peaks
		std::vector<int> pickModels(const std::vector<double> &mtd_vales, int num_of_models_to_peak);

	public:
		MSHClustering();

		MSHClustering(const cv::Mat &image1, const cv::Mat &image2, const std::vector<cv::Point2f> &points1,
			const std::vector<cv::Point2f> &points2);

		/**
		 Cluster points using Mode Seeking on Hypergraph method. Models will be generated automatically.
		 */
		void doClustering(std::vector<Cluster> &clusters, HomographyEstimator &estimator, int num_of_models);

		/**
		 Cluster points with given models using Mode Seeking on Hypergraph method.
		*/
		void doClustering(std::vector<Cluster> &clusters, const std::vector<HomographyModel> &models, double constant_scale = -1);

		Clusters doClustering(HomographyEstimator &estimator, const ExperimentParameters &params, const std::string &resultFolderName);
	};

	double ScaleEstimator(const cv::Mat &H, std::vector<double> residuals, int K, double E = 2.5);
}
