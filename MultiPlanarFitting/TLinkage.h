#pragma once

#include <vector>
#include <list>
#include <unordered_map>

#include <opencv2\opencv.hpp>
#include "kdtree++\kdtree.hpp"
#include "types.h"

namespace TLinkageClustering {
	// First element represents a homography, and second element represents a normal index
	struct Homography
	{
		cv::Mat homography;
		int normalIndex;
		bool isPositiveSide;
		float inlierThreshold;

		Homography() :normalIndex(0), isPositiveSide(false), inlierThreshold(2.5f) {}
	};

	struct sDist;
	struct sClLnk;

	// Store data points information
	struct sPtLnk {
		cv::Point2f  mCoord1;
		cv::Point2f  mCoord2;
		std::vector<float> mPreferenceFunction;
		sClLnk *mBelongingCluster;
		bool mToBeUpdateKDTree;
		unsigned int mAddedIdx;
		// Used for find k nearest ..
		bool mAlreadyFound;
	};

	// Store cluster information
	struct sClLnk{
		std::vector<float> mPreferenceFunction;
		std::list<sDist *>  mPairwiseTanimotoDistance;
		std::list<sPtLnk *> mBelongingPts;
		void *mOtherDataCl; // Pointer to other user-defined data. May be useful for user-defined cluster-cluster test
	};

	// Store the tanimoto distance beetwen two cluster
	struct sDist{
		sClLnk *mCluster1;
		sClLnk *mCluster2;
		float mPairwiseTanimotoDistance;
		bool mToBeUpdated;
		bool operator()(const sDist *left, const sDist *right) const{ return left->mPairwiseTanimotoDistance < right->mPairwiseTanimotoDistance; }
	};

	struct sPtLnkPointer{
		sPtLnk *mPtLnk;
		// Used for the kd-tree
		typedef float value_type;
		inline value_type operator[](size_t const N) const
		{
			switch (N % 4)
			{
			case 0:
				return mPtLnk->mCoord1.x;
			case 1:
				return mPtLnk->mCoord1.y;
			case 2:
				return mPtLnk->mCoord2.x;
			case 3:
				return mPtLnk->mCoord2.y;
			default:
				return mPtLnk->mCoord1.x;
			}
		}
	};

	// Used for the kd-tree
	inline bool operator==(sPtLnkPointer const& A, sPtLnkPointer const& B) {
		if (A.mPtLnk == B.mPtLnk)
			return true;
		return false;
	}

	// Predicate to check if a node was already extracted in k-nn
	struct sPredicateAlreadyFoundJL {
		bool operator()(const sPtLnkPointer& A) const{
			return !A.mPtLnk->mAlreadyFound;
		}
	};

	// given two Preference function Vectors compute the tanimoto distance
	float PFTanimotoDist(const std::vector<float> &nB1, const std::vector<float> &nB2);

	class ClusterSort
	{
	public:
		bool operator() (const sClLnk *left, const sClLnk *right) const
		{
			return left->mBelongingPts.size() > right->mBelongingPts.size();
		}
	};

	class TLinkage
	{
	public:
		// Costructor and destructor
		TLinkage(float nInliersThreshold, // Scale of the algorithm
			unsigned int nModelBufferSize = 0, // Model buffer size, set to 0 to allow growable size
			bool nCopyCoord = true, // Copy the coords value of the new points or use directly the pointer? (The second choice is faster but you need to deallocate memory outside the class)		
			unsigned int nPtDimension = 3, //Used by Kd-Tree
			int nKNeighboards = -1,  //K-neighboards for each points to be used for the clusterization (neg value means use all the dataset)
			bool(*nClusterClusterDiscardTest)(const sClLnk *, const sClLnk *) = NULL, // Function handler to perform a cluster-cluster test. Should return true if the cluster can be merged by some additional condition, false otherwise.
			void(*nInitializeAdditionalData)(sClLnk *) = NULL, // Function handler -  initialization routine when additional data are created
			void(*nClusterMergeAdditionalOperation)(const sClLnk *) = NULL, // Function handler - called when two clusters are merged
			void(*nDestroyAdditionalData)(sClLnk *) = NULL // Function handler -  called when additional data are destroyed
			);
		~TLinkage();

		// Data Points accessor
		sPtLnk *AddPoint(const cv::Point2f &nCoordinates1, const cv::Point2f &nCoordinates2, unsigned char indicator = 0);

		unsigned int AddModel(Homography &homography);

		// Do a clusterization, return the final clusters
		const std::list<sClLnk *> DoTLClusterization(void(*OnProgress)(float) = NULL);

		void setMultiInliersThreshold(bool setTrue);

		// Debug functions
		cv::Mat drawPointTestVanishingLine(cv::Mat image, int normal_index);

	private:

		// Max models sizes, set to 0 to allow list
		unsigned int mModelBufferSize;
		unsigned int mCurrentModelPointer;

		bool mCopyPtsCoords;

		bool mUseVanishingLineConstraint;

		// Scale of the algorithm
		float mInliersThreshold;

		// Multiple scale threshold of the data
		bool mUseIndividualThresholds;

		// Initial Data Points
		std::list<sPtLnk *> mDataPoints;
		unsigned int mDataPointsSize;

		// Current clusters
		std::list<sClLnk *> mDataClusters;

		// Current models
		std::vector<Homography*> mModels;

		// Represents which side of vanishing directions that the point belongs to.
		std::unordered_map<Match, unsigned char, MatchHash> pointIndexer;

		// Store the distances that have to be updated
		std::list<sDist *> mDistancesToBeUpdated;
		int mDistancesToBeUpdatedSize;

		// Mode k-nearest neighboard to find
		int mKNeighboards;

		// if the Number of neighboards is set(>0), we need a kd-tree to store the neighorhood information
		KDTree::KDTree<sPtLnkPointer> mKDTree;

		// Helper threadable functions
		static void UpdateDistance(sDist * tIterDist, float *nCurrentInvalidatingDistance,
			bool(*nClusterClusterDiscardTest)(const sClLnk *, const sClLnk *));

		// Distance function
		float DistanceFunction(const Homography &model, const cv::Point2f &dataPt, const cv::Point2f &dataPtT);

		// Compute preference function
		float PreferenceFunction(const Homography &model, const cv::Point2f &coord1, const cv::Point2f &coord2);

		// Remove a model from the buffer
		// Helper function called only by addModel
		void RemoveModel(unsigned int nModelN);

		// Find k-nearest neighboard in kd-tree
		std::list<sPtLnkPointer> FindKNearest(sPtLnkPointer __V, const float __Max_R, int k, int nMaxSize);

		// Function handler to perform a cluster-cluster test. Should return true if the clusters can be merged togheter, false otherwise.
		// The test is additive to the one already performed inside J-Linkage. If function pointer is null, simply it's ignored
		bool(*mClusterClusterDiscardTest)(const sClLnk *, const sClLnk *);

		// Function handler - initialization routine when additional data are created
		void(*mInitializeAdditionalData)(sClLnk *);

		// Function handler - called when two clusters are merged
		void(*mClusterMergeAdditionalOperation)(const sClLnk *);

		// Function handler - called when additional data are destroyed
		void(*mDestroyAdditionalData)(sClLnk *);
	};
}
